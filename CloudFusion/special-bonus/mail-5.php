<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="CloudFusion Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="CloudFusion Bonuses">
      <meta property="og:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Team BizOmart">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="CloudFusion Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="CloudFusion Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <title>CloudFusion Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Kalam:wght@300;400;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'August 15 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz6.com/c/47069/398674/';
         $_GET['name'] = 'Team BizOmart';      
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://jvz6.com/c/47069/398672/';
         $_GET['name'] = 'Team BizOmart';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px;"><defs><style>.cls-1b{fill:#0068ff;}.cls-2y{fill:#f89b1c;}.cls-3w{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1b" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path><path class="cls-2y" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text><path class="cls-3w" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path><path class="cls-3w" d="M344.08,43.53v86H326.19v-86Z"></path><path class="cls-3w" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path><path class="cls-3w" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path><path class="cls-3w" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path><path class="cls-3w" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path><path class="cls-3w" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path><path class="cls-3w" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path><path class="cls-3w" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path><path class="cls-3w" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path><path class="cls-3w" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path><text></text></g></g></svg>
                     </div>
                  </div>

                  <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w500 lh140">
                  Grab My 20 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 relative">
                     <div class="mainheadline f-md-45 f-28 w400 text-center black-clr lh140">
                     <span class="w700 underline-text"> Host UNLIMITED Videos, Website Images,</span> <span class="w700 underline-text"> Training, Audios or Any Media Files </span> at at Blazing-Fast Speed at A Low One Time Fee
                     </div>
                  </div>
                  <div class="col-12 mt-md40 mt20 f-18 f-md-24 w400 text-center lh150 white-clr text-capitalize">
                     And… Supercharge Your or Client’s Websites, Apps &amp; Pages with Fast-Loading Media Content.<br class="d-none d-md-block"> NO Tech Hassles &amp; Monthly Fee Ever…
                  </div>
            </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
               <div class="col-12 responsive-video border-video">
                     <iframe src="https://cloudfusion.oppyo.com/video/embed/ypzjqhcfxg" style="width:100%; height:80%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
               </div>
            </div>
            <!-- <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li> Launch Your &amp; Client's Products &amp; Services Online with ZERO Tech Hassles.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Profit with Agency/Reseller/PLR Right Products You Ever Purchased.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Create Stunning Websites, E-stores and Membership Sites Easily</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell On Your Own Marketplace &amp; Keep 100% Profits</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Smart-Checkout Links To Get Orders Directly from Social Media, Emails or Anywhere Else</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell High In-Demand Services with <span class="w600 white-clr">Included Commercial License.</span></li>
                  </ul>
               </div>
            </div> -->
         </div>
      </div>
      <!-- Header Section End -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="f-md-40 f-28 w400 lh140 text-center black-clr">
                     Captivate Your Audience with Your Media Content<br class="d-none d-md-block"><span class="w600"> In 3 Easy Steps...</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-md-4 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 1</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/upload.webp " class="img-fluid d-block" alt="step one">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Upload</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Simply drag and drop your files into CloudFusion or upload from your PC. It supports almost all type of files - videos, Images, audios, documents etc.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 relative mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 2</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/code.webp " class="img-fluid d-block" alt="step two">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Get File URL</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        CloudFusion gives you a single line of code to share your media anywhere after optimizing it according to internet speed &amp; make multiple resolutions for faster delivery on any device.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow1.webp " class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 3</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/publish.webp " class="img-fluid d-block" alt="step three">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Publish &amp; Profit</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Just paste the code on any page or get a secure DFY file sharing URL to publish your content anywhere online to start getting eyeballs and get paid.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700  orange-clr2">"CLOUD"</span> for an Additional <span class="w700  orange-clr2">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700  orange-clr2">"CLOUDFUSION" </span> for an Additional <span class="w700  orange-clr2">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
                <!-- Timer -->
                <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w400 lh140 text-capitalize text-center black-clr">
                  “We Guarantee, CloudFusion is Going to Be The <span class="w700 orange-clr2"> 
                     <br class="d-none d-md-block">Last CLOUD Hosting App You’ll Ever Need”</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row header-list-block gx-md-5">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li><span class="w600">Host and Manage All Your Files from One Easy Dashboard –</span> Videos, Web Images, PDFs, Docs, Audio, and Zip Files.</li>
                           <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!) - </span> This lead to More Engagement, Leads &amp; Sales. </li>
                           <li><span class="w600"> Play Sales, Demo &amp; Training Videos in HD</span>  on Any Page, Site, or Members Area.</li>
                           <li><span class="w600">Tap Into HUGE E-Learning Industry - </span> Deliver Your Videos, Docs, and PDF Training on Done-For-You and Beautiful Doc Sharing Sites &amp; Pages. </li>
                           <li>Use Our App to Help <span class="w600">Speed-Up Your Website, Landing Pages &amp; Online Shops with Fast Loading &amp; Optimized Images &amp; Videos. </span> </li>
                           <li>Build Your Online Empire - <span class="w600">Deliver Digital Products Securely, Fast &amp; Easy </span></li>
                           <li><span class="w600">Generate Tons of Leads &amp; Affiliate Sales –  </span> Deliver Freebies &amp; Affiliate Bonuses to Your Subscribers Without a Hitch</li>
                           <li><span class="w600">Share Your Files Privately </span>  with Your Clients, Customers &amp; Team Members.</li>
                           <li><span class="w600">Free Hosting </span> Is Included, Unlimited Bandwidth &amp; Upto 250 GB Storage </li>
                           <li> PLUS, <span class="w600">Limited Time Free Commercial License  </span> (only for today) to Serve Your Clients</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>Elegant, <span class="w600">SEO Optimized &amp; 100% Mobile Responsive </span> File Share Pages </li>
                           <li>Inbuilt <span class="w600">Lead Management System </span></li>
                           <li><span class="w600">Access Files Anytime, Anywhere</span> Directly from The CLOUD on Any Device!</li>
                           <li><span class="w600">Highly Encrypted Unbreakable File Security</span> with SSL &amp; OTP Enabled Login </li>
                           <li>30 Days <span class="w600">Online Back-up &amp; File recovery </span> So You Never Lose Your Precious Data and Files. </li>
                           <li><span class="w600">Single Dashboard</span> to Manage All Business Files- No Need to Buy Multiple Apps 
                           </li><li><span class="w600">Manage Files Effortlessly –</span> Share Multiple Files, Full Text Search &amp; File Preview </li>
                           <li>Inbuilt Elegant Video Player with <span class="w600">HDR Support</span> </li>
                           <li><span class="w600">Real-Time Analytics </span>for Every Single File You Upload - See Downloads, Shares, etc.  </li>
                           <li><span class="w600">Easy and Intuitive</span> to Use Software with <span class="w600">Step-by-Step Video Training </span></li>
                           <li><span class="w600">100% Newbie Friendly</span> &amp; Fully CLOUD Based Software </li>
                           <li><span class="w600">Live Chat -</span> Customer Support So You Never Get Stuck or Have any Issues </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->
     
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : NEXUSGPT
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="nexusgpt-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }

                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }

                                 .cls-2, .cls-3 {
                                 fill: #00a4ff;
                                 }

                                 .cls-4 {
                                 fill: #f19b10;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                                 <g>
                                    <g>
                                       <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                       <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                       <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                    </g>
                                    <g>
                                       <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                       <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                       <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                       <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                       <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                    <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                    <g>
                                       <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                       <g>
                                          <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                          <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                       </g>
                                       <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                    
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="nexusgpt-pre-heading f-md-22 f-18 w500 lh140">
                  Exploit The NFC &amp; Ai Tech to <u>Make Multiple Recurring Income Streams </u>  
                  </div>
               </div>
               <div class="col-12 mt80 nexusgpt-head-design relative">
                  <div class="nexusgpt-gametext">
                     First-to-JVZoo Technology 
                  </div>
                  <div class=" f-md-40 f-28 w400 text-center black-clr lh140">
                     <span class="w600">Super-Easy NFC App Creates Contactless Digital Business Cards with Ai-Assistant, </span> Generates Leads, Followers, Reviews &amp; Sales  <span class="w600 underline-text2">with Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-20 f-md-24 w400 text-center lh140 white-clr text-capitalize">
               Easily Create &amp; Sell Contactless NFC Digital Cards &amp; GPT-Ai Assistants for Big Profits to Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, Coaches, Affiliates &amp; 100+ Other Niches... 
               </div>
               <div class="col-12 mt20 f-20 f-md-24 w500 text-center lh140 orange-clr2 text-capitalize">
               No Tech Skills of Any Kind Needed. No Monthly Fee Ever. 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/e786pt8zf4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="">
                     <ul class="nexusgpt-list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li><span class="w600">Nexus Of NFC Tech &amp; GPT AI</span> Revolutionise Your Marketing</li>
                        <li><span class="w600">Help Desperate Local Businesses In Any Niche</span> within Minutes</li>
                        <li><span class="w600">GPT AI Assistant Closes Leads &amp; Sales 24x7</span> on Automation</li>
                        <li><span class="w600">Impress Your Colleagues &amp; Client’s</span> with Contactless NFC card</li>
                        <li><span class="w600">Tons of Ready-To-Go Templates</span> with Free-Flow Editor</li>
                        <li>Sell High In-Demand Services with <span class="w600">Included Commercial License.</span> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/nexusgpt.webp">
               <source media="(min-width:320px)" srcset="assets/images/nexusgpt-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/nexusgpt.webp" alt="" class="img-fluid" style="width: 100%;">
            </picture>
            
         </div>
      </div>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : LinkPro
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------LinkPro Section------>
      <div class="linkpro-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w700 black-clr lh140 preheadline">
                    <span class="orange-gradient"> Are you sick & tired of losing clicks </span> with long, broken, or <br class="d-none d-md-block"> suspicious links in your emails, website, etc.?
                     <!-- First to JVZoo & <span class="under">MUST HAVE Solution</span> for All Marketers…   -->
                  </div>
               </div>


               <div class="col-12 mt20 mt-md80 relative">
                  <div class="linkpro-gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="linkpro-main-heading">
                      <div class=" f-md-45 f-28 w800 text-center white-clr lh140 ">
                           <span class="orange-gradient"> Convert ANY Long & Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, & QR Code Ready) <u>in Just 30 Seconds</u>... 
                      </div>
                    
                  </div>
               </div>

               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Builder – <span class="orange-gradient w700">QR Code, Link Cloaker, Shortener, Checkout Links & Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-md-8 col-12 mx-auto">
                  <div class="video-box">
                       <!--<video width="100%" height="auto" controls autoplay muted="muted">-->
                       <!--       <source src="assets/images/coming-soon.mp4" type="video/mp4">-->
                       <!--    </video> -->
                      <div style="padding-bottom: 56.25%;position: relative;">
                         <iframe src="https://linkpro.dotcompal.com/video/embed/x8ipwmt2vf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> No Matter What You Do Online for Living
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mx-auto">
                  <div class="key-features-bg">
                     <ul class="linkpro-list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                        <li><span class="orange-gradient w700">	Boost Your Click Through Rates</span> By Cloaking Long & Suspicious Links</li>
                        <li><span class="orange-gradient w700">	Create QR Codes</span> For Touch-Less Payments & Branding For Local Businesses</li>
                        <li><span class="orange-gradient w700">	Double Your Email Clicks & Profits</span></li>
                        <li><span class="orange-gradient w700">	Boost Followership</span> Using Ready-To-Use Bio Pages.</li>
                        <li><span class="orange-gradient w700">	Fix All Broken Links on Any Website</span> In Just A Few Clicks </li>
                        <li><span class="orange-gradient w700">	Track Even A Single Click</span> So Never Lose Affiliate Commissions Again</li>
                        <li>	Sell High In-Demand Services With <span class="orange-gradient w700">Included Commercial License.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
         <source media="(min-width:768px)" srcset="assets/images/linkpro.webp">
               <source media="(min-width:320px)" srcset="assets/images/linkpro-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/linkpro.webp" alt="" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
      <!------Trendio Section Ends------>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : LegalSuites
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="legalsuite-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#FFA829;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                     </style>
                     <g>
                        <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                           c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                           C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                           c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                           v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"/>
                        <g>
                           <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"/>
                           <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"/>
                           <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                              C336.9,72.4,330.3,66.1,330.3,54.5z"/>
                           <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"/>
                           <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"/>
                           <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"/>
                        </g>
                        <g>
                           <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                              C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"/>
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop  offset="0" style="stop-color:#0055FF"/>
                              <stop  offset="1" style="stop-color:#008CFF"/>
                           </linearGradient>
                           <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                              c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                              l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"/>
                           <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop  offset="0" style="stop-color:#0055FF"/>
                              <stop  offset="1" style="stop-color:#008CFF"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                              c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                              C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                              l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                              L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                              l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                              l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"/>
                        </g>
                        <g>
                           <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                           <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"/>
                           <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                              c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                              c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"/>
                           <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"/>
                           <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                        </g>
                     </g>
                  </svg>
                  
               </div>
            </div>
         </div>
         <div class="legalsuite-header-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="legalsuite-preheadline f-16 f-md-22 w400 white-clr lh140">
                        Kick Start Simple 7-Minute Agency Services to Website Owners… 
                     </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-45 f-26 w700 text-center white-clr lh140">
                     Analyse, Detect, & Fix Deadly Legal Flaws<br class="d-none d-md-block"> on Your Website & For Clients Just by<br class="d-none d-md-block"> <span class="cyan-clr w900">Copy-Pasting a Single Line of Code </span>
                  </div>
                  <div class="col-12 mt20 mt-md30 text-center">
                     <div class="f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">               
                        Charge BIG for Fixing Deadly Law Issues in Minutes Like ADA, GDPR, Policies,<br class="d-none d-md-block"> T&C & More | No Coding | 100% Newbie Friendly 
                     </div>
                     <img src="assets/images/legalsuites-h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                  </div>
               </div>
               <div class="row mt20 mt-md40">
                  <div class="col-md-10 col-12 mx-auto">
                     <!-- <img src="https://cdn.oppyo.com/launches/legelsuites/special/product-image.webp" class="img-fluid d-block mx-auto"> -->
                     <div class="col-12 responsive-video">
                        <iframe src="https://legelsuites.dotcompal.com/video/embed/t9ev45pyjl" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="legalsuite-feature-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="legalsuite-feature-list">
                             
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Quick-Start with Built-in Agency Website Builder</span> with Drag & Drop Editor to Start Selling Services Starting Today </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Scans Any Website with LegelSuites Analyzer</span> to detect Legal Flaws on your Website like GDPR, ADA, Privacy Policy, T&C, etc</div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Help You Make Your Website ADA Compliant</span> by Copy-Pasting a Single Line of Code </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400">
                                 <span class="w600">Get Your Website GDPR & Privacy Laws Comply</span> as per International Laws 
                              </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Generate Cookies Policies & Cookies Widgets</span> with an inbuilt GDPR Tool </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="assets/images/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Copy Paste a Single Line Of Code</span> To Make Any Of Your Business Websites 100% Compliance </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6">
                           <div class="f-18 lh140 w400">                             
                              <div class="legalsuite-feature-list">
                              <img src="assets/images/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Get Tailor-Made Legal Policies</span> and Other Legal Documents For Your's And Your Client's Websites </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="assets/images/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w600">Tap into Hot Website Agency Services to Make Huge Profits </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="assets/images/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Website Widget to add on your Agency Website</span> to Offer Free Legal & Compliance Tests to quickly generate leads </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="assets/images/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Step-by-Step Training</span> is Included to get you Started Today </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="assets/images/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Very Limited Agency License Included</span> to Serve Your Clients and Make Huge Profits </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
         <picture>
         <source media="(min-width:768px)" srcset="assets/images/legelsuites.webp">
               <source media="(min-width:320px)" srcset="assets/images/legelsuites-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/legelsuites.webp" alt="" class="img-fluid" style="width: 100%;">
         </picture>
         </div>
      </div>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : RestroSuite
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------LinkPro Section------>
      <div class="restrosuite-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" viewBox="0 0 387 252" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      style="enable-background:new 0 0 495.7 104.2; max-height:130px;" xml:space="preserve">
                     
                        <path d="M0.0500488 213.15V169.6H14.05C16.8232 169.528 19.5773 170.078 22.1101 171.21C24.2763 172.198 26.1101 173.792 27.39 175.8C28.6677 177.908 29.3197 180.336 29.2701 182.8C29.3098 185.47 28.5958 188.097 27.21 190.38C25.9094 192.541 23.9553 194.233 21.63 195.21L29.8601 213.11H20.16L13.16 196.64H8.94005V213.11L0.0500488 213.15ZM8.94005 188.93H14.07C15.7477 189.035 17.4027 188.499 18.7001 187.43C19.2586 186.895 19.6945 186.245 19.9777 185.525C20.2608 184.805 20.3844 184.032 20.34 183.26C20.3866 182.459 20.2647 181.657 19.9822 180.906C19.6997 180.155 19.263 179.472 18.7001 178.9C17.4163 177.802 15.756 177.246 14.07 177.35H8.94005V188.93Z" fill="white"></path>
                        <path d="M36.74 213.15V169.6H63.59V177.23H45.51V187H61.51V194.63H45.51V205.49H63.59V213.13L36.74 213.15Z" fill="white"></path>
                        <path d="M85.6599 213.74C82.9126 213.808 80.1831 213.279 77.6599 212.19C75.528 211.275 73.7051 209.765 72.4099 207.84C71.143 205.865 70.4985 203.555 70.5599 201.21H79.5599C79.5338 201.887 79.6753 202.559 79.9719 203.168C80.2685 203.777 80.711 204.303 81.2599 204.7C82.6036 205.622 84.2122 206.079 85.8399 206C87.3993 206.091 88.9435 205.65 90.2199 204.75C90.7414 204.339 91.1576 203.81 91.4341 203.207C91.7106 202.604 91.8394 201.943 91.8099 201.28C91.8331 200.075 91.4051 198.905 90.6099 198C89.7277 197.036 88.555 196.387 87.2699 196.15L82.8599 195.15C79.5403 194.495 76.5187 192.791 74.2399 190.29C72.1727 187.879 71.075 184.785 71.1599 181.61C71.1055 179.271 71.7158 176.965 72.9199 174.96C74.1288 173.045 75.8651 171.521 77.9199 170.57C80.3095 169.48 82.9142 168.944 85.5399 169C89.9999 169 93.5299 170.103 96.1299 172.31C97.418 173.401 98.4399 174.771 99.1176 176.317C99.7952 177.863 100.111 179.544 100.04 181.23H91.0399C91.0672 180.62 90.9543 180.011 90.7098 179.451C90.4654 178.891 90.096 178.395 89.6299 178C88.4076 177.125 86.9198 176.701 85.4199 176.8C84.0119 176.705 82.6167 177.12 81.4899 177.97C81.023 178.38 80.6559 178.892 80.4168 179.465C80.1776 180.039 80.0727 180.66 80.1099 181.28C80.0784 182.461 80.4724 183.615 81.2199 184.53C82.0285 185.472 83.1314 186.113 84.3499 186.35L88.9999 187.37C92.3575 187.97 95.4326 189.636 97.7699 192.12C99.8216 194.559 100.891 197.675 100.77 200.86C100.813 203.242 100.16 205.584 98.8899 207.6C97.6017 209.575 95.7818 211.145 93.6399 212.13C91.135 213.26 88.4072 213.811 85.6599 213.74Z" fill="white"></path>
                        <path d="M117 213.15V178H106V169.6H137V178H126V213.2L117 213.15Z" fill="white"></path>
                        <path d="M143.23 213.15V169.6H157.23C160 169.528 162.751 170.078 165.28 171.21C167.45 172.193 169.285 173.788 170.56 175.8C171.844 177.905 172.496 180.335 172.44 182.8C172.485 185.471 171.77 188.099 170.38 190.38C169.083 192.54 167.132 194.232 164.81 195.21L173.04 213.11H163.32L156.32 196.64H152.14V213.11L143.23 213.15ZM152.12 188.93H157.25C158.925 189.036 160.577 188.5 161.87 187.43C162.43 186.896 162.867 186.246 163.15 185.526C163.434 184.806 163.556 184.033 163.51 183.26C163.559 182.459 163.438 181.656 163.156 180.905C162.873 180.153 162.435 179.47 161.87 178.9C160.591 177.801 158.933 177.245 157.25 177.35H152.12V188.93Z" fill="white"></path>
                        <path d="M193 213.74C190.382 213.806 187.786 213.243 185.43 212.1C183.353 211.088 181.616 209.493 180.43 207.51C179.222 205.419 178.613 203.035 178.67 200.62V182.12C178.613 179.705 179.222 177.321 180.43 175.23C181.62 173.251 183.356 171.657 185.43 170.64C187.814 169.559 190.402 169 193.02 169C195.638 169 198.226 169.559 200.61 170.64C202.665 171.665 204.383 173.258 205.56 175.23C206.763 177.323 207.372 179.706 207.32 182.12V200.62C207.372 203.034 206.763 205.417 205.56 207.51C204.386 209.484 202.667 211.078 200.61 212.1C198.242 213.251 195.632 213.813 193 213.74ZM193 205.99C193.727 206.041 194.457 205.944 195.144 205.703C195.832 205.462 196.464 205.084 197 204.59C197.476 204.044 197.839 203.41 198.069 202.724C198.299 202.037 198.391 201.312 198.34 200.59V182.09C198.444 180.637 197.974 179.2 197.03 178.09C195.867 177.196 194.442 176.711 192.975 176.711C191.508 176.711 190.083 177.196 188.92 178.09C187.972 179.198 187.501 180.636 187.61 182.09V200.59C187.559 201.312 187.651 202.037 187.881 202.724C188.111 203.41 188.474 204.044 188.95 204.59C189.49 205.093 190.129 205.478 190.827 205.721C191.524 205.964 192.264 206.059 193 206V205.99Z" fill="white"></path>
                        <path d="M228.84 213.74C226.093 213.808 223.363 213.279 220.84 212.19C218.712 211.269 216.89 209.76 215.59 207.84C214.328 205.863 213.683 203.555 213.74 201.21H222.69C222.666 201.887 222.808 202.559 223.104 203.167C223.401 203.776 223.842 204.302 224.39 204.7C225.744 205.625 227.363 206.081 229 206C230.559 206.091 232.104 205.65 233.38 204.75C233.9 204.339 234.315 203.81 234.59 203.206C234.865 202.603 234.992 201.942 234.96 201.28C234.987 200.076 234.562 198.906 233.77 198C232.886 197.039 231.714 196.39 230.43 196.15L226.01 195.15C222.692 194.49 219.672 192.787 217.39 190.29C215.325 187.878 214.231 184.784 214.32 181.61C214.266 179.271 214.876 176.965 216.08 174.96C217.285 173.042 219.022 171.517 221.08 170.57C223.478 169.485 226.089 168.956 228.72 169.02C233.167 169.02 236.697 170.123 239.31 172.33C240.596 173.422 241.617 174.793 242.295 176.339C242.972 177.884 243.288 179.564 243.22 181.25H234.27C234.293 180.631 234.172 180.016 233.917 179.452C233.662 178.888 233.28 178.391 232.8 178C231.582 177.124 230.097 176.699 228.6 176.8C227.189 176.707 225.791 177.122 224.66 177.97C224.197 178.383 223.833 178.895 223.595 179.468C223.358 180.041 223.254 180.661 223.29 181.28C223.262 182.459 223.651 183.61 224.39 184.53C225.203 185.472 226.309 186.113 227.53 186.35L232.18 187.42C235.538 188.02 238.613 189.686 240.95 192.17C243.002 194.609 244.071 197.725 243.95 200.91C243.993 203.292 243.34 205.634 242.07 207.65C240.787 209.629 238.965 211.2 236.82 212.18C234.312 213.295 231.584 213.828 228.84 213.74Z" fill="white"></path>
                        <path d="M264.63 213.74C260.223 213.74 256.733 212.547 254.16 210.16C251.587 207.773 250.303 204.513 250.31 200.38V169.6H259.26V200.32C259.158 201.854 259.664 203.367 260.67 204.53C261.77 205.473 263.171 205.991 264.62 205.991C266.069 205.991 267.47 205.473 268.57 204.53C269.074 203.957 269.459 203.29 269.705 202.568C269.95 201.845 270.05 201.081 270 200.32V169.6H279V200.38C279 204.48 277.717 207.73 275.15 210.13C272.583 212.53 269.077 213.733 264.63 213.74Z" fill="white"></path>
                        <path d="M287.3 213.15V205.39H296V177.39H287.3V169.6H313.55V177.35H304.9V205.35H313.55V213.11L287.3 213.15Z" fill="white"></path>
                        <path d="M331.75 213.15V178H320.75V169.6H351.75V178H340.75V213.2L331.75 213.15Z" fill="white"></path>
                        <path d="M358.89 213.15V169.6H385.74V177.23H367.66V187H383.66V194.63H367.66V205.49H385.74V213.13L358.89 213.15Z" fill="white"></path>
                        <path d="M184.19 99.44C189.09 104.58 193.93 99.44 193.93 99.44C190.803 100.685 187.317 100.685 184.19 99.44Z" fill="white"></path>
                        <path d="M213.19 117.16L211.05 110.46C209.627 112.173 207.971 113.678 206.13 114.93C203.27 116.81 203.82 122.2 204.83 124.79C207.814 122.472 210.609 119.92 213.19 117.16Z" fill="white"></path>
                        <path d="M182.27 129.81C183.64 125.64 180.12 119.91 176.99 118.24C174.74 116.937 172.649 115.376 170.76 113.59L168.32 120C173.81 126.1 182.27 129.81 182.27 129.81Z" fill="white"></path>
                        <path d="M196.19 61.2C195.363 61.3429 194.6 61.7334 194 62.32C193.868 62.514 193.757 62.7219 193.67 62.94C193.59 63.12 193.52 63.3 193.46 63.47C193.34 63.83 193.24 64.18 193.15 64.47C192.986 65.1559 192.859 65.8503 192.77 66.55C192.591 67.9097 192.491 69.2787 192.47 70.65C192.47 72.01 192.47 73.36 192.56 74.71C192.65 76.06 192.75 77.4 192.88 78.71C192.93 77.36 192.99 76.02 193.09 74.71C193.09 74.2 193.19 73.71 193.24 73.2L193.41 73.09C194.41 72.35 193.53 68.97 194.72 68.39C195.01 68.25 196.43 69.5 197.59 69.95C197.951 70.1233 198.33 70.2574 198.72 70.35C199.152 70.4603 199.595 70.5207 200.04 70.53C200.492 70.5551 200.945 70.5044 201.38 70.38C200.563 70.098 199.732 69.8577 198.89 69.66L198.42 69.55C198.512 69.4791 198.577 69.3791 198.604 69.2664C198.631 69.1537 198.619 69.035 198.57 68.93C198.343 68.4575 198.027 68.0331 197.64 67.68C197.33 67.44 197.09 67.29 196.64 66.96C196.601 66.9181 196.558 66.8812 196.51 66.85L196.22 66.7C195.87 66.41 195.51 66.12 195.15 65.85C196.063 65.5529 197.005 65.3518 197.96 65.25H198.11C198.11 65.25 198.11 65.32 198.11 65.35C198.11 66.67 199.61 67.73 201.46 67.73C202.071 67.7684 202.681 67.6465 203.23 67.3764C203.779 67.1062 204.248 66.6973 204.59 66.19L205.11 65.99C205.9 66.2 206.71 66.45 207.49 66.73H207.43C207.05 67.06 206.66 67.38 206.25 67.73C205.439 68.3636 204.552 68.8943 203.61 69.31C204.697 69.3174 205.774 69.096 206.77 68.66C207.27 68.45 207.77 68.21 208.24 67.96C208.723 67.6943 209.191 67.4005 209.64 67.08L210.64 66.36L209.51 65.68C208.326 64.9699 207.06 64.4058 205.74 64C205.08 63.78 196.4 63.61 195.74 63.47C195.99 63.11 201.34 62.41 203.63 62.04C204.369 61.9301 205.121 61.9301 205.86 62.04C207.257 62.2127 208.643 62.4664 210.01 62.8C210.218 62.8452 210.434 62.8229 210.628 62.7366C210.823 62.6503 210.984 62.5044 211.09 62.32L212.09 60.45C212.133 60.3543 212.154 60.2504 212.153 60.1456C212.152 60.0407 212.128 59.9374 212.084 59.8425C212.039 59.7476 211.975 59.6634 211.895 59.5957C211.815 59.528 211.721 59.4783 211.62 59.45C203.3 57.45 199.92 59.58 196.62 61.18C196.45 61.14 196.32 61.16 196.19 61.2Z" fill="white"></path>
                        <path d="M179.18 70.4101C180.063 70.2208 180.879 69.7957 181.54 69.1801C180.64 69.3101 179.84 69.5301 179.01 69.7201C178.18 69.9101 177.38 70.1301 176.52 70.4401C176.955 70.5644 177.408 70.6151 177.86 70.59C178.305 70.5807 178.748 70.5203 179.18 70.4101Z" fill="white"></path>
                        <path d="M206.34 94.17C206.13 92.09 203.78 87.44 197.08 85.88C195.999 85.6239 194.89 85.5063 193.78 85.53L194.56 85.23L195.27 84.95H195.36L195.51 84.88L195.78 84.73C195.967 84.5955 196.129 84.4298 196.26 84.24C196.457 83.8874 196.524 83.477 196.45 83.08C196.431 82.9197 196.394 82.7621 196.34 82.61C196.295 82.4686 196.239 82.3314 196.17 82.2C196.031 81.9329 195.874 81.6756 195.7 81.43C195.542 81.135 195.332 80.8708 195.08 80.65C194.793 80.3243 194.482 80.0202 194.15 79.74C194.586 80.6335 194.921 81.5726 195.15 82.54C195.25 82.98 195.15 83.43 195.08 83.44L194.79 83.5L194.05 83.65C192.939 83.8274 191.868 84.2034 190.89 84.76C190.438 85.0848 190.023 85.4569 189.65 85.87C189.537 85.9782 189.409 86.0692 189.27 86.14H189.15H188.91H188.79C188.651 86.0692 188.523 85.9782 188.41 85.87C188.037 85.4569 187.622 85.0848 187.17 84.76C186.193 84.1873 185.118 83.8008 184 83.62L183.26 83.47C183.166 83.4375 183.069 83.4173 182.97 83.41C182.87 83.41 182.8 82.95 182.9 82.51L182.96 82.28C183.409 81.2794 184.02 80.3603 184.77 79.56C184.998 79.3054 185.144 78.9885 185.19 78.65C185.185 78.6965 185.185 78.7435 185.19 78.79C185.19 78.69 185.19 78.6 185.19 78.51C185.35 77.5786 185.431 76.6351 185.43 75.69L185.49 74.76C185.56 73.41 185.6 72.06 185.58 70.7C185.559 69.3287 185.459 67.9597 185.28 66.6C185.193 65.8993 185.062 65.2047 184.89 64.52C184.81 64.18 184.71 63.8467 184.59 63.52L184.53 63.39C184.475 63.2155 184.409 63.0451 184.33 62.88C184.251 62.7025 184.157 62.5319 184.05 62.37C182.832 61.4573 181.483 60.7326 180.05 60.22C177.54 58.87 173.33 57.85 166.63 59.4C166.506 59.4299 166.391 59.4874 166.293 59.5682C166.195 59.649 166.117 59.751 166.064 59.8665C166.011 59.982 165.985 60.1081 165.988 60.2351C165.99 60.3622 166.022 60.4869 166.08 60.6L166.8 61.95C166.956 62.2509 167.208 62.4911 167.516 62.6324C167.824 62.7738 168.17 62.8081 168.5 62.73C170.617 62.2447 172.771 61.9436 174.94 61.83C175.73 61.83 181.32 62.92 181.74 63.32C180.58 63.46 173.44 63.7 172.34 64.06C171.041 64.467 169.796 65.0277 168.63 65.73L167.5 66.41L168.5 67.13C168.949 67.4504 169.416 67.7443 169.9 68.01C170.38 68.26 170.9 68.5 171.37 68.71C172.366 69.1471 173.443 69.3686 174.53 69.36C173.587 68.9466 172.7 68.4157 171.89 67.78C171.48 67.48 171.09 67.16 170.71 66.78H170.65C171.43 66.5 172.24 66.25 173.03 66.04L173.55 66.24C173.892 66.7472 174.361 67.1562 174.91 67.4264C175.459 67.6965 176.069 67.8184 176.68 67.78C178.53 67.78 180.03 66.72 180.03 65.4C180.03 65.4 180.03 65.33 180.03 65.3H180.17C181.128 65.4007 182.074 65.6018 182.99 65.9C182.61 66.19 182.24 66.48 181.87 66.79L181.71 66.92L181.65 66.97C181.45 67.2 181.44 67.45 181.8 67.77C182.16 68.09 183.03 68.12 183.53 68.6L183.79 68.86C183.74 69.8594 183.74 70.8606 183.79 71.86C183.95 74.18 181.79 79.24 181.63 80.68C181.615 80.8964 181.615 81.1136 181.63 81.33C181.63 81.51 181.73 81.83 181.73 81.92C181.766 82.0517 181.816 82.1792 181.88 82.3C181.831 82.4041 181.787 82.511 181.75 82.62C181.691 82.7706 181.654 82.9289 181.64 83.09C181.568 83.4005 181.594 83.7256 181.714 84.0208C181.834 84.3161 182.042 84.5672 182.31 84.74L182.58 84.89L182.72 84.96H182.81L183.53 85.24L184.22 85.5C183.14 85.4812 182.061 85.5988 181.01 85.85C174.62 87.33 171.85 94.12 171.78 94.25C173.35 91.59 176.33 93.94 178.19 93.1C181.06 96.94 186.26 98.1 189.19 98.1C190.979 98.0997 192.739 97.6523 194.311 96.7982C195.883 95.9442 197.216 94.7107 198.19 93.21L199.1 92.88C201.31 93.63 204.48 91.17 206.29 94.28M189 88.3H189.07C189.076 88.3365 189.076 88.3736 189.07 88.41L189 88.3ZM182.71 92.3C184.28 92.36 186.54 92.72 187.96 91.75C188.122 91.6427 188.27 91.515 188.4 91.37C188.69 91.0632 188.906 90.6936 189.03 90.29C189.146 90.7038 189.359 91.0842 189.65 91.4C189.785 91.5429 189.936 91.6703 190.1 91.78C191.52 92.78 193.78 92.39 195.35 92.33C196.176 92.298 197.004 92.3483 197.82 92.48C197.795 92.7114 197.744 92.9394 197.67 93.16C189.9 98.48 182.07 94.97 179.01 93.16C178.92 92.78 179.28 92.6 179.53 92.59C180.581 92.343 181.663 92.252 182.74 92.32L182.71 92.3Z" fill="white"></path>
                        <path d="M244.67 125.34L216.55 116.66C215.76 114.37 212.65 105.4 212.1 104.21C211.55 103.02 209.48 102 207.77 101.29C210.24 98.89 211.98 96.75 212.27 95.62C212.56 94.49 213.42 90.06 214.48 84.57C214.589 84.5528 214.696 84.5261 214.8 84.49C216.8 83.87 218.58 80.38 219.02 79.49C220.72 77.16 228.08 65.24 224.02 62C223.673 61.7195 223.267 61.5201 222.833 61.4163C222.399 61.3125 221.947 61.307 221.51 61.4C220.369 61.7334 219.353 62.3982 218.59 63.31C218.85 61.9 219.03 60.98 219.08 60.73L219.57 58.46C221.557 58.9306 223.607 59.0725 225.64 58.88C228.142 58.6789 230.535 57.7702 232.54 56.26C234.532 54.6477 235.98 52.4625 236.69 50C237.063 48.8108 237.321 47.5885 237.46 46.35C237.46 45.73 237.54 45.12 237.58 44.51C237.62 43.9 237.58 43.51 237.58 43.09C238.343 42.4012 239.015 41.6183 239.58 40.76C240.147 39.9158 240.623 39.0141 241 38.07C241.355 37.1435 241.623 36.1861 241.8 35.21C242.452 31.3298 242.041 27.3453 240.61 23.68C239.898 21.8691 238.971 20.1501 237.85 18.56L236.95 17.42L236.5 16.85L236.01 16.32L235.01 15.26C234.66 14.93 234.29 14.61 233.93 14.26C230.996 11.7275 227.504 9.92505 223.74 9.00002C223.278 8.86133 222.807 8.7577 222.33 8.69002L220.92 8.45002C220.44 8.40002 219.92 8.35002 219.48 8.32002C219.04 8.29002 218.48 8.32002 218.04 8.32002C216.43 8.36287 214.831 8.58422 213.27 8.98002C209.437 5.78498 205.023 3.36055 200.27 1.84002C198.803 1.3663 197.311 0.975683 195.8 0.67002C194.278 0.396831 192.742 0.203235 191.2 0.0900196C189.655 -0.00502282 188.105 -0.00502282 186.56 0.0900196C185.031 0.186871 183.509 0.367139 182 0.63002C180.476 0.925069 178.971 1.30567 177.49 1.77002C176.026 2.27793 174.593 2.87231 173.2 3.55002C172.496 3.87357 171.812 4.23751 171.15 4.64002C170.49 5.05002 169.83 5.43002 169.15 5.86002C167.844 6.72535 166.637 7.73137 165.55 8.86002C164.438 9.9847 163.51 11.2771 162.8 12.69C162.434 13.4488 162.146 14.2429 161.94 15.06C159.418 14.9498 156.895 15.2332 154.46 15.9C150.981 16.8707 147.729 18.5188 144.889 20.7497C142.049 22.9805 139.677 25.7503 137.91 28.9C136.135 32.0947 135.09 35.6431 134.85 39.29C134.81 39.7458 134.81 40.2042 134.85 40.66L134.91 42.04C134.931 42.4971 134.988 42.9518 135.08 43.4C135.15 43.86 135.21 44.31 135.3 44.76C135.711 46.5501 136.364 48.2759 137.24 49.89C138.104 51.487 139.147 52.98 140.35 54.34C141.57 55.6661 142.909 56.8778 144.35 57.96C147.102 60.038 150.132 61.7209 153.35 62.96C151.28 67.16 157.57 77.3 159.12 79.44C159.57 80.35 161.38 83.84 163.35 84.44C163.454 84.4761 163.561 84.5028 163.67 84.52C164.76 90.01 165.67 94.52 165.93 95.57C166.19 96.62 167.93 98.85 170.43 101.25C168.72 101.96 166.63 103.03 166.12 104.16C165.61 105.29 162.46 114.32 161.67 116.61L133.52 125.3L131.44 125.94C131.544 126.651 131.687 127.355 131.87 128.05C135.87 143.18 160.04 154.83 189.14 154.83C218.24 154.83 242.39 143.17 246.41 128.05C246.597 127.353 246.741 126.645 246.84 125.93L244.67 125.34ZM209.36 104.46C209.187 106.183 208.524 107.821 207.45 109.18C209.37 105.4 207.2 103.73 205.96 102.99L206.04 102.91C207.186 103.338 208.296 103.856 209.36 104.46ZM205.16 111.26C204.61 111.62 203.98 112.03 203.28 112.47C197.28 116.32 191.41 120.33 189.06 124.08C186.71 120.33 180.83 116.32 174.83 112.47L173 111.26L172.66 111.01L173.47 107.01C175.99 109.9 183.38 114.73 189.02 114.73C195.92 114.73 205.57 105.96 205.57 105.96L205.99 105.6L206.93 109.86C206.394 110.391 205.8 110.86 205.16 111.26ZM170.04 108.26C169.325 107.102 168.887 105.795 168.76 104.44C169.799 103.872 170.882 103.39 172 103C171.52 103.216 171.088 103.525 170.727 103.908C170.367 104.292 170.086 104.743 169.9 105.235C169.714 105.727 169.627 106.251 169.645 106.777C169.662 107.303 169.782 107.821 170 108.3L170.04 108.26ZM189.04 49.2C184.27 49.2 170.21 49.2 160.48 58.43L156.32 43.67C157.996 43.285 159.605 42.6477 161.09 41.78C162.945 40.6961 164.523 39.1969 165.7 37.4L166 37C166.963 37.5024 167.967 37.9239 169 38.26C171.369 39.0606 173.888 39.317 176.37 39.01C179.506 38.6147 182.396 37.1108 184.52 34.77C186.71 34.55 192.64 34.04 196.01 34.6V34.54C196.887 35.2604 197.839 35.884 198.85 36.4C199.27 36.62 199.69 36.83 200.12 37.03L201.43 37.52C201.87 37.68 202.31 37.85 202.75 37.99L204.08 38.37L205.43 38.7L206.81 38.94C207.729 39.1069 208.657 39.2138 209.59 39.26C211.487 39.3636 213.387 39.1438 215.21 38.61L215.51 38.51C216.101 40.1138 217.084 41.544 218.37 42.67C219.003 43.2377 219.701 43.7279 220.45 44.13L217.35 58.33C207.66 49.22 193.78 49.24 189.06 49.24L189.04 49.2ZM221.9 63.44C222.029 63.4071 222.163 63.4062 222.292 63.4376C222.421 63.469 222.541 63.5317 222.64 63.62C224.31 64.94 220.88 73.32 217.22 78.23L217.11 78.41C216.549 79.5956 215.844 80.7074 215.01 81.72C215.25 80.44 215.51 79.13 215.76 77.82C216.295 77.3637 216.76 76.8313 217.14 76.24C217.532 75.6443 217.826 74.9892 218.01 74.3C218.208 73.6096 218.322 72.8978 218.35 72.18C218.364 71.5809 218.324 70.9818 218.23 70.39C218.599 69.6745 219.049 69.0035 219.57 68.39C219.829 68.0866 220.133 67.8237 220.47 67.61C220.633 67.4933 220.82 67.4154 221.018 67.3825C221.215 67.3496 221.418 67.3624 221.61 67.42C221.42 67.3166 221.208 67.2575 220.992 67.2471C220.775 67.2367 220.559 67.2753 220.36 67.36C219.952 67.5159 219.569 67.7279 219.22 67.99C219.06 68.11 218.92 68.24 218.77 68.37C218.796 68.1675 218.796 67.9625 218.77 67.76C218.72 67.34 218.69 66.91 218.67 66.49C219.65 65.17 220.91 63.7 221.92 63.48L221.9 63.44ZM234.56 49.3C234.273 50.3508 233.849 51.3593 233.3 52.3C232.777 53.2388 232.101 54.0837 231.3 54.8C229.619 56.2619 227.591 57.2674 225.41 57.72C224.286 57.9779 223.142 58.1352 221.99 58.19C221.19 58.19 220.38 58.19 219.57 58.19L222.47 45C222.83 45.12 223.18 45.25 223.54 45.34C224.477 45.5922 225.434 45.7662 226.4 45.86C226.89 45.91 227.4 45.86 227.85 45.93C228.333 45.96 228.817 45.96 229.3 45.93C230.272 45.8958 231.241 45.799 232.2 45.64C233.192 45.4289 234.153 45.0928 235.06 44.64L235.35 44.49C235.279 46.1308 235.021 47.7581 234.58 49.34L234.56 49.3ZM141.64 53.13C140.584 51.8599 139.675 50.4744 138.93 49C138.179 47.5277 137.621 45.9648 137.27 44.35C137.2 43.95 137.15 43.54 137.09 43.13C137.012 42.7245 136.968 42.313 136.96 41.9C136.9 41.0778 136.9 40.2523 136.96 39.43C137.246 36.0972 138.201 32.8563 139.768 29.901C141.335 26.9456 143.482 24.3367 146.08 22.23C150.477 18.5073 155.968 16.32 161.72 16C161.66 16.4 161.61 16.81 161.59 17.21C161.493 20.3191 162.507 23.3611 164.45 25.79C162.904 23.2444 162.237 20.2617 162.55 17.3C163.02 14.464 164.467 11.8817 166.64 10C167.712 9.0272 168.87 8.15362 170.1 7.39002C170.73 7.02002 171.38 6.68002 172.02 6.32002C172.663 5.94124 173.331 5.60712 174.02 5.32002C178.034 3.5404 182.329 2.47768 186.71 2.18002C189.636 2.05609 192.566 2.22022 195.46 2.67002C196.89 3.00002 198.35 3.22002 199.75 3.67002C204.106 4.91018 208.239 6.82687 212 9.35002C208.565 10.4791 205.395 12.2922 202.68 14.68C205.695 12.5451 209.088 11.0042 212.68 10.14C216.216 9.32537 219.905 9.4634 223.37 10.54C226.815 11.5606 229.985 13.3487 232.64 15.77L233.64 16.68L234.53 17.68L234.98 18.17L235.39 18.7L236.19 19.76C237.663 21.971 238.743 24.4205 239.38 27C239.51 27.43 239.54 27.87 239.63 28.3C239.723 28.7318 239.787 29.1695 239.82 29.61C239.91 30.4869 239.94 31.369 239.91 32.25C239.91 32.69 239.91 33.13 239.85 33.57C239.79 34.01 239.77 34.44 239.67 34.86C239.53 35.7115 239.312 36.5483 239.02 37.36C238.719 38.1602 238.337 38.9275 237.88 39.65C237.426 40.361 236.881 41.0098 236.26 41.58C235.666 42.1566 234.993 42.6449 234.26 43.03C233.502 43.4205 232.7 43.7161 231.87 43.91C231.001 44.0761 230.122 44.1929 229.24 44.26C228.798 44.3099 228.354 44.33 227.91 44.32C227.46 44.32 227.01 44.32 226.57 44.32C225.686 44.2644 224.807 44.1475 223.94 43.97C222.208 43.6236 220.577 42.8885 219.17 41.82C218.468 41.2832 217.845 40.6503 217.32 39.94C216.924 39.4236 216.588 38.8632 216.32 38.27C217.897 37.6379 219.309 36.6546 220.449 35.3949C221.59 34.1352 222.428 32.6321 222.9 31C221.987 32.5158 220.8 33.8485 219.4 34.93C218.031 35.9381 216.463 36.6437 214.8 37C213.135 37.3355 211.429 37.4164 209.74 37.24C208.881 37.1676 208.027 37.0507 207.18 36.89L205.91 36.64C205.49 36.54 205.07 36.41 204.65 36.3L203.38 36C202.97 35.87 202.57 35.72 202.16 35.58L200.94 35.15L199.78 34.62C199.002 34.281 198.255 33.8727 197.55 33.4C197.2 33.1727 196.866 32.9221 196.55 32.65L196.37 32.52L196.06 32.28C195.9 32.14 195.77 31.98 195.63 31.84C194.424 30.6111 193.568 29.0834 193.148 27.4139C192.728 25.7444 192.759 23.9931 193.24 22.34C192.356 23.9872 191.977 25.8585 192.15 27.72C192.304 29.365 192.895 30.9391 193.86 32.28C191.334 32.2069 188.807 32.2804 186.29 32.5C186.751 31.7542 187.143 30.9676 187.46 30.15C185.927 31.8812 184.179 33.4101 182.26 34.7C181.309 35.278 180.304 35.7636 179.26 36.15C178.229 36.5156 177.164 36.7737 176.08 36.92C173.858 37.189 171.607 37.108 169.41 36.68C168.41 36.5 167.49 36.27 166.53 36.01C167.183 34.6179 167.566 33.1148 167.66 31.58C166.766 33.3343 165.661 34.973 164.37 36.46C163.103 37.8565 161.629 39.0504 160 40C158.346 40.9696 156.593 41.7576 154.77 42.35C152.878 42.9698 150.952 43.4774 149 43.87C150.902 44.2716 152.861 44.319 154.78 44.01L154.22 44.16L158.9 60.78C158.95 61.03 159.12 61.88 159.36 63.16C158.62 62.3109 157.652 61.693 156.57 61.38C156.132 61.2854 155.678 61.2902 155.242 61.394C154.806 61.4978 154.398 61.6981 154.05 61.98L153.84 62.18C150.752 60.7429 147.862 58.9146 145.24 56.74C143.954 55.6463 142.757 54.4526 141.66 53.17L141.64 53.13ZM160.91 78.41L160.8 78.23C157.14 73.32 153.72 64.94 155.38 63.62C155.48 63.5338 155.6 63.4726 155.728 63.4413C155.857 63.4101 155.991 63.4096 156.12 63.44C157.03 63.63 158.19 64.88 159.12 66.23C159.187 66.8968 159.163 67.5695 159.05 68.23L158.78 68C158.43 67.7399 158.046 67.528 157.64 67.37C157.44 67.2853 157.225 67.2467 157.008 67.2571C156.792 67.2675 156.58 67.3266 156.39 67.43C156.582 67.3732 156.784 67.3607 156.982 67.3936C157.179 67.4266 157.367 67.504 157.53 67.62C157.863 67.8358 158.162 68.0985 158.42 68.4C158.945 69.0111 159.394 69.6825 159.76 70.4C159.664 70.9915 159.627 71.5912 159.65 72.19C159.674 72.9074 159.785 73.6192 159.98 74.31C160.172 74.9981 160.469 75.6525 160.86 76.25C161.233 76.8292 161.687 77.3513 162.21 77.8C162.46 79.1 162.72 80.41 162.98 81.69C162.167 80.6974 161.479 79.6094 160.93 78.45L160.91 78.41ZM167.91 95.06C167.72 94.27 167.09 91.22 166.29 87.22C166.54 84.89 167.4 84.01 167.95 83.68C167.497 82.1572 166.95 80.6643 166.31 79.21C165.59 78.21 164.85 77.13 164.09 76.09L163 70.34C163.558 66.8516 163.892 63.3311 164 59.8C173.529 53.9601 184.597 51.1269 195.76 51.67C201.2 52.22 208.38 53.81 214.28 58.48C214.28 61.25 214.28 68.81 214.55 72.91C214.34 74.02 214.12 75.15 213.9 76.29C213.18 77.29 212.48 78.29 211.8 79.29C211.34 80.36 211.55 84.64 211.91 86.67C211.07 90.97 210.4 94.31 210.19 95.14C209.6 97.53 195.13 110.77 189.07 110.77C183.01 110.77 168.53 97.49 167.93 95.1L167.91 95.06ZM133.81 127.43L163.24 118.35L163.42 117.83C164.23 115.47 166.07 110.19 167.16 107.18C167.991 109.587 169.612 111.642 171.76 113.01C172.31 113.38 172.95 113.79 173.66 114.24C178.3 117.24 187.97 123.42 187.97 127.24V133.15C177.335 132.969 166.804 131.019 156.81 127.38L155.16 126.72L155.86 133.19L151.56 132.4C151.56 132.4 152.84 141.21 153.96 146.17C143.2 141.65 135.74 135 133.83 127.47L133.81 127.43ZM177.3 152.04C171.526 150.043 165.927 147.571 160.56 144.65L160.97 148.72L159.65 148.3L157.65 129.99C163.551 132.029 169.632 133.509 175.81 134.41C185.81 135.88 200.2 136.27 212.57 130.12L213.29 149.78C212.51 149.98 211.71 150.17 210.9 150.35L209.04 134.55C209.04 134.55 195.57 137.5 190.69 137.8C194.99 138.604 199.329 139.181 203.69 139.53C198.85 140.969 193.861 141.848 188.82 142.15L189.22 150.73C194.255 151.394 199.333 151.674 204.41 151.57C195.425 152.898 186.306 153.056 177.28 152.04H177.3ZM215.44 149.21L214.6 126.61L213.08 127.45C205.9 131.39 197.84 132.92 190.08 133.13V127.25C190.08 123.42 199.75 117.25 204.39 114.25L206.29 113.02C208.448 111.647 210.074 109.581 210.9 107.16C211.99 110.16 213.84 115.47 214.66 117.84L214.83 118.36L244.24 127.44C241.9 136.89 230.85 144.91 215.46 149.25L215.44 149.21Z" fill="white"></path>
                        <path d="M198.58 124.43C197.937 124.369 197.291 124.505 196.727 124.818C196.163 125.131 195.706 125.608 195.418 126.185C195.129 126.763 195.022 127.414 195.11 128.054C195.198 128.693 195.478 129.291 195.912 129.768C196.347 130.246 196.915 130.581 197.543 130.73C198.171 130.879 198.83 130.834 199.432 130.601C200.034 130.369 200.552 129.96 200.918 129.428C201.283 128.896 201.479 128.266 201.48 127.62C201.513 126.814 201.227 126.027 200.684 125.43C200.142 124.833 199.386 124.474 198.58 124.43ZM196.53 128.43C196.437 128.16 196.389 127.876 196.39 127.59C196.354 126.972 196.565 126.366 196.975 125.903C197.386 125.44 197.963 125.159 198.58 125.12C198.693 125.11 198.807 125.11 198.92 125.12C198.348 125.212 197.829 125.51 197.461 125.957C197.093 126.405 196.901 126.971 196.92 127.55C196.922 127.724 196.938 127.898 196.97 128.07L196.53 128.43ZM200.53 128.72C200.387 128.953 200.187 129.147 199.949 129.283C199.712 129.419 199.444 129.494 199.17 129.5C198.697 129.452 198.259 129.229 197.941 128.877C197.622 128.524 197.446 128.065 197.446 127.59C197.446 127.115 197.622 126.656 197.941 126.303C198.259 125.951 198.697 125.728 199.17 125.68C199.444 125.686 199.712 125.761 199.949 125.897C200.187 126.033 200.387 126.227 200.53 126.46C200.695 126.814 200.78 127.2 200.78 127.59C200.78 127.98 200.695 128.366 200.53 128.72Z" fill="white"></path>
                        <path d="M107.82 154.88C108.2 153.45 99.3001 151.31 97.9601 148.48C96.2001 144.82 102.07 128.09 103.96 123.55C105.15 120.69 109.47 119.76 109.47 119.76C115.65 118.31 120.47 112.2 122.47 104.51C125.19 94.3401 125.08 81.2401 125.08 81.2401L103.4 75.4301C103.4 75.4301 96.7601 86.7301 94.0301 96.8901C91.9701 104.58 93.0301 112.27 97.6901 116.62C97.6901 116.62 101.15 119.62 100.75 122.71C100.13 127.6 95.5001 144.12 92.7501 147.2C90.9601 149.2 81.9601 146.46 81.5801 147.88L107.82 154.88ZM103.82 117.69H103.74H103.67L103.43 117.61C95.2601 115.15 92.8701 105.66 95.4301 96.1401C95.9101 94.3601 100.99 81.7901 103.82 76.9601L124.16 82.4801C124.22 87.0501 122.22 101.41 121.74 103.18C119.19 112.71 112.38 119.73 104.07 117.78L103.82 117.69Z" fill="white"></path>
                        <path d="M97.3099 98C95.4799 105.72 97.5599 113.24 104.19 115.24C104.255 115.265 104.322 115.285 104.39 115.3H104.46H104.52H104.72C111.46 116.88 119.21 109.3 120.5 97.2C120.5 97.2 116.17 102.99 107.6 99.41C99.5999 96.12 97.3099 98 97.3099 98Z" fill="#F2A736"></path>
                        <path d="M120.25 94.7901C119.64 98.5701 112.42 98.7901 112.42 98.7901C112.42 98.7901 115.61 97.0401 116.09 94.1401C116.123 93.8564 116.212 93.5823 116.354 93.3343C116.496 93.0864 116.686 92.8697 116.914 92.6976C117.142 92.5254 117.402 92.4012 117.679 92.3327C117.956 92.2641 118.245 92.2524 118.527 92.2985C118.808 92.3446 119.078 92.4474 119.319 92.6007C119.56 92.7539 119.767 92.9545 119.928 93.1903C120.089 93.426 120.201 93.692 120.256 93.9722C120.311 94.2523 120.309 94.5408 120.25 94.8201V94.7901Z" fill="#F2A736"></path>
                        <path d="M119.44 88.39C121.19 89.01 120.66 92.45 120.66 92.45C120.502 91.9859 120.254 91.5578 119.93 91.1904C119.605 90.823 119.211 90.5238 118.77 90.31C118.642 90.2675 118.525 90.1997 118.424 90.1107C118.323 90.0216 118.241 89.9132 118.184 89.7918C118.126 89.6705 118.093 89.5387 118.087 89.4044C118.081 89.27 118.103 89.1359 118.15 89.01C118.193 88.8837 118.26 88.7671 118.348 88.6672C118.437 88.5673 118.544 88.4861 118.664 88.4283C118.785 88.3705 118.915 88.3374 119.048 88.3308C119.181 88.3242 119.315 88.3444 119.44 88.39Z" fill="#F2A736"></path>
                        <path d="M114.15 89.73C115.46 88.59 118.04 90.61 118.04 90.61C117.586 90.5259 117.121 90.5321 116.669 90.6282C116.218 90.7243 115.79 90.9084 115.41 91.17C115.215 91.2581 114.996 91.2811 114.786 91.2355C114.577 91.1899 114.388 91.0782 114.246 90.9169C114.105 90.7555 114.02 90.5531 114.002 90.3394C113.985 90.1257 114.037 89.912 114.15 89.73Z" fill="#F2A736"></path>
                        <path d="M304.26 147.86C303.88 146.44 294.88 149.17 293.1 147.18C290.34 144.1 285.71 127.58 285.1 122.69C284.71 119.62 288.17 116.6 288.17 116.6C292.79 112.25 293.88 104.6 291.82 96.87C289.1 86.71 282.46 75.41 282.46 75.41L260.77 81.22C260.77 81.22 260.67 94.32 263.39 104.49C265.45 112.18 270.23 118.29 276.39 119.74C276.39 119.74 280.7 120.67 281.9 123.53C283.8 128.07 289.66 144.8 287.9 148.46C286.55 151.29 277.65 153.46 278.04 154.86L304.26 147.86ZM281.8 117.74C273.49 119.74 266.68 112.67 264.13 103.14C263.66 101.37 261.66 87.01 261.71 82.44L282.05 76.92C284.89 81.75 289.97 94.32 290.44 96.1C292.99 105.62 290.61 115.1 282.44 117.57L282.21 117.65H282.13H282.06L281.8 117.74Z" fill="white"></path>
                        <path d="M288.54 98C290.36 105.72 288.28 113.24 281.65 115.24C281.585 115.266 281.518 115.286 281.45 115.3H281.39H281.33H281.12C274.38 116.88 266.63 109.3 265.34 97.2C265.34 97.2 269.67 102.99 278.25 99.41C286.25 96.12 288.54 98 288.54 98Z" fill="#F2A736"></path>
                        <path d="M265.59 94.79C266.21 98.57 273.43 98.79 273.43 98.79C273.43 98.79 270.23 97.04 269.76 94.14C269.715 93.8662 269.617 93.6039 269.471 93.368C269.325 93.1321 269.134 92.9273 268.909 92.7653C268.684 92.6033 268.429 92.4872 268.159 92.4236C267.889 92.3601 267.609 92.3504 267.335 92.395C267.061 92.4397 266.799 92.5378 266.563 92.6839C266.327 92.8299 266.122 93.021 265.96 93.2461C265.798 93.4713 265.682 93.7262 265.619 93.9963C265.555 94.2663 265.545 94.5462 265.59 94.82V94.79Z" fill="#F2A736"></path>
                        <path d="M266.4 88.39C264.66 89.01 265.19 92.45 265.19 92.45C265.346 91.9849 265.593 91.5558 265.918 91.1881C266.242 90.8205 266.638 90.5219 267.08 90.31C267.335 90.2199 267.543 90.0322 267.659 89.7884C267.775 89.5446 267.79 89.2646 267.7 89.01C267.61 88.7554 267.422 88.5471 267.178 88.4308C266.935 88.3145 266.655 88.2999 266.4 88.39Z" fill="#F2A736"></path>
                        <path d="M271.7 89.73C270.39 88.59 267.81 90.61 267.81 90.61C268.264 90.5259 268.73 90.5321 269.181 90.6282C269.632 90.7243 270.06 90.9084 270.44 91.17C270.635 91.3269 270.883 91.4046 271.133 91.388C271.383 91.3713 271.617 91.2614 271.79 91.08C271.947 90.8846 272.025 90.6375 272.008 90.3875C271.991 90.1375 271.881 89.9028 271.7 89.73Z" fill="#F2A736"></path>
                        <path d="M68.1199 99.12C65.6399 99.12 63.2432 100.015 61.3699 101.64C61.2697 101.729 61.1921 101.841 61.1434 101.966C61.0947 102.091 61.0764 102.226 61.0899 102.36C60.6191 102.989 60.3663 103.754 60.3699 104.54C60.3347 104.988 60.3927 105.439 60.54 105.864C60.6874 106.288 60.921 106.678 61.2263 107.008C61.5315 107.338 61.9017 107.602 62.3136 107.782C62.7256 107.962 63.1703 108.055 63.6199 108.055C64.0695 108.055 64.5143 107.962 64.9262 107.782C65.3382 107.602 65.7084 107.338 66.0136 107.008C66.3188 106.678 66.5525 106.288 66.6998 105.864C66.8472 105.439 66.9051 104.988 66.8699 104.54C66.8777 103.891 66.7075 103.253 66.3778 102.694C66.0481 102.135 65.5716 101.677 64.9999 101.37C65.9923 100.989 67.0469 100.796 68.1099 100.8C73.1099 100.8 77.3299 105.23 77.4799 110.66C77.5221 112.239 77.2219 113.808 76.5999 115.26C75.5999 115.8 70.5999 118.03 64.0399 112.92C63.8897 112.808 63.7021 112.758 63.5159 112.78C63.3298 112.802 63.1593 112.895 63.0399 113.04C63.0399 113.04 58.4299 118.35 43.5999 121.15C25.6699 121.15 9.23993 132.59 1.59993 149.95V142C1.59993 137.58 4.91993 134 9.00993 134C9.23272 134 9.44637 133.911 9.6039 133.754C9.76143 133.596 9.84993 133.383 9.84993 133.16C9.84993 132.937 9.76143 132.724 9.6039 132.566C9.44637 132.408 9.23272 132.32 9.00993 132.32C4.06993 132.32 0.0099346 136.67 0.0099346 142.02V154.02C0.00488078 154.211 0.0657752 154.398 0.182355 154.549C0.298935 154.7 0.464072 154.806 0.649935 154.85H0.779935C0.941029 154.845 1.09674 154.791 1.22567 154.694C1.3546 154.598 1.45041 154.463 1.49993 154.31C8.11993 135.46 25.0599 122.79 43.6499 122.79H68.3899C69.9988 122.753 71.5771 122.342 72.9999 121.59C75.1967 120.57 76.9209 118.749 77.8199 116.5C78.6817 114.657 79.0864 112.633 78.9999 110.6C78.8599 104.28 73.9999 99.13 68.1199 99.12ZM74.8399 118.05C74.2271 118.736 73.5201 119.332 72.7399 119.82C71.322 120.566 69.7671 121.015 68.1699 121.14H50.7799C58.7799 118.78 62.4299 115.86 63.6699 114.68C68.6699 118.34 72.7999 118.19 75.2399 117.56C75.1099 117.73 74.9999 117.89 74.8399 118.05Z" fill="white"></path>
                        <path d="M307.05 110.6C306.984 112.64 307.416 114.665 308.31 116.5C309.209 118.749 310.933 120.57 313.13 121.59C314.539 122.344 316.103 122.762 317.7 122.81H342.43C361.03 122.81 377.97 135.48 384.59 154.33C384.638 154.484 384.734 154.619 384.863 154.716C384.992 154.813 385.149 154.867 385.31 154.87H385.45C385.636 154.826 385.801 154.72 385.918 154.569C386.034 154.418 386.095 154.231 386.09 154.04V142C386.09 136.65 382.09 132.3 377.09 132.3C376.867 132.3 376.654 132.388 376.496 132.546C376.339 132.704 376.25 132.917 376.25 133.14C376.25 133.363 376.339 133.576 376.496 133.734C376.654 133.891 376.867 133.98 377.09 133.98C381.17 133.98 384.5 137.58 384.5 141.98V150C376.83 132.64 360.4 121.23 342.5 121.2C327.67 118.4 323.1 113.14 323.05 113.09C322.931 112.945 322.76 112.852 322.574 112.83C322.388 112.808 322.2 112.858 322.05 112.97C315.5 118.08 310.49 115.85 309.5 115.31C308.869 113.861 308.568 112.29 308.62 110.71C308.77 105.28 312.97 100.86 317.99 100.85C319.053 100.847 320.107 101.04 321.1 101.42C320.529 101.727 320.053 102.185 319.725 102.744C319.397 103.303 319.229 103.942 319.24 104.59C319.205 105.038 319.263 105.489 319.41 105.914C319.558 106.338 319.791 106.728 320.096 107.058C320.402 107.388 320.772 107.652 321.184 107.832C321.596 108.012 322.04 108.105 322.49 108.105C322.94 108.105 323.384 108.012 323.796 107.832C324.208 107.652 324.579 107.388 324.884 107.058C325.189 106.728 325.423 106.338 325.57 105.914C325.717 105.489 325.775 105.038 325.74 104.59C325.739 103.805 325.486 103.041 325.02 102.41C325.034 102.276 325.015 102.141 324.967 102.016C324.918 101.891 324.84 101.779 324.74 101.69C322.876 100.05 320.483 99.138 318 99.12C312.12 99.13 307.22 104.28 307.05 110.6ZM310.84 117.6C313.29 118.23 317.44 118.38 322.41 114.72C323.65 115.9 327.34 118.82 335.3 121.18H317.91C316.316 121.053 314.765 120.604 313.35 119.86C312.57 119.372 311.863 118.776 311.25 118.09C311.104 117.921 310.967 117.744 310.84 117.56V117.6Z" fill="white"></path>
                        <path d="M339.3 237.89H377.82C378.947 237.89 380.049 238.224 380.987 238.851C381.924 239.477 382.655 240.367 383.086 241.409C383.518 242.45 383.63 243.596 383.411 244.702C383.191 245.808 382.648 246.823 381.851 247.621C381.053 248.418 380.038 248.961 378.932 249.18C377.826 249.4 376.68 249.288 375.639 248.856C374.597 248.425 373.707 247.694 373.081 246.757C372.454 245.819 372.12 244.717 372.12 243.59H369.55C369.55 245.226 370.035 246.825 370.944 248.185C371.853 249.545 373.144 250.605 374.655 251.23C376.166 251.856 377.829 252.02 379.433 251.701C381.038 251.382 382.511 250.594 383.668 249.438C384.824 248.281 385.612 246.808 385.931 245.203C386.25 243.599 386.086 241.936 385.461 240.425C384.835 238.914 383.775 237.622 382.415 236.714C381.055 235.805 379.456 235.32 377.82 235.32H339.31" fill="white"></path>
                        <path d="M46.78 237.89H8.26999C7.14264 237.89 6.0406 238.224 5.10324 238.851C4.16588 239.477 3.4353 240.367 3.00388 241.409C2.57246 242.45 2.45958 243.596 2.67952 244.702C2.89945 245.808 3.44233 246.823 4.23949 247.621C5.03664 248.418 6.05229 248.961 7.15798 249.18C8.26367 249.4 9.40975 249.288 10.4513 248.856C11.4928 248.425 12.383 247.694 13.0094 246.757C13.6357 245.819 13.97 244.717 13.97 243.59H16.53C16.53 245.226 16.0447 246.825 15.1356 248.186C14.2264 249.546 12.9342 250.606 11.4225 251.231C9.91074 251.857 8.24741 252.02 6.64292 251.7C5.03842 251.38 3.56487 250.592 2.40868 249.434C1.2525 248.277 0.465652 246.802 0.147682 245.197C-0.170287 243.592 -0.00508636 241.929 0.622384 240.418C1.24985 238.907 2.3114 237.616 3.67271 236.709C5.03402 235.801 6.63392 235.318 8.26999 235.32H46.78" fill="white"></path>
                        <path d="M61.1101 243.12L64.2501 230.71H66.8401L70.0001 243.12H68.0001L67.2701 240.05H63.8701L63.1501 243.12H61.1101ZM64.2001 238.42H66.8701L66.0901 235C65.9501 234.42 65.8401 233.89 65.7401 233.42C65.6401 232.95 65.5801 232.64 65.5501 232.42C65.5501 232.6 65.4501 232.92 65.3601 233.42C65.2701 233.92 65.1501 234.42 65.0201 234.99L64.2001 238.42Z" fill="white"></path>
                        <path d="M72.45 243.12V230.71H74.45V241.29H79.78V243.12H72.45Z" fill="white"></path>
                        <path d="M82.6499 243.12V230.71H84.6499V241.29H89.9999V243.12H82.6499Z" fill="white"></path>
                        <path d="M93.4299 238.42V236.62H98.8699V238.42H93.4299Z" fill="white"></path>
                        <path d="M102.84 243.12V241.37H105.34V232.47H102.84V230.71H109.84V232.47H107.34V241.37H109.84V243.12H102.84Z" fill="white"></path>
                        <path d="M112.75 243.12V230.71H115.2L118.74 240.88C118.74 240.54 118.68 240.14 118.64 239.67C118.6 239.2 118.57 238.67 118.55 238.2C118.53 237.73 118.55 237.25 118.55 236.85V230.71H120.37V243.12H117.92L114.37 233C114.37 233.31 114.43 233.68 114.46 234.14C114.49 234.6 114.52 235.06 114.54 235.54C114.56 236.02 114.54 236.47 114.54 236.88V243.16L112.75 243.12Z" fill="white"></path>
                        <path d="M124 238.42V236.62H129.45V238.42H124Z" fill="white"></path>
                        <path d="M136.94 243.29C136.248 243.306 135.562 243.155 134.94 242.85C134.384 242.576 133.926 242.135 133.63 241.59C133.312 240.997 133.153 240.332 133.17 239.66V234.18C133.151 233.507 133.31 232.842 133.63 232.25C133.926 231.705 134.384 231.264 134.94 230.99C135.577 230.694 136.272 230.54 136.975 230.54C137.678 230.54 138.372 230.694 139.01 230.99C139.566 231.264 140.024 231.705 140.32 232.25C140.64 232.842 140.799 233.507 140.78 234.18V239.66C140.796 240.332 140.638 240.997 140.32 241.59C140.024 242.135 139.566 242.576 139.01 242.85C138.368 243.168 137.656 243.319 136.94 243.29ZM136.94 241.54C137.19 241.559 137.441 241.525 137.677 241.441C137.913 241.356 138.129 241.223 138.31 241.05C138.637 240.663 138.805 240.166 138.78 239.66V234.18C138.814 233.67 138.645 233.166 138.31 232.78C138.126 232.611 137.91 232.481 137.675 232.398C137.439 232.316 137.189 232.282 136.94 232.3C136.692 232.282 136.444 232.315 136.21 232.398C135.976 232.48 135.761 232.611 135.58 232.78C135.412 232.97 135.284 233.192 135.203 233.432C135.123 233.673 135.091 233.927 135.11 234.18V239.66C135.085 240.166 135.253 240.663 135.58 241.05C135.76 241.221 135.975 241.353 136.209 241.438C136.443 241.522 136.692 241.557 136.94 241.54Z" fill="white"></path>
                        <path d="M143.35 243.12V230.71H145.8L149.34 240.88C149.34 240.54 149.28 240.14 149.24 239.67C149.2 239.2 149.17 238.67 149.15 238.2C149.13 237.73 149.15 237.25 149.15 236.85V230.71H150.96V243.12H148.52L145 233C145 233.31 145.06 233.68 145.09 234.14C145.12 234.6 145.15 235.06 145.17 235.54C145.19 236.02 145.17 236.47 145.17 236.88V243.16L143.35 243.12Z" fill="white"></path>
                        <path d="M153.79 243.12V230.71H161.15V232.45H155.75V235.83H160.56V237.57H155.75V241.39H161.15V243.12H153.79Z" fill="white"></path>
                        <path d="M173.68 243.12V230.71H176.08L177.3 234.71C177.43 235.09 177.53 235.45 177.61 235.79C177.69 236.13 177.74 236.37 177.78 236.53C177.78 236.37 177.87 236.12 177.95 235.79C178.03 235.46 178.12 235.09 178.24 234.71L179.41 230.71H181.81V243.12H179.94V239.12C179.94 238.57 179.94 237.96 179.99 237.3C180.04 236.64 180.06 235.98 180.12 235.3C180.18 234.62 180.22 234 180.29 233.39C180.36 232.78 180.4 232.26 180.45 231.82L178.7 237.75H176.83L175 231.82C175.06 232.25 175.11 232.76 175.17 233.34C175.23 233.92 175.28 234.55 175.33 235.21C175.38 235.87 175.43 236.54 175.46 237.21C175.49 237.88 175.51 238.51 175.51 239.11V243.11L173.68 243.12Z" fill="white"></path>
                        <path d="M183.51 243.12L186.65 230.71H189.24L192.38 243.12H190.38L189.64 240.05H186.24L185.53 243.12H183.51ZM186.6 238.42H189.27L188.49 234.96C188.35 234.38 188.24 233.85 188.14 233.38C188.04 232.91 187.98 232.6 187.94 232.38C187.94 232.56 187.88 232.893 187.76 233.38C187.67 233.84 187.55 234.38 187.42 234.95L186.6 238.42Z" fill="white"></path>
                        <path d="M194.4 243.12V230.71H198.31C199.036 230.695 199.756 230.852 200.41 231.17C200.987 231.449 201.47 231.89 201.8 232.44C202.138 233.026 202.308 233.694 202.29 234.37C202.307 235.141 202.087 235.898 201.66 236.54C201.253 237.15 200.655 237.608 199.96 237.84L202.46 243.12H200.23L198.01 238.12H196.38V243.12H194.4ZM196.4 236.36H198.33C198.592 236.372 198.855 236.332 199.102 236.243C199.349 236.154 199.576 236.017 199.77 235.84C199.954 235.654 200.098 235.432 200.191 235.187C200.284 234.943 200.324 234.681 200.31 234.42C200.323 234.157 200.281 233.895 200.188 233.649C200.095 233.403 199.953 233.179 199.77 232.99C199.576 232.813 199.349 232.676 199.102 232.587C198.855 232.498 198.592 232.458 198.33 232.47H196.4V236.36Z" fill="white"></path>
                        <path d="M204.6 243.12V230.71H206.6V235.82H208.15L210.6 230.71H212.74L209.9 236.71L212.9 243.16H210.68L208.14 237.64H206.61V243.16L204.6 243.12Z" fill="white"></path>
                        <path d="M215 243.12V230.71H222.36V232.45H217V235.83H221.81V237.57H217V241.39H222.4V243.12H215Z" fill="white"></path>
                        <path d="M227.76 243.12V232.53H224.46V230.7H233V232.53H229.7V243.12H227.76Z" fill="white"></path>
                        <path d="M235.44 243.12V241.37H237.94V232.47H235.44V230.71H242.44V232.47H240V241.37H242.49V243.12H235.44Z" fill="white"></path>
                        <path d="M245.35 243.12V230.71H247.8L251.34 240.88C251.34 240.54 251.28 240.14 251.24 239.67C251.2 239.2 251.17 238.67 251.15 238.2C251.13 237.73 251.15 237.25 251.15 236.85V230.71H252.96V243.12H250.52L247 233C247 233.31 247.06 233.68 247.09 234.14C247.12 234.6 247.14 235.06 247.17 235.54C247.2 236.02 247.17 236.47 247.17 236.88V243.16L245.35 243.12Z" fill="white"></path>
                        <path d="M259.41 243.29C258.701 243.309 257.999 243.158 257.36 242.85C256.795 242.574 256.325 242.135 256.01 241.59C255.681 241.001 255.515 240.335 255.53 239.66V234.18C255.512 233.505 255.678 232.838 256.01 232.25C256.325 231.705 256.795 231.266 257.36 230.99C257.998 230.679 258.701 230.524 259.41 230.54C260.104 230.532 260.789 230.69 261.41 231C261.963 231.282 262.424 231.716 262.74 232.25C263.072 232.838 263.238 233.505 263.22 234.18H261.22C261.237 233.925 261.201 233.669 261.115 233.428C261.029 233.187 260.895 232.967 260.72 232.78C260.532 232.611 260.312 232.482 260.074 232.399C259.835 232.317 259.582 232.283 259.33 232.3C259.077 232.284 258.822 232.318 258.582 232.4C258.342 232.483 258.12 232.612 257.93 232.78C257.753 232.965 257.618 233.186 257.532 233.427C257.445 233.668 257.411 233.925 257.43 234.18V239.66C257.413 239.915 257.448 240.171 257.534 240.412C257.62 240.653 257.755 240.874 257.93 241.06C258.118 241.233 258.339 241.367 258.579 241.453C258.82 241.539 259.075 241.575 259.33 241.56C259.584 241.576 259.838 241.54 260.077 241.454C260.316 241.368 260.535 241.234 260.72 241.06C260.894 240.869 261.028 240.645 261.114 240.401C261.2 240.157 261.236 239.898 261.22 239.64V238.33H258.99V236.6H263.19V239.66C263.205 240.331 263.039 240.995 262.71 241.58C262.395 242.12 261.934 242.56 261.38 242.85C260.765 243.144 260.092 243.295 259.41 243.29Z" fill="white"></path>
                        <path d="M279.78 243.29C279.032 243.309 278.288 243.165 277.6 242.87C277.017 242.617 276.522 242.196 276.18 241.66C275.84 241.111 275.666 240.476 275.68 239.83H277.68C277.67 240.071 277.717 240.311 277.818 240.53C277.919 240.749 278.07 240.941 278.26 241.09C278.721 241.414 279.278 241.572 279.84 241.54C280.371 241.57 280.895 241.41 281.32 241.09C281.5 240.937 281.643 240.745 281.737 240.528C281.83 240.312 281.873 240.076 281.86 239.84C281.871 239.429 281.741 239.026 281.49 238.7C281.22 238.363 280.842 238.13 280.42 238.04L278.92 237.62C278.07 237.416 277.309 236.942 276.75 236.27C276.249 235.621 275.984 234.82 276 234C275.985 233.362 276.148 232.731 276.47 232.18C276.787 231.663 277.245 231.247 277.79 230.98C278.415 230.684 279.099 230.537 279.79 230.55C280.805 230.498 281.803 230.826 282.59 231.47C282.933 231.784 283.206 232.168 283.388 232.595C283.571 233.023 283.66 233.485 283.65 233.95H281.65C281.66 233.724 281.62 233.499 281.534 233.29C281.448 233.081 281.317 232.893 281.15 232.74C280.758 232.424 280.262 232.267 279.76 232.3C279.274 232.271 278.794 232.42 278.41 232.72C278.246 232.86 278.117 233.035 278.032 233.233C277.947 233.43 277.909 233.645 277.92 233.86C277.906 234.278 278.036 234.688 278.29 235.02C278.545 235.344 278.894 235.582 279.29 235.7L280.84 236.14C281.689 236.335 282.451 236.802 283.01 237.47C283.521 238.137 283.786 238.96 283.76 239.8C283.78 240.442 283.61 241.075 283.272 241.621C282.934 242.167 282.443 242.601 281.86 242.87C281.208 243.167 280.496 243.31 279.78 243.29Z" fill="white"></path>
                        <path d="M289.94 243.29C289.432 243.32 288.922 243.248 288.443 243.076C287.963 242.904 287.523 242.637 287.15 242.29C286.807 241.934 286.542 241.512 286.37 241.049C286.197 240.586 286.123 240.093 286.15 239.6V230.69H288.15V239.59C288.122 240.107 288.286 240.616 288.61 241.02C288.979 241.36 289.463 241.549 289.965 241.549C290.467 241.549 290.951 241.36 291.32 241.02C291.652 240.62 291.82 240.109 291.79 239.59V230.69H293.79V239.6C293.813 240.093 293.737 240.585 293.565 241.047C293.393 241.509 293.129 241.932 292.79 242.29C292.409 242.643 291.959 242.913 291.469 243.085C290.978 243.257 290.458 243.327 289.94 243.29Z" fill="white"></path>
                        <path d="M296.64 243.12V241.37H299.14V232.47H296.64V230.71H303.64V232.47H301.14V241.37H303.64V243.12H296.64Z" fill="white"></path>
                        <path d="M309.36 243.12V232.53H306.06V230.7H314.63V232.53H311.33V243.12H309.36Z" fill="white"></path>
                        <path d="M317 243.12V230.71H324.36V232.45H318.95V235.83H323.77V237.57H318.95V241.39H324.36V243.12H317Z" fill="white"></path>
                        </svg>
                  
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12  text-center">
                  <div class="f-16 f-md-20 w400 white-clr lh140 text-capitalize restrosuite-preheadline">                       
                    <span class="orange-clr w600 text-uppercase">Finally!</span> Restaurants Saviour from UberEats, GrubHub, & Deliveroo Is Here…
                  </div>
               </div>
               <!-- <div class="col-12 mt-md30 mt20 f-md-45 f-26 w700 text-center white-clr lh140">
                  All-In-One Marketing Suite Lets YOU <span class="orange-clr">Help Desperate Local Restaurants Grow Online & Charge BIG Fee </span>for Your 7-Minute Work
               </div> -->
               <div class="col-12 f-md-45 f-28 w600 text-center white-clr lh140 mt20 mt-md80 relative">
                  <img src="assets/images/restrosuite-game-changer.webp" class="img-fluid d-none d-md-block restrosuite-gameimg">
                  <div class="restrosuite-gametext d-md-none">
                     Game Changer
                  </div>
                  <div class="restrosuite-mainheadline">
                     All-In-One Marketing Suite Lets YOU <span class="orange-clr3">Help Desperate Local Restaurants Grow Online & Charge BIG Fee </span>for Your 7-Minute Work
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 text-center">
                  <div class="f-18 f-md-20 w500 lh140 white-clr text-capitalize">               
                     Auto Create Modern Websites | Zero-Contact Menu | Online Ordering & Payments | <br class="d-none d-md-block"> Table Booking | QR Code I Bio Links | Email Marketing System | 100% Newbie Friendly
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40 align-items-center">
               <div class="col-md-7 col-12 mx-auto">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto"> -->
               
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://restrosuite.dotcompal.com/video/embed/67fmqio11q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
               </div>
               <div class="col-md-5 col-12 mx-auto">
                  <div class="restrosuite-key-features-bg">
                     <ul class="restrosuite-list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                       <li> Sell Something New, Simple & Done-For-You 7-Minute Services to Businesses</li>
                       <li> Every Restaurant, Spa Etc. Desperately Need This Future-Ready Technology</li>
                       <li> Help Them Get More Reach, Leads & Orders</li>
                       <li> Complete All-In-One Marketing & Sales System</li>
                       <li> <span class="w600">Bonus Training</span>  to Get Tons of Pre-Qualified Restaurant Leads using Software</li>
                       <li> <span class="w600">FREE Commercial License</span>  to Serve Clients </li>
                     </ul>
                  </div>
               </div>
            </div>
            
         </div>
         <img src="assets/images/restrosuite-chef-icon.webp" alt="Chef-ico" class="img-fluid d-none d-md-block restrosuite-ele1">
         <img src="assets/images/restrosuite-puff.webp" alt="Puff" class="img-fluid d-none d-md-block restrosuite-ele2">
      </div>
      <div class="tr-all">
         <picture>
         <source media="(min-width:768px)" srcset="assets/images/restrosuite.png">
               <source media="(min-width:320px)" srcset="assets/images/restrosuite-mview.png" style="width:100%" class="vidvee-mview">
               <img src="assets/images/restrosuite.png" alt="" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase CloudFusion, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                  
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        50 Animated Video Backgrounds

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Nice PRO Looking Animated Backgrounds For Your Videos
                           </li>
                           <li>
                           Are you looking for an animated video backgrounds that are easy to use? Motion background that you will surely love? Then, download this one!
                           </li>
                           <li>
                           This tool provides 50 animated video background that are created magnificently! Some shapes are elegant abstract particle background. These animated videos will definitely of great use for your videos!
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        100 Video Transition Backgrounds
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Nice PRO Looking Transition Backgrounds For Your Videos
                           </li>
                           <li>
                           Are you looking for video that could be a transition backgrounds that are easy to use? Transition background that you could could use for your video edition? Then, download this one!
                           </li>
                           <li>
                           This tool provides 100 videos that are created thoroughly! This product assures you that the videos are easily to use as well as it has high definition so that your output will look like a professionally-made project.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                         </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Pro Music Tracks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">More Professional Quality Sound Tracks For Your Marketing Needs!</li>
                           <li>Professional Quality Music Tracks For Your Marketing Needs! These music tracks vary in length from 30 seconds to 5 minutes!</li>
                           <li>With this professional music track you can seperate your plain ordinary video from professional videos that used professional music or audio tracks. So if you're an audio engineer or a video creator, these tracks are very helpful in dubbing your text slides, creating an effects or insert it in your logo animation.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        47 Motion Videos!

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Attention video producers and resellers of Rich Media products Still paying $50 at Digital Juice for your motion video background loops? This 47 motion video background loops will give your video productions that POLISHED FEEL you've been after.
                           </li>
                           <li>
                           Television producers have been using motion video loops and elements for years. Ever see those notices that warn about "viewer discretion is advised" etc? They use motion video loops. And now that many internet marketers are producing their video advertisements, video demos, and video info-products, you'll find many of these same VIDEO MOTION ELEMENTS in their arsenal of tools.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md30">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-colo mt20 mt-md30">
                        Beautiful Outdoors Stock Images

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!
                           </li>
                           <li>
                           Many successful online business owners have said that making money online is easy as a piece of cake as long as you have all the ingredients in doing the process.
                           </li>
                           <li>
                           And one of those ingredients is graphics or images which is a huge help in marketing your product or service online using social media networking sites.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"CLOUD"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CLOUDFUSION"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Shopify Traffic

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.
                           </li>
                           <li>
                           With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you have little budget, you can start an online shop on a small scale.
                           </li>
                           <li>
                           Shopify Traffic is a series of training course that will teach you how to generate traffic to your Shopify e-store with effective methods and platform from personal experience and culmination of researches together with years of studies.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        JVZoo Training Videos

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Learn How to Use JVZoo to Make Money Online!
                           </li>
                           <li>
                           If you are into selling digital products, selling it using a reputable platform will give you the ease of selling your products as well managing your affiliates if you wish to do so.
                           </li>
                           <li>
                           You may already have heard about JVZoo. Like Clickbank, this marketplace and platform is a huge help to make your online business a huge success on the internet.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12  d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt2- mt-md30">
                        Double Your Email Campaign Conversion Rates

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           How to Double Your Email Campaign Conversion Rates!
                           </li>
                           <li>
                           The money is in the list. This words came form the very mouth of those many successful online business owners. 
                           </li>
                           <li>
                           The thing is that, building a list is not your only job, you also need to learn how to maximize your list to make it more profitable.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        WordPress Speed Video Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Brand New Techie Training Videos That Are Brain-Dead Simple To Follow!
                           </li>
                           <li>
                           Website loading speed matters this is one of the factors to get your site rank higher in Google search results. If your website is built with WordPress, optimizing your website is very easy to do.
                           </li>
                           <li>Inside this video course is a series of training on how you can speed up the loading of your WordPress website easily even if you haven't done this before.</li>
                        </ul>
                     </div>
                  </div>
               </div>s
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                        </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Bitcoin Profit Secrets Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           <span class="w600">Whether you heard of Bitcoin years ago (but didn’t take action), or you just heard of it today, anyone can profit from Bitcoin!</span> Don’t be scared of this new technology because this video course will take you by the hand and teach you everything you need to know to succeed.
                           </li>
                           <li>
                           It will give you the background on Bitcoin, how it started, who developed it, why it was developed in the first place, and why it’s so much better than any national currency on earth. 
                           </li>
                           <li>
                           You will also learn how to acquire your first bitcoin, how to mine it, how to trade or invest it, and so much more!
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"CLOUD"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CLOUDFUSION"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Forex Trading Fortunes

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Forex trading is about currency trading on Forex market. </span> The basic principle that operates on every market, applies here as well: in order to make money, you have to buy low then sell high. That's the whole philosophy.</li>
                           <li>Although there is a strong potential of earnings on Forex market, you should keep in mind that there are risks as well. Knowing the basics only would not be enough. A correct plan of investment and a strategy for it are strongly recommended.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Sales Funnel Optimization Strategies Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.</li>
                           <li>The sales funnel model helps you craft together working strategies that would help you turn your content and traffic into cash.</li>
                           <li>This video course teaches you how to optimize your sales funnel. It will show you key strategies that will help you maximize conversions and thereby maximize your profits.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Seo Revolution Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">There is plenty of information available online to manipulate the rankings in one way or another, but one factor is constant: SEO’s usage is on an ever-increasing rise. </li>
                        <li>
                        This is a complete collection of 15 High Definition videos with step by step content. 
                        </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Buying Traffic to Generate Massive Website Visitors

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.</li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        US Free Ads Traffic Video Course

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">With 1000s of visitors a month and a high Alexa ranking this classified ad source is a gold mine for those who take advantage of this!</li>
                           <li>Traffic is very important to every online business owners. Traffic is where you can find the source of qualified leads that will turn into a loyal customer if you have built a good relationship to them.</li>
                           <li>The thing is that traffic generation is always been a challenge to every website owners. That's why trying many sources they can is also a good move to know which traffic source is good for their online business.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"CLOUD"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CLOUDFUSION"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #16 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Youtube Success Step By Step

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">YouTube Success Step By Step is a new powerful report that explains to the reader how they can create a successful YouTube channel in the fastest possible time.</span> Readers of the report will learn everything that they need to know about choosing the right subject for their YouTube channel and publishing the most engaging video content on it.</li>
                           <li>Readers will learn that they must choose the right subject or niche for their YouTube channel. They will understand why the theme of their channel is so important and how to make the right decision about this based on the needs of their audience. There are examples of the best video content to create for a successful channel.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Virtual Networking Success

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.</li>
                           <li>Virtual networking allows this to happen, whether you are in the office or work from anywhere.</li>
                           <li>Through virtual networking, you will be able to take advantage of online platforms, social media, and even online webinars and conferences in order to form connections with others, even when you are not able to meet them in-person.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Start Your Own Hosting Company

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>If you’re reading this then congratulations, you’re well on your way to starting a wildly successful Internet business in website hosting! Everybody who’s anybody has a website these days from production artists to babysitters. Competitive web hosting services are among the fastest growing online businesses around the globe!</li>
                           <li>Before you start your journey into online success you need to make sure that this endeavor is really for you. The beauty of this kind of online business is that it’s pretty easy to set up and when you get tired of it there are “escape” or “exit” plans for cashing in and abandoning ship.</li>
                           
                        </ul>
                     </div>
                  </div>
                     </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Instant Email Popup Generator

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Opens Your Visitors Default Email Program With Your Message, Subject Line And Email Address</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Easy Popup Generator

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <span class="w600">No one likes popups now a days, but let's face it.</span> There are times when they are absolutely neccessary, or at least vital, to the layout and deployment workings of your website.</li>
                           <li><span class="w600">This software will enable you to create simple popup windows for your own sites.</span> Great little tool for those who need to display images, screenshots or open links within a new window. Customize whether or not to display the browser address bar, status bar and so much more...</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"CLOUD"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  Grab CloudFusion + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CLOUDFUSION"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px;"><defs><style>.cls-1b{fill:#0068ff;}.cls-2{fill:#f89b1c;}.cls-3w{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1b" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path><path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text><path class="cls-3w" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path><path class="cls-3w" d="M344.08,43.53v86H326.19v-86Z"></path><path class="cls-3w" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path><path class="cls-3w" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path><path class="cls-3w" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path><path class="cls-3w" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path><path class="cls-3w" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path><path class="cls-3w" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path><path class="cls-3w" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path><path class="cls-3w" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path><path class="cls-3w" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path><text></text></g></g></svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © CloudFusion 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
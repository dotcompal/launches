<!Doctype html>
<html>
   <head>
      <title>GoofyShort - Creates & Publish YouTube Shorts in Just 60 Seconds…</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="GoofyShort Special">
      <meta name="description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta name="keywords" content="GoofyShort">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.goofyshortsai.co/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ariel Sanders">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="GoofyShort Special">
      <meta property="og:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="og:image" content="https://www.goofyshortsai.co/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="GoofyShort Special">
      <meta property="twitter:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="twitter:image" content="https://www.goofyshortsai.co/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css"
         type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
   <script src="../common_assets/js/bootstrap.min.js"></script>

   <script>
                        (function(w, i, d, g, e, t, s) {
                            if(window.businessDomain != undefined){
                                console.log("Your page have duplicate embed code. Please check it.");
                                return false;
                            }
                            businessDomain = 'goofyshort';
                            allowedDomain = 'www.goofyshortsai.co';
                            if(!window.location.hostname.includes(allowedDomain)){
                                console.log("Your page have not authorized. Please check it.");
                                return false;
                            }
                            console.log("Your script is ready...");
                            w[d] = w[d] || [];
                            t = i.createElement(g);
                            t.async = 1;
                            t.src = e;
                            s = i.getElementsByTagName(g)[0];
                            s.parentNode.insertBefore(t, s);
                        })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');</script>

   </head>
   <body>
   <div class="header-wrap">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-3 text-md-start text-center">
                  <div>
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     viewBox="0 0 627.01 153.63">
                     <defs>
                        <style>
                           .cls-1tb {
                              fill: none;
                           }

                           .cls-2tb {
                              fill: #231f20;
                           }

                           .cls-3tb {
                              clip-path: url(#clip-path);
                           }

                           .cls-4tb {
                              fill: url(#linear-gradient);
                           }

                           .cls-5tb {
                              fill: url(#linear-gradient-2);
                           }

                           .cls-6tb {
                              clip-path: url(#clip-path-2);
                           }

                           .cls-7tb {
                              clip-path: url(#clip-path-3);
                           }

                           .cls-8tb {
                              clip-path: url(#clip-path-4);
                           }

                           .cls-9tb {
                              fill: url(#linear-gradient-3);
                           }
                        </style>
                        <clipPath id="clip-path">
                           <path class="cls-1tb"
                              d="M190.85,44.64A21.3,21.3,0,0,1,198.51,46a22.82,22.82,0,0,1,6.19,3.5,22,22,0,0,1,4.61,5.07,24.06,24.06,0,0,1,2.89,6l-10.47,5.36a9.28,9.28,0,0,0-.7-2.43,11.79,11.79,0,0,0-5.2-5.64,10.21,10.21,0,0,0-5-1.12A14.81,14.81,0,0,0,184.5,58a14.47,14.47,0,0,0-5,3.88A18.45,18.45,0,0,0,176.2,68,26.11,26.11,0,0,0,175,76.3a31.07,31.07,0,0,0,.91,7.38,19.73,19.73,0,0,0,2.89,6.51,15.46,15.46,0,0,0,5,4.62,14.14,14.14,0,0,0,7.25,1.77,15.54,15.54,0,0,0,4.29-.54,15.73,15.73,0,0,0,3.38-1.36V83.63H186.23V72.92h25.14V107.3H198.76v-2.23a15.7,15.7,0,0,1-4.95,2.1,23.84,23.84,0,0,1-5.52.62,23.09,23.09,0,0,1-9.4-2.06,25.54,25.54,0,0,1-8.45-6.06,31.7,31.7,0,0,1-6.14-9.89,35.68,35.68,0,0,1-2.39-13.48,36.42,36.42,0,0,1,2.14-12.66,29.76,29.76,0,0,1,6-10,27.84,27.84,0,0,1,20.78-9Z" />
                        </clipPath>
                        <linearGradient id="linear-gradient" x1="216.73" y1="84.3" x2="260.14" y2="84.3"
                           gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#0568af" />
                           <stop offset="1" stop-color="#49c0ef" />
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1="263.48" y1="84.29" x2="306.93" y2="84.29"
                           xlink:href="#linear-gradient" />
                        <clipPath id="clip-path-2">
                           <path class="cls-1tb"
                              d="M309,71.76V61.87h5.69V57.33a21.75,21.75,0,0,1,1.94-9.93,15.41,15.41,0,0,1,11.17-8.53,35.49,35.49,0,0,1,6.51-.66,12.91,12.91,0,0,1,2.06.16c.61.11,1.27.25,2,.41a6.28,6.28,0,0,1,2,.83l-2.14,11.21-1.61-.37a7.33,7.33,0,0,0-1.61-.21,9.19,9.19,0,0,0-4.49.95A6,6,0,0,0,328,53.75a10.93,10.93,0,0,0-1.07,3.67,36.76,36.76,0,0,0-.24,4.45H337.9v9.89H326.64l.08,35.45h-12l-.08-35.45Z" />
                        </clipPath>
                        <clipPath id="clip-path-3">
                           <path class="cls-1tb"
                              d="M353.35,61.7l11,29.44L375,61.7h12q-4.69,12.38-8.49,22.1c-1.05,2.8-2.09,5.54-3.14,8.2s-2,5.07-2.8,7.22-1.48,3.89-2,5.23-.77,2.08-.82,2.19a42.63,42.63,0,0,1-4.87,9.85,23.54,23.54,0,0,1-5,5.4,12.93,12.93,0,0,1-4.65,2.31,17.79,17.79,0,0,1-3.84.49h-2.14a20.28,20.28,0,0,1-3.3-.33c-1.27-.22-2.45-.47-3.55-.74l3.6-11.79a8.25,8.25,0,0,0,4.12,1.07,6.49,6.49,0,0,0,5.07-2,18.51,18.51,0,0,0,3.42-4.94L341.14,61.7Z" />
                        </clipPath>
                        <clipPath id="clip-path-4">
                           <polygon class="cls-1tb" points="63.23 70.74 46.44 94.51 33.66 72.48 63.23 70.74" />
                        </clipPath>
                        <linearGradient id="linear-gradient-3" x1="0" y1="76.82" x2="141.06" y2="76.82"
                           xlink:href="#linear-gradient" />
                     </defs>
                     <title>goofyshorts svg logo</title>
                     <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g id="Layer_1-2-2" data-name="Layer 1-2">
                              <path class="cls-2tb"
                                 d="M395.58,89.55,403,88.9a16.27,16.27,0,0,0,2.43,7.27,13.31,13.31,0,0,0,5.94,4.59,22.54,22.54,0,0,0,9.06,1.75,21.64,21.64,0,0,0,7.89-1.33,10.89,10.89,0,0,0,5.09-3.64A8.4,8.4,0,0,0,435,92.49a7.6,7.6,0,0,0-1.61-4.85,11.94,11.94,0,0,0-5.31-3.49,108.77,108.77,0,0,0-10.51-2.87c-5.42-1.31-9.22-2.53-11.39-3.69a15.92,15.92,0,0,1-6.3-5.49,13.54,13.54,0,0,1-2.07-7.35,15,15,0,0,1,2.53-8.35,15.71,15.71,0,0,1,7.41-5.9,28.24,28.24,0,0,1,10.83-2,29.57,29.57,0,0,1,11.57,2.11,16.69,16.69,0,0,1,7.71,6.22,17.79,17.79,0,0,1,2.9,9.3l-7.49.56q-.6-5.59-4.08-8.45c-2.33-1.91-5.75-2.86-10.29-2.86q-7.08,0-10.33,2.6a7.78,7.78,0,0,0-3.24,6.26,6.7,6.7,0,0,0,2.3,5.23q2.25,2.06,11.77,4.21t13.07,3.76a18,18,0,0,1,7.61,6,14.7,14.7,0,0,1,2.45,8.39,16.13,16.13,0,0,1-2.7,8.88,17.67,17.67,0,0,1-7.75,6.48,26.84,26.84,0,0,1-11.37,2.32,33.82,33.82,0,0,1-13.42-2.34,18.77,18.77,0,0,1-8.5-7A20,20,0,0,1,395.58,89.55Z" />
                              <path class="cls-2tb"
                                 d="M452.3,108.51v-59h7.25V70.67a16.17,16.17,0,0,1,12.8-5.88,17.25,17.25,0,0,1,8.25,1.87,11,11,0,0,1,5,5.18c1,2.2,1.51,5.39,1.51,9.58v27.09h-7.25V81.42c0-3.63-.78-6.26-2.35-7.91A8.76,8.76,0,0,0,470.86,71a11.86,11.86,0,0,0-6.06,1.67,9.39,9.39,0,0,0-4,4.53,20.79,20.79,0,0,0-1.21,7.89v23.39Z" />
                              <path class="cls-2tb"
                                 d="M529.92,70.57a19.19,19.19,0,0,0-14.41-5.78,19.91,19.91,0,0,0-13.45,4.75q-6.6,5.73-6.6,17.59,0,10.89,5.54,16.65t14.51,5.76a20.93,20.93,0,0,0,10.32-2.62A17.44,17.44,0,0,0,533,99.57q2.48-4.75,2.48-13Q535.52,76.36,529.92,70.57Zm-5.43,28.86a10.77,10.77,0,0,1-1.17,1.17,11.93,11.93,0,0,1-16.83-1.17q-3.58-4.08-3.58-12.3t3.56-12.22a11.78,11.78,0,0,1,1.06-1.06,12,12,0,0,1,16.94,1.06q3.6,4.07,3.6,12Q528.05,95.32,524.49,99.43Z" />
                              <path class="cls-2tb"
                                 d="M543.93,108.51V65.76h6.52v6.48c1.67-3,3.2-5,4.61-6a8.05,8.05,0,0,1,4.65-1.45,14.13,14.13,0,0,1,7.45,2.34l-2.5,6.72a10.28,10.28,0,0,0-5.31-1.57,6.93,6.93,0,0,0-4.27,1.43,7.66,7.66,0,0,0-2.69,4,28.15,28.15,0,0,0-1.21,8.46v22.38Z" />
                              <path class="cls-2tb"
                                 d="M587.29,102l1,6.4a26.53,26.53,0,0,1-5.47.64,12.45,12.45,0,0,1-6.12-1.24,7,7,0,0,1-3.06-3.28c-.59-1.33-.88-4.19-.88-8.56V71.39h-5.32V65.76h5.32V55.17l7.2-4.35V65.76h7.29v5.63H580v25a12.83,12.83,0,0,0,.38,4,3,3,0,0,0,1.25,1.41,4.85,4.85,0,0,0,2.48.52A22.54,22.54,0,0,0,587.29,102Z" />
                              <path class="cls-2tb"
                                 d="M591.48,95.75l7.16-1.13a10,10,0,0,0,3.36,6.6q2.76,2.3,7.71,2.3t7.41-2a6.08,6.08,0,0,0,2.41-4.77,4.42,4.42,0,0,0-2.13-3.87q-1.49-1-7.41-2.46a68.23,68.23,0,0,1-11-3.48,10.53,10.53,0,0,1-6.26-9.8A11.08,11.08,0,0,1,594,71.82a11.69,11.69,0,0,1,3.56-4,15.07,15.07,0,0,1,4.61-2.11,22.06,22.06,0,0,1,6.26-.87,24.64,24.64,0,0,1,8.84,1.45,12,12,0,0,1,5.62,3.92,15.43,15.43,0,0,1,2.49,6.63l-7.08,1a7.79,7.79,0,0,0-2.8-5.15,10.28,10.28,0,0,0-6.5-1.94q-5,0-7.13,1.65a4.81,4.81,0,0,0-2.13,3.86,4,4,0,0,0,.88,2.54,6.43,6.43,0,0,0,2.78,1.93c.73.27,2.87.88,6.4,1.85A86.73,86.73,0,0,1,620.48,86a10.78,10.78,0,0,1,4.77,3.8A10.65,10.65,0,0,1,627,96a12.12,12.12,0,0,1-2.11,6.82,13.62,13.62,0,0,1-6.1,5,22.06,22.06,0,0,1-9,1.76q-8.32,0-12.7-3.47C594.14,103.71,592.28,100.28,591.48,95.75Z" />
                              <g class="cls-3tb">
                                 <image width="51" height="65" transform="translate(161.49 43.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAABBCAYAAAB8S5zgAAAACXBIWXMAAAsSAAALEgHS3X78AAAGQUlEQVRoQ91aS5bkOAgM5Jq7zu36iMxCgAChX7/uzUQ9p2UJZEAQcjqL/vn3F7evoX0N39fQvg/fj15/o//ns/HWCK01kJ6/fm6NQHL24+1rAAFE1I9Go727BtA/ui7kmvpH6CciNPyPUDvDcrhLMINzX5L3fWEcPZAEgCi1KbXl+B2UzqghzJwMrcx1Z8ZwWnQ5u7XyeBeJHZxs4UyciZ03fmUsb4l6JHPO60F/KZMLh39sQI0N0fX9yRvVSx0MAg3/Qe6zRB+e2jHT/FVNBICujCQxScKGyBpjCStlcBUk6eFycI8H+SyarONxcqvAsjxHAijG9fxEAMjwd1u1V2wm8AQgPX60PicC2Kbmov2wOEG4zJt4j+5NLplrAmi/ybMnFB7/fQLQBcuiQ2UigNn9P0kA30wA7BqzobK62fkbcOVMjTz7mgBCEvNYoSyr3vhaceN69UIAVXDO7R0BMIwAolmT2NzHMOdfH02e19Ip3BGA0fIY0XylHQFUYb4L8hmF/BMBUA7zNKESAANMUv1Uiw6VQAAEd214JADSKNuur8UvBNDiIrJrVHX+GmjFS1rme5iFLJ+W79rD9pFUOZ4dAUQi6LglgBp+rlX7iQB+YwXqZ5M1+DBfBafwSAADRwIgglJ6wCbIT46o8FQzkkU7AmiN5jTgcIIFgqevZODVwaNtghNqAsjLfkEA/QXFMsDeslpixmKyfd1UiBPVBACLMVijvDR2nCMBROld0fvSqkPge9dB2r7Q4OGZdNSi/qxXfU96CnNJMEc4nTMBuDD7/kwA4b0XbeK8CDJPjQN42KDYEABbP7U2WCtN6E7wgcg28ergG/vJtQYBZL0FATR7U0mta1XGWcMHAzinSzGZ1tApM3dTJwJQQ3LRV+FjOMkhvPCI9E9T0bf31b9BVNi+0MhPADt410bd+DDz5vCtCn5kLXUmAPMmelUTQBMn0mp5FHYFqZvICQgxJzdfAbrx+jaf/GagUD3oUNUaoiVY5La1UhAA5ntEArDv/U0e+8n2mmxMqBULQD8fo5vG/Sb6RwgAgJNMBm0M5OJcub8mgCFzZMEJUeEndrvvM5pqk8oOjCZ109PhJtHGc996UXxC5eQaOBLACNcc6ZkA1LBaZ+riYjXuIzcRwPYJQOsnPqI4qJ5eOkMqG6ujFFboLfXeiSmyWkkA/bG/mWOVYkUA/fIitE6E9N6YbIW/RdarUBJAf+x3TmwMjPeRT/YjHRMBpOKfNVbwUlFjSQBgGAEUehHMAOteJDU0FWmagAGAwE7sjgDWWBIAj8vSj0AAL2/7c3zy5Ifl8UG6JoBRKzGRFGaI6cWXHpWN+VgKKzIBIIpmtWll9EZNWKzRSJ9ZcjhrEz8QgBU/uSS6XOAKE5s1OW4JACplS6XiUWcigCRS3oHTebqIWvH7jKSKpZtHcTf7isBwD6M7AvCHtDQOXmzCKZgdJQFYvWweZwij8MsNdQUOpzekIB0JYNr7Fndlp9c7HgigGqwgtlqw0ipmtfJVk/5nUqddmWwKvq5aWr1jfXUQ7gngbsZAAFL8RBgmoq4fDx7paF2FfCSAoaun8haH7MhwK8OjGKdc4HlC54StooS7XMiE55QrBaLw9HoW7FdlPbPt/ESXmSXmp9q6gyg8EwAQw1UsSu9mGZPRBwKYsHJObX0jgOGN/muibW5LAsBQhZv4Iuy6aXoCGIOFwnlKAEYA4oD9bjm0lwTAGPvQLQFY8ccIXNp6xCAAsVqNz2/PPPyV/ZZzQwA2tzuyJ+VtV7ZE5UAAfWJxY3OTbrPs/NcFLcHS5gNs03whAEAXiKXNNpxRbZY3BFBiNTDq3hrrdRFnGP0Fhj4xBwKgNQH4WvFO7eCfmj0JDIGF4gUaAPsH68K00raQKm4lo0CEPgHkNdqumBe6QF8Z3Sg1VXikEaQ/gPsLD904kQgACwLoGTAcCEEZQgVWyRWFHZvpEFdy4dqKH0Cwbgsumyt4BvdfMY4EwGM5nFPavwe71okAligGGZgJYNMGgKZpbI/9ljJz6tgkshqls6sAcNw4/w4BMIrfLV1raRsPUQ4VVkZ6pEq8ix5bHAU63Kap0dZrhyJn7G0/3REA6x8nB4q5y/YFWoz8aqY46/ULP8FF6R1wZ9d/MrZIFSpkXSoAAAAASUVORK5CYII=" />
                              </g>
                              <path class="cls-4tb"
                                 d="M216.73,84.29a27.34,27.34,0,0,1,1.61-9.48,21.78,21.78,0,0,1,4.54-7.5,21.24,21.24,0,0,1,6.88-4.91A21.69,21.69,0,0,1,254,67.31a22.09,22.09,0,0,1,4.54,7.5,27.33,27.33,0,0,1,1.6,9.48,26.88,26.88,0,0,1-1.6,9.44,22.56,22.56,0,0,1-4.54,7.51,20.7,20.7,0,0,1-6.92,4.94,21.18,21.18,0,0,1-8.7,1.78,20.75,20.75,0,0,1-15.54-6.72,22.39,22.39,0,0,1-4.54-7.51A27.08,27.08,0,0,1,216.73,84.29ZM238.42,97A8,8,0,0,0,243,95.75a10.28,10.28,0,0,0,3-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24A12.38,12.38,0,0,0,246,75.93a10.43,10.43,0,0,0-3-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1A13.75,13.75,0,0,0,229.14,80a17.51,17.51,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,238.42,97Z" />
                              <path class="cls-5tb"
                                 d="M263.48,84.29a27.34,27.34,0,0,1,1.61-9.48,21.91,21.91,0,0,1,4.53-7.5,21.29,21.29,0,0,1,6.89-4.91,21.75,21.75,0,0,1,24.28,4.91,21.91,21.91,0,0,1,4.53,7.5,27.34,27.34,0,0,1,1.61,9.48,26.89,26.89,0,0,1-1.61,9.44,22.37,22.37,0,0,1-4.53,7.51,20.64,20.64,0,0,1-6.93,4.94,21.18,21.18,0,0,1-8.7,1.78,20.82,20.82,0,0,1-15.54-6.72,22.37,22.37,0,0,1-4.53-7.51A26.89,26.89,0,0,1,263.48,84.29ZM285.16,97a8,8,0,0,0,4.58-1.24,10.57,10.57,0,0,0,3.05-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24,12.38,12.38,0,0,0-1.69-4.12,10.6,10.6,0,0,0-3.05-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1,13.57,13.57,0,0,0-1.7,4.06,17.27,17.27,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,285.16,97Z" />
                              <g class="cls-6tb">
                                 <image width="32" height="70" transform="translate(308.49 37.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAABGCAYAAACt15azAAAACXBIWXMAAAsSAAALEgHS3X78AAAEz0lEQVRoQ61YaZorOQgTznt3ndvNDZv5YQNiqaW7h3wkZeKykYTJIn//+VfXWlif7J/PQo1/7Hpx7BNzVplH8Raz+bgyHULa4wrdb/ibup9jeOZhjM8JaNlnWChWjIUjj5OIFv/aEzg2JyCAlDEEEPHLuLC4ACICOQOZfB0/42sJKgMWM7Rn7Aw4C0GDqkILCzgseAxXEhxEU8xYyOgDVUO/hrHF9K4GKgWscYsF4qqxIUaJQQDFXQ3cMBCxrvPIQGVBxDX+JQNd48aAKsC6K5KWYwINKd8jsKcZ/YB2s7Ag62znyV4koP6UY6r5gqv8Ej3XifWBs6bgoREpjW1g+ytPIHnYYRtrFONmCKcI744hEM1Iwk0B4Vjyi8I7GweLe+nvMUBxrTFHPhXeQS+B3tB9iwGheGfg4fihSPeGgRZjZpgBBWpBsu6M/j0Dgmys8xljQj6xgAH9IwOVAqVFUqweu6L916n8VdDfMeBUDTFfBDeIU5wKdNhrbkQDA74IFcGIvlQ/a19VBZ4YoDuCgX0RZ/uChSUQSE58sPtWTDcpDP2g84X2/X1i8LEIY04aUA6BSnNMv/YrMwSS4rEIrxqRx0tBSnI6evRlxE/WrxiweFksoVc+ettH9I8MSGdAinusHT9k/VEAkd02osoAsekMMOp89Fj/IinZ7TFkBir6+7bL1Y8uKdmPGpEeCupRTNoz+gqI7EeN6L7tWkJo0k12zwDdFOg10I9tN5KpxTvZMwNWQIQelYWkfUH7wMCfKaj2VAoomkppuaLQNTCkCiUapxzsizrtdhawB20KDIXn9K/OkI2P3RRhF8wfQrHJ/VMvJ5uO4bFfMzCiH9suoec6uk5gYMBYqAyko7dp54RyooQLAwPy01bs3ttu/Uy4ZUCfWrHdIRzbm6yx7Z5rZPTGwGTvGlFBnbSfChMFvcz6A28b0ULaYJ0VZ+0L2ipnsWcGKvLjVXOQ7i4dkOSbrCUgyAyw7qY9UNAfkZWcbYqZtVbs0k/oT9vFilhQHFKEHJn7KYf7RlRot4XHwpMqhY33DlZSFwnUkr3YINFv+RYpwAnDYV8ocMNAYQFHgl0La0zQCiehN1z3CWQG7ui1mmCdWftdDUh1hFwKyUYGXmm/enKePA7i/4UB+5SDtISqRDte0L5jIFthFPqF67ZLDOzrgtbImTbCQyPavu8e/3Q+LCT0AwMtRtYSUBT0pv34NbzqH4m7TTGyZZNSG1gg7dG1J9+Z4syLxNmmmNluxRruiwxtN5wZogTPIrs4ad14aZaPoYbbokbzMsqXIP7xGqSA7Aex+v4YupadfkYe6Pox3AwcxMxs3xvABQOqmr7tOvpzKkQEuGpEkF80orQgPBEGmJoOOgMk+7ZvMUALuvbD8avat6PIO7xigBdw7TPqhtQ8Fa+/hL1igBf+Uv+4De2DhYY+FS9GBlqMLDFgG9Tf/RVti4HQD2inmFliwLW3Hx4XWrfYEZmUTDbFzJwBkQUg/86rDs2FqgUaTUk2xcy8FdtitpH/8eC8aqf4zOfkEKIgLh4kUBD95nT8gnams0uDI8cRBa8bkRyBHH2jPugNOtUT1/RmQXvm3zMw/L+Xjx8XWAyEWIhYQfvEgOpZlDJljVuM6NCK6wrpFXwA/wFL7o37L3LKTAAAAABJRU5ErkJggg==" />
                              </g>
                              <g class="cls-7tb">
                                 <image width="47" height="64" transform="translate(340.49 60.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAABACAYAAACKhS4jAAAACXBIWXMAAAsSAAALEgHS3X78AAAF7klEQVRoQ9Vaa84kOQoMSrN3ndvtDYv9YQzBw5XOlkajRXKRtsEOQ5isr6vlP3//V0UE8hF8Ph/TAvl8irZ5ubT7SLG98Hm7Nv6P5T14sXYhIgIIIOaz3MTdQ9vMXlus0/aJgXeRV27qQ6F1DSugqkUvn6XUGoqOZ6hCvwp863xu9+CHaNcIRlbOqRGLas4G5wMxsW0kW8gaeAEeKCj7ZCwO27QAStqeCeTqCllFb1rzAH4G5+rhELWbuO8WzH0a2XaeHXE/piIA/IVRFA2EKqASnBeBQiEQs1aoCsS4KypQVdKAqgCy78bWMt+Rr8ZdAoAPIB87mUG7p006C6e2JMKi1YV8mPOeDV5TYh23y3sCb8ADAe4wmQ6zN4xAObBYYiMPkHCfANuL65L3nN+PG+EvOx9icHDfCmniPpfUoNqyP4DXYcguisZzsHJ9qtl1DittPNf2sX0104s08IY25LQfpPQ8GXSpQvJArigl6tva705bDMAb8FvSIc4mAPDq6wHbCQpNgi4XtGFhuiBogIkuND9S44kugH4V36/mDE4NV+A9ZDxCWqLjduGQabCjO1Qc6orU7Mz15gK8yQFc9ILzkX62GyKQ6r2t4mPFfpD3pVKo/bKjbuZ+vZr8lOnllHTtdAdwVSo354P7zmnQBulyKdlx6VucPpVGAHSg9Zn7OTQXtBGKtI+QfsoIWXH03c3mvbxeZNPkAjzJCG6Iktnlrwf524sbbrttQQd8kvec34/HDYYxL3kZ3K7nY8nUzfb1+eecB2bO10v1xHnVxXlqjT4lPy2rCLmgjSS1HnkDqhVjRgooirqXUs+Mm1/JBXiSnxuU6GxwsgHnYzBdWrZQ6fKKNixMl63LBkSXABXgphIZMhGizs1yAd5DdtiGUl6jv0c8+ts23qJRmcg++c/rAlfgTRwggNMGDkSQdmEr+fGGHfb4Je9LpVD7ZYc4hB9mV5tUgZj7TMmJ+1ecZ5PN+c1txEZ2GZjznftRFke62C4zXc4auKJN7DAt4PMGikZNU/SbN/vuqbx69wm5AG8ybJDhiE9Vujz/RUXgfh4iy3vO70cBvHqMdks3fh85j4GS/yTnneN7njm/Wnvt26GTBh9ZSp9Hc6wuaCMe6bqAb9MyIdZd/QwwkcQmaI0X8o42DJLsMqjyvaX5xlgNhnekjY7i4OtCZ20QbZNW+iTzfKQXgFVieR5Oz2VQqDuIg88Xgi9I3mB6uUQth9fzgRt1IOxKRgbDYewl52NE9kz0OAvbrpbK5M1LSqgZ5yh/yPnd2E5yJsY2ZQ5UqSzD+MWEkOdSedpgU4XA5ahK0cjZsQB0e1xp4IY2wwY8EWD40kpotm8ilMUHu0GewQ8bNM4LH6tslA7D3ug+6RDPciiVfYMokQRk1cVEn5FergHnL1UwuLbnJtPYTalsF40al8V9GItuSI+zd9m32T+N3dBmcxmcD+vJzlOebdx3j/hs0g7xLPelkqKULyfSAZ982SIsyefFAR6+Hszcb1Ta/4g0llTmPpAoiX+S887vzPl2wB1Vb1P4DmONLre+T5wvlBDfjDQZp2wJSomMw3aZDvEs/vN9i2Ydl/VzPb6ReIUCH0BVghau+Wd5AUSxchY/5a+f+lkDKvxfAmB6+UZ/SaGNGvU6Xb7GxePfpRbudugS72VWLnydv9BAow1tJPmtKRswKiCiiwOSvAtZj2PC0z/silxz/viGHZ3QbX0mvy2Sz4zxKA9fD4QqztC+VAJ1l8ipVALlpiyx+dDYH0WmsRPnCSCQiEGu4pnxtDtdLlNfXl5HuzvaGEgD4tQoIMdLSEdL3Cc7tm5yGP4lZ9rsA7R5MWCIAyYpA3TwvlbxkTT4KAs8bQAR53N81c1UamPAwH0U3TmvGDivbznPG/j/cZlqcaaBZ2l3vE3hO4y1aN/6DpwXQxIAN3LaSGI2+fKoIEplsp6B9EM8i9NG5Dc1Oh14HC/oYhRodNlTr2mT6dIjMIQkZWKb3Kc8+/2ZfPx+SKwk1u/c3/NMgH/v68H/ADy+xA388a7LAAAAAElFTkSuQmCC" />
                              </g>
                              <g class="cls-8">
                                 <image width="30" height="25" transform="translate(33.49 69.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAYAAAAmNZ4aAAAACXBIWXMAAAsSAAALEgHS3X78AAABtElEQVRIS8VVW3LEIAyTnelde7vecNUPY5B5pJndj3oG4giIsYSJfX3/0C+HX46rPf26xE/8muY4roa9s9bxT1YCU7zhJ679OquunXFOszMwnzYCjEdiJB/hzPUl8NZ2Wad3YoR9jx3pGyDMDbAYi8AWzSx9g5n1dzPAomt4bd59h5vB3WBucI+DZB5Nd3XIWPM5YeMrygbBQjc55qkVjatGO58LnlRS5mVApsYtSA9vb2rM7RjLWFJtrrIgpORjjWddvesYAQzmHoGazpn57sQDtxmrUfrwksbx0aGnsnGyg8arlilixTUjHdMtjl7tY40JNMor/SoRVN9zHdfDYKY6Di1dDs2OjZ75gmngYnOmSWUulFJZiDyd9pXshz8JSj82MbQ/rz3ZzU9if8D0Sixtkig1Dr20VDXwTmPXO3cEOF8eK507JO18qommI3rNzjrPbJxYqmvj838frhfrh48zB/rEbi4QTpTHr238AmvJaRkO6R5obKYBImAaZXdzPlz6dXRnQnUrl6IdNxpVfSH4ih0a5jp+RYT5StQ67nPlTb06Z+6HeWZCiA4wDKJTFkU+t18L97cypuSgqgAAAABJRU5ErkJggg==" />
                              </g>
                              <path class="cls-9tb"
                                 d="M138.92,91.49l-6.76,3.92-6.49-11.19,6.75-3.92-2.14-3.69L117.72,83.9l-33.08-57L98.25,19l-2.2-3.8-7.81,4.53L81.74,8.54,89.55,4,87.23,0,53.51,19.57a19.6,19.6,0,0,0-24,13.93s0,0,0,.07l-2,1.16A19.62,19.62,0,0,0,1.66,44.81,19.83,19.83,0,0,0,.12,50.56,19,19,0,0,0,0,52.69a19.64,19.64,0,0,0,38,7,20.11,20.11,0,0,0,1-3.88,19.92,19.92,0,0,0,9.42,2.44A19.56,19.56,0,0,0,61.74,53l.32.58a2.71,2.71,0,0,0,3.69,1,2.62,2.62,0,0,0,.89-.8L75,42l-1-1.77A5.29,5.29,0,0,1,76,33l0,0,.33-.18L103,80.32l-.33.18a5.28,5.28,0,0,1-7.19-2L81,79.33a2.72,2.72,0,0,0-2.53,2.9,2.8,2.8,0,0,0,.32,1.1l2.53,4.51L48,107.48a21.72,21.72,0,0,1-23.49-1L11.2,97.19,36,131.31l-6.44,3.74,2.14,3.7,5.7-3.31,6.5,11.19-5.7,3.3,2.14,3.7L141.06,95.18ZM28.41,56a9.37,9.37,0,0,1-8.78,6.08,9.27,9.27,0,0,1-3.3-.6A9.38,9.38,0,1,1,28.41,56Zm20-8.07a9.38,9.38,0,0,1-.55-18.75h.55a9.39,9.39,0,0,1,9.32,8.27,10.32,10.32,0,0,1,.06,1.11A9.38,9.38,0,0,1,48.44,47.92ZM76.27,26.68,70.72,29.9l-2.49-4.29-3.58-6.16,7.81-4.54,6.07,10.46Zm-21.13,112-5.89-10.14,2.26-1.32L57.06,124l2.31,4L63,134.18Zm19.63-11.4-3.58-6.16-2.31-4,7.82-4.53,2.3,4,3.58,6.17ZM94.4,115.93l-3.58-6.17-2.3-4,7.81-4.54,2.3,4,3.58,6.16Zm19.6-11.4-3.57-6.16-2.31-4,5.55-3.22L116,89.86,121.84,100Z" />
                           </g>
                        </g>
                     </g>
                  </svg>
                     
                  </div>
               </div>
               <div class="col-md-9 mt15 mt-md0">
                  <ul class="leader-ul f-20 white-clr text-md-end text-center">
                     <li class="affiliate-link-btn mt10 mt-md0">
                        <a href="#buynow">Buy Now</a>
                     </li>
                  </ul>
               </div>
            </div>
            </div>
         </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mt20">
                  <div class="pre-heading f-18 f-md-18 w400 lh140 white-clr">
                     Make BIG Profits in 2023 by Copying a Proven System That Make Us $545/Day Over and Over Again…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Brand New AI APP <span class="red-clr">Creates 100s  Reels, Boomerang, & Short Videos with Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing & Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://goofyshort.dotcompal.com/video/embed/8x6aokyyl7"
                        style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                        allowfullscreen=""></iframe>
                     </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-20 f-md-28 w700 lh140 text-center white-clr">
                        Bonus Agency License Included for Time!
                     </div>
                     <div class="f-18 f-md-20 w400 white-clr lh140 mt10">
                        Use Coupon Code <span class="red-clr w600">"GoofyShort"</span>  for an Additional <span class="red-clr w600"> $3 Discount</span>
                     </div>
                     <div class="row">
                        <div class="col-md-11 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To GoofyShort</a>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-fluid mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-none d-md-block">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels & Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors & Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To GoofyShort</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">
                  Got 34,309 Targeted Visitors in Last 30 Days…                
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making Consistent $535 In Profits <br class="d-none d-md-block"> Each & Every Day        
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!-- Testimonials Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-44 f-28 w700 lh140 text-center black-clr2">
                  Checkout What GoofyShort Early Users Have to SAY
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views & 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to GoofyShort, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views & 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast & easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 red-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about GoofyShort is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, GoofyShort Team. <span class="w700">The best part about GoofyShort is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              GoofyShort is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "goofyShort" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->

      <!-- Reels Section Start -->
      <section class="reel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr2">
                     Checkout Some of Reels & Shorts <br class="d-none d-md-block">
                     Videos Created Using GoofyShort
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1  mt-md50">
               <div class="col mt20 mt-md100">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel1.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md0">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel2.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md60">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel3.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md30">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel4.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-90 mp4-border" controls>
                   <source src="assets/images/reel5.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md40">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-50 mp4-border" controls>
                   <source src="assets/images/reel6.mp4" type="video/mp4">
               </video>
               </div>
            </div>
         </div>
      </section>
      <!-- Reels Section End -->

      <!-- Ground-Breaking Section Start -->
      <div class="ground-breaking">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 text-center black-clr2 lh140">
                  GoofyShort Is Packed with Tons of Cool Features
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1">
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat1.webp" alt="Create Niche based short video" class="img-fluid">
                     <div class="sep-line">
                     </div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Niche based short video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat2.webp" alt="Create Video from Text" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">
                        Create Video from Text
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat3.webp" alt="Create Video Using Video Clips" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video Using Video Clips</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat4.webp" alt="Create Video from Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video from Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat5.webp" alt="Create Boomerang Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Boomerang Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat6.webp" alt="Create Podcasts" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Podcasts</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat7.webp" alt="50+ Short Video Templates" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">50+ Short Video Templates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat8.webp" alt="Add 25+ Vector Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add 25+ Vector Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat9.webp" alt="Add Sound Waves in Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Sound Waves in Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat10.webp" alt="Add Voiceover to Any Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Voiceover to Any Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat11.webp" alt="Add Background Music to Any video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Background Music to Any video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat12.webp" alt="Create Voiceover in 150+ Voices in 30+ Languages" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Voiceover in 150+ Voices in 30+ Languages</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat13.webp" alt="Simple Video Editor" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Simple Video Editor</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat14.webp" alt="Add Logo &amp; Watermark" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Logo &amp; Watermark</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat15.webp" alt="Free Stock Images &amp; Videos" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Free Stock Images &amp; Videos</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat16.webp" alt="Multiple Video Quality (HD, 720p, 1080p, etc)" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Multiple Video Quality (HD, 720p, 1080p, etc)</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat17.webp" alt="Inbuilt Music Library" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Inbuilt Music Library</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat18.webp" alt="Built-in Biz Drive to Store &amp; Share Media" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Built-in Biz Drive to Store &amp; Share Media</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat19.webp" alt="Post Videos on YouTube" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Post Videos on YouTube</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat20.webp" alt=" 100+ Social Sharing Platforms" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500"> 100+ Social Sharing Platforms</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat21.webp" alt="Drive Unlimited Viral Traffic" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Drive Unlimited Viral Traffic</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat22.webp" alt="Capture Unlimited Leads" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Capture Unlimited Leads</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat23.webp" alt="100% Cloud-Based Software" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat24.webp" alt="100% Newbie Friendly" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Newbie Friendly</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Ground-Breaking Section Start -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "goofyShort" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->


      <!-- Billion Section Start-->
      <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels & Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels & Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA & 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block" alt="Snapchat Users">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels & Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Billion Section End -->

      <section class="prob-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-32 w700 lh140 text-center black-clr2">
                     Now You’re Probably Wondering “Can Anyone Make a Full-Time Income With Short Videos?”                                  
                  </div>
                  <div class="f-20 f-md-32 w400 lh140 text-center mt15">
                  <span class="w700">Answer Is - Of Course, You Can!</span>   
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr2">
                     See How People Are Making Crazy <br class="d-none d-md-block">
                     Money Using the POWER Of Reels & Short Videos
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/ryan.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700">Here’s “Ryan’s World” </span>– A 10-Year-Old Kid that has over <br class="d-none d-md-block"> 31.9M Subscribers with a whooping <span class="w700">Net Worth of $32M</span><br class="d-none d-md-block"> and growing… As he is now capitalizing on YouTube Shorts. <br><br>
                              Yes! You heard me right... He is just a 10-year-kid but making BIG.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/huddakattan.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w700">“HuddaKattan” </span>is an American makeup artist, blogger & entrepreneur, Founder of cosmetic lines Huda Beauty on Instagram has over 51.8M Followers. She is using Instagram to interatct with here followers on daily basis.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/khabane-lame.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                           <span class="w700">“Khabane Lame”</span> is an Italian born social personality started uploading short video on dancing on Tiktok after laid off during Covid 19 and today he is most followed creator with 142.4M followers.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/dj-khaled.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700"> DJ Khaled known as  ‘King of Snapchat' </span> <br><br>
                              He is an American DJ, record executive, record producer and rapper. Now-a-days He teaches his audience by creating Reels that how anyone can get succeed on social media.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md100">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w900 white-clr button-shape">
                        Impressive right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12 f-md-28 f-20 lh160 w400 text-center">
                  So, it can be safely stated that…<br>
                  <span class="f-22 f-md-32 w700 red-clr">Reels & Shorts Are The BIGGEST, Traffic and Income<br class="d-none d-md-block"> Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Affiliate Section Start -->
      <div class="affiliate-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr2">
                  <span class="w700">The SECRET Is: </span><br><br>When you can combine selling top products, Reels & Shorts, and free viral traffic to make sales & profits, you’re the fastest way possible.
               </div>
               <div class="col-12 f-18 f-md-20 w500 lh140 black-clr2 mt20 mt-md50">
                  <img src="assets/images/fb-viral.webp" class="img-fluid d-block mx-auto" alt="Viral">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-40 f-24 w700 lh140 text-capitalize text-center black-clr2">
                     Checkout Free Traffic & Commissions <br class="d-none -md-block">
                     We’re Getting By Following This Proven System…
                  </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                  <img src="assets/images/proof-02.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section End -->

      <!-----Option Section Start------>
      <div class="option-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w700 lh140 black-clr2 text-center">That’s A Real Deal But…</div>
                  <div class="f-28 f-md-65 w700 lh140 red-clr1 text-center">Here’s The Big Problem</div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Creating Content Daily is Painful & Time Consuming!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                          <ul class="problem-list">
                           <li> You need to research, plan, and write content daily. And staying 
                           up-to-date with the latest niche trends needs lots of effort </li>

                           <li>You Need To Be On Camera. This can be a major blocker in the case 
                           you are shy or introverted and have a fear that people would judge 
                           and laugh at your videos.</li>                           
                          </ul>                        
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt1.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Buying Expensive Equipment Can Leave Your Back Accounts Dry                        
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           To even get started with the first video, you need expensive equipment, like a<br><br>
                           <ul class="problem-list pl0">
                              <li>Nice camera</li>
                              <li>Microphone, and</li>
                              <li>Video-audio editing software That would cost you THOUSANDS of dollars.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>               
            </div>
            <div class="row mt-md70 mt20">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           You Need To Learn Complex Editing Tools
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li>You need to learn COMPLEX video & audio editing skills. 
                              Most software are complex and difficult to learn, especially 
                              if you are a non-technical guy like us.
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt3.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Even If You Outsource Any of the Above Tasks!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li> 	
                                 Firstly, it can cost you 1000’s Dollars even for a 
                                 small project.                                  
                              </li>
                              <li> 	
                                 Finding a well skilled Freelancer who could handle 
                                 all your task is a big headache. 
                              </li>
                              <li> 	
                                 Even then you have to work for countless hours 
                                 finding ideas and explaining your requirement to them 
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt4.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>              
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 f-md-22 f-20 w400 lh140 black-clr2">
                  <span class="w700"> Problem does not end here, after all these efforts, still there is no security that you will get the desired results.</span> <br><br>
                  There’re tons of areas where marketers can face challenges that we had faced in our journey before creating this masterpiece
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="any-shape text-center">
                     <div class="f-md-45 f-28 lh140 orange-clr w700 text-uppercase">
                        BUT NOT ANYMORE!
                     </div>
                     <div class="f-18 lh140 white-clr w400 mt15">
                        Imagine if you had an ultimate ground-breaking beginner friendly technology that’s custom <br class="d-none d-md-block"> created keeping every marketer in mind, and that creates Short Videos for YouTube without<br class="d-none d-md-block"> having to spend money on costly equipment, without creating much content, and without <br class="d-none d-md-block">having to be on camera at all!          
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-----Option Section End------>

      <!-- Proudly Section -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w700 text-center orange-clr lh140 text-capitalize">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                  viewBox="0 0 627.01 153.63" style="max-height: 100px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: none;
                        }

                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-3 {
                           clip-path: url(#clip-path);
                        }

                        .cls-4 {
                           isolation: isolate;
                        }

                        .cls-5 {
                           fill: url(#linear-gradient);
                        }

                        .cls-6 {
                           fill: url(#linear-gradient-2);
                        }

                        .cls-7 {
                           clip-path: url(#clip-path-2);
                        }

                        .cls-8 {
                           clip-path: url(#clip-path-3);
                        }

                        .cls-9 {
                           clip-path: url(#clip-path-4);
                        }

                        .cls-10 {
                           fill: url(#linear-gradient-3);
                        }
                     </style>
                     <clipPath id="clip-path">
                        <path class="cls-1"
                           d="M190.85,44.64A21.3,21.3,0,0,1,198.51,46a23.09,23.09,0,0,1,6.19,3.5,22,22,0,0,1,4.61,5.07,24.06,24.06,0,0,1,2.89,6l-10.47,5.36a9.28,9.28,0,0,0-.7-2.43,11.82,11.82,0,0,0-5.2-5.64,10.21,10.21,0,0,0-5-1.12A14.81,14.81,0,0,0,184.5,58a14.47,14.47,0,0,0-5,3.88A18.36,18.36,0,0,0,176.2,68a26.23,26.23,0,0,0-1.2,8.3,31.07,31.07,0,0,0,.91,7.38,19.73,19.73,0,0,0,2.89,6.51,15.46,15.46,0,0,0,5,4.62,14.12,14.12,0,0,0,7.25,1.77,15.54,15.54,0,0,0,4.29-.54,15.73,15.73,0,0,0,3.38-1.36V83.63H186.23V72.92h25.14V107.3H198.76v-2.23a15.7,15.7,0,0,1-4.95,2.1,23.84,23.84,0,0,1-5.52.62,23.09,23.09,0,0,1-9.4-2.06,25.54,25.54,0,0,1-8.45-6.06,31.7,31.7,0,0,1-6.14-9.89,35.68,35.68,0,0,1-2.39-13.48,36.42,36.42,0,0,1,2.14-12.66,29.76,29.76,0,0,1,6-10,27.84,27.84,0,0,1,20.78-9Z" />
                     </clipPath>
                     <linearGradient id="linear-gradient" x1="216.73" y1="71.7" x2="260.14" y2="71.7"
                        gradientTransform="matrix(1, 0, 0, -1, 0, 156)" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#0568af" />
                        <stop offset="1" stop-color="#49c0ef" />
                     </linearGradient>
                     <linearGradient id="linear-gradient-2" x1="263.48" y1="71.71" x2="306.93" y2="71.71"
                        xlink:href="#linear-gradient" />
                     <clipPath id="clip-path-2">
                        <path class="cls-1"
                           d="M309,71.76V61.87h5.69V57.33a21.75,21.75,0,0,1,1.94-9.93,15.41,15.41,0,0,1,11.17-8.53,35.58,35.58,0,0,1,6.51-.66,12.91,12.91,0,0,1,2.06.16c.61.11,1.27.25,2,.41a6.28,6.28,0,0,1,2,.83l-2.14,11.21-1.61-.37a7.33,7.33,0,0,0-1.61-.21,9.19,9.19,0,0,0-4.49.95A6,6,0,0,0,328,53.75a10.93,10.93,0,0,0-1.07,3.67,36.76,36.76,0,0,0-.24,4.45H337.9v9.89H326.64l.08,35.45h-12l-.08-35.45Z" />
                     </clipPath>
                     <clipPath id="clip-path-3">
                        <path class="cls-1"
                           d="M353.35,61.7l11,29.44L375,61.7h12q-4.69,12.38-8.49,22.1c-1.05,2.8-2.09,5.54-3.14,8.2s-2,5.07-2.8,7.22-1.48,3.89-2,5.23-.77,2.08-.82,2.19a42.63,42.63,0,0,1-4.87,9.85,23.54,23.54,0,0,1-5,5.4,12.93,12.93,0,0,1-4.65,2.31,17.79,17.79,0,0,1-3.84.49h-2.14a20.28,20.28,0,0,1-3.3-.33c-1.27-.22-2.45-.47-3.55-.74l3.6-11.79a8.28,8.28,0,0,0,4.12,1.07,6.48,6.48,0,0,0,5.07-2,18.51,18.51,0,0,0,3.42-4.94L341.14,61.7Z" />
                     </clipPath>
                     <clipPath id="clip-path-4">
                        <polygon class="cls-1" points="63.23 70.74 46.44 94.51 33.66 72.48 63.23 70.74" />
                     </clipPath>
                     <linearGradient id="linear-gradient-3" x1="0" y1="79.18" x2="141.06" y2="79.18"
                        xlink:href="#linear-gradient" />
                  </defs>
                  <title>goofyshotrs Logo White</title>
                  <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g id="Layer_1-2-2" data-name="Layer 1-2">
                           <g id="Layer_1-2-2-2" data-name="Layer 1-2-2">
                              <path class="cls-2"
                                 d="M395.58,89.55,403,88.9a16.27,16.27,0,0,0,2.43,7.27,13.31,13.31,0,0,0,5.94,4.59,22.54,22.54,0,0,0,9.06,1.75,21.64,21.64,0,0,0,7.89-1.33,10.82,10.82,0,0,0,5.09-3.64A8.4,8.4,0,0,0,435,92.49a7.6,7.6,0,0,0-1.61-4.85,11.94,11.94,0,0,0-5.31-3.49,108.77,108.77,0,0,0-10.51-2.87c-5.42-1.31-9.22-2.53-11.39-3.69a15.92,15.92,0,0,1-6.3-5.49,13.54,13.54,0,0,1-2.07-7.35,15,15,0,0,1,2.53-8.35,15.71,15.71,0,0,1,7.41-5.9,28.24,28.24,0,0,1,10.83-2,29.57,29.57,0,0,1,11.57,2.11,16.69,16.69,0,0,1,7.71,6.22,17.79,17.79,0,0,1,2.9,9.3l-7.49.56q-.6-5.59-4.08-8.45c-2.33-1.91-5.75-2.86-10.29-2.86q-7.08,0-10.33,2.6a7.78,7.78,0,0,0-3.24,6.26,6.7,6.7,0,0,0,2.3,5.23q2.25,2.06,11.77,4.21t13.07,3.76a18,18,0,0,1,7.61,6,14.7,14.7,0,0,1,2.45,8.39,16.13,16.13,0,0,1-2.7,8.88,17.67,17.67,0,0,1-7.75,6.48,26.84,26.84,0,0,1-11.37,2.32,33.82,33.82,0,0,1-13.42-2.34,18.77,18.77,0,0,1-8.5-7A20,20,0,0,1,395.58,89.55Z" />
                              <path class="cls-2"
                                 d="M452.3,108.51v-59h7.25V70.67a16.17,16.17,0,0,1,12.8-5.88,17.25,17.25,0,0,1,8.25,1.87,11,11,0,0,1,5,5.18c1,2.2,1.51,5.39,1.51,9.58v27.09h-7.25V81.42c0-3.63-.78-6.26-2.35-7.91A8.76,8.76,0,0,0,470.86,71a11.86,11.86,0,0,0-6.06,1.67,9.39,9.39,0,0,0-4,4.53,20.79,20.79,0,0,0-1.21,7.89v23.39Z" />
                              <path class="cls-2"
                                 d="M529.92,70.57a19.19,19.19,0,0,0-14.41-5.78,19.91,19.91,0,0,0-13.45,4.75q-6.6,5.73-6.6,17.59,0,10.89,5.54,16.65t14.51,5.76a20.93,20.93,0,0,0,10.32-2.62A17.46,17.46,0,0,0,533,99.57q2.47-4.75,2.48-13Q535.53,76.36,529.92,70.57Zm-5.43,28.86a10.77,10.77,0,0,1-1.17,1.17,11.93,11.93,0,0,1-16.83-1.17q-3.58-4.08-3.58-12.3t3.56-12.22a10.61,10.61,0,0,1,1.06-1.06,12,12,0,0,1,16.94,1.06h0q3.6,4.07,3.6,12Q528.05,95.32,524.49,99.43Z" />
                              <path class="cls-2"
                                 d="M543.93,108.51V65.76h6.52v6.48c1.67-3,3.2-5,4.61-6a8.05,8.05,0,0,1,4.65-1.45,14.13,14.13,0,0,1,7.45,2.34l-2.5,6.72a10.28,10.28,0,0,0-5.31-1.57,6.93,6.93,0,0,0-4.27,1.43,7.66,7.66,0,0,0-2.69,4,28.15,28.15,0,0,0-1.21,8.46v22.38Z" />
                              <path class="cls-2"
                                 d="M587.29,102l1,6.4a26.53,26.53,0,0,1-5.47.64,12.45,12.45,0,0,1-6.12-1.24,7,7,0,0,1-3.06-3.28c-.59-1.33-.88-4.19-.88-8.56V71.39h-5.32V65.76h5.32V55.17l7.2-4.35V65.76h7.29v5.63H580v25a12.83,12.83,0,0,0,.38,4,3,3,0,0,0,1.25,1.41,4.85,4.85,0,0,0,2.48.52A22.54,22.54,0,0,0,587.29,102Z" />
                              <path class="cls-2"
                                 d="M591.48,95.75l7.16-1.13a10,10,0,0,0,3.36,6.6q2.76,2.3,7.71,2.3t7.41-2a6,6,0,0,0,2.41-4.77,4.42,4.42,0,0,0-2.13-3.87q-1.49-1-7.41-2.46a68.23,68.23,0,0,1-11-3.48,10.53,10.53,0,0,1-6.26-9.8A11.08,11.08,0,0,1,594,71.82a11.69,11.69,0,0,1,3.56-4,15.18,15.18,0,0,1,4.61-2.11,22.06,22.06,0,0,1,6.26-.87,24.64,24.64,0,0,1,8.84,1.45,12,12,0,0,1,5.62,3.92,15.43,15.43,0,0,1,2.49,6.63l-7.08,1a7.79,7.79,0,0,0-2.8-5.15,10.28,10.28,0,0,0-6.5-1.94q-5,0-7.13,1.65a4.81,4.81,0,0,0-2.13,3.86,4,4,0,0,0,.88,2.54,6.43,6.43,0,0,0,2.78,1.93c.73.27,2.87.88,6.4,1.85A86.73,86.73,0,0,1,620.48,86a10.78,10.78,0,0,1,4.77,3.8A10.65,10.65,0,0,1,627,96a12.12,12.12,0,0,1-2.11,6.82,13.67,13.67,0,0,1-6.1,5,22.06,22.06,0,0,1-9,1.76q-8.32,0-12.7-3.47C594.14,103.71,592.28,100.28,591.48,95.75Z" />
                              <g class="cls-3">
                                 <image class="cls-4" width="51" height="65" transform="translate(161.49 43.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAABCCAYAAAAYA/U3AAAACXBIWXMAAAsSAAALEgHS3X78AAAGVUlEQVRoQ91aSXo1Jwws8Zx9bpTr5HbZ5XrKAjQgicn5vUjKXzc0CNBE9fBMzIz/E+i3P//i9mlon4bPp6F9Pvh8yfXH2r8+2t8aobUGkvLTy9YINErf3z4NIICI+tHI6rtrAP3Ux2JcUz9N7USEv//4nVoy8T+O2iAeh7sEMzi2BXnfFhOZ5KBQp1Afx3dRGiTKMHNQtlLZlQwzfIzlaNrK6p03dgiyhUGzBDuLfIQ0j4m6R+MekOOnsnph9Jd2isKTl317sEjGhQYGgcwHIHcu0btTfc46f1WTg6CpzGAhRA8rkw22iuDKWaOFy849HuQr0aAhW+GiwSNMR1Io+qV8IgVE+NVW9Y5tgntSGC2+ty4DKWzTdFF/CFISLnNoXqdbFLfQNSm0f8HBOyys/nlSkMBFURuSSCG74FeTwieTArtKVnZE+TvPiVwZVKOafU0KU1KzRSrKikV+77h+uXohhcpB53rHmhQYSgqzakkstzHUAa+PMc8xDQPuSEEp23okf2lHCpW77529x0L+iRQoujtNKqTAANNgBKpFbchECgR3rfgGKZB4W58OhBAGKbQ5mOwq1d5/dbjgJUWrNVRLHmfNf2lhPYWhPJeOFGZy6LglhRp+rlW944EUvhGJ+jlmDT7MVyEMeCQFw5EUiCB0P2Hj7CdjRLjcQyOjdqTQGuWU4KmAOoPTax14dbDVVTChJoUq/Bek0D96LB3ttaslMhaT7fdRhbxGTQpQX4PF20uFrZxJYZbeEYHfarUbfOvGUTh8JGGzbjTUor6Uq37PenJ3STpHhDFnUnDu9u2RFKbvarTx98LZnCoHsOngsSEF1nZqzdgsTOoKeGdEvXh18I0N5GpGCtW4BSk0/SJKrY+sFNSKdwhwTp1iMtlTpyw9TR1IQZSJRFC5keEkTXhhFcmfpKWv7xlhgzxg+5EkPins4M2zfeTdzZvD1yr4nr02Z1JQi2bLalJow5AQNY9Ct0nqxnsDhJyfm9eHboD8ikD+ZiGQcZCuqmaiJXjIbfdOQQrIawCRFPQ7QhuvDKT3oqjQtHfUCb08ejn0+xvtLyMFAE46KLVRkouycsGaFEzmyI4JecDX3OXehyTtymErMNrYRz01bpLOnhPXwfHJVSWa4UgK5rbs8UwKolw9JjVxEZV775WksH1SkP00P844yDi5dMpUelZHKSyQJWXtwB7VsJIU+itDU+OqwRUp9MsLFzsRkrWR9IVfIo5boSSF/srgDNkoOa81zux7OhIpBELII1bwUnnEkhTAUFJwAjWYAZZ71dhTKb/DBAwABHZid6Swx5IU2C5LWyZSePmVIfooTn4Ik3fUEynY3pmTSqDK6Lj5Q0qlZzyWwoJICphFq2EpQrJYG+zWyFIpS5rBOvkDKSghkEuoy0CvkFiujeOWFCBSGjIRn8ckUggi5QocynSRR83vQyNtNPU8ihX19YLhHmB3pOCPURNfeLGEk0MNJSno/tk8+hCMDMqb7go8FW8IjroihXR/XKzMblxveCCFqrPC0FcdFqJZDSs/Y8l/VHVKHhMmZ0j0QhSP+62DcE8KdzN2OFIYhEAEUxP1fvJgS01tKuRnUrCxUpRLHLKkgosQ2wZNecF5UmeIRnO4vQxowHP6lQJZOH0KBvvorGfXJwSiyywbJoS9docx4FukAMxuK4LTm3n0jd4HUkhYGSj6vpOCWST/Zqk3wCUpwIbCTX7hfrmxelKwzmLAeUrFIIVhhP6OajMsSYFh96lbUlBCmL3woO8RRgpDczEgfp3z8Ff6W9INKejc7ojWlMuudMmumEihTz5M2SzU9R5PCNebfDhMqg/QG+srKQASKB511u6I6oZ6QwolVh3GBVrZx2cYxOgfReRJeyIFWpOC3zvesB3807YnBhNYDLxEA6D/RF6oV+o3pY2L6CwwQ54UYqy2kfNCl+gRkpuppA1bSmG0T+D+EUVurgikgAUp9EwwIybHmFCBVaJlYcdy0s21rLtWQgAwabgFl9UVPLv715MrUmALizNM2vdgVzuRwhJFJwOZFDZ1QZO01lcGTZ+cRjrRiEpp8MoJPN9cf44UGMXvqK621I9NlKcdV7rO0mZeRY4tjgIGd2MVr8u1Q5E/+isD3ZECyx8HI4q5y/ol2hyB1WzzzNcfFQcutuIBd3oBwD8WI0wZdPg0+QAAAABJRU5ErkJggg==" />
                              </g>
                              <path class="cls-5"
                                 d="M216.73,84.29a27.34,27.34,0,0,1,1.61-9.48,21.78,21.78,0,0,1,4.54-7.5,21.24,21.24,0,0,1,6.88-4.91A21.7,21.7,0,0,1,254,67.31a22.09,22.09,0,0,1,4.54,7.5,27.33,27.33,0,0,1,1.6,9.48,26.88,26.88,0,0,1-1.6,9.44,22.56,22.56,0,0,1-4.54,7.51,20.7,20.7,0,0,1-6.92,4.94,21.18,21.18,0,0,1-8.7,1.78,20.75,20.75,0,0,1-15.54-6.72,22.39,22.39,0,0,1-4.54-7.51A27.12,27.12,0,0,1,216.73,84.29ZM238.42,97A8,8,0,0,0,243,95.75a10.28,10.28,0,0,0,3-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24A12.38,12.38,0,0,0,246,75.93a10.31,10.31,0,0,0-3-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1A13.75,13.75,0,0,0,229.14,80a17.51,17.51,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,238.42,97Z" />
                              <path class="cls-6"
                                 d="M263.48,84.29a27.34,27.34,0,0,1,1.61-9.48,21.91,21.91,0,0,1,4.53-7.5,21.39,21.39,0,0,1,6.89-4.91,21.3,21.3,0,0,1,28.81,12.41,27.34,27.34,0,0,1,1.61,9.48,26.85,26.85,0,0,1-1.61,9.44,22.37,22.37,0,0,1-4.53,7.51,20.64,20.64,0,0,1-6.93,4.94,21.18,21.18,0,0,1-8.7,1.78,20.82,20.82,0,0,1-15.54-6.72,22.37,22.37,0,0,1-4.53-7.51A26.85,26.85,0,0,1,263.48,84.29ZM285.16,97a8,8,0,0,0,4.58-1.24,10.57,10.57,0,0,0,3.05-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24,12.38,12.38,0,0,0-1.69-4.12,10.6,10.6,0,0,0-3.05-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1,13.57,13.57,0,0,0-1.7,4.06,17.27,17.27,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,285.16,97Z" />
                              <g class="cls-7">
                                 <image class="cls-4" width="32" height="70" transform="translate(308.49 37.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABHCAYAAACJSS4oAAAACXBIWXMAAAsSAAALEgHS3X78AAAE+klEQVRoQ8WYXZIrNwiFD3KygOwoy8nu8pjdDXmQEIcfye2ZW3VxyW7htgQfR3jGoqr43SZ//vOvjjEwXnG8XgPZ/7Lrwb6X3zPSfeQvvuX/7++/ZBzDawCpVr9C5xv7TZ3PPl33ofUDQB+Epr2axXxVX9xjWcFoGl/zBvYBpyAEkDSHACL70i/ML4CIQNZEujHWWPMx5vbPSJjPsl7zTWLTcByqCk00sGhs31r6TCKgQMx4zd3n2RUKo5mbb0VxJpFRcM2LzzPPNbfMkXwQwFh8i4T7at1bEpmGSKj3D0nUmhcSqgDrQJHqegiiZEw+EdhTT6HJetIYkHUadjY3Teh+ij7VeMHqP1Jg3VifWGtantcjqjS3icWgfAOVigdsc3WBTlJYwpx2bVZC830kwWVBEOwcBzGuzZ2mb/cZCfJr9m0CnRgXBXEK3JI/IiHkryTeHE2kMj4lUXxMiEkokEXKOmAKn5Ggm9hnNJxCcyQzDTQUHpHIKJQWCr58JJMWvtaJGInCOxIbW+PbC+GSefCTaJu9gFOzakjshUgULYV0KlgLucJmdxL0KScxL/zsH2gMgUBi8Ae7t236oMIoNHU/aKG+TyQfCZPvownF4dlp9OnXfGVSoLI8EuapWW1/EqmEQceS/qDZJ+7HJMyfFgwUlI/lHC2FRySkkpA0tq8cTUQ9oCQf7NqsMgkiu0lw9vFYsh5K8sGuR5RJZAr3Fs2nArW8yb7VrHShyMc0aIEp5KSSfatZ3Vu0BYVSxpPdSdAHnYI6hbZFe0BZ0Cd7T8JERRSQaQQtpKwfkPijc6o9JVF540ntWRQ6GlKqUMJ5isP+EfBhi9iDNgYaMe5SjErK5sveCLMWcD+EfN3Y35Yx4HBEl/0SEi2FtkUTBdbVPYiGhNHIJMKxnCXgoGKwlBsaEhKCSGbJ05wg0agtOn+HXEmsDe5t2z4l7JsbjbZFr2tECkbiZM+aVco+aKETKxIFOesBeNqsBsImY63aayFlnUvb2HsSmcAaWQMgHewyAqGUJytBCCIJ1oFpAUgUVtGVBlvnYytte0uho7BaNIb7HLeXxUsT63CK496sUgls8VaMksti87mDSewSRJbyYZNQCqyYU1nAQWOnf6nGhUSigVWOqY3RBmlCChQst2MIBxI31KYRrjtrYaoDQVeI0ijWknikhVED3AlgZf7LSNi3I6QElcs1/Snr5ySiJbrQL5xbNJGY1ylrg9RttOzarOaYK7Q/lC8agUJDoviSlSAUiYJpof0TP+vBg9/W+ZINuzG0iQHSAqoWaMxose7z4Nk6H9ts2+pjL9S0aB9MioJci0zB0rr+0lo8ourDFjbkw/APgf8S15QFMh9E97MjumtbS8EEPMt6RCeJlTkTrntva0moavgrelNYp0VEgFOzgvywWYVFsYPhRENjQiVBMpj2MQladGuhOZpZC+WY8g6PSfAiWwsx+5KxjSDo/eL2mAQv/qX7q9q14DQKhSBotCSKL1kgYZvk3x1y1sUHotBk3fnYAomtBfvn5lD74ltFp6oG63xsm4TIABD/r8wDGsWrKUW6JVjnY9tt2xa0zfaPH5uxVtzrfg4QXiD4xYNyKKgUNuhoegkYbS0TVmlWgfBRs5JVsE2hlMFRO1rdwWt4M2W97n9Povn9MR5NFp1PhGi4L2X9hITqWpgi5poXH2HRnN8p4xsGAP8DLvKSAi070PIAAAAASUVORK5CYII=" />
                              </g>
                              <g class="cls-8">
                                 <image class="cls-4" width="47" height="64" transform="translate(340.49 60.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAABBCAYAAACXfCfjAAAACXBIWXMAAAsSAAALEgHS3X78AAAGFUlEQVRoQ9VaW7IrNwhsXMkCsqMsJ7vLZ3Zn8iEBzUMe+aZuJaFKZiSBANHgOacsqor/M8mvf/ypIgJ5CV6v1+YCeb0K3/tyKfeSInuh8+XZf/3+m7yeIvyv0/cByB4XJCKAALJ1lpq4evC9Y2fLnjQ7sSCynu8DUB7qS8F1LSugqoUvncV0DxQez1CFvhV41/08gG8CGG693mRk55wi2bebs8J5QWyYjGQJu33gmwCA4mnfDAPYhotTie9ncnRNhaRiNp95DGB20NlDIHWaasEluBZoxeQ8S+J6DEujXzCSojmiCqhEDYhAoRDIllaoCmRjWVSgqsQBVQHEasW4zDXz1qgtAHgB8trRkWv3EErxcJpLQvatdSIdrgHPCp8pcY7LZZtG9wEA4eBhMwVkRunCzIU4wrwPR+E64XBvvEHf14A9mpef5HyJHYTrVremWuB2G7CLow8BkIQv7eLReA6Urk/dch3TSsbn3j+Ot2aoETe6h1BSFPqMmSelFBpL+WzLuWP5lH1WzVGn+wCMyg2cRADgq1cJlhMUyAR0LiHExNBBQAITdGh/hMkTdAB9K95vzZmcxqaLACQp7BXiEhOXC4UMCbvloRPRVKRm6dyHLgLYdHAwZlEDAQWWG24hfR/sU3ytujrT921UaHySo2muhVqu/JSh5vB0nuAP4KqNWg1ELTjGQUZSwSnJcVtcGD+1TQAU1PrM835lFxASunFfIf6UGZLiLLja3vfWe5FVoosAiEYHh9vacvlVIr/tuKDJmQQFeUPf14A9Ho0Ma94Os4PW78d2qob+9fnPagCYa6AW2lMNqK4aoNGgVPLUsotMFxCSxNYjG6EeMmamOEa3723WM+Ti13QRANFHI+WWzEExp3MoDJ2WNVTofA0hJoaO8WKEoBOOhYNT+ww6geO0lukiAL+6gylKf82CrXgWTDa+baNjkXzSn881ughgkzsJ4GTEnRE0SyYlH76JBxtP9H0bFRqf5BCBeEDWhVJn4lpgeE61cF0DLGY1YFhHGNvFwTXQayFa5gidbWWGzpkbXUAorMyHCGWFbt85ZaFps65tVRdPa4suAtg0GMkuiW9V6Dz/ZUYOfgyk0/c1YI8CeFcZ5RZveD/WAAZ4/uwacMzbPtfAGu0VYQeeODhsKXNe7fd1ASHxG6+HuKmWEdnTNc9OJsDsDTrjS/oOQuwoyWXHyntO0421eiE+kbZ6JA+gHnbm281tqLVFybgfoQZgtV/eh0N1CRQYH8gDyEXCRZONTF9A0evh/X7ASV0IuZKZQXBYW/RVDcSK2E7MOBsmV9to0uYjJdjZ15F+sAZssJzkjIxjyiCog+1M4xMiMj230ZMRgw05mG9XCkfO0r6ELo8rbvQMocEIb4RDXMgSnOUbCWXzQe5AzwEMRloNCIdWjKWAWBtdJwVyR4c22o1E+yRnVs9MUBqh5hxwPFNng/P93GhaW/TcRlvx0eCWaQHtWw7q9+1T1m3yT2uLniFk2AbnZc/E8pV3Wy24Rnw2aoHc0X0bpdvKBYsU5JMuS4Qk6XwZxMOrxFwLDVb2j6qx3XItAAme+Nk14HjPNdCCtNv1MV3jYa1B51Z30ecaKPAQN0ichFPWBKV9RsCdpkDuyH9q0G61rsv6aQHeAQKFAi9AVQIizvknBAKIYuUufnawfpbAHFDhny9g86Ub86ACId1Q7NB5b2we/87d194CL/e+xEoTqPsX3KhAiIxJ/nYVcxrVKYKOOyXdUl+INeHtD3IDXdfA8Zt4VEKX9Z38bZJ0zn4e6eFVQqgTDeNN7VGtfU5tFCiVs2jvB4d9FJrWFs01QE4CCSSkKp4hh4BD5xIG5QvuKHcPoe3odsZhUhwdC5PCS7VAcizd6OzjRzpDyIJo+7KdQwSZqCxQ8P2soiNp8YpWAGQEIo7veE3OsGprwFALKLzXgGKoAf2RGmAj/hudqVdnSHi2bOJjusbDWrv1W91FrQZkexNOmvdkTGI36fKqINpokj440wK5I4eQyGeYdGjwOr6AzoZDg45t/RCEMnT6TQxXkzJiIl+kP+n9OL28ZiROkz3vtWD7DIZ/91Xib9ELyBRLpQWwAAAAAElFTkSuQmCC" />
                              </g>
                              <g class="cls-9">
                                 <image class="cls-4" width="30" height="25" transform="translate(33.49 69.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAaCAYAAABPY4eKAAAACXBIWXMAAAsSAAALEgHS3X78AAABzUlEQVRIS8VWW5IcIQyz3LUHyI1ynNxuP/d2o/0Ag2ygpyuPiqqgPeZhkDAMSNr/Aj5+fdIvN7/crv716xI7/Ffp43Z13++M/fr5A/5udf8SKTjFUjG41GuvPLb6WXo3uLQ9KDRj+4SP5CM/Y/wSfIvd7sM6McOxzuEZi6DBYYY5rgVHK0DYMADjN2CGVnV/Lj5sNwfMHQaHubfDBW8lrcyOO8/07H1zJmWFxkQ9OftVJM2zZjubiz9opfSLoAzNe5CxBGjwLc6ac9vG1Ba0w1Uia4H74IeaV5196NqCwODegnXdg4FdJgQeXjKUullB6Zx46qus3OGg+aptiJr9ujNt02XOuuKPNadZpz9LoXKZ6n2f5/mAAKrr1NblIO1YGQwsvho8oe44aLUxYc3fzIqOrXXGw4eFUs+FzLNwHnuHm4dlf+j0+kylyBWaN+00jWvwneaud/QMcr5gdtTec3A+7TSLv1jjmkyscGHlxFYeO0O8P3Av5smPPaf3KW4uGRb627M4n8+cjpqiU8aHmgMapAUNUFZY98WlXltPENp7KiUtudEs623iX32H0pHz/NWi1OtT83z0lV9q5T61zvDYEU10Mag03VbP38E3FCS7Ocyt6xMAAAAASUVORK5CYII=" />
                              </g>
                              <path class="cls-10"
                                 d="M138.92,91.49l-6.76,3.92-6.49-11.19,6.75-3.92-2.14-3.69L117.72,83.9l-33.08-57L98.25,19l-2.2-3.8-7.81,4.53L81.74,8.54,89.55,4,87.23,0,53.51,19.57a19.6,19.6,0,0,0-24,13.89v.11l-2,1.16A19.63,19.63,0,0,0,1.66,44.81,19.83,19.83,0,0,0,.12,50.56,19,19,0,0,0,0,52.69a19.64,19.64,0,0,0,38,7,20.11,20.11,0,0,0,1-3.88,19.9,19.9,0,0,0,9.42,2.44A19.56,19.56,0,0,0,61.74,53l.32.58a2.71,2.71,0,0,0,3.69,1,2.62,2.62,0,0,0,.89-.8L75,42l-1-1.77A5.3,5.3,0,0,1,76,33h0l.33-.18L103,80.32l-.33.18a5.28,5.28,0,0,1-7.19-2L81,79.33a2.72,2.72,0,0,0-2.53,2.9h0a2.85,2.85,0,0,0,.32,1.1l2.53,4.51L48,107.48a21.72,21.72,0,0,1-23.49-1L11.2,97.19,36,131.31l-6.44,3.74,2.14,3.7,5.7-3.31,6.5,11.19-5.7,3.3,2.14,3.7L141.06,95.18ZM28.41,56a9.37,9.37,0,0,1-8.78,6.08,9.27,9.27,0,0,1-3.3-.6A9.38,9.38,0,1,1,28.41,56Zm20-8.07a9.38,9.38,0,0,1-.55-18.75h.55a9.39,9.39,0,0,1,9.32,8.27,10.32,10.32,0,0,1,.06,1.11,9.38,9.38,0,0,1-9.35,9.36ZM76.27,26.68,70.72,29.9l-2.49-4.29-3.58-6.16,7.81-4.54,6.07,10.46Zm-21.13,112-5.89-10.14,2.26-1.32L57.06,124l2.31,4L63,134.18Zm19.63-11.4-3.58-6.16-2.31-4,7.82-4.53,2.3,4,3.58,6.17ZM94.4,115.93l-3.58-6.17-2.3-4,7.81-4.54,2.3,4,3.58,6.16Zm19.6-11.4-3.57-6.16-2.31-4,5.55-3.22L116,89.86,121.84,100Z" />
                           </g>
                        </g>
                     </g>
                  </g>
               </svg>
               </div>
               <div class="col-12 f-md-40 f-28 mt-md30 mt20 w700 text-center white-clr lh140">
                  A Push-Button Technology Creates Reels & Short Videos to Publish on YouTube, 
                  Instagram, TikTok, Facebook & Snapchat using Just One Keyword and Drive Viral Traffic to Any of Your Offers
                  
               </div>
               <!-- <div class="col-12 f-16 f-md-18 lh140 w500 text-center white-clr mt20">
                  It enabled you to create Engaging, Highly Professional Short Videos, <br class="d-none d-md-block">
                  Add Voiceover or Background Music with Zero Tech Hassles. 
               </div> -->
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Demo Section  -->
      <div class="demo-sec" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="white-clr f-md-45 f-28 w700 text-center">
                     Watch GoofyShort in Action
                  </div>
               </div>
               <div class="col-12 col-md-9 mx-auto mt-md40 mt20">
                     <div class="responsive-video">
                     <iframe src="https://goofyshort.dotcompal.com/video/embed/jwjoicqx4d" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen"
                        allowfullscreen></iframe>
                     </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                  Use Coupon Code <span class="red-clr w600">"GoofyShort"</span> for an Additional <span class="red-clr w600">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Section End -->

      

      <!-- Testimonials Section -->
      <div class="testimonial-section1">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr2">
                  Here's What REAL Users Have to <br class="d-none d-md-block"> Say About GoofyShort
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10 offset-md-2">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c4.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Lucas Rodriguez</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Easy To Use Software with Training Included</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              GoofyShort is next-level quality software to full fill all my YouTube marketing needs. Also, it is  <span class="w700">so easy to use, and the training included makes it even easier</span> to create highly engaging and professional videos & shorts in a few minutes. I'd say that this is a MUST HAVE technology for marketers.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="assets/images/c5.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Theodore Miller</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Revolutionary software comes at a one-time price</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              GoofyShort combines ease of use with  <span class="w700">super-powerful video creation technology.</span> I'm using it to create short videos for my YouTube channel that are well-suited to my audience.<br><br>
                              Trust me, it works like a breeze and the best part is, <span class="w700">that this revolutionary software comes at a one-time price.</span> It’s amazing! I am enjoying working with it. Great Job Ayush & Pranshu. 
                              
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c6.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Harper Garcia</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">I Can Make Money with Complete freedom</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              I have been in the Video Marketing arena for quite some time now, and I must say that GoofyShort will take the industry by storm.<br.<br>
                              The coolest part is that <span class="w700">it enables you to create video shorts in just a few minutes which usually takes me a long day to create even a single video</span> to boost engagement and traffic on my YouTube channel. Now, I can make money with Amazon/Affiliate offers, AdSense & my own offers with fewer efforts on video creation.<span class="w700"> Complete freedom is what it provides me.</span> Something to check on a serious note. 
                              
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

      <!-- <div class="feature-highlight" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w700 text-center balck-clr2 lh140">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes GoofyShort A CUT ABOVE The Rest  
               </div>
            </div>
         </div>
      </div> -->

      <!-- Features Section Star -->
      <div class="features-section-one">
         <div class="container">           
            <div class="row align-items-center">
               <div class="col-12 mb20 mb-md50">
                  <div class="f-md-45 f-28 w700 lh140 white-clr text-center">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes GoofyShort A CUT ABOVE The Rest 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Highly Engaging & Professional Reels & Short Videos 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & Shorts for YouTube, Instagram, TikTok, Facebook & SnapChat                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Shorts by One Keyword
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & short videos by entering Just one keyword and you will get number of short videos to choose or upload your own.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create 9:16 Vertical Video
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create highly professional & engaging 9:16 vertical video. Perfect for mobile viewing and guaranteed to entertain your audience.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Boomerang
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create boomerang short video loops that boomerang forward & reverse through the action.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <!-- <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f4.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create High-Quality Explanatory Whiteboard Video Shorts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create whiteboard video shorts by just adding your text and customizing font, color, and alignment. These videos are highly effective and informative.                    
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Create Videos by Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Find images for videos by searching images with keywords inside GoofyShort or upload your own images                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     50+ Background Templates
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easily create Reels & shorts by using 50+ background templates to make your video attractive &get more & more engagement.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     25+ Vector Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Create more engaging & attention grabbing Reels & Shorts by using 25+vector images. 
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="white-feature">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                  	100+ Stylish Fonts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Use 100+ different Stylish fonts to make your shorts more presentable
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Add Music Waves
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Add Music Waves run with audio of different color to your Short videos to make your video engaging.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <!-- <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f10.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Build Authority by Adding Your Branding
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Build your authority in the audience by adding your brand watermarks and logos in the video.                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Share Shorts Directly to YouTube
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can easily share the video on YouTube or can download it is to use wherever you want.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100+ Social Sharing Platforms
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can alsoshare these Reels & short videos t0  100+ social media platforms directly to get free viral traffic                            
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Completely Cloud-Based Software
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Fully Cloud-Based Software with Zero Tech Hassles & 24*7 Customer Support          
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100% Newbie Friendly
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easy and Intuitive to Use Software. Also comes with Step-by-Step Video Training & PDF
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     No Monthly Fees or Additional Charges
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="red-clr w600">"GoofyShort"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->


            <!-- Imagine Section Start -->
            <section class="imagine-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140 balck-clr2 order-md-2">
                  A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br>
                  <span class="w700">That’s EXACTLY what we’ve designed GoofyShort to do for you.</span>
                  <br><br>
                  So, if you want to build super engaging Short Videos for YouTube and other social media at the push of a button, and get viral traffic automatically and convert it into SALES & PROFITS, all from start to finish, then GoofyShort is made for you!
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                  <img src="assets/images/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </section>
      <!-- Imagine Section End -->

      <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                           <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              <ul class="noneed-listing pl0">
                                 <li>Create instant videos shorts for YouTube and other social media in just 3 clicks</li>
                                 <li>Drive tons of viral traffic from RED-HOT YouTube & other social media giants. </li>
                                 <li>Get the best results without getting into complex video creation & editing hassles</li>
                                 <li>Get more exposure for your offers in a hands-down manner</li>
                                 <li>Boost sales & profits without spending much</li>
                                 <li>Jump into the most trending video marketing strategy with YouTube Shorts.</li>
                              </ul>
                           </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/target-arrow.webp" class="img-fluid d-block mx-auto">
 
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                        <span class="w700">Seriously, having to do all video creation manually is expensive,</span> time-consuming and put frankly downright irritating, especially when you have to do this on a daily basis. <br><br>
                        That's exactly the reason so many people are afraid to even get started with YouTube Shorts and the same  <span class="w700">reason why out of all that DO get started, very few truly "make it".</span><br><br>
                        
                        We have done all the grunt work for you. So, you don’t need to worry at all
                        
 
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section Start -->

      <div class="need-marketing">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Who Can Benefit from GoofyShort?
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-20 f-md-22 w400 lh140 balck-clr2">
                       <span class="w700"> This is Simple.</span> It helps<br><br>
                     </div>
                 </div>
             
              <div class="col-12 mt0 mt-md20">
                     <div class="row flex-wrap">
                         <div class="col-12 col-md-5">
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li> <span class="w700">Marketers</span> to drive more traffic</li>
<li> <span class="w700">Affiliates,</span> make more commissions</li>
<li> <span class="w700">SEO marketers</span> get #1 ranking and traffic</li>
<li> <span class="w700">Social & Video Marketers</span> get free viral traffic</li>
<li> <span class="w700">Creators</span> can get more income handsfree without extra efforts in video creation.</li> 
                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-7 mt0 mt-md0">
                            <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li><span class="w700">Product sellers, </span> sell more with RED HOT traffic</li>
<li><span class="w700">List builders</span>  skyrocket their subscribers...</li>
<li><span class="w700">Freelancers and Agencies –</span>  Provide RED Hot service & make profits</li>
<li><span class="w700">Local Business Owners – </span> get more engagement & TRAFFIC</li>
<li>Weight Loss, Gaming, Real Estate, Health, Wealth –</span> <span class="w700">Any Kind of Business</span> </li>

 
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             
              <div class="col-12 mt20 mt-md70 text-center">
                     
                         <div class="f-22 f-md-32 w700 lh140 red-clr">
                           It Works Seamlessly for ANY NICHE… All at the push of a button. 
                         </div>
                 </div>
             
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                   <div class="f-20 f-md-26 w700 lh140 balck-clr2 text-center text-md-start">
                     GoofyShort Is Designed to Meet Every Marketer’s Need:
                         </div>
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2 mt20">
                      
                                 <ul class="noneed-listing pl0">
<li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
<li>Lazy people who want easy traffic & profits</li>
<li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
<li>People with products or services who want to kickstart online</li>                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/platfrom.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70 text-center">
                     <div class="look-shape">
                         <div class="f-22 f-md-30 w700 lh140 balck-clr2">
                           Look, it doesn't matter who you are or what you're doing.
                         </div>
                         <div class="f-20 f-md-22 w500 lh140 balck-clr2 mt10">
                           If you want to finally be successful online, make the money that you want and live in a way<br class="d-none d-md-block"> you dream of, GoofyShort is for you.
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Even BETTER: It’s Entirely Newbie Friendly
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              Whatever your level of technical expertise, this amazing software enables 
                              you to create engaging & highly professional video shorts for any niche in 
                              the 7-minute flat. It really doesn’t matter.
                              <br><br>
                              <span class="w700">The ease of use for GoofyShort is STUNNING,</span> allowing users to drive 
                              free viral traffic from YouTube, without ever having to turn their hair 
                              grey for complex video editing skills.
                              <br><br>
                              With GoofyShort, not only do you get the BEST possible system for the LOWEST 
                              price around, but you can also sleep safe knowing you CONTROL every aspect of your business, from building a stunning video marketing strategy to promoting affiliate products, to traffic generation, monetization, list building, and TON more - no need to ever outsource those tasks again!
                              
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/better.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section End -->
      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"GoofyShort"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 white-clr button-shape">
                        We’re not done Yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md30 f-20 f-md-22 w500 balck-clr2 lh140 text-center">
                  When You Grab Your GoofyShort Account Today, You’ll Also Get These <span class="w700">Fast Action Bonuses!</span>
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    DotcomPal- All in One Growth Platform for Entrepreneurs (That's PRICELESS)
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    All-in-one growth platform for Entrepreneurs & SMBs to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touchpoint... without any designer, tech team, or the usual hassles. 
<br><br>
 This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Video Training on Viral Marketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. Social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. 
<br><br>
                                    So, with this video training you will discover a shortcut to online viral marketing secrets. This will cover - The Two-Step Trick to Effective Viral Marketing, How Do You Find Hot Content? Maximize Niche Targeting for Your Curated Content, remember to protect yourself when sharing others’ content, how to share viral content on Facebook, how to share viral content on Twitter, Filter your content format to go viral on many platforms and much more. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 "> Modern Video Marketing
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    It's about time for you to learn the ins and outs of successful online video marketing! <br><br>

                                    Regardless of what you've heard, video marketing as a whole is not exactly a new phenomenon. Video marketing content is increasingly plugged into a larger marketing infrastructure.  
                                    <br><br>
                                    You have to wrap your mind around the fact that modern video marketing is both new and old. Depending on how you navigate these factors, you will either succeed or fail. Either your video is going to convert people into buyers or they're just going to sit there on YouTube getting zero views. 
                                    <br><br>
                                    So, with this video training you will discover the secrets of successful video marketing- The Modern and Effective Ways of Video Marketing, Modern Video Marketing Essentials, Types of Video Marketing and much more 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Youtube Authority  
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Since its launch in 2005, YouTube has come a long way. It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world.So, if you are isn’t already leveraging the power of YouTube there are some massive benefits that you’re missing out on. 
<br><br>
                                    With this video course you will: <br>
                                    <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                      
                                       <ul class="noneed-listing pl0">
<li>A Clear understanding on starting a YouTube channel.  </li>
<li>Determine your target audience.  </li>
<li>Learn about the different types of videos  </li>
<li>Discover how you can increase engagement  </li>
<li>Learn the different avenues for monetizing your YouTube channel </li>
<li>Learn about the different mistakes that you can make on your YouTube channel     </li>                           
       
                                       </ul>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-50 w700 balck-clr2 lh140 text-center ">
                     That’s A Total Value of <br class="d-none d-md-block "> <span class="f-28 f-md-50 red-clr"> $2285</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
<!-- License Section Start -->
<div class="license-section">
   <div class="container">
      <div class="row">

         <div class="col-12 text-center">
            <div class="w700 f-28 f-md-45 balck-clr2 text-center lh140">
               Also Get Our Free Commercial License When <br class="d-none d-md-block">
               You Get Access To GoofyShort Today!
            </div>
         </div>
      </div>
      <div class="row align-items-center mt20 mt-md70">
         <div class="col-md-6 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 balck-clr2">
               As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic, NEED YOUR SERVICE & would love pay you monthly for your services.<br><br>

               Build their branded & traffic generating video channels. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.<br><br>
               
              <span class="w700"> Note:</span> This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
            </div>
         </div>
         <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
            <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto" alt="Features">
         </div>
      </div>
   </div>
</div>
<!-- License Section End -->
  <!-- Guarantee Section Start -->
  <div class="riskfree-section ">
   <div class="container ">
      <div class="row align-items-center ">
         <div class="col-12 text-center mb20 mb-md40">
            <div class="f-24 f-md-36 w600 lh140 white-clr">
               We’re Backing Everything Up with An Iron Clad...
            </div>
            <div class="f-md-45 f-28 w700 lh140 white-clr">
               "30-Day Risk-Free Money Back Guarantee"
            </div>
         </div>
         <div class="col-md-7 col-12 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 white-clr mt15">
               I'm 100% confident that GoofyShort will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
<br><br>
               If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
               <br><br>
               Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
               <br><br>
               I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
            </div>
         </div>
         <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
            <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto ">
         </div>
      </div>
   </div>
</div>
<!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-20 f-md-24 w500 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-48 f-28 w700 lh140 text-center mt10 ">
                     Get GoofyShort For A Low One-Time-<br class="d-none d-md-block">
                     Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 balck-clr">
                     Use Discount Coupon <span class="red-clr w700">"GoofyShort"</span> for Instant <span class="red-clr w700">$3 OFF</span> on Commercial Plan
                 </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                  viewBox="0 0 627.01 153.63" style="max-height: 90px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: none;
                        }

                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-3 {
                           clip-path: url(#clip-path);
                        }

                        .cls-4 {
                           isolation: isolate;
                        }

                        .cls-5 {
                           fill: url(#linear-gradient);
                        }

                        .cls-6 {
                           fill: url(#linear-gradient-2);
                        }

                        .cls-7 {
                           clip-path: url(#clip-path-2);
                        }

                        .cls-8 {
                           clip-path: url(#clip-path-3);
                        }

                        .cls-9 {
                           clip-path: url(#clip-path-4);
                        }

                        .cls-10 {
                           fill: url(#linear-gradient-3);
                        }
                     </style>
                     <clipPath id="clip-path">
                        <path class="cls-1"
                           d="M190.85,44.64A21.3,21.3,0,0,1,198.51,46a23.09,23.09,0,0,1,6.19,3.5,22,22,0,0,1,4.61,5.07,24.06,24.06,0,0,1,2.89,6l-10.47,5.36a9.28,9.28,0,0,0-.7-2.43,11.82,11.82,0,0,0-5.2-5.64,10.21,10.21,0,0,0-5-1.12A14.81,14.81,0,0,0,184.5,58a14.47,14.47,0,0,0-5,3.88A18.36,18.36,0,0,0,176.2,68a26.23,26.23,0,0,0-1.2,8.3,31.07,31.07,0,0,0,.91,7.38,19.73,19.73,0,0,0,2.89,6.51,15.46,15.46,0,0,0,5,4.62,14.12,14.12,0,0,0,7.25,1.77,15.54,15.54,0,0,0,4.29-.54,15.73,15.73,0,0,0,3.38-1.36V83.63H186.23V72.92h25.14V107.3H198.76v-2.23a15.7,15.7,0,0,1-4.95,2.1,23.84,23.84,0,0,1-5.52.62,23.09,23.09,0,0,1-9.4-2.06,25.54,25.54,0,0,1-8.45-6.06,31.7,31.7,0,0,1-6.14-9.89,35.68,35.68,0,0,1-2.39-13.48,36.42,36.42,0,0,1,2.14-12.66,29.76,29.76,0,0,1,6-10,27.84,27.84,0,0,1,20.78-9Z" />
                     </clipPath>
                     <linearGradient id="linear-gradient" x1="216.73" y1="71.7" x2="260.14" y2="71.7"
                        gradientTransform="matrix(1, 0, 0, -1, 0, 156)" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#0568af" />
                        <stop offset="1" stop-color="#49c0ef" />
                     </linearGradient>
                     <linearGradient id="linear-gradient-2" x1="263.48" y1="71.71" x2="306.93" y2="71.71"
                        xlink:href="#linear-gradient" />
                     <clipPath id="clip-path-2">
                        <path class="cls-1"
                           d="M309,71.76V61.87h5.69V57.33a21.75,21.75,0,0,1,1.94-9.93,15.41,15.41,0,0,1,11.17-8.53,35.58,35.58,0,0,1,6.51-.66,12.91,12.91,0,0,1,2.06.16c.61.11,1.27.25,2,.41a6.28,6.28,0,0,1,2,.83l-2.14,11.21-1.61-.37a7.33,7.33,0,0,0-1.61-.21,9.19,9.19,0,0,0-4.49.95A6,6,0,0,0,328,53.75a10.93,10.93,0,0,0-1.07,3.67,36.76,36.76,0,0,0-.24,4.45H337.9v9.89H326.64l.08,35.45h-12l-.08-35.45Z" />
                     </clipPath>
                     <clipPath id="clip-path-3">
                        <path class="cls-1"
                           d="M353.35,61.7l11,29.44L375,61.7h12q-4.69,12.38-8.49,22.1c-1.05,2.8-2.09,5.54-3.14,8.2s-2,5.07-2.8,7.22-1.48,3.89-2,5.23-.77,2.08-.82,2.19a42.63,42.63,0,0,1-4.87,9.85,23.54,23.54,0,0,1-5,5.4,12.93,12.93,0,0,1-4.65,2.31,17.79,17.79,0,0,1-3.84.49h-2.14a20.28,20.28,0,0,1-3.3-.33c-1.27-.22-2.45-.47-3.55-.74l3.6-11.79a8.28,8.28,0,0,0,4.12,1.07,6.48,6.48,0,0,0,5.07-2,18.51,18.51,0,0,0,3.42-4.94L341.14,61.7Z" />
                     </clipPath>
                     <clipPath id="clip-path-4">
                        <polygon class="cls-1" points="63.23 70.74 46.44 94.51 33.66 72.48 63.23 70.74" />
                     </clipPath>
                     <linearGradient id="linear-gradient-3" x1="0" y1="79.18" x2="141.06" y2="79.18"
                        xlink:href="#linear-gradient" />
                  </defs>
                  <title>goofyshotrs Logo White</title>
                  <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g id="Layer_1-2-2" data-name="Layer 1-2">
                           <g id="Layer_1-2-2-2" data-name="Layer 1-2-2">
                              <path class="cls-2"
                                 d="M395.58,89.55,403,88.9a16.27,16.27,0,0,0,2.43,7.27,13.31,13.31,0,0,0,5.94,4.59,22.54,22.54,0,0,0,9.06,1.75,21.64,21.64,0,0,0,7.89-1.33,10.82,10.82,0,0,0,5.09-3.64A8.4,8.4,0,0,0,435,92.49a7.6,7.6,0,0,0-1.61-4.85,11.94,11.94,0,0,0-5.31-3.49,108.77,108.77,0,0,0-10.51-2.87c-5.42-1.31-9.22-2.53-11.39-3.69a15.92,15.92,0,0,1-6.3-5.49,13.54,13.54,0,0,1-2.07-7.35,15,15,0,0,1,2.53-8.35,15.71,15.71,0,0,1,7.41-5.9,28.24,28.24,0,0,1,10.83-2,29.57,29.57,0,0,1,11.57,2.11,16.69,16.69,0,0,1,7.71,6.22,17.79,17.79,0,0,1,2.9,9.3l-7.49.56q-.6-5.59-4.08-8.45c-2.33-1.91-5.75-2.86-10.29-2.86q-7.08,0-10.33,2.6a7.78,7.78,0,0,0-3.24,6.26,6.7,6.7,0,0,0,2.3,5.23q2.25,2.06,11.77,4.21t13.07,3.76a18,18,0,0,1,7.61,6,14.7,14.7,0,0,1,2.45,8.39,16.13,16.13,0,0,1-2.7,8.88,17.67,17.67,0,0,1-7.75,6.48,26.84,26.84,0,0,1-11.37,2.32,33.82,33.82,0,0,1-13.42-2.34,18.77,18.77,0,0,1-8.5-7A20,20,0,0,1,395.58,89.55Z" />
                              <path class="cls-2"
                                 d="M452.3,108.51v-59h7.25V70.67a16.17,16.17,0,0,1,12.8-5.88,17.25,17.25,0,0,1,8.25,1.87,11,11,0,0,1,5,5.18c1,2.2,1.51,5.39,1.51,9.58v27.09h-7.25V81.42c0-3.63-.78-6.26-2.35-7.91A8.76,8.76,0,0,0,470.86,71a11.86,11.86,0,0,0-6.06,1.67,9.39,9.39,0,0,0-4,4.53,20.79,20.79,0,0,0-1.21,7.89v23.39Z" />
                              <path class="cls-2"
                                 d="M529.92,70.57a19.19,19.19,0,0,0-14.41-5.78,19.91,19.91,0,0,0-13.45,4.75q-6.6,5.73-6.6,17.59,0,10.89,5.54,16.65t14.51,5.76a20.93,20.93,0,0,0,10.32-2.62A17.46,17.46,0,0,0,533,99.57q2.47-4.75,2.48-13Q535.53,76.36,529.92,70.57Zm-5.43,28.86a10.77,10.77,0,0,1-1.17,1.17,11.93,11.93,0,0,1-16.83-1.17q-3.58-4.08-3.58-12.3t3.56-12.22a10.61,10.61,0,0,1,1.06-1.06,12,12,0,0,1,16.94,1.06h0q3.6,4.07,3.6,12Q528.05,95.32,524.49,99.43Z" />
                              <path class="cls-2"
                                 d="M543.93,108.51V65.76h6.52v6.48c1.67-3,3.2-5,4.61-6a8.05,8.05,0,0,1,4.65-1.45,14.13,14.13,0,0,1,7.45,2.34l-2.5,6.72a10.28,10.28,0,0,0-5.31-1.57,6.93,6.93,0,0,0-4.27,1.43,7.66,7.66,0,0,0-2.69,4,28.15,28.15,0,0,0-1.21,8.46v22.38Z" />
                              <path class="cls-2"
                                 d="M587.29,102l1,6.4a26.53,26.53,0,0,1-5.47.64,12.45,12.45,0,0,1-6.12-1.24,7,7,0,0,1-3.06-3.28c-.59-1.33-.88-4.19-.88-8.56V71.39h-5.32V65.76h5.32V55.17l7.2-4.35V65.76h7.29v5.63H580v25a12.83,12.83,0,0,0,.38,4,3,3,0,0,0,1.25,1.41,4.85,4.85,0,0,0,2.48.52A22.54,22.54,0,0,0,587.29,102Z" />
                              <path class="cls-2"
                                 d="M591.48,95.75l7.16-1.13a10,10,0,0,0,3.36,6.6q2.76,2.3,7.71,2.3t7.41-2a6,6,0,0,0,2.41-4.77,4.42,4.42,0,0,0-2.13-3.87q-1.49-1-7.41-2.46a68.23,68.23,0,0,1-11-3.48,10.53,10.53,0,0,1-6.26-9.8A11.08,11.08,0,0,1,594,71.82a11.69,11.69,0,0,1,3.56-4,15.18,15.18,0,0,1,4.61-2.11,22.06,22.06,0,0,1,6.26-.87,24.64,24.64,0,0,1,8.84,1.45,12,12,0,0,1,5.62,3.92,15.43,15.43,0,0,1,2.49,6.63l-7.08,1a7.79,7.79,0,0,0-2.8-5.15,10.28,10.28,0,0,0-6.5-1.94q-5,0-7.13,1.65a4.81,4.81,0,0,0-2.13,3.86,4,4,0,0,0,.88,2.54,6.43,6.43,0,0,0,2.78,1.93c.73.27,2.87.88,6.4,1.85A86.73,86.73,0,0,1,620.48,86a10.78,10.78,0,0,1,4.77,3.8A10.65,10.65,0,0,1,627,96a12.12,12.12,0,0,1-2.11,6.82,13.67,13.67,0,0,1-6.1,5,22.06,22.06,0,0,1-9,1.76q-8.32,0-12.7-3.47C594.14,103.71,592.28,100.28,591.48,95.75Z" />
                              <g class="cls-3">
                                 <image class="cls-4" width="51" height="65" transform="translate(161.49 43.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAABCCAYAAAAYA/U3AAAACXBIWXMAAAsSAAALEgHS3X78AAAGVUlEQVRoQ91aSXo1Jwws8Zx9bpTr5HbZ5XrKAjQgicn5vUjKXzc0CNBE9fBMzIz/E+i3P//i9mlon4bPp6F9Pvh8yfXH2r8+2t8aobUGkvLTy9YINErf3z4NIICI+tHI6rtrAP3Ux2JcUz9N7USEv//4nVoy8T+O2iAeh7sEMzi2BXnfFhOZ5KBQp1Afx3dRGiTKMHNQtlLZlQwzfIzlaNrK6p03dgiyhUGzBDuLfIQ0j4m6R+MekOOnsnph9Jd2isKTl317sEjGhQYGgcwHIHcu0btTfc46f1WTg6CpzGAhRA8rkw22iuDKWaOFy849HuQr0aAhW+GiwSNMR1Io+qV8IgVE+NVW9Y5tgntSGC2+ty4DKWzTdFF/CFISLnNoXqdbFLfQNSm0f8HBOyys/nlSkMBFURuSSCG74FeTwieTArtKVnZE+TvPiVwZVKOafU0KU1KzRSrKikV+77h+uXohhcpB53rHmhQYSgqzakkstzHUAa+PMc8xDQPuSEEp23okf2lHCpW77529x0L+iRQoujtNKqTAANNgBKpFbchECgR3rfgGKZB4W58OhBAGKbQ5mOwq1d5/dbjgJUWrNVRLHmfNf2lhPYWhPJeOFGZy6LglhRp+rlW944EUvhGJ+jlmDT7MVyEMeCQFw5EUiCB0P2Hj7CdjRLjcQyOjdqTQGuWU4KmAOoPTax14dbDVVTChJoUq/Bek0D96LB3ttaslMhaT7fdRhbxGTQpQX4PF20uFrZxJYZbeEYHfarUbfOvGUTh8JGGzbjTUor6Uq37PenJ3STpHhDFnUnDu9u2RFKbvarTx98LZnCoHsOngsSEF1nZqzdgsTOoKeGdEvXh18I0N5GpGCtW4BSk0/SJKrY+sFNSKdwhwTp1iMtlTpyw9TR1IQZSJRFC5keEkTXhhFcmfpKWv7xlhgzxg+5EkPins4M2zfeTdzZvD1yr4nr02Z1JQi2bLalJow5AQNY9Ct0nqxnsDhJyfm9eHboD8ikD+ZiGQcZCuqmaiJXjIbfdOQQrIawCRFPQ7QhuvDKT3oqjQtHfUCb08ejn0+xvtLyMFAE46KLVRkouycsGaFEzmyI4JecDX3OXehyTtymErMNrYRz01bpLOnhPXwfHJVSWa4UgK5rbs8UwKolw9JjVxEZV775WksH1SkP00P844yDi5dMpUelZHKSyQJWXtwB7VsJIU+itDU+OqwRUp9MsLFzsRkrWR9IVfIo5boSSF/srgDNkoOa81zux7OhIpBELII1bwUnnEkhTAUFJwAjWYAZZ71dhTKb/DBAwABHZid6Swx5IU2C5LWyZSePmVIfooTn4Ik3fUEynY3pmTSqDK6Lj5Q0qlZzyWwoJICphFq2EpQrJYG+zWyFIpS5rBOvkDKSghkEuoy0CvkFiujeOWFCBSGjIRn8ckUggi5QocynSRR83vQyNtNPU8ihX19YLhHmB3pOCPURNfeLGEk0MNJSno/tk8+hCMDMqb7go8FW8IjroihXR/XKzMblxveCCFqrPC0FcdFqJZDSs/Y8l/VHVKHhMmZ0j0QhSP+62DcE8KdzN2OFIYhEAEUxP1fvJgS01tKuRnUrCxUpRLHLKkgosQ2wZNecF5UmeIRnO4vQxowHP6lQJZOH0KBvvorGfXJwSiyywbJoS9docx4FukAMxuK4LTm3n0jd4HUkhYGSj6vpOCWST/Zqk3wCUpwIbCTX7hfrmxelKwzmLAeUrFIIVhhP6OajMsSYFh96lbUlBCmL3woO8RRgpDczEgfp3z8Ff6W9INKejc7ojWlMuudMmumEihTz5M2SzU9R5PCNebfDhMqg/QG+srKQASKB511u6I6oZ6QwolVh3GBVrZx2cYxOgfReRJeyIFWpOC3zvesB3807YnBhNYDLxEA6D/RF6oV+o3pY2L6CwwQ54UYqy2kfNCl+gRkpuppA1bSmG0T+D+EUVurgikgAUp9EwwIybHmFCBVaJlYcdy0s21rLtWQgAwabgFl9UVPLv715MrUmALizNM2vdgVzuRwhJFJwOZFDZ1QZO01lcGTZ+cRjrRiEpp8MoJPN9cf44UGMXvqK621I9NlKcdV7rO0mZeRY4tjgIGd2MVr8u1Q5E/+isD3ZECyx8HI4q5y/ol2hyB1WzzzNcfFQcutuIBd3oBwD8WI0wZdPg0+QAAAABJRU5ErkJggg==" />
                              </g>
                              <path class="cls-5"
                                 d="M216.73,84.29a27.34,27.34,0,0,1,1.61-9.48,21.78,21.78,0,0,1,4.54-7.5,21.24,21.24,0,0,1,6.88-4.91A21.7,21.7,0,0,1,254,67.31a22.09,22.09,0,0,1,4.54,7.5,27.33,27.33,0,0,1,1.6,9.48,26.88,26.88,0,0,1-1.6,9.44,22.56,22.56,0,0,1-4.54,7.51,20.7,20.7,0,0,1-6.92,4.94,21.18,21.18,0,0,1-8.7,1.78,20.75,20.75,0,0,1-15.54-6.72,22.39,22.39,0,0,1-4.54-7.51A27.12,27.12,0,0,1,216.73,84.29ZM238.42,97A8,8,0,0,0,243,95.75a10.28,10.28,0,0,0,3-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24A12.38,12.38,0,0,0,246,75.93a10.31,10.31,0,0,0-3-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1A13.75,13.75,0,0,0,229.14,80a17.51,17.51,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,238.42,97Z" />
                              <path class="cls-6"
                                 d="M263.48,84.29a27.34,27.34,0,0,1,1.61-9.48,21.91,21.91,0,0,1,4.53-7.5,21.39,21.39,0,0,1,6.89-4.91,21.3,21.3,0,0,1,28.81,12.41,27.34,27.34,0,0,1,1.61,9.48,26.85,26.85,0,0,1-1.61,9.44,22.37,22.37,0,0,1-4.53,7.51,20.64,20.64,0,0,1-6.93,4.94,21.18,21.18,0,0,1-8.7,1.78,20.82,20.82,0,0,1-15.54-6.72,22.37,22.37,0,0,1-4.53-7.51A26.85,26.85,0,0,1,263.48,84.29ZM285.16,97a8,8,0,0,0,4.58-1.24,10.57,10.57,0,0,0,3.05-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24,12.38,12.38,0,0,0-1.69-4.12,10.6,10.6,0,0,0-3.05-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1,13.57,13.57,0,0,0-1.7,4.06,17.27,17.27,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,285.16,97Z" />
                              <g class="cls-7">
                                 <image class="cls-4" width="32" height="70" transform="translate(308.49 37.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABHCAYAAACJSS4oAAAACXBIWXMAAAsSAAALEgHS3X78AAAE+klEQVRoQ8WYXZIrNwiFD3KygOwoy8nu8pjdDXmQEIcfye2ZW3VxyW7htgQfR3jGoqr43SZ//vOvjjEwXnG8XgPZ/7Lrwb6X3zPSfeQvvuX/7++/ZBzDawCpVr9C5xv7TZ3PPl33ofUDQB+Epr2axXxVX9xjWcFoGl/zBvYBpyAEkDSHACL70i/ML4CIQNZEujHWWPMx5vbPSJjPsl7zTWLTcByqCk00sGhs31r6TCKgQMx4zd3n2RUKo5mbb0VxJpFRcM2LzzPPNbfMkXwQwFh8i4T7at1bEpmGSKj3D0nUmhcSqgDrQJHqegiiZEw+EdhTT6HJetIYkHUadjY3Teh+ij7VeMHqP1Jg3VifWGtantcjqjS3icWgfAOVigdsc3WBTlJYwpx2bVZC830kwWVBEOwcBzGuzZ2mb/cZCfJr9m0CnRgXBXEK3JI/IiHkryTeHE2kMj4lUXxMiEkokEXKOmAKn5Ggm9hnNJxCcyQzDTQUHpHIKJQWCr58JJMWvtaJGInCOxIbW+PbC+GSefCTaJu9gFOzakjshUgULYV0KlgLucJmdxL0KScxL/zsH2gMgUBi8Ae7t236oMIoNHU/aKG+TyQfCZPvownF4dlp9OnXfGVSoLI8EuapWW1/EqmEQceS/qDZJ+7HJMyfFgwUlI/lHC2FRySkkpA0tq8cTUQ9oCQf7NqsMgkiu0lw9vFYsh5K8sGuR5RJZAr3Fs2nArW8yb7VrHShyMc0aIEp5KSSfatZ3Vu0BYVSxpPdSdAHnYI6hbZFe0BZ0Cd7T8JERRSQaQQtpKwfkPijc6o9JVF540ntWRQ6GlKqUMJ5isP+EfBhi9iDNgYaMe5SjErK5sveCLMWcD+EfN3Y35Yx4HBEl/0SEi2FtkUTBdbVPYiGhNHIJMKxnCXgoGKwlBsaEhKCSGbJ05wg0agtOn+HXEmsDe5t2z4l7JsbjbZFr2tECkbiZM+aVco+aKETKxIFOesBeNqsBsImY63aayFlnUvb2HsSmcAaWQMgHewyAqGUJytBCCIJ1oFpAUgUVtGVBlvnYytte0uho7BaNIb7HLeXxUsT63CK496sUgls8VaMksti87mDSewSRJbyYZNQCqyYU1nAQWOnf6nGhUSigVWOqY3RBmlCChQst2MIBxI31KYRrjtrYaoDQVeI0ijWknikhVED3AlgZf7LSNi3I6QElcs1/Snr5ySiJbrQL5xbNJGY1ylrg9RttOzarOaYK7Q/lC8agUJDoviSlSAUiYJpof0TP+vBg9/W+ZINuzG0iQHSAqoWaMxose7z4Nk6H9ts2+pjL9S0aB9MioJci0zB0rr+0lo8ourDFjbkw/APgf8S15QFMh9E97MjumtbS8EEPMt6RCeJlTkTrntva0moavgrelNYp0VEgFOzgvywWYVFsYPhRENjQiVBMpj2MQladGuhOZpZC+WY8g6PSfAiWwsx+5KxjSDo/eL2mAQv/qX7q9q14DQKhSBotCSKL1kgYZvk3x1y1sUHotBk3fnYAomtBfvn5lD74ltFp6oG63xsm4TIABD/r8wDGsWrKUW6JVjnY9tt2xa0zfaPH5uxVtzrfg4QXiD4xYNyKKgUNuhoegkYbS0TVmlWgfBRs5JVsE2hlMFRO1rdwWt4M2W97n9Povn9MR5NFp1PhGi4L2X9hITqWpgi5poXH2HRnN8p4xsGAP8DLvKSAi070PIAAAAASUVORK5CYII=" />
                              </g>
                              <g class="cls-8">
                                 <image class="cls-4" width="47" height="64" transform="translate(340.49 60.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAABBCAYAAACXfCfjAAAACXBIWXMAAAsSAAALEgHS3X78AAAGFUlEQVRoQ9VaW7IrNwhsXMkCsqMsJ7vLZ3Zn8iEBzUMe+aZuJaFKZiSBANHgOacsqor/M8mvf/ypIgJ5CV6v1+YCeb0K3/tyKfeSInuh8+XZf/3+m7yeIvyv0/cByB4XJCKAALJ1lpq4evC9Y2fLnjQ7sSCynu8DUB7qS8F1LSugqoUvncV0DxQez1CFvhV41/08gG8CGG693mRk55wi2bebs8J5QWyYjGQJu33gmwCA4mnfDAPYhotTie9ncnRNhaRiNp95DGB20NlDIHWaasEluBZoxeQ8S+J6DEujXzCSojmiCqhEDYhAoRDIllaoCmRjWVSgqsQBVQHEasW4zDXz1qgtAHgB8trRkWv3EErxcJpLQvatdSIdrgHPCp8pcY7LZZtG9wEA4eBhMwVkRunCzIU4wrwPR+E64XBvvEHf14A9mpef5HyJHYTrVremWuB2G7CLow8BkIQv7eLReA6Urk/dch3TSsbn3j+Ot2aoETe6h1BSFPqMmSelFBpL+WzLuWP5lH1WzVGn+wCMyg2cRADgq1cJlhMUyAR0LiHExNBBQAITdGh/hMkTdAB9K95vzZmcxqaLACQp7BXiEhOXC4UMCbvloRPRVKRm6dyHLgLYdHAwZlEDAQWWG24hfR/sU3ytujrT921UaHySo2muhVqu/JSh5vB0nuAP4KqNWg1ELTjGQUZSwSnJcVtcGD+1TQAU1PrM835lFxASunFfIf6UGZLiLLja3vfWe5FVoosAiEYHh9vacvlVIr/tuKDJmQQFeUPf14A9Ho0Ma94Os4PW78d2qob+9fnPagCYa6AW2lMNqK4aoNGgVPLUsotMFxCSxNYjG6EeMmamOEa3723WM+Ti13QRANFHI+WWzEExp3MoDJ2WNVTofA0hJoaO8WKEoBOOhYNT+ww6geO0lukiAL+6gylKf82CrXgWTDa+baNjkXzSn881ughgkzsJ4GTEnRE0SyYlH76JBxtP9H0bFRqf5BCBeEDWhVJn4lpgeE61cF0DLGY1YFhHGNvFwTXQayFa5gidbWWGzpkbXUAorMyHCGWFbt85ZaFps65tVRdPa4suAtg0GMkuiW9V6Dz/ZUYOfgyk0/c1YI8CeFcZ5RZveD/WAAZ4/uwacMzbPtfAGu0VYQeeODhsKXNe7fd1ASHxG6+HuKmWEdnTNc9OJsDsDTrjS/oOQuwoyWXHyntO0421eiE+kbZ6JA+gHnbm281tqLVFybgfoQZgtV/eh0N1CRQYH8gDyEXCRZONTF9A0evh/X7ASV0IuZKZQXBYW/RVDcSK2E7MOBsmV9to0uYjJdjZ15F+sAZssJzkjIxjyiCog+1M4xMiMj230ZMRgw05mG9XCkfO0r6ELo8rbvQMocEIb4RDXMgSnOUbCWXzQe5AzwEMRloNCIdWjKWAWBtdJwVyR4c22o1E+yRnVs9MUBqh5hxwPFNng/P93GhaW/TcRlvx0eCWaQHtWw7q9+1T1m3yT2uLniFk2AbnZc/E8pV3Wy24Rnw2aoHc0X0bpdvKBYsU5JMuS4Qk6XwZxMOrxFwLDVb2j6qx3XItAAme+Nk14HjPNdCCtNv1MV3jYa1B51Z30ecaKPAQN0ichFPWBKV9RsCdpkDuyH9q0G61rsv6aQHeAQKFAi9AVQIizvknBAKIYuUufnawfpbAHFDhny9g86Ub86ACId1Q7NB5b2we/87d194CL/e+xEoTqPsX3KhAiIxJ/nYVcxrVKYKOOyXdUl+INeHtD3IDXdfA8Zt4VEKX9Z38bZJ0zn4e6eFVQqgTDeNN7VGtfU5tFCiVs2jvB4d9FJrWFs01QE4CCSSkKp4hh4BD5xIG5QvuKHcPoe3odsZhUhwdC5PCS7VAcizd6OzjRzpDyIJo+7KdQwSZqCxQ8P2soiNp8YpWAGQEIo7veE3OsGprwFALKLzXgGKoAf2RGmAj/hudqVdnSHi2bOJjusbDWrv1W91FrQZkexNOmvdkTGI36fKqINpokj440wK5I4eQyGeYdGjwOr6AzoZDg45t/RCEMnT6TQxXkzJiIl+kP+n9OL28ZiROkz3vtWD7DIZ/91Xib9ELyBRLpQWwAAAAAElFTkSuQmCC" />
                              </g>
                              <g class="cls-9">
                                 <image class="cls-4" width="30" height="25" transform="translate(33.49 69.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAaCAYAAABPY4eKAAAACXBIWXMAAAsSAAALEgHS3X78AAABzUlEQVRIS8VWW5IcIQyz3LUHyI1ynNxuP/d2o/0Ag2ygpyuPiqqgPeZhkDAMSNr/Aj5+fdIvN7/crv716xI7/Ffp43Z13++M/fr5A/5udf8SKTjFUjG41GuvPLb6WXo3uLQ9KDRj+4SP5CM/Y/wSfIvd7sM6McOxzuEZi6DBYYY5rgVHK0DYMADjN2CGVnV/Lj5sNwfMHQaHubfDBW8lrcyOO8/07H1zJmWFxkQ9OftVJM2zZjubiz9opfSLoAzNe5CxBGjwLc6ac9vG1Ba0w1Uia4H74IeaV5196NqCwODegnXdg4FdJgQeXjKUullB6Zx46qus3OGg+aptiJr9ujNt02XOuuKPNadZpz9LoXKZ6n2f5/mAAKrr1NblIO1YGQwsvho8oe44aLUxYc3fzIqOrXXGw4eFUs+FzLNwHnuHm4dlf+j0+kylyBWaN+00jWvwneaud/QMcr5gdtTec3A+7TSLv1jjmkyscGHlxFYeO0O8P3Av5smPPaf3KW4uGRb627M4n8+cjpqiU8aHmgMapAUNUFZY98WlXltPENp7KiUtudEs623iX32H0pHz/NWi1OtT83z0lV9q5T61zvDYEU10Mag03VbP38E3FCS7Ocyt6xMAAAAASUVORK5CYII=" />
                              </g>
                              <path class="cls-10"
                                 d="M138.92,91.49l-6.76,3.92-6.49-11.19,6.75-3.92-2.14-3.69L117.72,83.9l-33.08-57L98.25,19l-2.2-3.8-7.81,4.53L81.74,8.54,89.55,4,87.23,0,53.51,19.57a19.6,19.6,0,0,0-24,13.89v.11l-2,1.16A19.63,19.63,0,0,0,1.66,44.81,19.83,19.83,0,0,0,.12,50.56,19,19,0,0,0,0,52.69a19.64,19.64,0,0,0,38,7,20.11,20.11,0,0,0,1-3.88,19.9,19.9,0,0,0,9.42,2.44A19.56,19.56,0,0,0,61.74,53l.32.58a2.71,2.71,0,0,0,3.69,1,2.62,2.62,0,0,0,.89-.8L75,42l-1-1.77A5.3,5.3,0,0,1,76,33h0l.33-.18L103,80.32l-.33.18a5.28,5.28,0,0,1-7.19-2L81,79.33a2.72,2.72,0,0,0-2.53,2.9h0a2.85,2.85,0,0,0,.32,1.1l2.53,4.51L48,107.48a21.72,21.72,0,0,1-23.49-1L11.2,97.19,36,131.31l-6.44,3.74,2.14,3.7,5.7-3.31,6.5,11.19-5.7,3.3,2.14,3.7L141.06,95.18ZM28.41,56a9.37,9.37,0,0,1-8.78,6.08,9.27,9.27,0,0,1-3.3-.6A9.38,9.38,0,1,1,28.41,56Zm20-8.07a9.38,9.38,0,0,1-.55-18.75h.55a9.39,9.39,0,0,1,9.32,8.27,10.32,10.32,0,0,1,.06,1.11,9.38,9.38,0,0,1-9.35,9.36ZM76.27,26.68,70.72,29.9l-2.49-4.29-3.58-6.16,7.81-4.54,6.07,10.46Zm-21.13,112-5.89-10.14,2.26-1.32L57.06,124l2.31,4L63,134.18Zm19.63-11.4-3.58-6.16-2.31-4,7.82-4.53,2.3,4,3.58,6.17ZM94.4,115.93l-3.58-6.17-2.3-4,7.81-4.54,2.3,4,3.58,6.16Zm19.6-11.4-3.57-6.16-2.31-4,5.55-3.22L116,89.86,121.84,100Z" />
                           </g>
                        </g>
                     </g>
                  </g>
               </svg>
                              <div class="f-22 f-md-32 w700 lh140 text-center text-uppercase mt15 white-clr ">
                              Premium
                              </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 lh140 w400 balck-clr2 mb0">
                                 
                              <li>Create Unlimited Reels & Shorts </li>
                              <li>Create Reels & Short Videos Just by Using One Keyword  </li>
                              <li>Create Boomerang Short Videos to Engage your Audience </li>
                              <li>Create Picture Video by Using Keyword Search  </li>
                              <li>Create Video Using Your Own Video Clips or Stock Videos </li>
                              <li>Create High-Quality Explanatory Whiteboard Video Shorts </li>
                              <li>Add VoiceOver to any Video</li>
                              <li>Add Background Music to any Video  </li>
                              <li>50 + Short frame Templates and 25+ Vector Images</li>
                              <li>100+ Stylish Fonts to make video more Presentable  </li>
                              <li>Eye Catchy Header Text Style to grab attention  </li>
                              <li>Add Waves with different color to your Short videos  </li>
                              <li>100+ social sharing platforms to share the videos for viral traffic </li>
                              <li>Add Your Brand Logo and/or Watermark to your Videos </li>
                              <li>Share Videos Directly to YouTube Shorts </li>
                              <li>Store up to 2 GB of Video, Audio, and other media files  </li>
                              <li>24*7 Customer Support</li>
                              <li>Commercial License Included</li>
                              <li>Provide High In-Demand Video Creation Services to your Clients </li>
                                 <li class="headline ">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - DotcomPal</li>
                                 <li>Fast Action Bonus 2 - Viral Marketing Secrets</li>
                                 <li>Fast Action Bonus 3 - Modern Video Marketing</li>
                                 <li>Fast Action Bonus 4 - Youtube Authority</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/jrrjv5/pdmlv2/ypzl19"><img src="https://warriorplus.com/o2/btn/fn200011000/jrrjv5/pdmlv2/332890" class="img-fluid mx-auto d-block"></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div> -->
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
    
       <!--------To Your Awesome Section ----------->
       <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh140 white-clr text-xs-center">
                     <span class="w700">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally help you build super engaging video channels in the best possible way - without wasting tons of money!
                     <br><br>  So, take action now... and I promise you won't be disappointed!
                  </div>
               </div>
              
               <div class="col-12 w700 f-md-28 f-22 text-start mt20 mt-md70 red-clr">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <div class="row">
                        <div class="col-md-6 col-12 text-center mt20 mt-md0">
                           <img src="assets/images/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta" style="max-height: 200px;"> 
                           <div class="f-24 f-md-24 w700 lh140 text-center white-clr mt20">
                              Pranshu Gupta
                           </div>
                        </div>
                        <div class="col-md-6 col-12 text-center mt30 mt-md0">
                           <img src="assets/images/cp-sir.webp" class="img-fluid d-block mx-auto " alt="Ayush Jain" style="max-height: 200px;">
                           <div class="f-24 f-md-24 w700 lh140 white-clr mt20">
                              Chandraprakash Kalwar
                           </div>
                        </div>
                     </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh140 white-clr">
                     <span class="w700">P.S- You can try "GOOFYSHORT" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.

                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping video channels packed with video content and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w700">P.S.S Don't Procrastinate - Take Action NOW! Get your copy of GOOFYSHORT!  
                     </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------To Your Awesome Section End----------->
      <!--------Faq Section------------->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-48 lh140 w700 text-center balck-clr2">
                  Frequently Asked <span class="red-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140 ">
                              What exactly GoofyShort is all about?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              GoofyShort is the ultimate push-button technology that creates engagin video shorts, drives FREE viral traffic & makes handsfree commissions, ad profits & sales.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you did everything before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140 ">
                              Do I have to install GoofyShort?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NO! GoofyShort is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Yep, my friend, GoofyShort is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NOT AT ALL. There are NO monthly fees to use GoofyShort during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is GoofyShort easy to use?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-32 w700 balck-clr2 px0 text-md-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="red-clr">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------Faq Section End------------->
      <!------Final Section------->
      <div class="final-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 balck-clr2 button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
               <div class="col-12 passport-content">
                  <div class="col-12 f-24 f-md-32 lh140 w600 text-center balck-clr2 mt20">
                     You Still Have the Opportunity to Raise Your Game… <br> Remember, It’s Your Make-or-Break Time! 
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section Start -->
            <div class="row mt20 mt-md40">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center balck-clr2">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 balck-clr2">
                     Use Discount Coupon <span class="red-clr w700">"GoofyShort"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To GoofyShort</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section End -->
         </div>
      </div>
      <!------Final Section End------->

      <!------Footer Section-------->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                  viewBox="0 0 627.01 153.63" style="max-height: 100px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: none;
                        }

                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-3 {
                           clip-path: url(#clip-path);
                        }

                        .cls-4 {
                           isolation: isolate;
                        }

                        .cls-5 {
                           fill: url(#linear-gradient);
                        }

                        .cls-6 {
                           fill: url(#linear-gradient-2);
                        }

                        .cls-7 {
                           clip-path: url(#clip-path-2);
                        }

                        .cls-8 {
                           clip-path: url(#clip-path-3);
                        }

                        .cls-9 {
                           clip-path: url(#clip-path-4);
                        }

                        .cls-10 {
                           fill: url(#linear-gradient-3);
                        }
                     </style>
                     <clipPath id="clip-path">
                        <path class="cls-1"
                           d="M190.85,44.64A21.3,21.3,0,0,1,198.51,46a23.09,23.09,0,0,1,6.19,3.5,22,22,0,0,1,4.61,5.07,24.06,24.06,0,0,1,2.89,6l-10.47,5.36a9.28,9.28,0,0,0-.7-2.43,11.82,11.82,0,0,0-5.2-5.64,10.21,10.21,0,0,0-5-1.12A14.81,14.81,0,0,0,184.5,58a14.47,14.47,0,0,0-5,3.88A18.36,18.36,0,0,0,176.2,68a26.23,26.23,0,0,0-1.2,8.3,31.07,31.07,0,0,0,.91,7.38,19.73,19.73,0,0,0,2.89,6.51,15.46,15.46,0,0,0,5,4.62,14.12,14.12,0,0,0,7.25,1.77,15.54,15.54,0,0,0,4.29-.54,15.73,15.73,0,0,0,3.38-1.36V83.63H186.23V72.92h25.14V107.3H198.76v-2.23a15.7,15.7,0,0,1-4.95,2.1,23.84,23.84,0,0,1-5.52.62,23.09,23.09,0,0,1-9.4-2.06,25.54,25.54,0,0,1-8.45-6.06,31.7,31.7,0,0,1-6.14-9.89,35.68,35.68,0,0,1-2.39-13.48,36.42,36.42,0,0,1,2.14-12.66,29.76,29.76,0,0,1,6-10,27.84,27.84,0,0,1,20.78-9Z" />
                     </clipPath>
                     <linearGradient id="linear-gradient" x1="216.73" y1="71.7" x2="260.14" y2="71.7"
                        gradientTransform="matrix(1, 0, 0, -1, 0, 156)" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#0568af" />
                        <stop offset="1" stop-color="#49c0ef" />
                     </linearGradient>
                     <linearGradient id="linear-gradient-2" x1="263.48" y1="71.71" x2="306.93" y2="71.71"
                        xlink:href="#linear-gradient" />
                     <clipPath id="clip-path-2">
                        <path class="cls-1"
                           d="M309,71.76V61.87h5.69V57.33a21.75,21.75,0,0,1,1.94-9.93,15.41,15.41,0,0,1,11.17-8.53,35.58,35.58,0,0,1,6.51-.66,12.91,12.91,0,0,1,2.06.16c.61.11,1.27.25,2,.41a6.28,6.28,0,0,1,2,.83l-2.14,11.21-1.61-.37a7.33,7.33,0,0,0-1.61-.21,9.19,9.19,0,0,0-4.49.95A6,6,0,0,0,328,53.75a10.93,10.93,0,0,0-1.07,3.67,36.76,36.76,0,0,0-.24,4.45H337.9v9.89H326.64l.08,35.45h-12l-.08-35.45Z" />
                     </clipPath>
                     <clipPath id="clip-path-3">
                        <path class="cls-1"
                           d="M353.35,61.7l11,29.44L375,61.7h12q-4.69,12.38-8.49,22.1c-1.05,2.8-2.09,5.54-3.14,8.2s-2,5.07-2.8,7.22-1.48,3.89-2,5.23-.77,2.08-.82,2.19a42.63,42.63,0,0,1-4.87,9.85,23.54,23.54,0,0,1-5,5.4,12.93,12.93,0,0,1-4.65,2.31,17.79,17.79,0,0,1-3.84.49h-2.14a20.28,20.28,0,0,1-3.3-.33c-1.27-.22-2.45-.47-3.55-.74l3.6-11.79a8.28,8.28,0,0,0,4.12,1.07,6.48,6.48,0,0,0,5.07-2,18.51,18.51,0,0,0,3.42-4.94L341.14,61.7Z" />
                     </clipPath>
                     <clipPath id="clip-path-4">
                        <polygon class="cls-1" points="63.23 70.74 46.44 94.51 33.66 72.48 63.23 70.74" />
                     </clipPath>
                     <linearGradient id="linear-gradient-3" x1="0" y1="79.18" x2="141.06" y2="79.18"
                        xlink:href="#linear-gradient" />
                  </defs>
                  <title>goofyshotrs Logo White</title>
                  <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g id="Layer_1-2-2" data-name="Layer 1-2">
                           <g id="Layer_1-2-2-2" data-name="Layer 1-2-2">
                              <path class="cls-2"
                                 d="M395.58,89.55,403,88.9a16.27,16.27,0,0,0,2.43,7.27,13.31,13.31,0,0,0,5.94,4.59,22.54,22.54,0,0,0,9.06,1.75,21.64,21.64,0,0,0,7.89-1.33,10.82,10.82,0,0,0,5.09-3.64A8.4,8.4,0,0,0,435,92.49a7.6,7.6,0,0,0-1.61-4.85,11.94,11.94,0,0,0-5.31-3.49,108.77,108.77,0,0,0-10.51-2.87c-5.42-1.31-9.22-2.53-11.39-3.69a15.92,15.92,0,0,1-6.3-5.49,13.54,13.54,0,0,1-2.07-7.35,15,15,0,0,1,2.53-8.35,15.71,15.71,0,0,1,7.41-5.9,28.24,28.24,0,0,1,10.83-2,29.57,29.57,0,0,1,11.57,2.11,16.69,16.69,0,0,1,7.71,6.22,17.79,17.79,0,0,1,2.9,9.3l-7.49.56q-.6-5.59-4.08-8.45c-2.33-1.91-5.75-2.86-10.29-2.86q-7.08,0-10.33,2.6a7.78,7.78,0,0,0-3.24,6.26,6.7,6.7,0,0,0,2.3,5.23q2.25,2.06,11.77,4.21t13.07,3.76a18,18,0,0,1,7.61,6,14.7,14.7,0,0,1,2.45,8.39,16.13,16.13,0,0,1-2.7,8.88,17.67,17.67,0,0,1-7.75,6.48,26.84,26.84,0,0,1-11.37,2.32,33.82,33.82,0,0,1-13.42-2.34,18.77,18.77,0,0,1-8.5-7A20,20,0,0,1,395.58,89.55Z" />
                              <path class="cls-2"
                                 d="M452.3,108.51v-59h7.25V70.67a16.17,16.17,0,0,1,12.8-5.88,17.25,17.25,0,0,1,8.25,1.87,11,11,0,0,1,5,5.18c1,2.2,1.51,5.39,1.51,9.58v27.09h-7.25V81.42c0-3.63-.78-6.26-2.35-7.91A8.76,8.76,0,0,0,470.86,71a11.86,11.86,0,0,0-6.06,1.67,9.39,9.39,0,0,0-4,4.53,20.79,20.79,0,0,0-1.21,7.89v23.39Z" />
                              <path class="cls-2"
                                 d="M529.92,70.57a19.19,19.19,0,0,0-14.41-5.78,19.91,19.91,0,0,0-13.45,4.75q-6.6,5.73-6.6,17.59,0,10.89,5.54,16.65t14.51,5.76a20.93,20.93,0,0,0,10.32-2.62A17.46,17.46,0,0,0,533,99.57q2.47-4.75,2.48-13Q535.53,76.36,529.92,70.57Zm-5.43,28.86a10.77,10.77,0,0,1-1.17,1.17,11.93,11.93,0,0,1-16.83-1.17q-3.58-4.08-3.58-12.3t3.56-12.22a10.61,10.61,0,0,1,1.06-1.06,12,12,0,0,1,16.94,1.06h0q3.6,4.07,3.6,12Q528.05,95.32,524.49,99.43Z" />
                              <path class="cls-2"
                                 d="M543.93,108.51V65.76h6.52v6.48c1.67-3,3.2-5,4.61-6a8.05,8.05,0,0,1,4.65-1.45,14.13,14.13,0,0,1,7.45,2.34l-2.5,6.72a10.28,10.28,0,0,0-5.31-1.57,6.93,6.93,0,0,0-4.27,1.43,7.66,7.66,0,0,0-2.69,4,28.15,28.15,0,0,0-1.21,8.46v22.38Z" />
                              <path class="cls-2"
                                 d="M587.29,102l1,6.4a26.53,26.53,0,0,1-5.47.64,12.45,12.45,0,0,1-6.12-1.24,7,7,0,0,1-3.06-3.28c-.59-1.33-.88-4.19-.88-8.56V71.39h-5.32V65.76h5.32V55.17l7.2-4.35V65.76h7.29v5.63H580v25a12.83,12.83,0,0,0,.38,4,3,3,0,0,0,1.25,1.41,4.85,4.85,0,0,0,2.48.52A22.54,22.54,0,0,0,587.29,102Z" />
                              <path class="cls-2"
                                 d="M591.48,95.75l7.16-1.13a10,10,0,0,0,3.36,6.6q2.76,2.3,7.71,2.3t7.41-2a6,6,0,0,0,2.41-4.77,4.42,4.42,0,0,0-2.13-3.87q-1.49-1-7.41-2.46a68.23,68.23,0,0,1-11-3.48,10.53,10.53,0,0,1-6.26-9.8A11.08,11.08,0,0,1,594,71.82a11.69,11.69,0,0,1,3.56-4,15.18,15.18,0,0,1,4.61-2.11,22.06,22.06,0,0,1,6.26-.87,24.64,24.64,0,0,1,8.84,1.45,12,12,0,0,1,5.62,3.92,15.43,15.43,0,0,1,2.49,6.63l-7.08,1a7.79,7.79,0,0,0-2.8-5.15,10.28,10.28,0,0,0-6.5-1.94q-5,0-7.13,1.65a4.81,4.81,0,0,0-2.13,3.86,4,4,0,0,0,.88,2.54,6.43,6.43,0,0,0,2.78,1.93c.73.27,2.87.88,6.4,1.85A86.73,86.73,0,0,1,620.48,86a10.78,10.78,0,0,1,4.77,3.8A10.65,10.65,0,0,1,627,96a12.12,12.12,0,0,1-2.11,6.82,13.67,13.67,0,0,1-6.1,5,22.06,22.06,0,0,1-9,1.76q-8.32,0-12.7-3.47C594.14,103.71,592.28,100.28,591.48,95.75Z" />
                              <g class="cls-3">
                                 <image class="cls-4" width="51" height="65" transform="translate(161.49 43.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAABCCAYAAAAYA/U3AAAACXBIWXMAAAsSAAALEgHS3X78AAAGVUlEQVRoQ91aSXo1Jwws8Zx9bpTr5HbZ5XrKAjQgicn5vUjKXzc0CNBE9fBMzIz/E+i3P//i9mlon4bPp6F9Pvh8yfXH2r8+2t8aobUGkvLTy9YINErf3z4NIICI+tHI6rtrAP3Ux2JcUz9N7USEv//4nVoy8T+O2iAeh7sEMzi2BXnfFhOZ5KBQp1Afx3dRGiTKMHNQtlLZlQwzfIzlaNrK6p03dgiyhUGzBDuLfIQ0j4m6R+MekOOnsnph9Jd2isKTl317sEjGhQYGgcwHIHcu0btTfc46f1WTg6CpzGAhRA8rkw22iuDKWaOFy849HuQr0aAhW+GiwSNMR1Io+qV8IgVE+NVW9Y5tgntSGC2+ty4DKWzTdFF/CFISLnNoXqdbFLfQNSm0f8HBOyys/nlSkMBFURuSSCG74FeTwieTArtKVnZE+TvPiVwZVKOafU0KU1KzRSrKikV+77h+uXohhcpB53rHmhQYSgqzakkstzHUAa+PMc8xDQPuSEEp23okf2lHCpW77529x0L+iRQoujtNKqTAANNgBKpFbchECgR3rfgGKZB4W58OhBAGKbQ5mOwq1d5/dbjgJUWrNVRLHmfNf2lhPYWhPJeOFGZy6LglhRp+rlW944EUvhGJ+jlmDT7MVyEMeCQFw5EUiCB0P2Hj7CdjRLjcQyOjdqTQGuWU4KmAOoPTax14dbDVVTChJoUq/Bek0D96LB3ttaslMhaT7fdRhbxGTQpQX4PF20uFrZxJYZbeEYHfarUbfOvGUTh8JGGzbjTUor6Uq37PenJ3STpHhDFnUnDu9u2RFKbvarTx98LZnCoHsOngsSEF1nZqzdgsTOoKeGdEvXh18I0N5GpGCtW4BSk0/SJKrY+sFNSKdwhwTp1iMtlTpyw9TR1IQZSJRFC5keEkTXhhFcmfpKWv7xlhgzxg+5EkPins4M2zfeTdzZvD1yr4nr02Z1JQi2bLalJow5AQNY9Ct0nqxnsDhJyfm9eHboD8ikD+ZiGQcZCuqmaiJXjIbfdOQQrIawCRFPQ7QhuvDKT3oqjQtHfUCb08ejn0+xvtLyMFAE46KLVRkouycsGaFEzmyI4JecDX3OXehyTtymErMNrYRz01bpLOnhPXwfHJVSWa4UgK5rbs8UwKolw9JjVxEZV775WksH1SkP00P844yDi5dMpUelZHKSyQJWXtwB7VsJIU+itDU+OqwRUp9MsLFzsRkrWR9IVfIo5boSSF/srgDNkoOa81zux7OhIpBELII1bwUnnEkhTAUFJwAjWYAZZ71dhTKb/DBAwABHZid6Swx5IU2C5LWyZSePmVIfooTn4Ik3fUEynY3pmTSqDK6Lj5Q0qlZzyWwoJICphFq2EpQrJYG+zWyFIpS5rBOvkDKSghkEuoy0CvkFiujeOWFCBSGjIRn8ckUggi5QocynSRR83vQyNtNPU8ihX19YLhHmB3pOCPURNfeLGEk0MNJSno/tk8+hCMDMqb7go8FW8IjroihXR/XKzMblxveCCFqrPC0FcdFqJZDSs/Y8l/VHVKHhMmZ0j0QhSP+62DcE8KdzN2OFIYhEAEUxP1fvJgS01tKuRnUrCxUpRLHLKkgosQ2wZNecF5UmeIRnO4vQxowHP6lQJZOH0KBvvorGfXJwSiyywbJoS9docx4FukAMxuK4LTm3n0jd4HUkhYGSj6vpOCWST/Zqk3wCUpwIbCTX7hfrmxelKwzmLAeUrFIIVhhP6OajMsSYFh96lbUlBCmL3woO8RRgpDczEgfp3z8Ff6W9INKejc7ojWlMuudMmumEihTz5M2SzU9R5PCNebfDhMqg/QG+srKQASKB511u6I6oZ6QwolVh3GBVrZx2cYxOgfReRJeyIFWpOC3zvesB3807YnBhNYDLxEA6D/RF6oV+o3pY2L6CwwQ54UYqy2kfNCl+gRkpuppA1bSmG0T+D+EUVurgikgAUp9EwwIybHmFCBVaJlYcdy0s21rLtWQgAwabgFl9UVPLv715MrUmALizNM2vdgVzuRwhJFJwOZFDZ1QZO01lcGTZ+cRjrRiEpp8MoJPN9cf44UGMXvqK621I9NlKcdV7rO0mZeRY4tjgIGd2MVr8u1Q5E/+isD3ZECyx8HI4q5y/ol2hyB1WzzzNcfFQcutuIBd3oBwD8WI0wZdPg0+QAAAABJRU5ErkJggg==" />
                              </g>
                              <path class="cls-5"
                                 d="M216.73,84.29a27.34,27.34,0,0,1,1.61-9.48,21.78,21.78,0,0,1,4.54-7.5,21.24,21.24,0,0,1,6.88-4.91A21.7,21.7,0,0,1,254,67.31a22.09,22.09,0,0,1,4.54,7.5,27.33,27.33,0,0,1,1.6,9.48,26.88,26.88,0,0,1-1.6,9.44,22.56,22.56,0,0,1-4.54,7.51,20.7,20.7,0,0,1-6.92,4.94,21.18,21.18,0,0,1-8.7,1.78,20.75,20.75,0,0,1-15.54-6.72,22.39,22.39,0,0,1-4.54-7.51A27.12,27.12,0,0,1,216.73,84.29ZM238.42,97A8,8,0,0,0,243,95.75a10.28,10.28,0,0,0,3-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24A12.38,12.38,0,0,0,246,75.93a10.31,10.31,0,0,0-3-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1A13.75,13.75,0,0,0,229.14,80a17.51,17.51,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,238.42,97Z" />
                              <path class="cls-6"
                                 d="M263.48,84.29a27.34,27.34,0,0,1,1.61-9.48,21.91,21.91,0,0,1,4.53-7.5,21.39,21.39,0,0,1,6.89-4.91,21.3,21.3,0,0,1,28.81,12.41,27.34,27.34,0,0,1,1.61,9.48,26.85,26.85,0,0,1-1.61,9.44,22.37,22.37,0,0,1-4.53,7.51,20.64,20.64,0,0,1-6.93,4.94,21.18,21.18,0,0,1-8.7,1.78,20.82,20.82,0,0,1-15.54-6.72,22.37,22.37,0,0,1-4.53-7.51A26.85,26.85,0,0,1,263.48,84.29ZM285.16,97a8,8,0,0,0,4.58-1.24,10.57,10.57,0,0,0,3.05-3.09,12.17,12.17,0,0,0,1.69-4.08,20,20,0,0,0,.5-4.29,19.5,19.5,0,0,0-.5-4.24,12.38,12.38,0,0,0-1.69-4.12,10.6,10.6,0,0,0-3.05-3.1,9.12,9.12,0,0,0-9.15,0,10.2,10.2,0,0,0-3,3.1,13.57,13.57,0,0,0-1.7,4.06,17.27,17.27,0,0,0,0,8.57,13.76,13.76,0,0,0,1.69,4.08,9.94,9.94,0,0,0,3,3.09A8,8,0,0,0,285.16,97Z" />
                              <g class="cls-7">
                                 <image class="cls-4" width="32" height="70" transform="translate(308.49 37.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABHCAYAAACJSS4oAAAACXBIWXMAAAsSAAALEgHS3X78AAAE+klEQVRoQ8WYXZIrNwiFD3KygOwoy8nu8pjdDXmQEIcfye2ZW3VxyW7htgQfR3jGoqr43SZ//vOvjjEwXnG8XgPZ/7Lrwb6X3zPSfeQvvuX/7++/ZBzDawCpVr9C5xv7TZ3PPl33ofUDQB+Epr2axXxVX9xjWcFoGl/zBvYBpyAEkDSHACL70i/ML4CIQNZEujHWWPMx5vbPSJjPsl7zTWLTcByqCk00sGhs31r6TCKgQMx4zd3n2RUKo5mbb0VxJpFRcM2LzzPPNbfMkXwQwFh8i4T7at1bEpmGSKj3D0nUmhcSqgDrQJHqegiiZEw+EdhTT6HJetIYkHUadjY3Teh+ij7VeMHqP1Jg3VifWGtantcjqjS3icWgfAOVigdsc3WBTlJYwpx2bVZC830kwWVBEOwcBzGuzZ2mb/cZCfJr9m0CnRgXBXEK3JI/IiHkryTeHE2kMj4lUXxMiEkokEXKOmAKn5Ggm9hnNJxCcyQzDTQUHpHIKJQWCr58JJMWvtaJGInCOxIbW+PbC+GSefCTaJu9gFOzakjshUgULYV0KlgLucJmdxL0KScxL/zsH2gMgUBi8Ae7t236oMIoNHU/aKG+TyQfCZPvownF4dlp9OnXfGVSoLI8EuapWW1/EqmEQceS/qDZJ+7HJMyfFgwUlI/lHC2FRySkkpA0tq8cTUQ9oCQf7NqsMgkiu0lw9vFYsh5K8sGuR5RJZAr3Fs2nArW8yb7VrHShyMc0aIEp5KSSfatZ3Vu0BYVSxpPdSdAHnYI6hbZFe0BZ0Cd7T8JERRSQaQQtpKwfkPijc6o9JVF540ntWRQ6GlKqUMJ5isP+EfBhi9iDNgYaMe5SjErK5sveCLMWcD+EfN3Y35Yx4HBEl/0SEi2FtkUTBdbVPYiGhNHIJMKxnCXgoGKwlBsaEhKCSGbJ05wg0agtOn+HXEmsDe5t2z4l7JsbjbZFr2tECkbiZM+aVco+aKETKxIFOesBeNqsBsImY63aayFlnUvb2HsSmcAaWQMgHewyAqGUJytBCCIJ1oFpAUgUVtGVBlvnYytte0uho7BaNIb7HLeXxUsT63CK496sUgls8VaMksti87mDSewSRJbyYZNQCqyYU1nAQWOnf6nGhUSigVWOqY3RBmlCChQst2MIBxI31KYRrjtrYaoDQVeI0ijWknikhVED3AlgZf7LSNi3I6QElcs1/Snr5ySiJbrQL5xbNJGY1ylrg9RttOzarOaYK7Q/lC8agUJDoviSlSAUiYJpof0TP+vBg9/W+ZINuzG0iQHSAqoWaMxose7z4Nk6H9ts2+pjL9S0aB9MioJci0zB0rr+0lo8ourDFjbkw/APgf8S15QFMh9E97MjumtbS8EEPMt6RCeJlTkTrntva0moavgrelNYp0VEgFOzgvywWYVFsYPhRENjQiVBMpj2MQladGuhOZpZC+WY8g6PSfAiWwsx+5KxjSDo/eL2mAQv/qX7q9q14DQKhSBotCSKL1kgYZvk3x1y1sUHotBk3fnYAomtBfvn5lD74ltFp6oG63xsm4TIABD/r8wDGsWrKUW6JVjnY9tt2xa0zfaPH5uxVtzrfg4QXiD4xYNyKKgUNuhoegkYbS0TVmlWgfBRs5JVsE2hlMFRO1rdwWt4M2W97n9Povn9MR5NFp1PhGi4L2X9hITqWpgi5poXH2HRnN8p4xsGAP8DLvKSAi070PIAAAAASUVORK5CYII=" />
                              </g>
                              <g class="cls-8">
                                 <image class="cls-4" width="47" height="64" transform="translate(340.49 60.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAABBCAYAAACXfCfjAAAACXBIWXMAAAsSAAALEgHS3X78AAAGFUlEQVRoQ9VaW7IrNwhsXMkCsqMsJ7vLZ3Zn8iEBzUMe+aZuJaFKZiSBANHgOacsqor/M8mvf/ypIgJ5CV6v1+YCeb0K3/tyKfeSInuh8+XZf/3+m7yeIvyv0/cByB4XJCKAALJ1lpq4evC9Y2fLnjQ7sSCynu8DUB7qS8F1LSugqoUvncV0DxQez1CFvhV41/08gG8CGG693mRk55wi2bebs8J5QWyYjGQJu33gmwCA4mnfDAPYhotTie9ncnRNhaRiNp95DGB20NlDIHWaasEluBZoxeQ8S+J6DEujXzCSojmiCqhEDYhAoRDIllaoCmRjWVSgqsQBVQHEasW4zDXz1qgtAHgB8trRkWv3EErxcJpLQvatdSIdrgHPCp8pcY7LZZtG9wEA4eBhMwVkRunCzIU4wrwPR+E64XBvvEHf14A9mpef5HyJHYTrVremWuB2G7CLow8BkIQv7eLReA6Urk/dch3TSsbn3j+Ot2aoETe6h1BSFPqMmSelFBpL+WzLuWP5lH1WzVGn+wCMyg2cRADgq1cJlhMUyAR0LiHExNBBQAITdGh/hMkTdAB9K95vzZmcxqaLACQp7BXiEhOXC4UMCbvloRPRVKRm6dyHLgLYdHAwZlEDAQWWG24hfR/sU3ytujrT921UaHySo2muhVqu/JSh5vB0nuAP4KqNWg1ELTjGQUZSwSnJcVtcGD+1TQAU1PrM835lFxASunFfIf6UGZLiLLja3vfWe5FVoosAiEYHh9vacvlVIr/tuKDJmQQFeUPf14A9Ho0Ma94Os4PW78d2qob+9fnPagCYa6AW2lMNqK4aoNGgVPLUsotMFxCSxNYjG6EeMmamOEa3723WM+Ti13QRANFHI+WWzEExp3MoDJ2WNVTofA0hJoaO8WKEoBOOhYNT+ww6geO0lukiAL+6gylKf82CrXgWTDa+baNjkXzSn881ughgkzsJ4GTEnRE0SyYlH76JBxtP9H0bFRqf5BCBeEDWhVJn4lpgeE61cF0DLGY1YFhHGNvFwTXQayFa5gidbWWGzpkbXUAorMyHCGWFbt85ZaFps65tVRdPa4suAtg0GMkuiW9V6Dz/ZUYOfgyk0/c1YI8CeFcZ5RZveD/WAAZ4/uwacMzbPtfAGu0VYQeeODhsKXNe7fd1ASHxG6+HuKmWEdnTNc9OJsDsDTrjS/oOQuwoyWXHyntO0421eiE+kbZ6JA+gHnbm281tqLVFybgfoQZgtV/eh0N1CRQYH8gDyEXCRZONTF9A0evh/X7ASV0IuZKZQXBYW/RVDcSK2E7MOBsmV9to0uYjJdjZ15F+sAZssJzkjIxjyiCog+1M4xMiMj230ZMRgw05mG9XCkfO0r6ELo8rbvQMocEIb4RDXMgSnOUbCWXzQe5AzwEMRloNCIdWjKWAWBtdJwVyR4c22o1E+yRnVs9MUBqh5hxwPFNng/P93GhaW/TcRlvx0eCWaQHtWw7q9+1T1m3yT2uLniFk2AbnZc/E8pV3Wy24Rnw2aoHc0X0bpdvKBYsU5JMuS4Qk6XwZxMOrxFwLDVb2j6qx3XItAAme+Nk14HjPNdCCtNv1MV3jYa1B51Z30ecaKPAQN0ichFPWBKV9RsCdpkDuyH9q0G61rsv6aQHeAQKFAi9AVQIizvknBAKIYuUufnawfpbAHFDhny9g86Ub86ACId1Q7NB5b2we/87d194CL/e+xEoTqPsX3KhAiIxJ/nYVcxrVKYKOOyXdUl+INeHtD3IDXdfA8Zt4VEKX9Z38bZJ0zn4e6eFVQqgTDeNN7VGtfU5tFCiVs2jvB4d9FJrWFs01QE4CCSSkKp4hh4BD5xIG5QvuKHcPoe3odsZhUhwdC5PCS7VAcizd6OzjRzpDyIJo+7KdQwSZqCxQ8P2soiNp8YpWAGQEIo7veE3OsGprwFALKLzXgGKoAf2RGmAj/hudqVdnSHi2bOJjusbDWrv1W91FrQZkexNOmvdkTGI36fKqINpokj440wK5I4eQyGeYdGjwOr6AzoZDg45t/RCEMnT6TQxXkzJiIl+kP+n9OL28ZiROkz3vtWD7DIZ/91Xib9ELyBRLpQWwAAAAAElFTkSuQmCC" />
                              </g>
                              <g class="cls-9">
                                 <image class="cls-4" width="30" height="25" transform="translate(33.49 69.74)"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAaCAYAAABPY4eKAAAACXBIWXMAAAsSAAALEgHS3X78AAABzUlEQVRIS8VWW5IcIQyz3LUHyI1ynNxuP/d2o/0Ag2ygpyuPiqqgPeZhkDAMSNr/Aj5+fdIvN7/crv716xI7/Ffp43Z13++M/fr5A/5udf8SKTjFUjG41GuvPLb6WXo3uLQ9KDRj+4SP5CM/Y/wSfIvd7sM6McOxzuEZi6DBYYY5rgVHK0DYMADjN2CGVnV/Lj5sNwfMHQaHubfDBW8lrcyOO8/07H1zJmWFxkQ9OftVJM2zZjubiz9opfSLoAzNe5CxBGjwLc6ac9vG1Ba0w1Uia4H74IeaV5196NqCwODegnXdg4FdJgQeXjKUullB6Zx46qus3OGg+aptiJr9ujNt02XOuuKPNadZpz9LoXKZ6n2f5/mAAKrr1NblIO1YGQwsvho8oe44aLUxYc3fzIqOrXXGw4eFUs+FzLNwHnuHm4dlf+j0+kylyBWaN+00jWvwneaud/QMcr5gdtTec3A+7TSLv1jjmkyscGHlxFYeO0O8P3Av5smPPaf3KW4uGRb627M4n8+cjpqiU8aHmgMapAUNUFZY98WlXltPENp7KiUtudEs623iX32H0pHz/NWi1OtT83z0lV9q5T61zvDYEU10Mag03VbP38E3FCS7Ocyt6xMAAAAASUVORK5CYII=" />
                              </g>
                              <path class="cls-10"
                                 d="M138.92,91.49l-6.76,3.92-6.49-11.19,6.75-3.92-2.14-3.69L117.72,83.9l-33.08-57L98.25,19l-2.2-3.8-7.81,4.53L81.74,8.54,89.55,4,87.23,0,53.51,19.57a19.6,19.6,0,0,0-24,13.89v.11l-2,1.16A19.63,19.63,0,0,0,1.66,44.81,19.83,19.83,0,0,0,.12,50.56,19,19,0,0,0,0,52.69a19.64,19.64,0,0,0,38,7,20.11,20.11,0,0,0,1-3.88,19.9,19.9,0,0,0,9.42,2.44A19.56,19.56,0,0,0,61.74,53l.32.58a2.71,2.71,0,0,0,3.69,1,2.62,2.62,0,0,0,.89-.8L75,42l-1-1.77A5.3,5.3,0,0,1,76,33h0l.33-.18L103,80.32l-.33.18a5.28,5.28,0,0,1-7.19-2L81,79.33a2.72,2.72,0,0,0-2.53,2.9h0a2.85,2.85,0,0,0,.32,1.1l2.53,4.51L48,107.48a21.72,21.72,0,0,1-23.49-1L11.2,97.19,36,131.31l-6.44,3.74,2.14,3.7,5.7-3.31,6.5,11.19-5.7,3.3,2.14,3.7L141.06,95.18ZM28.41,56a9.37,9.37,0,0,1-8.78,6.08,9.27,9.27,0,0,1-3.3-.6A9.38,9.38,0,1,1,28.41,56Zm20-8.07a9.38,9.38,0,0,1-.55-18.75h.55a9.39,9.39,0,0,1,9.32,8.27,10.32,10.32,0,0,1,.06,1.11,9.38,9.38,0,0,1-9.35,9.36ZM76.27,26.68,70.72,29.9l-2.49-4.29-3.58-6.16,7.81-4.54,6.07,10.46Zm-21.13,112-5.89-10.14,2.26-1.32L57.06,124l2.31,4L63,134.18Zm19.63-11.4-3.58-6.16-2.31-4,7.82-4.53,2.3,4,3.58,6.17ZM94.4,115.93l-3.58-6.17-2.3-4,7.81-4.54,2.3,4,3.58,6.16Zm19.6-11.4-3.57-6.16-2.31-4,5.55-3.22L116,89.86,121.84,100Z" />
                           </g>
                        </g>
                     </g>
                  </g>
               </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/jrrjv5" defer=""></script><div class="wplus_spdisclaimer f-16 f-md-16 w400 lh140 white-clr"><span class="w600">Disclaimer : </span> WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © GoofyShort</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.goofyshortsai.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
      <!------Footer Section End-------->
         <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="SiteSmartAi Special Bonuses">
   <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">    
   <meta name="author" content="Atul Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="SiteSmartAi Special Bonuses">
   <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta property="og:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="SiteSmartAi Special Bonuses">
   <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta property="twitter:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <title>SiteSmartAi Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'october 14 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }*/
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/m37b83/0';
      $_GET['name'] = 'Atul Pareek';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"/>
                            <stop offset="1" stop-color="#07c2fd"/>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"/>
                            <stop offset=".22" stop-color="#f88038"/>
                            <stop offset=".67" stop-color="#e74352"/>
                            <stop offset="1" stop-color="#da1467"/>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                            </g>
                          </g>
                        </g>
                      </svg>
                  </div>
               </div></div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="pre-heading f-20 f-md-21 w500 white-clr lh140">
                  Grab My 20 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh130 head-design">
               World's First AI App Builds Us <span class="orange-gradient">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
            <div class="col-12 mt-md30 mt20 f-18 f-md-24 w700 text-center lh140 white-clr text-capitalize">
            Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-18 f-md-22 w600 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
               Now Create &amp; Sell Stunning Websites For Architect, Dentist, Fitness, Lawyer, Painter, Restaurant, Pet, And 1000s Of Other Niches <u>With Just A Keyword &amp; Charge Instant $1,000 Per Website</u>
               </div> 
            </div>
            <div class="col-12 mt-md20 mt20 f-20 f-md-22 w600 text-center lh140 orange-clr1 text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>  
         </div>
         <div class="row mt20 mt-md30 mb20 mb-md30">
            <div class="col-12 col-md-10 mx-auto">
            <img src="assets/images/product-box.png" class="img-fluid mx-auto d-block" alt="ProductBox"> 
            </div>
         </div>
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list  pl0 m0 f-18 f-md-20 lh150 w400z">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                     <ul class="bonus-list  pl0 m0 f-18 f-md-20 lh150 w400z">
                        <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div> 
            </div>
         </div>
         <div class="row mt30 mt-md60">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black black-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-black black-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>

  
<!-- Step Section Start -->
<div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to SiteSmartAi App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;"><source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish &amp; Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Step Sction End -->


   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   
   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
               SiteSmartAi Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with WebGenie… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  SiteSmartAi eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With SiteSmartAi you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because SiteSmartAi is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using SiteSmartAi
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->




   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->

   <!--Proudly Introducing Start -->
   <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="presenting-shape f-md-38 f-24 w600 text-center white-clr lh140 text-uppercase">Presenting</div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: url(#linear-gradient-2);
                         }
                   
                         .cls-2 {
                           fill: #8b27ff;
                         }
                   
                         .cls-3 {
                           fill: #fff;
                         }
                   
                         .cls-4 {
                           fill: #df325c;
                         }
                   
                         .cls-5 {
                           fill: #ff992e;
                         }
                   
                         .cls-6 {
                           fill: url(#linear-gradient-3);
                         }
                   
                         .cls-7 {
                           fill: url(#linear-gradient);
                         }
                       </style>
                       <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#7c26d5"/>
                         <stop offset="1" stop-color="#07c2fd"/>
                       </linearGradient>
                       <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                       <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#ff992e"/>
                         <stop offset=".22" stop-color="#f88038"/>
                         <stop offset=".67" stop-color="#e74352"/>
                         <stop offset="1" stop-color="#da1467"/>
                       </linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                           <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                           <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                           <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                           <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                           <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                           <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                           <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                         </g>
                         <g>
                           <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                           <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                           <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                           <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                           <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                           <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                           <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                           <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                           <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                           <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                           <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                           <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                         </g>
                       </g>
                     </g>
                   </svg>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh130 head-design">
               World's First AI App Builds Us <span class="orange-gradient">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
            <div class="col-12 mt-md30 mt20 f-18 f-md-24 w700 text-center lh140 white-clr text-capitalize">
            Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-18 f-md-22 w600 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
               Now Create & Sell Stunning Websites For Architect, Dentist, Fitness, Lawyer, Painter, Restaurant, Pet, And 1000s Of Other Niches <u>With Just A Keyword & Charge Instant $1,000 Per Website</u>
               </div> 
            </div>
            <div class="col-12 mt-md20 mt20 f-20 f-md-22 w600 text-center lh140 orange-clr1 text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.png" class="img-fluid d-block mx-auto" alt="Product">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="proudly-list-bg">
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <ul class="proudly-tick pl0 m0 f-18 f-md-20 w400 black-clr lh140">
                              <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                              <li>Easily Create & Sell Beautiful, Highly Professional Website filled with <span class="w600">Unique Content in 10000+ Niches</span> </li>
                              <li>Just set your keywords & create your <span class="w600"> Website in 25+ languages with your own Content & Images</span></li>
                              <li><span class="w600">Automatically Publish Own Content and Images to your Website Posts & Pages All the day,</span> without you ever having to log in ChatGPT.</li>
                              <li><span class="w600">500+ Done-For-You Super Customizable Themes</span> to Instantly Create High Converting Website in Any Niche</li>
                              <li><span class="w600">Easily Integrate ChatGPT Bot onto your Website</span> to communicate with customers directly on the page they are browsing.</li>
                           </ul>
                        </div>
                        <div class="col-md-6 col-12">
                           <ul class="proudly-tick pl0 m0 f-18 f-md-20 w400 black-clr lh140">
                              <li class="w700">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</li>
                              <li>Automatically <span class="w600">answer to visitor question or comment on blog posts or page</span> from the AI engine directly.</li>
                              <li>Seamless <span class="w600">Woo Commerce Integration</span>  to Accept Payments for Online Selling & E-com Stores</li>
                              <li>Preloaded with <span class="w600">3000+ DFY Editable Templates</span> for Local Marketing Graphics, Marketing Videos, Social Media Graphics, Logo Kit and Marketing Letters & Scripts, etc</li>
                              <li>Create <span class="w600">SEO Optimized, 100% Mobile,</span>  and Fast Loading SiteSmartAi</li>
                              <li><span class="w600">UNLIMITED COMMERCIAL LICENSE</span> Included to Start Selling Website Creation Services and Make Profit Big Time</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <!--Proudly Introducing End -->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : TubeTraffic AI 
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="header-section-mailergpt">
         <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1tb {
                                fill: #fff;
                            }
        
                            .cls-2tb {
                                fill: #fd2f15;
                            }
        
                            .cls-3tb {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3tb" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3tb" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2tb" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3tb" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3tb" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1tb" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1tb" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1tb" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1tb" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1tb" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1tb" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1tb" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1tb" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1tb" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1tb" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1tb" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3tb" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2tb" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2tb" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-18 w600 white-clr lh130 pre-heading">
               YouTube has been dominated by Ai…
               </div>
            </div>
            <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 p0">
                    World's First Google-Bard Powered AI App That 
                    <span class="red-brush-top"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-28 w600 lh140 white-clr ">
                        Then Redirect Their Views To <u>ANY Link or Offer</u> of Our Choice & Sent Us…
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                    Thousands Of Views In 3 Hours or Less… Making Us $346.34 Daily on Autopilot
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                Without Creating Videos | Without Uploading | Without Paid Ads | Without Any Upfront Cost
                </div>
            </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12 mt-md80">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Click Play And Watch TubeTraffic Ai In Action Making Us Hundreds Of Dollars…
                     </div>
                     <div class=" mt20 ">
                        <!-- <img src="assets/images/product-image.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://tubetrafficai.oppyo.com/video/embed/mvuk8vau96" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li>Let AI Do the Research and Pull HOT Profitable Videos for You.</li>

                     <li>No Video Creation Whatsoever, Ai Does It All…</li>

                     <li>Built-In AI Traffic Generator Sends Thousands of Clicks on Any Link. 100% Free</li>

                     <li>Get Instant Access to 800 million+  Videos</li>

                     <li>Have 100% Control on Every Video Legally</li>

                    <li> One Clicks Ai Monetization with Our DFY High-Ticket Offers</li>

                     

                     <li>Works in Any Niche and Any Business</li>

                     <li>Create Self-growing Video Channels Without Creating Any Video</li>

                     <li>Capture Leads or Promote Affiliate Offers with Stunning DFY Templates.</li>

                    

                     <li>Every Video & Page You Generate with TubeTrafficAI is 100% Mobile Optimized.</li>

                     <li>Build a Huge List & Integrates with All Top Autoresponders</li>

                    <li> Instantly Tap Into 5.3 billion YouTube Buyers</li>

                    <li> No Ads or Promotions Required</li>

                     <li>No Complicated Setup - Get Up and Running In 30 Seconds</li>

                     <li>ZERO Upfront Cost</li>

                     <li>30 Days Money-Back Guarantee</li>
                     </ul>
                  </div>
               </div>
         </div>
         </div>
      </div>
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/tubetrafficai.webp">
            <source media="(min-width:320px)" srcset="assets/images/tubetrafficai-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/tubetrafficai.webp" alt="Vidvee Steps" class="img-fluid">
         </picture>
      </div>
   </div>

   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : VoiceFusion AI
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Vidvee Section------>
   <div class="vidvee-header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="assets/images/vidvee.logo.png" alt="" class="d-block mx-auto img-fluid">
            </div>
            <div class="col-md-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 80px;">
                    <defs>
                    <style>
                    .cls-1vf {fill:#fff;}
                    .cls-2vf ,
                    .cls-4vf {fill:#ff7c24;}
                    .cls-3vf {fill:#15c5f3;}
                    .cls-4vf {fill-rule:evenodd;}
                    </style>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                    <path class="cls-1vf" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"></path>
                    <path class="cls-1vf" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"></path>
                    <path class="cls-1vf" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"></path>
                    <path class="cls-1vf" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"></path>
                    <path class="cls-1vf" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"></path>
                    <path class="cls-2vf" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"></path>
                    <path class="cls-2vf" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"></path>
                    <path class="cls-2vf" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"></path>
                    <path class="cls-2vf" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"></path>
                    <path class="cls-2vf" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"></path>
                    <path class="cls-2vf" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"></path>
                    <path class="cls-1vf" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"></path>
                    <path class="cls-1vf" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"></path>
                    <path class="cls-3vf" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"></path>
                    <path class="cls-3vf" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"></path>
                    <path class="cls-3vf" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"></path>
                    <path class="cls-2vf" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"></path>
                    <path class="cls-2vf" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"></path>
                    <rect class="cls-2vf" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"></rect>
                    <path class="cls-2vf" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"></path>
                    <path class="cls-2vf" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"></path>
                    <path class="cls-2vf" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"></path>
                    <path class="cls-2vf" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"></path>
                    <path class="cls-2vf" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"></path>
                    <rect class="cls-2vf" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"></rect>
                    <path class="cls-4vf" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"></path>
                    </g>
                    </g>
                    </svg>
               </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-18 w600 white-clr lh130 vidvee-pre-heading">
               Finally! The Days of Dull & Robotic Voices Are Over…
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 text-center">
               <div class="f-md-40 f-24 w400 white-clr lh140 text-center">
               World’s First Super-Trending <span class="orange-clr">"IBM's Watson & ChatGPT4" Powered Ai App</span> Generates Us <span class="w700">Real Human 
               Emotion Based Voices, Unique Content, Audiobooks, and Podcasts</span> <u>with Just 1 Keyword.</u>
               </div>
               <div class="mt-md25 mt20 f-22 f-md-32 w700 text-center lh130 yellow-brsh">
               Then Sell Them to Our Secret Buyers Network of 3.2 million Users...
               </div>
               <div class="mt-md25 mt20 f-22 f-md-24 w400 text-center lh130 text-center white-clr">
               & Making Us <u>$382.38 Daily with Literally Zero Work</u>. Even A 100% Beginner Can Do It.
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-18 f-md-22 lh140 red-bg white-clr">
                  Without Hiring Anyone - No Writing Content - No Experience - No Hidden Fee - No BS
                  </div>
               </div>
               <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12 mt-md80">
                  <div class="col-12">
                     <div class=" mt20 ">
                        <!-- <img src="assets/images/product-image.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://tubetrafficai.oppyo.com/video/embed/mvuk8vau96" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                  <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li><span class="w700">Eliminate All the Guesswork</span> and Jump Straight to Results</li>
                        <li><span class="w700">	Let Ai Create Real Human Emotion Based Voices</span></li>
                        <li>Create Complete <span class="w700">AudioBooks &amp; Podcasts</span> Easily</li>
                        <li><span class="w700">	Even Ai Write High Quality Unique Content for You</span></li>
                        <li><span class="w700">A True Ai App </span> That Leverages IBM's Watson &amp; ChatGPT4 Technology</li>
                        <li><span class="w700">Instant High Converting Traffic For 100% Free…</span> No Need to Run Ads</li>
                        <li><span class="w700">300+ Real Voices</span> to Choose from with Human Emotions</li>
                        <li><span classs="w700">100+ Languages</span> to Choose From</li>
                        <li><span class="w700">DFY 1 million Articles</span> with Full PLR License</li>
                        <li><span class="w700">Create Unlimited</span> VSL’s, Sales Copies, Emails, Ads Copy Etc.</li>
                        <li><span class="w700">Turn Your Old Boring Robotic Voices</span> into a Real Human Voices</li>
                        <li>98% Of Beta Testers Made <span class="w700">At Least One Sale Within 24 Hours</span> of Using VoiceFusionAi</li>
                        <li><span class="w700">Pay Only Once</span> &amp; Get Profit Forever</li>
                        <li><span class="w700">No Complicated Setup</span> - Get Up and Running In 2 Minutes</li>
                        <li> <span class="w700">30 Days Money-Back Guarantee</span></li>
                        <li><span class="w700">Free COMMERCIAL LICENSE Included</span> - Serve Unlimited Clients</li>
                     </ul>
                  </div>
               </div>
         </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/voicefusionai.webp">
            <source media="(min-width:320px)" srcset="assets/images/voicefusionai-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/voicefusionai.webp" alt="Vidvee Steps" class="img-fluid mx-auto d-block" style="width:100%">
         </picture>
        
      </div>
   </div>
   <!------Vidvee Section------>
   <!----------Buziffy ---------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Ninja AI 
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS
                  If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="header-section-ninja">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
               <defs>
                  <style>
                  .cls-1ai{
                     fill: url(#linear-gradient-2);
                  }
               
                  .cls-2ai{
                     fill: #fff;
                  }
               
                  .cls-3ai{
                     fill-rule: evenodd;
                  }
               
                  .cls-3ai, .cls-4ai{
                     fill: #ff813b;
                  }
               
                  .cls-5ai{
                     fill: url(#linear-gradient-3);
                  }
               
                  .cls-6ai{
                     fill: url(#linear-gradient);
                  }
               
                  .cls-7ai{
                     fill: url(#linear-gradient-4);
                  }
                  </style>
                  <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#6e33ff"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#a356fa"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#ac59fa"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
               </defs>
               <g id="Layer_1-2" data-name="Layer 1">
                  <g>
                  <g>
                     <g>
                     <path class="cls-6ai" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                     <path class="cls-1ai" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                     <circle class="cls-5ai" cx="59.23" cy="62.54" r="3.63"></circle>
                     <circle class="cls-7ai" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                     </g>
                     <g>
                     <g>
                        <path class="cls-3ai" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                        <path class="cls-3ai" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                     </g>
                     <path class="cls-4ai" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                     </g>
                  </g>
                  <g>
                     <path class="cls-2ai" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                     <path class="cls-2ai" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                     <path class="cls-2ai" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                     <path class="cls-2ai" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                     <path class="cls-2ai" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                     <path class="cls-4ai" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                     <path class="cls-4ai" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                  </g>
                  </g>
               </g>
            </svg>
            </div>
            <div class="col-12 text-center lh150 mt20 mt-md50">
               <div class="pre-heading-ninja f-18 f-md-22 w600 lh140 white-clr">
                  Legally... Swipe Guru's Profitable Funnels with Our “ChatGPT4 Secret Agent”
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
               World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
               <span class="white-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews &amp; Lead Magnet in Just 60 Seconds.</span>
            </div>
            <div class="col-12 mt-md15 mt15 text-center">
               <div class="f-22 f-md-28 w700 lh140 black-clr yellow-brsh">
                  Then Promote Them to Our Secret Buyers Network of 496 million Users...
               </div>
            </div>
            <div class="col-12 mt20 text-center">
               <div class="f-22 f-md-28 w400 lh140 white-clr">
                  &amp; Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
               </div>
            </div>
            <div class="col-12 mt20 text-center">
               <div class="f-20 f-md-22 w400 lh140 red-bg white-clr">
                  No Designing, No Copywriting, &amp; No Hosting…. Even A 100% Beginner Can Do It.
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md40 gx-md-5">
            <div class="col-md-7 col-12">
               <div class="col-12">
                  <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                  Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                  </div>
                  <div class="video-box mt20 mt-md30">
                     <div style="padding-bottom: 56.25%;position: relative;">
                     <iframe src="https://ninjaai.dotcompal.com/video/embed/q5429rqlji" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0">
               <div class="key-features-bg">
                  <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li><span class="w600">Eliminate All The Guesswork</span> And Jump Straight to Results</li>
                     <li><span class="w600">Let Ai Find</span> Hundreds Of The Guru's Profitable Funnels</li>
                     <li><span class="w600">Let Ai Create</span> Proven Money Making Funnels On Autopilot</li>
                     <li class="w600">All The Copywriting And Designing Is Done For You</li>
                     <li>Generate 100% <span class="w600">SEO &amp; Mobile Optimized</span> Funnels</li>
                     <li><span class="w600">Instant High Converting Traffic For 100% Free…</span> No Need To Run Ads</li>
                     <li>98% Of Beta Testers Made At Least One Sale <span class="w600 underline">Within 24 Hours</span> Of Using Ninja AI.</li>
                     <li><span class="w600">A True Ai App</span> That Leverages ChatGPT4</li>
                     <li>Connect Your Favourite Autoresponders And <span class="w600">Build Your List FAST.</span></li>
                     <li>Seamless Integration With Any Payment Processor You Want</li>
                     <li class="w600">ZERO Upfront Cost</li>
                     <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                     <li class="w600">So Easy, Anyone Can Do it.</li>
                     <li>30 Days Money-Back Guarantee</li>
                     <li><span class="w600">Free Commercial License Included -</span> That's Priceless</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>   
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/ninja-ai.webp">
            <source media="(min-width:320px)" srcset="assets/images/ninja-ai.webp">
            <img src="assets/images/ninja-ai.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase SiteSmartAi, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Meteora WordPress Theme </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Your Hands On A Killer, Out-of-the-box Wordpress Theme That's Versatile, Easy To Use And Extremely Customizable… Create An Impactful Good Impression On Your Clients So That They Keep Doing Business With You Over And Over Again!</li>
                        <li>Traffic is very important if you want to keep your website breathing for a living. But what is traffic if it will not become profitable to experience the luxery of fortune. </li>
                        <li>That's why, website design and user experience play a HUGE role in converting your visitors or leads into customers that might keep on coming back for more. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                     100+ WebPage Templates
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Why would you want to pay a lot for a website when you can use these 100+ Webpage templates to create professional looking websites for only pennies! This is one of the best deals on webpage templates ever put together!</li>
                        <li>You're sure to find more than one design you like and here you can have them all for one super low price! 100+ Web templates allow you to have your website up and running in a matter of minutes! Mix and match for unlimited possibilities! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course!</li>
                        <li>Search marketing is the process of generating traffic and gaining visibility from search engines like Google, Bing and Yahoo through paid and unpaid strategies. This includes generating traffic through organic or free listings as well as buying traffic through paid search listings on ad networks like Google AdWords. </li>
                         
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Membership Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           The effortless way to create professional sites in Wordpress using your favorite membership plugin!</li>
                        <li>With this plugin you can build a beautiful and robust membership sites. </li>
                        <li>Fully Customizable Membership Site</li>
                        <li>5 Custom Members Pages</li>
                        <li>Easy To Use Wordpress Theme</li>
                        <li>Works with Membership Plugins</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Latest Humans Stock Images
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</li>
                        <li>If you have your own online business or you are a freelancer that is hired by an online entrepreneur to expand his business, doing social media marketing is the best step to get started.</li>
                        <li>The fact is that many have said that this new marketing innovation is so powerful that would be a huge help to boost your sales to the roof.</li>
                        
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social pop up widget for your WordPress blog! Take advantage of this technique to improve your social conversions. </li>
                        <li>This product allows your people to conveniently follow,like or subscribe to your social media page to keep informed about updates and new releases. </li>
                        <li>You will be confident when you make changes to your like pop-up box. This is super fun to use and fun for your readers.</li>
                        <li>This plugin is point and click simple. You can create your popup in seconds and before clicking 'Save' you will know what to expect because the user experience is very friendly.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     My Blog Announcer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Software that automatically pings blog search engines to get your blogs indexed much faster in the search engines.</li>
                        <li>Just add your blog URLs and then add or remove ping services (the program comes pre-loaded with several services). Whenever you update your blog content, the program will automatically run all blogs to all ping services with the click of a button. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Outsourcing Templates
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                        Outsourcing can be helpful for business owners of every level.</li>
                        <li> You can assign a task to someone else and have it done for you. After the job is completed to your satisfaction, you simply pay the person you hired and the work is yours to keep.</li>
                        <li>This product includes templates for every step of a project outsourcing. Also, it includes Insiders Guide with great tips!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Rapid Product Creation
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course! (5 Days Training On How To Create Info Products At Lightning Speed.)</li>
                        <li>The hardest thing that probably many other people find to when they get started with any aspect of Internet Marketing is all of the hard work it involves.</li>
                        <li>Inside this product is a bundle of media and necessary tools that you can use today to get started creating your own digital product.

Terms</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Keyword Ninja
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of... </li>
                        <li>This is keyword software that finds synonyms and gets keyword data from Overture. It also gets related keywords from sites listed on Google and Yahoo. You can either enter a "starting" keyword manually or download an existing keyword list.</li>
                         <li>Quickly and easily create keyword lists that contain all possible combinations based on the phrases you query.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Cloaker Shadow 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stop losing your hard earned commissions to nasty affiliate thieves and add credibility while promoting others.</li>
                        <li>Hide your ugly affiliate links and boost your click through rate by at least 200%</li>
                        <li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
                        <li>Give you the ability to bypass merchant's squeeze pages and send your prospects directly to their sales pages or even the order page</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Installation Tips & Tricks
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn how easy it is to set up a website using the free platform WordPress!</li>
                        <li>If you are a blogger chances are you want to use WordPress as your blogging platform which can give you the freedom to maximize your marketing effort.</li>
                        <li>The challenge now is that if you haven't had the experience of setting up your first WordPress Website, you may find it difficult or challenging that will eventually consume you more time.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Content Shrinker 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Have You Ever Wished There were a way to Make Your Content Inside an I-Frame Search Engine Readable? Can you just imagine the possibilities for your Adsense and Content sites if this were possible? Wish No Longer! A New Software Program Creates a Content Box Like an I-frame with one HUGE difference, It is Search Engine Readable!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Security Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Secure Your Online Business Empire Using this Amazing WordPress Security Techniques and More!</li>
                        <li>If you are an online business owner, chances are you also have a website. And most of the time, one content management system that is frequently used - wordpress.</li>
                        <li>Because of that, internet marketers and WordPress Developers are finding ways to secure your website from being stolen or hacked. All of those will be taught inside this video tutorial.</li>
                        <li>WordPress is a very powerful tool and is also one of the target by hackers to take down your online business.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Site Speed Secrets
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Everybody hates slow websites. So, why haven't you done anything about your slow site? </li>
                        <li>Don't you realize that it's putting people off from visiting your site and checking out everything that you have to offer?</li>
                        <li>It doesn't matter if you've got a well-designed site or a life-changing product.</li>
                        <li>If people are leaving your site long before it loads on their browsers, then you may as well NOT have spent all that time and money creating your awesome website! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Content Auditor WordPress Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Effortless Data Exports Make it Easy to Manage and Promote Your Top Posts & Pages! Easy to use plugin will export a customized summary of all your Wordpress blog content!</li>
                        <li>If you are a blogger, SEO or an online business owner, auditing your blog content may not be necessary but if you want to scale marketing asset as well as know the progress of your result, doing it may have a huge help to know what step you are going to do next.</li>
                        <li>The thing is that auditing your whole website asset can also be a pain in the ass and time-consuming.<li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Theme Army Software
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Six Wordpress Themes within a Desktop Application that will not only allow you to download the themes but will give you THREE Monetization Methods!</li>
                        <li>WordPress is one of the best and most user-friendly Content Management System software for several years now. The good thing with this amazing CMS is that, you can change it's themes easily and some themes are free of charge. How cool was that?</li>
                        <li>Well, inside this product is a package of six elegant WordPress Themes that you can use for your WordPress-based Website and some tips on how to monetize it.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Database Magic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>An Easy Solution for Creating, Backing Up and Restoring Cpanel</li>
                        <li>Databases - Create/Backup/Restore Databases without Logging</li>
                        <li>
                        into Cpanel - You can decide the database name, username, and
                        </li>
                        <li>
                        password... -OR- You can let the program generate them at random!
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WordPress Ad Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Instantly Create an Ad Using WordPress Ad Creator!</li>
                        <li>Traffic is very important to ones blog or website. And because of that website owners and online entrepreneurs do their part to find those leads or traffic online whether via SEO, Social Media Marketing or Pay Per Click Advertisement.</li>
                        <li>If you want to create an ad inside your WordPress dashboard, using this amazing plugin is a huge help to you.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Visitors To Your WordPress Blog 24/7 Live Chat Support - Even While You Sleep!</li>
                        <li>Traffic is nothing if this will not convert into paying customers. The thing is that, even if your website is online 24/7, if no one will interact with your website visitor, they will just go away somewhere else and never got back again.</li>
                        <li>The good news is that inside this product is a tool that will connect you to your visitors by having a Live Support chat box in your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That's Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-clr">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
         <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w500 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-clr">"SUPERADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md30">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"/>
                            <stop offset="1" stop-color="#07c2fd"/>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"/>
                            <stop offset=".22" stop-color="#f88038"/>
                            <stop offset=".67" stop-color="#e74352"/>
                            <stop offset="1" stop-color="#da1467"/>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                            </g>
                          </g>
                        </g>
                      </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © SiteSmartAi 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
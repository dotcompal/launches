<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="SiteSmartAi Special Bonuses">
   <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Pranshu Gupta">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="SiteSmartAi Special Bonuses">
   <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta property="og:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="SiteSmartAi Special Bonuses">
   <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
   <meta property="twitter:image" content="https://www.sitesmartai.com/special-bonus/thumbnail.png">
   <title>SiteSmartAi Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'October 12 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/kdyk3s/0';
      $_GET['name'] = 'Pranshu Gupta';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"/>
                            <stop offset="1" stop-color="#07c2fd"/>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"/>
                            <stop offset=".22" stop-color="#f88038"/>
                            <stop offset=".67" stop-color="#e74352"/>
                            <stop offset="1" stop-color="#da1467"/>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                            </g>
                          </g>
                        </g>
                      </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="pre-heading f-20 f-md-21 w500 white-clr lh140">
                  Grab My 20 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh130">
               World's First AI App Builds Us <span class="orange-gradient">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
               <div class="col-12 mt-md20 mt20 f-22 f-md-25 w700 text-center lh140 black-clr text-capitalize">
                  <div class="head-yellow">
                  Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
                  </div> 
               </div>
               <div class="col-12 mt-md15 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               Now Create & Sell Stunning Websites For Architect, Dentist, Fitness, Lawyer, Painter, Restaurant, Pet, And 1000s Of Other Niches With Just A Keyword & Charge Instant $1,000 Per Website
            </div>
               <div class="col-12 mt20 text-center">
               <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
               </div>
            <div class="col-12 mt-md25 mt20">
                  <img class="img-fluid mx-auto d-block" src="assets/images/product-box.png" alt="">
            </div>
            </div>
         </div>
         
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list  pl0 m0 f-18 f-md-20 lh150 w400z">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                     <ul class="bonus-list  pl0 m0 f-18 f-md-20 lh150 w400z">
                        <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt30 mt-md60">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class=" col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black black-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-black black-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>

  
<!-- Step Section Start -->
<div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to SiteSmartAi App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;"><source src="assets/images/step1.mp4" type="video/mp4">
                     </video>        
                   </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish &amp; Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Step Sction End -->


   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   
   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
               SiteSmartAi Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with WebGenie… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  SiteSmartAi eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With SiteSmartAi you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because SiteSmartAi is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using SiteSmartAi
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->



   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->

   <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="presenting-shape f-md-38 f-24 w600 text-center white-clr lh140 text-uppercase">Presenting</div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: url(#linear-gradient-2);
                         }
                   
                         .cls-2 {
                           fill: #8b27ff;
                         }
                   
                         .cls-3 {
                           fill: #fff;
                         }
                   
                         .cls-4 {
                           fill: #df325c;
                         }
                   
                         .cls-5 {
                           fill: #ff992e;
                         }
                   
                         .cls-6 {
                           fill: url(#linear-gradient-3);
                         }
                   
                         .cls-7 {
                           fill: url(#linear-gradient);
                         }
                       </style>
                       <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#7c26d5"/>
                         <stop offset="1" stop-color="#07c2fd"/>
                       </linearGradient>
                       <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                       <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#ff992e"/>
                         <stop offset=".22" stop-color="#f88038"/>
                         <stop offset=".67" stop-color="#e74352"/>
                         <stop offset="1" stop-color="#da1467"/>
                       </linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                           <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                           <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                           <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                           <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                           <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                           <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                           <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                         </g>
                         <g>
                           <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                           <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                           <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                           <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                           <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                           <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                           <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                           <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                           <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                           <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                           <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                           <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                         </g>
                       </g>
                     </g>
                   </svg>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh130">
               World's First AI App Builds Us <span class="orange-gradient">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
            <div class="col-12 mt-md30 mt20 f-22 f-md-26 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
               Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
               </div> 
            </div>
            <div class="col-12 mt-md30 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               Now Create & Sell Stunning Websites For Architect, Dentist, Fitness, Lawyer, Painter, Restaurant, Pet, And 1000s Of Other Niches <u>With Just A Keyword & Charge Instant $1,000 Per Website</u>
            </div>
            <div class="col-12 mt-md30 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.png" class="img-fluid d-block mx-auto" alt="Product">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="proudly-list-bg">
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <ul class="proudly-tick pl0 m0 f-18 f-md-20 w400 black-clr lh140">
                              <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                              <li>Easily Create & Sell Beautiful, Highly Professional Website filled with <span class="w600">Unique Content in 10000+ Niches</span> </li>
                              <li>Just set your keywords & create your <span class="w600"> Website in 25+ languages with your own Content & Images</span></li>
                              <li><span class="w600">Automatically Publish Own Content and Images to your Website Posts & Pages All the day,</span> without you ever having to log in ChatGPT.</li>
                              <li><span class="w600">500+ Done-For-You Super Customizable Themes</span> to Instantly Create High Converting Website in Any Niche</li>
                              <li><span class="w600">Easily Integrate ChatGPT Bot onto your Website</span> to communicate with customers directly on the page they are browsing.</li>
                           </ul>
                        </div>
                        <div class="col-md-6 col-12">
                           <ul class="proudly-tick pl0 m0 f-18 f-md-20 w400 black-clr lh140">
                              <li class="w700">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</li>
                              <li>Automatically <span class="w600">answer to visitor question or comment on blog posts or page</span> from the AI engine directly.</li>
                              <li>Seamless <span class="w600">Woo Commerce Integration</span>  to Accept Payments for Online Selling & E-com Stores</li>
                              <li>Preloaded with <span class="w600">3000+ DFY Editable Templates</span> for Local Marketing Graphics, Marketing Videos, Social Media Graphics, Logo Kit and Marketing Letters & Scripts, etc</li>
                              <li>Create <span class="w600">SEO Optimized, 100% Mobile,</span>  and Fast Loading SiteSmartAi</li>
                              <li><span class="w600">UNLIMITED COMMERCIAL LICENSE</span> Included to Start Selling Website Creation Services and Make Profit Big Time</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
   <!-------Exclusive Bonus----------->
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase SiteSmartAi, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Meteora WordPress Theme </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Your Hands On A Killer, Out-of-the-box Wordpress Theme That's Versatile, Easy To Use And Extremely Customizable… Create An Impactful Good Impression On Your Clients So That They Keep Doing Business With You Over And Over Again!</li>
                        <li>Traffic is very important if you want to keep your website breathing for a living. But what is traffic if it will not become profitable to experience the luxery of fortune. </li>
                        <li>That's why, website design and user experience play a HUGE role in converting your visitors or leads into customers that might keep on coming back for more. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                     100+ WebPage Templates
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Why would you want to pay a lot for a website when you can use these 100+ Webpage templates to create professional looking websites for only pennies! This is one of the best deals on webpage templates ever put together!</li>
                        <li>You're sure to find more than one design you like and here you can have them all for one super low price! 100+ Web templates allow you to have your website up and running in a matter of minutes! Mix and match for unlimited possibilities! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course!</li>
                        <li>Search marketing is the process of generating traffic and gaining visibility from search engines like Google, Bing and Yahoo through paid and unpaid strategies. This includes generating traffic through organic or free listings as well as buying traffic through paid search listings on ad networks like Google AdWords. </li>
                         
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Membership Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
The effortless way to create professional sites in Wordpress using your favorite membership plugin!</li>
                        <li>With this plugin you can build a beautiful and robust membership sites. </li>
                        <li>Fully Customizable Membership Site</li>
                        <li>5 Custom Members Pages</li>
                        <li>Easy To Use Wordpress Theme</li>
                        <li>Works with Membership Plugins</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Latest Humans Stock Images
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</li>
                        <li>If you have your own online business or you are a freelancer that is hired by an online entrepreneur to expand his business, doing social media marketing is the best step to get started.</li>
                        <li>The fact is that many have said that this new marketing innovation is so powerful that would be a huge help to boost your sales to the roof.</li>
                        
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social pop up widget for your WordPress blog! Take advantage of this technique to improve your social conversions. </li>
                        <li>This product allows your people to conveniently follow,like or subscribe to your social media page to keep informed about updates and new releases. </li>
                        <li>You will be confident when you make changes to your like pop-up box. This is super fun to use and fun for your readers.</li>
                        <li>This plugin is point and click simple. You can create your popup in seconds and before clicking 'Save' you will know what to expect because the user experience is very friendly.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     My Blog Announcer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Software that automatically pings blog search engines to get your blogs indexed much faster in the search engines.</li>
                        <li>Just add your blog URLs and then add or remove ping services (the program comes pre-loaded with several services). Whenever you update your blog content, the program will automatically run all blogs to all ping services with the click of a button. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Unique Exit Popup
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Grab the Attention of Your Visitors When They Are About to Leave Your Website!</li>
                        <li>Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?</li>
                        <li>Well, inside this product is a software that will change the game of this issue. This plugin will engage and get the attention of your readers that is about to leave your page and offer them something valuable from your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Rapid Product Creation
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course! (5 Days Training On How To Create Info Products At Lightning Speed.)</li>
                        <li>The hardest thing that probably many other people find to when they get started with any aspect of Internet Marketing is all of the hard work it involves.</li>
                        <li>Inside this product is a bundle of media and necessary tools that you can use today to get started creating your own digital product.

Terms</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Keyword Ninja
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of... </li>
                        <li>This is keyword software that finds synonyms and gets keyword data from Overture. It also gets related keywords from sites listed on Google and Yahoo. You can either enter a "starting" keyword manually or download an existing keyword list.</li>
                         <li>Quickly and easily create keyword lists that contain all possible combinations based on the phrases you query.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Cloaker Shadow 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stop losing your hard earned commissions to nasty affiliate thieves and add credibility while promoting others.</li>
                        <li>Hide your ugly affiliate links and boost your click through rate by at least 200%</li>
                        <li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
                        <li>Give you the ability to bypass merchant's squeeze pages and send your prospects directly to their sales pages or even the order page</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Installation Tips & Tricks
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn how easy it is to set up a website using the free platform WordPress!</li>
                        <li>If you are a blogger chances are you want to use WordPress as your blogging platform which can give you the freedom to maximize your marketing effort.</li>
                        <li>The challenge now is that if you haven't had the experience of setting up your first WordPress Website, you may find it difficult or challenging that will eventually consume you more time.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Content Shrinker 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Have You Ever Wished There were a way to Make Your Content Inside an I-Frame Search Engine Readable? Can you just imagine the possibilities for your Adsense and Content sites if this were possible? Wish No Longer! A New Software Program Creates a Content Box Like an I-frame with one HUGE difference, It is Search Engine Readable!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Security Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Secure Your Online Business Empire Using this Amazing WordPress Security Techniques and More!</li>
                        <li>If you are an online business owner, chances are you also have a website. And most of the time, one content management system that is frequently used - wordpress.</li>
                        <li>Because of that, internet marketers and WordPress Developers are finding ways to secure your website from being stolen or hacked. All of those will be taught inside this video tutorial.</li>
                        <li>WordPress is a very powerful tool and is also one of the target by hackers to take down your online business.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Site Speed Secrets
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Everybody hates slow websites. So, why haven't you done anything about your slow site? </li>
                        <li>Don't you realize that it's putting people off from visiting your site and checking out everything that you have to offer?</li>
                        <li>It doesn't matter if you've got a well-designed site or a life-changing product.</li>
                        <li>If people are leaving your site long before it loads on their browsers, then you may as well NOT have spent all that time and money creating your awesome website! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ADMIN35"</span> for an Additional <span class="w700 orange-clr">35% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Content Auditor WordPress Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Effortless Data Exports Make it Easy to Manage and Promote Your Top Posts & Pages! Easy to use plugin will export a customized summary of all your Wordpress blog content!</li>
                        <li>If you are a blogger, SEO or an online business owner, auditing your blog content may not be necessary but if you want to scale marketing asset as well as know the progress of your result, doing it may have a huge help to know what step you are going to do next.</li>
                        <li>The thing is that auditing your whole website asset can also be a pain in the ass and time-consuming.<li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Theme Army Software
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Six Wordpress Themes within a Desktop Application that will not only allow you to download the themes but will give you THREE Monetization Methods!</li>
                        <li>WordPress is one of the best and most user-friendly Content Management System software for several years now. The good thing with this amazing CMS is that, you can change it's themes easily and some themes are free of charge. How cool was that?</li>
                        <li>Well, inside this product is a package of six elegant WordPress Themes that you can use for your WordPress-based Website and some tips on how to monetize it.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Database Magic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>An Easy Solution for Creating, Backing Up and Restoring Cpanel</li>
                        <li>Databases - Create/Backup/Restore Databases without Logging</li>
                        <li>
                        into Cpanel - You can decide the database name, username, and
                        </li>
                        <li>
                        password... -OR- You can let the program generate them at random!
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WordPress Ad Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Instantly Create an Ad Using WordPress Ad Creator!</li>
                        <li>Traffic is very important to ones blog or website. And because of that website owners and online entrepreneurs do their part to find those leads or traffic online whether via SEO, Social Media Marketing or Pay Per Click Advertisement.</li>
                        <li>If you want to create an ad inside your WordPress dashboard, using this amazing plugin is a huge help to you.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Visitors To Your WordPress Blog 24/7 Live Chat Support - Even While You Sleep!</li>
                        <li>Traffic is nothing if this will not convert into paying customers. The thing is that, even if your website is online 24/7, if no one will interact with your website visitor, they will just go away somewhere else and never got back again.</li>
                        <li>The good news is that inside this product is a tool that will connect you to your visitors by having a Live Support chat box in your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That's Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-clr">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab SiteSmartAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:60px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }s
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79"  gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"/>
                            <stop offset="1" stop-color="#07c2fd"/>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"/>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"/>
                            <stop offset=".22" stop-color="#f88038"/>
                            <stop offset=".67" stop-color="#e74352"/>
                            <stop offset="1" stop-color="#da1467"/>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"/>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"/>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"/>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"/>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"/>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"/>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"/>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"/>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"/>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"/>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"/>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"/>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"/>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"/>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"/>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"/>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"/>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"/>
                            </g>
                          </g>
                        </g>
                      </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © SiteSmartAi 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
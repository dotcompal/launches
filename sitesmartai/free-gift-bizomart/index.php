<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>SiteSmartAi | Free Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="SiteSmartAi | Free Gift">
    <meta name="description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta name="keywords" content="SiteSmartAi | Free Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.sitesmartai.com/free-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="BizOmart">
    
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="SiteSmartAi | Free Gift">
    <meta property="og:description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta property="og:image" content="https://www.sitesmartai.com/free-gift/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="SiteSmartAi | Free Gift">
    <meta property="twitter:description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta property="twitter:image" content="https://www.sitesmartai.com/free-gift/thumbnail.png">

   <!-- Font-Family -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

    
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://sellero.co/special/';
		$_GET['name'] = 'BizOmart';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
                    <div class="white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 gradient-clr">BizOmart's</span> for SuperVIP Customers
                    </div>
                    <div class="uy mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <span class="f-20 f-md-24 gradient-clr w600">Congratulations!</span> you have also <span class="gradient-clr w600">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "ADMIN35" </span> for my Lightning Fast Software <span class="gradient-clr w600">"SiteSmartAi"</span> launch on <br class="d-none d-md-block"> <span class="gradient-clr w600">12th October 2023  @ 11:00 AM EDT</span>
                        </div>
                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>
                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com" target="_blank" class="gradient-clr">https://support.bizomart.com</a>
                        </div>
                    </div>
                </div>
               	<div class="col-12 mt-md50 mt20 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:75px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"></stop>
                            <stop offset="1" stop-color="#07c2fd"></stop>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"></linearGradient>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"></stop>
                            <stop offset=".22" stop-color="#f88038"></stop>
                            <stop offset=".67" stop-color="#e74352"></stop>
                            <stop offset="1" stop-color="#da1467"></stop>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"></path>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"></path>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"></path>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"></path>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"></rect>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"></rect>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"></path>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"></path>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"></path>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"></path>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"></path>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"></path>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"></path>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"></path>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"></path>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"></path>
                            </g>
                          </g>
                        </g>
                      </svg>
               	</div>
				   <div class="col-12 mt-md25 mt20 f-md-50 f-28 w500 text-center white-clr lh140">
					World's First A.I App Builds Us <span class="orange-gradient w700"> Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content… 
					</div>
					<div class="col-12 mt-md20 mt20 f-22 f-md-26 w700 text-center lh140 black-clr text-capitalize">
					<div class="head-yellow">
					Then Promote It To Our Secret Network of 492 MILLION Buyers For 100% Free…
					</div> 
					</div>
					<div class="col-12 mt-md15 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
					Now Create &amp; Sell Stunning Eye-Catching Business Websites For Architect, Dentist, Fitness, Lawyer, Painter, Restaurant, Finance, Travel, Pet, and 1000s of Other Niches <u>with Just A Keyword &amp; Charge An Instant $1,000 Per Website</u>
					</div>
					<div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
					Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
					</div>
				<div class="col-12 white-clr text-center">
					<div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
						Download Your Free Gifts Below
					</div>
					<div class="mt10 f-20 f-md-32 lh160 ">
						<span class="tde">Enjoy Your Extra Bonus Below</span>
					</div>
				</div>
				<div class="col-12 mt20 mt-md50">
					<img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
			</div>
			<!-- Bonus-1 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div> 
						
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Time Management Graphics Pack
                  	</div>
                  	<div class="f-20 w600 lh140 white-clr mt20 mt-md30">
					  This graphics pack has 50 time management image. Images are delivered in PNG and Vector PDF formats. 
                  	</div>
                 	<div class="f-20 w400 lh140 white-clr mt20">
					 You can use graphics to add appeal to your blog posts, marketing and more.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/mr44kenm" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-2 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div> 
                     	
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Elite Presentation Kit 
                  	</div>
                  	<div class="f-20 w600 lh140 white-clr mt20 mt-md30">
					  Stunning & Professional Presentation That Can Easily Impress Your Clients/Prospects, Increase Your Credibility, And Close Any Deals. Even IF Your Never Create A Presentation Before!
                  	</div>
                  	<div class="f-20 w400 lh140 white-clr mt20">
					  It's not a secret that first impression is THE BEST impression. And winning a business deal is that critical part of your business, but does it ALWAYS depend on what you sell?
                  	</div>
					  <div class="f-20 w400 lh140 white-clr mt20">
					  Not necessarity. But... Presentation matters! With growing competition in every industry, you must stand first and ahead of others to survive and run any business. 
                  	</div>
					  <div class="f-20 w400 lh140 white-clr mt20">
					  In this scenario, you are ought to close all the deals you receive. Does this scare you? But don't worry! This is to communicate your idea tactically to your clients.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/mr3wryfb" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-3 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div> 
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Web Security Graphics
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  Many people process information better when it is presented in a visual manner. Use these web security graphics to add appeal to your blog posts, marketing, PLR content and so much more.
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  According to many Neuro-Linguistic Programming practitioners, human minds works visually in which our imagination, calculations and memories takes place.
                  	</div>
					  <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  The good thing to this is that, you can utilize this human attributes to your marketing efforts by simply providing people a good graphics presentation of your products or services.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/3s7ufddd" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-4 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div> 
                     	
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Funtastic Fonts 
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  If you are having a hard time finding just the right font for your projects then this collection is a must.
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  "Funtastic Fonts" is a huge collection of over 6,500 TrueType fonts!
                  	</div>
					  <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  Sure you could do those projects using fonts like "Arial" or "Tahoma", but why not jazz up those same presentations with some super fonts.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/5285sbyn" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-5 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div>
						 
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Money Graphics Pack
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  This graphics pack has 150 money related images. The images will be delivered in PNG and Vector PDF format!
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  If you are an online entrepreneur selling digital products, a blogger, affiliate marketer, freelance web graphics designer having a pre-made graphics that are relevant to your needs or your client's needs will give you more time to other things that will make your more productive.
                  	</div>
					<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					Images are indeed powerful to persuade your reader's understanding and attention for you to make them turn into a customer to the products that you promote or sell online.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/4t3csfkr" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-6 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 6</div>
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Elegant Press 
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  Finally... Now You Really Can Create Your Own PROFESSIONAL & ATTRACTIVE Presentation in Just 30 Minutes or Less!
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  Want to Create a Professional Presentation with Little Time and Skill? Now You Can Make YOUR Presentation Amazing in Seconds Without the Headache or Hassle of Learning PowerPoint! Create a Professional Presentation To Impress and Amaze Your Audience!
                  	</div>
					<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
						A wise man once said that 'Presentation is Everything'. It's such a timeless quote because it's true in so many areas of life.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/yc4dmtjz" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus6.webp" alt="Bonus 6" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-7 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 7</div>
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  125 Pro Header Templates 
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  125 high quality header image templates complete with PSD and blank jpg files - There Should Be Something For Everyone In This 125 Header Pack!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/fdduer55" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus7.webp" alt="Bonus 7" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-8 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 8</div>
                     
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  3D Box Templates 
                  	</div>
                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  3D Box Templates allow you to create great looking eBox covers in just a few simple steps; Upload template, Enter text, Set color and you're done!
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  Each template comes in 3 versions; with text, without text and with .psd source you you can make changes!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/2rm2zyas" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus8.webp" alt="Bonus 8" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-9 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 9</div>	
                  	</div>
					  <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  Evergreen Infographics Pack
                  	</div>

                  	<div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30">
					  One of the most important types of visuals for a business owners in the online arena are infographics.
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  Infographics are used to showcase statistics, to sell products, to advertise, promote, attract and connect better with your audience.
					  It is the visual representation that you showcase to your potential customers and that sends a clear message to them!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/y5fuyafr" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus9.webp" alt="Bonus 9" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-10 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 10</div><br>
                  	</div>
					<div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					   Go Graphics 
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20 mt-md30">
					  	You're About To Discover An Awfully Simple-But-Powerful System That Can Do All The E-Cover, CD & Membership Pass Works For You ... as easy as 123!
                  	</div>
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  	NO Waiting And NO Fancy Graphic Designing Gizmo, Knowledge & Experience Required - As Long As You Can Scroll And Click, You TOO Can Design Superb Covers For Your Digital Products!
                  	</div>
					<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
						Introducing The Awfully-Simple-But-Superb Cover Designing System - 123 Go Graphics - Finally! You Can Now Churn Out Unlimited Instant HIGH Quality 3D Covers!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/ycxvum36" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus10.webp" alt="Bonus 10" class="mx-auto d-block img-fluid">
               	</div>
            </div>
		</div>
	</div>

	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
			   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:75px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #8b27ff;
                            }
                      
                            .cls-3 {
                              fill: #fff;
                            }
                      
                            .cls-4 {
                              fill: #df325c;
                            }
                      
                            .cls-5 {
                              fill: #ff992e;
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#7c26d5"></stop>
                            <stop offset="1" stop-color="#07c2fd"></stop>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"></linearGradient>
                          <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#ff992e"></stop>
                            <stop offset=".22" stop-color="#f88038"></stop>
                            <stop offset=".67" stop-color="#e74352"></stop>
                            <stop offset="1" stop-color="#da1467"></stop>
                          </linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"></path>
                              <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"></path>
                              <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"></path>
                              <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"></path>
                              <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"></rect>
                              <rect class="cls-4" x="121.26" width="18.98" height="18.98"></rect>
                              <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"></path>
                              <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"></path>
                            </g>
                            <g>
                              <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                              <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"></path>
                              <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                              <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"></path>
                              <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                              <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"></path>
                              <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"></path>
                              <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"></path>
                              <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                              <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"></path>
                              <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"></path>
                              <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"></path>
                            </g>
                          </g>
                        </g>
                      </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © SiteSmartAi 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sitesmartai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
   <title>SiteSmartAi Special</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="SiteSmartAi Special">
   <meta name="description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta name="keywords" content="SiteSmartAi Special">
   <meta property="og:image" content="https://www.sitesmartai.com/special/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Pranshu Gupta">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="SiteSmartAi Special">
   <meta property="og:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta property="og:image" content="https://www.sitesmartai.com/special/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="SiteSmartAi Special">
   <meta property="twitter:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta property="twitter:image" content="https://www.sitesmartai.com/special/thumbnail.png">
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
   <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
   <script src="https://cdn.oppyotest.com/launches/webgenie/common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <script>
      (function(w, i, d, g, e, t, s) {
         if (window.businessDomain != undefined) {
            console.log("Your page have duplicate embed code. Please check it.");
            return false;
         }
         businessDomain = 'sitesmartai';
         allowedDomain = 'sitesmartai.com';
         if (!window.location.hostname.includes(allowedDomain)) {
            console.log("Your page have not authorized. Please check it.");
            return false;
         }
         console.log("Your script is ready...");
         w[d] = w[d] || [];
         t = i.createElement(g);
         t.async = 1;
         t.src = e;
         s = i.getElementsByTagName(g)[0];
         s.parentNode.insertBefore(t, s);
      })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
   </script>
   <!-- New Timer  Start-->
   <?php
         $date = 'October 20 2024 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();      
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";    
         }
         ?>
      <!-- New Timer End --> 
</head>
<body>
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:80px;">
                  <defs>
                     <style>
                     .cls-1 {
                        fill: url(#linear-gradient-2);
                     }
               
                     .cls-2 {
                        fill: #8b27ff;
                     }
               
                     .cls-3 {
                        fill: #fff;
                     }
               
                     .cls-4 {
                        fill: #df325c;
                     }
               
                     .cls-5 {
                        fill: #ff992e;
                     }
               
                     .cls-6 {
                        fill: url(#linear-gradient-3);
                     }
               
                     .cls-7 {
                        fill: url(#linear-gradient);
                     }
                     </style>
                     <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79" gradientUnits="userSpaceOnUse">
                     <stop offset="0" stop-color="#7c26d5"></stop>
                     <stop offset="1" stop-color="#07c2fd"></stop>
                     </linearGradient>
                     <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"></linearGradient>
                     <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                     <stop offset="0" stop-color="#ff992e"></stop>
                     <stop offset=".22" stop-color="#f88038"></stop>
                     <stop offset=".67" stop-color="#e74352"></stop>
                     <stop offset="1" stop-color="#da1467"></stop>
                     </linearGradient>
                  </defs>
                  <g id="Layer_1-2" data-name="Layer 1">
                     <g>
                     <g>
                        <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"></path>
                        <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"></path>
                        <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"></path>
                        <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"></path>
                        <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"></rect>
                        <rect class="cls-4" x="121.26" width="18.98" height="18.98"></rect>
                        <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"></path>
                        <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"></path>
                     </g>
                     <g>
                        <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                        <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"></path>
                        <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                        <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"></path>
                        <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                        <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"></path>
                        <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"></path>
                        <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"></path>
                        <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                        <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"></path>
                        <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"></path>
                        <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"></path>
                     </g>
                     </g>
                  </g>
               </svg>
            </div>
            <!-- <div class="col-md-7">
               <ul class="leader-ul f-md-20 f-18 w500 white-clr text-md-end text-center">
                  <li><a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                  <li><a href="#demo" class="white-clr t-decoration-none">Demo</a><span class="pl9 white-clr d-md-none">|</span></li>
                  <li class="d-md-none"><a href="#buynow" class="white-clr t-decoration-none">Buy Now</a></li>
               </ul>
            </div> 
            <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block">
               <a href="#buynow">Buy Now</a>
            </div>-->
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w400 lh140 white-clr">
               We Exploited ChatGPT4 To Create A Better And Smarter AI Model...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-45 f-28 w400 text-center white-clr lh130 head-design">
               World's First AI App Builds Us <span class="orange-gradient w700">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
            <div class="col-12 mt-md30 mt20 f-18 f-md-24 w700 text-center lh140 white-clr text-capitalize">
            Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-18 f-md-21 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
               Now Create & Sell Stunning Sites for Restaurants, Dentists, Gyms, Coaches, Affiliates, Marketers, Lawyers, And 1000s of Other Niches With <u>Just A Keyword & Charge Them Instant $1,000 Per Site</u>
               </div> 
            </div>
            <div class="col-12 mt-md20 mt20 f-20 f-md-22 w600 text-center lh140 orange-clr1 text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
           
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="f-18 f-md-20 w600 lh140 text-center white-clr blue-design">
                     Watch How With Just 3 Clicks We Deploy Profitable AI-Managed Websites With ZERO Work...
                     </div>
                     <div class="col-12 mt20">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://sitesmartai1.oppyo.com/video/embed/axydjih9d1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="border-dashed">
                           <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                              First 99 Action Taker Get Instant Access To Our Top 5 <br class="d-none d-md-block"> Profitable DFY Websites For <span class="yellow-clr w700">FREE</span> 
                           </div>
                           <div class="f-18 f-md-20 w500 lh140 text-center yellow-clr">
                              (Average User Saw 475% Increase In Profit <span class="w700">worth $1,997</span>)
                           </div>
                        </div>
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20 mt-md30">
                           Get SiteSmartAi For Low One Time Fee… <br class="d-none d-md-block"> Just Pay Once Instead Of <s class="red-clr w600">Monthly</s> 
                        </div>
                        <div class="row">
                           <div class="col-md-12 mx-auto col-12 mt20 mt-md30 text-center ">
                              <a href="#buynow" class="cta-link-btn px0 d-block">>>> Get Instant Access To SiteSmartAi >>> </a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable-Gurantee1" class="img-fluid d-block mx-auto " >
                        </div>
                     </div>

                     <div class="col-12 mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w300 white-clr">
                           <li>Launch Stunning Websites within Minutes<span class="w500">- 100% On Autopilot</span></li>
                           <li>AI Will Tell You Exactly How Much You Gonna Earn <span class="w500">BEFORE Doing Any Work…</span></li>
                           <li>All Websites Are Prefilled With <span class="w500">Smoking Hot, AI, Human-Like Content</span></li>
                           <li><span class="w500">Instant High Converting Traffic For 100% Free</span></li>
                           <li><span class="w500">High Ticket Affiliate Offers</span> For DFY Monetization</li>
                           <li><span class="w500">We Let AI Do All The Work For Us.</span></li>
                           <li>Instantly <span class="w500">Tap Into $1.3 Trillion Platform</span> </li>
                           <li><span class="w500">Promote Offers from ClickBank, W+, & JVZoo</span></li>
                           <li><span class="w500">Build a Huge List</span> with Inbuilt Forms & Popups </li>
                           <li><span class="w500">100% SEO Optimized & Mobile Responsive</span></li>
                           <li><span class="w500">No Complicated Setup -</span> Get Up and Running In 2 Minutes</li>
                           <li><span class="w500">ZERO Upfront Cost </span>or Any Extra Expenses</li>
                           <li><span class="w500">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Step Section Start -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to SiteSmartAi App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;"><source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish & Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;">
                        <source src="assets/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Step Sction End -->

   <!-- Cta Section -->
   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">>>> Get Instant Access To SiteSmartAi  <<<</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0 " alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0 " alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Cta Section End -->


   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
                  SiteSmartAi Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with SiteSmartAi… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     SiteSmartAi eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With SiteSmartAi you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because SiteSmartAi is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using SiteSmartAi
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->


   <!-- Cta Section -->
   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">>>> Get Instant Access To SiteSmartAi  <<<</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto " alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Cta Section End -->

  

   <!--Are You Ready Section Start -->
   <div class="areyouready-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Are You Ready For An A.I. Revolution?
            </div>
            <div class="col-12 col-md-7 mx-auto f-28 f-md-38 lh140 w600 text-center white-clr red-brush mt20">
               ChatGPT4 And AI Pay Us Daily…
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               There is nothing that AI can't do in the digital world… It's the most powerful technology that exists now… And SiteSmartAi is the only app on the market…<br><br>
               <span class="w600 lh120">
               That harnessed that power, and created something that makes us money day after day
            </span>
            </div>
            <img src="assets/images/proof11.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               Every day we make money without doing any work… And not pocket change…<br><br>
               <span class="w600 lh120">
               Last month alone we made a bit over $17,000 with SiteSmartAi
               </span>
            </div>
            <div class="col-12 col-md-10 mx-auto">
               <img src="assets/images/proof2.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            </div>
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Are You Ready Section Ends -->


   <!--Profitable Section Start -->
   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="easy-text f-28 f-md-65 w700 lh140 white-clr mx-auto">Easy</div>
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center mt20">
                  We Created <u><span class="w600">PROFITABLE</span></u> Websites…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  It's pretty straightforward…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We have dozens of websites across multiple niches…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All of them work to make us money…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               The best part is, we don't have to do anything
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't promote, run ads, create content… or any of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They work on complete autopilot to make us money like this…
               </div>
               <img src="assets/images/profitable-ss.webp" alt="screenshort" class="mx-auto d-block img-fluid mt20 "><br>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if you think creating a website is “old news”
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Let me explain something, my friend…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/profitable-img.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Profitable Section Ends -->

   <!--Don't Have a Website Section Start -->
   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  If You Don't Have A Website,<br>
                  <span class="red-clr w700">
                     You Can't Make Money!
                  </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md40">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
                  It's very simple…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If you don't have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You are going to face a very hard time trying to make money online…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  I mean, that's the idea of the Internet to start with…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  To have websites, and users can view it, and use it…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  When was the last time you saw a successful business…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or even an online guru…
               </div>
               <div class="f-18 f-md-20 w600 lh140 mt20 black-clr text-center text-md-start">
                  Without a website?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Every one of them MUST have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even we…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You're reading this letter now on a website we own…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/website-img.webp" alt="Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
      </div>
   <!--Don't Have a Website Section Ends -->


   <!--Take a look Section Start -->
   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center mt20">
                  I Mean… Take A Look At This…
                  <img src="assets/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
                  There are about 1.13 Billion WEBSITE ON THE INTERNET IN 2023
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  According to Forbes.com…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               There is over A BILLION website on the internet…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Why do you think there are that many?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               There must be a reason, <span class="w600">Right? </span> 
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               And there is no sign of slowing down…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Because according to Forbes also…
               </div>
            </div>
            <div class="col-12 col-md-6">
               <img src="assets/images/billion-website.webp" alt="Billion Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12">
               <img src="assets/images/website-internet.webp" alt="Website Internet" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
               A new website built every three seconds
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Every 3 seconds… can you believe it?
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Finally…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               If you think that websites are not essential to make money…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Take a look at this…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/new-website.webp" alt="New Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12">
               <img src="assets/images/website-buils.webp" alt="Website Buils" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="col-12 mt20 mt-md50 text-center">
            <div class="f-18 f-md-38 w600 lh140 white-clr text-center red-head">
               71% of business have a  website in 2023
            </div>
         </div>
      </div>
   </div>
   <!--Take a look Section Ends -->

   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->
   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  But… Creating A Website Is <span class="red-clr w700">NOT Easy!</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  I'm sure you have tried it before…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or at least have a general idea of how it works…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If not, let me tell you…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  It's a very difficult, and demanding task…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Here, let me break it down for you…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/not-easy-img.webp" alt="Not Easy" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Ends -->

   <!-- Requires Section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 w400 lh140 black-clr text-center text-md-start">
               It Requires <span class="w700">HUGE Techincal Knowledge…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There is a reason why professionals can charge you up to $20,000 for a website creation…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It requires years of experience…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Knowledge of multiple programming languages…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And a vast knowledge of server management…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And to the average guy like you and me…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's very… VERY complicated… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's why many people try to start a website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And abandon it midway…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or worse, create a horrible, slow, and non-functional website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/knowledge-img.webp" alt="Knowledge" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Requires Section Ends -->


   <!-- Content Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
               And If That Wasn't Enough… <span class="w600">You Need Content For That Website…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               I'm sure you have tried it before…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Or at least have a general idea of how it works…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               If not, let me tell you…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               It's a very difficult, and demanding task…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Here, let me break it down for you…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/content-img.webp" alt="Content" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Content Section Ends -->    
    
    <!-- Expensive Section Starts -->
   <div class="blue-section">
      <div class="container">
      <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
                     Creating Content Is <span class="red-clr w700">EXPENSIVE…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               A successful website needs high-quality content…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And let me tell you this…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               A quality article will cost you at least $100 to get done…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So imagine if you wanna create a website with just 100 articles (which is very low)
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's not counting
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               &#x2022; &nbsp;&nbsp;   Proofreading<br>
               &#x2022; &nbsp;&nbsp;   Editing<br>
               &#x2022; &nbsp;&nbsp;   Formatting<br>
               &#x2022; &nbsp;&nbsp;   Designing<br>
               &#x2022; &nbsp;&nbsp;   Publishing<br>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And still, that's for just one batch of articles…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The same for videos, pictures, infographics, and even podcasts…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/expensive-img.webp" alt="Expensive" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Requires Section Ends -->


   <!-- Its Even Harder Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
               And If You Decided To Do Yourself…
               </div>
               <div class="f-18 f-md-50 w700 lh140 black-clr text-center text-md-start">
               It's Even Harder…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt10 black-clr text-center text-md-start">
               You will need to do tons of research…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               And spend hours each day for months…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Writing, editing, and formatting…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               And all of that is just for one website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Imagine if you wanna build maybe 10?
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               It's just not feasible…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/its-even-harder-img.webp" alt="Even Harder" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
<!-- Its Even Harder Section Ends -->

<!-- You Still Need Traffic Section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-7">
               <div class="f-18 f-md-28 w600 lh140 black-clr text-center text-md-start">
               And After All Of That…
               </div>
               <div class="f-18 f-md-50 w700 lh140 black-clr text-center text-md-start">
               You Still Need Traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If somehow you managed to do all of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You will still need to spend 10x more that time and money...
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To get traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Doesn't matter if you're going for free or paid traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The paid one will cost you a fortune to just test it…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You will need at least $10,000 set aside for that purpose…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And who knows if its going to work or not…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And the free methods like SEO are no joy either…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It takes months (sometimes years) to see any significant results from it…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/need-traffic-img.webp" alt="Need Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- You Still Need Traffic Section Ends -->

   <!-- Beginner Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-38 w700 lh140 mt20 black-clr text-center text-md-start">
               It's Just Not Easy For A Beginner…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               There are a lot of moving parts…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               A lot of skills that you need to learn first…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               I mean, yea it's still wildly profile…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               But need a lot of dedication and hard work…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/beginner-img.webp" alt="Beginner" class="mx-auto d-block img-fluid ">
               </div>
         </div>
      </div>
   </div>
   <!-- Beginner Section Ends -->

   <!-- Another Way Section Starts -->
   <div class="another-way-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 white-clr text-center">
                  But What If I Told You <span class="orange-gradient w600">There Is Another Way…?</span>
               </div>
               <div class="f-18 f-md-26 w400 lh140 white-clr text-center">
                  A way that will let you skip all of that, A way that will put you in front of everyone else…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="imagine-box">
               <div class="row align-items-center">
                  <div class="col-12 col-md-4">
                     <img src="assets/images/imagine-man.webp" alt="Imagine Man" class="mx-auto d-block img-fluid ">
                  </div>
                  <div class="col-12 col-md-8">
                     <div class="f-22 f-md-32 w400 lh140 black-clr text-center text-md-start">
                        Imagine if you can do all of what we talked about above…
                     </div>
                     <div class="clicks-box mt20">
                        <div class="f-22 f-md-38 w700 lh140 text-center orange-gradient click">
                           But with just 3 clicks…
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <div class="f-28 f-md-50 w600 lh140 white-clr text-center">
                  That's it
               </div>
               <div class="f-18 f-md-26 w400 lh140 white-clr text-center ">
                  And it makes us daily commissions like this
               </div>
               <div class="arrow-down-2 mt20 mt-md40">
                  <img src="assets/images/proof3.webp" alt="Proof" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Another Way Section Ends -->


   <!-- Work For You Section Starts -->
   <div class="workforyou-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  A Way That Does 99% Of The Work For You
               </div>
               <div class="f-18 f-md-38 w400 lh140 black-clr text-center">
                  With SiteSmartAi your work is done for you by AI
               </div>
               <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20 mt-md50">
                  You don't need to:
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/dont-need1.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Create or setup any website
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/dont-need2.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Write any content
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/dont-need3.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 " >
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Design anything
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/dont-need4.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Run ads or work on SEO
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12 text-center mt20 mt-md50">
               <div class="done-text f-28 f-md-50 w700 lh140 black-clr mx-auto">All of that is done for you…</div>
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                     Every day we use SiteSmartAi to make money like this:
                  </div>
                  <div class="proof-arrow mt20 mt-md40">
                     <img src="assets/images/proof55.webp" alt="No Content Writing" class="img-fluid ">
                  </div>
                  <div class="col-12">
                     <div class="f-28 f-md-42 w600 lh140 black-clr text-center mt20">
                        WITHOUT doing any of the work…
                        <img src="assets/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <!-- Work For You Section Ends -->
   <div class="any-niche-sec">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="text-center mx-auto f-28 f-md-45 w700 white-clr">
               <div class="blue-bg">
                  Create Stunning AI Websites In ANY Niche…
               </div>
            </div>
         </div>
      </div>
      <div class="row align-items-center mt-md50">
         <div class="col-12 col-md-6 order-md-2">
            <div class="f-20 f-md-22 w400 mt20 text-start white-clr">
            If you are thinking that it makes websites with done-for-you content, images and design for only few niches like dentists, lawyers, course sellers, affiliate marketers etc…
            </div>
            <div class="f-20 f-md-22 w400 mt20 text-start white-clr">
            Then, You are wrong my friend…
            </div>
            <div class="f-20 f-md-22 w400 mt20 text-start white-clr">
            Well, you can create a website for these niches if you want…
            </div>
            <div class="f-20 f-md-22 w700 mt20 text-start white-clr">
            But SiteSmartAI is much much bigger than this…
            </div>
         </div>
         <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
            <img src="assets/images/any-niche-ai.webp" alt="Conquer" class="mx-auto img-fluid d-block">
         </div>
      </div>
      <div class="any-niche-white">
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
            <div class="col mt10 mt-md0">
               <div class="figure">
                  <img src="assets/images/ai1.webp" alt="Sports News" class="mx-auto d-block img-fluid">
                  <!-- <div class="figure-caption">
                     <div class="f-22 f-md-28 w400 lh140 text-center black-clr">
                        Sports News
                     </div>
                  </div> -->
               </div>
            </div>
            <div class="col mt10 mt-md0">
               <div class="figure">
                  <img src="assets/images/ai2.webp" alt="Technology News" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="col mt10 mt-md0">
               <div class="figure">
                  <img src="assets/images/ai3.webp" alt="Celebrity News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai4.webp" alt="Stock Market News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai5.webp" alt="Business News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai6.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai7.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai8.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai9.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai10.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai11.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col mt10 mt-md80">
               <div class="figure">
                  <img src="assets/images/ai12.webp" alt="Politics News" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
      </div>
      <div class="row mt20 mt-md50">
         <div class="col-12">
            <div class="mx-auto f-22 f-md-28 w600 white-clr pointer text-center">
            You can create STUNNING WEBSITES Like These in 1000s Of Niches with SiteSmartAi... All with just a click.
            </div>
         </div>
      </div>
   </div>
</div>
<div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto " alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">58&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">31</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Got You Covered Start -->
   <div class="got-you-covered-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
               Even If You Have Nothing To Sell, <span class="w600">SiteSmartAi Got You Covered…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Doesn't matter if you have a product or not…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Because SiteSmartAi comes with countless DFY offers…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That you can start using within seconds…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               And earn up to $997 per sale… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Without waiting for approval…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Paid directly to your bank account…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/got-you-covered-img.webp" alt="Got Covered" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Got You Covered Ends -->

   <!-- Works Globally Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
                  It Doesn't Matter Where You Live, <span class="w600">SiteSmartAi Works Globally…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Doesn't matter if you're in the US, India, Or in the middle of the desert…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  As long as you have a working internet connection…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  You can use <span class="w600">SiteSmartAi</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  And create dozens of profitable websites…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/works-globally-img.webp" alt="Work Globally" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Works Globally Section Ends -->

   <!-- With Few Clicks Section Starts -->
   <div class="with-few-clicks-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
               Imagine Creating A Stunning And Profitable Website <span class="w700"> With A Few Clicks…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
               That's all it takes…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               A few clicks, nothing more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We eliminated the need for any kind of technical experience…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we also eliminated the need to hire or buy any external help…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With our fool-proof system…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And one can create a website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/few-clicks-img.webp" alt="Few Clicks" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- With Few Clicks Section Ends -->


   <!-- Dominates Section Starts -->
   <div class="dominates-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  SiteSmartAi Allowed Us Dominate The Entire WORLD Market…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  Here is what makes SiteSmartAi really powerful… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  It doesn't only work with English content or English markets…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  SiteSmartAi is capable of creating and operating <span class="w600">websites in 28 different languages…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  That way, you can dominate any market in any country…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  And since SiteSmartAi will do everything for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  You don't even need to speak that language.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/dominate-img.webp" alt="Dominate" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Dominates Section Ends -->


   <!-- AI Agent section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 w400 lh140 black-clr text-center text-md-start">
               SiteSmartAi Will Be <br><span class="w700 f-md-50">Your A.I. Agent Too…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On each website… SiteSmartAi will have a customer chat box…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Where your viewer can chat and engage with a ChatGPT4-like bot…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To get help on anything they might need in your market…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               This alone is worth its weight in gold…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/ai-agent-img.webp" alt="AI Agent" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- AI Agent section Starts -->

<!-- Zero Monthly Fees Setion Starts -->
  <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-38 w600 lh140 black-clr text-center text-md-start">
                  ZERO Monthly Fees…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Paying monthly for an app isn't fair…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Especially for someone who is just starting out…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  This is why we decided to remove that with SiteSmartAi
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  We are offering you the full power of AI with SiteSmartAi
               </div>
               <div class="f-18 f-md-22 w600 lh140 mt20 black-clr text-center text-md-start">
                  For a small one-time fee…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/zero-monthly-img.webp" alt="Zero Monthly" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Zero Monthly Fees Setion Ends -->
   <div class="no-need-setup" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w700 lh140 text-center white-clr">
                  Here Is What <span class="orange-line">SiteSmart Ai Is Capable Of Doing…</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start white-clr">
                  Build A Fully Functional And Stunning Website With A Click
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start white-clr mt20">
                  Forget all about hiring web designers and paying thousands of dollars in fees…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start white-clr mt20">
                  With SiteSmartAi you get all of that done for you with just a click…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start white-clr mt20">
                  All you need is to choose your niche, and you are good to go.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/stunning-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>
         </div>
         </div>
         <div class="capability-sec" id="features">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Human-Like AI Content
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you generate, will be automatically filled with UNIQUE and most importantly human-like content… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This content is generated by the power of AI to do one thing… 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/human-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate All Content In 28 Languages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Localize all your content automatically in 28 different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  SiteSmartAi will automatically translate all content into different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  So it doesn't matter what country you are trying to target, SiteSmartAi got you covered.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/content-languages.webp" alt="Content Language" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <!-- <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/attention-sucking.mp4" type="video/mp4">
               </video>
            </div> -->
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/ai-images.webp" alt="Content Language" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI Auto Comment On Post & Pages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Let AI engage with each comment you get on your behalf
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With this feature, visitors will get instant answer for their queries
               </div>
             <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Driving more traffic and more engagement to your site
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/optimize-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Automatically Publish Content From ChatGPT
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  SiteSmartAi does everything for you on autopilot…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Even publishing your content…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It will automatically do all the work for you with ChatGPT4…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And publish the content according to your schedule…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/publish-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

      </div>
   </div>
   
   <!-- Dfy Section Start -->
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
                  500+ DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
                  Choose between over 500 DFY stunning templates in all niches possible… 
                  With one click you will be able to change your website design, With zero downtime.
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="capability-sec2">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you create is 100% mobile optimized
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  There are no glitches, no errors…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>


         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Engage & Convert More of Your Traffic, Into Brand Loyalists
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               When you combine red-hot traffic with super engaging business websites, the result can be massive sales for your business.
               </div>
               <div class="f-20 f-md-22 w600 text-center text-md-start black-clr mt20">
               That’s why you’ll love WEBPULL
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/ns1.webp" alt="Engage & Convert More of Your Traffic, Into Brand Loyalists" class="mx-auto d-block img-fluid ">
            </div>
         </div>
   </div>
   </div>
   <div class="no-need-setup1">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-42 w700 text-center black-clr">
               Get 20+ Beautiful Slider Combinations To Engage & Wow Visitors
               </div>
              
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/engage-convert.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
            <div class="f-18 f-md-22 w400 text-center black-clr mt20">
              <span class="w600"> You can customize anything on this beautiful slider.</span> From lead generation forms, images, calls to action text or buttons, headlines, background images and even use videos in almost all 20 combinations. This will help grab your visitor’s attention and skyrocket your conversions at will, period.
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-42 w700 text-center white-clr">
               Convert Any Website Into A Profit Generating Machine That Gets Tons of Leads, Engagement, And Sales
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/ns2.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- SiteSmartAi In Action Section Starts -->
   <div class="in-action-section" id="demo">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 white-clr text-center">
               Watch SiteSmartAi In Action, And See What It Can Do For You…
               </div>
               <div class="mt20">
               <img src="assets/images/blue-arrow.webp" alt="Product Box" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-md-10 col-12 mx-auto mt20">
                     <div class="col-12">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://sitesmartai1.oppyo.com/video/embed/vwl2wgs6k8" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20 mt-md30">
                           Get SiteSmartAi For Low One Time Fee…
                        </div>
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
                        </div>
                        <div class="row">
                           <div class="col-md-12 mx-auto col-12 mt20 mt-md30 text-center ">
                              <a href="#buynow" class="cta-link-btn px0 d-block">&gt;&gt;&gt; Get Instant Access To SiteSmartAi &gt;&gt;&gt; </a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee1" class="img-fluid d-block mx-auto ">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   <!-- SiteSmartAi In Action Section Ends -->

   <!-- Regardless Section Start -->
   <div class="regardless-experience">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  SiteSmartAi Is For Everyone! <br class="d-none d-md-block">
                  Regardless Of Your Experience…
               </div>
               <img src="assets/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  It doesn't matter if you have made money online before or not…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It doesn't matter if you know how a website is made or not…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  As long as you can follow fool-proof instructions…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And can click 3 clicks with your mouse…
               </div>
               <div class="f-20 f-md22 w600 lh140 blue-clr mt20">
                  You're all set…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/regardless-man.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Regardless Section End -->

   <!-- Hey There Section Start-->
   <div class="hi-their-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="assets/images/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart Team
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It's <span class="w600">Pranshu</span> along with <span class="w600">Bizomart Team.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I'm a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It's the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change… 
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>

         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day, I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I'm not telling you this to brag… Not at all… I'm telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Hey There Section End -->

   <div class="goal-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="assets/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="assets/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  You see…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Making money online is good any everything… 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  But what's 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 <span class="w600">I will help 100 new people…</span> 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  So far, it's been going GREAT…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  I've managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/goal.webp" alt="Women" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="success-story-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I'm Here To Help You My Next <br class="d-none d-md-block">
                  Success Story…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  Listen, if you're reading this page right now…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It means one thing, you're dedicated to making money online…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members… 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/successful-man.webp" alt="SuccessFull Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- Demo Section Start -->
   <div class="demo-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="assets/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="assets/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-12 mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;">
                  <source src="assets/images/step3.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using SiteSmartAi</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  We turn it on, and that's pretty much it. As simple as that…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline">  $27,548.65 on one of our affiliate accounts…</span> 
               </div>
               <img src="assets/images/arrow-down-3.webp" alt="Demo Proof" class="d-block img-fluid">
               <img src="assets/images/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>
   <!-- Demo Section End -->

   <!--Proudly Introducing Start -->
   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="presenting-head f-md-38 f-24 w600 text-center white-clr lh140 text-uppercase">
                  Proudly Presenting…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:80px;">
                                <defs>
                                  <style>
                                    .cls-1 {
                                      fill: url(#linear-gradient-2);
                                    }
                              
                                    .cls-2 {
                                      fill: #8b27ff;
                                    }
                              
                                    .cls-3 {
                                      fill: #fff;
                                    }
                              
                                    .cls-4 {
                                      fill: #df325c;
                                    }
                              
                                    .cls-5 {
                                      fill: #ff992e;
                                    }
                              
                                    .cls-6 {
                                      fill: url(#linear-gradient-3);
                                    }
                              
                                    .cls-7 {
                                      fill: url(#linear-gradient);
                                    }
                                  </style>
                                  <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#7c26d5"></stop>
                                    <stop offset="1" stop-color="#07c2fd"></stop>
                                  </linearGradient>
                                  <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"></linearGradient>
                                  <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ff992e"></stop>
                                    <stop offset=".22" stop-color="#f88038"></stop>
                                    <stop offset=".67" stop-color="#e74352"></stop>
                                    <stop offset="1" stop-color="#da1467"></stop>
                                  </linearGradient>
                                </defs>
                                <g id="Layer_1-2" data-name="Layer 1">
                                  <g>
                                    <g>
                                      <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"></path>
                                      <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"></path>
                                      <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"></path>
                                      <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"></path>
                                      <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"></rect>
                                      <rect class="cls-4" x="121.26" width="18.98" height="18.98"></rect>
                                      <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"></path>
                                      <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"></path>
                                    </g>
                                    <g>
                                      <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                                      <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"></path>
                                      <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                                      <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"></path>
                                      <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                                      <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"></path>
                                      <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"></path>
                                      <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"></path>
                                      <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                                      <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"></path>
                                      <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"></path>
                                      <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </svg>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-45 f-28 w400 text-center white-clr lh130 head-design">
               World's First AI App Builds Us <span class="orange-gradient w700">Done-For-You Stunning Websites in 1000s of Niches</span> Prefilled With Smoking Hot Content…
            </div>
            <div class="col-12 mt-md30 mt20 f-18 f-md-24 w700 text-center lh140 white-clr text-capitalize">
            Then Promote It To Our Secret Network Of 492 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-18 f-md-22 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
               Now Create & Sell Stunning Websites For Architect, Dentist, Fitness, Lawyer,  Restaurant, Pet, And 1000s Of Other Niches <u>With Just A Keyword & Charge Instant $1,000 Per Website</u>
               </div> 
            </div>
            <div class="col-12 mt-md20 mt20 f-20 f-md-22 w600 text-center lh140 orange-clr1 text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-10 mx-auto">
               <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto " alt="Product">
            </div>
         </div>
      </div>
   </div>
   <!--Proudly Introducing End -->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Here Is What <span class="orange-line">SiteSmart Ai Is Capable Of Doing…</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Fully Functional And Stunning Website With A Click
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about hiring web designers and paying thousands of dollars in fees…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With SiteSmartAi you get all of that done for you with just a click…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  All you need is to choose your niche, and you are good to go.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/stunning-website11.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Human-Like AI Content
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you generate, will be automatically filled with UNIQUE and most importantly human-like content… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This content is generated by the power of AI to do one thing… 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/human-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate All Content In 28 Languages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Localize all your content automatically in 28 different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  SiteSmartAi will automatically translate all content into different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  So it doesn't matter what country you are trying to target, SiteSmartAi got you covered.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/content-languages.webp" alt="Content Language" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
              <!-- <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/attention-sucking.mp4" type="video/mp4">
               </video>
            </div> -->
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/ai-images.webp" alt="Content Language" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI Auto Comment On Post & Pages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Let AI engage with each comment you get on your behalf
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With this feature, visitors will get instant answer for their queries
               </div>
             <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Driving more traffic and more engagement to your site
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/optimize-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Automatically Publish Content From ChatGPT
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  SiteSmartAi does everything for you on autopilot…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Even publishing your content…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It will automatically do all the work for you with ChatGPT4…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And publish the content according to your schedule…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/publish-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

      </div>
   </div>
  
   <!-- Dfy Section Start -->
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
                  500+ DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
                  Choose between over 500 DFY stunning templates in all niches possible… 
                  With one click you will be able to change your website design, With zero downtime.
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="capability-sec2">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you create is 100% mobile optimized
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  There are no glitches, no errors…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>


         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Engage & Convert More of Your Traffic, Into Brand Loyalists
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               When you combine red-hot traffic with super engaging business websites, the result can be massive sales for your business.
               </div>
               <div class="f-20 f-md-22 w600 text-center text-md-start black-clr mt20">
               That’s why you’ll love WEBPULL
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/ns1.webp" alt="Engage & Convert More of Your Traffic, Into Brand Loyalists" class="mx-auto d-block img-fluid ">
            </div>
         </div>
   </div>
   </div>
   <div class="no-need-setup1">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-42 w700 text-center black-clr">
               Get 20+ Beautiful Slider Combinations To Engage & Wow Visitors
               </div>
              
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/engage-convert.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
            <div class="f-18 f-md-22 w400 text-center black-clr mt20">
              <span class="w600"> You can customize anything on this beautiful slider.</span> From lead generation forms, images, calls to action text or buttons, headlines, background images and even use videos in almost all 20 combinations. This will help grab your visitor’s attention and skyrocket your conversions at will, period.
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-42 w700 text-center white-clr">
               Convert Any Website Into A Profit Generating Machine That Gets Tons of Leads, Engagement, And Sales
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/ns2.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="capability-sec2">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with SiteSmartAi AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let SiteSmartAi do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Integration With WooCommerce
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Selling physical products?
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  No problem, SiteSmartAi integrates with wooCommerce in seconds…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And it will allow you to operate your store with no hassle…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/integration-woocommerce.webp" alt="Integration With WooCommerce" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In CRM
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Manage all your leads from one central dashboard…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  SiteSmartAi comes with a powerful CRM that will easily do all the heavy lifting for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/crm.webp" alt="Built-In CRM" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Appointment Booking
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Does your business requires setting appointments?
               </div>
               <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">
                  Easy.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Let SiteSmartAi does that for you on autopilot with its powerful appointment-setting features
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of SiteSmartAi will come with a commercial license, so you can use it on your client website too.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>

      </div>
   </div>


   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="conquar-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 text-center">
                  Conquer ANY Niche…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  It doesn't matter what niche you are trying to get into…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  SiteSmartAi will let you dominate ANY niche in any country…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  All it takes is just a few clicks…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  And you will have a STUNNING and most importantly…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                 <span class="w700">PROFITABLE…</span> website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/conquer-girl.webp" alt="Conquer" class="mx-auto img-fluid d-block">
            </div>
         </div>
         <div class="regardless-sec">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w400 text-center white-clr dotted-border">
                     SiteSmartAi Is For You…
                  </div>
                  <div class="f-28 f-md-50 w700 mt20 text-center orange-gradient">
                     Regardless Of What You Are Doing…
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 text-center white-clr mt10">
                     Choose between over 500 DFY stunning templates in all niches possible… <br class="d-none d-md-block">
                     With one click you will be able to change your website design, With zero downtime.
                  </div>
                  <div class="f-20 f-md-22 w700 lh140 text-center white-clr mt10">
                     It work for…
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="no-need-setup1">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr"><u> NO More</u></span>  Tech Setup…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Creating a website is a must for any business…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The issue is, it's not easy…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You need great technical knowledge in order to create a website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if you decided to hire someoen to do it for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will end up paying thousands of dollars in fees…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And even after that, it won't even be just like you wanted…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  They will charge you extra for any tiny edits you wanna do
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  But with SiteSmartAi, You will skip all of that…
               </div>
               <ul class="cross-list pl0 m0 f-18 f-md-20 w400 black-clr lh140  mt10">
                  <li>No Website Creation</li>
                  <li>No Programming Required</li>
                  <li>No Complicated Setup</li>
                  <li>No Hiring Anyone</li>
               </ul>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Within seconds, SiteSmartAi will create you a unique, and stunning website in any niche, in any language.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/no-setup.webp" alt="No Setup" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="chatgpt-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  ChatGPT4 And AI Changed All Of That…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No, all you need is AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't need anything else.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if you know how to use it, you are set to financial freedom, my friend…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  SiteSmartAi leverages the BEST AI model known…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  To give you all that power in your hands… On complete autopilot…
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  By using it, it allowed us to turn our account from this:
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  [$0 bank account]
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20 arrow-down">
                  To this
               </div>
               <img src="assets/images/proof.webp" alt="Proof SS" class="mx-auto d-block img-fluid mt20 mt-md24 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  In just a few days…
               </div>
               <div class="text-center text-md-start">
                  <div class="smile-text f-20 f-md-22 w400 lh140 white-clr mt20 mt-md24">Sounds good, huh?</div>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/chatgpt.webp" alt="Chatgpt" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="work-sec"> 
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span>  Does It Work?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We can spend HOURS explaining how everything inside SiteSmartAi works…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's a complicated AI model…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But the thing is, you don't have to learn any of that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Since you are not doing anything in that process…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's all done for you on autopilot…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We spent years developing this robust AI model…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And today, we are giving it to you on a silver platter…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               SiteSmartAi is so powerful…It gets us results <span class="w600">EVERY SINGLE DAY</span> 
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/work-men.webp" alt="Work Men" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="different-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span> Is It Different?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  That's an easy one… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  SiteSmartAi is the only app on the market that does it all for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have to lift a finger to see results with SiteSmartAi…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It will start with creating your website, filling it with content, promoting it… and even monetizing it for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  All of that happens in the background with <span class="w600">zero human interference…</span> 
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/different-girl.webp" alt="Different" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="powerfull-ai">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-38 w400 text-center black-clr dotted-border">
                  With SiteSmartAi…
               </div>
               <div class="f-28 f-md-50 w700 lh140 mt11 black-clr">
               Everything Is Done For You By The Most <br class="d-none d-md-block">Powerful AI Model Ever Created…
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-websites.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Websites</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-content.webp" alt="DFY Content" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Content</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-translation.webp" alt="DFY Translation" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Translation</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-designs.webp" alt="DFY Designs" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Designs</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-seo-optimization.webp" alt="DFY SEO Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY SEO Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-mobile-optimization.webp" alt="DFY Mobile Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Mobile Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-traffic.webp" alt="DFY Traffic" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Traffic</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-social-media-syndication.webp" alt="DFY Social Media Syndication" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Social Media Syndication</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-crm.webp" alt="DFY CRM" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY CRM</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-appointment-schedule.webp" alt="DFY Appointment Schedule" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Appointment Schedule</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-templates.webp" alt="DFY Templates" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Templates</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-woocomerce-integration.webp" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY WooComerce Integration</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <!-- Step Section Start -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to SiteSmartAi App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;"><source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish & Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #8B27FF;">
                        <source src="assets/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Step Sction End -->

   <!-- Need Section Start -->
   <div class="need-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                 <span class="left-line">Everything You Need</span> Is Included…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  SiteSmartAi App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  The app that is responsible for everything… 
                  Get 100% full access to our state-of-the-art AI app… 
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-app.webp" alt="SiteSmartAi App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  AI Content Generator
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  With one click, fill your entire website with human-like content in any niche, and in over 28 languages.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/ai-content-generator.webp" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  SiteSmartAi Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  You don't have to worry about traffic any more, let AI handle that for you.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-built-in-traffic.webp" alt="SiteSmartAi Built-In Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  SiteSmartAi Mobile Optimizer
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  Make your website and your client's website mobile-ready with just a click…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/webgenie-mobile-optimizer.webp" alt="SiteSmartAi Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  SiteSmartAi Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  This will allow you to also operate SiteSmartAi, even from your mobile phone…  <br>
                  Whether it's an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $497)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-mobile-edition.webp" alt="SiteSmartAi Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  Training videos
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  There is NOTHING missing in this training… Everything you need to know is explained in IMMENSE details.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/training-videos.webp" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  Have a question? Just reach out to us and our team will do their best to fix your problem in no time.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/world-class-support.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Need Section End -->

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="get-better-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr red-line">
               But It Gets Better…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  By getting access to SiteSmartAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will immediately unlock access to our custom-made bonuses…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this pack exclusively for SiteSmartAi
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  Our goal was simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Give fast action takers the advantage…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  By giving them whatever they need to achieve 10x the results… In half the time…
               </div>
            </div>
            <div class="col-12 col-md-6">
               <img src="assets/images/better-man.webp" alt="Better Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <!-- Bonus Section Start -->
   <div class="bonus-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Wordpress Automation Secrets
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Automation is the key to success for every successful marketer today. If you're able to save your time and use it for other productive tasks, then you're on the right track.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     This 8-part video course is designed to show you exactly how you can free up your time by quickly and easily automating tedious and boring tasks within your WordPress site!
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Lead Generation On Demand
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     So, to help you to build your leads and make the max from them, this comprehensive guide lays down proven tips and tricks on how you can create lead generation on demand.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Video Training on How To Create a Profitable Authority Blog in Any Niche
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Want to learn how to create a profitable authority blog in any niche that converts, then you're at the right place.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     This useful package helps to boost your authority by giving useful tricks on how to create a profitable blog the right way. Use it and scale your business to the next level.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Social Media Traffic Streams
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Social media is a popular way for businesses to engage with their target audiences. But getting people to your website through social media engagement can be tricky.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     So, don't hang around! This stunning guide will teach you how you can successfully drive traffic from social media to your website.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section End -->

   <div class="join-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  Join The Future… <br class="d-none d-md-block">
                  Join SiteSmartAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Aren't you tired of wasting your money and time……on half-complete apps?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you are…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Then this is your chance to finally jump on something that has been proven to work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Not just for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But for <span class="w600">HUNDREDS</span> of beta-testers…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  With %100 success rate…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We're confident to say that SiteSmartAi is the BEST app on the market right now…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1">
               <img src="assets/images/join-webgenie.webp" alt="Better Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="know-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
                  Wanna Know <span class="red-clr">How Much It Costs?</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  SiteSmartAi does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I make enough money from using SiteSmartAi.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So I don't need to charge you that.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I use it on a daily basis… So I don't see why not I shouldn't give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I'm willing to give you full access to SiteSmartAi.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  For a fraction of the price.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Less than the price of a cheap dinner.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Which is enough for me to cover the cost of the servers running SiteSmartAi.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/costs.webp" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="last-long-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start white-clr">
                  But That Won't Last Long…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20 mt-md30">
                  The last thing on earth I want is to get SiteSmartAi saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  So, sadly I'll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  After that, I'm raising the price to <span class="w600 yellow-clr">$997 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/money-scale.webp" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="decide-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr decide-line">
                  Can't Decide?
               </div>
               <img src="assets/images/decide-line.webp" alt="Red Line" class="d-block img-fluid ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Listen…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's really simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  SiteSmartAi is for you <span class="w600">PERIOD</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Right now, you have the option to change your life With just a few clicks
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/confused-girl.webp" alt="Confused Girl" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="get-better-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-24 f-md-38 w400 lh140 text-center black-clr">
                  We Will Pay You To Fail With SiteSmartAi
               </div>
               <div class="f-28 f-md-45 w700 lh140 text-center black-clr mt10">
                  Our 30 Days Iron Clad <span class="need-head white-clr">Money Back Guarantee</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  We trust our app blindly…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We know it works, after all we been using it for a year… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And not just us… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But hey, I know you probably don't know me… and you may be hesitant… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And I understand that. A bit of skepticism is always healthy… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I can help… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Here is the deal, get access to SiteSmartAi now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Use it, and enjoy its features to the fullest… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if for any reason you don't think SiteSmartAi is worth its weight in gold… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just send us a message to our 24/7 customer support… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  And we will refund every single penny back to you… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No questions asked… Not just that… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We will send you $300 as a gift for wasting your time.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Worst case scenario, you get SiteSmartAi and don't make any money
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 <u> You will still get paid $300 for trying it out. </u>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/money-back-guarantee.webp" alt="Money Back Guarantee" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <!-- Pricing Table Start -->
   <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with SiteSmartAi, You're Getting
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                     <li>Deploy Stunning Websites by Leveraging ChatGPT - <span class="w700">Worth $997/Month</span></li>
                     <li>AI Content Generator - Generate Top-Notch Content on Automation - <span class="w700">Worth $997 </span></li>
                     <li>Fully SEO-Optimized Websites in 28+ Languages - <span class="w700">Worth $297</span></li>
                     <li>SiteSmartAi Built-In Traffic -<span class="w700"> Worth $997 </span></li>
                     <li>500+ Eye-Catching Done-For-You Themes - <span class="w700">Worth $997 </span></li>
                     <li>Sell Unlimited Products, Services or Promote Affiliates Offers -<span class="w700"> Worth $297 </span></li>
                     <li>Generate Attention-Sucking Images from AI -<span class="w700"> Worth $297 </span></li>
                     <li>Get More Viral Traffic with Built-In Social Media Tools <span class="w700"> Worth $297 </span></li>
                     <li>Live Chat from ChatGPT4 AI BOT To Engage with Your Customer <span class="w700">Worth $297 </span></li>
                     <li>100% Mobile Optimizer <span class="w700">Worth $497 </span></li>
                     <li>SiteSmartAi Mobile EDITION <span class="w700">Worth $497 </span></li>
                     <li>Built-In CRM To Manage All Your Leads <span class="w700">Worth $297 </span></li>
                     <li>100% Newbie Friendly - <span class="w700">Beyond A Value </span></li>
                     <li>Step-By-Step Training videos <span class="w700">Worth $997 </span></li>
                     <li>Round-The-Clock Support <span class="w700">Priceless - It's Worth A LOT</span></li>
                     <li>Commercial License - <span class="w700">Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $2,458.34 FREE!!! </li>
                        <li><span class="w700 red-clr">Bonus 1 - </span>Wordpress Automation Secrets</li>
                        <li><span class="w700 red-clr">Bonus 2 - </span>Lead Generation On Demand</li>
                        <li><span class="w700 red-clr">Bonus 3 - </span>Video Training on How To Create a Profitable Authority Blog in Any Niche</li>
                        <li><span class="w700 red-clr">Bonus 4 - </span>Social Media Traffic Streams</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-60 w700 red-clr mt12 l140">
                        $6,795.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     
                     <div class="col-12 mx-auto text-center">
                     <a href="https://warriorplus.com/o2/buy/nqy65h/h2kbnf/s29n55" id="buyButton" class="cta-link-btn mt20"> Get Instant Access To SiteSmartAi</a>
                           </div>
                     <!-- <a href="https://warriorplus.com/o2/buy/nqy65h/h2kbnf/jt52hg"><img src="https://warriorplus.com/o2/btn/fn200011000/nqy65h/h2kbnf/368072" class="d-block img-fluid mx-auto mt20 mt-md30"></a>  -->
                  </div>
                  <div class="table-border-content">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Unlock Secret $2k/Day AI Traffic System</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">Limited to the First 30 Buyers:</b> Enhance your order with an AI-powered traffic source, generating an additional $2k/day using FREE Traffic + Supercharge Your Website with a Faster Server for Quick Publishing, Traffic Flow, and Increased Profits.
                        <br> <br><span class="w700">(97% of Customers Grab This and Witness Immediate Results)</span></p>
                        <p class="f-18 text-center text-md-start"> Get it now for just<span class="green-clr w600">  $9.97 One Time Only</span> <span class="black-clr"><strike>(Normally $247)</strike></span>
                       
                     </p>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Pricing Table End -->

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="#buynow" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To SiteSmartAi  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="get-better-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  Remember…
               </div>
               <img src="assets/images/remember-line.webp" alt="Red Line" class="d-block img-fluid">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After that, you will have to pay $997/mo for it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  While it will still be worth it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 mt20 mt-md0">
               <img src="assets/images/remember.webp" alt="Remember" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="know-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  So, Are you ready yet?
               </div>
               <img src="assets/images/ready-line.webp" alt="Red Line" class="d-block mx-auto img-fluid mt5 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You reading this far into the page means one thing..
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  You're interested.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The bad news is, it will sell out FAST So you need to act now.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/ready.webp" alt="Ready" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

      <!-- CTA Btn Start-->

      <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                  <a href="#buynow"><img src="https://warriorplus.com/o2/btn/fn200011000/nqy65h/h2kbnf/368069"></a> 
                     <div class="mt20 mt-md30 ">
                        <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  We'll see you inside,
               </div>
               <img src="assets/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5 ">
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="assets/images/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart Team
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start">
                  <span class="w600">PS:</span>  If you act now, you will instantly receive [bonuses] worth over <span class="w600">$7,458.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span> There is nothing else required for you to start earning with SiteSmartAi No hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span>  Remember, you're protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                  We will send you <span class="red-clr">$300 out of our own pockets just for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>  If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                  And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <!-- FAQ Section Start -->
   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started?     
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $997/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to SiteSmartAi.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, SiteSmartAi is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried SiteSmartAi and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of SiteSmartAi at a one-time fee 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- FAQ Section End -->
   <!--final section-->
   <div class="final-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 text-center">
               <div class="f-45 f-md-45 lh140 w700 white-clr final-shape">
                  FINAL CALL
               </div>
               <div class="f-22 f-md-32 lh140 w600 white-clr mt20 mt-md30">
                  You Still Have the Opportunity To Raise Your Game… <br> Remember, It's Your Make-or-Break Time!
               </div>
            </div>
         </div>
         <div class="row mt20">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get SiteSmartAi For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr w600">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7 mx-auto col-12 text-center ">
            <a href="https://warriorplus.com/o2/buy/nqy65h/h2kbnf/jt52hg"><img src="https://warriorplus.com/o2/btn/fn200011000/nqy65h/h2kbnf/368072"></a> 
               <div class="mt20 mt-md30 ">
                  <img src="assets/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
               </div>
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   </div>
   <!--final section-->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 713.58 150" style="max-height:80px;">
                                <defs>
                                  <style>
                                    .cls-1 {
                                      fill: url(#linear-gradient-2);
                                    }
                              
                                    .cls-2 {
                                      fill: #8b27ff;
                                    }
                              
                                    .cls-3 {
                                      fill: #fff;
                                    }
                              
                                    .cls-4 {
                                      fill: #df325c;
                                    }
                              
                                    .cls-5 {
                                      fill: #ff992e;
                                    }
                              
                                    .cls-6 {
                                      fill: url(#linear-gradient-3);
                                    }
                              
                                    .cls-7 {
                                      fill: url(#linear-gradient);
                                    }
                                  </style>
                                  <linearGradient id="linear-gradient" x1="48.23" y1="128.3" x2="146.83" y2="10.79" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#7c26d5"></stop>
                                    <stop offset="1" stop-color="#07c2fd"></stop>
                                  </linearGradient>
                                  <linearGradient id="linear-gradient-2" x1="3.88" y1="111.77" x2="105.93" y2="12.79" xlink:href="#linear-gradient"></linearGradient>
                                  <linearGradient id="linear-gradient-3" x1="0" y1="77.39" x2="102.29" y2="77.39" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ff992e"></stop>
                                    <stop offset=".22" stop-color="#f88038"></stop>
                                    <stop offset=".67" stop-color="#e74352"></stop>
                                    <stop offset="1" stop-color="#da1467"></stop>
                                  </linearGradient>
                                </defs>
                                <g id="Layer_1-2" data-name="Layer 1">
                                  <g>
                                    <g>
                                      <path class="cls-7" d="m41.02,137.57c-.08-.07-.17-.14-.26-.22-.18-.07-.36-.13-.54-.2.27.14.53.29.8.42Z"></path>
                                      <path class="cls-1" d="m85.9,32.3l-.06-.09s0,0-.01,0c.04.06.07.09.07.09Z"></path>
                                      <path class="cls-4" d="m104.35,42.22c9.25,7.55,15.93,17.91,18.96,29.52-1.31-10.83-5.47-20.98-11.83-29.52h-7.13Z"></path>
                                      <path class="cls-6" d="m40.23,137.15c.18.07.36.13.54.2.11.04.21.08.32.12-.17-.09-.34-.19-.51-.29-9.68-5.52-23.74-21.6-27.37-41.89,2.95.67,5.85,1.2,8.7,1.61,4.87.7,9.6,1.04,14.19,1.03,3.57,9.56,9.18,17.59,16.15,21.96-5.16-7.22-8.6-14.73-10.81-22.13,24.02-1.48,44.01-12.63,60.86-30.08-20.38,15.41-43.43,21.85-62.31,24.5-2.04-9.24-2.31-18.14-1.76-25.89,11.11-1.36,23.69-5.36,35.6-14.84,4.09,6.66,7.83,15.01,10.22,25.29.79-7.22-.3-18.49-6.39-28.55,2.29-2.08,4.54-4.37,6.74-6.91-2.4,1.58-5.04,3.29-7.93,5.05-4.24-6.24-10.54-11.85-19.69-15.53,7.03-1.52,14.46-1.72,21.98-.36,2.42.44,4.78,1.04,7.07,1.76,0,0,0,0,.01,0,.02,0,.04.01.06.02v-10.06c-4.09-1.72-8.43-3.03-12.98-3.85C39.29,12.22,7.1,34.53,1,68.15c-5.43,29.94,11.67,58.74,39.22,69Zm-1.48-99c5.06-3.17,10.63-5.55,16.51-7,3.82,2.7,10.62,8.32,16.92,17.72-8.82,5.03-19.78,10.1-33.57,13.24,1.47-13.13,5-22.05,5-22.05-5.98,6.23-9.51,14.43-11.01,23.26-2.64.46-5.38.85-8.21,1.16-2.62.28-5.33.5-8.13.63,4.51-11.37,12.51-20.7,22.49-26.96Zm-25.57,37.74c.63-3.45,1.55-6.77,2.75-9.93,2.32.33,4.93.61,7.78.77,2.62.15,5.43.19,8.4.06-.95,8.68-.06,17.77,2.31,26.08-4.68.51-9.03.79-12.93.94-3.15.12-6.01.17-8.51.17-.89-5.75-.94-11.83.19-18.08Z"></path>
                                      <rect class="cls-4" x="90.21" y="11.59" width="25.46" height="25.46"></rect>
                                      <rect class="cls-4" x="121.26" width="18.98" height="18.98"></rect>
                                      <path class="cls-2" d="m121.81,57.97c8.63,2.18,10.59,12.15.49,23.27-15.53,17.09-67.09,39.78-67.09,39.78,0,0,49.16-17.5,72.09-34.42,22.76-16.79,12.52-31.87-5.49-28.63Z"></path>
                                      <path class="cls-2" d="m17.29,126.17c-13.67,12.18-8.12,29.54,19.14,21.99,27.46-7.6,71.57-35.48,71.57-35.48,0,0-49.67,26.56-72.7,28.23-14.98,1.09-21.62-6.61-18-14.75Z"></path>
                                    </g>
                                    <g>
                                      <path class="cls-3" d="m211.03,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                                      <path class="cls-3" d="m246.76,113.99h-12.23v-54.46h12.23v54.46Z"></path>
                                      <path class="cls-3" d="m275.4,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                                      <path class="cls-3" d="m318.33,115c-7.75,0-14.03-2.44-18.85-7.32-4.82-4.88-7.22-11.38-7.22-19.5v-1.51c0-5.44,1.05-10.29,3.15-14.57,2.1-4.28,5.04-7.61,8.83-9.99,3.79-2.38,8.02-3.57,12.68-3.57,7.42,0,13.14,2.37,17.19,7.1,4.04,4.73,6.06,11.43,6.06,20.08v4.93h-35.58c.37,4.5,1.87,8.05,4.5,10.67,2.63,2.62,5.95,3.93,9.94,3.93,5.6,0,10.17-2.26,13.69-6.79l6.59,6.29c-2.18,3.26-5.09,5.78-8.73,7.58-3.64,1.79-7.73,2.69-12.26,2.69Zm-1.46-46.66c-3.36,0-6.06,1.18-8.13,3.52-2.06,2.35-3.38,5.62-3.95,9.81h23.3v-.91c-.27-4.09-1.36-7.19-3.27-9.29-1.91-2.1-4.56-3.15-7.95-3.15Z"></path>
                                      <path class="cls-3" d="m389.15,95.12c0-3.22-1.13-5.7-3.4-7.45-2.26-1.74-6.35-3.51-12.26-5.28-5.91-1.78-10.6-3.76-14.09-5.94-6.68-4.19-10.02-9.66-10.02-16.41,0-5.9,2.41-10.77,7.22-14.6,4.81-3.83,11.06-5.74,18.75-5.74,5.1,0,9.65.94,13.64,2.82,3.99,1.88,7.13,4.56,9.41,8.03,2.28,3.47,3.42,7.32,3.42,11.55h-12.68c0-3.83-1.2-6.82-3.6-8.98-2.4-2.16-5.83-3.25-10.29-3.25-4.16,0-7.39.89-9.69,2.67-2.3,1.78-3.45,4.26-3.45,7.45,0,2.69,1.24,4.92,3.72,6.72,2.48,1.79,6.58,3.54,12.28,5.23,5.7,1.69,10.28,3.62,13.74,5.79,3.46,2.16,5.99,4.65,7.6,7.45,1.61,2.8,2.42,6.08,2.42,9.84,0,6.11-2.34,10.96-7.02,14.57-4.68,3.61-11.03,5.41-19.05,5.41-5.3,0-10.18-.98-14.62-2.94-4.45-1.96-7.9-4.67-10.37-8.13-2.47-3.45-3.7-7.48-3.7-12.08h12.73c0,4.16,1.38,7.38,4.13,9.66,2.75,2.28,6.69,3.42,11.83,3.42,4.43,0,7.76-.9,9.99-2.69,2.23-1.8,3.35-4.17,3.35-7.12Z"></path>
                                      <path class="cls-3" d="m423.37,59.53l.35,5.69c3.83-4.46,9.06-6.69,15.7-6.69,7.28,0,12.26,2.79,14.95,8.35,3.96-5.57,9.53-8.35,16.71-8.35,6.01,0,10.48,1.66,13.41,4.98,2.94,3.32,4.44,8.22,4.5,14.7v35.79h-12.23v-35.43c0-3.45-.75-5.99-2.26-7.6s-4.01-2.42-7.5-2.42c-2.79,0-5.06.75-6.82,2.24-1.76,1.49-2.99,3.45-3.7,5.86l.05,37.35h-12.23v-35.84c-.17-6.41-3.44-9.61-9.81-9.61-4.9,0-8.37,2-10.42,5.99v39.46h-12.23v-54.46h11.53Z"></path>
                                      <path class="cls-3" d="m534.1,113.99c-.54-1.04-1.01-2.73-1.41-5.08-3.89,4.06-8.66,6.09-14.29,6.09s-9.93-1.56-13.39-4.68c-3.46-3.12-5.18-6.98-5.18-11.58,0-5.8,2.16-10.26,6.47-13.36,4.31-3.1,10.48-4.66,18.5-4.66h7.5v-3.57c0-2.82-.79-5.07-2.37-6.77-1.58-1.7-3.98-2.54-7.2-2.54-2.79,0-5.07.7-6.85,2.09-1.78,1.39-2.67,3.16-2.67,5.31h-12.23c0-2.99.99-5.78,2.97-8.38,1.98-2.6,4.67-4.64,8.08-6.12,3.41-1.48,7.21-2.21,11.4-2.21,6.37,0,11.46,1.6,15.25,4.81,3.79,3.2,5.74,7.71,5.84,13.51v24.56c0,4.9.69,8.81,2.06,11.73v.86h-12.48Zm-13.44-8.81c2.42,0,4.69-.59,6.82-1.76,2.13-1.17,3.73-2.75,4.81-4.73v-10.27h-6.59c-4.53,0-7.94.79-10.22,2.37-2.28,1.58-3.42,3.81-3.42,6.69,0,2.35.78,4.22,2.34,5.61,1.56,1.39,3.65,2.09,6.27,2.09Z"></path>
                                      <path class="cls-3" d="m585.84,70.71c-1.61-.27-3.27-.4-4.98-.4-5.6,0-9.38,2.15-11.32,6.44v37.25h-12.23v-54.46h11.68l.3,6.09c2.95-4.73,7.05-7.1,12.28-7.1,1.74,0,3.19.24,4.33.7l-.05,11.48Z"></path>
                                      <path class="cls-3" d="m611.41,46.3v13.24h9.61v9.06h-9.61v30.4c0,2.08.41,3.58,1.23,4.51.82.92,2.29,1.38,4.4,1.38,1.41,0,2.84-.17,4.28-.5v9.46c-2.79.77-5.47,1.16-8.05,1.16-9.4,0-14.09-5.18-14.09-15.55v-30.85h-8.96v-9.06h8.96v-13.24h12.23Z"></path>
                                      <path class="cls-2" d="m672.57,96.93h-28.39l-5.94,17.06h-13.24l27.68-73.28h11.43l27.73,73.28h-13.29l-5.99-17.06Zm-24.81-10.27h21.24l-10.62-30.4-10.62,30.4Z"></path>
                                      <path class="cls-2" d="m713.58,113.99h-12.68V40.71h12.68v73.28Z"></path>
                                      <path class="cls-5" d="m231.88,41.83c-.69.28-.64,1.31.08,1.52.03,0,7.47,2.13,7.47,2.13l2.13,7.47c.21.72,1.24.77,1.52.08.01-.03,6.77-16.93,6.77-16.93.26-.66-.42-1.29-1.04-1.05-.03.01-16.93,6.77-16.93,6.77h0Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </svg>
                                 <div class="f-14 f-md-16 w500 mt20 lh150 white-clr text-center text-md-start" style="color:#7d8398;"><span class="w700">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                                 <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/nqy65h" defer></script><div class="wplus_spdisclaimer"> </div> 
                              </div>
                           </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © SiteSmartAi 2023</div>
               <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://sitesmartai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section End -->
   <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?> 
    <!-- Exit Popup and Timer End -->
   
   <!-- timer --->
   <?php
   
         if ($now < $exp_date) {
             
         ?>
      <script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
            }
         
         }


         timerBegin(59 * 60); 

      </script>
      <?php
         } else {
         //echo "Times Up";
         }
         ?>
      <!--- timer end-->
      
      <script type="text/javascript" src="https://warriorplus.com/o2/js/jj9mkz"></script>
      
<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/nqy65h/h2kbnf/jt52hg");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/nqy65h/h2kbnf/s29n55");
	  }
}
</script>

</body>

</html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="Vidz Agency Bonuses">
    <meta name="description" content="Vidz Agency Bonuses">
    <meta name="keywords" content="Done-for-You STUNNING Video Shorts That Drive MASSIVE Engagement, Traffic, and Sales">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Vidz Agency Bonuses">
    <meta property="og:description" content="Done-for-You STUNNING Video Shorts That Drive MASSIVE Engagement, Traffic, and Sales">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Vidz Agency Bonuses">
    <meta property="twitter:description" content="Done-for-You STUNNING Video Shorts That Drive MASSIVE Engagement, Traffic, and Sales">

	<title>Vidz Agency Bonuses</title>
	<!-- Shortcut Icon  -->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <script src="../common_assets/js/jquery.min.js"></script>

		
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
	<?php
		$date = 'May 22 2023 11:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
	?>
	<?php
		} else {
			echo "Times Up";
		}
	?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://jvz3.com/c/10103/395764/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                    		   <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                           <img src="assets/images/logo-ee.png" class="img-fluid d-block" alt="ProductBox" style="max-width: 300px; ">
                    	</div>
                     </div>
                        <div class="col-12 mt20 mt-md50 text-center">
                           <div class="preheadline f-18 f-md-22 w500 lh140">
                              TIRED OF LAME VIDEO SOFTWARE WITH TEMPLATES YOU'D BE EMBARRASSED TO USE? 
                           </div>
                        </div>
                        <div class="col-12 mt-md30 mt10 f-md-50 f-28 w700 text-center white-clr lh140">
                           Done-for-You STUNNING Video Shorts That Drive MASSIVE Engagement, Traffic, and Sales
                        </div>
                        <div class="col-12 mt20 mt-md30 f-md-22 f-18 w400 white-clr text-center lh150">
                           Includes Done-for-You Websites and FREE Traffic
                        </div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md30">
                     <div class="col-12 col-md-10 mx-auto">
                        <!-- <img src="assets/images/logo-ee.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                        <div class="responsive-video">
                           <iframe src="https://player.vimeo.com/video/824367167?autoplay=1&muted=0?muted=1&autoplay=1&&title=0&byline=0&wmode=transparent&autopause=0" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div> 
                  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 mt20 mt-md40" style="row-gap: 20px;">
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/1.png" alt="Product Creators" class="point-img mx-auto d-block img-fluid lazyloaded">
                     <div class="figure-caption">
                        Create STUNNING Video Shorts in Minutes
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/2.png" alt="Affiliate Marketers" class=" point-img mx-auto d-block img-fluid lazyloaded">
                     <div class="figure-caption">
                     50+ STUDIO Quality Done-for-You Templates
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/3.png" alt="eCom Store Owners" class="point-img mx-auto d-block img-fluid lazyloaded">
                     <div class="figure-caption">
                     Videos That Build Your Brand and Drive Sales
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/4.png" alt="Blog Owners" class="point-img mx-auto d-block img-fluid lazyloaded"   >
                     <div class="figure-caption">
                     Commercial License To Sell Your Videos For Profits
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/5.png" alt="CPA Marketers" class="point-img mx-auto d-block img-fluid lazyloaded" >
                     <div class="figure-caption">
                     Drag-n-Drop Simple Complete Video Software
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/6.png" alt="Video Marketers" class="point-img mx-auto d-block img-fluid lazyloaded">
                     <div class="figure-caption">
                     Wide and Vertical Story Videos in HD & 4K
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/7.png" alt="Artists/Content Creators" class="point-img mx-auto d-block img-fluid lazyloaded" >
                     <div class="figure-caption">
                     Included 1,000+ Elements & Effects Library
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/8.png" alt="Personal Brands" class="point-img mx-auto d-block img-fluid lazyloaded" >
                     <div class="figure-caption">
                     Included 1,000+ Elements & Effects Library
                     </div>
                  </div>
               </div>
               
            </div>       
               </div>
	         </div>
   
	<!-- Header Section Start -->
    <!-- <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li> <span class="w600">Proven REAL Busines</span> no gimmicks, loopholes, or fads</li>
                           <li><span class="w600">Up & Running TODAY </span> start making sales fast </li>
                           <li><span class="w600">SCALE to any income level </span> make extra or replace your job</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li><span class="w600">COMPLETE System </span>nothing left to guess or figure out </li>
                           <li><span class="w600">VALUE for Customers</span> be proud of your business</li>
                           <li><span class="w600">NO Experience Needed </span> just follow <br class="d-none d-md-block">the steps</li>
                        </ul>
                     </div>
                  </div>
                </div>
            </div> -->

			<div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div> </div>       
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
                  
              
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
              
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">36&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">27</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
            </div>
        </div>
    </div>
    <!-- Header Section End -->
    <img src="assets/images/va-ss1.png" class="img-fluid mx-auto d-block" alt="ss">
    <img src="assets/images/va-ss2.png" class="img-fluid mx-auto d-block" alt="ss">
    <img src="assets/images/va-ss4.png" class="img-fluid mx-auto d-block" alt="ss">
    <img src="assets/images/va-ss3.png" class="img-fluid mx-auto d-block" alt="ss">
    <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : eLEARN EMPIRE
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                  Get Instant access to the exclusive <span class="w600">eLEARN EMPIRE bundle deal</span> on the purchase of Vidz Agency Bundle. <br> Similarly, Instant access to the eLEARN EMPIRE FE deal on the purchase of Vidz Agency FE.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="elearn-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                           <img src="assets/images/logo-elearn.png" class="img-fluid d-block" alt="ProductBox" style="max-width: 300px; background-color: white;">
                    	</div>
                     </div>
                        <div class="col-12 mt20 mt-md50 text-center">
                           <div class="preheadline f-18 f-md-22 w500 lh140">
                              World's #1 Video Course Creator Studio & Marketing System
                           </div>
                        </div>
                        <div class="col-12 mt-md30 mt10 f-md-50 f-28 w700 text-center white-clr lh140">
                        <span class="yellow-clr">Create AND Sell AMAZING Video Courses in MINUTES </span> Without Creating Any Content or Speaking a Single Word
                        </div>
                        <div class="col-12 mt20 mt-md30 f-md-22 f-18 w400 white-clr text-center lh150">
                           Includes Done-for-You Websites and FREE Traffic
                        </div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md30">
                     <div class="col-12 col-md-10 mx-auto">
                        <!-- <img src="assets/images/logo-ee.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                        <div class="responsive-video">
                           <iframe src="https://player.vimeo.com/video/762673357?autoplay=1&mute=0?muted=1&autoplay=1&&title=0&byline=0&wmode=transparent&autopause=0" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>    
               </div>
	         </div>
            </div></div>
	<!-- Header Section Start -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li> <span class="w600">Proven REAL Busines</span> no gimmicks, loopholes, or fads</li>
                           <li><span class="w600">Up & Running TODAY </span> start making sales fast </li>
                           <li><span class="w600">SCALE to any income level </span> make extra or replace your job</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li><span class="w600">COMPLETE System </span>nothing left to guess or figure out </li>
                           <li><span class="w600">VALUE for Customers</span> be proud of your business</li>
                           <li><span class="w600">NO Experience Needed </span> just follow <br class="d-none d-md-block">the steps</li>
                        </ul>
                     </div>
                  </div>
                </div>
            </div></div>
            <img src="assets/images/ss-1.png" class="img-fluid mx-auto d-block mt10 mt-md30" alt="ss">
	<div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : WriterArc
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                  Get Instant access to the exclusive <span class="w600">WriterArc bundle deal</span> on the purchase of Vidz Agency Bundle. <br> Similarly, Instant access to the WriterArc FE deal on the purchase of Vidz Agency FE.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="writer-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap flex-md-nowrap">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-20 f-md-22 w500 blue-clr1 lh140">
                     Never Waste Your Time &amp; Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="gradient-orange"> Any Local or Online Niche 10X Faster…Just by Using a Keyword</span>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back &amp; Relax, WriterArc Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/writerarc-productbox.webp" class="img-fluid d-block mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/spru84q6w8" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div> -->
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w500 white-clr">
                     <li><span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                     <li><span class="orange-clr1"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                     <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                     <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                     <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                     <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/writerarc-steps.png">
               <source media="(min-width:320px)" srcset="assets/images/writerarc-mview-steps.png" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/writerarc-steps.png" alt="WriterArc Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                  Get Instant access to the exclusive <span class="w600">Trendio bundle deal</span> on the purchase of Vidz Agency Bundle. <br> Similarly, Instant access to the Trendio FE deal on the purchase of Vidz Agency FE.
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/trendio-productbox.png" alt="Steps" class="img-fluid d-block mx-auto">
                  <!-- <div class="col-12 responsive-video">
                     <iframe src="assets/images/trendio-productbox.png" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> -->
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE AGENCY LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/trendio.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/trendio-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/trendio.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
       <!-- CTA Btn Section Start -->
       <div class="row mt20 mt-md30">
       <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div> </div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
                  
              
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
              
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">36&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">27</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
               </div>



  <!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700"> When You Purchase Vidz Agency, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
<!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block" alt="Bonus3">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block" alt="Bonus4">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block" alt="Bonus5">
               </div>
            </div>
         </div>
      </div>

            
	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
              
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">20</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
	<!-- CTA Button Section End   -->


      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block" alt="Bonus6">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block" alt="Bonus7">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block" alt="Bonus8">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block" alt="Bonus9">
               </div>
            </div>
         </div>
      </div>
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 align-content-center">
                  <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block" alt="Bonus10">
               </div>
            </div>
         </div>
      </div>
   


   
	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
              
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">20</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
	<!-- CTA Button Section End   -->

 <!-- Bonus #11 start -->
 <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Video Analytics Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        Video Analytics Plugin Is Your Companion Solution to Google Analytics for Tracking Embedded Video Usage on Site! This plugin is the ultimate analytics that provides bloggers with live tracking, insightful data, and analysis of what videos are truly consumed on their sites.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Mega Music Tracks
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        Wow! Another 200 audio tracks for you to use as background music for your marketing videos! If you are a blogger or an online business owner, utilizing the second search engine - YouTube is a clever idea.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Quick Image Downloader
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        How Would You Like To Automatically Download Free Images For Your Projects With Just One Click? Images is one of the best medium to have a good conversation rate of your website.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Text To Speech Converter
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        The Text To Speech Converter will turn any text into an audio file at the click of a button! Forget reading endless articles! Use this software wherever and whenever you like to keep up to date, without having to sift through the junk, even whilst doing other things!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Professional Music Tracks
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        More Professional Quality Sound Tracks For Your Marketing Needs! Professional Quality Music Tracks For Your Marketing Needs! These music tracks vary in length from 30 seconds to 5 minutes!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
              
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">20</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
	<!-- CTA Button Section End   -->
	
  <!-- Bonus #16 Start -->
  <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Video Launch Method
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        Video product launches are the go-to for the world’s best brands. This guide will show you the best techniques used in videos during successful product launches that will help you improve your brand positioning.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        WP Image Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        Easily Tap Into More Than 1,000,000 Copyright-Free, High-Quality Images Straight From The Admin Area Of Your Wordpress Blog!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        3D Box Templates
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        3D Box Templates allow you to create great looking eBox covers in just a few simple steps; Upload template, Enter text, Set color and you're done!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        YouTube Cash
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        Everyone watches videos online nowadays so shouldn't you cash in on the booming video industry? People really wants videos and in most type of content that people are spending time on the internet, videos standout the longest.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Making Money With Amazon Video Direct
                        </div>
                        <ul class="bonus-list f-18 f-md-23 w400 lh150 mt20 mt-md30 p0">
                        With this video course you will learn how to easily monetize your own video content by uploading to Amazon Video Direct, Amazon's new video platform.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 gradient-clr">$2895!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center">
                	<div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 purple-gradient">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 purple-gradient">completely NO Brainer!!</span></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<div class="row mt20 mt-md70 pb20">
               <!-- CTA Btn Section Start -->
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
               </div>
               
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w800 purple-gradient">"FAST100"</span> for an Additional <span class="w800 purple-gradient">$100 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/395786/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vidz Agency + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">30&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">18</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
            </div>
	
	<!-- Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <img src="assets/images/logo-ee.png" class="img-fluid d-block mx-auto" alt="ProductBox" style="max-width: 300px; ">
						<br><br><br>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © Vidz Agency</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->


	<!-- timer --->
  	<?php
	 	if ($now < $exp_date) {
	?>

  	<script type="text/javascript">
		// Count down milliseconds = server_end - server_now = client_end - client_now
		var server_end = <?php echo $exp_date; ?> * 1000;
		var server_now = <?php echo time(); ?> * 1000;
		var client_now = new Date().getTime();
		var end = server_end - server_now + client_now; // this is the real end time
		
		var noob = $('.countdown').length;
		
		var _second = 1000;
		var _minute = _second * 60;
		var _hour = _minute * 60;
		var _day = _hour * 24
		var timer;
		
		function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
			return;
		}
		
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);
		if (days < 10) {
			days = "0" + days;
		}
		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var i;
		var countdown = document.getElementsByClassName('countdown');
		for (i = 0; i < noob; i++) {
			countdown[i].innerHTML = '';
		
			if (days) {
				countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
			}
		
			countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
		}
		
		}
		timer = setInterval(showRemaining, 1000);
  	</script>
  	<?php
	 	} else {
	 	echo "Times Up";
	 	}
	?>
  <!--- timer end-->
</body>
</html>

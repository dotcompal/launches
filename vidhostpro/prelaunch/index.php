<!Doctype html>
<html>
   <head>
      <title>VidHostPro Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidHostPro | Prelaunch">
      <meta name="description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
      <meta name="keywords" content="VidHostPro">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.vidhostpro.co/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidHostPro | Prelaunch">
      <meta property="og:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
      <meta property="og:image" content="https://www.vidhostpro.co/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidHostPro | Prelaunch">
      <meta property="twitter:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script>
         function getUrlVars() {
             var vars = [],
                 hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for (var i = 0; i < hashes.length; i++) {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
                 document.getElementById('awf_field_aid').setAttribute('value', first);
             })
             //document.getElementById('myField').value = first;
      </script>
      <!-- Google Tag Manager -->
      <script>
         (function(w, d, s, l, i) {
             w[l] = w[l] || [];
             w[l].push({
                 'gtm.start': new Date().getTime(),
                 event: 'gtm.js'
             });
             var f = d.getElementsByTagName(s)[0],
                 j = d.createElement(s),
                 dl = l != 'dataLayer' ? '&l=' + l : '';
             j.async = true;
             j.src =
                 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
             f.parentNode.insertBefore(j, f);
         })(window, document, 'script', 'dataLayer', 'GTM-NMK2MBB');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/vidhostpro/prelaunch/sale.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="col-md-3 text-center text-md-start mt20">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md25">
                        <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                           <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-20 w600 black-clr lh140">
                    Register For This Value Packed Free Training; All Attendees Receive FREE Gifts
                  </div>
               </div>
               <div class="col-12 mt-md40 mt10 f-md-50 f-28 w500 text-center black-clr lh140 mainheadline">
              <span class="w700"> Discover How to Market & Monetize Your Videos </span>to Instantly Boost Engagement, Conversions, And Sales

               </div>
               <div class="col-12 mt20 mt-md30 f-md-27 f-18 w400 white-clr text-center lh170">
                    Watch how we have sold over $7 Million worth of digital products <span class="underline-head w600">using the power of videos</span>  and saved 100’s of dollars monthly on money sucking video hosting apps.
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.dotcompaltest.com/uploads/launches/vidhostpro/prelaunch/video-thumb.webp" class="img-fluid d-block mx-auto" alt="Prelaunch"> -->
                  <div class="video-box">
                   <div class="responsive-video">
                        <iframe src="https://promos.oppyo.com/video/embed/8zmwukr3e3" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> 
                    </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">       
                  <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                     <div class="calendar-wrap-inline">
                        <div class="f-22 f-md-24 w700 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
                           Register for Free Training <br> Nov
                           <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">24<sup contenteditable="false">th</sup> , 10 AM EST</span><br>
                           
                        </div>
                       
                        <!-- Aweber Form Code -->
                        <div class="col-12 p0 mt15 mt-md20">
                           <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="238254624" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6670575" />
                                 <input type="hidden" name="redirect" value="https://vidhostpro.co/prelaunch-thankyou/" id="redirect_242490c1e8b0da381f4dff3108e2dbad" />
                                 <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-238254624" class="af-form">
                                 <div id="af-body-238254624" class="af-body af-standards">
                                    <div class="af-element width-form1">
                                       <label class="previewLabel" for="awf_field-116553581"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-116553581" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="1"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form1">
                                       <label class="previewLabel" for="awf_field-116553582"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-116553582" type="text" name="email" value="" tabindex="2" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                    <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                              <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TMwcTKwsbEws" alt="" /></div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Header Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-45 f-24 w600 lh150 text-capitalize text-center gradient-clr">
                              Why Attend This Free Training Webinar?
                           </div>
                           <div class="f-20 f-md-22 w500 lh150 text-capitalize text-center black-clr mt20">
                              Our 35+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li>How We Have Sold Over $7 Million In Digital Products Using the Power Of Videos And <span class="w600"> You Can Follow The Same To Build A Profitable Business Online.</span></li>
                                <li>How You Can Boost Your Conversions And Sales By <span class="w600">Simply Using The Power Of Video Marketing.</span></li>
                                <li>How Videos Can Help To Build Trust Among Your Audience And Boost Profits</li>
                                <li><span class="w600">How You Can Start Your Video Marketing Agency Without Any Hassles.</span></li>
                                <li> <span class="w600"> How We Have Saved 100s Of Dollars Monthly </span> On Money Sucking Expensive Video Marketing Platforms Which Never Provided Desired Results.</li>

                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Discounted Price Section Start -->
      <div class="form-sec">
         <div class="container">
            <div class="row mt20 mt-md70 mb20 mb-md70">
            <div class="col-12 f-md-45 f-28 w700 lh150 text-center ">
                    Check Out VidHostPro In Action!
                </div>
               <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">
                    <!-- <img src="https://cdn.dotcompaltest.com/uploads/launches/vidhostpro/prelaunch/proudly.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly border-video responsive-video "> -->
                    <div class="responsive-video border-video">
                        <iframe src="https://vidhostpeo.oppyo.com/video/embed/VHP_Demo_Video" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                    </div> 
                </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md30" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-24 f-md-32 w600 text-center lh150 black-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar 							
                     </div>
                     <div class="col-12 f-md-22 f-20 w600 lh160 mx-auto my20 text-center">
                        Register Now! And Join Us Live On <br>
                         Nov 24th, 10:00 AM EST
                        
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                        <!-- Aweber Form Code -->
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="238254624">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6670575">
                              <input type="hidden" name="redirect" value="https://vidhostpro.co/prelaunch-thankyou/" id="redirect_e4d877facc78e808848fb10614427a30">
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-238254624" class="af-form">
                              <div id="af-body-238254624" class="af-body af-standards">
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-116553581"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-116553581" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="4" placeholder="Your Name">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-116553582"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-116553582" type="text" name="email" value="" tabindex="5" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TMwcTKwsbEws" alt=""></div>
                        </form>
                     </div>
                     
                  </div>
               </div>
               
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
                        <br><br><br>
                    <!-- <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div> -->
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © VidHostPro</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us"  target="_blank" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/privacy-policy.html"  target="_blank" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/terms-of-service.html"  target="_blank" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/disclaimer.html"  target="_blank" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/gdpr.html"  target="_blank" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/dmca.html"  target="_blank" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/anti-spam.html"  target="_blank" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-238254624').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-238254624")) {
                     document.getElementById("af-form-238254624").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-238254624")) {
                     document.getElementById("af-body-238254624").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-238254624")) {
                     document.getElementById("af-header-238254624").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-238254624")) {
                     document.getElementById("af-footer-238254624").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
   </body>
</html>
<!Doctype html>
<html>
   <head>
      <title>NexusGPT | Bundle Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <!------Meta Tags-------->
      <meta name="title" content="NexusGPT Exclusive Bundle Deal">
      <meta name="description" content="GET NexusGPT With All The Upgrades For 85% Off & Save Over $697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta name="keywords" content="NexusGPT">
      <meta property="og:image" content="https://www.getnexusgpt.com/bundle/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="NexusGPT Exclusive Bundle Deal">
      <meta property="og:description" content="GET NexusGPT With All The Upgrades For 85% Off & Save Over $697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta property="og:image" content="https://www.getnexusgpt.com/bundle/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="NexusGPT Exclusive Bundle Deal">
      <meta property="twitter:description" content="GET NexusGPT With All The Upgrades For 85% Off & Save Over $697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta property="twitter:image" content="https://www.getnexusgpt.com/bundle/thumbnail.png">
      <!------Meta Tags-------->
       <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script type='text/javascript' src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <script src="assets/js/timer.js"></script>
     
  <!-- New Timer  Start-->
  <?php
  $date = 'June 13 2023 11:00 AM EST';
  $exp_date = strtotime($date);
  $now = time();  
 
  if ($now < $exp_date) {
  ?>
<?php
  } else {
     echo "Times Up";
  }
  ?>
<!-- New Timer End -->


  <style>
     .timer-header-top{background: #000; padding-top:10px; padding-bottom:10px;}
  </style>  
</head>
<body>

<div class="timer-header-top fixed-top">
  <div class="container">
     <div class="row align-items-center">
        <div class="col-12 col-md-3">
           <div class="tht-left f-14 f-md-16 white-clr w700 text-md-start text-center">
               Coupon Code <span class="orange-clr">"NEXUSGPT"</span> <br class="d-block d-md-none"> for <span class="orange-clr"> $50 Discount </span> Ending In
           </div>
        </div>
        <div class="col-7 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-16 f-md-18 timerbg oswald">00</span><br><span class="f-12 f-md-14 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-5 col-md-3">
                  <div class=".instant-btn"><a href="#" class="t-decoration-none">Buy Now</a></div>
               </div>
     </div>
  </div>
</div>
   </head>
   <body>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                        <defs>
                           <style>
                              .cls-1 {
                              fill: #fff;
                              }

                              .cls-2 {
                              fill-rule: evenodd;
                              }

                              .cls-2, .cls-3 {
                              fill: #00a4ff;
                              }

                              .cls-4 {
                              fill: #f19b10;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                    <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                    <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                    <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                    <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                    <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                    <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                 </g>
                              </g>
                              <g>
                                 <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                 <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                 <g>
                                    <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                    <g>
                                       <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                       <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                    </g>
                                    <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
            </div>
            <div class="row">
               <div class="col-12 mt20 mt-md30 text-center">
                  <div class="preheadline f-20 f-md-22 w500 lh140">
                     <u class="w600 red-clr"> WARNING</u> :&nbsp;THIS LIMITED TIME DISCOUNTED BUNDLE OFFER EXPIRES SOON 
                  </div>
               </div>
               <div class="col-12 mt80 head-design relative">
                  <div class="gametext">
                     First-to-JVZoo Technology 
                  </div>
                  <div class=" f-md-40 f-28 w400 text-center black-clr lh140">
                     <span class="underline-text w600">The Futuristic NFC Tech App Transforms Your Marketing, </span>Creates 100% Contactless Ai-Powered Digital Business Cards, Gets Leads, Followers, Reviews, Sales, & More  <span class="w600 underline-text2">with Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-20 f-md-24 w400 text-center lh140 white-clr text-capitalize">
                  <div class="f-22 f-md-26 w500 lh140 white-clr text-capitalize">               
                     GET NexusGPT With All The Upgrades For 70% Off & Save Over $697 When <br class="d-none d-md-block">You Grab This Highly-Discounted Bundle Deal... 
                 </div>
               </div>
               

               <div class="col-12 mt20 mt-md50 mx-auto">
                 <div class="responsive-video">
                      <iframe src="https://nexusgpt.oppyo.com/video/embed/5i527bk1j7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  <!-- <img src="assets/images/pbb-img.webp" class="img-fluid d-block mx-auto img-shadow" alt="ProductBox"> -->
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!---new Section Start---->
      <div class="new-section content-mbl-space">
         <div class="container">
             <div class="row">
                 <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                     Save $697 RIGHT AWAY-
                     <div class="gradient-clr w700 f-md-70 f-40">
                         Deal Ends Soon
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>GET Complete NexusGPT Package (FE + ALL Upgrades + Agency License) </li>
                          <li>No Monthly Payment Hassles- It's Just A One-Time Payment</li>
                          <li>GET Priority Support from Our Dedicated Support Engineers</li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>Provide Top-Notch Services to Your Clients</li>
                          <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                          <li>GET 30-Days Money Back Guarantee</li>
                       </ul>
                    </div>
                 </div>
              </div>
             <div class="row">
                 <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                     <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                         <img src="assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                         <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                     </div>
                     <div>
                         Use Coupon Code <span class="w600 orange-clr">"NEXUSGPT"</span> for an Additional <span class="w600 orange-clr">$50 Discount</span> 
                     </div>
                     <div class="mt20 red-clr w600">
                     <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 close-img">
                        No Recurring | No Monthly | No Yearly
                     </div>
                     <div class="col-md-8 col-12 instant-btn mt20 f-24 f-md-30 mx-auto px0">
                         <a href="#buybundle">
                      GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                      </a>
                     </div>
                     <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                         No Download or Installation Required
                     </div>
                     <div class="col-12 mt20 mt-md20">
                         <img src="assets/images/compaitable-with.png" class="img-fluid mx-auto d-block">
                      </div>
                 </div>
             </div>
         </div>
     </div>
      <!--1. New Section End -->
      <!--2. Second Section Start -->
      <div class=" section1">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Here's What You Get With <span class=" w600">Limited Time <br class="d-none d-md-block"> NexusGPT Discounted</span> Bundle Today
                  </div>
               </div>
               <!-- AgencyBUNDLE START -->
               <div class="col-12 mt20 mt-md70 section-margin px-md-0">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                        <div class="pimg">
                           <img src="assets/images/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 NexusGPT Commercial ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Upto 50 Businesses </li>
                                 <li>Create Contactless NFC Cards - Unlimited</li>
                                 <li>Create GPT AI Assistants - Unlimited </li>
                                 <li>Launch FAST - Create Digital Cards for Any Niche Within Minutes</li>
                                 <li>Tons of Mobile Responsive & Ready-To-Go Business Card Templates</li>
                                 <li>Drive Up To 100,000 Card Visits/Month</li>
                                 <li>Capture Upto 30,000 Leads In A Hassle Free</li>
                                 <li>Manage Leads Effortlessly & Make The Most From Them</li>
                                 <li>Generate Reviews, Followers, Payments & Much More - Unlimited </li>
                                 <li>Fully Drag & Drop Advanced WYSIWYG Card Editor</li>
                                 <li>Create Business Card Templates From Scratch</li>
                                 <li>Get FREE Storage Upto 50GB Unlimited And Bandwidth Upto 250GB/m</li>
                                 <li>Seamless Integration With 20+ Top Autoresponders To Send Emails To Your Clients</li>
                                 <li>Manage All Business Cards, Virtual Assistance Hassle- Free, All in Single Dashboard.</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Mobile Friendly Fast Loading Business Cards So You Will Never Lose Any Contact/Client Again.</li>
                                 <li>Manage All Business Cards, Virtual Assistants Hassle- Free, All In Single Dashboard.</li>
                                 <li>Inbuilt SEO Management For Business Cards</li>
                                 <li>128-Bit SSL Encryption For Maximum Security Of Your Data & Files</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting Or Installation Required</li>
                                 <li>100% ADA, GDPR, & CAN-SPAM Compliant</li>
                                 <li>No Coding, Design Or Tech Skills Needed</li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training</li>
                                 <li>24*5 Customer Support</li>
                                 <li>Unparallel Price</li>
                                 <li>Commercial License Included To Charge Monthly From Clients</li>
                                 <li>Use For Your Clients</li>
                                 <li>Provide High In Demand Services</li>
                              </ul>
                           </div>
                        </div>

                   
                     </div>
                  </div>
               </div>
               <!-- AGENCY BUNDLE END -->
               <!-- ELITE BUNDLE START -->
               <div class="col-12 px-md0 mt40 mt-md140 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 1- NexusGPT Elite ($297)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                              <ul class="kaptick pl0">
                             <li>  Create Unlimited Businesses/Sub-Domains</li>
                             <li>  Unlimited Custom Domains</li>
                             <li>  Serve Unlimited Clients</li>
                             <li>  Unlimited Bandwidth for Hosting</li>
                             <li>  Get 1 TB Extra Storage for Hosting</li>
                             <li>  Customize GPT Assistance Image</li>
                             <li>  Customize GPT Assistance Theme Color</li>
                             <li>  Customize GPT Assistance Name</li>
                             <li>  Create Your Own Business Card Templates and Sell them your Clients to Make More Profits</li>
                             <li>  Generate Unlimited Leads </li>
                             <li>  Unlimited Business Card Visits</li>
                             <li>  4X EXTRA Proven Converting, Mobile Responsive & Ready-to-Go Business Card Templates</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt0 mt-md50">
                           <ul class="kaptick pl0 p0 m0">   
                             <li>  Unlimited Virtual Assistance for your Business Cards</li>
                             <li>  Advanced Editor to Give Look and Feel Better to your Business Cards</li>
                             <li>  Advanced Analytics To Have Clear Insight of What's Working & What's Not To Boost ROI</li>
                             <li>  Download & Share Business Card Templates with your Team, Clients & Group   </li>
                             <li>  Duplicate Business Cards In Between Businesses And Projects And Save Templates For Further Use</li>
                             <li>  Play Your Videos On Business Cards Using Inbuilt Professional, Ultra-Light & Attractive HLS Video Player</li>
                             <li>  Easily Revert to the Last Published Version of Your Business Card in Case you want to Scrape.</li>
                             <li>  Get Your Subscribers Auto-Registered for Your Webinars with Webinar Platform Integrations</li>
                           </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ELITE BUNDLE END -->
               <!-- ENTERPRISE BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 2- NexusGPT Enterprise ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                              <ul class="kaptick pl0">
                              <li>Send Unlimited Beautiful Emails</li>
                              <li>Unlimited Email Scheduling </li>
                              <li>Unlimited Email Lists</li>
                              <li>Advanced Follow-up Emails Journey with Exclusive Marketing  Automation Technology</li>
                              <li>Inbuilt Text & Inline Editor to Craft Best Emails</li>
                              <li>Boost Email Delivery, Click and Open Rate</li>
                              <li>List Checking to Clean Bad Email Addresses on your list to Further Boost Delivery</li>
                              <li>Use Smart Tags to Segment your Subscribers & Send Exclusive Emails</li>
                              <li>Works Seamlessly with Almost Every SMTP Server</li>
                              <li>250+ Beautiful, Mobile-Friendly and Ready-To-Use Lead Pages, Popups & Email Templates</li>
                              <li>Reduce Bounce Rate of Your Email Campaigns</li>
                              <li>Precise Analytics of Your Email Campaigns to See Overview of Their Performance</li>
                              <li>Personalize your Mails to Get High Opening Rates</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w500 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                              <li>Advanced SPAM Checker to Increase Your Open and Click Rate</li>
                              <li>Strengthen Your Relationship with Your Customers using CRM Integrations </li>
                              <li>Set Multiple Actions on Journey like Add Pages, Tags, Lead Score, Add to List, Schedule the Actions on Trigger to Get More Conversions and Leads</li>
                              <li>Work Collaboratively & Share Proven Funnels & Templates</li>
                              <li>Create Your Own Funnel & Page Templates and Sell them to your Clients to Make More Profits</li>
                              <li>Accurate Analysis for Team Member’s Activities for Effective Monitoring</li>
                              <li>Remove NexusGPT/OPPYO Branding</li>
                              <li>Upto 50 Team Members</li>
                              <li>Commercial Use License to Serve Unlimited Clients </li>
                              <li>Subscription Management System to Manage Your Clients Plans</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ENTERPRISE BUNDLE END -->
               <!-- BUSINESS DRIVE BUNDLE START -->
               <div class="col-12 px-md0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/bizdrive.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class=" col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 3- NexusGPT Agency($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-18 f-md-20 lh140 w500 mt40 mt-md50">
                              <ul class="kaptick pl0">
                              <li>Unlimited Team Members</li>
                              <li>License to serve to Unlimited clients</li>
                              <li>Remove Branding from Business Cards</li>
                              <li>Work Collaboratively & Share Proven Funnels & Templates</li>
                              <li>Create Your Own Business Card Templates and Sell them your Clients to Make More Profits</li>
                              </ul>
                          </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-18 f-md-20 lh140 w500 mt20 mt-md50">
                              <ul class="kaptick pl0">
                              <li>Accurate Analysis for Team Member’s Activities For Effective Monitoring</li>
                              <li>Support to you & your clients - chat support directly from software</li>
                              <li>Subscription Management System to Manage Your Clients Plans</li>
                              <li>Get All These Benefits For One Time Price</li>
                              </ul>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
              
               <!-- FAST ACTION BONUSES BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="mx-auto mt-md0 mt20">
                        <div class="pimg ">
                           <img src="assets/images/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                        </div>
                     </div>
                     <div class="row mt20">
                        <div class="col-12 p0 mt-md50 mt20 text-center">
                           <div class="title-shape1">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                 And You're Also Getting These Premium Fast Action Bonuses Worth ($409)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Bonus #1 - Live Training - 0-10k a Month With NexusGPT (First 1000 buyers only - $1000 Value) </li>
                                 <li>Bonus #2 - Private Access to Online Business VIP Club </li>
                                 <li>Bonus #3 - Video Training on How To Start Online Business</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Bonus #4 - Lead Generation Video Workshop</li>
                                 <li>Bonus #5 - Video Training on How To Make Money With Digital Business Cards</li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                               <ul class="kaptick pl0">
                                   <li>Super Value Bonus #5 - Video Training To Monetizing Your Website </li>
                                   <li>Super Value Bonus #6 - Training To Start Affiliate Marketing </li>
                                   <li>Super Value Bonus #7 - Video Training On Viral Marketing </li>
                                   <li>Super Value Bonus #8 - Video Marketing Profit Kit </li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--3. Third Section Start -->
      <div class="section3" id="buybundle">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" >
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Grab This Exclusive BUNDLE DISCOUNT Today…
                  </div>
                  <div class="f-18 f-md-26 w400 lh150 text-center mt20 black-clr">
                     Use Coupon Code <span class="orange-clr w600">"NEXUSGPT"</span> for an Additional <span class="orange-clr w600"> $50 Discount</span> 
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }

                           .cls-2 {
                           fill-rule: evenodd;
                           }

                           .cls-2, .cls-3 {
                           fill: #00a4ff;
                           }

                           .cls-4 {
                           fill: #f19b10;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <g>
                                 <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                 <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                 <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                              </g>
                              <g>
                                 <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                 <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                 <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                 <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                 <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                              </g>
                           </g>
                           <g>
                              <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                              <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                              <g>
                                 <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                 <g>
                                    <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                    <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                 </g>
                                 <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
                        </div>
                        <div class="col-12 mt-md20 mt10 table-headline-shape">
                           <div class="w600 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $409
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w600 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col-12 px-md0 px15 mt20 w600 f-md-25 f-20 black-clr text-center">
                  One Time Fee
               </div>
               <div class="red-clr w600 f-md-26 f-22 lh140 w400 text-center">
                     <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 hurry-img" style="width:50px">
                        No Recurring | No Monthly | No Yearly
                     </div>
               <div class="row justify-content-center mx-0 ">
                  <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                     <div class="hideme-button" style="z-index: 100;">
                        <a href="https://www.jvzoo.com/b/109143/397650/2">
                           <img src="https://i.jvzoo.com/109143/397650/2" alt="NexusGPT Bundle" border="0" class="img-fluid d-block mx-auto"/></a> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!----Second Section---->
      <div class="second-header-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 lh140 text-center white-clr">
                     TOP Reasons Why You Can't Ignore <br class="d-none d-md-block">
                     <span class="f-md-50 f-24 w600 ">
                        NexusGPT Bundle Deal
                     </span>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md65">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #1
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get All The Benefits Of NexusGPT & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #2
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Regular Price For NexusGPT, All Upgrades & Bonuses Is $997. You Are Saving $697 Today When You Grab The Exclusive Bundle Deal Now at ONLY $297.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #3
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        This Limited Time Additional $50 Coupon Will Expires As Soon As Timer Hits Zero So Take Action Now.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #4
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get Priority Support From Our Dedicated Support Team to Get Assured Success.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----Second Section---->
      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 white-clr text-center col-12 mb-md40">
                  Test Drive EVERYTHING NexusGPT Has To Offer Risk Free For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-18 f-18 w400 lh150 white-clr mt15 ">
                     Your purchase is secured with our 30-day 100% money back guarantee.
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your customers risk free. If you face any issue or don't get the results you desire after using it, just raise a ticket on our support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     However, we want to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto " alt="Risk Free">
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section Start -->
      <div class="section2" id="buybundle">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" >
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Limited Time Offer!
                  </div>
                  <div class="f-22 f-md-32 w600 lh150 text-center mt20 black-clr">
                     Get Complete Package of All <span class="w600">NexusGPT</span> Upgrade<br class="d-md-block d-none"> for A Low One-Time Fee
                  </div>
               </div>
               <div class="f-18 f-md-26 w400 lh150 text-center mt20 black-clr">
                  Use Coupon Code <span class="orange-clr w600">"NEXUSGPT"</span> for an Additional <span class="orange-clr w600"> $50 Discount</span> 
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                        <defs>
                           <style>
                              .cls-1 {
                              fill: #fff;
                              }

                              .cls-2 {
                              fill-rule: evenodd;
                              }

                              .cls-2, .cls-3 {
                              fill: #00a4ff;
                              }

                              .cls-4 {
                              fill: #f19b10;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                    <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                    <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                    <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                    <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                    <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                    <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                 </g>
                              </g>
                              <g>
                                 <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                 <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                 <g>
                                    <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                    <g>
                                       <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                       <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                    </g>
                                    <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                        </div>
                        <div class="col-12 mt-md20 mt10 table-headline-shape">
                           <div class="w600 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                       
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $409
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w600 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col-12 px-md0 px15 mt20 w600 f-md-25 f-20 black-clr text-center">
                  One Time Fee
               </div>
               <div class="red-clr w600 f-md-26 f-22 lh140 w400 text-center">
                     <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 hurry-img" style="width:50px">
                        No Recurring | No Monthly | No Yearly
                     </div>
               <div class="row justify-content-center mx-0 ">
                  <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                     <div class="hideme-button">
                        <a href="https://www.jvzoo.com/b/109143/397650/2"><img src="https://i.jvzoo.com/109143/397650/2" alt="NexusGPT Bundle" border="0" class="img-fluid d-block mx-auto"/></a> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="f-md-36 f-24 black-clr  text-center w600 lh140">
                     We are always here by your side <br class="d-none d-md-block">
                     <span class="orange-clr">Get in touch with us…</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
       <!--Footer Section Start -->
       <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }

                           .cls-2 {
                           fill-rule: evenodd;
                           }

                           .cls-2, .cls-3 {
                           fill: #00a4ff;
                           }

                           .cls-4 {
                           fill: #f19b10;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <g>
                                 <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                 <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                 <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                              </g>
                              <g>
                                 <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                 <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                 <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                 <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                 <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                              </g>
                           </g>
                           <g>
                              <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                              <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                              <g>
                                 <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                 <g>
                                    <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                    <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                 </g>
                                 <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © NexusGPT 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            showRemaining();
            //tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               //timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-24 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-16 f-md-24 timerbg oswald">' + seconds + '</span><br><span class="f-12 f-md-15 w500 smmltd">Sec</span> </div>';
            }
         
         }


      //timerBegin(59 * 60); 




      function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 59*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         timerBegin(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();



      </script>
            <!--- timer end-->

   </body>
</html>
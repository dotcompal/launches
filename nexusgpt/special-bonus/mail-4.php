<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="Sellero Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="Sellero Bonuses">
      <meta property="og:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Er. Ashu Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Sellero Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Sellero Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <title>Sellero Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'June 14 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz2.com/c/47069/396539/';
         $_GET['name'] = 'Er. Ashu Pareek';      
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://jvz1.com/c/47069/396537/';
         $_GET['name'] = 'Er. Ashu Pareek';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 green-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                  <defs>
                    <style>
                      .cls-1s {fill: #fff;}
                      .cls-2s {fill: #1fe29f;}
                      .cls-3s {fill: #7764ff;}
                    </style>
                  </defs>
                  <g id="Layer_1-2" data-name="Layer 1">
                    <g>
                      <g>
                        <path class="cls-3s" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                        <path class="cls-3s" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                        <path class="cls-3s" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                        <g>
                          <circle class="cls-3s" cx="130.39" cy="198.16" r="10.38"></circle>
                          <circle class="cls-3s" cx="75.08" cy="198.16" r="10.38"></circle>
                        </g>
                      </g>
                      <g>
                        <path class="cls-1s" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                        <path class="cls-1s" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                        <path class="cls-1s" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                        <path class="cls-1s" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                        <path class="cls-1s" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                        <path class="cls-1s" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                        <g>
                          <path class="cls-1s" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                          <polygon class="cls-2s" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                          <path class="cls-2s" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                        </g>
                      </g>
                    </g>
                  </g>
               </svg>
                     </div>
                  </div>

                  <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                  Grab My 20 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt-md80 mt50 head-design relative">
                  <div class="gametext">
                  CUTTING-EDGE TECHNOLOGY
                  </div>
                  <div class=" f-md-44 f-28 w400 text-center black-clr lh140">
                  <span class="w600">Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes...</span>  NO Tech Hassles. No Monthly Fee Ever.
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
               Today Sellero Is Also Coming With 20+ Ready-To-Use Products & FREE Commercial License To Start Selling Online & Profiting Right Away!
               </div>
                  
            </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://yoseller.dotcompal.com/video/mydrive/182331" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> -->
                  <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li> Launch Your &amp; Client's Products &amp; Services Online with ZERO Tech Hassles.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Profit with Agency/Reseller/PLR Right Products You Ever Purchased.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Create Stunning Websites, E-stores and Membership Sites Easily</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell On Your Own Marketplace &amp; Keep 100% Profits</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Smart-Checkout Links To Get Orders Directly from Social Media, Emails or Anywhere Else</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell High In-Demand Services with <span class="w600 white-clr">Included Commercial License.</span></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container"> 
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w600 lh140 text-capitalize text-center black-clr">
                  With Sellero, Launch Your Product Or Service For Recurring Profit <span class="w700 purple-gradient">In 3 Easy Steps </span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Add A Product ">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                           Add A Product 
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 text-center">
                        Unlock the Potential &amp; Start Selling Your Purchased Agency / Reseller / White label / PLR Right Products, Your Own Creations, or Choose from Our 20+ Done-For-You Products to Start
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-4 mt20 mt-md0">
                     <div class="steps-block">
                        <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                           Choose Payment Gateway 
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 text-center">
                        Integrate your PayPal or Stripe account to receive payments from your clients directly in your account.
                        <br>
                        <span class="w600"> NO Commission or Fee ever... </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-4 mt20 mt-md0">
                     <div class="steps-block">
                        <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                           Publish &amp; Profit 
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 mt-md20 text-center">
                        Now get your smart checkout link with your live professional website to start getting orders on product pages, social media, and right from your emails like a pro. Start selling online 360 degrees - <span class="w600"><br class="d-none d-md-block">- Quick and Easy.</span> 
                        </div>
                     </div>
                  </div>
                 <div class="col-12 w400 f-18 f-md-26 black-clr text-center lh140 mt5 mt20 mt-md40">
                 It Just Takes Minutes to go live…
                 </div>
                 <div class="col-12 w400 f-26 f-md-36 black-clr text-center lh140 mt5 mt10 ">
                     <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
                 </div>
                 <div class="col-12 f-20 f-md-24 w400 lh140 mt20 text-center  ">
                 Plus, with included FREE commercial license, <span class="w600"> this is the easiest &amp; fastest way </span>to start 6 figure <br class="d-none d-md-block">business and sell to desperate local businesses in no time! 
                 </div>
             </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Features Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="second-sec-list">
               <div class="row">
                  <div class="col-12">
                     <div class="f-28 f-md-50 lh140 w600 text-center black-clr">
                        <span class="w700 purple-gradient">We Guarantee,</span>  This Is The Only Selling <br class="d-none d-md-block">
                         Platform You'll Ever Need…
                     </div>
                     <div class="f-20 f-md-28 lh140 w400 text-center black-clr mt20 mt-md30">
                     DFY Products. Membership Sites. Website &amp; Marketplace. Smart Checkout 
                     </div>
                  </div>
               </div>
               <div class="row mt0 mt-md50">   
                  <div class="col-12 col-md-6">
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Tap into the <span class="w600">Super-Hot $25 Trillion E-Selling Industry</span>
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Unlock the Potential of <span class="w600">Your Purchased Agency/Reseller/PLR Right Products</span> &amp; Start Selling Right Away
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Complete <span class="w600">All-in-One Online Selling Platform</span> for Entrepreneurs, Marketers, and Newbies 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Quick Start</span>  Your Online Selling Business with <span class="w600">20+ Done-For-You Products</span>  
                        </div>
                     </div>
   
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600"> Easily Create and Manage Unlimited Products -</span> Digital Products, Courses, Services, and Goods 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Create <span class="w600">Membership Sites to Deliver Products</span>  in Secured &amp; Password Protected Member's Area
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Create</span>  Unlimited Beautiful, Mobile Ready and Fast-Loading <span class="w600">Landing Pages Easily </span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Use Smart-Checkout Links</span> to Directly <span class="w600">Receive Payments</span> from Social Media, Emails &amp; On Any Page
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Precise Analytics</span> to Measure the Performance and Know Exactly What's Working and What's Not
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">SEO Friendly</span> &amp; <span class="w600">Social Media Optimized</span> Pages for More Traffic 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">100% GDPR</span> and <span class="w600">Can-Spam</span> Compliant
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                           Launch Fast - Create Stunning <span class="w600">Websites &amp; Stores</span> for Your and Your Client's Business in Any Niche 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 lh140 w400">   
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Sell Unlimited Products and Accept <span class="w600">Payments Through PayPal and Stripe</span> with Zero Fee, you can Keep 100% Profit
                           </div>
                        </div>                          
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Selling on ClickBank, JVZoo &amp; Warrior Plus? Get <span class="w600">Seamless Integration</span> to Deliver Products on Automation. 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">100+ Battle Tested,</span>  Beautiful, and Mobile-Ready <span class="w600">Page Templates</span> 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Fully Customizable <span class="w600"> Drag &amp; Drop WYSIWYG Page Editor</span> that Requires Zero Designing or Tech Skills 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Inbuilt Lead Management System</span> for Effective Contacts Management in Automation 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Connect Sellero with your Favorite Tools, <span class="w600">20+ Integrations </span> with Autoresponders and other Services 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">128 Bit Secured,</span> SSL Encryption For Maximum Security To Your Videos, Files, And Data
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Manage all the Pages, Products &amp; Customers Hassle-Free, <span class="w600">All in Single Dashboard. </span> 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Step-By-Step Video Training</span> and Tutorials Included 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">24*5 Customer Support</span>
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w600">
                           PLUS, YOU'LL RECEIVE - <span class="w600"> A COMMERCIAL LICENSE IF YOU BUY TODAY </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div>
      <!-- Features Section End -->


     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Testimonial Section Starts -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Checkout What Other Marketers &amp; Early Users <span class="w700 purple-gradient"> Have to Say About Sellero</span>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 ">
                  <div class="responsive-video border-video">
                     <iframe src="https://sellero.oppyo.com/video/embed/mnnpfte4vg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
              <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://sellero.oppyo.com/video/embed/1v91j7nmdq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Chris Sciullo </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Internet Marketer</div>  
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">

            <div class="col mt20 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                      </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Samuel Marco</div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I have <span class="w600">bought multiple software products in the past with commercial license</span> "I've been searching for a solution like this for a long so I can use those purchased software and start selling my services to local businesses for an additional income steam.
                     </p>
                     <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                         <span class="w600">And now SELLERO enabled me to launch my agency services in NO TIME.</span> It's a complete package that delivers on its promises. I couldn't be happier!"
                     </p>
                     
                  </div>
               </div>


            
               
               <div class="col mt20 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/vivek-gour.webp" class="img-fluid d-block mx-auto img-rounded" alt="Sarah M.">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Vivek Gour </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        "I couldn't believe how easy it was to <span class="w600">launch my first online course using SELLERO.</span>
                     </p>
                     <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                        In just few minutes, I had everything set up and ready to go. No technical hurdles, no monthly fees. It's a game-changer!"
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section Ends -->
      <!-- Proudly Section Start -->
      <div class="proudly-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 black-clr caveat proudly-head">
                     Proudly Presenting…
                  </div>
                  <div class="f-28 f-md-50 w700 lh140 white-clr mt20 mt-md50">
                     One Platform for All Your Digital Selling Needs
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid lazyload mt20 mt-md50">
                  <!--<div class="f-22 f-md-28 w700 lh140 white-clr mt20 mt-md80">
                     All-In-One Platform to Market & Sell Any Agency & Reseller Right Products, Courses, Services, and Physical Goods with Zero Technical Hassle.
                  </div>-->
                  <div class="proudly-box mt20 mt-md50">
                     <div class="f-22 f-md-28 w700 lh140 green-clr text-center">
                     Eliminate Any Obstacles That May Be Hindering Your Path to Success
                     </div>
                     <ul class="high-converting-list pl0 mt20 f-18 f-md-20 lh150 w400 white-clr text-start">
                        <li>Sell Online &amp; Keep 100% Profit With You - Never Loose Traffic Again.</li>
                        <li>Switch to One Time Pricing &amp; Save BIG Monthly Fee</li>
                        <li>20+ Ready-to-Use Products to Start Selling Right Away.</li>
                        <li>No Tech Hassles at All &amp; No More Hiring A Costly Designer</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->
      
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------Trendio Section------>
      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 green-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 green-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE AGENCY LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!------Trendio Section Ends------>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Kyza
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-ky">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-md-12 mx-auto">
                  <img src="assets/images/kyza-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center">
                  <div class="mt20 mt-md60 text-center px-sm15 px0">
                     <!--<div class="f-20 f-sm-24 w600 text-center post-heading lh140">
                        STOP Paying To Multiple Marketing Apps For Running Your Business!
                        </div>-->
                     <div class="f-20 f-sm-22 w600 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                        <img src="assets/images/stop.png" class="img-responsive mx-xs-center stop-img">
                        <div class="post-heading1">
                           <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="f-md-41 f-28 w700 black-clr lh140 line-center">	
                     Game Changer All-In-One Growth Suite... <br class="visible-lg">
                     ✔️Builds Beautiful Pages & Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms & Pop-Ups, & ✔️Lets You Send UNLIMITED Emails 					
                  </div>
                  <div class="mt-md25 mt20 f-20 f-md-26 w600 black-clr text-center lh140">
                     All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                  </div>
                  <div class="col-12 col-md-12  mt20 mt-md40">
                     <div class="row d-flex flex-wrap align-items-center">
                        <div class="col-md-7 col-12 mt20 mt-md0">
                           <div class="responsive-video">  
                              <iframe class="embed-responsive-item"  src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
                                 box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                           </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                           <div class="key-features-bg-ky">
                              <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
                                 <li>Create Unlimited Stunning Pages, Popups, And Forms </li>
                                 <li>Mobile, SEO, And Social Media Optimized Pages </li>
                                 <li>Send Unlimited Beautiful Emails For Tons Of Traffic & Sales </li>
                                 <li>Over 100 Done-For-You High Converting Templates </li>
                                 <li>2 Million+ Stock Images & Branding Graphics Kit </li>
                                 <li>Comes With Agency License To Serve & Charge Your Clients </li>
                                 <li>100% Newbie Friendly & Step-By-Step Training Included  </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <img src="assets/images/kyza-gb1.png" class="img-fluid mx-auto d-block" style="width: 100%;">
      <img src="assets/images/kyza-gb2.png" class="img-fluid mx-auto d-block" style="width: 100%;">
      <img src="assets/images/kyza-gb3.png" class="img-fluid mx-auto d-block" style="width: 100%;">
      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Vocalic
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 green-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                  <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                  <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
                  Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                     Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Agency License Included <br> for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt20 mt-md50 white-clr">
                        Use Coupon Code <span class="w800 green-clr">"VOCALIC"</span> for an Extra <span class="w800 green-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Agency License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Agency License Included for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt10 white-clr">
                        Use Coupon Code <span class="w800 green-clr">"VOCALIC"</span> for an Extra <span class="w800 green-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
               <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Vocalic Section ENds -->
      
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase Sellero, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Buying Traffic to Generate Massive Website Visitors
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.  </b></li>
                           <li> Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li> Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Product Creation Guru
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Learn How to Create Your Own Digital Product Like an Expert!</li>
                           <li>If you want to make so much money using the power of the internet, selling your own digital product is one of the best way to do it.</li>
                           <li>The challenge now is that, creating your own product may takes time to master for you to become of the authority in your niche market.</li>
                           <li>The good news is that inside this product is a report that will give you some effective ideas on how to ideally create your own product like a guru.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Viral Marketing Tips and Success Strategies
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Viral marketing buzz occurs daily, in normal, unspectacular circumstances and when you least anticipate it.</b></li>
                           <li>Harnessing the Power of Viral Marketing Where Everyone is Suddenly Talking About Your Company, Product or Service is the an Effective Means to Becoming a Rapid Success in a Short Period of Time!</li>
                           <li>We all recognize what a computer virus is. It spreads from one PC to another in the blink of an eye. Now “Buzz” is the virus of promoting… it spreads to other buyers and has an epidemic of sales of your product or service.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Are you looking for that Laptop Millionaire lifestyle that comes from AFFILIATE MARKETING?</b></li>
                           <li>Ever seen videos of people claiming they make 'a million a day posting a few ads'? If so, then you've come into contact with affiliate marketing. </li>
                           <li>Sure, affiliate marketing is not particularly likely to make you that much money unless you're an absolute pro, and as a general rule, it will involve a lot more work than 'posting a few ads a day' - particularly to begin with!</li>
                           <li>But despite all this, there is truth in the claim that you can earn an awful lot of money doing very little with affiliate marketing. In fact, affiliate marketing is really the only legitimate tool you have to accomplish this kind of lifestyle.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Better Copywriting Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>How To Become A Better Copywriter!</b></li>
                           <li>Content is King. And of the best media to deliver information online is via article content. You maybe confuse between the difference of normal article (academic) writing and copywriting. </li>
                           <li>Copywriting is something that you will convince your readers that your information is real and persuasive. If you are not that good at it then you need to read this book. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Turbo Video Genie
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Put Up A Few Quality Review Videos And You Can Earn Up To $882 Per Video In Passive Income EVERY MONTH! </li>
                           <li>Turbo Video Genie is an easy to use Video Creation Software. Within seconds you will be able to create your own high quality video presentations which you can you for your own websites, or upload to video sharing sites such as YouTube, Vimeo, etc. </li>
                           <li>You probably know how much money you can make from uploading your marketing videos on YouTube.</li>
                           <li>YouTube is the 3rd most visited website in the world. Over 6 billion hours of video are watched each month on YouTube — that's almost an hour for every person on Earth!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        ECommerce Golden Steps
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <b>This 5-part video course will reveal you a formula for succeeding in your own e-commerce business. You will learn how to:</b><br><br>
                           <li>Find a niche </li>
                           <li>Create a brand </li>
                           <li>List your products on Amazon  </li>
                           <li>Get traffic</li>
                           <li>Provide the best customer service</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Turbo ECom + Addon PRO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Turbo eCom Amazon™ Module For PrestaShop is the latest in speed, ingenuity and grace and gives you the ability to effectively incorporate eCommerce functionality to your PrestaShop site. </li>
                           <li>It is the most flexible, easiest-by-far architecture (especially for newbies!) Module that literally enhances your existing PrestaShop site by fetching Products from Amazon to it with complete ease, essentially turning your shop into money.</li>
                           <li>It's about making easy money with less steps for people and therefore more conversion...But mostly, it makes you look more professional and clued in. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Traffic Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>You can have the best product or the best service in the world but if you have no traffic – it’s all completely worthless.</li>
                           <li>This 5 - part video course will help new and experienced marketers generate huge amount of traffic from five different sources. <b>Topics covered: Email Traffic, Facebook Traffic, Forum Traffic, Pinterest Traffic and Youtube Traffic.</b></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Youtube Channel SEO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>In this video training course, you will learn how to take your YouTube Channel to brand new heights.  </b></li>
                           <li>Video marketing is one of the easiest ways to get highly targeted traffic, but you have to do it right. </li>
                           <li>The problem is that ranking on YouTube isn’t as easy as it was back in 2005, or even 2010.</li>
                           <li>Plus, on top of that, there are tons and tons of different marketing strategies out there online.  Many of these work and many of them have faded away.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #11 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           The Art Of Selling Online
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Persuade the toughest potential customers and start winning new prospects effortlesly with 'The Art Of Selling Online'!</b></li>
                           <li>It really comes down to the basic principles that apply to selling in any context. Many people tend to think that Internet selling has to be easy. If you're in that group, it's really not your fault. There are hundreds, really thousands of Internet marketers who are selling you fancy-looking packages with lots of bells and whistles, telling you that all you need to do is buy their offer, and money will start flowing into your PayPal account faster than you can count it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Online Viral Marketing Secrets Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Believe it or not, people interested in whatever it is you are promoting are already congregating online.</b></li>
                           <li>Twitter, YouTube, Facebook and other social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. </li>
                           <li>Too many people try 'viral marketing' and fail. The reason should be obvious. Most people have no clue what viral marketing is really all about. In fact, the more they hear the term, the more confused they get.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Modern Social Media Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>A step-by-step guide to unleash the power of modern social media marketing in 10 steps!</b></li>
                           <li>Here is what you will learn: What Social Media Marketing is and What it Isn't? 8 Reasons Why You Need to Do Social Media Marketing? You Need to Modify Your Social Media Marketing Campaign Based on Your Online Business Type?</li>
                           <li>You are going to understand that whatever the case may be, the endgame is to get people to join your list. You will know how to call to action to the people through social media so they join your list and much more! Includes ready sales materials!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Facebook Messenger Bot Marketing Unleashed
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <b>Facebook Messenger has revolutionized how marketers can access and grow their audience. With this step by step guide... </b><br><br>

                           <li>You are going to understand the importance of Facebook Messenger bots.</li>
                           <li>You will learn to take advantage of the Facebook Messenger platform automation features.</li>
                           <li>You will understand how it can help your business.</li>
                           <li>You can begin to walk prospects through an automated sales funnel.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boost Your Website Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against. </b> </li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn’t always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #16 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        How To Use Webinars For Your Business
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>Webinars are one of the few online marketing methods that allow you to have real time communication with your audience, build credibility and authority and ultimatly drive more sales in profits in your business.</b>
                        </li>
                        <li>With this video course you will learn how to: 
                           <br>1.Create high quality webinars 
                           <br>2.Prepare effectively for a webinar
                           <br>3.Sell products and high ticket offers through webinars
                           <br>4.and much more! 
                        </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Free Traffic System Advanced
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Secure your Website's Bandwith to Receive Tons of Traffic that Will Come Using this Advance Free Traffic System!</b></li>
                        <li>In the first part of the video series package of the Free Traffic System, you were introduced to some effective sources of traffic that you can find leads.</li>
                        <li>Inside this product is a bundle of another set of video tutorials that you can learn the ADVANCE METHODS of Free Traffic Generation that you can apply to your website today or to your clients if you are a freelancer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Surefire Profit System
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>The Ultimate Business Automation System. You're about to Discover the Exact Strategies Used to Build a Million-Dollar Internet Business!</b></li>
                           <li>Making money online isn’t hard. But it doesn’t mean the “not hard” you hear when you read a sales page screaming about how money will fall from the sky at the press of button. While there isn’t one right way to structure an online business, there is a method of doing things that will help you.</li>
                           <li>The Surefire Profit System Course is going to show you how to create this kind of online business for yourself.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        eBook Templates V1
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <b>eBook Templates allow you to create great looking eBook covers in just a few simple steps; Upload template, Enter text, Set color and you're done!</b><br><br>
                           <li>This package comes with 5 eBook templates.</li>
                           <li>Each skin comes in 3 versions; with text, without text and with .psd source you you can make changes!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Auto Video Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Discover how to create your own professional videos in a snap! You don't even have to speak ... the software will do it for you!' </b> </li>
                           <li>If you want to build your brand, chances are you need to have a video to show your expertise. But the if you are not good at creating video or you don't have the necessary tools yet to shoot your own video, this cool software will do the stuffs for you. </li>
                           <li>What this software does is that, it enables you to create video without using cameras, powerpoint, camtasia and even voice overs.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 green-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 green-clr">"ASHUVIP"</span> for an Additional <span class="w700 green-clr">$3 Discount</span> on Sellero
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  Grab Sellero + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 green-clr">"ASHUBUNDLE"</span> for an Additional <span class="w700 green-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Sellero Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #1fe29f;
                         }
                   
                         .cls-3 {
                           fill: #7764ff;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                           <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                           <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                           <g>
                             <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                             <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                           </g>
                         </g>
                         <g>
                           <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                           <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                           <g>
                             <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                             <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                             <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-20 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
                  <ul class="footer-ul w400 f-md-20 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
<!Doctype html>
<html>
   <head>
      <title>NexusGPT</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="NexusGPT | Commercial">
      <meta name="description" content="Futuristic NFC-Tech Creates Digital Business Cards 100% Managed by GPT Ai-Assistant with Just One Touch.">
      <meta name="keywords" content="NexusGPT">
      <meta property="og:image" content="https://www.getnexusgpt.com/preview/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="Website">
      <meta property="og:title" content="NexusGPT | Commercial">
      <meta property="og:description" content="Futuristic NFC-Tech Creates Digital Business Cards 100% Managed by GPT Ai-Assistant with Just One Touch.">
      <meta property="og:image" content="https://www.getnexusgpt.com/preview/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="NexusGPT | Commercial">
      <meta property="twitter:description" content="Futuristic NFC-Tech Creates Digital Business Cards 100% Managed by GPT Ai-Assistant with Just One Touch.">
      <meta property="twitter:image" content="https://www.getnexusgpt.com/preview/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <!-- Link Swiper's CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css" />
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script type='text/javascript' src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <script src="assets/js/timer.js"></script>
        
      <!-- New Timer  Start-->
      <?php
         $date = 'July 22 2023 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
        
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->

         <style>
            .timer-header-top{background: #000; padding-top:10px; padding-bottom:10px;}
         </style>  

         <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10968789258"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10968789258');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 439749027 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-439749027"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-439749027');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 313468456 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-313468456"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-313468456');
</script>




<!-- Global site tag (gtag.js) - Google Ads: 306712238 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-306712238"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-306712238');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10818650306 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10818650306"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10818650306');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 308753853 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-308753853"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-308753853');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10792224287 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10792224287"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10792224287');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10821727055 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10821727055"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10821727055');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10824497363 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10824497363"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10824497363');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10829273347 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10829273347"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10829273347');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10831673865 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10831673865"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10831673865');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 568337586 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-568337586"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-568337586');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10833972856 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10833972856"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10833972856');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 561913114 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-561913114"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-561913114');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10864906425 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10864906425"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10864906425');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 10862372267 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10862372267"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10862372267');
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-10840806352"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-10840806352');
</script>


<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '591482499328167');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=591482499328167&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1735276043516465');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1735276043516465&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1754890194860458');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1754890194860458&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '830284504825482');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=830284504825482&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '622685582658803');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=622685582658803&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->





<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '657580102231465');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=657580102231465&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '397841505881400');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=397841505881400&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1427921811021455');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1427921811021455&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1130621381169428');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1130621381169428&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '784403155503878');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=784403155503878&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '546674586547480');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=546674586547480&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '641341254172552');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=641341254172552&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '450983180346362');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=450983180346362&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '432166435685314');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=432166435685314&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1568686166893197');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1568686166893197&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '645811396899052');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=645811396899052&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->





<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '803684574207106');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=803684574207106&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1101916784051397');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1101916784051397&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '613984380195566');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=613984380195566&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code →

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '5409470042505088');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=5409470042505088&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '136498442754401');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=136498442754401&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->













<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '614254060636678');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=614254060636678&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

      </head>
      <body>
      
      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-16 white-clr w700 text-md-start text-center">
                     Use Coupon Code <span class="orange-clr">"NEXUS"</span>  for <br class="d-block d-md-none">Extra <span class="orange-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-18 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-16 f-md-18 timerbg oswald">00</span><br><span class="f-12 f-md-14 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn top-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>	

      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }

                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }

                                 .cls-2, .cls-3 {
                                 fill: #00a4ff;
                                 }

                                 .cls-4 {
                                 fill: #f19b10;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                                 <g>
                                    <g>
                                       <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"/>
                                       <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"/>
                                       <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"/>
                                    </g>
                                    <g>
                                       <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"/>
                                       <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"/>
                                       <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"/>
                                       <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"/>
                                       <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"/>
                                       <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"/>
                                       <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"/>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"/>
                                    <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"/>
                                    <g>
                                       <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"/>
                                       <g>
                                          <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"/>
                                          <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"/>
                                       </g>
                                       <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"/>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 w500 white-clr text-md-end text-center">
                           <li><a href="#features" class="t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                           <li><a href="#demo" class="t-decoration-none">Demo</a></li>
                           <li class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="pre-heading f-md-24 f-20 w500 lh140">
                     Promote the First-to-Market Business Amplifier Tech…
                  </div>
               </div>
               <div class="col-12 mt80 head-design relative">
                  <div class="gametext">
                     Futuristic Technology
                  </div>
                  <div class=" f-md-45 f-28 w600 text-center black-clr lh140">
                  <span class="underline-text"> NFC-Tech Creates Digital Business Cards 100%</span>  <span class="underline-text">Managed by GPT Ai-Assistant </span> with Just One Touch. <span class="w400"> Zero Tech Hassles. Zero Headache.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-28 w500 text-center lh140 white-clr text-capitalize">
               Today NexusGPT Is Also Coming With 20+ Ready-To-Use Products & FREE Commercial License To Start Selling Online & Profiting Right Away!
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/bixdym9ln1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="f-28 f-md-45 w600 lh140 white-clr">Coming Soon</div>
                  <!-- <div class="calendar-wrap">
                     <ul class="list-head pl0 m0 f-19 lh140 w400 white-clr">
                        <li>	Launch Your & Client's Products & Services Online with ZERO Tech Hassles.</li>
                        <li>	Profit with Agency/Reseller/PLR Right Products You Ever Purchased.</li>
                        <li>	Create Stunning Websites, E-stores and Membership Sites Easily</li>
                        <li>	Sell On Your Own Marketplace & Keep 100% Profits</li>
                        <li>	Smart-Checkout Links To Get Orders Directly from Social Media, Emails or Anywhere Else</li>
                        <li>	Sell High In-Demand Services with <span class="w700">Included Commercial License.</span> </li>
                     </ul>
                  </div> -->
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section Start -->
      <div class="step-section relative">
         <div class="container"> 
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh140 text-capitalize text-center black-clr">
                     NexusGPT Create Digital Business Cards Managed by GPT Ai-Assistant <span class=" orange-clr"> In 4 Easy Steps </span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row row-cols-1 row-cols-md-2 gap80 justify-content-center mt0 mt-lg80">
                     <div class="col">
                        <div class="steps-block step1">
                           <div class="f-18 f-md-20 lh140 w500 white-clr step-box">
                              Step 1
                           </div>
                           <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block mt20" alt="Add A Product ">
                           <div class="f-24 f-md-28 w600 black-clr lh140 mt20 text-center">
                              Choose a template 
                           </div>
                           <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-center">
                              Login and create a digital business card for your niche. 
                              Choose from our beautiful templates or design your own.
                           </div>
                        </div>
                     </div>
                     <div class="col">
                        <div class="steps-block step2">
                           <div class="f-18 f-md-20 lh140 w500 white-clr step-box">
                              Step 2
                           </div>
                           <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block mt20" alt="Upload">
                           <div class="f-24 f-md-28 w600 black-clr lh140 mt20 text-center">
                              Customize GPT Assistance
                           </div>
                           <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-center">
                              Customize GPT Assistance according to your brand, Image, colour, and theme. 
                           </div>
                        </div>
                     </div>
                     <div class="col">
                        <div class="steps-block step1">
                           <div class="f-18 f-md-20 lh140 w500 white-clr step-box ">
                              Step 3
                           </div>
                           <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                           <div class="f-24 f-md-28 w600 black-clr lh140 mt20 text-center">
                              Publish Business Card 
                           </div>
                           <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-center">
                              Setup all your Personal details, social media links, location, bio, about and Publish it.
                           </div>
                        </div>
                     </div>
                     <div class="col">
                        <div class="steps-block">
                           <div class="f-18 f-md-20 lh140 w500 white-clr step-box">
                              Step 4
                           </div>
                           <img src="assets/images/step4.webp" class="img-fluid mx-auto d-block mt20" alt="Upload">
                           <div class="f-24 f-md-28 w600 black-clr lh140 mt20 text-center"> 
                              Write on NFC Card 
                           </div>
                           <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-center"> 
                              Use a free NFC writing app to easily copy the published URL and write it to the NFC tag instantly 
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
            
            <div class="row mt20 mt-md80">
               <div class="col-12 w400 f-20 f-md-24 black-clr text-center lh140 ">
                  It Just Takes Minutes to go live… 
               </div>
               <div class="col-12 w400 f-26 f-md-36 black-clr text-center lh140 mt10 ">
                  <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt10 text-center">
                  Plus, with included FREE commercial license, <u class="w600 "> this is the easiest & fastest way </u> to tap into the fast-growing NFC Market and sell to desperate local businesses in no time!
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr cta-text">
                     Use Coupon Code <span class="w600 orange-clr">"NEXUS"</span> for an Additional <span class="w600 orange-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To NexusGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- second-section Start -->
      <div class="second-section">
         <div class="container">
            <div class="second-sec-list">
               <div class="row">
                  <div class="col-12">
                     <div class="f-28 f-md-45 lh140 w600 text-center black-clr">
                        <span class="w700 orange-clr">We Guarantee,</span>  The ultimate solution for <br class="d-none d-md-block">
                         your business growth needs.… 
                     </div>
                  </div>
               </div>
               <div class="row mt0 mt-md50">   
                  <div class="col-12 col-md-6">
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">First-to-Market</span> NFC Tech Digital Business Cards with GPT-Assistance That brings a revolutionary combination 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Capture Unlimited Leads </span> with Ready to Use NFC Digital Business cards.
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Supercharge your Online Presence</span> with NFC Cards 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Market & Sell</span> your Products, Course, Services   
                        </div>
                     </div>
   
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           All-in-One Online Platform for <span class="w600">Creators, Marketers & Local Business Owners </span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Create Unlimited</span> Beautiful, Mobile Ready Business Cards 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Precise Tracking & Analytics To Measure Every Click -</span> Make The Right Decisions For Future Success! 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">SEO Friendly</span> & <span class="w600">Social Media Optimized</span>  Business Cards for More Traffic 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">100% GDPR</span> and <span class="w600">Can-Spam</span> Compliant 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Plus, you'll also receive Commercial to Start Selling 
                           Pro Level Services to your Clients Right Away </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 lh140 w400">   
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Let <span class="w600"> GPT Assistance will Manage all your Business Cards 24X7</span>
                           </div>
                        </div>                          
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Customize GPT Assistance </span> according to your brand, Image, colour, and theme 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Next-Gen Drag And Drop Editor </span> To Create Pixel Perfect Business cards from Scratch 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Grab More Leads & Manage Them – </span> Manage Your Prospects & Customers with Inbuilt Lead Management 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Connect NexusGPT with your Favourite Tools, <span class="w600">20+ Integrations</span> with Autoresponders, and other Service 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">128 Bit Secured,</span> SSL Encryption For Maximum Security To Your Videos, Files, And Data 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">Step-By-Step Video Training</span> and Tutorials Included 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">24*5 Customer Support</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div>
      <!-- second-section End -->

      <section class="grey-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 text-center text-md-start">
                  <div class="f-28 f-md-38 lh140 black-clr w600">
                     What does NFC mean (Near Field Communication)?
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                     NFC, or Near Field Communication, refers to the short-range exchange of data within a distance of up to 4 centimeters.
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                     Commonly employed in credit and debit cards for facilitating contactless transactions.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20">
                  <img src="assets/images/nf-cmmunication.webp" alt="NFC Cmmunication" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row mt20 mt-md100 align-items-center">
               <div class="col-12 col-md-6 text-center text-md-start order-md-2">
                  <div class="f-28 f-md-38 lh140 black-clr w600">
                     What does GPT- Assistance mean ? 
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                     GPT-Assistance is an AI-powered Chat GPT Assistant available 24/7 to help you grow your business, convert leads into sales, enhance your brand presence, boost your social profile and followers, and achieve unimaginable possibilities.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 order-md-1">
                  <img src="assets/images/gpt-assistance.webp" alt="GPT Assistance" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </section>

      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w400 lh140 black-clr">
                    And Here Comes <span class="w600">NexusGPT</span>
                  </div>
                  <img src="assets/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5">
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md30">
                     An extraordinary fusion of NFC technology and GPT-Assistance. Experience the power-packed synergy as NFC tech-enabled business cards effortlessly share your details, while GPT-Assistance effectively communicates, converts leads into sales, enhances brand presence, and boosts social followership.
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md80">
                  <div class="video-box1">
                     <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/bixdym9ln1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="benefit-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-38 w600 lh140 black-clr">
                     NFC tech and GPT Assistance benefit your business, as well as your clients' businesses, in various ways. 
                  </div>
               </div>
            </div>
            <div class="benefit-wall mt20 mt-md80">
               <div class="row">
                  <div class="col-12">
                     <ul class="f-20 f-md-22 w400 lh140 black-clr know-list pl0 mt0 mb0">
                        <li><span class="w600">Realtors:</span> Enhanced Property Listings, Share Location, Images & Videos </li>
                        <li><span class="w600">Lead Generation:</span> Effortlessly lead generation with a simple touch. </li> 
                        <li><span class="w600">Secure Document Transfer:</span> NFC facilitates secure and encrypted document transfer between devices, ideal for sensitive information sharing. </li>
                        <li><span class="w600">Share photos & videos:</span> Bump phones to share photos, playlists, or videos.</li>
                        <li><span class="w600">Show offers:</span> Present offers in discos, gyms, music & dance classes or any physical store.</li> 
                        <li><span class="w600">Business Profile:</span> Show your project, clients, catalog, products & services on Business card pages. </li>
                        <li><span class="w600">NFC wedding invitations.</span> Share wedding, engagement, birthday, anniversary Invite Using NFC Invitation, add video or music in addition to key information about the occasion, place, and time </li>
                        <li><span class="w600">NFC rings.</span> NFC ring contains important information about a person, especially relevant for vulnerable people, for example, the elderly, whose memory is deteriorating.</li>
                        <li><span class="w600"> Public Transportation:</span> NFC technology enables contactless ticketing systems for seamless and efficient public transportation experiences.</li>
                        <li><span class="w600">Social Brand:</span> Conveniently collect customer reviews and ratings & Make them loyal.</li>
                        <li><span class="w600"> Sell: </span>Sell your courses, services & physical products. </li>
                        <li><span class="w600">Prebooking:</span> Book an appointment or Demo, Reserve a table. </li>
                        <li><span class="w600">Affiliate Promotions: </span>Showcase your website, funnel, bonuses & YouTube Review Videos</li>
                        <li><span class="w600">Social Connectivity:</span>Enable seamless contact through text messages, WhatsApp, Skype, Messenger, or email without any tech hassle. </li>
                        <li><span class="w600">Use in Medical field:</span> Retrieve patient information efficiently in healthcare settings with a simple tap on NFC tags. </li>
                        <li><span class="w600">Smart Advertising:</span> NFC tags can be integrated into advertising materials, allowing users to access additional information or promotional offers by simply tapping their devices.</li>
                        <li><span class="w600">Event Management:</span> NFC wristbands or badges can streamline event registration, access control, and cashless transactions for attendees. </li>
                        <li><span class="w600">Loyalty Programs:</span> NFC-based loyalty cards enable customers to conveniently collect and redeem rewards points at participating businesses. </li>
                        <li><span class="w600">Social Networking:</span> NFC can simplify the process of connecting and sharing information on social media platforms. </li>
                        <li><span class="w600"> Digital Business Cards:</span> NFC-enabled business cards can instantly share contact information and link to online profiles, simplifying networking.</li>
                        <li><span class="w600">Inventory Management:</span> NFC tags can be used for efficient inventory tracking, asset management, and supply chain optimization.1= </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Checkout What Other Marketers & Early Users <span class="w700 orange-clr"> Have to Say About NexusGPT</span>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 ">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/mnnpfte4vg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Craig McIntosh </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant </div>  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/1v91j7nmdq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Chris Sciullo </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Internet Marketer</div>  
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!--Awesome Section -->
      <div class="awesome-sections">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="awesome-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-45 f-28 lh140 w600 black-clr relative trigger-block">
                              Hello There,
                           </div>
                           <div class="f-md-28 f-22 lh140 w600 white-clr mt20 mt-md40 text-center">
                              It's Dr. Amit Pareek along with my business partner Atul Pareek!
                           </div>
                        </div>
                        <div class="col-12 text-center mt20 mt-md30">
                           <div class="row row-cols-1 row-cols-md-3 justify-content-center">
                              <div class="col">
                                 <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto mt20 mt-md0" alt="Dr. Amit Pareek">
                                 <div class="f-26 f-md-32 w600 lh140 text-center white-clr  mt20 ">
                                    Dr. Amit Pareek
                                 </div>
                              </div>
                              <div class="col">
                                 <img src="assets/images/atul-pareek.webp" class="img-fluid d-block mx-auto mt20 mt-md0" alt="Atul Pareek">
                                 <div class="f-26 f-md-32 w600  lh140 text-center white-clr mt20 ">
                                     Atul Pareek
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 f-18 f-md-22 lh160 w400 white-clr mt20 mt-md30 text-center text-md-start">
                           We have happily served over 150,000 customers online and have 18+ years of online business experience combined.
                        </div>
                        <div class="col-12 f-18 f-md-22 lh160 w400 white-clr mt20 text-center text-md-start">
                           Not only have we achieved the desired results of boosted followership, brand image, ratings, and reviews for ourselves, but more significantly, we have also facilitated thousands of local business owners and clients in attaining immediate results and profits. 
                        </div>
                        <div class="col-12 f-22 f-md-28 w600 lh160 white-clr mt20 mt-md30 text-center">                              
                           <div class="hithere">                              
                             <div class="w600 f-md-32 f-24 lh160 w400 text-center black-clr"> And it's not just us who are cashing from sharing profiles... </div>
                             <div class="w400 f-md-32 f-24 lh160 text-center black-clr mt20">
                             Millions of Creators, Marketers & Local Business Owners <br class="d-none d-md-block">
                              are utilizing this opportunity to reach thousands upon thousands of people & <span class="w600 black-clr">getting unfair advantage. </span>     
                             </div>
                           </div>                 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Awesome Section End-->

      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-44 w400 lh140 black-clr">
                    <span class="w600">NFC in Business</span> - What It Is and How It Can Be Used
                  </div>
                  <img src="assets/images/nfc-bussiness.webp" alt="NFC Bussiness" class="mx-auto d-block img-fluid mt5">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 col-md-6 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to connect
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to pay
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to get information
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for entertainment
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for shopping
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for Social Sharing
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <!-- thatsall-section Start -->
      <div class="enough-sec relative">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     Benefits of Using NFC Technology for Business
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md80">
                  <div class="power-shape power-shape-right">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/automated-funnel.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start">
                              The global market size of NFC technology is going to more than double by 2024. By the end of 2022, its volume was $20.3 billion, and this figure is expected to reach <span class="w600">$47.3 billion in one year.</span> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape power-shape-left">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/automated-funnel1.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              To date, there are 2 billion NFC-enabled smartphones (out of 3.4 billion smartphones used globally), and 20% of the global population actively use this technology.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape power-shape-right">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/automated-funnel3.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              What’s more, there will be <span class="w600">500 billion IoT and NFC-powered devices by 2030.</span> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/automated-funnel4.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              The recent pandemic has boosted NFС development. According to ABI Research, 96% of surveyed respondents use this technology for some other purpose in addition to contactless payments.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- thatsall-section End -->

      <section class="diff-bussiness">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     The Ways Different Businesses Use NFC
                  </div>
               </div>
               <div class="col-12 col-md-8 mt20 mt-md80 mx-auto">
                  <img src="assets/images/service-nfc.webp" alt="Services NFC" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
      </section>

      <section class="grey-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 lh140 black-clr w600">
                     Motivation behind NexusGPT Development...
                  </div>
                  <img src="assets/images/remove-line.webp" alt="Remove line" class="mx-auto d-block img-fluid mt5">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <img src="assets/images/easy-wireless.webp" class="mx-auto d-block img-fluid">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     The thought of developing this incredible solution stemmed from the desire to explore the untapped potential of NFC technology and provide the World’s first GPT- Assisted Digital business
                  </div>
                  <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                     - for Creators, Marketers &amp; Local Business Owners
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                     We observed in business meetings, conferences, shopping malls, and other places where personal info is shared through physical cards.
                     <br>
                     But it’s no longer an option, businesses are moving toward the digital era.
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                     So we NEXUS the offline world with a fast-growing digital world using industry-first NFC tech that creates GPT Assisted Business Cards.
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr heading-design">
                     Let’s face it
                  </div>
                  <div class="f-20 f-md-28 w400 white-clr mt20 mt-md30 warning-sec">
                    <span class="red-clr w700 pl30 pl-md0">Attention Creators, Marketers &amp; Local Business Owners </span> <br class="d-none d-md-block">
                     If you are not using the Latest NFC Tech - You Might Short Charging your Business&nbsp;
                  </div>
                  
               </div>
            </div>
            <div class="mt20 mt-md50 lets-wall">
               <div class="row align-items-center">
                  <div class="col-md-4 order-md-2">
                     <img src="assets/images/lets-face-1.webp" alt="Lets Face One" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="col-md-8 mt20 mt-md0 order-md-1">
                     <div class="f-20 f-md-22 w400 lh140 white-clr">
                        Physical business cards can be easily lost or misplaced, making it difficult for customers to access vital business information when needed.
                     </div>
                  </div>
               </div>
            </div>
            <div class="mt20 mt-md50 lets-wall">
               <div class="row align-items-center">
                  <div class="col-md-4">
                     <img src="assets/images/lets-face-2.webp" alt="Lets Face Two" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="col-md-8 mt20 mt-md0">
                     <div class="f-20 f-md-22 w400 lh140 white-clr">
                        Limited Social Presence, Not leveraging digital platforms and social media to enhance brand visibility and engage with customers.
                     </div>
                  </div>
               </div>
            </div>
            <div class="mt20 mt-md50 lets-wall">
               <div class="row align-items-center">
                  <div class="col-md-4 order-md-2">
                     <img src="assets/images/lets-face-5.webp" alt="Lets Face Five" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="col-md-8 mt20 mt-md0 order-md-1">
                     <div class="f-20 f-md-22 w400 lh140 white-clr">
                        Missed Growth Opportunities, reduced efficiency, and potential setbacks in an increasingly digital business landscape.
                     </div>
                  </div>
               </div>
            </div>
            <div class="mt20 mt-md50 lets-wall">
               <div class="row align-items-center">
                  <div class="col-md-4">
                     <img src="assets/images/lets-face-4.webp" alt="Lets Face Four" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="col-md-8 mt20 mt-md0">
                     <div class="f-20 f-md-22 w400 lh140 white-clr">
                        Incompatibility with Modern Trends, not utilizing it can make a company appear outdated or less technologically advanced, potentially affecting its reputation and credibility among customers.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 lh140 black-clr">
                    Ok, Let’s Remove the Objections!!!
                  </div>
                  <img src="assets/images/remove-line.webp" alt="Remove line" class="mx-auto d-block img-fluid mt5">
                  <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md30">
                    <span class="w600"> Empower Your Customers to Stand Out and Establish Themselves as the Leading Experts</span> for Prospects with the Cutting-Edge NFC Technology of NEXUSGPT. Unlock a World of Possibilities and Achieve Unparalleled Success..
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-6 order-2 order-md-1">
                  <img src="assets/images/sb-main.webp" alt="Mobile" class="mx-auto d-block img-fluid">
               </div>
               <div class="col-md-6 order-1 order-md-2">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     With a Simple touch, Empower Your Customers to Seamlessly -
                  </div>
                  <ul class="list-head1 pl0 m0 f-19 lh140 w400 black-clr mt5">
                     <li>Save Contact Information</li>
                     <li>Share Location</li>
                     <li>Grow Mailing Lists</li>
                     <li>Process Payments</li>
                     <li>Showcase Products &amp; Services</li>
                     <li>Drive floods of Sales</li>
                     <li>Enhance Brand Presence and Beyond.</li>
                  </ul>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
               <div class="f-20 f-md-22 w400 lh140 black-clr">
                     Simplify Customer Engagement and Amplify Business Opportunities with Ease.
                  </div>
                  <div class="f-20 f-md-22 w400 l140 black-clr mt15">
                     Say goodbye to the worries and endless waiting for your potential customers to reach out to you. With NEXUSGPT, you take control and initiate meaningful interactions with ease.
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="grey-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-28 f-md-42 w600 lh140 black-clr">
                  We NEXUS NFC Tech Digital Business Cards with GPT-Assistance That brings a revolutionary combination.
                  </div>
               </div>
               <div class="col-md-8 col-12 mt20 mt-md50 mx-auto">
                  <img src="assets/images/nfc-tech.webp" alt="NFC Tech" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 col-12 mt20 mt-md50">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     This powerful combination enhances customer engagement, streamlines operations, and optimizes business processes by leveraging the efficiency and intelligence of GPT-assistance, ultimately driving business growth and customer satisfaction.    
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="proudly-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 white-clr caveat proudly-head">
                     Proudly Presenting…
                  </div>
                  <div class="f-28 f-md-42 w600 lh140 white-clr mt20 mt-md50">
                     One platform for NEXUS the offline world with a fast-growing digital world using industry-first NFC tech that creates GPT Assisted digital Business Cards.
                  </div>
                  <img src="assets/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid  mt20 mt-md50">
                  <div class="proudly-box mt20 mt-md80">
                     <div class="f-20 f-md-24 w600 lh140 white-clr">
                        Eliminate Any Obstacles That May Be Hindering Your Customers' Path to Success
                     </div>
                     <div class="f-20 f-md-24 w600 lh140 white-clr mt15">
                        And Nexus the Fusion of NFC &amp; GPT for Limitless Possibilities
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>


      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to connect 
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to pay
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to get information
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for entertainment
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Step Section Start -->
      <div class="step-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-capitalize text-center black-clr">
                     NexusGPT Create Digital Business Cards Managed by GPT Ai-Assistant <span class=" orange-clr"> In 4 Easy Steps </span>
                  </div>
               </div>
            </div>
            <div class="row mt60 mt-md100 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="f-24 f-md-32 w600 black-clr lh140 mt20 text-center text-md-start d-flex align-items-center">
                     <img src="assets/images/step-01.webp" class="img-fluid d-block mr15" alt="Add A Product ">
                     Choose a template 
                  </div>
                  <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-cente text-md-startr">
                     Login and create a digital business card for your niche. 
                     Choose from our beautiful templates or design your own.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                 <div class="video-box-testi border-video">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/pmntg1nuf7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                 </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/proven-arrow2.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-6 col-12 order-md-2 relative">
                  <div class="f-24 f-md-32 w600 black-clr lh140 mt20 text-center text-md-start d-flex align-items-center">
                     <img src="assets/images/step-02.webp" class="img-fluid d-block mr15" alt="Add A Product ">
                     Customize GPT Assistance
                  </div>
                  <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-cente text-md-startr">
                     Customize GPT Assistance according to your brand, Image, colour, and theme. 
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <div class="video-box-testi border-video">
                     <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/zeb97bcst7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/proven-arrow1.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="f-24 f-md-32 w600 black-clr lh140 mt20 text-center text-md-start d-flex align-items-center">
                     <img src="assets/images/step-03.webp" class="img-fluid d-block mr15" alt="Add A Product ">
                     Publish Business Card 
                  </div>
                  <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-cente text-md-startr">
                     Setup all your Personal details, social media links, location, bio, about and Publish it. 
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <div class="video-box-testi border-video">
                     <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/vhzy95v71b" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                  </div>
               </div>
            </div> 
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/proven-arrow2.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
               </div>
            </div>
            <div class="row mt60 mt-md100 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="f-24 f-md-32 w600 black-clr lh140 mt20 text-center text-md-start d-flex align-items-center">
                     <img src="assets/images/step-04.webp" class="img-fluid d-block mr15" alt="Add A Product ">
                     Write on NFC Card
                  </div>
                  <div class="f-18 f-md-20 w400 black-clr lh140 mt10 text-cente text-md-startr">
                  Use a free NFC writing app to easily copy the published URL and write it to the NFC tag instantly 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <video class="border-video video-box-testi" width="100%" height="auto" loop="" autoplay="" muted="muted">
                     <source src="assets/images/step-1.mp4" type="video/mp4">
                 </video>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 w400 f-20 f-md-24 black-clr text-center lh140 ">
                  It Just Takes Minutes to go live… 
               </div>
               <div class="col-12 w400 f-26 f-md-36 black-clr text-center lh140 mt10 ">
                  <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt10 text-center">
                  Plus, with included FREE commercial license, <u class="w600 "> this is the easiest &amp; fastest way </u> to tap into the fast-growing NFC Market and sell to desperate local businesses in no time!
               </div>
            </div>          
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Demo Section Start -->
      <div class="demo-section" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center white-clr">
                  Check Out NexusGPT In Action! 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="assets/images/down-arrow.webp" class="img-fluid d-block mx-auto vert-move" alt="arrow">
               </div>
               <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">
                  <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://cmsservices.oppyo.com/video/embed/bx13h4xkpz" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="Demo Video"></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                    Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 orange-clr">"NEXUS"</span> for an Additional <span class="w600 orange-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To NexusGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Demo Section End -->

      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Checkout What Other Marketers &amp; Early Users <span class="w700 orange-clr"> Have to Say About NexusGPT</span>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 ">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/mnnpfte4vg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/1v91j7nmdq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Chris Sciullo </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Internet Marketer</div>  
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <div class="feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-28 f-md-45 lh140 black-clr w400">
                    Welcome to the <span class="underline-text1 w600">Futuristic Digital ERA</span>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 justify-content-center mt0 mt-md50">
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/personal-branding.webp" alt="Personal Branding" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Personalize Your VA & Improve Personal Branding
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/businessman-social.webp" alt="Generate Leads" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Realtors Generate Leads like a Pro 
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/sell-product.webp" alt="Sell Product" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Sell Product, Courses & Services in a Single Touch
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/boost-followership.webp" alt="Boost followership" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Boost followership on Facebook, YouTube & Other Social Handles
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/loyalty-programs.webp" alt="Loyalty Programs" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Run Loyalty Programs, collect and redeem rewards or discounts
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/event-ticketing.webp" alt="Event Ticketing" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Event Ticketing & allowing attendees to enter events.
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/share-business-profile.webp" alt="Share Business Profile" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Share Business Profile
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/nfc-wedding-invitations.webp" alt="NFC Wedding Invitations" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        NFC Wedding Invitations
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="era-box">
                     <img src="assets/images/loyalty-programs.webp" alt="Loyalty Programs" class="mx-auto d-block img-fluid">
                     <div class="f-20 f-md-22 w400 lh40 black-clr mt30">
                        Affiliate Promotions
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
               


      <!-- Swiper -->
      <div class="swiper mySwiper first-card first-slider">
         <div class="container">
            <div class="row">
               <div class="col-12 tet-center">
                  <div class="f-28 f-md-45 lh140 w700 white-clr text-center">
                     Checkout Some Amazing Business Card Templates we have in NexusGPT 
                  </div>
                  <div class="f-24 f-md-32 w700 lh140 text-center mt20 mt-md30 orange-clr mb20 mb-md50">
                  Business Card Templates 
                  </div>
               </div>
            </div>
         </div>
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card1.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card2.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card3.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card4.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card5.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card6.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card7.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card8.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card9.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card10.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card11.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card12.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card13.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card14.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card15.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card16.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card17.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card18.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card19.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bussiness-card20.webp" alt="Bussiness Card">
            </div>
         </div>
         <div class="swiper-pagination"></div>
      </div>


      <div class="swiper mySwiper bussiness-card first-card">
         <div class="container">
            <div class="row">
               <div class="col-12 tet-center">
                  <div class="f-24 f-md-32 w700 lh140 text-center mt20 mt-md30 orange-clr mb20 mb-md50">
                  Business Card Templates 
                  </div>
               </div>
            </div>
         </div>
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <img src="assets/images/bakery.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/business-coach.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/catering.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/chef.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/crypto-trader.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/deliver-agency.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/dj.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/ebook.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/gardening.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/gift-shop.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/mart.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/pet-care.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/real-estate.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/teacher.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/technician.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/the-church.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/travel-agency.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/travel-agency-2.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/wedding-organizer.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/yoga-teacher.webp" alt="Bussiness Card">
            </div>
         </div>
         <div class="swiper-pagination"></div>
      </div>


      <div class="swiper mySwiper bio-card">
         <div class="container">
            <div class="row">
               <div class="col-12 tet-center">
                  <div class="f-24 f-md-32 w700 lh140 text-center mt20 mt-md30 orange-clr mb20 mb-md50">
                     Bio Card Templates 
                  </div>
               </div>
            </div>
         </div>
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <img src="assets/images/bio1.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio2.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio3.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio4.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio5.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio6.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio7.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio8.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio9.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio10.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio11.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio12.webp" alt="Bussiness Card">
            </div>
            <div class="swiper-slide">
               <img src="assets/images/bio13.webp" alt="Bussiness Card">
            </div>
         </div>
         <div class="swiper-pagination"></div>
      </div>


      

     
      <!-- Features Headline Start -->
      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 white-clr">
                     Here Are Ground - Breaking Features That <br class="d-none d-md-block"> Make 
                     NexusGPT A Cut Above The Rest                     
                  </div>
                  <img src="assets/images/move-arrow.webp" alt="Move" class="mx-auto d-block img-fluid arrow-head">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Headline End -->
      <!-- Feature Section Start -->
      <div class="features-one">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 relative">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Futuristic Platform for Creators, Marketers & Local Business Owners 
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                     No more relying on costly service providers. NexusGPT brings together everything you need to start generating floods of leads & profit by creating digital business cards. 
                     <br><br>
                     Create Separate businesses for you & your clients. 
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20">
               <div class="col-12">
                  <img src="assets/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Create No-Touch Digital Business Cards In Any Niche 
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                     Choose, create & manage No-Touch Digital Business cards from Done-for-You Templates, 
                     Share Profile, Manage Links, Generate Leads & Profit. 
                     <br><br>
                     Tailor-made for your business or client, our templates are designed to captivate specific audiences and entice them with irresistible offers. Say goodbye to complex & old school business cards and hello to simplified success with NexusGPT.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>

      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-center">
                     Generate & Manage GPT-Assistant 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  GPT Assistant will streamlines operations, and optimizes business processes by leveraging the efficiency and intelligence of GPT-4, ultimately driving business growth and customer satisfaction. 
                  <br><br>
                  <span class="w600">
                  Change GPT Assistance Branding, Image, Theme Colour, Name and much more. 
                  </span>
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Fully Drag and Drop Digital Business Card Editor that Requires Zero Designing or Tech Skills
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  A Next generation, pixel perfect & drag & drop editor to create whatever you want on page and wherever you want without even 1 Pixel error. We have reinvented page editor which is not like old school bootstrap editor that set your elements without your control.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/f9.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  We have designed the builder completely newbie friendly to accomplish task at pro level. Change font, colour, link, elements & much more effortlessly. Here're few-
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 list-gaps mt0">
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr1.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Image/Logo
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        <span class="w600">Upload Any Image and Edit it,</span> Even you Can use this Image in Future for any other Campaign
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr2.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Video
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        Show Any Video on Page with <span class="w600">Professional Inbuilt HLS Video Player</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr3.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Button
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Show effective CTA that <span class="w600">entices your visitors to click, sign up, read and buy.</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr4.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Section
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        <span class="w600">Edit and add a new section in 1 click.</span> You can customize it separately for Mobile and Desktop Visitors
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr5.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Headline/Text
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Create Engaging Headline with <span class="w600">Advanced Typography Editing Technology</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Countdown Timer
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Ticking Timers are legitimately one of the most effective ways to <span class="w600">create scarcity and generate more sales for you.</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Form
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        Use our DFY optin-forms and <span class="w600">send leads directly to Your favourite autoresponder</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Shapes
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Draw any Box, Circle or lines to <span class="w600">differentiate any part which helps you to get more focused by your visitors</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Social
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        <span class="w600">
                        Add social media icons anywhere on your site and connect visitors with brand or ask them to share your pages for viral traffic.</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-18 f-md-20 w600 lh140  black-clr">
                  And much more…you can build almost anything from scratch using this next generation editor in few minutes.
               </div>
            </div>
         </div>
      </div>

      <div class="features-three">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Inbuilt Lead Management System For Effective Contacts Management In Automation
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Tap into ready to use & beautiful Lead forms in 8 different colours. Use these forms to <span class="w600">collect maximum leads & keep them organized in your very own lead management panel.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="assets/images/f10.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Intelligent Analytics To Measure The Performance - Know Exactly What's Working And What's Not.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                     <span class="w600"> Know your numbers -</span> what's performing well and what simply is not working.
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/f11.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w400 lh140  black-clr">
                     <span class="w600">Checkout unique visitors, conversions stats in nice graphs on the analytics page</span> to build strategy & improve your marketing campaigns to get more Returns and Profits.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="features-twelve">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 white-clr text-capitalize features-title">
                     Connect NexusGPT With Your Favourite Tools. 20+ Integrations With Autoresponders And Other Service
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/f12.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     Setup integration in few clicks and send all your leads into your favorite autoresponders for prompt communication.<span class="w600"> Send a series of personalized email messages to new leads at pre-defined interval.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Create Fast-Loading, Mobile Ready Business Cards To Share Details with Every Single Mobile User
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Each website & landing page created with this NexusGPT is <span class="w600">100% mobile-ready & loads ultra-fast on ANY mobile device</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f13.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="assets/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     SEO Friendly & Social Media Optimized Business Cards For Floods of Traffic  
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     You get 100% SEO pages <span class="w600">to get better SERP rankings and additional search traffic.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="assets/images/f14.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="assets/images/proven-arrow1.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     128 Bit Secured, SSL Encryption For Maximum Security To Your Files, Data And Cards 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Normally you'd have to pay extra for a SSL certificate, but not with us. You're not only getting encryption more secure than Ft. Knox with NexusGPT, but you're getting that same SSL Encryption for every website you ever create with your account. Normally, this would run you thousands. But we'll hook you up when you sign up today at no additional charge.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f15.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="assets/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Manage All The Cards, GPT Assistance, Customers Hassle- Free, All In Single Dashboard
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     This lets you manage everything effortlessly. <span class="w600">Use our never offered before customized drag & drop business central dashboard.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="assets/images/f16.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="assets/images/proven-arrow1.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     No Domain and NO Hosting is Needed
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     <span class="w600"> With NexusGPT, You're 100% covered-</span>
                     No worries of monthly hosting bills, domains and websites. You simply create engaging and high-converting websites, Membership sites etc with NexusGPT deploy them and see the magic rolling in.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f17.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Feature Section End -->
      <section class="more-feature-sec">
         <div class="container">
            <div class="row">
                <div class="col-12 text-center f-22 f-md-45 w700 lh140 black-clr text-capitalize">
                     Here Are Some More Features
                  </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="row g-0">
                      <!-- first -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-rb boxborder top-first ">
                              <img src="assets/images/k1.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Round-the-Clock Support
                              </div>
                          </div>
                      </div>
                      <!-- second -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-rb boxborder ">
                              <img src="assets/images/k2.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  No Coding, Design or Technical
                                  <br class="visible-lg "> Skills Required
                              </div>
                          </div>
                      </div>
                      <!-- third -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-b boxborder ">
                              <img src="assets/images/k3.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Regular Updates
                              </div>
                          </div>
                      </div>
                      <!-- fourth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-r boxborder ">
                              <img src="assets/images/k4.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Complete Step-by-Step Video
                                  <br class="visible-lg ">Training and Tutorials Included
                              </div>
                          </div>
                      </div>
                      <!-- fifth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-r boxborder ">
                              <img src="assets/images/k5.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Newbie Friendly &amp; Easy To Use
                              </div>
                          </div>
                      </div>
                      <!-- sixth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder ">
                              <img src="assets/images/k6.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Limited Commercial License
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
         </div>
      </section>

      <!-- no-comparison Section Start -->
      <section class="no-comparison">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 lh140 black-clr">
                     There Is <span class="w700 orange-clr">NO Comparison</span>  Of Futuristic <br class="d-none d-md-block"> NexusGPT Technology...
                     </span>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7 order-md-2">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     Correct! There's no match to the power-packed NexusGPT because it's built with years of experience, designing, coding, debugging & real user testing to <span class="w600"> get you guaranteed results.</span> 
                     <br><br>
                     <span class="w600">NexusGPT brings you speed & ease of use on a silver platter.</span> It is highly optimized to build only high-converting mobile responsive and fast-loading digital business cards to literally crush your competition. 
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <img src="assets/images/quickfunnel-thumbsup.webp" alt="NexusGPT Thumbsup" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     <span class="w600"> So, grab NexusGPT today for this low one-time price because, after launch, it will never be available again at this price.</span>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- no-comparison Section End -->


      <div class="this-open-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="youknow-block ">
                        <div class="f-md-45 f-28 w700 lh150 text-center text-uppercase">
                            This Opens
                        </div>
                        <div class="f-md-38 f-28 w600 lh150 text-center mt-md0">
                            A Huge Business Opportunity for You…
                        </div>
                    </div>    
                </div>
            </div>
            <div class="row mt-md50 mt20">
                <div class="col-12 f-md-23 f-22 w600 lh150 black-clr text-center">
                    Here’re some real-life examples of how average freelancers are charging anywhere from $500-$2000 a pop for building client’s websites on platforms like Fiverr, Odesk, Freelancers, etc.
                </div>
            </div>
            <div class="row mt-md70 mt20 align-items-center">
                <div class="col-md-5">
                    <img src="./assets/images/boomer-smith.webp" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20">
				 <span class="w600">Boomer Smith</span> has made <span class="w600">$20,400 on Fiverr in just 4 months</span> of his start in Dec’21 &amp; still counting.  
              After quitting his Tele calling Job which he never liked, he started searching for
                    any Freelancing opportunity on the internet where he encountered Fiverr and found 2 Big words Wed Design, and WordPress.
                    <br><br> Now, he is charging $1,200 per project and has completed almost 17 projects in just 4 months which made him $20,400 in income.
                </div>
            </div>
            <div class="row mt-md70 mt20 align-items-center">
                <div class="col-md-5 order-md-2">
                    <img src="./assets/images/brain.webp" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
                    Meet <span class="w600">Brain C.</span> ‘Rising Talent’ (official tag given by Upwork) earned almost <span class="w600">$17,495 in just 47 Days on Upwork.</span>
                    <br><br> His journey started on Upwork with Graphic Designing in 2019. But as he came to know about many newbies earning huge just by servicing WordPress Website development, he gave it a shot and started earning more than his graphic
                    design service.
                    <br><br> He started his Website Development Services for the Lawyers &amp; charges them <span class="w600">$3,499 per website on Upwork, which Lawyers are happy to pay.</span>
                </div>
            </div>
            <div class="row mt-md70 mt20 align-items-center">
                <div class="col-md-6">
                    <img src="./assets/images/bright.webp" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
                    <span class="w600">Bright Dock</span> has made almost <span class="w600">$183k (till yet) on Freelancer</span> and has completed Over 180 successfully completed Freelancer projects in under 2 years.
                    <br><br> He started his WordPress Designing Freelancing journey 2 years ago on Freelancer and now has a freelancing team of 40 digital designers to work for him, so he just focuses on growth. He is charging <span class="w600">$100 per Hr. and people are ready to pay for his awesome job.</span>
                </div>
            </div>
        </div>
    </div>


            <div class="license-section">
               <div class="container ">
                  <div class="row ">
                     <div class="col-12 f-28 f-md-43 w400 white-clr text-center lh140">
                     <span class="red-clr alert-sec">Alert! FREE Limited Time Upgrade:</span><br class="d-none d-md-block"> 
                        <span class="w600">Get Commercial License to Use NEXUSGPT To Create An Incredible Income Serving Your Customers!</span>
                     </div>
                     <div class="col-12 mt20 mt-md30 f-md-22 f-18 w400 lh140 white-clr text-center ">
                     As we have shown you, there are tons of businesses which enter in the market every day – Realtors, Creators, Marketers, Yoga Gurus, fitness centres or anyone else who wants to grow online. They desperately need your services & are eager to pay you BIG for them.
                     </div>
                  </div>
                  <div class="row align-items-center mt20 ">
                     <div class="col-12 col-md-6">
                        <img src="assets/images/agency-license-img.webp" class="img-fluid d-block mx-auto img-animation" alt="Agency License">
                     </div>
                     <div class="col-12 col-md-6 mt20">
                        <div class="f-md-22 f-18 w400 lh140 white-clr ">
                           Boost your & your client's business by supercharging digital business cards 100% managed by GPT- Assistant and help businesses improve their brand, generate floods of leads & profits without any Touch.
                           <br><br>
                           They will pay top dollar to you when you deliver top notch services to them. We've taken care of everything so that you can deliver those services simply and easily.
                           <br><br>
                           <span class="w600">Note :</span> This special Agency licence is being included in the Pro offer ONLY for this launch. Take advantage of it now because it will never be offered again. 
                        </div>
                     </div>
                  </div>
               </div>
            </div>


 <!-- Silver Platter Section End -->
    <div class="potential-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-45 f-24 w600 text-center black-clr">
                        Here's A <span class="orange-clr">Never-Ending List Of Businesses Ready</span>  To Pay You For Your Digital Business Card Services...
                    </div>
                </div>
            </div>
            <div class="row row-cols-2 row-cols-md-3 gap30 justify-content-center mt20 mt-md50">
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n1.webp" class="img-fluid mx-auto d-block" alt="Business Coaches">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Business Coaches 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n2.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">                 
                         Affiliate Marketers
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n3.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         E-Com Sellers
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n4.webp" class="img-fluid mx-auto d-block" alt="All Info">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         All Info  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n5.webp" class="img-fluid mx-auto d-block" alt="Gym">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Gym 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n6.webp" class="img-fluid mx-auto d-block" alt="Music Classes">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Music Classes
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n7.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Sports Clubs
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n8.webp" class="img-fluid mx-auto d-block" alt="Bars">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Bars  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n9.webp" class="img-fluid mx-auto d-block" alt="Restaurants">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Restaurants 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n10.webp" class="img-fluid mx-auto d-block" alt="Hotels">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Hotels
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n11.webp" class="img-fluid mx-auto d-block" alt="Schools">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Schools
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n12.webp" class="img-fluid mx-auto d-block" alt="Churches">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Churches  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n13.webp" class="img-fluid mx-auto d-block" alt="Taxi Services">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Taxi Services 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n14.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Garage Owners
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n15.webp" class="img-fluid mx-auto d-block" alt="Dentists">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Dentists
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n16.webp" class="img-fluid mx-auto d-block" alt="Carpenters">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Carpenters  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n17.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Chiropractors 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n18.webp" class="img-fluid mx-auto d-block" alt="LockSmiths">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Locksmiths
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n19.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Home Tutors
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n20.webp" class="img-fluid mx-auto d-block" alt="Real-Estate">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Real-Estate  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="assets/images/n21.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Lawyers 
                      </div>
                   </div>
                </div>
             </div>
        </div>
    </div>
    
    <!-----Proven Technology------>


    <section class="benefit-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-38 w600 lh140 black-clr">
                     What Makes NexusGPT different from other NFC Tech Apps... 
                  </div>
               </div>
            </div>
            <div class="benefit-wall mt20 mt-md80">
               <div class="row">
                  <div class="col-12">
                     <ul class="f-20 f-md-22 w400 lh140 black-clr know-list pl0 mt0 mb0">
                        <li class="w600">Futuristic NFC App 100% Managed by GPT Ai-Assistant </li>
                        We NEXUS NFC Tech Digital Business Cards with GPT-Assistance That brings a revolutionary combination. <br>
                        This powerful combination enhances customer engagement, streamlines operations, and optimizes business processes by leveraging the efficiency and intelligence of GPT-assistance, ultimately driving business growth and customer satisfaction. 
                        <li class="w600">No-Touch Digital Business Cards </li>
                        NexusGPT Creates No-Touch Digital Business cards for you & for your clients. <br>
                        Templates specifically crafted with the needs of creators, marketers, and local business owners. 
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
    

      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-44 w400 lh140 black-clr">
                    <span class="w600">Future of NFC in Business</span>
                  </div>
                  <div class="f-md-22 f-20 lh140 w400 text-center mt20">
                     The concept of NFC is quite understandable. In a nutshell, this technology allows two devices to communicate with each other, performing different business tasks while in close proximity. 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 col-md-6 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to connect
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to pay
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch to get information
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for entertainment
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for shopping
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Touch for Social Sharing
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <div class="testimonial-section3">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Checkout What Other Marketers &amp; Early Users <span class="w700 orange-clr"> Have to Say About NexusGPT</span>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 ">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/mnnpfte4vg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://cmsservices.oppyo.com/video/embed/1v91j7nmdq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Chris Sciullo </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Internet Marketer</div>  
               </div>
            </div>
         </div>
      </div>
    

      <!-- Bonus Section Start -->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <img src="assets/images/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="f-18 f-md-22 w400 lh140 text-center">
                     When You Grab Your NexusGPT Account Today, You'll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Live Training - 0-10k a Month With NexusGPT (First 1000 buyers only - $1000 Value)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Start Your Own SIX Figure Business In FEW Minutes. This awesome LIVE training will help you to build a SIX FIGURE Business PLUS THREE lucky attendees will be selected randomly to win $100 cash each! And there will be a Live Q & A Session at the end of the training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #2
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on How To Start Online Business
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Learn how to start online business and sell information you're passionate about. Combine this useful info with Nexusgpt Reloaded, and get best results for your business like you always aspired.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Traffic Generation Video Workshop
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Get proven tips and tricks on how to drive laser targeted leads to your offers and boost your email list in a cost effective manner. When used with Sellero Reloaded, it reaps long term business benefits.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on Affiliate Marketing
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              There’s no easier way to skyrocket your income, get your products in front of thousands of new prospects and build a recognized brand than by connecting with seasoned affiliate marketers.Affiliates will help build momentum before, during and after your product launch, but they do much more than that.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #5
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on How to Boost Sales in Marketing
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Social media is designed to keep us entertained. Many of us spend hours trawling through content looking to find things that are unique and special to add to our feeds.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w600 white-clr lh240 text-center ">
                        That's A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w600">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---Bonus Section End-->

      <!-- Guarantee Section Start -->
      <div class="noneed-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-20 f-md-22 w400 lh140 text-center">
                     Just 7 Minutes it Takes to Start Profiting Online…
                  </div>
                  <div class="f-md-45 f-28 w700 black-clr lh140 text-center mt15">
                     With NEXUSGPT, Turn Your Worries Into <br class="d-none d-md-block">
                     A Profitable Opportunity
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 w400 lh140">
                           <ul class="noneed-list pl0">
                              <li><span class="w600">No need to pay monthly</span> for expensive hosting & software services</li>
                              <li class="w600">No more profit sharing or transaction fee.</li>
                              <li><span class="w600">No need to lose your traffic & leads</span> with 3rd party marketplaces- EVERYTHING is in your control!</li>
                              <li><span class="w600">No need to spare a thought</span> for building website, store, or e-learning sites!</li>
                              <li><span class="w600">No need to worry</span> about lack of technical or design skills</li>
                              <li><span class="w600">No more complicated & time-consuming processes.</span> It is an easy 3 steps & your products are ready to get published within minutes.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/worry.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 f-18 f-md-20 w400 lh140 mt20">
                        And… Finally say YES to living a LAPTOP lifestyle that gives you an ability to spend more time with your family and have complete financial FREEDOM.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="white-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Tap & Share Contact
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Display a Profile Page
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Lead Generation
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50 text-center">
                  <div class="connect-wall">
                     <div class="f-24 f-md-28 w600 lh140 black-clr">
                        Get More Followers
                     </div>
                     <div class="video-wall mt20">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://nexusgpt.oppyo.com/video/embed/uo1phni9k4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 white-clr text-center col-12 mb-md40">Test Drive It Risk FREE For 30 Days
               </div>
               <div class="col-md-6 col-12 mt15 mt-md0">
                  <div class="f-18 w400 lh140 white-clr"><span class="w600">Your purchase is secured with our 30-day money back guarantee.</span>
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your clients risk free. If you face any Issue or don't get results you desired after using it, just raise a ticket on support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section" id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-18 f-md-20 w400 text-center lh140">
                     Take Advantage of Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 text-center mt10 ">
                     Get NEXUSGPT For a Low <br class="d-none d-md-block ">One-Time- Price, No Monthly Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 black-clr">
                     Use Coupon Code <span class="blue-clr w600">"NEXUS"</span> for an Additional <span class="blue-clr w600 "> $3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50 ">
                  <div class="row gx-5">
                     <div class="col-12 col-md-6 ">
                        <div class="table-wrap ">
                           <div class="table-head text-center ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                                 <defs>
                                    <style>
                                       .cls-1n {
                                       fill-rule: evenodd;
                                       }

                                       .cls-1n, .cls-2n {
                                       fill: #00a4ff;
                                       }

                                       .cls-3n {
                                       fill: #0e1846;
                                       }

                                       .cls-4n {
                                       fill: #f19b10;
                                       }
                                    </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                    <g>
                                       <g>
                                       <g>
                                          <path class="cls-3n" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"/>
                                          <path class="cls-3n" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"/>
                                          <path class="cls-3n" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"/>
                                       </g>
                                       <g>
                                          <path class="cls-3n" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"/>
                                          <path class="cls-3n" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"/>
                                          <path class="cls-3n" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"/>
                                          <path class="cls-3n" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"/>
                                          <rect class="cls-3n" x="343.41" y="75.29" width="71.69" height="22.79"/>
                                          <rect class="cls-3n" x="343.41" y="121.79" width="71.69" height="22.79"/>
                                          <rect class="cls-3n" x="343.41" y="169.5" width="71.69" height="22.79"/>
                                       </g>
                                       </g>
                                       <g>
                                       <path class="cls-2n" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"/>
                                       <path class="cls-4n" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"/>
                                       <g>
                                          <path class="cls-1n" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"/>
                                          <g>
                                             <path class="cls-1n" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"/>
                                             <path class="cls-1n" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"/>
                                          </g>
                                          <path class="cls-1n" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"/>
                                       </g>
                                       </g>
                                    </g>
                                 </g>
                                 </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 personal-shape white-clr">Start</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list pl0 f-18 lh140 w400 black-clr">
                                 <li>50 Businesses/Subdomains</li>
                                 <li>Up To 50,000 Pages Visits/Month</li>
                                 <li>Capture 50,000 Leads In A Hassle Free</li>
                                 <li>NFC Tech Ebabed Ready to Share Business Cards</li>
                                 <li>Get Virtual Assistance for your Business Cards</li>
                                 <li>Launch FAST - Create Beautiful Business Cards</li>
                                 <li>Create Unlimited Business Card Templates from Scratch</li>
                                 <li>Fully Drag & Drop Advanced WYSIWYG Editor</li> 
                                 <li>DFY Proven Converting, Mobile Responsive & Ready-to-Go Business Card Templates</li>
                                 <li>Manage Leads Effortlessly & make the most from them with our powerful lead management feature.</li>
                                 <li>Get FREE Storage upto 100GB and Bandwidth Upto 250 GB/m</li>
                                 <li>Seamless Integration with 20+ Top Autoresponders to Send Emails to Your Clients/leads on Automation </li>
                                 <li>Mobile Friendly Fast Loading Business Cards So you Will Never Lose any Contact/client again.</li>
                                 <li>Manage All Business Cards, Virtual Assistance Hassle- Free, All in Single Dashboard.</li>
                                 <li>Inbuilt SEO Management For Business Cards</li>
                                 <li>128-Bit SSL Encryption for Maximum Security of your Data & Files</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</li>
                                 <li>100% ADA, GDPR - SPAM Compliant</li>
                                 <li>Customized Drag & Drop Business Central Dashboard</li>
                                 <li>No Coding, Design or Tech Skills Needed</li>
                                 <li>Easy and Intuitive To Use Software with Step by Step Video Training</li>
                                 <li>24*5 Customer Support</li>
                                 <li>Unparallel Price</li>
                                 <li>Enjoy Bundles of Leads and Conversions from Viral Business Cards</li>
                                 <li>Toddler- Friendly Easy to Use Dashboard</li>
                                 <li>A-Z complete video training included</li>
                                 <li>100% Newbie friendly</li>
                                 <li>Commercial license Included to charge monthly from clients for top-notch trending solution</li>
                                 <li class="headline f-md-24">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li class="cross-sign">Fast Action Bonus 1</li>
                                 <li class="cross-sign">Fast Action Bonus 2</li>
                                 <li class="cross-sign">Fast Action Bonus 3</li>
                                 <li class="cross-sign">Fast Action Bonus 4</li>
                                 <li class="cross-sign">Fast Action Bonus 5</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
                                 <a href="https://www.jvzoo.com/b/109143/397959/2"><img src="https://i.jvzoo.com/109143/397959/2" alt="NexusGPT Personal" border="0" class="img-fluid d-block"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                                 <defs>
                                    <style>
                                       .cls-1 {
                                       fill: #fff;
                                       }

                                       .cls-2 {
                                       fill-rule: evenodd;
                                       }

                                       .cls-2, .cls-3 {
                                       fill: #00a4ff;
                                       }

                                       .cls-4 {
                                       fill: #f19b10;
                                       }
                                    </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                    <g>
                                       <g>
                                          <g>
                                             <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"/>
                                             <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"/>
                                             <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"/>
                                          </g>
                                          <g>
                                             <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"/>
                                             <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"/>
                                             <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"/>
                                             <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"/>
                                             <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"/>
                                             <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"/>
                                             <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"/>
                                          </g>
                                       </g>
                                       <g>
                                          <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"/>
                                          <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"/>
                                          <g>
                                             <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"/>
                                             <g>
                                                <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"/>
                                                <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"/>
                                             </g>
                                             <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"/>
                                          </g>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 commercial-shape white-clr">Commercial</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 lh140 w400 white-clr">
                                 <li>Unlimited Businesses/Subdomains</li>
                                 <li>Up To Unlimited Pages Visits/Month</li>
                                 <li>Capture Unlimited Leads In A Hassle Free</li>
                                 <li>NFC Tech Ebabed Ready to Share Business Cards</li>
                                 <li>Get Virtual Assistance for your Business Cards</li>
                                 <li>Launch FAST - Create Beautiful Business Cards</li>
                                 <li>Create Unlimited Business Card Templates from Scratch</li>
                                 <li>Fully Drag & Drop Advanced WYSIWYG Editor </li>
                                 <li>DFY Proven Converting, Mobile Responsive & Ready-to-Go Business Card Templates</li>
                                 <li>Manage Leads Effortlessly & make the most from them with our powerful lead management feature.</li>
                                 <li>Get FREE Storage upto Unlimited and Bandwidth Upto Unlimited GB/m</li>
                                 <li>Seamless Integration with Unlimited Top Autoresponders to Send Emails to Your Clients/leads on Automation </li>
                                 <li>Mobile Friendly Fast Loading Business Cards So you Will Never Lose any Contact/client again.</li>
                                 <li>Manage All Business Cards, Virtual Assistance Hassle- Free, All in Single Dashboard.</li>
                                 <li>Inbuilt SEO Management For Business Cards</li>
                                 <li>128-Bit SSL Encryption for Maximum Security of your Data & Files</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</li>
                                 <li>100% ADA, GDPR - SPAM Compliant</li>
                                 <li>Customized Drag & Drop Business Central Dashboard</li>
                                 <li>No Coding, Design or Tech Skills Needed</li>
                                 <li>Easy and Intuitive To Use Software with Step by Step Video Training</li>
                                 <li>24*7 Customer Support</li>
                                 <li>Unparallel Price</li>
                                 <li>Enjoy Bundles of Leads and Conversions from Viral Business Cards</li>
                                 <li>Toddler- Friendly Easy to Use Dashboard</li>
                                 <li>A-Z complete video training included</li>
                                 <li>100% Newbie friendly</li>
                                 <li>Commercial license Included to charge monthly from clients for top-notch trending solution</li>
                                 <li class="headline f-md-24">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Fast Action Bonus 1</li>
                                 <li>Fast Action Bonus 2</li>
                                 <li>Fast Action Bonus 3</li>
                                 <li>Fast Action Bonus 4</li>
                                 <li>Fast Action Bonus 5</li>

                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://www.jvzoo.com/b/109143/397652/2"><img src="https://i.jvzoo.com/109143/397652/2" alt="NexusGPT Commercial" border="0" class="img-fluid d-block mx-auto"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->

      <!-- Contact Sec Start -->
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              Thanks for checking out NexusGPT!
                           </div>
                        </div>
                     </div>
                     <div class="row mt20 mt-md80 justify-content-between">
                        <div class="col-12 col-md-8 mx-auto">
                           <div class="row">
                              <div class="col-12 col-md-6">
                                 <div class="contact-shape">
                                    <img data-src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto minus ls-is-cached lazyloaded" alt="Amit Pareek" src="assets/images/amit-pareek.webp">
                                    <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Dr. Amit Pareek
                                    </div>
                                 </div>
                              </div>
                              <div class="col-12 col-md-6 mt20 mt-md0">
                                 <div class="contact-shape">
                                    <img data-src="assets/images/atul-pareek.webp" class="img-fluid d-block mx-auto minus ls-is-cached lazyloaded" alt="Atul Pareek" src="assets/images/atul-pareek.webp">
                                    <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Atul Pareek
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Contact Sec End -->

      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr">
                  Frequently Asked <span class="gradient-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row ">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do I need to download or install NexusGPT somewhere?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NO! You just create an account online and you can get started immediately. NexusGPT is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. And it works across all browsers and all devices including Windows and Mac.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">                              
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don't offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you did everything before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140">
                              Is NexusGPT compliant with all guidelines & compliances?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party services which are not a part of NexusGPT while choosing it for your business.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              What is the duration of service with this NexusGPT launch special deal?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary renewal to our founder members for buying from us early.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              How is NexusGPT different from other available tools in the market?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Well, we have a nice comparison chart with other service providers. We won't like to boast much about our software, but we can assure you that this is a cutting-edge technology that will enable you to create and sell stunning niche websites at such a low introductory price.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its 100% optional & not mandatory for working with NexusGPT. Those are recommended if you want to multiply your benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Will I get any training or support for my questions?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member's area. You will also get Premium Customer Support so you never get stuck or have any issues.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Is NexusGPT Windows and Mac compatible?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We've already stated that NexusGPT is a web-based solution. So, it runs directly on the web and works across all browsers and all devices.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh140 ">If you have any query, simply visit our  <a href="mailto:support@oppyo.com" class="blue-clr ">support@oppyo.com</a></div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->

      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 passport-content">
                  <div class="text-center">
                     <div class="f-45 f-md-45 lh140 w600 text-center white-clr caveat final-shape">
                        FINAL CALL
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0 mb20 mb-md50">
                     You Still Have The Opportunity To Take Your Game To The Next Level… <br> Remember, It's Your Make Or Break Time!
                  </div>
                  <!-- CTA Btn Section Start -->
                  <div class="col-12">
                     <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                      Free Commercial License + A Low One-Time Price
                     </div>
                     <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                        Use Coupon Code <span class="w600 orange-clr">"NEXUS"</span> for an Additional <span class="w600 orange-clr">$3 Discount</span>
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn">Get Instant Access To NexusGPT</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                     </div>
                  </div>
                  <!-- CTA Btn Section End -->
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }

                           .cls-2 {
                           fill-rule: evenodd;
                           }

                           .cls-2, .cls-3 {
                           fill: #00a4ff;
                           }

                           .cls-4 {
                           fill: #f19b10;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <g>
                                 <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"/>
                                 <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"/>
                                 <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"/>
                              </g>
                              <g>
                                 <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"/>
                                 <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"/>
                                 <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"/>
                                 <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"/>
                                 <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"/>
                                 <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"/>
                                 <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"/>
                              </g>
                           </g>
                           <g>
                              <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"/>
                              <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"/>
                              <g>
                                 <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"/>
                                 <g>
                                    <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"/>
                                    <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"/>
                                 </g>
                                 <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"/>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © NexusGPT 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   
<!-- timer --->
<script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            showRemaining();
            //tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               //timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-18 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-18 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-14 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-16 f-md-18 timerbg oswald">' + seconds + '</span><br><span class="f-12 f-md-14 w500 smmltd">Sec</span> </div>';
            }
         
         }


      //timerBegin(59 * 60); 




      function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 59*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         timerBegin(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();



      </script>
            <!--- timer end-->

  <!-- Swiper JS -->
  <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
              <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
        1320:{
            slidesPerView: 6,
            spaceBetween: 30,
        },
        1500:{
            slidesPerView: 7,
            spaceBetween: 30,
        },
        1900:{
            slidesPerView: 8,
            spaceBetween: 30,
        },
      },
    });
  </script>    
  
  
  <script>
    var swiper = new Swiper(".mySwiper.bio-card", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
      },
    });
  </script> 

   </body>
</html>
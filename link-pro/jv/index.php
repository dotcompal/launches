<html>
   <head>
      <title>JV Page - LinkPro JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LinkPro | JV">
      <meta name="description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta name="keywords" content="LinkPro">
      <meta property="og:image" content="https://www.linkpro.live/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LinkPro | JV">
      <meta property="og:description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta property="og:image" content="https://www.linkpro.live/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LinkPro | JV">
      <meta property="twitter:description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta property="twitter:image" content="https://www.linkpro.live/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-GTtd-wozS-Rm7H-DqGe" crossorigin="anonymous" embed-id="oppyo-GTtd-wozS-Rm7H-DqGe"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/load-gdpr/5d9132b263ea11ed" crossorigin="anonymous" embed-id="5d9132b263ea11ed"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
     
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">
                  <div class="f-28 f-md-45 w700 lh140 white-clr">
                     LinkPro
                  </div>
                  <div class="d-flex align-items-center flex-wrap justify-content-center justify-content-md-end text-center text-md-end mt20 mt-md0">
                     <ul class="leader-ul f-16 f-md-18">
                        <li>                        
                           <a href="https://docs.google.com/document/d/1mUsukJfZtlTNG8XGWkel0tbaOEdofSuU/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank" class="active">JV Docs </a>
                        </li>
                        <li>
                           <a href="https://docs.google.com/document/d/186dne19eYkkQuMTwD0MernbF67Qmf7sv/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes & Bonuses </a>
                        </li>
                        <li>
                           <a href="#funnel">SP Preview</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/393326" class="affiliate-link-btn ml-md15 mt25 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row">
                     <div class="col-12 text-center">
                        <div class="f-16 f-md-20 w500 white-clr lh140 text-uppercase preheadline">
                        Backbone of Internet Marketing is here…
                        </div>
                     </div>
                     <div class="col-12 mt-md30 mt20 f-md-45 f-28 w700 text-center white-clr lh140"> 
                        Revolutionary Software <span class="orange-clr"> Effortlessly beautify and convert your links into profit-pulling machines with Zero Hassles! </span>
                     </div>
                     <div class="col-12 mt20 mt-md25 text-center">
                        <div class="f-18 f-md-20 w500 text-center lh140 white-clr text-capitalize">               
                           Link Cloaker || Link Shortener || QR Code Generator ||
                           Bio-Link Generator || Product & Payment Links <br class="d-none d-md-block">
                           Manage all Your Links from a Single Platform 
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt20 mt-md70">
                     <div class="col-12 col-md-8">
                        <!-- <img src="assets/images/pbox.webp" alt="" class="mx-auto img-fluid d-block"> -->
                        <!-- <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://quickfunnel.dotcompal.com/video/embed/m6gsa685be" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                           </div>
                        </div>  -->
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <img src="assets/images/launch-date.webp" alt="" class="mx-auto img-fluid d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- <img src="assets/images/chef-icon.webp" alt="Chef-ico" class="img-fluid d-none d-md-block ele1">
         <img src="assets/images/puff.webp" alt="Puff" class="img-fluid d-none d-md-block ele2"> -->
      </div>
      <!-- Header Section End -->

      <div class="live-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="live-formbg">
                     <div class="col-12 col-md-12">
                        <div class="f-24 f-md-45 w600 text-center lh140 black-clr">
                           Grab Your <span class="red-clr1">JVZoo</span>  Affiliate Link to Jump on <br class="d-none d-md-block"> This Amazing Product Launch
                        </div>
                     </div>
                     <div class="col-md-12 mx-auto mt20 mt-md30">
                        <div class="row justify-content-center align-items-center">
                           <div class="col-12 col-md-3">
                              <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" alt="Jvzoo">
                           </div>
                           <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                              <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/393326" class="f-18 f-md-20 w600 mx-auto" target="_blank">Request Affiliate Link</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- List Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center d-flex mx-auto justify-content-center">
               <span class="promote f-20 f-md-45 w600 lh140 white-clr">7 Reasons</span><div class="f-20 f-md-44 w600 lh140 black-clr winner-heading"> To Promote LinkPro</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/thumbsup.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 f-md-20 f-18 w500 lh150 mt30 mt-md0">
                  <ul class="f-18 f-md-20 w400 lh140 black-clr know-list pl0 mt0">
                     <li>Brand new first to Market Product with Proven results!</li> 
                     <li>Change lengthy & Boring Links with your Own Branding at a low Onetime FEE!</li> 
                     <li>Own Branded Beautiful Links Need of Every Online/Affiliate Marketer/Business Owner ! </li>
                     <li>Works for Open Market, all list types, niches, and audiences! </li>
                     <li>High-converting sales pages with Proven Funnel </li> 
                     <li>Irresistible offer! </li>
                     <li>Sells itself… everyone wants these results! </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- List Section End -->

      <!-- Problem Section Start -->
      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-26 f-md-45 w700 white-clr heading-design">
                      The <span class="red-clr">Problem</span> Is
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="f-18 f-md-20 w400 white-clr text-capitalize lh140 text-center lh180">
               In this fast-moving era, people prefer to spend quality time with their family & loved ones... & there is no limit on the budget to buying happiness. They love to take their day meal or dinner 
on weekends in a good restaurant... Today people search for a good restaurant online & check the review & ratings but the problem is -
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 col-12 ">
                  <img src="assets/images/depress.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <ul class="problem-list f-18 w600 lh140 pl0 black-clr mt0">
                     <li>Are You Losing Commissions?</li>
                     <li>Getting Low Clicks & Traffic?</li>
                     <li>Facing low In boxing Open rates?</li>
                     <li>Hard to Compete with Big Brands?</li>
                     <li>Leaving Tons of Commission on Table?</li> 
                  </ul>
               </div>
               <div class="f-18 f-md-20 w400 white-clr text-capitalize lh140  mt20 mt-md40">
                  Are you struggling to build your brand, generate leads, and turn a profit using social media? You're not alone - many marketers feel the same way. But what if we told you that your worries could end forever? 
                  <br><br>
                  Until now, only big brands, influencers, and marketers have been able to truly leverage the power of social media to drive massive business growth and build their brands. 
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-28 f-md-45 w700 red-clr lh140">
                     If Yes, then you are in Big Trouble….. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Problem Section End -->

      <section class="change_forever">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-22 f-md-28 w500 white-clr lh140">
                     But things are going to change forever.
                  </div>
                  <div class="f-28 f-md-45 w700 white-clr lh140">
                     No Matter What Niche you are Working…
                  </div>
               </div>
            </div>
            <div class="change_forever-box">
               <div class="row row-cols-1 row-cols-md-4 gap30  justify-content-center mt20 mt-md70">
                  <div class="col">
                     <img src="assets/images/iconb4.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        Affiliate Marketing & MMO
                     </div>
                  </div>
                  <div class="col">
                     <img src="assets/images/iconb7.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        eCommerce Store Owner
                     </div>
                  </div>
                  <div class="col">
                     <img src="assets/images/iconb10.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        Local Cafe & Quick Service Restaurants
                     </div>
                  </div>
                  <div class="col">
                     <img src="assets/images/iconb11.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        Social Media Marketers 
                     </div>
                  </div>
                  <div class="col">
                     <img src="assets/images/iconb5.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        Bloggers & Content Creators
                     </div>
                  </div>
                  <div class="col">
                     <img src="assets/images/iconb1.png" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15 white-clr text-center">
                        Webinar
                     </div>
                  </div>
               </div>
            </div>  
             <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 white-clr lh140">
                     Or any other niche…
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 mt20 white-clr">
                  Transform your concerns into a lucrative machine and boost your leads and profits to new heights
                  <br><br>
                  With our proven strategies, even small businesses can achieve big results. We'll show you how to effectively build your brand, generate quality leads, and increase your profits using the power of Smart Links.
                  <br><br>
                  Say goodbye to the frustration and stress. Let us help you unlock its true potential and take your business to the next level.
                  </div>
               </div>
             </div>
         </div>
      </section>

      <!-- Short Video Section -->
      <section class="short-videos">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 white-clr lh140">
                  We All Know Short-videos, TikTok and Instagram Reels are the Future of Social media & Majority of them only
                  </div>
                  <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  allow Links in their user’s Bio
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt0 mt-md70">
               <div class="col">
                  <img src="assets/images/icona1.png" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/icona2.png" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/icona3.png" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/icona4.png" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/icona5.png" class="img-fluid mx-auto d-block">
               </div><div class="col">
                  <img src="assets/images/icona6.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>
      <!-- Short Video Section End-->

      <!-- Imagine BioLink Section -->
      <section class="imagine-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="imagine-box">
                     <div class="f-20 f-md-22 w400 white-clr text-capitalize lh140">
                        Imagine what if you don’t have a Bio-Link or Put your Ugly, Lengthy & Unbranded Links ????
                        <br><br>
                        It simply means that you are shooting yourself in the root… <br><br>
                        Yes, My Friend, Maybe I am harsh but it’s TRUE…..
                     </div>
                  </div>
                  <div class="f-28 f-md-45 w700 white-clr lh140 m0 mt-md70">
                     After 6 Months of Brain Storming, coding, debugging & added Super Powerful features we are back with LinkPro Which Make It a Cut above the Rest
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Imagine BioLink Section End -->

      <!-- Presenting Section Start -->
      <div class="presenting-sec">
         <div class="presenting-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w700 white-clr lh140 presenting-head">
                        Presenting…
                     </div>
                     <div class="f-28 f-md-45 w700 white-clr lh140">
                    <span class="orange-clr">Game-Changing All-In-One Marketing Suite</span> Lets YOU Help <span class="white-clr"> Local Restaurants </span> Grow Online & Charge BIG for Your 7-Minute Service
                     </div>
                     <div class="mt20 mt-md90">
                        <img src="assets/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid">
                     </div>
                  </div>
               </div>
            </div>
            <img src="assets/images/pb-icon.webp" alt="product-icon" class="img-fluid d-none d-md-block ele3">
            <div class="f-18 f-md-20 w500 lh140 mt20 mt-md70 white-clr text-center">
               First-To-Market Technology That Creates Profit Pulling ‘Cloacked Links, QR Codes & Bio-Links Without Any Tech Skills! Works for Affiliate Marketer, Influrancer, Digital Markert or anyone have Online Presence. 
            </div>
         </div>
      </div>
      <!-- Presenting Section End -->

      <!-- Presenting-List Section Start -->
      <!-- <div class="presenting-list">
         <div class="container">
            <div class="presenting-list-wrap">
               <div class="row">
                  <div class="col-md-6">
                     <ul class="f-18 f-md-20 w500 lh140 black-clr reason-list pl0 mt0">
                        <li>Create Hot Restaurant Websites & Start Agency Services  to Make Huge Profit</li>
                        <li>Online Seat Booking to maximize reservation, Avoid Waiting & to Grab leads</li>
                        <li>Maximize the Happy Customers to Grow Organically </li>
                        <li>QR Code to share Menu & Restaurant Site</li>
                        <li>Ready to Share Bio-Link & Skyrocket Leads</li>
                        <li>Build 100% mobile responsive, SEO & social media friendly to Grab 360° traffic</li>
                     </ul>
                  </div>
                  <div class="col-md-6">
                     <ul class="f-18 f-md-20 w500 lh140 black-clr reason-list pl0 mt0">
                        <li>Highly Customizable Done For You Theme</li>
                        <li>100% ADA Compliant Websites</li>
                        <li>Built-in Auto responder to Convert Potential Customers into Buyers</li>
                        <li>Pop-up & Sticky Bar to Promote offer & Deals</li>
                        <li>Store & Share All Your Files at Lightning-Fast Speed with World's Best CDNs</li>
                        <li>Commercial License Included So Your Buyers Can Sell Services And Make Profit Big Time.</li>
                     </ul>
                  </div>
               </div>
            </div>   
         </div>
      </div> -->
      <!-- Presenting-List Section End -->

      <!-- Feature Description Start -->
      <!-- <div class="feature-description">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     <span class="yellow-bg white-clr">LinkPro is Packed with Super Powerful</span><br> Features That Make It A Cut Above The Rest
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Website">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start">  <span class="w700"> Create Done For You Mobile Responsive Website </span> </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Book A Seat">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Online Seat Booking to maximize reservations,</span> Avoid Waiting,  Grab leads & Make Recurring Customers 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="QR Code">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">QR Code to Offer a contactless experience </span> & Collect customer data.
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Bio-Links">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Ready to Share Bio-Link & Skyrocket Your Leads</span> . Take followers to your Social Sites .
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Autoresponder">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Built-in Autoresponder to Convert </span> Potential Customers into Buyers 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Pop-up & Bar">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Pop-up & Sticky Bar to Promote offers & Deals,</span> 
                     Collet lead & convert Potential Buyers
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Drive ">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Store & Share All Your Marketing Files at Lightning-Fast Speed with World's Best CDNs</span>
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div> -->
      <!-- Featue-Description End -->

      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 black-clr">                    
                  Check Out Some <span class="red-clr">GROUND - BREAKING Features</span>        
                  </div>

                  <div class="mt20 mt-md50">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>

                  <div class="w600 f-24 f-md-36 black-clr lh150 text-center mt20 mt-md50">
                  Manage All your Links from a single solution <br class="d-none d-md-block">
Include as much bio-Links on social platforms and enjoy better <br class="d-none d-md-block"> traffic, leads, sales and profits
                  </div>

                  <div class="f-18 w400 lh150 mt10 text-center  black-clr">
                  Don't miss this opportunity to provide a high-quality service to buyers who are eager to <br class="d-none d-md-block"> pay top dollar for it. And the best part? You keep 100% of every sale you make.
                  </div>
               </div>
            </div>
         </div>
         <div class="container mt30 mt-md100">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="w700 f-24 f-md-36 black-clr lh150 text-center text-md-start">
                  Cloak your ugly links -
                  </div>
                  <div class="f-18 w400 lh150 mt10 text-center text-md-start black-clr">
                  To Make Links simple, easy to remember, and more user-friendly.
                  </div>
                  <ul class="f-18 f-md-20 w400 lh140 black-clr reason-list pl0 mt20">
                     <li>Reduce Commission Loss</li>
                     <li>Higher CTR </li>
                     <li>Appears more trustworthy.</li>
                     <li>Track user clicks & Visits.</li>
                     <li>Increase email Delivery Rates.</li>
                     <li>Protects affiliate commissions.</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.png" alt="Website" class="mx-auto img-fluid d-block">
               </div>
            </div>
         </div>
         <div class="features-section mt30 mt-md100">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="w700 f-24 f-md-36 black-clr lh150 text-center text-md-start">
                        Shorten your lengthy links -
                     </div>
                     <div class="f-18 w400 lh150 mt10 text-center text-md-start black-clr">
                        To Make it Short and more user-friendly.
                     </div>
                     <ul class="f-18 f-md-20 w400 lh140 black-clr reason-list pl0 mt20">
                        <li>Appears more trustworthy.</li>
                        <li>Track user clicks & Visits.</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                     <img src="assets/images/f2.png" alt="Book A Seat" class="mx-auto img-fluid d-block">
                  </div>
               </div>
            </div>
         </div>
         <div class="container mt30 mt-md100">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="w700 f-24 f-md-36 black-clr lh150 text-center text-md-start">
                     Generate & Share Dynamic QR Code -
                  </div>
                  <div class="f-18 w400 lh150 mt10 text-center text-md-start black-clr">
                     To Make your Online Presence more Professional.
                  </div>
                  <ul class="f-18 f-md-20 w400 lh140 black-clr reason-list pl0 mt20">
                     <li>Offer Contact less experience.</li>
                     <li>Collect Customer date.</li>
                     <li>Generate leads & Profit </li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f3.png" alt="QR Code" class="mx-auto img-fluid d-block">
               </div>
            </div>
         </div>
         <div class="features-section mt30 mt-md100">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="w700 f-24 f-md-36 black-clr lh150 text-center text-md-start">
                     Ready to Share Bio links -
                     </div>
                     <div class="f-18 w400 lh150 mt10 text-center text-md-start black-clr">
                     Become a Pro on social media.
                     </div>
                     <ul class="f-18 f-md-20 w400 lh140 black-clr reason-list pl0 mt20">
                        <li>Skyrocket your Online Presence.</li>
                        <li>Increase Social Media Followers.</li>
                        <li>Generate leads & Profit. </li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                     <img src="assets/images/f4.png" alt="Bio-Links" class="mx-auto img-fluid d-block">
                  </div>
               </div>
            </div>
         </div>
         <div class="container mt30 mt-md100">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="w700 f-24 f-md-36 black-clr lh150 text-center text-md-start">
                  Store, Backup, Share & Deliver All Marketing Files-
                  </div>
                  <div class="f-18 w400 lh150 mt10 text-center text-md-start black-clr">
                  Store & Share media at Lightning-Fast Speed
                  </div>
                  <ul class="f-18 f-md-20 w400 lh140 black-clr reason-list pl0 mt20">
                     <li>Store & Host Media & Files.</li>
                     <li>Folder & Segmentation. </li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f5.png" alt="Autoresponder" class="mx-auto img-fluid d-block">
               </div>
            </div>
         </div>
      </div>

      <!-- Feature Section Start -->
      <!-- <div class="feature-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
                  Have A Look at Some More <span class="white-clr feature-head">Cutting-Edge Features</span> 
of LinkPro...

                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0 mt-md50 justify-content-center">
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/website.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Create Unlimited Local Restaurant Websites
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/online-booking.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Online Seat Booking
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/qr-code.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        QR Code Generator
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/bio-link.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Bio Link Generator 
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/pop-up.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Pop-up & Sticky Bar to Promote offers 
                     </div>
                  </div>
               </div>

               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Send Emails to Clients
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        List Management & Tagging
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140">
                        List Segmentation & Suppression
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140">
                        Audience Management
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr10.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140">
                        Advance Analytics
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr11.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Advance Integration
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr12.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Built-in Business Drive to Store Manage & Share Files
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr13.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Create Unlimited Business/Domains
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr14.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Custom Domains
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr15.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Single Dashboard to Manage All Business & Clients
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr16.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        100% Cloud Based – No Hosting/Installation Required
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr17.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Easy & Intuitive to Use Software
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr18.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Complete Step-by-Step Training Included
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr19.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        No Coding, Design or Technical Skills Required
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr20.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-20 w500 lh140 mt15">
                        Regular Updates
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Feature Section End -->

      <!-- Watch Demo Sction Start -->
      <!-- <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center demo-text">
                  Check Out Some <br class="d-none d-md-block">
                  Beautiful Responsive Website Pages  
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md60">
                  <div class="logos-effect">
                     <img src="assets/images/templates.webp" alt="Demo Image" class="d-block img-fluid mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Watch Demo Sction End -->

      <!-- Unbeatable Value Section Start -->
      <!-- <div class="unbeatable-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-26 f-md-36 w700 text-center lh140">
                    <span class="orange-clr"> LinkPro  Has An UNBEATABLE Value </span>
                     For Any Online Business
                  </div>
                  <div class="f-24 f-md-32 w500 lh140 text-center mt10">
                     Why Should You Promote
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 order-2 order-md-1">
                  <img src="assets/images/unbeatable-value.webp" alt="Value" class="mx-auto img-fluid d-block">
               </div>
               <div class="col-md-6 order-1 order-md-2">
                  <ul class="f-18 f-md-20 w500 lh140 black-clr reason-list pl0 mt0">
                     <li>Top-Notch Website Creation Framework at low 1-time price.</li>
                     <li>Huge EPCs and Up to $47 Commissions/Sale.</li>
                     <li>Get 100% Commissions on Front End.</li>
                     <li>$12,000 Hard Cash in JV Prizes</li>
                     <li>We'll Reciprocate Crazily for our Top Affiliates.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Unbeatable Value Section End -->

      <!-- Exciting Launch Event Section Start -->
      <div class="exciting-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-42 w600 lh140 white-clr text-center">
                  7 Days Launch Event to Make You Juicy <br class="d-none d-md-block">Commissions
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-6 mt0 mt-md90">
                  <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md30 mt20 pl20 lh140 white-clr">
                     
                     <li>All Leads Are Hardcoded</li>
                     <li>We'll Re-Market Your Leads Heavily</li>
                     <li>Pitch Bundle Offer on webinars</li>                                                 
                     <li>High in Demand Product with Top Conversion</li>
                     <li>Deep Funnel to Make You Double Digit EPCs</li>
                     <li>Earn upto $447/Sale</li>
                  </ul>
               </div>
               <div class="col-md-6">
                  <img src="assets/images/launch.webp" alt="PreLaunch" class="mx-auto img-fluid d-block">
               </div>
            </div>
         </div>   
      </div>
      <!-- Exciting Launch Event Section End -->

      <!-- Deep-Funnel Section Start -->
      <div class="deep-sec" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center">
                     Our Deep & High Converting Sales Funnel
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Deep-Funnel Section End -->

      <!-- Prize Section Start -->
      <div class="prize-section">
         <div class="container">
            <div class="prize-inner-sec">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-65 w700 lh140 text-center black-clr text-uppercase launch-head">$10,000 Launch Contest</div>
                  </div>
               </div>
               <div class="row mt20 mt-md90">
                  <div class="col-md-6">
                     <img src="assets/images/prelaunch.webp" alt="PreLaunch" class="d-block img-fluid mx-auto">
                  </div>
                  <div class="col-md-6 mt20 mt-md0">
                     <img src="assets/images/exiciting-launch.webp" alt="Exiciting Launch" class="d-block img-fluid mx-auto">
                  </div>
               </div>
            </div>   
         </div>
         <img src="assets/images/businessman.webp" alt="Businessman" class="img-fluid d-none d-md-block ele4">
      </div>
      <!-- Prize Section End -->

      <!-- Reciprocate Section Start -->
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center">
                     Our Solid Track Record of <br class="d-none d-md-block">
                     <span class="orange-clr"> Launching Top Converting Products</span>
                  </div>
                  <img src="assets/images/product-logo.webp" class="img-fluid d-block mx-auto mt20">
                  <div class="text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 orange-clr">
                     Do We Reciprocate?
                  </div>
                  <div class="f-18 f-md-18 lh140 mt20 w500 text-center black-clr">
                     We've been in top positions on various launch leaderboards &amp; sent huge sales for our valued JVs.<br><br>  
                     So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are just some results from
                     our recent promotions. 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w700 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <!-- Reciprocate Section End -->

      <!-- Contact Section Start -->
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                             <span class="orange-clr">Have any Query?</span>   Contact us Anytime
                           </div>
                        </div>
                        <!-- <div class="row"> -->
                           <!-- <div class="col-12 col-md-12 mx-auto"> -->
                              <div class="row gx-md-5">
                                 <div class="col-md-4 col-12 text-center">
                                    <div class="contact-shape">
                                       <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Dr Amit Pareek
                                       </div>
                                       <div class="f-14 w400 lh140 text-center white-clr">
                                          (Techpreneur &amp; Marketer)
                                       </div>
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:amit.pareek77" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-4 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/tim-verdouw.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Tim Verdouw
                                       </div>
                                       <div class="f-14 w400 lh140 text-center white-clr">
                                          (Marketer)
                                       </div>
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href=" https://www.facebook.com/tim.verdouw" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="#" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-4 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Atul Pareek
                                       </div>
                                       <div class="f-14 w400 lh140 text-center white-clr">
                                          (Entrepreneur &amp; Product Creator)
                                       </div>
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           <!-- </div> -->
                        <!-- </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Contact Section End -->

      <div class="terms-section">
         <div class="container px-md-15">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center black-clr">
                  Affiliate Promotions Terms &amp; Conditions
               </div>
               <div class="col-md-12 col-12 f-18 f-md-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this 
                  launch and promote this product, you must abide by the instructions listed below:
               </div>
               <div class="col-md-12 col-12 px-md-0 px30 mt15 mt-md50">
                  <ul class="b-tick1 pl0 m0 f-md-16 f-16 lh140 w400">
                     <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned 
                        from all future promotion with us without any reason whatsoever. No
                        exceptions will be entertained. 
                     </li>
                     <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our 
                        knowledge that you have violated it, your affiliate account will be removed
                        from our system with immediate effect. 
                     </li>
                     <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through 
                        your affiliate link. </li>
                     <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or
                        brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any
                     manner. </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr">
                     LinkPro
                  </div>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © LinkPro 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right mt20 mt-md0">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-575032983').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-575032983")) {
                     document.getElementById("af-form-575032983").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-575032983")) {
                     document.getElementById("af-body-575032983").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-575032983")) {
                     document.getElementById("af-header-575032983").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-575032983")) {
                     document.getElementById("af-footer-575032983").className = "af-footer af-quirksMode";
                 }
             }
         })();
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
   </body>
</html>
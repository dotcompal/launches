<!DOCTYPE html>
<html>
   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LinkPro Special Bonuses">
      <meta name="description" content="LinkPro Special Bonuses">
      <meta name="keywords" content="LinkPro Special Bonuses">
      <meta property="og:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LinkPro Special Bonuses">
      <meta property="og:description" content="LinkPro Special Bonuses">
      <meta property="og:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LinkPro Special Bonuses">
      <meta property="twitter:description" content="LinkPro Special Bonuses">
      <meta property="twitter:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/legelsuites/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve" style="max-height:55px">
                                 <g>
                                     <g id="Layer_1-2">
                                         
                                             <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-1521.2245" y1="8551.4561" x2="-1622.4146" y2="8655.3457" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                                             c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                                         
                                             <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-1357.7941" y1="8721.4482" x2="-1265.4182" y2="8721.4482" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                                             c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                                             z"></path>
                                         
                                             <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-1621.1945" y1="8584.6367" x2="-1625.9146" y2="8690.4863" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                                             c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                                         
                                             <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-1329.8795" y1="8785.458" x2="-1427.5396" y2="8597.5684" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                                             c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                                             l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                                             C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                                         
                                             <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-1440.667" y1="8764.8115" x2="-1314.1017" y2="8764.8115" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                                             c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                                             C186.6,296.6,181.3,304,174.9,310.3z"></path>
                                         
                                             <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-1642.7045" y1="8691.5225" x2="-1462.8146" y2="8737.333" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                                             c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                                             c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                                             c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                                         <rect x="613.5" y="166.6" fill="#14142A" width="44.4" height="159.8"></rect>
                                         <path fill="#14142A" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                                             c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                                         <path fill="#14142A" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                                             c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                                             c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                                         <polygon fill="#14142A" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                                         <polygon fill="#14142A" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                                             920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                                         <path fill="#14142A" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                                             c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                                             c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                                             c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                                         <path fill="#14142A" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                                             c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                                             c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                                         <path fill="#14142A" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                                             c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                                             c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                                             C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                                             c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                                             c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                                     </g>
                                 </g>
                                 </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>
      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                  <?php
                     if(isset($_POST['submit'])) 
                     {
                        $name=$_REQUEST['name'];
                        $afflink=$_REQUEST['afflink'];
                        //$picname=$_REQUEST['pic'];
                        /*$tmpname=$_FILES['pic']['tmp_name'];
                        $type=$_FILES['pic']['type'];
                        $size=$_FILES['pic']['size']/1024;
                        $rand=rand(1111111,9999999999);*/
                        if($afflink=="")
                        {
                           echo 'Please fill the details.';
                        }
                        else
                        {
                           /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                           {
                              echo "Please upload image file (size must be less than 1 MB)";	
                           }
                           else
                           {*/
                              //$filename=$rand."-".$picname;
                              //move_uploaded_file($tmpname,"images/".$filename);
                              $url="https://www.linkpro.at/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                              echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                              //header("Location:$url");
                           //}	
                        }
                     }
                     ?>
                  <form class="row" action="" method="post" enctype="multipart/form-data">
                     <div class="col-12 form_area ">
                        <div class="col-12 clear">
                           <input type="text" name="name" placeholder="Your Name..." required>
                        </div>
                        <div class="col-12 mt20 clear">
                           <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                        </div>
                        <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                           <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © LinkPro 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   </body>
</html>
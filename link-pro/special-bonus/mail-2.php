<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="LinkPro Bonuses">
      <meta name="description" content="LinkPro Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LinkPro Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LinkPro Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.linkpro.at/special-bonus/thumbnail.png">
      <title>LinkPro Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
     
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
       <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'March 22 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/47069/393326/';
         $_GET['name'] = 'Pranshu Gupta';      
         }
         ?>
         
     <!-- Header Section Start -->   
     <div class="main-header">
         <div class="container">
            <div class="row">
            <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  
               </div>
               <div class="col-md-12 col-12 text-center mt30 mt-md40">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-gradient"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for LINKPRO
                        
                     </div>
                  </div>
               </div>
            
            
         
               <div class="row mt20">
               <div class="col-12  text-center">
                  <div class="f-20 f-md-22 w700 black-clr lh140 text-capitalize preheadline">                       
                  Grab LinkPro With Great Grand Bonuses Of The Year 2023, Before The Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80 relative">
                  <div class="gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="main-heading f-md-45 f-28 w700 text-center white-clr lh140">
                     <span class="orange-gradient w800"> Convert ANY Long &amp; Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, &amp; Easy-To-Share) in Just 30 Seconds…<br>
                     <span class="f-20 f-md-26 w500 lh130 white-clr1">Boost CTR, Commissions &amp; Sales. No Tech Hassles. No Monthly Fee Ever.</span>
                  </div>
               </div>
              
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Smart Link Builder – <span class="orange-gradient w700">Link Cloaker, Shortener, QR Code, Checkout Links &amp; Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-md-8 col-12 mx-auto">
               <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://linkpro.dotcompal.com/video/embed/j7hfbipy9d" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mx-auto">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                        <li><span class="orange-gradient w700">	Boost Your Click Through Rates</span> By Cloaking Long &amp; Suspicious Links</li>
                        <li><span class="orange-gradient w700">	Create QR Codes</span> For Touch-Less Payments &amp; Branding For Local Businesses</li>
                        <li><span class="orange-gradient w700">	Double Your Email Clicks &amp; Profits</span></li>
                        <li><span class="orange-gradient w700">	Boost Followership</span> Using Ready-To-Use Bio Pages.</li>
                        <li><span class="orange-gradient w700">	Fix All Broken Links on Any Website</span> In Just A Few Clicks </li>
                        <li><span class="orange-gradient w700">	Track Even A Single Click</span> So Never Lose Affiliate Commissions Again</li>
                        <li>	Sell High In-Demand Services With <span class="orange-gradient w700">Included Commercial License.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>

      <!-- Header Section End -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w800 lh140 text-center black-clr">
                     Create <span class="orange-gradient under1">Profit Pulling Smart Links, QR Codes &amp; Bio-Pages </span> <br class="d-none d-md-block"> <span class="steps-icon"> in 3 Easy Steps </span> 
                     <!-- <img src="assets/images/line.webp" alt="Line" class="mx-auto d-block img-fluid mt5"> -->
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-34 w800 black-clr lh150 mt20 text-center">
                        Login &amp; Enter Any Link
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt10 text-center">
                        To start, just login to your LinkPro account &amp; enter the link which you want to beautify.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-34 w800 black-clr lh150 mt20 text-center">
                        Generate
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt10 text-center">
                        Create an easy to share Smart Link/QR Code/Bio-Links without knowing a single line of code. Even use your own custom domain name to make it branded.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-34 w800 black-clr lh150 mt20 text-center">
                        Publish &amp; Get Paid
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt10 text-center">
                        Now publish your link that works 24*7 for you &amp; bring automated clicks, commissions &amp; sales.
                     </div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-20 f-md-24 w400 lh170 black-clr text-center">
                  Literally, It Just Takes <span class="w600">30 Seconds</span> 
               </div>
               <div class="col-12 f-24 f-md-34 lh140 w800 mt15 text-center orange-gradient">
                  No Technical &amp; Designing Skills of Any Kind is Needed!
               </div>
               <div class="col-12 f-md-24 f-20 w400 lh140 black-clr text-center mt15">
                  Plus, with included FREE commercial license,  <span class="w700">this is the easiest &amp; fastest way </span>  to start 6 figure business and help desperate local businesses with QR Codes, Bio-Links &amp; Smart links in no time!
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Step Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w500 text-center black-clr">
                     We Guarantee, This Is The <span class="w800">Only Smart Link Solution  <br class="d-none d-md-block"> You’ll Ever Need.</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">   
               <div class="col-12 col-md-6">
                  <ul class="f-18 f-md-20 w400 lh140 black-clr know-list pl0 mt0">
                     <li> 
                        Turn Any URL Into A  <span class="w800">Clean &amp; Professional Link With Zero Tech Hassles. </span>  
                     </li>
                     <li>
                        <span class="w800">	Shorten Any Ugly Affiliate Link </span> to Make It Easy to Understand &amp; Get More Clicks
                     </li> 
                     <li> 
                       <span class="w800">	Never Loose Traffic &amp; Sales Again - </span> Fix All Broken Links on Your &amp; Client Websites In Just A Few Clicks 
                     </li> 
                     <li> 
                        <span class="w800">	Precise Tracking &amp; Analytics To Measure Every Click -</span> Make The Right Decisions For Future Success!
                     </li>
                     <li> 
                        <span class="w800"> 	Easy To Share Links </span> On Social Media/Emails/Webinars/Website For Instant Results.
                     </li>
                     <li>
                        <span class="w800">	Smart Links Increases Click-Through Rate (CTR) Immediately </span> 
                     </li>
                     <li> 
                        <span class="w800">	Double Your Email Delivery &amp; Clicks –  </span> 2X Your Email Marketing Profits 
                     </li>
                     <li>
                        <span class="w800">	Grab More Leads &amp; Manage Them –  </span> Manage Your Prospects &amp; Customers with Inbuilt Lead Management
                     </li>
                     <li>
                        <span class="w800">	Cutting-Edge Autoresponder Integration-   </span>Use Any Email Autoresponder Without Any Technical Glitches
                     </li>
                     <li>	
                        User Friendly Business Central Dashboard- <span class="w800">	Everything You Need, Under One Roof At Your Fingertips! </span>
                     </li>
                 </ul>
               </div>
               <div class="col-12 col-md-6">
                  <ul class="f-18 f-md-20 w400 lh140 black-clr know-list pl0 mt0">
                     <li>  
                        <span class="w800"> 	Create Beautiful QR Codes Easily </span> For Local Businesses to Help Them Go Digital
                     </li>
                     <li> 
                        <span class="w800">	Setup Touch-Less Payments &amp; Social Profiles </span> For Small Businesses in Just Minutes
                     </li> 
                     <li> 
                        <span class="w800">	Create Bio-Pages for Yourself/Clients - </span> Boost Social Branding, Followership &amp; Traffic 
                     </li> 
                     <li> 
                        <span class="w800">	Ready-To-Go &amp; Beautiful Bio-Pages Templates - </span> Just Put Your Business Details &amp; Get Bio Links to Share
                     </li>
                     <li> 
                        <span class="w800">	Next-Gen Drag And Drop Editor </span> To Create Pixel Perfect Pages Or Templates From Scratch
                     </li>
                     <li> 
                          <span class="w800">Easy To No-Follow Affiliate Links </span> For Better SEO &amp; More Search Traffic. 
                     </li>
                     <li>
                        <span class="w800">	Mobile Friendly &amp; Fast Loading Bio-Pages- </span> Never Lose A Single Visitor, Lead Or Sale.
                     </li>
                     <li>
                        <span class="w800">	Automatic SSL Encryption -  </span> 100% Unbreakable Security
                     </li>
                     <li>
                        <span class="w800">	No Expensive Domains &amp; Hosting Services -   </span> Host Everything On Our Lightning Fast Server.
                     </li>
                     <li>
                        <span class="w800">		PLUS, YOU’LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY!   </span> 
                     </li>
                 </ul>
                  </div>
               </div>
            </div>
         </div>
      <!-- Step Section End -->
     
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-28 f-md-45 w400 lh140 text-capitalize text-center black-clr col-12">
                  LinkPro Is So Powerful That We Are Personally Using It  <br class="d-none d-md-block">
                  <span class="f-28 f-md-45 w800 orange-gradient">To Run Our 6-Figure Online Business Without A Hitch! </span>
               </div>
               <div class="col-12 f-md-24 f-18 w400 lh140 black-clr text-center mt15">
                  And as a matter of fact, every single Link and Bio Pages We use in our Emails, Webinars, Product Review websites, Pages &amp; YouTube Videos is powered by LinkPro. 
               </div>
               <!-- <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div> -->
             
               <div class="col-12 f-md-28 f-22 w800 lh140 black-clr text-center mt20 mt-md60 demo-text">
                  Using LinkPro For Our Product Launches and In&nbsp;Affiliate Promos we have Generated $700K in Last 6 Months, Which is 3X High From the Past 6 Months’ Assessment.     
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md60">
               <div class="col-12 col-md-6">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md-60">
               <div class="col-12">
                  <div class="f-22 f-md-28 w400 lh140 black-clr text-center">
                     We Guarantee, this is your last Link Solution to Save your commissions &amp; boost Sales.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="future-section">
         <div class="container">
            <div class="row">
                  <div class="col-12 f-28 f-md-45 w800 lh130 black-clr text-center thunder">
                     With LinkPro, It Just Got Powerful…
                     <img src="assets/images/double-blue-line.webp" alt="line" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 f-18 f-md-24 w500 lh140 black-clr text-center mt20 mt-md30">
                     It took 2 years to plan, develop, and super-optimize using the world's best cloud services for delivering blazing fast Links, Bio-pages and QR codes to enhance your visitor’s experience.
                     <br><br>
                     LinkPro makes link cloaking a piece of cake, but it is not limited to just link cloaking. <u>It turns your links into smart links.</u>
                  </div>
                  <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/future-img.webp" alt="" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-md-12 mt-md80 mt20">
                     <img src="assets/images/yes-you-can.webp" alt="" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-28 w500 lh150 black-clr text-center mt20 mt-md50">
                     The Best Thing is You Just Need to Enter your URL and LinkPro Will Create a Beautiful, Trackable &amp; Easy-To-Share <u>Smart Link, Bio-Page &amp; QR Code for You in Just 30 Seconds.  </u>
                  </div>
            </div>
         </div>
      </section>

    <!-- CTA Btn Start-->
    <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->



      
      <section class="imagine-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w800 black-clr lh140">
                     LinkPro Is the Must Have Solution for Marketers &amp; Entrepreneurs of All Kinds. 
                  </div>
               </div>
            </div>
            
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1 justify-content-center mt0 m-md30">
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k1.webp" alt="Create Businesses/ Sub Domains" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">
                        Affiliate Marketers
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k2.webp" alt="Unlimited Visitors" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">
                        Webinar Marketers
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k3.webp" alt="Accept Payments" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Email-Marketers</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k4.webp" alt="Generate &amp; Manageup" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Social Media Marketers</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k5.webp" alt="YouTubers" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">YouTubers</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k6.webp" alt="Product Sellers" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Product Sellers</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k7.webp" alt="Course Sellers" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Course Sellers</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k8.webp" alt="Coaches" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Coaches</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k9.webp" alt="Local Business
                     Owners" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="white-clr lh140 f-18 f-md-24 w600">Local Business Owners</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <div class="white-clr lh140 f-18 f-md-24 w600">And many more...</div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <div class="presenting-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="presenting-head f-28 f-md-50 w700 white-clr caveat">
                  Proudly Presenting… 
                  </div>
                  <div class="col-12 mt20 mt-md80 relative">
                  <div class="gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="main-heading f-md-45 f-28 w700 text-center white-clr lh140">
                     <span class="orange-gradient w800"> Convert ANY Long &amp; Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, &amp; Easy-To-Share) in Just 30 Seconds…<br>
                     <span class="f-16 f-md-26 w500 white-clr1">Boost CTR, Commissions &amp; Sales. No Tech Hassles. No Monthly Fee Ever.</span>
                  </div>
               </div>
              
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Smart Link Builder – <span class="orange-gradient w700">Link Cloaker, Shortener, QR Code, Checkout Links &amp; Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md40">
                     <img src="assets/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
         </div>
      </div>


           <!-------WriterArc Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : WriterArc
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="writer-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap flex-md-nowrap">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-20 f-md-22 w500 blue-clr1 lh140">
                     Never Waste Your Time &amp; Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="gradient-orange"> Any Local or Online Niche 10X Faster…Just by Using a Keyword</span>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back &amp; Relax, WriterArc Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/spru84q6w8" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w500 white-clr">
                     <li><span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                     <li><span class="orange-clr1"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                     <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                     <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                     <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                     <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/writerarc-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/writerarc-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/writerarc-steps.webp" alt="WriterArc Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- WriterArc End -->

      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : WebPrimo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="WebPrimo-header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto"></div>
               <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                  <div>
                     The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                  </div>
                  <div class="mt5">
                     <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-md-45 f-28 w600 text-center white-clr lh150">
                     First Ever on JVZoo…  <br class="d-none d-md-block">
                     <span class="w700 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites &amp; Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
                  Easily create &amp; sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches &amp; 100+ other niches...
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                        <div class="col-12 responsive-video">
                           <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="WebPrimo-key-features-bg">
                           <ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
                              <li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
                              <li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
                              <li>Create Elegant, Fast loading &amp; SEO Friendly Website within 7 Minutes</li>
                              <li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
                              <li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
                              <li>Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="WebPrimo-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row webprimo-header-list-block">
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li><span class="w600">Create Ultra-Fast Loading &amp; Beautiful Websites</span></li>
                              <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w600">For Any Business Need.</span>
                              </li>
                              <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
                              </li>
                              <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
                              <li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
                              <li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
                              <li><span class="w600">Analytics &amp; Remarketing Ready</span> Websites</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                              <li>Customize and Update Themes on Live Website Easily</li>
                              <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                              <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                              <li><span class="w600">Accept Payments</span> For Your Services &amp; Products With Seamless WooCommerce Integration</li>
                              <li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12  header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!--WebPrimo ends-->
      <!-- Vocalic Section ENds -->

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE AGENCY LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Trendio End -->
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/trendio.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/trendio-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/trendio.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!--Trendio ends-->

      <!-------Coursova Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Coursova
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="coursova-bg">
         <div class="container">
            <div class="row inner-content">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                  </div>
               </div>
               <div class="col-12 text-center px-sm15">
                  <div class="mt20 mt-md50 mb20 mb-md50">
                     <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                        Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-md-8 col-sm-3 ml-md70 mt20">
                     <div class="game-changer col-md-12">
                        <div class="f-md-24 f-20 w600 text-center  black-clr lh120">Game-Changer</div>
                     </div>
                  </div>
                  <div class="col-12 heading-co-bg">
                     <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
                        <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                     </div>
                  </div>
                  <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
                     Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>
                  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15">
                  <div class="col-12 p0 mt-md15">
                     <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15">
                  <ul class="list-head list-head-co pl0 m0">
                     <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                     <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                     <li>Easily Create Courses - Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                     <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                     <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                     <li>Get Agency License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <img src="assets/images/coursova-gr1.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/coursova-gr2.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/coursova-gr3.png" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!------Coursova Section ends------>
      
   
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-11 mx-auto main-heading text-center">
                  <div class="f-24 f-md-45 lh140 w700 white-clr"> When You Purchase LinkPro, You Also Get  Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      
    <!-- Bonus #1 Start -->
    <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Cross Link Randomizer
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Here's A Quick And Easy Way To Boost Your Income By Making Your Site More Sticky And Providing Easier Access To All Your Website Content!</b></li> 
                           <li>You may have a page in your website that ranks very well in Google but if this website don't convert well, having a cross selling via sharing relevant offers and links would be a good strategy to convert your traffic into buyers.</li>
                           <li>Sounds critical to think? Well, not anymore, as inside this product, you are about immediately receive and amazing software that does all that. Plus, this cross-linking feature can also be a huge help in boosting your search engine traffic as this will your site do on-page optimization. Cross Link Randomizer offers an easy automatic way to do it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 End -->
      <!-- Bonus #2 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing A-Z 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>The Affiliate Marketing A-Z report is a very enticing lead magnet, especially for newbies. </b>It explains the process of affiliate marketing and how to get started in the best possible way. Readers will learn what they must do to get started and what they must avoid. There is an A-Z of the most commonly used affiliate marketing terms that are essential to know. </li>

                           <li>This powerful report is ideal for those new to affiliate marketing. The introduction explains the benefits of getting started with affiliate marketing and provides some different ways for affiliates to make money. Your readers will understand that they do not require any special skills or experience to be an affiliate marketer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 End -->
      <!-- Bonus #3 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Turbo Dynamic URL 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Introducing the Most Effective, Fast and Empowering Affiliate Bonus Link Generation Software on the Market, Allowing You to Boost Your Affiliates’ Efforts On Steroid!</b></li>
                           <li>Why most affiliate marketers don't make much money from their offers, because majority of your affiliates are new to the industry.</li>
                           <li>What this means is that, they may think that they can't do what other successful affiliate marketers can do especially in creating a good-looking affiliate pages properly-designed for conversion.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Backlinks Analyzer 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Discover A Powerful Software That You Can Use Right Now To Instantly Analyze The Quality Of All Your Backlinks... With The Click Of A Mouse!</li> 
                           <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that 
                              your business will work perfectly!</li> 
                           <li>The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With 
                              Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More
                               Satisfied Customers, Affiliates, and Partners! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Tactical Backlinks Method 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Learn the technical side of SEO Link Building!</li>
                           <li>Backlinks come in various forms (some good, some bad), but the most coveted of all are 'authority backlinks'...</li>
                           <li>These are the ones that have the most impact on your SEO success and ultimately your income. But how do you get these elusive backlinks? That's one of the biggest questions that plagues the minds of Internet marketers looking to increase their organic rankings and traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 start -->
   <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Amazon Affiliate Blueprint 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> How to Build a Profitable Business as an Amazon Associate!</b></li>   
                           <li>Amazon, the world’s largest e-commerce site has a well-established affiliate marketing program called Amazon Associates that allows you to earn up to 12 per cent of the total sales value of a transaction originating from your referral website. </li>
                           <li>That’s a pretty decent figure you earn for a successful referral. E-commerce companies nowadays are trying to outdo each other in terms of sales and they are investing on every marketing channel available. Affiliate marketing is where they are interested mainly for the returns it can guarantee. </li>
                           </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Traffic Beast 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Whether you have a personal blog, business website, or are making money through online advertising, today's currency of success relies, almost exclusively, on the science of cultivating more significant traffic to your website.</b></li>
                           <li>The traffic that you bring to your website is crucial because it helps you increase your rankings on the various search engines, which is how potential customers can find your company.</li>
                           <li>The five powerful techniques outlined in this guide are geared toward a single purpose; helping you drive more traffic to your website. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Finding Back Links 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Learn From This Audio, Give It Away To Build Your Email List & Sell The Whole Product With MRR</b></li>
                           <li>If you are relatively new to the Internet community, it may seem a daunting task to get free, natural, backlinks to your site. You may understand now how important natural backlinks are towards establishing a solid reputation with search engines, but you are caught in the dilemma of the first-time job seeker. </li>
                           <li>You don’t have the qualifications to attract someone who is interested in you, but the only way to gain those qualifications is to have the working reputation (backlinks) already in place.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Backlink Ideas 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Learn From This Audio, Give It Away To Build Your Email List & Sell The Whole Product With MRR </b> </li>
                          <li>It seems like every day someone on the Internet discovers and ingenious way to get a one-way backlink. </li>
                          <li>  Sometimes people luck into them by buying advertising in a paper publication or directory that later gets posted on the Internet. If they happen to land on a .edu site this way, they can end up with great backlink ratings and not even know how they managed to get it. So, always keep an eye out for additional strategies. </li>
                          <li>  Here we offer you a sampling of some innovative ways that other people have used and whether they will help or hurt you in the long run.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate List Pro 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> With this software you can easily build your own list through your affiliate links - all on autopilot. </b></li> 
                          <li>Adding All Sorts Of Popups To Other People's Sales Pages</li> 
                          <li>Creating Your Affiliate Link Popups</li> 
                          <li>Controling All Aspects Of Your Popups</li>
                          <li>Boosting Your Response By Personalizing Your Web Pages</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
  
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Keyword To Link Plugin 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Save HOURS, reduce bounce rate and increase your website's ranking by automatically adding links to any keywords in seconds: watch your search engine traffic and conversions go through the roof!</b></li>   
                           <li>One of the factors that your website will rank to Google and other big search engine is your bounce rate. The lower your bounce rate is the better factor your website would likely rank your site higher.</li>
                           <li>The good news is that inside this product is software that will help you reduce your bounce rate by simply giving your audience a relevant information and links to your keyword. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Covert Affiliate Link Masker 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>At Last! Covertly Mask 'UGLY' Affiliate Links & Increase Your Commissions! </b></li>
                           <li>Masking your affiliate link is one of the best technique to promote the products that you promote. This is because this masking feature will make your affiliate link more readable and easy to remember.</li>
                           <li>Well, there are many free services out there that can do this feature but the truth is that, it's beyond your control sometimes because you don't own the service. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Ultimate Link Building 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Outrank Your Competitors And Make More Sales Using This Ultimate Guide To Building Links For Top Search Engine Rankings!</b></li>
                           <li>After years of programming development, the search engines have mastered and refined the use of link data, where complex algorithms are put in place to create nuance evaluation of websites based on this information.</li>
                           <li>By using links, the engines can determine a site’s popularity in a specific niche based on the number of pages linked to it and then this information is combined with other metrics like trust and authority, the search engines assign a rank for the website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Cash Link Buddy 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Don't Even THINK About Inserting Even a Sliver of Content on Your Wordpress Site Again, Without First Off Using This One of a KIND WP Plugin! </b></li>
                          <li>Think of WP-Cash Link Buddy as YOUR Personal Money Making Assistant! Use PROVEN To WORK techniques to Send Your Conversions Straight UP & OFF the Charts!</li>
                          <li>This is achieved by the Unique Ability of WP Cash Link to Allow the Casual WordPress User the Ability to Easily Apply a Powerful Interstitial Advertising System with the Click of the Mouse.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Miner
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Get Paid For Your Affiliate Efforts, Even if Your Visitors Don't Buy Through Your Link! </b></li> 
                          <li>1. The plugin works with affiliate networks that support iframing (like JVZoo, ClickBank etc - at least at the moment of writing this). 2. You can add as many affiliate links as you need per page / post (we recommend no more than 3 per page / post, to speed up the load of your page). 3. Avoid adding links of sites that autoplay videos / audios, or your visitors will also hear them.</li> 
                          <li>The plugin allows you to set a cookie for your affiliate link/s when your visitors visit your wordpress pages / posts, so if they don’t click your links, you still get paid in case they buy the product/s later.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
  <!-- Bonus #16 start -->
  <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Link Click Counter 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Here's A Quick And Easy, Low Cost Way To Track The EXACT Response To Any Ad, Anywhere!</b></li>   
                           <li>If you're sending emails to your list, you should know how many people click on links in the email, so you can determine whether the ad was of interest to your subscribers - and whether your email copy was up to the job.</li>
                           <li>For ads placed on your web site, you need to see how many visitors are clicking on the ads, to determine whether they could be improved.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Backlinks Warrior
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Discover An Easy To Use Software Tool That Finds High PageRank Relevant Backlinks, With Just A Few Clicks Of Your Mouse!</b></li>
                           <li>If you are a website owner, making your business website ranks higher depends on the quality of your backlinks.</li>
                           <li>You see, you can't deny the fact that backlinks are still one of the huge factors to rank a website and finding the most relevant and quality links is time-consuming.</li>
                           <li>The good news is that inside this product is a powerful software that will help you find those quality backlink opportunities.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Link Weeder 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Here's A Really Easy Way To Find The Most Profitable Affiliate Program To Promote For Any Niche!</b></li>
                           <li>No matter what niche you are promoting, there are always many different affiliate programs that you can promote to earn commissions.</li>
                           <li>But not all affiliate programs are equal. Some will make you a lot of money - while others are just a complete waste of time and effort. Just enter all the different affiliate links into Affiliate Link Weeder and the software will automatically split your traffic equally between the links. Just wait a short while to see which affiliate programs pay you the most money - then dump the losers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Easy Protected Links 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Finally! A Fast, Easy Solution to Creating Protected Download Links for Your Valuable Content! Stop giving away your work for free and discover how you can easily protect your pdfs, mp3s and more!</b></li>
                          <li>How many times have you found your hard work freely available all over the Internet in the form of downloadable pdfs, mp3s, videos, and more? If you've been creating products for any length of time at all, chances are it's happened more than once. </li>
                          <li>Sure, you can install a shopping cart or membership plugin to control downloads, but the cost and maintenance is a headache, and for low cost products it may not be worth it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Rockstar 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Create Affiliate Campaigns Like a Pro With Just a Couple of Clicks! </b></li> 
                          <li>If you're doing direct linking (Linking Directly to An Affiliate Offer), you may find that you conversions may be not so good as your not pre-selling the product.</li> 
                          <li>If you look the campaigns of many successful affiliate marketers they have one thing in common. Pre-Selling is the key to better conversions. The problem is that many affiliate don't know how to easily implement this amazing technique to their affiliate websites.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w800 orange-gradient">"PRANSHUVIP"</span> for an Additional <span class="w800 orange-gradient">$10 Discount</span> on Agency Licence
                  </div>
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn mt15 mt-md20">
                  <span class="text-center">Grab LinkPro + My 20 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w800 orange-gradient">"PRANSHULINK"</span> for an Additional <span class="w800 orange-gradient">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz3.com/c/47069/393330/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LinkPro Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © LinkPro 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://linkpro.at/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
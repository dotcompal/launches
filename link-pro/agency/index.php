<!Doctype html>
<html>
   <head>
      <title>LinkPro Agency</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LinkPro Agency">
      <meta name="description" content="Setup Your Own White-Labelled Marketing Agency Instantly and Make Huge Profit Per Client">
      <meta name="keywords" content="">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.linkpro.at/agency/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LinkPro Agency">
      <meta property="og:description" content="Setup Your Own White-Labelled Marketing Agency Instantly and Make Huge Profit Per Client">
      <meta property="og:image" content="https://www.linkpro.at/agency/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LinkPro Agency">
      <meta property="twitter:description" content="Setup Your Own White-Labelled Marketing Agency Instantly and Make Huge Profit Per Client">
      <meta property="twitter:image" content="https://www.linkpro.at/agency/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <!-- End -->
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'March 20 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div>
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
                        <g>
                           <g id="Layer_1-2">
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                 <stop offset="1" style="stop-color:#006DE3"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                              c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#EFA222"></stop>
                                 <stop offset="1" style="stop-color:#F0592D"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                              c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                              z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                 <stop offset="1" style="stop-color:#006DE3"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                              c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#EFA222"></stop>
                                 <stop offset="1" style="stop-color:#F0592D"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                              c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                              l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                              C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#EFA222"></stop>
                                 <stop offset="1" style="stop-color:#F0592D"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                              c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                              C186.6,296.6,181.3,304,174.9,310.3z"></path>
                              <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                                 <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                 <stop offset="1" style="stop-color:#006DE3"></stop>
                              </linearGradient>
                              <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                              c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                              c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                              c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                              <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                              <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                              c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                              <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                              c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                              c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                              <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                              <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                              920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                              <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                              c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                              c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                              c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                              <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                              c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                              c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                              <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                              c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                              c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                              C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                              c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                              c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                           </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-24 w700 black-clr lh140 text-uppercase preheadline">
                     BEFORE YOU ACCESS YOUR PURCHASE OF LINKPRO, I HAVE A QUESTION FOR YOU!
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 f-md-45 f-28 w800 text-center white-clr lh140 main-heading"> 
                  How Would You Like to Setup Your Own White-Labelled Marketing Agency Instantly and <span class="orange-gradient w800">Make Huge Profit Per Client</span>  Without Any Extra Effort?   
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-24 w500 text-center lh140 white-clr text-capitalize">               
                  We Did All The Hard Work For You!! You Just Have To Start Serving High In Demand <br class="d-none d-md-block">
Marketing Services To Hungry Clients & Keep 100% Profits
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/productbox.webp" class="d-block img-fluid mx-auto">
                  <!-- <div class="responsive-video ">
                     <iframe src="https://linkpro.dotcompal.com/video/embed/g1grbzwfvc" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div> -->
                  <!-- <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://linkpro.dotcompal.com/video/embed/g1grbzwfvc" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
                  </div> -->
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 f-20 f-md-24 w500 lh140 text-center white-clr">
                  This is LinkPro Customer’s Only One Time Exclusive Deal, Available for Limited Time!
               </div>
               <div class="col-md-10 mx-auto col-12 mt20 mt-md30">
                  <div class="f-18 f-md-36 text-center probtn1">
                     <a href="#buynow" class="text-center">Upgrade to LinkPro Agency Now</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="assets/images/payment-options.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12">
                        <div class="f-md-45 f-28 w800 lh140 text-capitalize text-center black-clr">
                        The Secret Weapon To 10X Your Profits <br class="d-none d-md-block">
                        From Your LinkPro Subscription
                        </div>
                     </div>
                     <div class="col-12 text-center">
                        <div class="f-18 f-md-20 text-center w400 lh140 text-left mt15 mt-md40">
                           This Agency upgrade puts you on another level with a click of a button and enables you to <b>tap into the $413 Billion Marketing Industry.</b>
                           <br><br>
                           The agency upgrade has everything Super Done for you Product, Bio-Pages & use cases in tons of niches, flexibility to define your own Bio-Pages, Product Pages, a complete business/team management panel to manage your client’s biz, white label license to LinkPro, and an agency license to serve unlimited clients from a single dashboard.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt30 mt-md70">
                        <div class="row">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe1.webp" class="img-fluid mx-auto d-block" alt="Sell-Image">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w600">Unlimited Team Members </span>  
                              </div>
                           </div>
                        </div>
                        <div class="row mt20">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe2.webp" class="img-fluid mx-auto d-block" alt="Sell-Image">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                              <span class="w600">  License to serve to Unlimited clients</span>  
                              </div>
                           </div>
                        </div>
                        <!---3-->
                        <div class="row mt20 mt-md20 align-items-center">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe3.webp" class="img-fluid mx-auto d-block" alt="Serve Unlimited Client">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                              <span class="w600">Sell LinkPro As Your Own And Keep 100% Profits in Your Pocket</span>  
                              <span class="w600">White Label Branding </span>  
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20 align-items-center">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe4.webp" class="img-fluid mx-auto d-block" alt="Serve Unlimited Client">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                              <span class="w600">Agency Panel to manage Clients</span>  
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20 align-items-center">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block" alt="Serve Unlimited Client">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                              <span class="w600">Work Collaboratively & Share Proven</span> Smart Links & Bio -Templates   
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe6.webp" class="img-fluid mx-auto d-block" alt="Bussiness Mangement"></div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                              <span class="w600">Create Your Own QR Codes & Page Templates  </span>  and Sell them your Clients to Make More Profits 
                              </div>
                           </div>
                        </div>
                        <!--8-->
                        <!--11-->
                       
                     </div>
                     <div class="col-md-6 col-12 mt30 mt-md70">
                     <div class="row mt20 mt-md0">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe7.webp" class="img-fluid mx-auto d-block" alt="Benefits"></div>
                           </div>
                           <div class="col-md-10  col-12 mt20 mt-md15">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w600">Accurate Analysis for Team Member’s Activities </span>  For Effective Monitoring                                
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe8.webp" class="img-fluid mx-auto d-block" alt="Benefits"></div>
                           </div>
                           <div class="col-md-10  col-12 mt20 mt-md15">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w600">Support to you & your clients - </span>  chat support directly from software                                
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe9.webp" class="img-fluid mx-auto d-block" alt="Benefits"></div>
                           </div>
                           <div class="col-md-10  col-12 mt20 mt-md15">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w600">Subscription Management System </span>  to Manage Your Clients Plans
                              </div>
                           </div>
                        </div>
                        <div class="row mt20 mt-md20">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe10.webp" class="img-fluid mx-auto d-block" alt="Benefits"></div>
                           </div>
                           <div class="col-md-10  col-12 mt20 mt-md15">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w600">Get All These Benefits For One Time Price</span>                                  
                              </div>
                           </div>
                        </div>
                       
                     </div>
                     <div class="col-12">
                        <div class="row mt20 mt-md70">
                           <div class="col-12 f-20 f-md-24 w500 black-clr text-center">
                              This is LinkPro Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                           </div>
                           <div class="col-md-10 mx-auto col-12 mt20 mt-md30">
                              <div class="f-18 f-md-36 text-center probtn1">
                                 <a href="#buynow" class="text-center">Upgrade to LinkPro Agency Now</a>
                              </div>
                           </div>
                           <div class="col-12 mt20 mt-md30">
                              <img src="assets/images/payment-options1.webp" class="img-fluid mx-auto d-block">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--2. Second Section End -->

      <div class="presenting-sec">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="presenting-head f-28 f-md-50 w700 white-clr caveat">
                     Introducing… 
                     </div>
                     <div class="mt-md50 mt20 f-md-45 f-28 w800 text-center white-clr lh140"> 
                        Setup Your Own Marketing Agency To Serve Hungry Clients and Keep 100% of Profits
                     </div>
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md80">
                     <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block " alt="ProductBox-Img">
                  </div>
               </div>
               <div class="row mt20 mt-md50">
               <div class="col-12  f-20 f-md-24 w500 lh140  white-clr text-center">
                  This is LinkPro Customer’s Only One Time Exclusive Deal, Available for Limited Time!
               </div>
               <div class="col-md-10 mx-auto col-12 mt20 mt-md30">
                  <div class="f-18 f-md-36 text-center probtn1">
                     <a href="#buynow" class="text-center">Upgrade to LinkPro Agency Now</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="assets/images/payment-options.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            </div>
      </div>

      
      <div class="thats-not-all-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w800 lh140 text-center black-clr">
               With LinkPro Agency Upgrade, You Can Tap into The Very Fast-Growing Marketing Industry </span>
               </div>
               <div class="col-12 col-md-12  mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 f-md-20 lh140 w400 black-clr text-xs-center lh140 mt20 mt-md0">
                        Also, you can <span class="w600"> start your own highly profitable QR Code, Bio-Page designing services</span> or tap into the $413 billion marketing industry.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="assets/images/freelancing-img.webp" class="img-fluid mx-auto d-block" alt="freelancing-img">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-md-45 f-28 w800 lh140 text-center black-clr mt20 mt-md50 text-capitalize">
                  See How Much Freelancers Are Charging For these Services
                  </div>
                  <div class="mt10 mt-md15">
                     <img src="assets/images/acquiring.webp" class="img-fluid mx-auto d-block" alt="Sales-Funnel">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-xs-center mt15">
                     With a LinkPro Agency License not only you can serve clients in your local area and Provide a Complete Marketing Service to Local Clients with proven Bio Page templates, but also you can find them on websites like Fiverr, Freelancer, Odesk, and Upwork where tons of Marketers & businesses are looking for a legitimate Services.
                  </div>
               </div>
               <div class="col-12 text-center mt30 mt-md80">
                  <div class="one-time-shape" editabletype="shape" style="z-index: 8;">
                     <div class="f-24 f-md-36 w700 text-center lh120 white-clr text-center">And it’s not a 1-Time Income</div>
                  </div>
               </div>
               <div class="col-12  f-30 f-md-50 w800 lh120 black-clr text-center mt30 mt-md5">Over 500K New Social Media Influencer <br> Start Every Single Month 
               </div>
               <div class="col-12 f-18 f-md-20 lh140 w400 text-center black-clr mt30 mt-md30">
                 <b>You can do this for life!</b> Add new clients every month, plus retain existing clients for recurring income again, and again forever. This will keep increasing your profit exponentially month by month.
                  <br><br>
                  Yes, Tons of Influencer & Marketers Are Looking for A Solution and there are <b>Absolutely No Limitations!</b> 
               </div>
            </div>
         </div>
      </div>
      <div class="amazing-software-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-42 f-28 w800 lh140 text-capitalize text-center">
                  Nothing To Research, Create Or Manage Support... Setup A Complete Done For You System In 3 Simple Steps
                  </div>
               </div>
               <!-- feature 1 -->
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-1">
                        <div class="f-22 f-md-32 w800 lh140 black-clr text-capitalize">
                        Add A New Client In 1 Minute
                        </div>
                        <div class="f-18 f-md-20 lh140 w400 mt10">
                        All you need to do is add a new client’s details, select theme, and choose templates & you're all set
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                        <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Step1">
                     </div>
                  </div>
               </div>
               <!-- feature 2 -->
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140 black-clr text-capitalize">
                        Accept The Monthly Payments & Keep 100% Profits
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10">
                        Charge them monthly, yearly, or one-time high fees for providing them with high-in-demand Website Development service & Keep 100% profits with you.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Step2">
                     </div>
                  </div>
               </div>
               <!-- feature 3 -->
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-1">
                        <div class="f-22 f-md-32 w800 lh140 black-clr text-capitalize">
                        We Did All The Hard Work.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10">
                        We invested a lot of money to get this disruptive Done-for-you technology ready. And you know what the best part is, we will take care of all the customer support about you and your client’s queries. You don’t even need to lift a finger.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                        <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Step3">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- feature1 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="f-md-50 f-28 w800 lh140 text-center black-clr mb-md0 mb20">
                   Here’s What You are Getting with
                  <br class="d-md-block d-none"><span class="orange-gradient">
                  This Agency Upgrade Today</span>
               </div>
            </div>
            <div class="row mt30 mt-md50">
               <div class="col-12 ">
                  <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize features-title">
                     <img src="../common_assets/images/1.webp" class="img-fluid size">
                     A Top-Notch Software, LinkPro With Agency License To Provide High In Demand QR Code & Bio-Page Development Services To Your Clients.
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/feature1.webp" class="img-fluid d-block mx-auto" alt="Feature First">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w500 lh140 mt20 mt-md30 text-left">
                  Providing high-in-demand QR Code & Bio-Page Development services couldn’t get easier than this. You’re getting the never offered before LinkPro Agency License to provide these services to your clients. This is something that will get you way above your competitors.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature1 -->
      <!--feature2 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize features-title">
                     <img src="../common_assets/images/2.webp" class="img-fluid size">
                     Done-For-Your Team Management Panel - Add Unlimited Team And Clients
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/feature2.webp" class="img-fluid d-block mx-auto" alt="Feature Second">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w500 lh140 mt20 mt-md30 text-left">
                  Oh yeah! You can manage all your clients & team members from a single dashboard to have full control. You can assign them limited or full access to features according to their role with separate login details to outsource your manual work.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature3 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize features-title">
                     <img src="../common_assets/images/3.webp" class="img-fluid size">
                     Serve Unlimited Clients with Agency License
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/feature3.webp" class="img-fluid d-block mx-auto" alt="Feature Third">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w500 lh140 mt20 mt-md30 text-left">
                     LinkPro Agency gives you the complete power to go beyond the normal and have a passive income source by serving unlimited clients in hands-down manner 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature4 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize features-title">
                     <img src="../common_assets/images/4.webp" class="img-fluid size">
                     White Label License 
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/feature4.webp" class="img-fluid d-block mx-auto" alt="Feature Fourth">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w500 lh140 mt20 mt-md30 text-left">
                     You will also get White Label License along with Agency License to put your own logo and name and present LinkPro as your own tool to your clients. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w700 lh140 mt20 mt-md0 black-clr text-capitalize features-title">
                           <img src="../common_assets/images/5.webp" class="img-fluid size">
                           Get All These Benefits for One Time Price
                        </div>
                        <div class="f-18 f-md-20 w500 lh140 mt20">
                        And here's the best part. When you get access to LinkPro Agency, you can get all these benefits by paying just a small one time price. Now that's something you can't afford to miss out on
                        </div>
                     </div>
                     <div class="col-12 col-md-6  mt20 mt-md0">
                        <img src="assets/images/feature5.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back Start -->
      <div class="moneyback-section">
         <div class="container">
            <div class="row">
               <div class="col-12 mb-md30">
                  <div class="f-md-45 f-28 lh140 w800 text-center orange-gradient">
                     30 Days Money Back Guarantee
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap">
               <div class="col-md-4 col-12 mt30 mt-md40">
                  <img src="assets/images/moneyback.webp" class="img-fluid mx-auto d-block" alt="MoneyBack">
               </div>
               <div class="col-md-8 col-12 mt30">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     We have absolutely no doubt that you'll love the extra benefits, training and LinkPro Agency upgraded features.
                     <br>
                     <br> You can try it out without any risk. If, for any reason, you’re not satisfied with your LinkPro Agency upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                     <br>
                     <br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back End-->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 white-clr feature-shape">
                     But That’s Not All
                  </div>
               </div>
               <div class="col-12 mt40">
                  <div class="f-20 f-md-22 w400 text-center lh140">
                     In addition, we have a number of bonuses for those who want to take action <br class="d-none d-md-block"> today and start profiting from this opportunity
                  </div>
               </div>
               <!-- bonus1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <img src="assets/images/bonus1.webp" alt="Bonus 1" class="d-block img-fluid">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                     Best Marketing Strategies
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        This useful package that includes source Ebook document, 25 PLR articles, Product Analysis PDF and fast action ideas PDF is something that can scale your marketing agency. Use it without fail with LinkPro Agency upgrade, and see results like you always wanted.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox">
                     </div>
                  </div>
               </div>
               <!-- bonus2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="d-block img-fluid">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                        Outsourcing Fundamentals Development and Strategy
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        This internet marketing report based on outsourcing fundamentals development and strategy includes blog posts, forums, YouTube videos and other related stuff for your business growth. Use it with the immense powers of LinkPro Agency Upgrade to get results that your competitors envy.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6">
                     <div class="mt20 mt-md50">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox2">
                     </div>
                  </div>
               </div>
               <!-- bonus3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <img src="assets/images/bonus3.webp" alt="Bonus 3" class="d-block img-fluid">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                     Success in Business
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Wondering how to dominate your competition with success in business PLR content package, you’re at the right place. This Package includes source Ebook document, 25 PLR articles, product analysis PDF and a fast action ideas PDF. Don’t spend time thinking, just use this with LinkPro Agency Upgrade and scale your business to new heights.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox3">
                     </div>
                  </div>
               </div>
               <!-- bonus4 -->
               <div class="col-12 col-md-12  mt40 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <img src="assets/images/bonus4.webp" alt="Bonus 4" class="d-block img-fluid">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                     The Copywriter's Handbook
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                     Get your hands on proven tips and tricks used by world’s top marketers and learn why they became such envious success stories. Use this information with LinkPro Agency Upgrade, and see yourself on the road to online marketing success.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt-md30">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox4">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--10. Table Section Starts -->
      <div class="table-section padding10" id="buynow">
         <div class="container">
            <div class="row gx-6">
               <div class="col-12 col-md-12">
                  <div class="f-md-45 f-28 w800 lh140 mt20 mt-md30 text-center">
                     Today You Can <span class="orange-gradient">Get Unrestricted Access to LinkPro Agency</span> Suite for Less Than the Price of Just One Month’s Membership.
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md70">
                  <div class="tablebox2" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg2 text-center">
                        <div>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve" style="max-height:55px">
                                 <g>
                                     <g id="Layer_1-2">
                                         
                                             <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-1521.2245" y1="8551.4561" x2="-1622.4146" y2="8655.3457" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                                             c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                                         
                                             <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-1357.7941" y1="8721.4482" x2="-1265.4182" y2="8721.4482" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                                             c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                                             z"></path>
                                         
                                             <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-1621.1945" y1="8584.6367" x2="-1625.9146" y2="8690.4863" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                                             c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                                         
                                             <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-1329.8795" y1="8785.458" x2="-1427.5396" y2="8597.5684" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                                             c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                                             l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                                             C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                                         
                                             <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-1440.667" y1="8764.8115" x2="-1314.1017" y2="8764.8115" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#EFA222"></stop>
                                             <stop offset="1" style="stop-color:#F0592D"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                                             c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                                             C186.6,296.6,181.3,304,174.9,310.3z"></path>
                                         
                                             <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-1642.7045" y1="8691.5225" x2="-1462.8146" y2="8737.333" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -6984.1504 -4910.4199)">
                                             <stop offset="0" style="stop-color:#3BB6FF"></stop>
                                             <stop offset="1" style="stop-color:#006DE3"></stop>
                                         </linearGradient>
                                         <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                                             c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                                             c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                                             c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                                         <rect x="613.5" y="166.6" fill="#14142A" width="44.4" height="159.8"></rect>
                                         <path fill="#14142A" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                                             c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                                         <path fill="#14142A" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                                             c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                                             c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                                         <polygon fill="#14142A" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                                         <polygon fill="#14142A" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                                             920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                                         <path fill="#14142A" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                                             c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                                             c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                                             c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                                         <path fill="#14142A" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                                             c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                                             c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                                         <path fill="#14142A" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                                             c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                                             c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                                             C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                                             c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                                             c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                                     </g>
                                 </g>
                                 </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md30 w700 f-md-24 f-22 text-center black-clr lh120">Agency – 100 Client License</div>
                     </div>
                     <div>
                        <ul class="f-18 f-md-20 w400 lh140 text-center vgreytick mb0">
                           <li>Sell high-in-demand QR Code & Bio Page Building Services to your clients & Make a Huge Profit Per Month.</li>
                           <li>Start your own software business to get results like Gurus starting Today</li>
                           <li>No Profit Sharing</li>
                           <li>No Investment</li>
                           <li>Nothing to Upload, Host or Configure</li>
                           <li>Serve 100 Clients with Agency License</li>
                           <li>100 Team Members</li>
                           <li>Business Management</li>
                           <li>Done-For-Your Team Management Panel -Manage all your clients & your team from a single dashboard to have full control.</li>
                           <li>Work Collaboratively & Share Proven Smart Links & Bio -Templates</li>
                           <li>Create Your Own QR Codes & Page Templates and Sell them your Clients to Make More Profits</li>
                           <li>Accurate Analysis for Team Member’s Activities For Effective Monitoring</li>
                           <li>Support to you & your clients - chat support directly from software</li>
                           <li>Get All These Benefits For One Time Price</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="" editabletype="button" style="z-index: 10;">
                        <a href="https://www.jvzoo.com/b/107757/390832/2"><img src="https://i.jvzoo.com/107757/390832/2" alt="LinkPro 100 Agency License" border="0" class="img-fluid d-block mx-auto" /></a> 
                        <a href="https://www.jvzoo.com/b/108261/393938/2"><img src="https://i.jvzoo.com/108261/393938/2" alt="LinkPro 100 Agency License" border="0" class="img-fluid d-block mx-auto" /></a> 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md70">
                  <div class="tablebox3" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg3 text-center">
                        <div>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md30 w700 f-md-24 f-22 text-center white-clr lh120">Agency – Unlimited Client License</div>
                     </div>
                     <div>
                     <ul class="f-18 f-md-20 w400 lh140 text-center vgreytick mb0">
                     <li>Sell high-in-demand QR Code & Bio Page Building Services to your clients & Make a Huge Profit Per Month.</li>
                           <li>Start your own software business to get results like Gurus starting Today</li>
                           <li>No Profit Sharing</li>
                           <li>No Investment</li>
                           <li>Nothing to Upload, Host or Configure</li>
                           <li>Serve Unlimited Clients with Agency License</li>
                           <li>Unlimited Team Members</li>
                           <li>Business Management</li>
                           <li>Done-For-Your Team Management Panel -Manage all your clients & your team from a single dashboard to have full control.</li>
                           <li>Work Collaboratively & Share Proven Smart Links & Bio -Templates</li>
                           <li>Create Your Own QR Codes & Page Templates and Sell them your Clients to Make More Profits</li>
                           <li>Accurate Analysis for Team Member’s Activities For Effective Monitoring</li>
                           <li>Support to you & your clients - chat support directly from software</li>
                           <li>Get All These Benefits For One Time Price</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast myfeatureslastborder f-md-25 f-16 w400 text-center lh140 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="" editabletype="button" style="z-index: 10;">
                        <a href="https://www.jvzoo.com/b/107757/390822/2"><img src="https://i.jvzoo.com/107757/390822/2" alt="LinkPro Unlimited Agency License" border="0" class="img-fluid d-block mx-auto"/></a> 
                        <a href="https://www.jvzoo.com/b/108261/393936/2"><img src="https://i.jvzoo.com/108261/393936/2" alt="LinkPro Unlimited Agency License" border="0" class="img-fluid d-block mx-auto"/></a> 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                  <a href="https://www.linkpro.co/premium-membership " target="_blank" class="kapblue f-18 f-md-20 lh140 w400">
                  No thanks - I don't want to use the untapped POWER to setup my own pro agency without doing any extra work. I know that LinkPro Agency Upgrade can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase. 
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!--10. Table Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div>
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  </div>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © LinkPro 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right mt20 mt-md0">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://linkpro.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End  -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + days + '</span><br><span class="f-18 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + hours + '</span><br><span class="f-18 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + minutes + '</span><br><span class="f-18 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">' + seconds + '</span><br><span class="f-18 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MailerGPT Bonuses">
      <meta name="description" content="MailerGPT Bonuses">
      <meta name="keywords" content="World's First ChatGPT AI–Powered App Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword">
      <meta property="og:image" content="https://www.mailergpt.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailerGPT Bonuses">
      <meta property="og:description" content="World's First ChatGPT AI–Powered App Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword">
      <meta property="og:image" content="https://www.mailergpt.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailerGPT Bonuses">
      <meta property="twitter:description" content="World's First ChatGPT AI–Powered App Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword">
      <meta property="og:image" content="https://www.mailergpt.com/special-bonus/thumbnail.png">
      <title>MailerGPT Bonuses</title>
      <!-- Shortcut Icon  -->

      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'April 6 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/10103/394283/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>

    <!-- Header Section Start -->   
    <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center align-items-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 blue-gradient"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height:60px;"><defs>
                        <style>
                            .cls-1{fill:#fff;}
                            .cls-2{fill:url(#radial-gradient);}
                            .cls-3{fill:url(#radial-gradient-2);}
                            .cls-4{fill:url(#radial-gradient-3);}
                        </style>
                        <radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#18e6ff"/>
                            <stop offset="0.99" stop-color="#005fed"/>
                        </radialGradient>
                        <radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"/>
                        <radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"/>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"/>
                            <path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"/>
                            <path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"/>
                            <path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"/>
                            <path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"/>
                            <path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"/>
                            <path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"/>
                            <path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"/>
                            <path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"/>
                            <path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"/>
                            <path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"/>
                            <path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"/>
                        </g>
                    </g>
                </svg>
                     </div>
                  </div>
                  <div class="col-12 text-center mt20 mt-md50">
						   <div class="f-md-20 f-18 w500 text-center white-clr pre-heading lh150">
                        <span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span>
						   </div>
                  </div>
                  <div class="col-12 mt-md50 mt20 text-center"> 
                  <div class="f-md-45 f-28 w700 white-clr lh150">               
                       <span class="blue-gradient w700">World's First ChatGPT AI–Powered App  </span>Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks <span class="brush w700"> with Just One Keyword </span>
                  </div>
               </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-md-7 col-12">
                         <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                         <div class="video-box">
                         <div class="col-12 responsive-video">
                           <iframe src="https://mailergpt.dotcompal.com/video/embed/xdphj6h2hr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen ></iframe>
                        </div> 
                           </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li> <span class="list-highlight"> Create 1000s Of </span> High-ConvertingEmails   <span class="list-highlight">With Just One Keyword </span> </li>
                              <li> <span class="list-highlight"> Just Enter Keyword &amp; MailerGPT AI Plans And  </span> Writes Email Content For You </li> 
                              <li><span class="list-highlight">	Send Beautiful Emails For Sales, Promotion, </span> Product Delivery And Many More ...  </li>                           
                              <li> <span class="list-highlight"> Unlimited Everything- </span> Import Unlimited Leads with no loss - No Restrictions </li> 
                              <li><span class="list-highlight"> Send Unlimited Emails </span> To Unlimited Subscribers. </li>
                              <li> <span class="list-highlight"> 	AI Enabled Smart Tagging </span> For Lead Personalization &amp; Traffic </li>
                              <li> <span class="list-highlight"> Attract &amp; Capture Maximum Leads- </span> by Creating Thousands of Unique High Converting Lead Pages  </li>
                              <li> <span class="list-highlight"> 	100+ Stunning Done-For-You Templates </span>For All Your List Building Needs </li>                            
                              <li> Sell High In-Demand Services with <span class="list-highlight"> Included Commercial License.  </span>  </li> 
                           </ul>
                        </div>
                     
                     </div>
                  </div>
               </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
            Create & Send Profit-Pulling Emails with  <br class="d-none d-md-block"><span class="blue-gradient d-inline-grid">MailerGPT in Just 3 Simple Clicks
               <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>
            </div>
            <div class="col-12 mt30 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5 relative">
                     <img src="assets/images/step1.webp" class="img-fluid">
                     <div class=" step1 f-28 f-md-65 w600 mt15">
                     Login & Enter a <br class="d-none d-md-block"> Keyword
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Login to your MailerGPT account & just enter a desired keyword for which you want to create an Email.
                     </div>
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <img src="assets/images/step1.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-5 order-md-2 relative">
                     <img src="assets/images/step2.webp" class="img-fluid">
                     <div class=" step2 f-28 f-md-65 w600 mt15">
                     Generate
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     MailerGPT AI generates profitable, engaging & high-quality email for you in seconds.
                     </div>
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5">
                     <img src="assets/images/step3.webp" class="img-fluid">
                     <div class=" step3 f-28 f-md-65 w600 mt15">
                     Send & Profit
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Send unlimited emails directly into inbox for tons of autopilot traffic & sales with just push of a button.
                     </div>
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step3.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
             <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Single Keyword…</div>
            </div>
            
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr"> No Email Writing &nbsp; </div>
                  </div>
                  <!-- <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div> -->
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <!-- <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div> -->
               </div>
            </div>
           <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
           Plus, with included free commercial license, this is the easiest & fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
         </div>
      </div>
   </div>       
<!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailerGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w500 text-center black-clr">
                     <span class="w700"> MailerGPT Have Everything You Need </span>To Build A Profitable Email Marketing Business With Ease!
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md50">   
               <div class="col-12 col-md-6">
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                     With MailerGPT, <span class="w600">Tap into Profitable Email Marketing Industry</span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600"> Fully ChatGPT and AI Powered Cloud Based Software </span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Just Enter Keyword </span> & MailerGPT AI Research And Writes Email Content For You
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Sell More Of Your Products, Services & Courses Online </span> Using Email Marketing
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Long-Term Relationships </span>  with Your Subscribers
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Massive Profitable Lists </span>  Using Beautiful Forms
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Import Your Subscribers Without Losing Even a Single Lead </span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Craft & Send Unlimited Beautiful Emails</span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w600">
                     <span class="w600"> 100+ Beautiful & Mobile Friendly Email Templates </span>  for Sales, Promotion, Offers, Leads & Many More…
                     </div>
                  </div>
                  <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                        <span class="w600"> Attractive Built-In Lead Forms </span> To Generate More Potential Subscribers.
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Advance Subscriber Management </span> to Manage Subscribers with Ease
                        </div>
                     </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 lh140 w400">                             
                    
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">AI Enabled Smart Tagging</span> For Easy Segmentation, Lead Personalization
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Boost Email Delivery, Click And Open Rates Instantly.</span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Have 100% Control </span> On Your Online Business
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Promote Affiliate Offers to Make Commissions– </span> No Product Required to Start Online
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                        <span class="w600">Write & Design Newsletter/Autoresponder Mails</span>  Using The Power Of AI
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Deep Analytics - </span> Know your Opens, Clicks & Impressions to boost results
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Inbuilt List Cleaning </span> and List Checking Included
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> 100% GDPR & CAN-Spam Compliant </span> Lead Capture and Emailing System
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> 100% Newbie Friendly </span> 100% Newbie Friendly
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Dedicated Customer Support So </span> You Never Get Stuck
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> PLUS, YOU'LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY! </span> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
              
               </div>
            </div>
               </div>
            </div>





            
         </div>
   <section class="future-section">
      <div class="container">
         <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh130 black-clr text-center thunder">
                  MailerGPT Is FUTURE Of Email Marketing
                  <img src="assets/images/double-blue-line.png" alt="line" class="img-fluid d-block mx-auto">
               </div>
               <!-- <div class="col-12 f-18 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Here you Can See What you Can Do With This Fully ChatGPT Cutting<br class="d-none d-md-block"> Edge Software.
               </div> -->
               <div class="col-md-12 mt-md80 mt20">
               <img src="assets/images/future-img.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/yes-you-can.png" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="f-18 f-md-28 w400 lh150 black-clr text-center mt20 mt-md60">
                 <span class="blue-gradient1 under w600"> The Best Thing is You Just Need to Enter a Keyword </span> And MailerGPT Will Create, <br class="d-none d-md-block">Design & Send Emails For You In Just 60 Seconds Flat.
               </div>
         </div>
      </div>
   </section> 

      
      
     
    <!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailerGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
<!-- Proof Section -->
<div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-42 w700 lh140 black-clr">
                     Checkout How We've Got 20,000+ Clicks & Made Over $25,879 Income by Using the Power of MailerGPT
                  </div>
                 
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
      <div class="white-bg mt-md70 mt30">
            <div class="row align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>

               <div class="col-md-6">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let's Check Out the Crazy Open And Click Rates 
                  We've Got for A Simple Email I Sent Using MailerGPT.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div></div>
      </div>
      <!-- Proof Section End --> 

      <!--Proudly Introducing Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
               </div>
               <div class="col-12 mt-md50 mt20 text-center"> 
                  <div class="f-md-37 f-28 w700 white-clr lh150">   
                     World's First  <span class="blue-gradient w700">ChatGPT AI - Powered Email Marketing App </span> That Writes, Design & Send Unlimited Profit-Pulling <span class="blue-gradient w700">  Emails <br> </span><span class="brush w700"> with Just One Keyword </span> Directly to Inbox for 4X Opens, Clicks and Targeted Traffic
                  </div>
                  <div class="f-md-22 f-18 w400 white-clr lh140 mt30">   
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, and that will send <br class="d-none d-md-block">all the existing money-sucking autoresponders back to their nest.
                  </div>
               </div>
                <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            </div>
         </div>
      </div> 
      <!--Proudly Introducing End -->
      <!-------Coursova Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Vidmaster
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-vidmaster">
         <div class="container">
            <div class="row">

            <div class="col-12 mt-md40 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 80px;" xml:space="preserve">
                     <style type="text/css">
                        .st0z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1z{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2z{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7z{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0z" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1z" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2z" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3z" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1z" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4z" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2z" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5z" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6z" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7z" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7z" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7z" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7z" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7z" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7z" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7z" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7z" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7z" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7z" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7z" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 text-center mt20">
                  <div class="pre-heading2 f-20 f-md-24 w500 lh140 white-clr">
                     Encash the Latest TRAFFIC Trend of 2023…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Game-Changer AI APP Creates Stories, Reels, Boomerang, &amp; Short Videos with <span class="red-clr">Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing &amp; Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left mt-md40 mt0">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/8x6aokyyl7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
                  
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels &amp; Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors &amp; Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidMaster</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/vidmaster.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/vidmaster-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/vidmaster.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!------Coursova Section ends------>

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Buzzify
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------Trendio Section------>

      <div class="buz-header-section">
         <div class="container">
            <div class="row">
		
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                              <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                              <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                              <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                              <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                              <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                              <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
			   	<!-- <div class="col-12 mt20 mt-md50"> <img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto"></div> -->
               <div class="col-12 text-center lh150 mt20 mt-md30">
                  <div class="pre-heading-b f-md-20 f-18 w600 lh150">
                     <div class="skew-con d-flex gap20 align-items-center ">
                        It’s Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022 
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
				  Breakthrough 3-Click Software Uses a <span class="under yellow-clr w800"> Secret Method to Make Us $528/Day Over and Over</span>  Again Using the Power of Trending Content &amp; Videos 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 yellow-clr">
                 All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                  <div class="col-12 responsive-video">
                     <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/buzzify.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/buzzify-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/buzzify.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
        
      </div>
      <!------Trendio Section Ends------>
   
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : AppZilo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-appzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-md-left text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
                     <style type="text/css">
                        .st0aa{fill:#FFFFFF;}
                        .st1aa{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                        .st2ch{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st3aa{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
                     </style>
                     <g>
                        <path class="st0aa" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                           c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                           s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                        <path class="st0aa" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                           M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                           c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                        <path class="st0aa" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                        <path class="st0aa" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                           c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                           l0-84.2L505.3,36.7z"></path>
                        <path class="st0aa" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                        <path class="st0aa" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                           c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                           c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                           C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                           c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
                     </g>
                     <g>
                        <path class="st2ch" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                           <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                           <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                           <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                           <stop offset="1" style="stop-color:#0D1793"></stop>
                        </linearGradient>
                        <path class="st2ch" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                           <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                           <stop offset="0.6981" style="stop-color:#F39242"></stop>
                           <stop offset="1" style="stop-color:#F2A246"></stop>
                        </linearGradient>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                           M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                           c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                           c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-appzilo f-md-22 f-16 w500 lh140 white-clr">
                  World's #1 &amp; Fastest 1-Click iOS/Android Mobile App Builder At A Low One Time Price!
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-38 f-28 w600 text-center white-clr lh140">
               Brand New 1-Click Cloud App LETS YOU CONVERT YOUR EXISTING WEBSITE INTO AN iOS &amp; Android APP + UNLIMITED MOBILE APPS FOR IOS &amp; ANDROID FROM <u class="stats-headline1">SCRATCH</u>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     Create &amp; Sell Mobile Apps for BIG Profits to Affiliates, Coaches, Attorney, Dentists,<br class="d-none d-md-block"> Gyms, &amp; 10,000+ Other Niches | No Coding or Prior Experience Needed...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <div class="row">
                     <div class="col-md-6 col-12  mt-md20">
                        <!-- <img src="https://cdn.oppyo.com/launches/appzilo/special/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://appzilo.dotcompal.com/video/embed/7fu418e5f4" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                    </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <div class="key-features-bg-appzilo">
                           <ul class="list-head-app pl0 m0 f-18 lh150 w400 white-clr">
                              <li>Create Unlimited iOS &amp; Android Apps </li>
                              <li>Comes With Simple Drag &amp; Drop Editor </li>
                              <li>Sell Limitless Apps to Online &amp; Local Businesses </li>
                              <li>Fastest and bug-free delivery.</li>
                              <li>Turn Any Website Into Fully Fledged Apps in Seconds </li>
                              <li>No App Store &amp; Play Store Approval </li>
                              <li>Comes With Pre-Built Templates </li>
                              <li>No Technical Skills Required </li>
                              <li>Google Ads Monetization </li>
                              <li>100% Newbie Friendly </li>
                              <li>One Time Price and Use Forever </li>
                              <li>Work For ANY Business in ANY Niche </li>
                              <li>Comes With Unlimited Commercial License </li>
                              <li>Ultra-Fast Customer Support </li>
                              <li>$200 Refund If Doesn’t Work for You </li>
                           </ul>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/appzilo-ss.webp">
            <source media="(min-width:320px)" srcset="assets/images/appzilo-mview.webp">
            <img src="assets/images/appzilo-ss.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : NinjaKash
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="headernj-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mt-md7">
							<img src="assets/images/njlogo.png" class="img-fluid mx-auto d-block">
						</div>
					</div>
					<div class="col-12 col-md-12 text-center">
						<div class="mt20 mt-md50">
							<div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-nj">
								Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="mt-md30 mt20">
							<div class="f-28 f-md-50 w500 text-center white-clr lh140">
							
							<span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
							</div>
						</div>
						<div class="f-22 f-md-28 w600 text-center orange lh140 mt-md30 mt20">
							
							NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
							
						</div>
					</div>
				   <div class="col-12 col-md-12">
						<div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left mt20 mt-md50">
                     <div class="col-12 responsive-video">
                        <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div> 

                  </div>
							
							<div class="col-md-4 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md40 mt20 min-md-video-width-right">
								<ul class="list-head pl0 m0">
									<li>Kickstart with 50 Hand-Picked Products</li>
									<li>Promote Any Offer in Any niche HANDS FREE</li>
									<li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
									<li>Drive TONS OF Social &amp; Viral traffic</li>
									<li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
									<li>No Monthly Fees…EVER</li>
								</ul>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/ninijakash.webp">
            <source media="(min-width:320px)" srcset="assets/images/ninijakash-mview.webp">
            <img src="assets/images/ninijakash.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
      <!-- Vocalic Section ENds -->
    

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MailerGPT, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

    
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Xyber Email Assistant  
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                       <b> <li>Do Your Customer Support with Ease Using Xyber Email Assistant!</b></li>
                       <li> If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                       <li> The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Subject Lines Standout 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span> </li>
                           <li>The money is in the list. And if you are building your email list today, the next question is that, are your email series get opens? </li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know.</li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Viral List Autopilot 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Finally, Discover How to Build a Highly Profitable List By Using This Untapped Viral Strategy!  </span></li>
                           <li>Starting Today! This video course will take you behind the scenes to help you understand how to build a higher converting list by leveraging other people’s lists! </li>
                           <li>Indeed, the money is in the list and if you are not implementing into your blog or in your business, you are missing a lot of potential customers. </li>
                           <li>Inside this product is a serious of video tutorial on how to build an email list on autopilot using the power of social media networking sites.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Rapid Lead Magnets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">How to create quick & easy 'Lead Magnet' funnels you can use to build targeted lists and attract buyers!</span> </li>
                           <li>Lead Magnets are basically things that you'll give away for free in exchange for an email address so that you can follow up with a visitor or subscriber and ultimately get them to build a relationship with you and build rapport. In that way, you will be able to sell them your front and offers.</li>
                           <li>As long as everything is congruent and related to each other, you should have very high conversion. Think quality oer the amounts of the lead magnets that you have.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        CPA Email Marketing 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">        
                           <li class="w600">Learn From This Audio, Give It Away To Build Your Email List & Sell The Whole Product With MRR</li>
                           <li>One of the ways you can organize and automate your CPA network offers is through automatic email campaigns. If you already have a list of subscribers to some websites or blogs you own, you already have a means to do an email campaign with CPA offers.</li>
                           <li>When someone signs up to your website or blog, you are going to add them to your email list and send them a notice that you recognize their signup. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

<!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailerGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
  
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Marketing Expert 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Doing business is more than a full-time job.</span></li>
                           <li>Your days are spent selling and procuring products, ensuring customer satisfaction and when you are home, you have to work on new products, ideas to improve your service, track finances and do the research to grow your business. </li>
                           <li>This leaves little or no time to learn new things. </li>
                           <li>This course has everything you need to know to boost your online reputation and GET HUNDREDS OF PEOPLE SINGING UP TO YOUR LISTS EVERY WEEK.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boost Your Productivity 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">We need to be more proficient in our work to accomplish more.  </span>In many cases, people fail to be productive because they lose focus and let their minds wander, leading to a loss in productivity.</li>
                           <li>This quick guide will reveal you basic ingredients of productivity and tehniques how to better manage your time. </li>
                           <li>This product contains all the features for building your list: List Building Report, 'Mobile Responsive' Minisite, Confirmation + Thank You Page, Professional Graphics Pack etc</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        OptiRoi
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">OptiROI will help you maximize profits regardless of what niche you're in!</span> </li>
                           <li>You can also use this technology to build bigger email lists, which equates to much more future revenue! If you want to outsmart and dominate your competition in today's crowded and highly-competitive landscape, then you need to be proactive with your marketing. </li>     
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           How to Keep Your Email Subscribers 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn How to Keep Your Email Subscribers!</span> </li>
                           <li>Indeed, the money is in the list. That's why you decided to build your own email list but as you go along, building a list is not just your task that you have to take care of.</li>
                           <li>There is the concern of how to make your list conversion increase and most of all how to keep your list intact or at least you have low number of attrition.</li>
                           <li>Well, if you will look to other business model, attrition is normal but if you will handle your list quite well, you can decrease it numbers and make more money from it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Double Your Conversions 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn How to Double Your Email Conversions! </span></li>
                           <li>The money is in the list. You may already have heard this from many successful online entrepreneurs.</li>
                           <li>If you have been building your email list, your next challenge is how you will be able to make your email conversions higher.</li>
                           <li>Fortunately, inside this product is a podcast that you will give you the proven system that will guide you how to increase your email conversion rate and eventually make sales.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

<!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailerGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->


      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Marketing Video Course 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Discover How to Set Up Your Email Autoresponder withGetResponse So That You Can Grow an Email List That Gets Clicks and Converts into Sales…Starting Today!</li>
                           <li>This video course will take you behind the scenes to help you understand how to build a relationship with your list… </li>
                           <li>It will show you how to plan out your email series, but also how to take the series and set them up on GetResponse.com.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Email Monetizer 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turning your email list into a passive income money maker isn’t as difficult, or time consuming as you may think. </span></li>
                           <li>Every day, thousands of online marketers are transforming their mailing lists into powerful cash funnels, and quite often, they don’t even have their own product line!</li>
                           <li>This special report will make it easy for you to start making money with your subscriber base even if you’re just starting out. </li>
                           <li>It will show you how you can join the ranks of successful list builders quickly and easily, while increasing engagement, building your tribe and positioning yourself as a thought leader in your market.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Modern Email Marketing and Segmentation 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">Failure in e-marketing comes in many different forms because people try many different things.</li>
                        <li>Social medial marketing faces many challenges because of the evolving algorithms of platforms like Facebook. It’s getting worse and worse with each passing year.</li>
                        <li>E-mail marketing is hands down the most powerful and effective form of online marketing.</li>
                        <li>This is a step-by-step guide to start earning REAL list marketing money with modern email marketing and segmentation techniques. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Find Your Niche 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. </span> </li>
                           <li>Unfortunately, too many new business owners fail within their first year.  </li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Countdown Plugin 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">With this plugin you can create unlimited email countdown optin pages. It works in Wordpress and any WP theme. </li>
                           <li>Collect leads with your countdown page using only the HTML for any auto-responder service. Paste auto-responder code and it will automatically connect to your page.</li>
                           <li>Countdown to any date with a live text countdown that will redirect to any URL after and on the date that you choose. </li>
                           <li>Use the wordpress meta options panel to have complete control over your email countdown page. Edit a variety of options, including your logo or banner image.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
   <!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailerGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

       <!-- Bonus #16 Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email List Management Secrets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">List maintenance is one of the most important subjects in online marketing. Your list is your number one and most basic bottom level output for your promotions.</span></li>
                           <li>It’s expensive and time consuming to gather, but forms one of the most powerful resources and profit potential you have.</li>
                           <li>Depending on your business, there are several solutions that might be right for you. With this ebook you will learn the big five solutions to allow you to decide which one is going to make you the most cash.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Timer Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even INSIDE your emails!   </li>
                           <li>This will help to increase conversions, sales and also clickthrough rate inside your emails because the moment someone opens your email, they immediately see the timer ticking to zero and urging them to take action right away. </li>
                           <li>Other than email, you will have the option to add the countdown timer to your blogs/websites as a widget.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Market Storm Magazines 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600"> This is a collection of Internet Marketing Magazines with 380+ pages of quality content! </span></li>
                        <li>You can start your own monthly or annual magazine program and make 100% passive income.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Traffic Beast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Whether you have a personal blog, business website, or are making money through online advertising, today's currency of success relies, almost exclusively, on the science of cultivating more significant traffic to your website.</span> </li>
                           <li>The traffic that you bring to your website is crucial because it helps you increase your rankings on the various search engines, which is how potential customers can find your company.</li>
                           <li>Unfortunately bringing more traffic to your site these days can be a challenge. With millions of competing websites, it can be difficult for potential customers to find your site.</li>
                           <li>The five powerful techniques outlined in this guide are geared toward a single purpose; helping you drive more traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Marketing Success 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Email marketing for business isn’t a new concept, and it has been proven to be one of the best marketing tactics for return on investment. </li>
                           <li>With more than 205 billion emails being sent and received every day if your business isn't taking advantage of this powerful and massive marketing channel, then you are missing out on a highly effective way to reach your target audience.</li>
                           <li>Creating a successful email marketing campaign isn’t difficult, but it does require you to do more than just send out an occasional newsletter. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 blue-gradient">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITVIP"</span> for an Additional <span class="w700 blue-gradient">$3 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab MailerGPT + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w500 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-gradient">"AMITMAIL"</span> for an Additional <span class="w700 blue-gradient">$50 Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/10103/394281/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailerGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height:60px;"><defs>
                        <style>
                            .cls-1{fill:#fff;}
                            .cls-2{fill:url(#radial-gradient);}
                            .cls-3{fill:url(#radial-gradient-2);}
                            .cls-4{fill:url(#radial-gradient-3);}
                        </style>
                        <radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#18e6ff"/>
                            <stop offset="0.99" stop-color="#005fed"/>
                        </radialGradient>
                        <radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"/>
                        <radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"/>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"/>
                            <path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"/>
                            <path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"/>
                            <path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"/>
                            <path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"/>
                            <path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"/>
                            <path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"/>
                            <path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"/>
                            <path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"/>
                            <path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"/>
                            <path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"/>
                            <path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"/>
                        </g>
                    </g>
                </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color:#fff;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                 
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © MailerGPT 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->


      
      <script>
         const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");

const textArray = ["Emails & Messages" , "Newsletter", "Autoresponder"];
const typingDelay = 100;
const erasingDelay = 80;
const newTextDelay = 1000; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
  if (charIndex < textArray[textArrayIndex].length) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
    charIndex++;
    setTimeout(type, typingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
  	setTimeout(erase, newTextDelay);
  }
}

function erase() {
	if (charIndex > 0) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
    charIndex--;
    setTimeout(erase, erasingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
    textArrayIndex++;
    if(textArrayIndex>=textArray.length) textArrayIndex=0;
    setTimeout(type, typingDelay + 300);
  }
}

document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
  if(textArray.length) setTimeout(type, newTextDelay + 250);
});
      </script>
   </body>
</html>
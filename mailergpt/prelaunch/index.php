<!Doctype html>
<html>
   <head>
      <title>MailerGPT Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="MailerGPT | Prelaunch">
      <meta name="description" content="World's First ChatGPT AI - Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails & Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta name="keywords" content="MailerGPT">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.mailergpt.com/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailerGPT | Prelaunch">
      <meta property="og:description" content="World's First ChatGPT AI - Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails & Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta property="og:image" content="https://www.mailergpt.com/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailerGPT | Prelaunch">
      <meta property="twitter:description" content="World's First ChatGPT AI - Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails & Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta property="twitter:image" content="https://www.mailergpt.com/prelaunch/prelaunch/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" >

      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
       <!-- Start Editor required -->
      
      <script>
      function getUrlVars() {
         var vars = [],
            hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
         for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
         }
         return vars;
      }
      var first = getUrlVars()["aid"];
      //alert(first);
      document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value', first);
      })
      //document.getElementById('myField').value = first;
   </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height:60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                           <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-20 w700 white-clr lh140 text-uppercase preheadline">
                     REGISTER FOR ONE TIME ONLY VALUE PACKED FREE TRAINING & GET AN ASSURED GIFT + 5 FREE LICENSES
                  </div>
               </div>    
                  <div class="col-12 mt-md50 mt20 text-center">
                    <div class="f-md-45 f-28 w700 white-clr lh150">               
                       <span class="blue-gradient w700">World's First ChatGPT AI–Powered App  </span>Writes &amp; Send Unlimited Profit Pulling Emails to 4X Your Email Opens &amp; Clicks <span class="brush w700"> with Just One Keyword </span>
                  </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="f-20 f-md-24 w500 text-center lh140 white-clr text-capitalize">               
                        Say Goodbye To Expensive Email Marketing Apps, Writers, & Freelancers.  No Monthly Fee Ever...
                     </div>
                  </div>     
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="video-box">
                  <img src="assets/images/video-thumb.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
               </div></div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
                  <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                     <div class="f-20 f-md-24 w700 text-center lh180 orange-clr1" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">04<sup contenteditable="false">th</sup> April, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh150 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="94594911" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6499625"/>
                              <input type="hidden" name="redirect" value="https://www.mailergpt.com/prelaunch-thankyou" id="redirect_2de7ae6ed77c4c040c8314f21d95c2d8" />
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-94594911" class="af-form">
                              <div id="af-body-94594911" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-115564973"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-115564973" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-115564974"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-115564974" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-36 f-26 w700 lh140 text-capitalize text-center orange-clr1">
                              Why Attend This Free Training Webinar & What Will You Learn?
                           </div>
                           <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                              Our 20+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You'll Learn:
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <ul class="features-list pl0 f-18 f-md-18 lh150 w400 black-clr text-capitalize">
                             <li> How You Can Generate More Commissions & Sales using Email Marketing.</li> 
                             <li> The importance of segmentation and personalization in email marketing.</li> 
                             <li> Best practices for email design, copywriting and call-to-action.</li> 
                             <li> How You Can Sell More Of Your Products, Services & Training Using ChatGPT AI Powered Email Solutions. </li> 
                             <li> Boost affiliate commissions using Email Marketing With ZERO Tech Hassles. </li> 
                             <li> How you can Get 3X More Email Opening & Clicks & Increases Click-Through Rate (CTR). </li> 
                             <li> Track & Analyse Everything - Know What's Working & What Simply Not For Your Offers. </li> 
                             <li> Lastly, everyone who attends this session will get a chance to Win a Free Gift as we are giving 5 licenses of our Premium solution - MailerGPT</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-md-45 f-26 lh140 w700 text-center white-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar
                     </div>
                     <div class="col-12 f-20 f-md-24 w400 text-center mt10 white-clr">
                        Register Now! And Join Us Live on April 04th, 10:00 AM EST
                     </div>
                      <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="94594911">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6485732">
                           <input type="hidden" name="redirect" value="https://www.mailergpt.com/prelaunch-thankyou" id="redirect_3db605f47672b8ad4cfcaab7f182c2b2">
                           <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-94594911" class="af-form">
                           <div id="af-body-94594911" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-115564973"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-115564973" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-115564974"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-115564974" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height:60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © MailerGPT 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right mt20 mt-md0">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://restrosuit.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-1316343921').parentElement.removeAttribute('target');
             }
         })();
         </script><script type="text/javascript">
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-94594911")) {
                document.getElementById("af-form-94594911").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-94594911")) {
                document.getElementById("af-body-94594911").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-94594911")) {
                document.getElementById("af-header-94594911").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-94594911")) {
                document.getElementById("af-footer-94594911").className = "af-footer af-quirksMode";
            }
        }
    })();
</script>
   </body>
</html>
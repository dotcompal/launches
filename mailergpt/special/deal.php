<!DOCTYPE html>
<html>
   <head>
      <title>MailerGPT Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="MailerGPT Special">
      <meta name="description" content="World's First ChatGPT AI–Powered App  Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword ">
      <meta name="keywords" content="MailerGPT Special">
      <meta property="og:image" content="https://www.mailergpt.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailerGPT Special">
      <meta property="og:description" content="World's First ChatGPT AI–Powered App  Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword ">
      <meta property="og:image" content="https://www.mailergpt.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailerGPT Special">
      <meta property="twitter:description" content="World's First ChatGPT AI–Powered App  Writes & Send Unlimited Profit Pulling Emails to 4X Your Email Opens & Clicks with Just One Keyword ">
      <meta property="twitter:image" content="https://www.mailergpt.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>   
      <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'mailergpt';
             allowedDomain = 'mailergpt.com';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
               </div>
               <!--<div class="col-md-7 mt20 mt-md0">-->
               <!--   <ul class="leader-ul f-16 f-md-18 w500 white-clr text-md-end text-center">-->
               <!--      <li><a href="#features" class="white-clr">Features</a></li>-->
               <!--      <li><a href="#demo" class="white-clr">Demo</a></li>-->
               <!--      <li class="d-md-none affiliate-link-btn"><a href="https://www.jvzoo.com/b/108463/394283/2" class="white-clr">Buy Now</a></li>-->
               <!--   </ul>-->
               <!--</div>-->
               <!--<div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block f-18">-->
               <!--   <a href="https://www.jvzoo.com/b/108463/394283/2">Buy Now</a>-->
               <!--</div>-->
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-20 f-20 w600 text-center white-clr pre-heading lh150">					
                  Stop Paying 100s Of Dollars Monthly to Old-School Email Marketing Services
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center"> 
                  <div class="f-md-45 f-28 w700 white-clr lh150">  
                  
                       <span class="blue-gradient w700">World's First ChatGPT AI–Powered App  </span> Writes & Send Amazing Profit Pulling Emails to 4X Your Email Opens & Clicks <span class="brush w700"> with Just One Keyword </span>
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class=" f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Say Goodbye To Monthly Charging Email Apps & Writers. Free Commercial License Included To Let You Sell Handsfree Lead Generation & Email Marketing Services for BIG Profits
                  </div>
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-12 mt20 mt-md60">
                  <div class="row ">
                     <div class="col-md-7 col-12">
                        <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="video-box">
                         <div class="col-12 responsive-video">
                           <iframe src="https://mailergpt.dotcompal.com/video/embed/xdphj6h2hr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen ></iframe>
                        </div> 
                           </div>
                        <div class="col-12 mt20 mt-md40 d-none d-md-block">
                           <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, <br>Today Only 1-Time $27 
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                           Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">
                                 <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn px0 d-block">Get Instant Access To MailerGPT</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                                
                              <li> Just Enter a Keyword <span class="list-highlight"> & MailerGPT A.I. Research and Writes Email Copies for You </span></li>
                              <li> <span class="list-highlight"> Send Beautiful Emails  </span> for Sales, Promos, Product Updates and More ...</li> 
                              <li><span class="list-highlight">	Promote Affiliate Offers and Earn Commissions– </span> No Product or Service Required </li>                           
                              <li> <span class="list-highlight"> AI Enabled Smart Tagging </span> for Lead Personalization & Traffic </li> 
                              <li><span class="list-highlight"> Upload Unlimited Subscribers</span> with Zero Restrictions </li>
                              <li> <span class="list-highlight"> 	100% Control </span> on Your Online Business </li>
                              <li> <span class="list-highlight"> 100+ Stunning Done-For-You Templates </span> for All Your List Building Needs  </li>
                              <li> <span class="list-highlight"> 	100% Cloud Based </span>No Special Skills Required. No Recurring Fee </li>                            
                              <li> <span class="list-highlight"> FREE Commercial License - </span> Build an Incredible Income Offering Services To Clients! </li> 
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27
                              
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn px0 d-block">Get Instant Access To MailerGPT</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
            Write & Send Profit-Pulling Emails with  <br class="d-none d-md-block"><span class="blue-gradient d-inline-grid">MailerGPT in Just 3 Simple Clicks
               <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>
            </div>
            <div class="col-12 mt30 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5 relative">
                     <img src="assets/images/step1.webp" class="img-fluid">
                     <div class=" step1 f-28 f-md-65 w600 mt15">
                     Login & Enter a <br class="d-none d-md-block"> Keyword
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Login to your MailerGPT account & just enter a desired keyword for which you want to create an Email.
                     </div>
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <img src="assets/images/step1.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-5 order-md-2 relative">
                     <img src="assets/images/step2.webp" class="img-fluid">
                     <div class=" step2 f-28 f-md-65 w600 mt15">
                     Generate
                     </div>
                     <div class="f-18 f-md-20 w600 lh140 mt15">
                     MailerGPT Artificial Intelligence generates profitable & engaging email subject lines & copies for you in seconds.
                     </div>
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5">
                     <img src="assets/images/step3.webp" class="img-fluid">
                     <div class=" step3 f-28 f-md-65 w600 mt15">
                     Send & Profit
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Send unlimited emails directly into inbox for tons of autopilot traffic & sales with just push of a button.
                     </div>
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step3.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
             <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Single Keyword…</div>
            </div>
            
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr"> No Email Copywriting &nbsp; </div>
                  </div>
                  <!-- <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div> -->
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 black-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <!-- <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div> -->
               </div>
            </div>
           <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
           Plus, with included free commercial license, this is the easiest & fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
         </div>
      </div>
   </div>       
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
 
        <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w500 text-center black-clr">
                     <span class="w700"> MailerGPT Have Everything You Need </span>To Build A Profitable Email Marketing Business With Ease!
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md50">   
               <div class="col-12 col-md-6">
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                     With MailerGPT, <span class="w600">Tap into Profitable Email Marketing Industry</span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600"> Fully ChatGPT and AI Powered Cloud Based Software </span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Just Enter Keyword </span> & MailerGPT AI Research And Writes Email Content For You
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Sell More Of Your Products, Services & Courses Online </span> Using Email Marketing
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Long-Term Relationships </span>  with Your Subscribers
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Massive Profitable Lists </span>  Using Beautiful Forms
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Import Your Subscribers Without Losing Even a Single Lead </span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Craft & Send Unlimited Beautiful Emails</span> 
                     </div>
                  </div>
                  <div class="feature-list">
                     <div class="f-18 f-md-20 lh140 w600">
                     <span class="w600"> 100+ Beautiful & Mobile Friendly Email Templates </span>  for Sales, Promotion, Offers, Leads & Many More…
                     </div>
                  </div>
                  <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                        <span class="w600"> Attractive Built-In Lead Forms </span> To Generate More Potential Subscribers.
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Advance Subscriber Management </span> to Manage Subscribers with Ease
                        </div>
                     </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 lh140 w400">                             
                    
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">AI Enabled Smart Tagging</span> For Easy Segmentation, Lead Personalization
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Boost Email Delivery, Click And Open Rates Instantly.</span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Have 100% Control </span> On Your Online Business
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Promote Affiliate Offers to Make Commissions– </span> No Product Required to Start Online
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                        <span class="w600">Write & Design Newsletter/Autoresponder Mails</span>  Using The Power Of AI
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Deep Analytics - </span> Know your Opens, Clicks & Impressions to boost results
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Inbuilt List Cleaning </span> and List Checking Included
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> 100% GDPR & CAN-Spam Compliant </span> Lead Capture and Emailing System
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> 100% Newbie Friendly </span> 100% Newbie Friendly
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> Dedicated Customer Support So </span> You Never Get Stuck
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> PLUS, YOU'LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY! </span> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
              
               </div>
            </div>
               </div>
            </div>





            
         </div>
   <section class="future-section">
      <div class="container">
         <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh130 black-clr text-center thunder">
                  MailerGPT Is FUTURE Of Email Marketing
                  <img src="assets/images/double-blue-line.png" alt="line" class="img-fluid d-block mx-auto">
               </div>
               <!-- <div class="col-12 f-18 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Here you Can See What you Can Do With This Fully ChatGPT Cutting<br class="d-none d-md-block"> Edge Software.
               </div> -->
               <div class="col-md-12 mt-md80 mt20">
               <img src="assets/images/future-img.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/yes-you-can.png" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="f-18 f-md-28 w400 lh150 black-clr text-center mt20 mt-md60">
                 <span class="blue-gradient1 under w600"> The Best Thing is You Just Need to Enter a Keyword </span> And MailerGPT Will Create, <br class="d-none d-md-block">Design & Send Emails For You In Just 60 Seconds Flat.
               </div>
         </div>
      </div>
   </section> 

      
      <!-- Proof Section -->
      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-42 w700 lh140 black-clr">
                     Checkout How We've Got 20,000+ Clicks & Made Over $25,879 Income by Using the Power of MailerGPT
                  </div>
                 
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
      <div class="white-bg mt-md70 mt30">
            <div class="row align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>

               <div class="col-md-6">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let's Check Out the Crazy Open And Click Rates 
                  We've Got for A Simple Email I Sent Using MailerGPT.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div></div>
      </div>
      <!-- Proof Section End --> 
     
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>

       <div class="revolutionary-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Here's What MailerGPT Users Have to 
               </div>
               <!-- <div class="col-12 f-20 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
               Here's What Industry Experts Have to 

               </div> -->
               <div class="col-md-8 col-12 mx-auto f-20 f-md-28 w600 lh140 white-clr blue-bg text-center mt20 mt-md30">
               Say About This Unbeatable, Fully ChatGPT AI Powered Email Marketing Technology - MailerGPT 
               </div>
               <div class="mt20 mt-md50">
                  <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
               </div>
            </div>
            <div class="row mt30">
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     As someone who is always on the go, I often struggle to find the time and energy to craft thoughtful and engaging emails. That's why MailerGPT has been a game-changer for me. The AI-powered app is incredibly intuitive and has allowed me to streamline my communication like never before. I highly recommend MailerGPT.
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi1.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Robert John  
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     Finally, I am getting my emails delivered, clicked, and opened. Yes, <span class="w600">MailerGPT is one of the BEST tools </span> I've used for my business yet! No more dependency on other email software. Stupidly simple to use. MailerGPT made me go crazy.<span class="w600"> Five Stars from my side for this product…</span> </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi2.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     William Smith 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               
           
         </div>
      </div>
   </div> 
    
     
      <div class="unstoppable-section">
         <div class="container">
            <div class="row">
               <div class=" col-12 text-center mx-auto">
                  <div class="f-22 f-md-55 w700 lh140 text-center white-clr unstoppable ">
                     ChatGPT 4.0 is UNSTOPPABLE 
                  </div>
                  <!-- <div class="f-28 f-md-45 w500 lh140 black-clr mt20 mt-md30">
                  It's Bigger After Investment of 10 Billion Dollars By Microsoft
                  </div> -->
                  <div class="mt20 mt-md30 chatgpt-head-design">
                     <div class="f-22 f-md-32 w700 black-clr lh140">
                    It's Bigger After Investment of 10 Billion Dollars By Microsoft
                     </div>
                  </div>
                  <div class="f-22 f-md-34 w700 lh160 mt20 mt-md30 black-clr">
                  And Instantly Became the  <span class="blue-gradient1 under"> Undisputed Ruler Of The Digital World...</span> 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-7 p0">
                  <div class="f-24 f-md-32 w600 black-clr lh140">
                  It solves all your tedious & time-consuming email writing problems as 
                  </div>
                  <ul class="problem-list pl0 m0 f-20 f-md-22 lh150 w500 black-clr mt20 ">
                     <li>		<span class="w700"> Writing catchy subject </span>lines for more opens & clicks</li>
                     <li>		<span class="w700">Researching best content </span>for your email campaigns.</li>
                     <li>		It follows your exact message, and tone.</li>
                     <li>		<span class="w700">Writes content for any purpose -</span> sales, follow-ups, customer relationship. </li>
                     <li>		<span class="w700">Serve clients within minutes in any niche</span> e.g., Restaurants, Gym etc.</li>
                     <li>		<span class="w700">Ultimately makes your email marketing a piece of cake.</span></li>
                  </ul>
               </div>
               <div class="col-12 col-md-5 relative mt10 mt-md0 p0">
                  <img src="assets/images/game-changer-robot.webp" alt="Robot Question" class="img-fluid mx-auto d-block robot-ques">
               </div>
            </div>
         </div>
      </div>

<!--research section starts-->
<div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                     And MONEY IS IN THE LIST
                     </div>
                  </div>
               </div>
               <!-- <div class="col-md-4 col-10 text-center mt20 mx-auto">
                  <div class="its-simple">
                     <div class="f-20 f-md-30 w700 lh140 black-clr">
                     It's POWERFUL -
                     </div>
                  </div>
               </div> -->
               <div class=" col-10 text-center mt20 mx-auto">
                     <div class="f-20 f-md-30 w700 lh140 black-clr">
                     Hope you have heard that quote already – It's Powerful.
                     </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start alone-bg">
                        	Yes. Email marketing yields an average 4300% return on investment for businesses worldwide. 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start alone-bg">
                        	Email gets 3 times higher conversions & ROI than social media or any other mode of marketing.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/ems1.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start alone-bg">
                        	Traffic from email converts better than any cold ads, search, or social traffic
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/ems2.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That's Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                        Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Unique Twist Section -->
      <section class="unique-sec"> 
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr">
                  Here's The Unique TWIST
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <!-- <div class="f-20 f-md-24 w500 lh140 white-clr mt20 mt-md50">
                  Now we all know that ChatGPT is the future of online world & emails are the most <br class="d-none d-md-block"> powerful and low-cost marketing method. 
                  </div> -->
                  <div class="f-20 f-md-28 w500 lh140 white-clr mt20 mt-md50">
                  When we combine these tow two powerful tools, it brings tons of <br class="d-none d-md-block">Traffic, commission, and sales on 100% autopilot mode. 
                 
                  </div>
                  <!--<div class="mt20 mt-md30 lh140 w500 white-clr f-20 f-md-24 w500 unique-text">-->
                  <!--   So Here we comes with-->
                  <!--</div>-->
                  <div class="col-md-10 col-12 combile-mailgpt mt20 mt-md70">
                     <img src="assets/images/mailgpt.webp" alt="MailerGPT" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-24 f-md-38 w500 lh140 white-clr mt20 mt-md90">
                   With MailerGPT, You Need Just One Keyword To Write, Design, And Send Profitable Emails for...
                  </div>
                  <div class="mt20 mt-md30">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-4 gap30  justify-content-center mt0 mt-md50">
               <div class="col">
                  <img src="assets/images/u1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u6.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u7.webp" class="img-fluid mx-auto d-block">
               </div>
               <!-- <div class="col">
                  <img src="assets/images/u8.webp" class="img-fluid mx-auto d-block">    
               </div> -->
               <div class="col">
                  <img src="assets/images/u9.webp" class="img-fluid mx-auto d-block">
               </div>
              
            </div>
         </div>
      </section>
      <div class="hi-their">

         <div class="container">

            <div class="row">

               <div class="col-12 text-center">

                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr ">

                     If You're Not Using Emails, You're <br class="d-none d-md-block">Actually <br class="d-block d-md-none"><span class="money-head">Losing Money!</span>  

                  </div>

               </div>

            </div>

            <div class="row">

               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">

                  <img src="assets/images/power-point.webp" alt="power-point" class="mx-auto d-block img-fluid">

               </div>

            </div>

            <div class="row">

               <div class="col-12 col-md-12  mt20 mt-md100">

                  <div class="power-block">

                     <div class="">

                     <div class="f-md-38 f-28 w600 lh140 black-clr mt10 text-center text-md-start">

                     Top companies & marketers know this secret and that’s why…

                     </div>

                     <div class="f-md-28 f-20 lh140 black-clr mt20">

                        <span class="w700">1000’S Of Successful Entrepreneurs (Both Big And Small)</span>  Promote Offers With Email Marketing Everyday To Make Thousands of Dollars Just From Their Laptop! 

                     </div>

                     </div>

                     

                     <img src="assets/images/entrepreneurs.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">

                  </div>

               </div>

            </div>

</div>

 </div>
<!--research section ends-->

     

      <section class="still-think">
         <div class="container">
            <div class="row">
                 <div class="f-24 f-md-28 w400 lh140 black-clr text-center">
                 <span class="w700">By using ChatGPT +  </span> Email marketing, you can promote lots of affiliate products in any niche in any country those pay healthy recurring commissions for life. Tons of such great products from top vendors are available to promote on:
                  </div>
               <div class="col-12 text-center">
                  
                  <div class="mt30 mt-md50">
                     <img src="assets/images/logo-images.png" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="col-12 mt20 mt-md100">
                  
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh170 text-center">So, it can be safely stated that…<br><br>
                     <span class="w600 f-md-40 w800 mt20 mt-md60">Chat GPT AI + Email Marketing Together is the <br class="d-none d-md-block" >BIGGEST Income Opportunity Right Now!</span>
                     <div class="f-18 f-md-26 w400 lh140 text-center mt20">And, I'm sure you're a smart entrepreneur and definitely <br class="d-none d-md-block"> want to exploit this huge cash-cow! </div>
                  </div>
               </div>
                 
               </div>
            </div>
        
         </div>
      </section>
      <div class="big-question-bg">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7">
                  <div class="f-28 f-md-45 w700 white-clr big-headline">
                  But Here's a Big Question…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center text-md-start">
                  Even after the fact that email marketing is the need of the hour and is a most efficient form of marketing. It is surprising to see that only few of small businesses use email marketing to reap the benefits.
                     <br><br>
                     <span class="w600 f-24 f-md-32">Why?</span>
                  </div>
               </div>
               <div class="col-md-5 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bigquestion-shape.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>

      <div class="problem-bg-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-40 w600 lh140 text-center white-clr">
                  Because Trying To Piece Together The Perfect Email Marketing Solution Can Be <u class="red-clr1">A Serious Pain & Expensive!</u>
                  </div>
                  
                  <div class="f-20 f-md-22 w600 lh140 text-center white-clr mt20 problem-area-shape mt-md30">
                  So, If You are Planning for a 3rd Party Email Service, THINK AGAIN!!
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7">
                  <ul class="problem f-18 w400 lh160 white-clr">
                     <li>	You need to write email manually which is time consuming. And if you don't have good writing skills, it's a BIG headache.</li>
                     <li>It's almost impossible to import your subscriber list and if you succeed, you still may lose 20-30% of your leads. </li>
                     <li>	You're stuck with many other customers who share the same resources, constant downtimes, spam complaints, bounces, and unexplained delays. It happens even when you don't do anything wrong. It's due to someone else's mistake.</li>
                     <li>That's not all. They can shut down your account anytime without any prior notice & you can lose your entire business overnight.</li>
                     <li>	They will also have access to your valuable list, so data leak is also a BIG risk.</li>
                     <li>	They charge you anywhere from $100-150 PER MONTH for just 10,000 subscribers and you can only send limited emails to those limited subscribers for that price. </li>
                     
                  </ul>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/problem.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="f-20 f-md-30 w600 text-center white-clr">Even after that, there is still no guarantee that you’ll get the desired ROI from your campaigns as you expected.</div>
               </div>
            </div>
         </div>
      </div>
      <section class="hi-their">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-45 w600 blue-clr lh140 relative trigger-block ">
                     Hey There,
                  </div>
                  <div class="f-md-24 f-20 w400 lh140 text-center black-clr mt20 mt-md50">
                     <b>It's Brett Ingram along with my business partner Pranshu Gupta & Bizomart Apps!</b>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50"> 
               <div class="col-md-4 col-12 text-center mt30 mt-md0">
                  <img src="assets/images/brett.webp" class="img-fluid d-block mx-auto " alt="Brett Ingram" style="max-height: 200px;">
                  <div class="f-24 f-md-24 w700 lh140 black-clr mt20">
                  Brett Ingram
                  </div>
               </div>
               <div class="col-md-4 col-12 text-center mt20 mt-md0">
                  <img src="assets/images/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta" style="max-height: 200px;"> 
                  <div class="f-24 f-md-24 w700 lh140 text-center black-clr mt20">
                     Pranshu Gupta
                  </div>
               </div>
               <div class="col-md-4 col-12 text-center mt30 mt-md0">
                  <img src="assets/images/bizomart.webp" class="img-fluid d-block mx-auto " alt="Bizomart" style="max-height: 200px;">
                  <div class="f-24 f-md-24 w700 lh140 black-clr mt20">
                     Bizomart Apps
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 ">
            
                  <div class="f-24 f-md-38 w800 blue-gradient lh100">Let me tell you my story: <br>
                  <img src="assets/images/let-line.webp" class="img-fluid"></div>
               <div class="col-12 f-18 f-md-24 lh140 w400 black-clr text-center text-md-start mt20">
               3 years back, I was searching for a better solution for my email marketing. So, after doing my research and numerous brainstorming sessions, I decided to choose MailChimp for sending emails for boosting sales and profits.
               <br><br>
               Things were going quite smooth; my emails were getting opened and click-through rates were also decent enough. So, I felt like a king, and my dreams clouds started showering rains of overnight fame and success.<br><br></div>
               <div class="col-12 f-18 f-md-24 lh140 w600 black-clr text-center ">
                  But destiny had something else in the store, and one morning while sipping my daily cappuccino, my eyes were split wide open to read this horrendous email
               </div>
            </div>
         </div>
      </section>
     


      <div class="problem-bg-section2">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12">
                  <img src="assets/images/suspended.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 mt-md50 white-clr mb20 mb-md50">
                  These were literally the first words that echoed in my mind after I checked that mail. After all, I didn't do anything wrong or unethical. I wasn't spamming, nor was I sending fishy emails to my subscribers. <br><br>
                  I felt completely done and dusted. I couldn't do anything as I had ZERO control and had to rely on a 3rd party for sending mails and earning my daily bread. 
               </div>
               <div class="col-12 col-md-5 order-md-2">
                  <img src="assets/images/problem2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 order-md-1 mt20 mt-md0">
                  <div class="f-18 w400 lh160 white-clr">
                     I gathered some courage and got in touch with their support team. And you'll be
                     amazed to know that I couldn't get a justifiable and believable reply from their 
                     end, and ignored any contact attempts that I made after that. They kept playing 
                     with me like a football, kicking me mercilessly from one corner to another.
                     <br><br>
                     I am still amazed why it happened. It sounds as difficult as a Greek puzzle.
                     <br><br>
                     My business was dying a slow, painful death, and I wasn't giving my 
                     subscribers what they needed... they were not getting due worth for 
                     their faith in me.
                     <br><br>
                     If you've already gone through all this, there's no doubt you've experienced 
                     frustration and the loss of profits. And so, all that came to my mind is that 
                     I had to prepare for a hard and bumpy ride.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="problem-bg-section3">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w600 black-clr shape-headline lh140">
                     I NEED FULLControl Over My Online Business
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20  w400 lh160 mt20 mt-md50 black-clr">
                  That's it, I told myself. I decided from now on, I won't put my faith in these money-sucking service providers that are sitting with a big crocodile's mouth open to eat you once and for all.<br><br>
                  So, I put everything else aside, got my technical team into a huddle, and decided that we will create a cloud-based email marketing platform that gives better and trackable results time and time again.<br><br>
                  After lots of hard work and burning my midnight lamps for countless nights, I came up with this magnificent feature-packed Email marketing system that gives me complete freedom of work &amp; control over my business.
               </div>
            </div>
         </div>
      </div>

      <!--Proudly Introducing Start -->
     <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
               </div>
               <div class="col-12 mt-md50 mt20 text-center"> 
                  <div class="f-md-37 f-28 w700 white-clr lh150">   
                     World's First  <span class="blue-gradient w700">ChatGPT AI - Powered Email Marketing App </span> That Writes, Design & Send Unlimited Profit-Pulling <span class="blue-gradient w700">  Emails <br> </span><span class="brush w700"> with Just One Keyword </span> Directly to Inbox for 4X Opens, Clicks and Targeted Traffic
                  </div>
                  <div class="f-md-22 f-18 w400 white-clr lh140 mt30">   
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, and that will send <br class="d-none d-md-block">all the existing money-sucking autoresponders back to their nest.
                  </div>
               </div>
                <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            </div>
         </div>
      </div> 
      <!--Proudly Introducing End -->

      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                  Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                          Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                           </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="assets/images/v-line.webp" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>

   
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Replace Old-School Email Marketing Techniques With This Cutting-Edge AI Powered Solution  <span class="blue-gradient d-inline-grid"> In Just 3 Easy Steps...
                  <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5 relative">
                     <img src="assets/images/step1.webp" class="img-fluid">
                     <div class=" step1 f-28 f-md-65 w600 mt15">
                     Login &amp; Enter a <br class="d-none d-md-block"> Keyword
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Login to your MailerGPT account & just enter a desired keyword for which you want to create an Email.
                     </div>
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <img src="assets/images/step1.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-5 order-md-2 relative">
                     <img src="assets/images/step2.webp" class="img-fluid">
                     <div class=" step2 f-28 f-md-65 w600 mt15">
                     Generate
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     MailerGPT AI generates profitable, engaging & high-quality email for you in seconds.
                     </div>
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
             <div class="col-12 mt-md150">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5">
                     <img src="assets/images/step3.webp" class="img-fluid">
                     <div class=" step3 f-28 f-md-65 w600 mt15">
                     Send & Profit
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Send unlimited email to your subscriber or schedule it for later for automated traffic & sales.
                     </div>
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step3.gif" class="img-fluid d-block mx-auto gif-bg">
                  </div>
               </div>
            </div>
               <div class="col-12 mt20 mt-md30 text-center">
                 
                  <div class="d-flex gap-2 gap-md-0 justify-content-center flex-wrap mt20 mt-md30">
                     <div class="d-flex align-items-center gap-2 justify-content-center">
                        <!-- <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block"> -->
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> NO Writing  &nbsp; | &nbsp;ZERO Technical Skills &nbsp; |&nbsp; ZERO Headache</div>
                     </div>
                     
                    
                  </div>
               </div>
               <!--<div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md30 aos-init aos-animate" data-aos="fade-up">-->
               <!--   <div class="smile-text">It's EASY, FAST, and BUILT with Love.</div>-->
               <!--</div>-->
            </div>
         </div>
      </div>
      <!-- Demo Video Section Start -->
       <div class="demo-section-bg" id="demo">
         <div class="container ">
            <div class="row ">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 orange-clr not-believe-text">
                  Watch This Short Demo Video
                  </div>
               </div>
              
               <div class="mt20">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               <div class="col-12 col-md-9 mx-auto mt20 mt-md50">
                  <!--<img src="assets/images/demo-img.webp" alt="" class="d-block mx-auto img-fluid">-->
                   <div class="responsive-video ">
                     <iframe src="https://mailergpt.dotcompal.com/video/embed/9wjaffttmf" style="width: 100%; height: 100%; " frameborder="0 " allow="fullscreen " allowfullscreen class="gif-bg"></iframe>
                  </div> 
               </div>
               <div class="col-12 mt30">
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="assets/images/v-line.webp" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <!-- Demo Video Section End -->

      <section class="blue-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr still-headline">
                     Checkout The Ground-Breaking Features
                  </div>
                  <div class="f-28 f-md-45 w700 lh140 black-clr mt10">
                     That Make MailerGPT A Cut Above The Rest 
                  </div>
               </div>
            </div>

      
         <div class="container">
            <div class="row align-items-center mt20 mt-md30">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/1.webp" class="img-responsive size">
                     A Fully ChatGPT AI Powered Cloud Based Platform 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     MailerGPT as an advanced technology platform. Its artificial intelligence designed to generate natural language email responses. 
                  </div>
                  <!-- <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 261px;"> -->
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center ">
            <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/2.webp" class="img-responsive size">
                  Create High-Quality Emails & Messages With Just One Keyword 
               </div>
               <div class="col-md-9 mt30 mx-auto">
                  <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
                  MailerGPT AI creates an engaging, high quality & personalized email & subject line using the keyword on which want to craft email for your campaign. 
               </div>
            </div>
         </div>
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center ">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/3.webp" class="img-responsive size">
                     	Send Unlimited Mails Instantly Or Schedule Them For Later Date/Time
                  </div>
                  <div class="f-18  lh140 black-clr w400 mt30">
                  Yes, the sky is the limit for mailing with MailerGPT. It allows you to send unlimited emails or newsletters and get rid of endless complications of email marketing. You can send emails right away or schedule them for a later date and time. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center ">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/4.webp" class="img-responsive size">
                  	AI Enabled Smart Tagging For Lead Personalization & Traffic
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  This is our masterpiece. Using this Latest & VERY Powerful feature, you can assign tags to your subscribers and segment them in a very simple manner. Now you can send emails exclusively to the subscribers related to any smart Tag or group. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/5.webp" class="img-responsive size">
                     	Boost Email Delivery, Click And Open Rate with Engaging Emails
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Opens and clicks give you an accurate idea about how your campaign is performing. Today all the marketers want to get their mails delivered in inbox, clicked and opened on time and that is what MailerGPT is made for. 
                        <br><br>
                     It increases your delivery, click and open rate and gives you full control on your campaigns. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">

         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/6.webp" class="img-responsive size">
               Design Beautiful Newsletters & Autoresponders Using AI 
               </div>
               <div class="col-md-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               Create HUNDREDS of stellar combinations with incredibly magnificent email templates. You are leveraged with premium templates for the HOTTEST of niches. They are completely done-for-you and all you need to do is just get them in sync with your best offers and start funnelling the valuable prospects. 
               </div>
            </div>
         </div>  
      </section>

      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/7.webp" class="img-responsive size">
                     	Import Unlimited Subscribers with Just 1 Click
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                        MailerGPT enables you to import unlimited subscribers list and the best part is that you won't lose even a single ID in the process. So, you can mail freely to your subscribers without any restrictions whatsoever. 
                        <br><br>
                        Most email marketing service providers charge a hefty fees for importing your lists so stop paying a huge monthly rental just to keep your list even if you don't mail them. 
                        <br><br>
                        They also need double optin before allowing to import so you lose anywhere from 20-30% of your subscribers. But with MailerGPT, all that will become a case of the bygone era. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f7.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">

         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/8.webp" class="img-responsive size">
               Generate Tons of Subscribers With Our Attractive In-Built Lead Form -
               </div>
               <div class="col-md-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               Lead forms are the simplest way to generate qualified leads and build a huge base of cash paying customers for your business. 
                        <br><br>
                        Keeping this in mind, MailerGPT helps you to grab attention of more and more subscribers on your blog, e-commerce sites or WordPress sites with an eye-catchy lead generation form. 
                        <br><br>
                        All you need to do is: copy one line of code and paste it on your site.
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/9.webp" class="img-responsive size">
               Inbuilt AI Text & Inline Editor To Craft Best Emails - 
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               You can create simple text emails or html emails with our LIVE Inline editor feature to send best emails for maximum engagement. This is all built to attract, capture, nurture and convert your potential prospect. 
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize ">
                  <img src="../common_assets/images/10.webp" class="img-responsive size">
                     Reduce bounce rate 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Higher bounce spoils your image and get murky with every bounce. With MailerGPT, you can get rid of all the bounced and spammed mails. MailerGPT automatically remove mails that were counted as bounce and make your list clear without any grunt work. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f10.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/11.webp" class="img-responsive size">
                     MailerGPT is 100% CAN SPAM Compliant - 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     It provide one-click unsubscribe feature that is user-friendly and helps to greatly reduce spam complaints and built your better send reputation. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f11.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/12.webp" class="img-responsive size">
               Manage your subscribers hassle-free
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               MailerGPT offers you the easiest way to find, filter or clean your subscribers in never-ending lists. You can find out a subscriber out of a list of thousands with just 1 click. 
                        <br><br>
                  Track duplicate entries and create a backup of your list in no time with MailerGPT
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/13.webp" class="img-responsive size">
                     All-in-one cloud based email marketing software -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  MailerGPT is built on the idea to deliver maximum quality, ease and efficiency. And to make it simpler we made it a cloud based platform. 
                  <br><br>
                  Guys you will be thrilled by numbers of features that we are offering to you to make your email marketing simple and fun. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f13.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/14.webp" class="img-responsive size">
                     Boost sales and increase revenue -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Email marketing is taking the world by storm and if you are searching the right way to send best emails to attract more customers, MailerGPT is the best among rest
                     <br><br>
                     You could send a free whitepaper, more information on your products, "subscriber-only" discount, or even personize mails to contend your customers for taking action. Your imagination and creativity really are the only limits and you have endless opportunities to get success. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f14.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">


         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/15.webp" class="img-responsive size">
               Personalize your mails to get high opening rates 
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="f-20 lh140 black-clr w400 mt30 mt-md60 ">
                     Now this is something that will really prove to be of great worth for your marketing efforts. Personalization of emails is the best and easiest way to get attention of your subscribers, with MailerGPT, you can personalize your every email to every subscriber to get high open rates. 
                  </div>
            </div>



            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/16.webp" class="img-responsive size">
                     Create long-term relationships with your subscribers with beautiful newsletters -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Always try to keep in touch with your subscribers to create a strong relationship with them. You can send newsletters to update them about your products or upcoming launches. 
                     <br><br>
                     You can redirect them on your blog or website to keep your brand in their mind. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f16.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/17.webp" class="img-responsive size">
                     100% newbie friendly and fully automated
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  Effective email marketing is all about sending right email to right people at right time and our software team has got everything covered and made it no hands software.
                        <br><br>
                  With the robust features of MailerGPT, you have the complete freedom to automate your email marketing. The software delivers automated, one-to-one messages across all of your marketing channels and even a newbie can manage his email marketing campaign without any hassle.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f17.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/18.webp" class="img-responsive size">
                  No monthly fees or additional charges
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                        If you are a newbie and starting out with email marketing, then sending mails can be a costly affair. They charge you like wildfire and you have to provide a heavy monthly fee just to reach out to your email list.
                        <br><br>
                        But MailerGPT is hands down the best email marketing software available today that charges no recurring fee and allows you to send unlimited emails with just one click.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f18.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/19.webp" class="img-responsive size">
                  Brand new system- Absolutely NO rehashes
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  We always believe in giving you something that's packed with latest features and which is simply not an add-on to a pre-existing product.
                        <br><br>
                        So, MailerGPT is packed with great features and it's the ultimate email marketing technology that's never been seen before.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f19.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/20.webp" class="img-responsive size">
                  Designed by Marketers for Marketers
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  MailerGPT has been built from the ground up to be A-Z marketer-friendly, meaning you can upload your list of subscribers straight into the software with no technical hassles, and get best results without any complications.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f20.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/21.webp" class="img-responsive size">
                  Step-By-Step Training to Make Everything Easy For You...
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  Yep, we know software can get complex. And while MailerGPT is DEAD easy to use, we wanted to make 100% sure it's accessible to everyone and everyone can earn with it. That's why we did 2 things:
                        <br><br>
                        #1 We've added in-depth video training for every feature so you can always look at the RIGHT way to do things
                        <br><br>
                        #2 We're also offering 24*7 on-going support so you're always just a message away from having your problem solved.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f21.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      

      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               And Here's What Some More Marketers <br class="d-none d-md-block"> Says About MailerGPT
               </div>
              
               <div class="mt20 mt-md50">
                  <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
               </div>
            </div>
            <div class="row mt30">
               
            
               
           
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                          Importing Lists without losing any Leads? DONE! Sending UNLIMITED Emails? DONE! Want to Generate More Leads? DONE! Automating Email Marketing Campaigns? That's DONE as well! <br>
                        This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing...
                        
                   </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi3.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Rodney Paul 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     MailerGPT will surely take the industry by storm and will help marketers to generate more leads from any Blog, eCommerce, or WordPress site. I like its latest smart tag feature which enables me to build targeted marketing campaigns. Yet another killer product from Team Pranshu & Bizomart Apps...
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi4.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Alex Holland
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                       BINGO, I've tested MailerGPT for myself, and man, it delivered exactly as promised. I just recently sent mail to my entire list and got 20% more open rates with this super-amazing Email Marketing Technology. Two-Thumbs up for this one…
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi5.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Tom Garfield 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     I always struggle to find the time to write the perfect email. That's where MailerGPT comes in - this AI-powered app has completely changed My Way of Writing Mail! It helps me write professional and Attractive emails that truly Interact with my audience.
                      With MailerGPT, I never have to worry about the stress of crafting the perfect message .</div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi6.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Andrew Hardy 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
         </div>
      </div>
   </div> 
    
      
    <section class="comp-table">
         <div class="comparetable-section ">
            <div class="container ">
               <div class="row ">
                  <div class="f-md-45 f-28 w700 lh140 black-clr text-center">
                    There is Nothing Like MailerGPT in The Market
                  </div>
                  <!--<div class="col-12">-->
                  <!--   <div class="f-md-45 f-28 w700 lh140 black-clr text-center ">-->
                  <!--      No Other Software Even Comes Close!-->
                  <!--   </div>-->
                  <!--</div>-->
                  <div class="col-12 mt10 d-none d-md-block">
                     
                     <div class="row g-0 mt70 mt-md100">
                        <div class="col-md-2 col-2">
                           <div class="fist-row">
                              <ul class="f-md-18 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700 justify-content-start justify-content-md-center">Features</li>
                                 <li class="f-md-32 w600">No. of contacts </li>
                                 <li>Ai Writer</li>
                                 <li>Smart Tagging </li>
                                 <li>Send Unlimited Mails </li>
                                 <li>SMTP Integration </li>
                                 <li>Automation</li>
                                 <li>ChatGPT Enabled </li>
                                 <li>One Time Pay </li>
                                 <li>Ai Graphics </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="second-row">
                              <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                                 <li class="f-md-20 f-16 white-clr">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                                 </li>
                                 <li class="f-md-24 w600" style="line-height:36px;"> Unlimited </li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li class="w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>GetResponse</span></li>
                                 <li class="f-md-24 w600">Unlimited </li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Aweber</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Convertkit</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                        <div class="third-row">
                        <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>SendinBlue</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                        </div>
                        </div>
                     </div>
                     <div class="mt-md30 mt20 f-16 f-md-18 w400 lh140 "><span class="w700 ">Note :</span> All the features mentioned in the above table are bifurcated in different upgrade options according to the need of individual users and the features that you will get with purchase of MailerGPT Start
                        or Pro Commercial plan are mentioned in the pricing table below on this page.
                     </div>
                  </div>
                  <div class="col-12">
                     <img src="assets/images/comp-table.webp" alt="comptable" class="mx-auto d-block img-fluid d-md-none">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="revolution-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-42 w700 lh140 white-clr">
                  MailerGPT Is The Revolution In Email Marketing World
                  </div>
                  <div class="mt10">
                     <img src="assets/images/revolution-line.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="mt20 mt-md30 white-clr f-20 f-md-24 w400 lh140">
                    That is going to change the future of email marketing, writing & designing for “Better”.
                  </div>
                  <div class="mt20 mt-md80">
                     <img src="assets/images/mailgpt-img.webp" alt="" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
         </div>
      </section>

      
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="unlock-opportunities-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="black-clr w700 f-28 f-md-35 unlimited-head">
                     <div class="unlimited-blub">
                        With MailerGPT, Tap into Endless Earning Opportunities...
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/op3.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Promote Any Product as An Affiliate from Warrior+, JVZoo, ClickBank
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op5.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Create And Sell Your Own Courses, E-Book, or Digital Products
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op9.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Earn More with Cross-Selling Similar Product/Services with Emails
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op2.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Start Email Marketing Agency & Charge Customers for Your Services
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op8.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                    	Offer Paid Newsletter Services by Charging Subscription Fee 
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op7.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Craft Personalized, Engaging, High Converting Email Swipes and Sell Them
                  </div>
               </div>
               
            </div>
         </div>
      </section>

    
      <div class="benefit-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr">So, Who Can Benefit From MailerGPT? </div>
               </div>
            </div>
            <div class="row row-cols-1 gap30 mt0 mt-md20">
               <div class="col">
                  
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b1.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        	Any Internet Marketer
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                        	Regardless of The Niche. MailerGPT Helps Them Boost Inboxing, Clicks & Sales.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b2.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        		List Builders Skyrocket
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                        		Their Subscribers' Numbers... All At The Push Of A Button.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b3.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        	Anyone Who Wants To Streamline
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                        	Their Business While Focusing On Bigger Projects.
                     </div>
                  </div>
               </div>
            </div>
            
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b4.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        	Affiliate Marketers
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                        	Who Don't Have Any Product Or Service To Sell
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b5.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        	Product/Service/Course Sellers 
                     </div>
                     <div class="f-18 w400 lh140 grey-clr ">
                        	To Boost Their Sales & Customer Satisfaction
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b6.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                    	Online Marketing Agency 
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                    To Provide Services To Local Businesses & Build Income
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b7.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                    	Local Businesses
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                    	To Automate Their Client Communication.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b8.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                    	Lazy People
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                     Who Are Reluctant To Work
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b9.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-20 w600 lh140 grey-clr mt10">
                        Anyone Who Wants To Value
                     </div>
                     <div class="f-18 w400 lh140 grey-clr">
                       Their Business And Money 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      
      <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
     
      <!-- Cta Section End -->
    
    
     
      <section class="still-think">
         <div class="container">
            <div class="row">
            <div class="col-12 text-center">
                     <div class="f-28 f-md-38 w600 black-clr lh140">
                     Kiss Expensive & Complex Software Apps Goodbye Forever...
                     <br>
                     MailerGPT Replaces Them All (For Good)
                     </div>
                     
                  </div>
            <div class="letsee-block mt30 mt-md60">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-34 w600 yellow-clr lh140">
                        Just take a look of the total value you are getting with MailerGPT
                     </div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/yellow-line.webp" alt="Line" class="img-fluid mx-auto d-block mt10">
                  </div>
               </div>
               <div class="letsee-price-table mt20 mt-md40">
                  <div class="row">
                     <div class="col-8 col-md-10 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                     </div>
                     <div class="col-4 col-md-2">
                        <div class="f-28 f-md-45 w600 lh140 white-clr">
                           Price
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Content creation platform 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $597/yr
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Groundbreaking Email Marketing Technology 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $936/yr
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	100% Cloud-Based Technology 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	SMTP Mailing Service 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $2000/yr
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Schedule Emails Anytime with a push of a button 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Ai Enabled Smart Tagging & Segmenting 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-20 f-md-24 w500 white-clr lh140">
                        	List Management 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $47
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Automation and Autoresponder 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $1000/yr
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	AI Inbuilt Text & Inline Editor 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Stunning Optin Forms
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Ready-To-Use Email Templates 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $197
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	CAN SPAM compliant 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $47
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Commercial Use License 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                        $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Beginner Friendly 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           Priceless
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30 border-b">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                        	Fully Automated. 100% Hands-Free 
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           Priceless
                        </div>
                     </div>
                     <div class="col-12  mt20 mt-md30 text-center">
                        <div class="f-28 f-md-45 lh140 w500 white-clr">
                        Thats's a HUGE  <span class="w700 green-clr1">$5286 in Savings</span> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="newbie-headline">
         <div class="container ">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr">THE BEST PART - It's Entirely Newbie Friendly</div>
               </div>
            </div>
         </div>
      </div>
      <div class="newbie-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 lh160 w400 text-center text-md-start">               
                  If you are newbie &amp; have no or little knowledge of marketing! Then, the trickiest thing is to get 
                  maximum open rates for your mails and make more profits. But not anymore as MailerGPT follows
                  a proven formula &amp; works round the clock in the background and automates email marketing 
                  and takes care of your business even while you're busy with other tasks.
                  <br><br>
                  <span class="w600">And it will do a whole lot of things you haven't even thought were possible yet... 
                  All with ZERO technical skills and with ZERO headaches...</span>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/newbie.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <!-- <section class="comp-sec">
         <div class="container">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-55 f-28 w700 lh140 text-capitalize text-center black-clr">
                     Now Is the Time to Take Decision.
                  </div>
                  <div class="f-md-36 f-20 w500 lh140 text-capitalize text-center black-clr mt20">
                    Make Your Choice. Your Future...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 row-cols-1 row-cols-md-2">
                  <div class="col relative">
                     <div class="compare-red-wrap">
                        <div class="f-md-32 f-22 w700 lh140 text-center"> <u>Without MailerGPT</u> </div>
                        <br>
                       <div class="f-md-24 f-20 w600 lh140 text-center"> Keep struggling with multiple software - spend your hundreds of dollars and hours of time.</div>
                        <ul class="compare-red pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                           <li>	Keep Paying Big Amount To Expensive Email Writers...</li> 
                           <li>	Keep Paying Monthly To Hefty Autoresponders With Limited Email Sending Plans </li>
                           <li>	Get Sued For Violating Copyright Laws If Try To Copy Content...</li> 
                           <li>	Investing Countless Hours In Research And Information Gathering </li>
                           <li>	Continue Paying SMTP Providers For SMTP Service On Monthly Basis. </li>
                           <li>	Crafting Personalized Email Is Difficult...</li> 
                           <li>	Hiring Experts To Setup SMTP, Email Scheduling, & Writing Emails Cost Thousands. </li>
                           <li>	It's Quite Time-Consuming & Painful to Do All the Tasks Manually </li>
                           
                        </ul>                        
                     </div>
                  </div>
                  <div class="col mt20 mt-md0 relative">
                     <div class="compare-red-wrap1">
                        <div class="f-md-32 f-22 w700 lh140 text-center"> <u>With MailerGPT</u> </div> <br><br>
                         <div class="f-md-24 f-20 w600 lh140 text-center"> Pay Only One-Time for Automated Content Writing, Unlimited Mailing & Sales for Lifetime.</div>
                        <ul class="compare-red1 pl0 f-20 f-md-24 w500 lh160 black-clr text-capitalize mt20">
                           <li>	Get This Fully ChatGPT AI Driven Platform Paying One Time Only</li> 
                           <li>	Generate An Engaging Emails Using Just One Keyword</li> 
                           <li>	Send & Scheduled Unlimited Emails By Paying One Time Only</li> 
                           <li>	Get Fully Copyright Free Content and Can Be Used Anywhere </li>
                           <li>	Send Your Emails Using Inbuilt SMTP </li>
                           <li>	ChatGPT AI Craft Personalized Emails Easily In Few Seconds </li>
                           <li>	No Need To Hire Anyone. MailerGPT Is One Stop Solution For Email Marketing </li>
                           <li>	Enjoy You Time and Live Your Dream Laptop Lifestyle </li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
         </div>
      </section>
      
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
       <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      
       <div class="travel-section">
        
              <img src="assets/images/travel-section.png" class="img-fluid d-block mx-auto"></div>
             </div>
             
     <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
    <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div> -->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <img src="assets/images/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="f-18 f-md-32 w500 lh140 text-center">
                      
                     When You Grab Your MailerGPT Account Today, <br class="d-none d-md-block">You'll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Email Marketing DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Both online and offline marketers can make a killing using this up-to-date Email Marketing DFY Business. Our step-by-step Email Marketing exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever. </div>
                        <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                        </div>  
                        
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #2
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Progressive List Building DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                             Both online and offline marketers can make a killing using this up-to-date Progressive List Building DFY Business training. Our step-by-step Progressive List Building DFY Business exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever.
                            <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Affiliate Marketing Made Easy
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Marketers do not want to waste their money, time and effort just for not doing, and even worse, for not knowing how to do Affiliate Marketing!
<br><br>
Our step-by-step Affiliate Marketing Training System is going to take you and your customers by the hand and show you how to make as much money as possible, in the shortest time ever with Affiliate Marketing online. </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                            Auto Video Creator - Create your own professional videos!
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Uncover the secrets to create your own professional videos in minutes with this easy to use software. You don't even have to speak ... the software will do it for you!
Auto Video Creator - Create your own professional videos in a snap! </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
              
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w700 white-clr lh240 text-center">
                        That's A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w700">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
     
      <!-- License Section Start -->
      <div class="license-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
                     Also, Get Free Commercial License 
                  </div>
                  <div class="f-md-20 f-18 w400 lh140 black-clr mt20 text-center text-md-start">
                     MailerGPT comes with Commercial use license so that you can use MailerGPT for commercial purposes and sell services to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
    
      <!-- Cta Section End -->
        <section class="best-part-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr best-text  d-flex align-items-center justify-content-center">
                  This Is Your Chance To Turn Worries into<br class="d-none d-md-block"> Income Opportunity!
                  <img src="assets/images/not-believe-smile.webp" alt="" class="img-fluid d-block ml15">
                  </div>
                  <img src="assets/images/best-line.webp" alt="Best Line" class="mx-auto d-block img-fluid mt20">
                  <div class="mt20 mt-md30 lh140 f-22 f-md-28 w600">
                  Just 3 Clicks It Takes to Start Profiting with the Power of Artificial Intelligence… 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-md-6">
                  <ul class="f-18 f-md-20 lh140 black-clr best-list pl0 m0 w400">
                     <li><span class="w600">	No Need To Pay </span>To Expensive Content Creation Platforms...</li>
                     <li><span class="w600">	No More Wasting Time</span> With The Old-School Autoresponder Tools</li>
                     <li><span class="w600">	No More Feeling Bankrupt</span> Paying Money Month After Month To Email Marketing Service Providers That Cost A Fortune</li>
                     <li><span class="w600">	No More Losing Your List</span> While Importing So Don't Lose Money Instead Make More Profits</li>
                     <li><span class="w600">	No More Waiting Game </span>To Get Authentic Results</li>
                     <li><span class="w600">	No Limits on Sending Emails </span>So Send Emails As Often As You'd Like With NO Downtime.</li>
                     <li><span class="w600">	It's Very Easy To Setup</span> Regardless Of Prior Technical Experience.</li>
                     <li>	It's Your Chance to Take Control of Your Life - <span class="w600">MailerGPT Puts You on Driver Seat</span></li>
                     <li><span class="w600">	Finally Become Your Own BOSS</span></li>
                  </ul>
               </div>
               <div class="col-md-6">
                  <img src="assets/images/Say-good-bye.webp" alt="Say Good Bye" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="mt20 mt-md30 lh140 f-18 f-md-24 w300 text-center">
                <i> So, what are you waiting for? Click the button below to grab your copy of MailerGPT now… </i>
                  </div>
         </div>
      </section>
      
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
     
      <!-- Cta Section End -->
      <!-- Guarantee Section Start -->
     <div class="riskfree-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w400 lh140 white-clr text-center col-12 mb-md40">
                  We're Backing Everything Up with An Iron Clad... <br class="d-none d-md-block"><span class="w600 blue-gradient">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 white-clr mt15 ">
                     I'm 100% confident that MailerGPT will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
     <div class="table-section" id="buynow">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get MailerGPT for A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50 ">
                  <div class="row gx-5">
                     <div class="col-12 col-md-6">
                        <div class="table-wrap ">
                           <div class="table-head text-center ">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height:60px;">
                        <defs>
                            <style>
                                .cls-1a{fill:#152028;}
                                .cls-2a{fill:url(#radial-gradient);}
                                .cls-3a{fill:url(#radial-gradient-2);}
                                .cls-4a{fill:url(#radial-gradient-3);}
                            </style>
                            <radialGradient id="radial-gradient" cx="-3589.75" cy="-2350.54" r="86.7" gradientTransform="translate(9529.36 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop>
                            </radialGradient><radialGradient id="radial-gradient-2" cx="-3585.74" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient>
                            <radialGradient id="radial-gradient-3" cx="-3589.75" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1a" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path>
                                    <path class="cls-1a" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path>
                                    <path class="cls-1a" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path>
                                    <path class="cls-1a" d="M422.87,145.1h-7.71V46.39h7.71Z"></path>
                                    <path class="cls-1a" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path>
                                    <path class="cls-1a" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path>
                                    <path class="cls-1a" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path>
                                    <path class="cls-1a" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path>
                                    <path class="cls-1a" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path>
                                    <path class="cls-2a" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path>
                                    <path class="cls-3a" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path>
                                    <path class="cls-4a" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path>
                                </g>
                            </g>
                        </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 black-clr">START PLAN</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list pl0 f-18 lh140 w400 black-clr">                                
                                 <li>	World First ChatGPT AI Powered Email Marketing APP</li>
                                 <li>	Create 1K High Converting Emails with Just One keyword</li>
                                 <li>	 Send 10000 Mails Instantly using Inbuilt SMTP or Schedule Them for Later</li>
                                 <li>	Import 5000 Subscribers Without Even Losing A Single Email ID</li>
                                 <li>	100+ High Converting Email Templates </li>
                                 <li>	70+ Lead Generating WebForm Templates</li>
                                 <li>	Boost Email Delivery, Click And Open Rate With Engaging Emails</li>
                                 <li>	Send Beautiful Emails For Sales, Promotion, Product Delivery And Many More ...</li>
                                 <li>	AI Enabled Smart Tagging For Lead Personalization & Traffic</li>
                                 <li>	Collect upto 5K Leads With Built-In Lead Form</li>
                                 <li>	Design Newsletter & Autoresponder Mails Using The Power Of AI</li>
                                 <li>	Works Seamlessly with Almost Every SMTP server</li>
                               
                                 <li>	Inbuilt AI Text & Inline Editor To Craft Best Emails</li>
                                 <li>	Personalize Your Mails To Get High Opening Rates</li>
                                 <li>	Create Long-Term Relationships With Your Subscribers With Beautiful Newsletters</li>
                                 <li>	100% Control On Your Online Business</li>
                                 <li>	No Monthly Fee Forever. Pay One Time Only...</li>
                                 <li>	MailerGPT Is 100% GDPR & Can-Spam Compliant</li>
                                 <li>	Brand New System- Absolutely NO Rehashes</li>
                                 <li>	100% Newbie Friendly And Fully Automated</li>
                                 <li>	Step-By-Step Training To Make Everything Easy For You</li>
                                 
                                 <li class="cross-sign">Commercial License</li>
                                 <li class="cross-sign">Use For Your Clients</li>
                                 <li class="cross-sign">Provide High In Demand Services</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button"> 
                                 <a href="https://www.jvzoo.com/b/108463/394279/2"><img src="https://i.jvzoo.com/108463/394279/2" alt="MailerGPT Personal" border="0"  class="img-fluid d-block"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <div class="table-wrap1">
                           <div class="table-head1">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 white-clr">COMMERCIAL PLAN</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 lh140 w400 black-clr">                                 
                              <li>	World First ChatGPT AI Powered Email Marketing APP</li>
                              <li>	Create Unlimited High Converting Emails with Just One keyword</li>
                              <li>	 Send Unlimited Mails Instantly using Inbuilt SMTP or Schedule Them for Later</li>
                              <li>	Import Unlimited Subscribers Without Even Losing A Single Email ID</li>
                              <li>	100+ High Converting Email Templates </li>
                              <li>	70+ Lead Generating WebForm Templates</li>
                              <li>	Boost Email Delivery, Click And Open Rate With Engaging Emails</li>
                              <li>	Send Beautiful Emails For Sales, Promotion, Product Delivery And Many More ...</li>
                              <li>	AI Enabled Smart Tagging For Lead Personalization & Traffic</li>
                              <li>	Collect upto 10K Leads With Built-In Lead Form</li>
                              <li>	Design Newsletter & Autoresponder Mails Using The Power Of AI</li>
                              <li>	Works Seamlessly with Almost Every SMTP server</li>
                             
                              <li>	Inbuilt AI Text & Inline Editor To Craft Best Emails</li>
                              <li>	Personalize Your Mails To Get High Opening Rates</li>
                              <li>	Create Long-Term Relationships With Your Subscribers With Beautiful Newsletters</li>
                              <li>	100% Control On Your Online Business</li>
                              <li>	No Monthly Fee Forever. Pay One Time Only...</li>
                              <li>	MailerGPT Is 100% GDPR & Can-Spam Compliant</li>
                              <li>	Brand New System- Absolutely NO Rehashes</li>
                              <li>	100% Newbie Friendly And Fully Automated</li>
                              <li>	Step-By-Step Training To Make Everything Easy For You</li>
                            
                              <li>  Commercial License</li>
                              <li>  Use For Your Clients</li>
                              <li>  Provide High In Demand Services</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a  href="https://www.jvzoo.com/b/108463/394283/2"><img src="https://i.jvzoo.com/108463/394283/2" alt="MailerGPT Commercial" border="0"  class="img-fluid d-block mx-auto"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <!-- Discounted Price Section End -->
      <!-- To your awesome section -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh140 white-clr text-xs-center">
                     <span class="w700">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally help you build super engaging video channels in the best possible way - without wasting tons of money!
                     <br><br>  So, take action now... and I promise you won't be disappointed!
                  </div>
               </div>
              
               <div class="col-12 w700 f-md-28 f-22 text-start mt20 mt-md70 red-clr">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <div class="row">
                        
                        <div class="col-md-4 col-12 text-center mt30 mt-md0">
                           <img src="assets/images/brett.webp" class="img-fluid d-block mx-auto " alt="Brett Ingram" style="max-height: 200px;">
                           <div class="f-24 f-md-24 w700 lh140 white-clr mt20">
                           Brett Ingram
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <img src="assets/images/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta" style="max-height: 200px;"> 
                           <div class="f-24 f-md-24 w700 lh140 text-center white-clr mt20">
                              Pranshu Gupta
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt30 mt-md0">
                           <img src="assets/images/bizomart.webp" class="img-fluid d-block mx-auto " alt="Bizomart" style="max-height: 200px;">
                           <div class="f-24 f-md-24 w700 lh140 white-clr mt20">
                             Bizomart Apps
                           </div>
                        </div>
                     </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh140 white-clr">
                     <span class="w700">P.S- You can try "MAILERGPT" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.

                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping video channels packed with video content and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w700">P.S.S Don't Procrastinate - Take Action NOW! Get your copy of MAILERGPT!  
                     </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- To your awesome section End -->
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
    
      <!-- Cta Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 lh150 text-center black-clr">
                  Frequently Asked <span class="orange-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What exactly MailerGPT is all about? 
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              MailerGPT is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              MailerGPT is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I have to install MailerGPT?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! MailerGPT is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that
                              has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is it ‘Newbie Friendly'?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yep, my friend, MailerGPT is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NOT AT ALL. There are NO monthly fees to use MailerGPT during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is MailerGPT easy to use?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              I bet you it's the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely
                              profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there's a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, that depends on how well you make the use of this ultimate software. We've created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member's area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-26 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row">
               <div class="col-12">
                  <div class="final-block">
                     <div class="text-center">
                        <div class="f-45 f-md-45 lh140 w700 text-center black-clr red-shape2">
                           FINAL CALL
                        </div>
                     </div>
                     <div class="col-12 f-22 f-md-28 lh140 w500 text-center white-clr mt20 p0">
                        You Still Have the Opportunity To Raise Your Game… <br class="d-none d-md-block">
                        Remember, It's Your Make-or-Break Time!
                     </div>
                     <!-- CTA Btn Section Start -->
                     <div class="col-12 mt40 mt-md40">
                        <div class="f-22 f-md-26 w500 lh140 text-center white-clr">
                           Grab MailerGPT Now for <strike>$47/Month</strike>, Today Only 1-Time $27 
                           </div>
                        <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                           Use Coupon Code <span class="blue-gradient w700"> Mail3 </span> for an Addition <span class="blue-gradient w700"> $3 Discount </span>
                        </div>
                        <div class="row">
                           <div class="col-md-10 mx-auto col-12 mt20 text-center">
                              <a href="https://www.jvzoo.com/b/108463/394283/2" class="cta-link-btn">Get Instant Access To MailerGPT</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                           <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                        </div>
                     </div>
                     <!-- CTA Btn Section End -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#radial-gradient);}.cls-3{fill:url(#radial-gradient-2);}.cls-4{fill:url(#radial-gradient-3);}</style><radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#18e6ff"></stop><stop offset="0.99" stop-color="#005fed"></stop></radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient></defs><title>MailerGPT Logo White0</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path><path class="cls-1" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path><path class="cls-1" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path><path class="cls-1" d="M422.87,145.1h-7.71V46.39h7.71Z"></path><path class="cls-1" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path><path class="cls-1" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path><path class="cls-1" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path><path class="cls-1" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path><path class="cls-1" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path><path class="cls-2" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path><path class="cls-3" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path><path class="cls-4" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path></g></g></svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color: #ffffff;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                  <!-- <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/d8j629" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                  </div> -->
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © MailerGPT 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailergpt.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>
      <!-- Exit Popup and Timer End -->

      <script>
         const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");

const textArray = ["Emails & Messages" , "Newsletter", "Autoresponder"];
const typingDelay = 100;
const erasingDelay = 80;
const newTextDelay = 1000; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
  if (charIndex < textArray[textArrayIndex].length) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
    charIndex++;
    setTimeout(type, typingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
  	setTimeout(erase, newTextDelay);
  }
}

function erase() {
	if (charIndex > 0) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
    charIndex--;
    setTimeout(erase, erasingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
    textArrayIndex++;
    if(textArrayIndex>=textArray.length) textArrayIndex=0;
    setTimeout(type, typingDelay + 300);
  }
}

document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
  if(textArray.length) setTimeout(type, newTextDelay + 250);
});
      </script>
   </body>
</html>

<!Doctype html>
<html>
   <head>
      <title>Sellero | Bundle Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <!------Meta Tags-------->
      <meta name="title" content="Sellero Exclusive Bundle Deal">
      <meta name="description" content="GET Sellero With All The Upgrades For 85% Off & Save Over $1697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta name="keywords" content="Sellero">
      <meta property="og:image" content="https://www.sellero.co/bundle/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Sellero Exclusive Bundle Deal">
      <meta property="og:description" content="GET Sellero With All The Upgrades For 85% Off & Save Over $1697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta property="og:image" content="https://www.sellero.co/bundle/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Sellero Exclusive Bundle Deal">
      <meta property="twitter:description" content="GET Sellero With All The Upgrades For 85% Off & Save Over $1697 When You Grab This Highly-Discounted Bundle Deal... ">
      <meta property="twitter:image" content="https://www.sellero.co/bundle/thumbnail.png">
      <!------Meta Tags-------->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script type='text/javascript' src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <script src="assets/js/timer.js"></script>
     
  <!-- New Timer  Start-->
  <?php
  $date = 'June 13 2023 11:00 AM EST';
  $exp_date = strtotime($date);
  $now = time();  
 
  if ($now < $exp_date) {
  ?>
<?php
  } else {
     echo "Times Up";
  }
  ?>
<!-- New Timer End -->


  <style>
     .timer-header-top{background: #000; padding-top:10px; padding-bottom:10px;}
     .tht-left{color:#fff;  font-size: 20px; font-weight:700}
  </style>  
</head>
<body>

<div class="timer-header-top fixed-top">
  <div class="container">
     <div class="row align-items-center">
        <div class="col-12 col-md-3">
           <div class="tht-left f-14 f-md-16">
              Use Coupon Code <span class="green-clr">"SELLERO"</span>  for <span class="green-clr"> $50 Discount </span> On One Time Deal
           </div>
        </div>
        <div class="col-12 col-md-6">
           <div class="countdown counter-white text-center">
              <div class="timer-label text-center"><span class="f-31 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
              <div class="timer-label text-center"><span class="f-31 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
              <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
              <div class="timer-label text-center "><span class="f-31 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
           </div>
        </div>
        <div class="col-12 col-md-3 text-center text-md-right">
           <div class="affiliate-link-btn"><a href="#buybundle" class="t-decoration-none">Buy Now</a></div>
        </div>
     </div>
  </div>
</div>
   </head>
   <body>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                  <defs>
                    <style>
                      .cls-1 {fill: #fff;}
                      .cls-2 {fill: #1fe29f;}
                      .cls-3 {fill: #7764ff;}
                    </style>
                  </defs>
                  <g id="Layer_1-2" data-name="Layer 1">
                    <g>
                      <g>
                        <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                        <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                        <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                        <g>
                          <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                          <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                        </g>
                      </g>
                      <g>
                        <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                        <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                        <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                        <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                        <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                        <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                        <g>
                          <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                          <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                          <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                        </g>
                      </g>
                    </g>
                  </g>
               </svg>
            </div>
            <div class="row">
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                     <u class="w600 red-clr"> WARNING </u>:  &nbsp; THIS LIMITED TIME DISCOUNTED BUNDLE OFFER EXPIRES SOON 
                  </div>
               </div>
               <div class="col-12 mt-md80 mt50 head-design relative">
                  <div class="gametext">
                     CUTTING EDGE TECHNOLOGY 
                   </div>
                  <div class=" f-md-44 f-28 w500 text-center black-clr lh140">
                     Launch & Profit with Your Digital Downloads, Courses, Services or Any Purchased Agency/PLR/Reseller Right Products <span class="w700">in Next 7 Minutes Flat....</span> 
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                  <div class="f-22 f-md-26 w500 lh140 white-clr text-capitalize">               
                     GET Sellero With All The Upgrades For 85% Off & Save Over $1697 When You Grab This Highly-Discounted Bundle Deal... 
                 </div>
               </div>
               

               <div class="col-12 col-md-10 mt20 mt-md50 mx-auto">
                 <!-- <div class="responsive-video">
                      <iframe src="https://yodrive.dotcompal.com/video/embed/wfnsl2hhbg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div> -->
                  <img src="assets/images/pbb-img.webp" class="img-fluid d-block mx-auto img-shadow" alt="ProductBox">
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!---new Section Start---->
      <div class="new-section content-mbl-space">
         <div class="container">
             <div class="row">
                 <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                     Save $1697 RIGHT AWAY-
                     <div class="gradient-clr w700 f-md-70 f-40">
                         Deal Ends Soon
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>GET Complete Sellero Package (FE + ALL Upgrades + Agency License) </li>
                          <li>No Monthly Payment Hassles- It's Just A One-Time Payment</li>
                          <li>GET Priority Support from Our Dedicated Support Engineers</li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>Provide Top-Notch Services to Your Clients</li>
                          <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                          <li>GET 30-Days Money Back Guarantee</li>
                       </ul>
                    </div>
                 </div>
              </div>
             <div class="row">
                 <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                     <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                         <img src="assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                         <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                     </div>
                     <div>
                         Use Coupon Code <span class="w600 green-clr">"SELLERO"</span> for an Additional <span class="w600 green-clr">$50 Discount</span> 
                     </div>
                     <div class="col-md-8 col-12 instant-btn mt-md30 mt20 f-24 f-md-30 mx-auto px0">
                         <a href="#buybundle">
                      GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                      </a>
                     </div>
                     <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                         No Download or Installation Required
                     </div>
                     <div class="col-12 mt20 mt-md20">
                         <img src="assets/images/compaitable-with.png" class="img-fluid mx-auto d-block">
                      </div>
                 </div>
             </div>
         </div>
     </div>
      <!--1. New Section End -->
      <!--2. Second Section Start -->
      <div class=" section1">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Here's What You Get With <span class=" w600">Limited Time <br class="d-none d-md-block"> Sellero Discounted</span> Bundle Today
                  </div>
               </div>
               <!-- AgencyBUNDLE START -->
               <div class="col-12 mt20 mt-md70 section-margin px-md-0">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                        <div class="pimg">
                           <img src="assets/images/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Sellero Commercial ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Tap into the <span class="w600">Super-Hot $25 Trillion E-Selling Industry</span> </li>
                                 <li>Complete <span class="w600"> All-in-One Online Selling Platform  </span> for Entrepreneurs, Marketers, and Newbies </li>
                                 <li><span class="w600"> Quick Start  </span>  Your Online Selling Business with <span class="w600"> 20+ Done-For-You Products</span></li>
                                 <li><span class="w600"> Easily Create and Manage Unlimited Products -  </span> Digital Products, Courses, Services, and Goods </li>
                                 <li>Create  <span class="w600">Membership Sites to Deliver Products  </span> in Secured & Password Protected Member's Area </li>
                                 <li><span class="w600">Create </span> Unlimited Beautiful, Mobile Ready and Fast-Loading <span class="w600"> Landing Pages Easily</span></li>
                                 <li><span class="w600">	Use Smart-Checkout Links   </span> to Directly <span class="w600">Receive Payments </span> from Social Media, Emails & On Any Page.</li>
                                 <li><span class="w600">	Precise Analytics  </span>  to Measure the Performance and Know Exactly What's Working and What's Not</li>
                                 <li><span class="w600">SEO Friendly & Social Media Optimized  </span> Pages for More Traffic</li>
                                 <li><span class="w600">100% GDPR and Can-Spam   </span> Compliant</li>
                                <li><span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Launch Fast - Create Stunning  <span class="w600">Websites & Stores  </span> for Your and Your Client’s Business in Any Niche </li>
                                 <li>	Sell Unlimited Products and Accept  <span class="w600">Payments Through PayPal and Stripe</span> with Zero Fee, you can Keep 100% of your Profit </li>
                                 <li>	Selling on ClickBank, JVZoo & Warrior Plus? Get <span class="w600">Seamless Integration </span> to Deliver Products on Automation. </li>
                                 <li><span class="w600">400+ Battle-Tested, </span> Beautiful, and Mobile-Ready  <span class="w600"> Page Templates</span>  </li>
                                 <li>Fully Customizable  <span class="w600">Drag & Drop WYSIWYG Page Editor  </span> that Requires Zero Designing or Tech Skills </li>
                                 <li><span class="w600"> Inbuilt Lead Management System </span> for Effective Contacts Management in Automation
                                 <li>Connect Sellero with your Favorite Tools, <span class="w600">20+ Integrations </span> with Autoresponders and other Services. </li>
                                 <li> <span class="w600"> 128 Bit Secured, SSL Encryption </span>for Maximum Security of Your Files, Data, and Websites </li>
                                 <li> Manage all the Pages, Products & Customers Hassle-Free, <span class="w600">All in Single Dashboard. </span>  </li>
                                 <li><span class="w600">Step-By-Step Video Training </span> and Tutorials Included </li>
                                 <li><span class="w600">24*5 Customer Support</span>  </li>
                              </ul>
                           </div>
                        </div>

                        <div class="col-12 col-md-12 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md20">
                              <ul class="kaptick pl0 p0 m0">
                                 <li><span class="w600">Plus, you'll also Receive Agency to Start Selling Pro Level Services to your Clients Right Away</span>  </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- AGENCY BUNDLE END -->
               <!-- ELITE BUNDLE START -->
               <div class="col-12 px-md0 mt40 mt-md140 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 1- Sellero Elite ($297)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li> <span class="w600"> Break Free & Go Limitless- </span> No Limits On What You Create & Make Profits With Sellero Elite.</li>
                                 <li> Run All Your Pages On Your Own Domains <span class="w600"> With Unlimited Custom Domains </span> </li>
                                 <li> Create <span class="w600">Unlimited Websites, Stores, And Membership</span> Levels Like A Pro.</li>
                                 <li> Collect  <span class="w600">Unlimited Leads</span> & Make The Most From Them Without Any Restrictions</li>
                                 <li> <span class="w600">Get 100+ More Mobile Responsive & Ready-To-Go Landing Page Templates </span> </li>
                                 <li> <span class="w600"> Unlimited A/B Testing For Landing Pages </span> To Choose The Best Performer </li>
                                 <li> Play Your Videos On Landing Pages Using <span class="w600"> Ultra-Light & Attractive HLS Video Player </span></li>
                                 <li><span class="w600"> Easily Revert To The Last Published Version </span> Of Your Pages In Case You Want To Scrape</li>
                                 <li> <span class="w600"> Create Unlimited Businesses/Subdomains </span>To Keep Each Of Your Own And Your Client's Business Project Separate</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                           <ul class="kaptick pl0 p0 m0">   
                              <li> <span class="w600"> Serve Unlimited Clients In Any Niche </span> Without Any Restrictions </li>
                              <li><span class="w600"> Get Unlimited Bandwidth To Give Best User Experience </span></li>
                              <li><span class="w600"> Use Unlimited Smart Cart Links To Get Unlimited Sales </span> For You & Your Clients With No Restrictions Ever</li>
                              <li><span class="w600">Use Our Advanced Editor To Give A Customized Look And Feel To Your Websites </span> & Get Maximum Audience Hooked.</span></li>
                              <li><span class="w600">Advanced Analytics </span> To Have Clear Insight Of What's Working & What's Not To Boost ROI </li>
                              <li><span class="w600">Duplicate Pages In Between Businesses & Projects </span> And Save Templates For Further Use </li>
                              <li><span class="w600">Register Leads Directly To Your Next Webinars </span> & Convert More Sales With Webinar Integrations</li>
                              <li> Get All These Benefits At An <span class="w600">Unparalleled Price </span></li>
                           </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ELITE BUNDLE END -->
               <!-- ENTERPRISE BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 2- Sellero Enterprise ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>
                                    <span class="w600">Create Unlimited Email Mailing Lists </span> To Segment Contacts Hands Free.
                                 </li>
                                 <li>
                                    <span class="w600">Send Unlimited Emails To Unlimited Subscribers-</span>
                                    Build Customer Relations, Promote Unlimited Offers.
                                 </li>
                                 <li>
                                    <span class="w600">Unlimited Advanced Follow-Up Emails </span> Journey With Exclusive Automation Technology
                                 </li>
                                 <li>
                                    <span class="w600">Advanced Analytics </span> For Boosting Your Email Campaigns ROI
                                 </li>
                                 <li>
                                    <span class="w600">Get 50 EXTRA Beautiful, Mobile-Friendly And Ready-To-Use </span> Lead Pages & Popup Templates
                                 </li>
                                 <li>
                                    <span class="w600">List Cleaning And List Checking </span>  Included At No Extra Cost!
                                 </li>
                                 
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li><span class="w600">Use Smart Tags To Segment </span> Your Subscribers & Send Exclusive Emails</li>
                                 <li> <span class="w600">Advanced SPAM Checker -</span> Makes Sure Your Emails Get Delivered</li>
                                 <li> <span class="w600">Personalization Options</span> To Boost Open Rates</li>
                                 <li><span class="w600">Remove Our Branding From Your Lead Pages & Popups - </span> Present Yourself As An AUTHORITY </li>
                                 <li><span class="w600">Add 10 Team Members</span> (Inhouse Or Freelancers)</li>
                                 <li><span class="w600">Unlimited Work Collaboration</span> For Faster & Better Results</li>
                                 <li><span class="w600">Team Members Analysis & Activity Monitoring</span></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ENTERPRISE BUNDLE END -->
               <!-- BUSINESS DRIVE BUNDLE START -->
               <div class="col-12 px-md0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/bizdrive.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class=" col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 3- Sellero Agency($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-18 f-md-20 lh140 w400 mt40 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Agency License To Serve Unlimited Clients & Add Unlimited Team Members</li>
                                 <li>Create Unlimited Custom Domains/Subdomains/Businesses</li>
                                 <li>Biz Management Panel</li>
                                 <li>Remove & Add Your Own Branding On The Player</li>
                              </ul>
                          </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-18 f-md-20 lh140 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                  <li>Accurate Analysis for Team Member’s Activities For Effective Monitoring</li>
                                  <li>Support To You & Your Clients - Chat Support Directly From Software</li>
                                  <li>Subscription Management System to Manage Your Clients' Plans</li>
                              </ul>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- BUSINESS DRIVE BUNDLE END -->
               <div class="col-12 col-md-12 text-center mt20 mt-md50">
                  <div class="bonus-design f-22 f-md-38 w600 lh140 text-center white-clr">
                     Additional Upgrades
                  </div>
              </div>
               <div class="col-12 px-md0 mt20 mt-md120 section-margin">
                  <div class="col-12 plan2-shape mt0 mt-md50">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/writerarc.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Additional Upgrade 1- WriterArc ($297) 
                              </div>
                           </div>
                           <div class="f-22 f-md-28 w600 orange-clr lh150 text-center mt10">
                              Included In Your Sellero Purchase! 
                           </div>
                        </div>
                          <div class="col-12 col-md-6 px0 px-md15">
                              <div class="f-18 f-md-20 lh140 w400 mt40 mt-md50">
                                  <ul class="kaptick pl0">
                                      <li> <span class="w700">A 10X Faster and Easier</span>  way to generate killer content effortlessly. </li>
                                      <li> <span class="w700">An A.I. writing assistant</span> that helps you create high-quality content in just a few seconds.</li>
                                      <li> <span class="w700">Powered by the state of art language A.I.</span>  to generate unique, original, and Search Engine Optimized content for almost any vertical.</li>
                                      <li> <span class="w700">50+ use case based templates</span> to choose from to cover all your content needs.</li>
                                      <li> <span class="w700">Choose from 30+ languages</span> to write in your own or other languages for your clients.</li>
                                      <li>Write every content with the right emotions through <span class="w700">22+ tones of voice.</span> </li>
                                  </ul>
                              </div>
                          </div>
                          <div class="col-12 col-md-6 px0 px-md15">
                              <div class="f-18 f-md-20 lh140 w400 mt20 mt-md50">
                                  <ul class="kaptick pl0">
                                      <li> <span class="w700">Loaded with 36+ predefined business niches</span> so you don't have to hassle with finding keywords and the right descriptions.</li>
                                      <li> <span class="w700">Uses standard copywriting features like AIDA and PAS </span> to provide top converting output.</li>
                                      <li> <span class="w700"> 100% cloud-based software</span> so you can access it from any in the world.</li>
                                      <li> <span class="w700">Fast, responsive, and 100% mobile friendly</span> to work seamlessly with any device.</li>
                                      <li> <span class="w700">Download all your projects in PDF and word formats.</span> </li>
                                      <li> <span class="w700">100% newbie friendly, need no prior skills, no installation, and no tech hassles.</span></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="col-12 col-md-12">
                              <div class="f-18 f-md-20 lh140 w400 mt20">
                                  <ul class="kaptick pl0">
                                      <li>Best In Industry Customer Support With A 
                                          <span class="w700"> 96% Customer Satisfaction Rate. </span> 
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              <div class="col-12 px-md0 mt20 mt-md70 section-margin">
               <div class="col-12 plan2-shape">
                  <div class="col-md-10 mx-auto mt-md0 mt20">
                     <div class="pimg">
                        <img src="assets/images/restrosuite.webp" class="img-fluid d-block mx-auto" alt="Agency">
                     </div>
                  </div>
                  <div class="row mt-md0 mt20 mx-0">
                     <div class=" col-12 p0 text-center mt-md50 mt20">
                        <div class="title-shape">
                           <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                              Additional Upgrade 2- RestroSuite ($347) 
                           </div>
                        </div>
                        <div class="f-22 f-md-28 w600 orange-clr lh150 text-center mt10">
                           Included In Your Sellero Purchase! 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 px0 px-md15">
                        <div class="f-20 f-md-20 lh140 w400 mt20 mt-md50">
                           <ul class="kaptick pl0">
                              <li><span class="w600">Create upto 100</span>  Businesses/Domains</li>
                              <li><span class="w600">Unlimited Visitors/Month</span> </li>
                              <li>Done For You Client-Getting <span class="w600">Fully Customizable Resturant Website</span> </li>
                              <li><span class="w600">QR Code Genrator</span>  to Genrate code for any website</li>
                              <li> <span class="w600">Bio-Link Genrator</span> to Genrate Short Description about you - upto 10+</li>
                              <li> <span class="w600">Book A Table</span>  feature</li>
                              <li> <span class="w600">Social sharing</span> feature</li>
                              <li>Manage your <span class="w600">leads &amp; traffic, &amp; Analyze &amp; Segmentation</span></li>
                              <li> <span class="w600">Custome Domain upto 10</span> </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 px0 px-md15">
                        <div class="f-20 f-md-20 lh140 w400 mt0 mt-md50">
                           <ul class="kaptick pl0 p0 m0">
                              <li> <span class="w600">Accept Payments Through Paypal &amp; Stripe</span>  with Zero Fees </li>
                              <li> <span class="w600">Generate &amp; Manage</span> upto 100,000 Leads  </li>
                              <li> <span class="w600">Customized Business Central</span>  Dashboard</li>
                              <li>Completely <span class="w600">Cloud-Based - No Domain, Hosting or Installation Required</span> </li>
                              <li> <span class="w600">Easy and Intuitive</span> To Use Software with Step by Step Video Training</li>
                              <li> <span class="w600">24*5 Customer Support</span> </li>
                              <li> <span class="w600">Commercial License Included</span> to Serve Your Clients and Provide Hign n Demand Services</li>
                           </ul>
                        </div>
                     </div>
                   </div>
               </div>
           </div>
           <div class="col-12 px-md0 mt20 mt-md70 section-margin">
            <div class="col-12 plan2-shape">
               <div class="col-md-10 mx-auto mt-md0 mt20">
                  <div class="pimg">
                     <img src="assets/images/linkpro.webp" class="img-fluid d-block mx-auto" alt="Agency">
                  </div>
               </div>
               <div class="row mt-md0 mt20 mx-0">
                  <div class=" col-12 p0 text-center mt-md50 mt20">
                     <div class="title-shape">
                        <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                           Additional Upgrade 3- LinkPro ($247)
                        </div>
                     </div>
                     <div class="f-22 f-md-28 w600 orange-clr lh150 text-center mt10">
                        Included In Your Sellero Purchase! 
                     </div>
                  </div>
                  <div class="col-12 col-md-6 px0 px-md15">
                     <div class="f-20 f-md-24 lh140 w400 mt20 mt-md50">
                        <ul class="kaptick pl0">
                           <li> <span class="w600">  Create upto 100 Businesses/Domains </span></li>
                           <li> <span class="w600">  Get 30,000 Page Visits/Month</span></li>
                           <li> <span class="w600">  Create Beautiful, Mobile-Friendly</span> &amp; Fast-Loading Bio Pages Easily</li>
                           <li> <span class="w600">  Proven Converting Smart Links</span> To Boost Sales &amp; Conversions</li>
                           <li> <span class="w600">  Shorten any Ugly, long, Suspicious URL</span> into Short with your own Branding</li>
                           <li> <span class="w600">  Generate &amp; Manage upto 10,000 Leads</span> In A Hassle Free Manner</li>
                           <li> <span class="w600">  Generate QR Code for Any Link</span></li>
                           <li> <span class="w600">  High Converting Done for You Bio-Pages</span></li>
                           <li> <span class="w600">  Smart Link Cloaking </span>for any Marketing URLs</li>
                           <li> <span class="w600">  Redirect any Link to New</span> Destination</li>
                           <li> <span class="w600">  Fully Drag &amp; Drop Advanced WYSIWYG</span> Page editor </li>
                           <li> <span class="w600">  Complete Analytics</span> to track &amp; Convert your Visitors in buyers</li>
                           <li> <span class="w600">  Manage multiple leads effortlessly</span> &amp; make the most from them with our more powerful lead management feature.</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 px0 px-md15">
                     <div class="f-20 f-md-24 lh140 w400 mt0 mt-md50">
                        <ul class="kaptick pl0 p0 m0">
                           <li> <span class="w600">  Mobile Friendly Fast Loading QR Code</span> &amp; Bio Pages To Catch All Mobile Visitors</li>
                               <li> <span class="w600">  Inbuilt SEO Management For Pages</span></li>
                               <li> <span class="w600">  Automatic SSL Encryption</span></li>
                               <li> <span class="w600">  Cutting Edge Integration with 20+ Autoresponders</span> to Send Emails to Your Subscribers</li>
                               <li> <span class="w600">  Customized Drag &amp; Drop</span> Business Central Dashboard</li>
                               <li> <span class="w600">  Complete Step-By-Step Video</span> Training &amp; Tutorials Included</li>
                               <li> <span class="w600">  No Coding, Design or Tech Skills Needed</span></li>
                               <li> <span class="w600">  100% Newbie Friendly &amp; Fully</span> Cloud Based Software</li>
                               <li> <span class="w600">  A-Z Complete Video Training</span></li>
                               <li> <span class="w600">  Live Chat - Customer Support </span></li>
                               <li> <span class="w600">  Commercial License included</span></li>
                               <li> <span class="w600">  Use For Your Clients</span></li>
                               <li> <span class="w600">  Provide High In Demand Services</span></li>
                        </ul>
                     </div>
                  </div>
                </div>
            </div>
        </div>
               <!-- FAST ACTION BONUSES BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="mx-auto mt-md0 mt20">
                        <div class="pimg ">
                           <img src="assets/images/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                        </div>
                     </div>
                     <div class="row mt20">
                        <div class="col-12 p0 mt-md50 mt20 text-center">
                           <div class="title-shape1">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                 And You're Also Getting These Premium Fast Action Bonuses Worth ($518)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Bonus #1 - Live Training - 0-10k a Month With Sellero Reloaded (First 1000 buyers only - $1000 Value) </li>
                                 <li>Bonus #2 - Private Access to Online Business VIP Club </li>
                                 <li>Bonus #3 - Video Training on How To Start Online Coaching Business</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Bonus #4 - Lead Generation Video Workshop</li>
                                 <li>Bonus #5 - Video Training on How To Make Money With Affiliate Marketing</li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                               <ul class="kaptick pl0">
                                   <li>Super Value Bonus #5 - Video Training To Monetizing Your Website </li>
                                   <li>Super Value Bonus #6 - Training To Start Affiliate Marketing </li>
                                   <li>Super Value Bonus #7 - Video Training On Viral Marketing </li>
                                   <li>Super Value Bonus #8 - Video Marketing Profit Kit </li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--3. Third Section Start -->
      <div class="section3" id="buybundle">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" >
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Grab This Exclusive BUNDLE DISCOUNT Today…
                  </div>
                  <div class="f-18 f-md-26 w400 lh150 text-center mt20 black-clr">
                     Use Coupon Code <span class="green-clr w600">"SELLERO"</span> for an Additional <span class="green-clr w600"> $50 Discount</span> 
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                              <defs>
                                <style>
                                  .cls-1 {fill: #fff;}
                                  .cls-2 {fill: #1fe29f;}
                                  .cls-3 {fill: #7764ff;}
                                </style>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                                    <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                                    <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                                    <g>
                                      <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                                      <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                                    <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                    <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                    <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                    <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                    <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                                    <g>
                                      <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                                      <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                                      <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                           </svg>
                        </div>
                        <div class="col-12 mt-md20 mt10 table-headline-shape">
                           <div class="w600 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                         <!-- PLAN 3 START -->
                         <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 WriterArc
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 RestroSuite 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $347
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 LinkPro 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $247
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $518
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$1997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w600 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col-12 px-md0 px15 mt20 w600 f-md-25 f-20 black-clr text-center">
                  One Time Fee
               </div>
               <div class="row justify-content-center mx-0 ">
                  <div class="myfeatures f-md-25 f-16 w400 text-center lh160 hideme">
                     <div class="hideme-button">
                        <a href="https://www.jvzoo.com/b/108915/396537/2"><img src="https://i.jvzoo.com/108915/396537/2" alt="Sellero Bundle" border="0" class="img-fluid d-block mx-auto"/></a> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!----Second Section---->
      <div class="second-header-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 lh140 text-center white-clr">
                     TOP Reasons Why You Can't Ignore <br class="d-none d-md-block">
                     <span class="f-md-50 f-24 w600 ">
                        Sellero Fast Pass Deal
                     </span>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md65">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #1
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get All The Benefits Of Sellero & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #2
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Regular Price For Sellero, All Upgrades & Bonuses Is $1997. You Are Saving $1697 Today When You Grab The Exclusive Bundle Deal Now at ONLY $297.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #3
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        This Limited Time Additional $50 Coupon Will Expires As Soon As Timer Hits Zero So Take Action Now.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #4
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get Priority Support From Our Dedicated Support Team to Get Assured Success.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----Second Section---->
      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 white-clr text-center col-12 mb-md40">
                  Test Drive EVERYTHING Sellero Has To Offer Risk Free For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-18 f-18 w400 lh150 white-clr mt15 ">
                     Your purchase is secured with our 30-day 100% money back guarantee.
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your customers risk free. If you face any issue or don't get the results you desire after using it, just raise a ticket on our support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     However, we want to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto " alt="Risk Free">
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section Start -->
      <div class="section2" id="buybundle">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" >
                  <div class="f-md-45 f-28 black-clr  text-center w600 lh140">
                     Limited Time Offer!
                  </div>
                  <div class="f-22 f-md-32 w600 lh150 text-center mt20 black-clr">
                     Get Complete Package of All <span class="w600">Sellero</span> Products<br class="d-md-block d-none"> for A Low One-Time Fee
                  </div>
               </div>
               <div class="f-18 f-md-26 w400 lh150 text-center mt20 black-clr">
                  Use Coupon Code <span class="green-clr w600">"SELLERO"</span> for an Additional <span class="green-clr w600"> $50 Discount</span> 
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                              <defs>
                                <style>
                                  .cls-1 {fill: #fff;}
                                  .cls-2 {fill: #1fe29f;}
                                  .cls-3 {fill: #7764ff;}
                                </style>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                                    <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                                    <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                                    <g>
                                      <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                                      <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                                    <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                    <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                    <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                    <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                    <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                                    <g>
                                      <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                                      <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                                      <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                           </svg>
                        </div>
                        <div class="col-12 mt-md20 mt10 table-headline-shape">
                           <div class="w600 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                         <!-- PLAN 3 START -->
                         <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 WriterArc
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 RestroSuite 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $347
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 LinkPro 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $247
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $518
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$1997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w600 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="row">
               <div class="col-12 px-md0 px15 mt20 w600 f-md-25 f-20 black-clr text-center">
                  One Time Fee
               </div>
               <div class="row justify-content-center mx-0 ">
                  <div class="myfeatures f-md-25 f-16 w400 text-center lh160 hideme">
                     <div class="hideme-button">
                        <a href="https://www.jvzoo.com/b/108915/396537/2"><img src="https://i.jvzoo.com/108915/396537/2" alt="Sellero Bundle" border="0" class="img-fluid d-block mx-auto"/></a> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="f-md-36 f-24 black-clr  text-center w600 lh140">
                     We are always here by your side <br class="d-none d-md-block">
                     <span class="red-clr">Get in touch with us…</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
       <!--Footer Section Start -->
       <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {fill: #fff;}
                         .cls-2 {fill: #1fe29f;}
                         .cls-3 {fill: #7764ff;}
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                           <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                           <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                           <g>
                             <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                             <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                           </g>
                         </g>
                         <g>
                           <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                           <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                           <g>
                             <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                             <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                             <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                  </svg>
                  <br><br><br>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-20 w400 lh150 white-clr text-xs-center">Copyright © Sellero 2023</div>
                  <ul class="footer-ul w400 f-16 f-md-20 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

       <!-- timer --->
   <script type="text/javascript">
      var noob = $('.countdown').length;
      var stimeInSecs;
      var tickers;

      function timerBegin(seconds) {     
         stimeInSecs = parseInt(seconds);           
         tickers = setInterval("showRemaining()", 1000);
      }

      function showRemaining() {

         var seconds = stimeInSecs;
            
         if (seconds > 0) {         
            stimeInSecs--;       
         } else {        
            clearInterval(tickers);       
            timerBegin(59 * 60); 
         }

         var days = Math.floor(seconds / 86400);
   
         seconds %= 86400;
         
         var hours = Math.floor(seconds / 3600);
         
         seconds %= 3600;
         
         var minutes = Math.floor(seconds / 60);
         
         seconds %= 60;


         if (days < 10) {
            days = "0" + days;
         }
         if (hours < 10) {
            hours = "0" + hours;
         }
         if (minutes < 10) {
            minutes = "0" + minutes;
         }
         if (seconds < 10) {
            seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');

         for (i = 0; i < noob; i++) {
            countdown[i].innerHTML = '';
         
            if (days) {
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
            }
         
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
            countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
            countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
      
      }


      timerBegin(59 * 60); 

   </script>
         <!--- timer end-->
   </body>
</html>
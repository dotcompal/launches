<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Sellero JV</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
      <meta name="title" content="Sellero By Dr. Amit Pareek & Er. Atul Kumar">
      <meta name="description" content="Launch & Profit with Your Digital Downloads, Courses, Services or Any Purchased Agency/PLR/Reseller Right Products in Next 7 Minutes Flat">
      <meta name="keywords" content="Sellero JV Invite">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.sellero.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Sellero By Dr. Amit Pareek & Er. Atul Kumar">
      <meta property="og:description" content="Launch & Profit with Your Digital Downloads, Courses, Services or Any Purchased Agency/PLR/Reseller Right Products in Next 7 Minutes Flat">
      <meta property="og:image" content="https://www.sellero.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Sellero By Dr. Amit Pareek & Er. Atul Kumar">
      <meta property="twitter:description" content="Launch & Profit with Your Digital Downloads, Courses, Services or Any Purchased Agency/PLR/Reseller Right Products in Next 7 Minutes Flat">
      <meta property="twitter:image" content="https://www.sellero.co/jv/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/jv/css/style.css" type="text/css">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/jv/css/timer.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <!-- New Timer  Start-->
      <?php
         $date = 'June 12 2023 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
   </head>
   <body>
      <a href="#" id="scroll" style="display: block;"><span></span></a>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-3 text-md-start text-center">
                  <div>
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                      
                            .cls-2 {
                              fill: #1fe29f;
                            }
                      
                            .cls-3 {
                              fill: #7764ff;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                              <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                              <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                              <g>
                                <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                                <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                              </g>
                            </g>
                            <g>
                              <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                              <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                              <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                              <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                              <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                              <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                              <g>
                                <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                                <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                                <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                              </g>
                            </g>
                          </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-md-9 mt15 mt-md0">
                  <ul class="leader-ul f-18 f-md-20 w400 white-clr text-md-end text-center">
                     <li>
                        <a href="https://docs.google.com/document/d/1etw4sXQPzMyL9NLFCcxcwZR9NQKFdZBN/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank" class="white-clr t-decoration-none">JV Docs</a>
                     </li>
                     <li>
                        <a href="https://docs.google.com/document/d/1Khuqb2rhHKvtDum6WMlrNsZIpdHnTvq9/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank" class="white-clr t-decoration-none">Swipe</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="#live-sec">Grab Your Affiliate Link</a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-12 text-center mt20 mt-md88">
                  <div class="pre-heading f-md-21 f-20 w500 lh140 black-clr">
                     After Back-to-Back 6-Figure Launches, We are Coming with Another WINNER Product...
                  </div>
               </div>
               <div class="col-12 mt-md80 mt20 head-design relative">
                  <div class="gametext d-none d-md-block">
                     CUTTING EDGE TECHNOLOGY
                   </div>
                  <div class=" f-md-45 f-28 w500 text-center black-clr lh140">
                     Launch & Profit with Your Digital Downloads, Courses, Services or Any Purchased Agency/PLR/Reseller Right Products <span class="w700">in Next 7 Minutes Flat....</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
                  <u class="w600">It Also Comes with 20+ Ready-To-Use Products</u> To Start Selling & Profiting Right Away.
               </div>
               
               <div class="col-12 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
                  No Tech Hassles. No Monthly Fee Ever... 
               </div>
               
               <div class="col-12 col-md-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-lg-8 col-md-6 col-12 min-md-video-width-left" style="z-index:99;">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://sellero.oppyo.com/video/embed/d6tgqbnhz3" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen title="JV Video"></iframe>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 col-12 min-md-video-width-right mt20 mt-md0">
                        <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/date.webp" alt="Date" class="mx-auto d-block img-fluid lazyload">
                        <!-- Timer -->
                        <div class="text-center mt20 mt-md30">
                           <div class="countdown counter-white white-clr">
                              <div class="timer-label text-center">
                                 <span class="f-28 f-md-45 timerbg oswald">00</span>
                                 <br>
                                 <span class="f-14 f-md-18 smmltd">Days</span>
                              </div>
                              <div class="timer-label text-center">
                                 <span class="f-28 f-md-45 timerbg oswald">00</span>
                                 <br>
                                 <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                              </div>
                              <div class="timer-label text-center timer-mrgn">
                                 <span class="f-28 f-md-45 timerbg oswald">00</span>
                                 <br>
                                 <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                              </div>
                              <div class="timer-label text-center ">
                                 <span class="f-28 f-md-45 timerbg oswald">00</span>
                                 <br>
                                 <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                              </div>
                           </div>
                        </div>
                        <!-- Timer End -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- launch-special Start -->
      <div class="live-section" id="live-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="left-live-box">
                     <div class="f-28 f-md-45 w700 text-center text-capitalize black-clr lh140">
                        Subscribe To Our <br class="d-none d-md-block"> JV List
                     </div>
                     <div class="f-20 f-md-22 w500 text-center text-capitalize black-clr lh140 mt10">
                        And Be The First to Know Our Special Contest, Events and Discounts
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt20 mt-md30">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1703814699" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6200898" />
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_3161ec97c09ca429c4183bf74a86fd90" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1703814699" class="af-form">
                              <div id="af-body-1703814699" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-12">
                                    <label class="previewLabel" for="awf_field-115787791" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb20 input-type">
                                       <input id="awf_field-115787791" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb20 col-md-12">
                                    <label class="previewLabel" for="awf_field-115787792" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-115787792" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-12">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img data-src="https://forms.aweber.com/form/displays.htm?id=zIwcTIxMbMwc" alt="image" /></div>
                        </form>
                     </div>
                     <!-- Aweber Form Code -->
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="right-live-box">
                     <div class="f-24 f-md-34 w700 text-center text-capitalize black-clr lh140">
                        Grab Your <span class="orange-clr">JVZoo</span> Affiliate Link to Jump on This Amazing Product Launch
                     </div>
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/jvzoo.webp" class="img-fluid d-block mx-auto mt20 jvzoo-img lazyload" alt="Jvzoo" />
                     <div class="request-affiliate mt20">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/396539" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request FE Link</a>
                     </div>
                     <div class="request-affiliate mt20">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/396537" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request Bundle Link</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt-md120">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 black-clr">
                     5 Reasons To Promote Sellero
                  </div>
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/purple-line.webp" alt="Purple" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 order-md-2">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w400 black-clr">
                     <li>We spent 1.5 years Planning, Developing, Testing, and Super-Optimizing with the World's Best Cloud Services: Akamai, Aws and HLS Technology to Deliver a Cutting Edge Marketing Experience.</li>
                     <li>Your customers will Sell Agency & Reseller Right Products using Sellero & Keep 100% Profit.</li>
                     <li>Your customers will receive payment directly to their accounts.</li>
                     <li>Your Customers will receive 40+ Ready-to-Sell Products to kick start online & get profit from day one.</li>
                     <li>Your customers can customize their website, show Products, services & more than 50 other cool features!</li>
                  </ul>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/sellero-thumbs-up.webp" alt="Sellero Thumbs Up" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
         </div>
      </div>
      <!-- launch-special End-->

      <!-- Problem Section Start -->
      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <!-- <div class="f-28 f-md-50 w700 white-clr heading-design">
                     The <span class="red-clr">Problem</span>  Is…
                  </div> -->
                  <div class="f-24 f-md-38 w700 white-clr heading-design">
                     Yes, you can Start Selling Online <br class="d-none d-md-block">
                     But the problem is… 
                  </div>
                  <div class="f-22 f-md-28 w700 white-clr mt20">
                     All Highly Lucrative Agency, E-Com, E-Learning Solutions Are Not Equal...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design mr10 mr-md20">Option 1</div>
                     DO-IT-YOURSELF
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
                     Yes. You can do it yourself But, for that, you need to be extremely technical & marketing savvy.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     You also need to Learn the Basics of the Domain, Hosting, HTML, CSS, JAVA Script, Photoshop, and Integrate a Payment Gateway.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     This is a Time Taking Process that will take months and the worst part is you won’t be earning during all.
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/option1.webp" alt="Option 1" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt-md120 align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design mr10 mr-md20">Option 2</div>
                     Hire Professionals
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
                     Yes, you can Hire Professionals to Help You Quick Start this time. But think twice before hiring to avoid costly freelancers, frustration, and quality concerns.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     Can cost you around $65 per hour, opting for fixed prices may require a minimum investment of $1500 to $2000 per project.
                  </div>
                  <div class="f-20 f-md-22 w700 lh140 white-clr mt20 d-flex align-items-center">
                    <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/stuck-smile.webp" alt="Stuck Smile" class="d-block img-fluid mr10 lazyload"> That Sucks.
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/option2.webp" alt="Option 2" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt-md120 align-items-center">
               <div class="col-md-6">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design mr10 mr-md20">Option 3</div>
                     Buy Multiple Paid Apps
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
                     Buy Multiple Paid Apps, Plug All of Them and Pay Recurring!
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     So, $49 here, $99 there, and $79 somewhere else, that adds up to potentially $100 to $300 per month. 
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/option3.webp" alt="Option 3" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt-md120 align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design mr10 mr-md20">Option 4</div>
                     Targeting the right audience.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
                     Identifying and reaching the right audience for your products or services is crucial.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     Without a clear understanding of your target market, you may struggle to effectively market your offerings and generate sales. 
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/option4.webp" alt="Option 4" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr changing-box">
                     If this is what your customers have been doing... <br class="d-none d-md-block">
                     then they may have been short-changing their businesses...
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Problem Section End -->

      <!-- Not Anymore Section Start -->
      <div class="but-not-anymore">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7 order-md-2">
                  <div class="f-28 f-md-50 w700 lh140 red-clr">
                     But Not Anymore
                  </div>
                  <div class="f-24 f-md-36 w600 lh140 black-clr mt20">
                     After Years of Planning, Coding, Debugging & Adding Robust Technology <span class="red-clr">to Sell Agency & Reseller Right Products, Courses & Services</span>
                  </div>
                  <div class="f-24 f-md-36 w600 lh140 black-clr mt20">
                     We are back with Sellero That Makes It a Cut above the Rest
                  </div>
               </div>
               <div class="col-md-5 order-md-1 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/but-not-anymore.webp" alt="But Not AnyMore" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
         </div>
      </div>
      <!-- Not Anymore Section End -->
      
      <!-- Proudly Section Start -->
      <div class="proudly-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 black-clr caveat proudly-head">
                     Proudly Presenting…
                  </div>
                  <div class="f-28 f-md-50 w700 lh140 white-clr mt20 mt-md50">
                     One Platform for All Your Customer's Digital Selling Needs 
                  </div>
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid lazyload mt20 mt-md50">
                  <div class="f-22 f-md-28 w700 lh140 white-clr mt20 mt-md80">
                     All-In-One Platform to Market & Sell Any Agency & Reseller Right Products, Courses, Services, and Physical Goods with Zero Technical Hassle.
                  </div>
                  <div class="proudly-box mt20 mt-md50">
                     <div class="f-22 f-md-28 w700 lh140 green-clr text-center text-md-start">
                        Eliminate Any Obstacles That May Be Hindering Your Customers' Path to Success
                     </div>
                     <ul class="f-20 f-md-22 lh160 w500 white-clr text-start">
                        <li>Your Customers Can Sell Online & Keep 100% of the Profit – Never Loose Traffic Again.</li>
                        <li>Switch To One Time Pricing & Save Huge</li>
                        <li>Done-for-Yow Selling Website Preloaded with 40+ Ready-to-Use Products to Start Selling Right Away.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Feature Section Start -->

      <div class="feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-28 f-md-50 lh140 black-clr w500">
                    Welcome to the <span class="purple-line w700">Future of Digital Selling</span> 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md100 align-items-center purple-bg">
               <div class="col-md-6">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     All-In-One Online Selling & Marketing Platform
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature1.webp" alt="Feature1" class="mx-auto d-block img-fluid lazyload lazyload">    
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center neon-bg">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Create and Manage Unlimited Products
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature2.webp" alt="Feature2" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center purple-bg">
               <div class="col-md-6">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Create Stunning Websites/Stores in Any Niche
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature3.webp" alt="Feature3" class="mx-auto d-block img-fluid lazyload lazyload">    
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center neon-bg">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Create Beautiful Membership Sites to Deliver Products
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature4.webp" alt="Feature4" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center purple-bg">
               <div class="col-md-6">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Get 40+ RED HOT, Done-For-You Products to Quick Start
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature5.webp" alt="Feature5" class="mx-auto d-block img-fluid lazyload lazyload">    
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center neon-bg">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     400+ Battle-Tested DFY Templates
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature6.webp" alt="Feature6" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center purple-bg">
               <div class="col-md-6">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Send Unlimited Emails & Set Follow-up, Emails Journey, Build Customer Relations
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature7.webp" alt="Feature7" class="mx-auto d-block img-fluid lazyload lazyload">    
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center neon-bg">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w700 black-clr lh140">
                     Inbuilt Lead Generation & Management System for Effective Management & Automation 
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/feature8.webp" alt="Feature8" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md60">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/down-arrow.webp" alt="Down Arrow" class="mx-auto d-block img-fluid lazyload lazyload jumparrow">
               </div>
            </div>
         </div>
      </div>
      <!-- Feature Section End -->

      <!-- Amazing Section Start -->
      <div class="amzing-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 black-clr">
                     Here are Some More Amazing Features
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 mt0 mt-md50 gap30">
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/unlimited-businesses.webp" alt="Unlimited Businesses" class="mx-auto d-block img-fluid lazyload lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Create Unlimited Businesses/Domains
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/custom-domain.webp" alt="Unlimited Custom Domain" class="mx-auto d-block img-fluid lazyload lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Unlimited Custom Domains
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/ab-testing.webp" alt="ab testing" class="mx-auto d-block img-fluid lazyload lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        A/B Testing
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/team-management.webp" alt="Single Dashboard to Manage All Businesses & Clients" class="mx-auto d-block img-fluid lazyload lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Single Dashboard to Manage All Businesses & Clients
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/file-sharing.webp" alt="Store, Manage & Share Unlimited Files" class="mx-auto d-block img-fluid lazyload lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Store, Manage & Share Unlimited Files
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/stock.webp" alt="1 Million+ Royalty Free Stock Photos and Videos" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        1 Million+ Royalty Free Stock Photos and Videos
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/ssl.webp" alt="Automatic & FREE SSL Encryption" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Automatic & FREE SSL Encryption
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/cloud.webp" alt="100% Cloud Based - No Hosting/Installation Required" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        100% Cloud Based - No Hosting/Installation Required
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/gdpr.webp" alt="100% GDPR and CAN-SPAM Compliant" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        100% GDPR and CAN-SPAM Compliant
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/seo.webp" alt="Inbuilt Page SEO for Better SERP Ranking & Traffic" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Inbuilt Page SEO for Better SERP Ranking & Traffic
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/easy.webp" alt="Easy and Intuitive to Use Software" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Easy and Intuitive to Use Software
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/training.webp" alt="Complete Step-by-Step Video Training and Tutorials" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                     Complete Step-by-Step Video Training and Tutorials Included
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/no-code.webp" alt="No Coding, Design or Technical Skills Required" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        No Coding, Design or Technical Skills Required
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/pop-up.webp" alt="Grab Visitors Engagement & Leads by Showing Pop-ups Inside Pages-" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Grab Visitors Engagement & Leads by Showing Pop-ups Inside Pages-
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="amzing-box text-center">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/software.webp" alt="Regular Updates" class="mx-auto d-block img-fluid lazyload">
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                        Regular Updates
                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center justify-content-center mt30">
               <div class="col-12 col-md-4">
                  <div class="much-more-box">
                     <div class="f-20 f-md-22 w600 lh140 black-clr text-center">
                        And Much More...
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Demo Section Start -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 lh140 w700 white-clr">
                     Watch The Demo <br class="d-none d-md-block">
                     Discover How Easy & Powerful It Is
                  </div>
                  <div class="mt20 mt-md30">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/down-arrow.webp" alt="Down Arrow" class="mx-auto d-block img-fluid lazyload jumparrow">
                  </div>
                  <!-- <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/demo-img.webp" alt="Demo Image" class="mx-auto d-block img-fluid lazyload mt20 mt-md30"> -->
                  <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://sellero.oppyo.com/video/embed/uu9l8no94u" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen title="Demo Video"></iframe>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Deep Funnel Section Start -->
      <div class="deep-funnel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 lh140 w700 black-clr">
                     Our Deep & High Converting Sales Funnel
                  </div>
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/funnel.webp" alt="Funnel" class="mx-auto d-block img-fluid lazyload mt20 mt-md80">
               </div>
            </div>
         </div>
      </div>

      <!-- Exciting Launch Section Start -->

      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 black-clr w700 lh140">
                  This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-34 lh140 black-clr w700">
                  To Make You Max Commissions
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>All Leads Are Hardcoded</li>
                     <li>We'll Re-Market Your Leads Heavily</li>
                     <li>Pitch Bundle Offer On Webinars</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/phase1.webp" class="img-fluid d-block mx-auto lazyload" alt="Phase1">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-34 lh140 black-clr w700">
                     Big Opening Contest & Bundle Offer
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>High in Demand Product with Top Conversion</li>
                     <li>Deep Funnel To Make You Double Digit EPCs</li>
                     <li>Earn Up To $415/Sale</li>
                     <li>Huge $10,000 JV Prizes</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/phase2.webp" class="img-fluid d-block mx-auto lazyload" alt="Phase2">
               </div>
            </div>
         </div>
      </div>

      <!-- Prize Value Section Start -->
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w700 lh140 white-clr">Get Ready to Grab Your Share of</div>
                  <div class="f-md-72 f-40 w700 purple-gradient lh140">$10,000 JV Prizes</div>
               </div>
            </div>
         </div>
      </div>

      <!-- Contest Section Start -->
      <div class="contest-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/prelaunch.webp" alt="Prelaunch" class="mx-auto d-block img-fluid lazyload">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/exiciting-launch.webp" alt="Exiciting" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
         </div>
      </div>

      <!-- Reciprocate Section Start -->
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 lh140 w700 black-clr">
                     Our Solid Track Record of <br class="d-md-block d-none"> <span class="purple-gradient"> Launching Top Converting Products</span>
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/product-logo.webp" class="img-fluid d-block mx-auto mt20 lazyload" alt="Product Logo">
                     <div class="f-md-50 f-28 lh140 w700 black-clr mt20 mt-md80 purple-gradient">
                        Do We Reciprocate?
                     </div>
                     <div class="f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                        We've been in top positions on hundreds of launch leaderboards &amp; sent huge sales for our valued JVs.
                     </div>
                     <div class="f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                        So, if you have a top-notch product with top conversions AND that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/logos.webp" class="img-fluid d-block mx-auto lazyload" alt="Logos">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>

      <!-- Contact Section Start -->

      <div class="contact-section">
         <div class="container">
            <div class="container-box">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-28 f-md-50 w700 white-clr lh140">
                        Have any Query? Contact us Anytime
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md80 justify-content-between">
                  <div class="col-12 col-md-8 mx-auto">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="contact-shape">
                              <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/amit-pareek.webp" class="img-fluid d-block mx-auto minus lazyload" alt="Amit Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr Amit Pareek
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-fb.webp" class="center-block lazyload" alt="Facebook">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-skype.webp" class="center-block lazyload" alt="Skype">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                           <div class="contact-shape">
                              <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp" class="img-fluid d-block mx-auto minus lazyload" alt="Er. Atul Kumar">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Er. Atul Kumar
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                 <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-fb.webp" class="center-block lazyload" alt="Facebook">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-skype.webp" class="center-block lazyload" alt="Skype">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Term Section Start -->
      <div class="term-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-50 lh140 w700 black-clr text-center">
                     Affiliate Promotions Terms & Conditions
                  </div>
                  <div class="f-16 f-md-16 lh140 p-md0 mt-md20 mt20 w400 text-center">
                     YOU MUST READ AND AGREE TO THESE AFFILIATE TERMS before requesting your affiliate link and being a part of this launch. Violation of ANY of these terms is cause for immediate termination and instant removal from this launch and any other of our launches - current or past -- and you agree that your current commissions will be forfeited without recourse and you may be banned from our future launches. Some violations may also be cause for LEGAL ACTIONS.:
                  </div>
                  <ul class="terms-list pl0 m0 f-16 f-md-16 lh140 w400 mt20 mt-md50">
                     <li>1). All email contacts MUST be your OWN opt in email list. You cannot send to lists that have been purchased or “gifted” from other vendors, buy solo ads, use safe lists, or obtained by illegal means. Email lists that are not your own are considered spam.</li>
                     <li>2) You may NOT create social media pages NOR purchase domain names with the PRODUCT NAME or BRAND NAME AND you may NOT use the PRODUCT NAME or the NAME OF THE VENDOR as your “from” in your emails instead of your own name AS IF YOU ARE THE PRODUCT OWNER. This is IMPERSONATION and will not be tolerated.</li>
                     <li>3) You may NOT purchase domain name(s) with the same or similar name as the PRODUCT NAME or BRAND NAME nor CLONE or otherwise copy our site and use that site to sell our product as your own. Furthermore, you may not add our product - whether purchased through us or obtained in any other manner - and sell or offer it on any type of “membership” site where multiple people have access to this product for any kind of fee or arrangement. This constitutes theft of our intellectual property rights and considered FRAUDULENT and is cause for LEGAL ACTIONS. </li>
                     <li>4) You may not encourage nor ask for or show a person HOW TO REFUND their purchase from another affiliate in order for them to purchase the same product through you.</li>
                     <li>5) You may not post OTO links on Review Sites because this will lead to confusion and refunds for those people who do not purchase the FE first and end up with NO LOGIN &amp; NO software or main product. </li>
                     <li>6) You may not use "negative" campaigns such as "is Product Name / Owner Name a scam?" or any other method to attract controversial click thru rates that an ordinary person would deem to portray a negative view of the product. You may not use offensive nor negative domain names.</li>
                     <li>7) You may not use misleading claims, inaccurate information or false testimonials (or anything that does not comply with FTC guidelines).</li>
                     <li>8) You may not use gray-hat/black-hat marketing practices to drive sales or for any other reason.</li>
                     <li>9) You may not give cash rebates of any kind as it may increase refund rates.</li>
                     <li>10) You may not purchase from your own affiliate link. Any 'self' purchase commission may be nullified or held back.</li>
                  </ul>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">NOTE:</span> These terms may change at any time without notice. (Please check back here regularly).
                  </div>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">NOTE:</span> Affiliate payments will be set according to the platform rules.
                  </div>     
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">CAUTION:</span> Do not send "raw" affiliate links. Utilize redirect links in emails &amp; website campaigns instead of your direct affiliate link. This increases conversions for both of us.
                  </div> 
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     We run a legitimate business, which means that we always correctly illustrate and represent our products and their features and benefits to the customer.
                  </div>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     Please make sure you do the same.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #1fe29f;
                         }
                   
                         .cls-3 {
                           fill: #7764ff;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                           <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                           <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                           <g>
                             <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                             <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                           </g>
                         </g>
                         <g>
                           <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                           <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                           <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                           <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                           <g>
                             <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                             <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                             <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                  </svg>
                  <!-- <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div> -->
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->

      <script>
         $(document).ready(function(){ 
            $(window).scroll(function(){ 
               if ($(this).scrollTop() > 100) { 
                     $('#scroll').fadeIn(); 
               } else { 
                     $('#scroll').fadeOut(); 
               } 
            }); 
            $('#scroll').click(function(){ 
               $("html, body").animate({ scrollTop: 0 }, 600); 
               return false; 
            }); 
         });
      </script>

      <script src="https://cdn.eduncle.com/webfiles/js/lazysizes.min.js"></script>

   </body>
</html>
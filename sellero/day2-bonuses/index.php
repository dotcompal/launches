<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="sellero Webinar Gift">
    <meta name="description" content="sellero Webinar Gift">
    <meta name="keywords" content="Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
    <meta property="og:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="sellero Webinar Gift">
    <meta property="og:description" content="Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
    <meta property="og:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="sellero Webinar Gift">
    <meta property="twitter:description" content="Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
    <meta property="twitter:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">

	<title>sellero Webinar Gift</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
   	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
   	<script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>

		
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
	<?php
		$date = 'June 12 2023 11:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
	?>
	<?php
		} else {
			echo "Times Up";
		}
	?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://sellero.co/special/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
               	<div class="col-12 text-center">
					<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
						<defs>
							<style>
							.cls-1 {
								fill: #fff;
							}
						
							.cls-2 {
								fill: #1fe29f;
							}
						
							.cls-3 {
								fill: #7764ff;
							}
							</style>
						</defs>
						<g id="Layer_1-2" data-name="Layer 1">
							<g>
								<g>
									<path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
									<path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
									<path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
									<g>
										<circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
										<circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
									</g>
								</g>
								<g>
									<path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
									<path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
									<path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
									<path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
									<path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
									<path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
									<g>
										<path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
										<polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
										<path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
									</g>
								</g>
							</g>
						</g>
					</svg>
               	</div>
               	<div class="col-12 text-center white-clr">
				   <div class="col-12 mt-md80 mt50 head-design relative">
                  <div class="gametext">
                  CUTTING-EDGE TECHNOLOGY
                  </div>
                  <div class=" f-md-44 f-28 w400 text-center black-clr lh140">
                  <span class="w600">Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes...</span>  NO Tech Hassles. No Monthly Fee Ever.
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
               Today Sellero Is Also Coming With 20+ Ready-To-Use Products & FREE Commercial License To Start Selling Online & Profiting Right Away!
               </div>
                   	<div class="f-md-36 f-28 lh160 w700 mt20 mt-md30">
				   		You've Earned It - <br class="d-none d-md-block">
						Enjoy This Assure Gift For Attending Our Webinar!
                   	</div>
                   	<div class="mt10 f-20 f-md-32 lh160">
                       <span class="tde">Download Your Assure Gifts Below</span>
                   	</div>
               	</div>
               	<div class="col-12 col-md-10 mx-auto mt20 mt-md30">
			   		<img src="https://cdn.oppyotest.com/launches/sellero/jv/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid mt20 mt-md50">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div>
                     	Niche Maketing Secrets
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	It’s very easy to get excited about niche marketing.
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	After all, how can you argue with working only a few hours per week while enjoying a full week’s (or even a full month’s) income?
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	As awesome as the idea of online passive niche marketing income may be, achieving this reality is another thing entirely.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://kyza.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwT1pXMU5kMVJ0Y0c1bFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVld4NlUxYzFRMlZYUmxsWGJXaHJVakZXY0ZReWNFZFBVVDA5" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div>
                     	Writing Tips Made Easy
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	If you run a blog or website you understand the need for writing regular content. While this may sound easy, it is not always easy to come up with ideas of what to write about.
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Even then you need to know how to write a compelling blog post that will attract attention. 
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	When it comes to writing online there are a few differences which you must be aware of. Writing this type of content is different than writing a novel or non-fiction book.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://kyza.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwT1pXMU5kMVJ0Y0hKbFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVld4NlUxYzFRMlZYUmxsWGJXaHJVakZXY0ZReWNFZFBVVDA5" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div>
                     	More Subscribers
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	How To Consistently Add Hundreds Of Subscribers To Your List Every Week!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Any online business will benefit greatly from building an opt-in email list that will read your valuable content, buy your products and services, or buy your affiliate offers.
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	Slow and steady wins the race and just know that it is feasible to add more than one hundred subscribers to your list on a weekly basis. This is without depending on Search Engine Optimization or any secret formula.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://kyza.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwT1pXMU5kMVJ0Y0dwTk1IaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVld4NlUxYzFRMlZYUmxsWGJXaHJVakZXY0ZReWNFZFBVVDA5" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div>
                     	Search Marketing 2.0
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	93% of marketers use video for online marketing, sales or communication! Explosive Growth of Visual Marketing has created new Battlefields for Online Marketers!93% of marketers use video for online marketing, sales or communication! Explosive Growth of Visual Marketing has created new Battlefields for Online Marketers!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Video marketing training, designed to take you by the hand and walk you through the process of getting out of Video Marketing.
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 This exclusive training will take you by the hand and show you step-by-step, topic by topic, and tool by tool, what you really need to know in order to dominate Video Marketing the easiest way possible, using the most effective tools and in the shortest time ever.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://kyza.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwT1pXMU5kMVJ0Y0c1T1ZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVld4NlUxYzFRMlZYUmxsWGJXaHJVakZXY0ZReWNFZFBVVDA5" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div>
                     	Build Your Audience
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	The single most important asset that any business has when it comes to promoting itself online and making profit is its audience. 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	If a business doesn't have an audience, then that means no one will know about it when that company releases an amazing product. 
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	It also means no one to read your posts and no one to click on your ads. 
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://kyza.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwT1pXMU5kMVJ0Y0ZabFZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVld4NlUxYzFRMlZYUmxsWGJXaHJVakZXY0ZReWNFZFBVVDA5" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

    	</div>
	</div>	
      
	<!-- Proudly Section End -->



	<!--Footer Section Start -->
	<div class="footer-section">
        <div class="container">
            <div class="row">
               	<div class="col-12 text-center">
                  	<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     	<defs>
							<style>
								.cls-1 {
								fill: #fff;
								}
						
								.cls-2 {
								fill: #1fe29f;
								}
						
								.cls-3 {
								fill: #7764ff;
								}
							</style>
                     	</defs>
						<g id="Layer_1-2" data-name="Layer 1">
                       		<g>
                         		<g>
									<path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
									<path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
									<path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                           			<g>
										<circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
										<circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
									</g>
                         		</g>
                         		<g>
                           			<path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                           			<path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
									<path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           			<path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           			<path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                           			<path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                           			<g>
                             			<path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                             			<polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                             			<path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                           			</g>
                         		</g>
                       		</g>
                     	</g>
                  	</svg>
               	</div>
               	<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  	<div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
					<ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
						<li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
						<li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
					</ul>
               	</div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


</body>
</html>

<!DOCTYPE html>
<html>
   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Sellero Special Bonuses">
      <meta name="description" content="Sellero Special Bonuses">
      <meta name="keywords" content="Sellero Special Bonuses">
      <meta property="og:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Sellero Special Bonuses">
      <meta property="og:description" content="Sellero Special Bonuses">
      <meta property="og:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Sellero Special Bonuses">
      <meta property="twitter:description" content="Sellero Special Bonuses">
      <meta property="twitter:image" content="https://www.sellero.co/special-bonus/thumbnail.png">
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <!--Load External CSS -->
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
   </head>
   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                    <defs>
                      <style>
                        .cls-1s {
                          fill: #1fe29f;
                        }
                  
                        .cls-2s {
                          fill: #121733;
                        }
                  
                        .cls-3s {
                          fill: #7764ff;
                        }
                      </style>
                    </defs>
                    <g id="Layer_1-2" data-name="Layer 1">
                      <g>
                        <g>
                          <path class="cls-3s" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                          <path class="cls-3s" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                          <path class="cls-3s" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                          <g>
                            <circle class="cls-3s" cx="130.39" cy="198.16" r="10.38"></circle>
                            <circle class="cls-3s" cx="75.08" cy="198.16" r="10.38"></circle>
                          </g>
                        </g>
                        <g>
                          <path class="cls-2s" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                          <path class="cls-2s" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-2s" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-2s" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-2s" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-2s" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                          <g>
                            <path class="cls-2s" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                            <polygon class="cls-1s" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                            <path class="cls-1s" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>
      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7 col-12">
                  <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                  <?php
                     if(isset($_POST['submit'])) 
                     {
                        $name=$_REQUEST['name'];
                        $afflink=$_REQUEST['afflink'];
                        //$picname=$_REQUEST['pic'];
                        /*$tmpname=$_FILES['pic']['tmp_name'];
                        $type=$_FILES['pic']['type'];
                        $size=$_FILES['pic']['size']/1024;
                        $rand=rand(1111111,9999999999);*/
                        if($afflink=="")
                        {
                           echo 'Please fill the details.';
                        }
                        else
                        {
                           /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                           {
                              echo "Please upload image file (size must be less than 1 MB)";	
                           }
                           else
                           {*/
                              //$filename=$rand."-".$picname;
                              //move_uploaded_file($tmpname,"images/".$filename);
                              $url="https://www.sellero.co/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                              echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                              //header("Location:$url");
                           //}	
                        }
                     }
                     ?>
                  <form class="row" action="" method="post" enctype="multipart/form-data">
                     <div class="col-12 form_area ">
                        <div class="col-12 clear">
                           <input type="text" name="name" placeholder="Your Name..." required>
                        </div>
                        <div class="col-12 mt20 clear">
                           <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                        </div>
                        <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                           <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #1fe29f;
                         }
                   
                         .cls-3 {
                           fill: #7764ff;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                           <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                           <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                           <g>
                             <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                             <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                           </g>
                         </g>
                         <g>
                           <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                           <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                           <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                           <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                           <g>
                             <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                             <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                             <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-20 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
                  <ul class="footer-ul w400 f-md-20 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   </body>
</html>
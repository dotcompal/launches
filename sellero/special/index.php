<!Doctype html>
<html>
   <head>
      <title>Sellero</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Sellero">
      <meta name="description" content="Launch Your Courses, Services, Or Any Purchased Agency/PLR Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
      <meta name="keywords" content="Sellero">
      <meta property="og:image" content="https://www.sellero.co/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="Website">
      <meta property="og:title" content="Sellero">
      <meta property="og:description" content="Launch Your Courses, Services, Or Any Purchased Agency/PLR Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
      <meta property="og:image" content="https://www.sellero.co/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Sellero">
      <meta property="twitter:description" content="Launch Your Courses, Services, Or Any Purchased Agency/PLR Right Product In Just 7 Minutes... NO Tech Hassles. No Monthly Fee Ever.">
      <meta property="twitter:image" content="https://www.sellero.co/special/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_https://cdn.oppyotest.com/launches/sellero/special/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script type='text/javascript' src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <script src="assets/js/timer.js"></script>
        
     <!-- New Timer  Start-->
<?php
         $date = 'June 13 2023 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
        
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->


         <style>
            .timer-header-top{background: #000; padding-top:10px; padding-bottom:10px;}
            .tht-left{color:#fff;  font-size: 20px; font-weight:700}
         </style>  
      </head>
      <body>
      
      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-16 text-md-start text-center">
                     Use Coupon Code <span class="green-clr">"SELL3"</span>  for <br class="d-block d-md-none">Extra <span class="green-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>	
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:85px;">
                           <defs>
                             <style>
                               .cls-1 {
                                 fill: #fff;
                               }
                         
                               .cls-2 {
                                 fill: #1fe29f;
                               }
                         
                               .cls-3 {
                                 fill: #7764ff;
                               }
                             </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                             <g>
                               <g>
                                 <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                                 <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                                 <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                                 <g>
                                   <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                                   <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                                 </g>
                               </g>
                               <g>
                                 <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                                 <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                 <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                 <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                 <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                 <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                                 <g>
                                   <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                                   <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                                   <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                                 </g>
                               </g>
                             </g>
                           </g>
                        </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 w400 white-clr text-md-end text-center">
                           <li><a href="#features" class="t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                           <li><a href="#demo" class="t-decoration-none">Demo</a></li>
                           <li class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="pre-heading f-md-23 f-20 w500 lh140 black-clr">
                     Have You Ever Bought Any Software/Course with Commercial/PLR Rights And <span class="black-clr w600"> <br class="d-block d-md-block"> Never Did Anything with Them? <u>It's Time To Put Them To Work For You!</u></span> 
                  </div>
               </div>
               <div class="col-12 mt-md80 mt20 head-design relative">
                  <div class="gametext d-none d-md-block">
                     CUTTING-EDGE TECHNOLOGY
                   </div>
                  <div class=" f-md-44 f-26 w500 text-center black-clr lh140">
                     <span class="w700"> Launch Your Courses, Agency Services, Or Any Purchased PLR/Resell Right Product In Just 7 Minutes...</span>  NO Tech Hassles. No Monthly Fee Ever.
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
               Today Sellero Is Also Coming With 20+ Ready-To-Use Products & FREE Commercial License To Start Selling Online & Profiting Right Away!
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box1">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://sellero.oppyo.com/video/embed/y1ygiv9f5q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="JV Video"></iframe>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="calendar-wrap">
                     <ul class="list-head pl0 m0 f-19 lh140 w400 white-clr">
                        <li>	Launch Your & Client's Products & Services Online with ZERO Tech Hassles.</li>
                        <li>	Profit with Agency/Reseller/PLR Right Products You Ever Purchased.</li>
                        <li>	Create Stunning Websites, E-stores and Membership Sites Easily</li>
                        <li>	Sell On Your Own Marketplace & Keep 100% Profits</li>
                        <li>	Smart-Checkout Links To Get Orders Directly from Social Media, Emails or Anywhere Else</li>
                        <li>	Sell High In-Demand Services with <span class="w700">Included Commercial License.</span> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container"> 
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w600 lh140 text-capitalize text-center black-clr">
                     With Sellero, Launch Your Product or Service For Recurring Profit  <span class="w700 purple-gradient">In 3 Easy Steps </span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step1.webp" class="img-fluid mx-auto d-block" alt="Add A Product ">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                        Add A Product
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 text-center">
                        Unlock the Potential & Start Selling Your Purchased Agency / Reseller / White label / PLR Right Products, Your Own Creations, or Choose from Our 20+ Done-For-You Products to Start
                        
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-4 mt20 mt-md0">
                     <div class="steps-block">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                        Choose Payment Gateway
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 text-center">
                        Integrate your PayPal or Stripe account to receive payments from your clients directly in your account. <br>
                        <span class="w600">NO Commission or Fee ever...</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-4 mt20 mt-md0">
                     <div class="steps-block">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh140 mt20 mt-md30 text-center">
                        Publish & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 black-clr lh140 mt15 mt-md20 text-center">
                        Now get your smart checkout link with your live professional website to start getting orders on product pages, social media, and right from your emails like a pro. Start selling online 360 degrees - <br><b> Quick and Easy.</b>
                        </div>
                     </div>
                  </div>
                 <div class="col-12 w400 f-18 f-md-26 black-clr text-center lh140 mt5 mt20 mt-md40">
                  It Just Takes Minutes to go live… 
                 </div>
                 <div class="col-12 w400 f-26 f-md-36 black-clr text-center lh140 mt5 mt10 ">
                     <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
                 </div>
                 <div class="col-12 f-20 f-md-24 w400 lh140 mt20 text-center  ">
                  Plus, with included FREE commercial license, <u class="w600 "> this is the easiest & fastest way </u>to start 6 figure <br class="d-none d-md-block">business and sell to desperate local businesses in no time!
                 </div>
             </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- second-section Start -->
      <div class="second-section">
         <div class="container">
            <div class="second-sec-list">
               <div class="row">
                  <div class="col-12">
                     <div class="f-28 f-md-50 lh140 w600 text-center black-clr">
                        <span class="w700 purple-gradient">We Guarantee,</span>  This Is The Only Selling <br class="d-none d-md-block">
                         Platform You'll Ever Need…
                     </div>
                     <div class="f-20 f-md-28 lh140 w400 text-center black-clr mt20 mt-md30">
                     DFY Products. Membership Sites. Website & Marketplace. Smart Checkout 
                     </div>
                  </div>
               </div>
               <div class="row mt0 mt-md50">   
                  <div class="col-12 col-md-6">
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Tap into the <span class="w600">Super-Hot $25 Trillion E-Selling Industry</span>
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Unlock the Potential of <span class="w600">Your Purchased Agency/Reseller/PLR Right Products</span> & Start Selling Right Away
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Complete <span class="w600">All-in-One Online Selling Platform</span> for Entrepreneurs, Marketers, and Newbies 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Quick Start</span>  Your Online Selling Business with <span class="w600">20+ Done-For-You Products</span>  
                        </div>
                     </div>
   
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600"> Easily Create and Manage Unlimited Products -</span> Digital Products, Courses, Services, and Goods 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           Create <span class="w600">Membership Sites to Deliver Products</span>  in Secured & Password Protected Member's Area
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Create</span>  Unlimited Beautiful, Mobile Ready and Fast-Loading <span class="w600">Landing Pages Easily </span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Use Smart-Checkout Links</span> to Directly <span class="w600">Receive Payments</span> from Social Media, Emails & On Any Page
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Precise Analytics</span> to Measure the Performance and Know Exactly What's Working and What's Not
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">SEO Friendly</span> & <span class="w600">Social Media Optimized</span> Pages for More Traffic 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">100% GDPR</span> and <span class="w600">Can-Spam</span> Compliant
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w600">
                           Launch Fast - Create Stunning <span class="w600">Websites & Stores</span> for Your and Your Client's Business in Any Niche 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 lh140 w400">   
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Sell Unlimited Products and Accept <span class="w600">Payments Through PayPal and Stripe</span> with Zero Fee, you can Keep 100% Profit
                           </div>
                        </div>                          
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Selling on ClickBank, JVZoo & Warrior Plus? Get <span class="w600">Seamless Integration</span> to Deliver Products on Automation. 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">100+ Battle Tested,</span>  Beautiful, and Mobile-Ready <span class="w600">Page Templates</span> 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Fully Customizable <span class="w600"> Drag & Drop WYSIWYG Page Editor</span> that Requires Zero Designing or Tech Skills 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Inbuilt Lead Management System</span> for Effective Contacts Management in Automation 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Connect Sellero with your Favorite Tools, <span class="w600">20+ Integrations </span> with Autoresponders and other Services 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">128 Bit Secured,</span> SSL Encryption For Maximum Security To Your Videos, Files, And Data
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Manage all the Pages, Products & Customers Hassle-Free, <span class="w600">All in Single Dashboard. </span> 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Step-By-Step Video Training</span> and Tutorials Included 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">24*5 Customer Support</span>
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w600">
                           PLUS, YOU'LL RECEIVE - <span class="w600"> A COMMERCIAL LICENSE IF YOU BUY TODAY </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div>
      <!-- second-section End -->
      <div class="amazing-feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="purple-design">
                     <div class="f-28 f-md-45 w700 black-clr">
                     Let's Make It 3X Profitable Deal for You Today
                     </div>
                     <div class="f-20 f-md-28 w400 black-clr">
                     You'll Have NO Reason, Not to Launch & Start Profiting TODAY Itself.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-7">
                  <div class="upgrade-design f-22 f-md-28 w700 lh140 white-clr">
                     Free Upgrade 1
                  </div>
                  <div class="f-24 f-md-36 w600 lh140 black-clr mt20">
                     Get 20+ RED HOT, Done- For-You Products To Quick Start
                  </div>
                  <div class="f-22 w400 lh140 black-clr mt15 mt-md20">
                     Simplify your product launch process with Sellero's curated collection of 20+ top-performing, high-converting products.
                  </div>
                  <div class="f-22 w400 lh140 black-clr mt15">
                     We've done the hard work for you, ensuring that you have a powerful arsenal to maximize your sales and profits without getting overwhelmed by creating products yourself at beginning. Get a quick start with Sellero's carefully chosen offerings.
                  </div>
               </div>
               <div class="col-md-5 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/amazing-feature-1.webp" alt="Option 1" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt20">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row mt30 mt-md60 align-items-center">
               <div class="col-md-7 order-md-2">
                     <div class="upgrade-design f-22 f-md-28 w700 lh140 white-clr mr10 mr-md20">
                        Free Upgrade 2
                     </div>
                     <div class="f-24 f-md-36 w600 lh140 black-clr mt20">
                        Get SELLERO Commercial License To Create An Incredible Income Serving Your Clients!
                     </div>
                  <div class="f-22 w400 lh140 black-clr mt15  mt-md20">
                     <span class="w600">There are millions of hungry local businesses & marketers who need your services.</span> They will pay top dollar to help them grow their business – online.
                  </div>
                  <div class="f-22 w400 lh140 black-clr mt15">
                  <span class="w600">Boost your client's business by launching their websites & stores, courses, membership sites, and help them improve their profits with smart checkout links</span> right from your home. We've taken care of everything so that you can deliver those services easily & charge 300-1000 dollars a pop.
                  </div>
                  <!-- <div class="f-20 f-md-22 w700 lh140 black-clr mt20 d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stuck-smile.webp" alt="Stuck Smile" class="d-block img-fluid mr10 lazyload"> That Sucks.
                  </div> -->
               </div>
               <div class="col-md-5 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/agency-license-img.webp" alt="Option 2" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="f-22 w400 lh140 black-clr mt30 mt-md50 text-center">
                     <span class="w600">Note: This special commercial licence is being included during this launch. </span><br class="d-none d-md-block"> Take advantage of it now because it will never be offered again.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section -->
      <!-- <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-24 f-md-40 w400 lh140 text-capitalize text-center black-clr col-12">
                  Sellero Is So Powerful That We Are Personally Using It <br class="d-none d-md-block">
                  <span class="f-28 f-md-42 w600">To Run Our 6-Figure Online Business Without a Hitch! </span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 f-md-36 f-22 w600 lh140 black-clr">
                  By Selling Online, We Have Generated $355k in Sales in The Last 10 Months Alone. 
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="reliable-sec mt20 mt-md90">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-22 f-md-32 lh140 w400 text-center white-clr but-design">
                        But it's not just us...               
                     </div>
                  </div>
                  <div class="col-12 col-md-12 mt20 mt-md50">
                     <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                        Sellero is a Proven Solution Powered by Reliable Technology,
                        Delighting Customers with Exceptional Service for Years.    
                     </div>
                  </div>
               </div>
               <div class="row row-cols-1 row-cols-md-3 mt20 mt-md60">
                  <div class="col">
                     <div class="d-flex align-items-center">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                        <div class=" text-capitalize ">
                           <div class="f-md-42 f-24 w700 white-clr">
                              18,500+ 
                           </div>
                           <div class="f-18 f-md-20 w500 white-clr">
                              Happy Customers
                           </div>
                        </div>
                     </div>
                  </div>   

                  <div class="col mt20 mt-md0">
                     <div class="d-flex align-items-center">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/play.webp" alt="play" class="img-fluid mr20">
                        <div class="text-capitalize ">
                           <div class="f-md-42 f-24 w700 white-clr">
                              110+
                           </div>
                           <div class="f-18 f-md-20 w500 white-clr">
                              Million Visitors
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="col mt20 mt-md0">
                     <div class="d-flex align-items-center">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                        <div class="text-capitalize ">
                           <div class="f-md-42 f-24 w700 white-clr">
                              160,000+
                           </div>
                           <div class="f-18 f-md-20 w500 white-clr">
                              Conversions
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="row">
                  <div class="col-12 col-md-12 mt20 mt-md70">
                     <div class="f-28 f-md-45 lh140 w700 text-center white-clr">
                        It's Totally MASSIVE.
                     </div>
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto mt10">
                     <div class="mt20 f-20 f-md-22 lh140 w400 text-center white-clr">
                        Now it's your turn to use it for your own business to skyrocket engagement & profits at a fraction of the cost during this 6 days launch special deal. 
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div> -->
      <!-- Proven Section End -->

      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Checkout What Other Marketers & Early Users <span class="w700 purple-gradient"> Have to Say About Sellero</span>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 ">
                  <div class="responsive-video border-video">
                     <iframe src="https://sellero.oppyo.com/video/embed/mnnpfte4vg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Craig McIntosh </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant </div>  
               </div>
              <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://sellero.oppyo.com/video/embed/1v91j7nmdq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20">Chris Sciullo </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Internet Marketer</div>  
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">

            <div class="col mt20 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                      </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Samuel Marco</div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I have <span class="w600">bought multiple software products in the past with commercial license</span> "I've been searching for a solution like this for a long so I can use those purchased software and start selling my services to local businesses for an additional income steam.
                     </p>
                     <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                         <span class="w600">And now SELLERO enabled me to launch my agency services in NO TIME.</span> It's a complete package that delivers on its promises. I couldn't be happier!"
                     </p>
                     
                  </div>
               </div>


            
               
               <div class="col mt20 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/vivek-gour.webp" class="img-fluid d-block mx-auto img-rounded" alt="Sarah M.">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Vivek Gour </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        "I couldn't believe how easy it was to <span class="w600">launch my first online course using SELLERO.</span>
                     </p>
                     <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                        In just few minutes, I had everything set up and ready to go. No technical hurdles, no monthly fees. It's a game-changer!"
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!--Awesome Section -->
      <div class="awesome-sections">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="awesome-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-45 f-28 lh140 w600 black-clr relative trigger-block">
                              Hello There,
                           </div>
                           <div class="f-md-28 f-22 lh140 w600 white-clr mt20 mt-md40 text-center">
                              It's Dr. Amit Pareek along with my business partner Er. Atul Kumar!
                           </div>
                        </div>
                        <div class="col-12 text-center mt20 mt-md30">
                           <div class="row row-cols-1 row-cols-md-3 justify-content-center">
                              <div class="col">
                                 <img src="https://cdn.oppyotest.com/launches/sellero/jv/images/amit-pareek.webp" class="img-fluid d-block mx-auto mt20 mt-md0" alt="Dr. Amit Pareek">
                                 <div class="f-26 f-md-32 w600 lh140 text-center white-clr  mt20 ">
                                    Dr. Amit Pareek
                                 </div>
                                 <!-- <div class="f-15 w500 lh140 text-center white-clr">
                                    (Techpreneur & Marketer)
                                 </div> -->
                              </div>
                              <div class="col">
                                 <img src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp" class="img-fluid d-block mx-auto mt20 mt-md0" alt="Er. Atul Kumar">
                                 <div class="f-26 f-md-32 w600  lh140 text-center white-clr mt20 ">
                                    Er. Atul Kumar
                                 </div>
                                 <!-- <div class="f-15 w500  lh140 text-center white-clr">
                                    (Entrepreneur & Product Creator)
                                 </div> -->
                              </div>
                           </div>
                        </div>
                        <div class="col-12 f-18 f-md-22 lh160 w400 white-clr mt20 mt-md30 text-center text-md-start">
                           We have happily served over 150,000 customers online and have 18+ years of online business experience combined.
                        </div>
                        <div class="col-12 f-18 f-md-22 lh160 w400 white-clr mt20 text-center text-md-start">
                           Not only we have made millions of dollars selling digital products for ourselves, but more importantly, we helped thousands and thousands of people get their desired sales and profits right from their home.
                        </div>
                        <!-- <div class="col-12 f-18 f-md-20 lh160 w400 white-clr mt20 text-center text-md-start">
                           In this digital era, every business must have an E-Selling set up to build a brand and ensure their business growth. 
                        </div> -->
                        <div class="col-12 f-22 f-md-28 w600 lh160 white-clr mt20 mt-md30 text-center">                              
                           <div class="hithere">                              
                             <div class="w600 f-md-32 f-24 lh160 w400 text-center black-clr"> And it is not only us who are cashing in selling products...</div>
                             <div class="w400 f-md-32 f-24 lh160 text-center black-clr mt20">
                              Millions of Entrepreneurs (Both Big and Small) Sell Their Own <br class="d-none d-md-block">
                              and Others Products Online to <span class="w600 purple-gradient">Get an Unfair Advantage</span>      
                             </div>
                           </div>                 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Awesome Section End-->
      <!-- Did you know Section Starts-->
         <section class="didyouknow-sec">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-28 f-md-45 w700 white-clr text-center lh140 head-design-red mt0 mt-md50">Is Selling Online Profitable In 2023?</div>
                     <div class="f-28 f-md-100 w700 lh140 mt20 mt-md20">
                     <span class="black-clr"><img src="https://cdn.oppyotest.com/launches/sellero/special/images/thumbsup-smily.webp" alt="Thumbsup Smily"></span> <span class="red-clr">Yep,</span> <br>
                     </div>
                     <div class="f-24 f-md-28 w400 lh140 black-clr mt20 demo-text">
                        Here's the proof that we have <span class="w600">got more than 389K targeted visitors with 21,872 sales, and total $551,778 in sales</span>  for our products. <u>ZERO PAID MARKETING.</u>
                     </div>  
                     <div class="mt50 mt-md90">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/siteefy.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md70">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-24 f-md-28 w600 lh140 black-clr mt20 demo-text">
                        And by promoting good offers to those valued customers, we have made another 263,617K in affiliate commissions in the last 10 months.
                     </div>  
                     <div class="mt50 mt-md90">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/website-process.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="didyouknow-box mt20 mt-md70">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-18 f-md-22 lh150 w400 black-clr">
                           Listen, if you're going to compete and get the life you desire in this ever-growing digital marketing era, then you're going to <span class="w600"> <u>need an offer that you can sell again and again.</u></span> 
                           <br><br>
                           That means you'll need to setup website, checkout process, & fast pages that guide, engage, and convert visitors into customers. <span class="w600">And that's what Sellero does for you. </span> 
                           <br><br>
                        <span class="w600">You only need to setup your offer once and it delivers RESULTS again and again on complete autopilot!</span> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- Did you know Section Ends-->
  <!-- Bought Section Start -->
  <section class="bought-sec">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-28 f-md-50 lh140 w700  black-clr">Let's Make It Simple for You</div>
            <div class="f-28 f-md-45 lh140 w700 red-clr">
               Have You Ever Bought Any Software or Course?
            </div>
            <div class="f-22 f-md-32 lh140 w400 black-clr mt10" style="word-wrap: break-word;">
               With Agency/Reseller/Whitelabel/PLR Rights!
            </div>
         </div>
      </div>
      <div class="answer-sec mt20 mt-md50">
         <div class="row align-items-center">
            <div class="col-12 col-md-8 order-md-2">
               <div class="f-22 f-md-32 lh140 black-clr w700">
                <u> I'm Sure Answer is YES.</u>
               </div>
               <div class="mt10 f-20 f-md-22 lh140 black-clr w400">
                  You likely possess a valuable asset already: at least five top-notch products with Agency/Reseller/Whitelabel/PLR (Private Label Rights) that you can launch & start selling right away.
               </div>
               
               <div class="mt10 f-20 f-md-22 lh140 red-clr w600">
                  But never did anything with them?
               </div>
            </div>
            <div class="col-12 col-md-4 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/sellero/special/images/yes-man.png" alt="Yes Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
      <div class="row mt20 mt-md50">
         <div class="col-12 text-center">
            <div class="f-22 f-md-32 w700 lh140 black-clr">
               Imagine the Possibilities!
            </div>
            <div class="f-20 f-md-22 w400 lh140 black-clr mt10">
               It's time to put them to work for you & unleash the power. Trust me; the rewards can be substantial.
            </div>
            <div class="f-20 f-md-24 w400 lh140 black-clr mt10">
               Not Convinced Yet...... <span class="w600">Check this Simple Calculation. Let's break it down for you.</span> 
            </div>
         </div>
      </div>
      <div class="row mt20 mt-md80 align-items-center">
         <div class="col-md-6">
            <div class="f-20 f-md-24 lh140 w400 black-clr">
               If you have at least 5 Products with Reselling Rights Now, suppose you manage to secure just two sales per month for each product, with a price tag of $200. Simple math reveals that you could be leaving a <span class="w600"> staggering $2,000 on the table each month, </span>   my friend.
            </div>
         </div>
         <div class="col-md-6 mt20 mt-md0">
            <img src="https://cdn.oppyotest.com/launches/sellero/special/images/money-calculation.png" alt="Money Calculation" class="mx-auto d-block img-fluid">
         </div>
      </div>
   </div>
</section>
<!-- Bought Section End -->
      <!-- thatsall-section Start -->
      <div class="enough-sec relative">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 lh140 black-clr truth-box">
                     If You’re Selling Online, You’re
                      Actually Losing Money!
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/automated-funnel.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-24 f-20 lh140 w400 text-center text-md-start">
                              By the year 2040, it's <span class="w600">estimated that 95% of all purchases</span> will be through eCommerce. (Nasdaq) 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/automated-funnel1.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-24 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w600">20+ Million Ecom sites across the entire globe,</span> with more and more being created every single day 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/automated-funnel3.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-24 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w600">The #1 reason people shop online</span> is that they're able to shop at all hours of the day. (KPMG) 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/automated-funnel4.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-24 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              eCommerce industry is growing 23% year-over-year, yet <span class="w600">46% of American small businesses still don't have a website  </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <img src="https://cdn.oppyotest.com/launches/sellero/special/images/laptop-man.png" class="img-fluid d-none that-man" alt="Man">

            <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
               <div class="col-12 mx-auto col-md-3 z-index9">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/laptop.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-9 mt-md0 mt30">
                  <div class="problme-area-shape">
                     <div class="f-20 f-md-28 w400 lh150 black-clr">
                        In simple terms…
                     </div>
                     <div class="f-20 f-md-28 w600 lh140 black-clr">
                        Now is the perfect time to get in and profit from this exponential growth!
                     </div>
                  </div>
               </div>
           </div>

         </div>
      </div>
      <!-- thatsall-section End -->

    

      <!-- Problem Section Start -->
      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-22 w500 white-clr">
                     If Selling Digital Products or Services Are So Great, Why Not Everyone Is Using It & Reaping the Benefits?
                  </div>
                  <div class="f-24 f-md-45 w700 white-clr heading-design mt20">
                      Here's The Problem…
                  </div>
                  <!-- <div class="f-20 f-md-22 w500 white-clr mt20">
                     Start This Highly Lucrative E-Com, E-Learning or Agency Business Online!
                  </div> -->
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6">
                  <div class="option-design f-22 f-md-28 w700 lh140 white-clr">
                     Option 1
                  </div>
                  <div class="f-24 f-md-36 w600 lh140 white-clr mt20">
                     DO-IT-YOURSELF
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt15 mt-md20">
                     You need to be extremely technical & marketing savvy if you ever want to do it yourself. You also need to Learn the Basics of the Domain, Hosting, HTML, CSS, JAVA Script, Photoshop, and How to Integrate a Payment Gateway.
                  </div>
                  <div class="f-18 w400 lh140 white-clr mt15">
                     This is a Time Taking Process which will take months and the worst part is you won't be earning during all.
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/opt1.webp" alt="Option 1" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt-md120 align-items-center">
               <div class="col-md-6 order-md-2">
                     <div class="option-design f-22 f-md-28 w700 lh140 white-clr mr10 mr-md20">
                        Option 2
                     </div>
                     <div class="f-24 f-md-36 w600 lh140 white-clr mt20">
                        Hire Professionals to Help You Quick Start this time.
                     </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt15  mt-md20">
                     If you are thinking of hiring professionals then think again. Because reasonably skilled freelance web developers make about $65 per hour. This figure can vary from $60-$300 on their experience level. Or if you go for a fixed price – It can cost you somewhere between $1500 to $2000 bare minimum per Project.
                  </div>
                  <div class="f-18 w400 lh140 white-clr mt15">
                     Not to mention, the frustration of explaining to them exactly what you need and that also massively lowers the QUALITY.
                  </div>
                  <div class="f-20 f-md-22 w700 lh140 white-clr mt20 d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stuck-smile.webp" alt="Stuck Smile" class="d-block img-fluid mr10 lazyload"> That Sucks.
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/opt2.webp" alt="Option 2" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <div class="row mt-md120 align-items-center">
               <div class="col-md-6">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design f-22 f-md-28 w700 lh140 white-clr">Option 3</div>
                     <div class="f-24 f-md-36 w600 lh140 white-clr mt20">Buy Multiple Paid Apps, Plug All of Them and Pay Recurring!</div>
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt15  mt-md20">
                     You need to buy multiple recurring apps because you will not get all the necessary features in one place. So, $49 here, $99 there, and $79 somewhere else, that adds up to potentially $100 to $300 per month, still not getting all the features and the headache of managing multiple dashboards but no guarantee of satisfactory results. So, ultimately you will only wind-up throwing money down the toilet.
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt15">
                     Recurring options just suck your wallet dry without all the necessary features included in one tool.
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/opt3.webp" alt="Option 3" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>
            <!--<div class="row mt-md120 align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">
                     <div class="option-design mr10 mr-md20">Option 4</div>
                     Targeting the right audience.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
                     Identifying and reaching the right audience for your products or services is crucial.
                  </div>
                  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
                     Without a clear understanding of your target market, you may struggle to effectively market your offerings and generate sales. 
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/option4.webp" alt="Option 4" class="mx-auto d-block img-fluid lazyload">
               </div>
            </div>-->
            <!-- <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr changing-box">
                     If this is what you are doing... <br class="d-none d-md-block">
                     You may short-charging your businesses...
                  </div>
               </div>
            </div> -->
         </div>
      </div>
      <!-- Problem Section End -->

      <!-- Not Anymore Section Start -->
      <div class="but-not-anymore">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7 order-md-2">
                  <div class="f-28 f-md-50 w700 lh140 red-clr">
                     But Not Anymore
                  </div>
                  <div class="f-22 f-md-26 w500 lh140 black-clr mt20">
                  After years of Planning, Coding, Debugging, & Real-User-Testing with thousands of real users  <br><br>
                  We are releasing our <span class="w600 red-clr">Reliable & Robust Technology that will not only help you launch your products & services in No Time</span> to maximize your profits but also boost your customers buying experience. 
                  </div>
                  
               </div>
               <div class="col-md-5 order-md-1 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/but-not-anymore.webp" alt="But Not AnyMore" class="mx-auto d-block img-fluid lazyload lazyload">
               </div>
            </div>
         </div>
      </div>
      <!-- Not Anymore Section End -->

      <!-- Proudly Section Start -->
      <div class="proudly-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 black-clr caveat proudly-head">
                     Proudly Presenting…
                  </div>
                  <div class="f-28 f-md-50 w700 lh140 white-clr mt20 mt-md50">
                     One Platform for All Your Digital Selling Needs
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid lazyload mt20 mt-md50">
                  <!--<div class="f-22 f-md-28 w700 lh140 white-clr mt20 mt-md80">
                     All-In-One Platform to Market & Sell Any Agency & Reseller Right Products, Courses, Services, and Physical Goods with Zero Technical Hassle.
                  </div>-->
                  <div class="proudly-box mt20 mt-md50">
                     <div class="f-22 f-md-28 w700 lh140 green-clr text-center">
                     Eliminate Any Obstacles That May Be Hindering Your Path to Success
                     </div>
                     <ul class="high-converting-list pl0 mt20 f-18 f-md-20 lh150 w400 white-clr text-start">
                        <li>Sell Online & Keep 100% Profit With You - Never Loose Traffic Again.</li>
                        <li>Switch to One Time Pricing & Save BIG Monthly Fee</li>
                        <li>20+ Ready-to-Use Products to Start Selling Right Away.</li>
                        <li>No Tech Hassles at All & No More Hiring A Costly Designer</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w600 lh140 text-center black-clr">
                  With Sellero, Launch Your Product or Service For Recurring Profit <span class="purple-gradient w700">In 3 Easy Steps </span>
                  </div>
               </div>
            </div>
            <div class="row mt60 mt-md100 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="f-24 f-md-34 w700 lh140 text-center text-md-start black-clr  d-flex align-items-center">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step1.webp " class="img-fluid mr10 mr-md20" alt="step One"> Add A Product
                  </div>
                  <div class="f-18 f-md-22 w400 black-clr lh140 mt15 text-center text-md-start">
                  Unlock the Potential & Start Selling Your Purchased Agency / Reseller / White label / PLR Right Products, Your Own Creations, or Choose from Our 20+ Done-For-You Products to Start
                  <br><br>
                  Yes - You likely have bought a Software or A Course already with Agency/Reseller/White label/PLR Rights and you can start selling it for BIG profits over and over again.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <video class="border-video video-box-testi" width="100%" height="auto" loop="" autoplay="" muted="muted">
                     <source src="https://cdn.oppyotest.com/launches/sellero/special/images/step-1.mp4" type="video/mp4">
                 </video>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-6 col-12 order-md-2 relative">
                  <div class="f-24 f-md-34 w700 lh140 text-center text-md-start black-clr  d-flex align-items-center">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step2.webp " class="img-fluid mr10 mr-md20" alt="step One"> Choose Payment Gateway
                  </div>
                  <div class="f-18 f-md-22 w400 black-clr lh140 mt15 text-center text-md-start">
                  Integrate your PayPal or Stripe account to receive payments from your clients directly in your account. Sellero also comes with seamless integration with JVZoo, ClickBank and WarriorPlus
                  <br><br>
                  <span class="w600">NO Commission or Fee ever...</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <video class="border-video video-box-testi" width="100%" height="auto" loop="" autoplay="" muted="muted">
                     <source src="https://cdn.oppyotest.com/launches/sellero/special/images/step-2.mp4" type="video/mp4">
                 </video>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow1.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="f-24 f-md-34 w700 lh140 text-center text-md-start black-clr  d-flex align-items-center">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/step3.webp " class="img-fluid mr10 mr-md20" alt="step One"> Publish & Profit 
                  </div>
                  <div class="f-18 f-md-22 w400 black-clr lh140 mt15 text-center text-md-start">
                  Now get your smart checkout link with your live professional website to start getting orders on product pages, social media, and right from your emails like a pro. Start selling online 360 degrees - <span class="w600">Quick and Easy. </span>
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <video class="border-video video-box-testi" width="100%" height="auto" loop="" autoplay="" muted="muted">
                     <source src="https://cdn.oppyotest.com/launches/sellero/special/images/step-3.mp4" type="video/mp4">
                 </video>
               </div>
            </div> 
            <div class="row mt20 mt-md50">
               <div class="col-12 w400 f-18 f-md-26 black-clr text-center lh140">
                  It Just Takes Minutes to Go Live…
                 </div>
                 <div class="col-12 w400 f-26 f-md-36 black-clr text-center lh140 mt5 mt10 ">
                     <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
                 </div>
                 <div class="col-12 f-20 f-md-24 w400 lh140 mt20 text-center  ">
                  Plus, with included FREE commercial license, <span class="w600"> this is the easiest &amp; fastest way </span>to start 6 figure <br class="d-none d-md-block">business and sell to desperate local businesses in no time! 
                 </div>
            </div>          
         </div>
      </div>
      <!-- Step Section End -->
 <!-- thatsall-section Start -->
 <div class="thatsall-section relative">
   <div class="container">
      <div class="reliable-sec">
         <div class="row">
            <div class="col-12 col-md-12 text-center">
               <div class="f-22 f-md-32 lh140 w400 text-center white-clr but-design">
                  100s Of Marketers Are Using And Loving Sellero.            
               </div>
            </div>
            <!-- <div class="col-12 col-md-12 mt20 mt-md50">
               <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                  Sellero is a Proven Solution Powered by Reliable Technology,
                  Delighting Customers with Exceptional Service for Years.    
               </div>
            </div> -->
         </div>
         <div class="row row-cols-1 row-cols-md-3 mt20 mt-md60">
            <div class="col">
               <div class="d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                  <div class=" text-capitalize ">
                     <div class="f-md-42 f-24 w700 white-clr">
                        18,500+ 
                     </div>
                     <div class="f-18 f-md-20 w500 white-clr">
                        Happy Customers
                     </div>
                  </div>
               </div>
            </div>   

            <div class="col mt20 mt-md0">
               <div class="d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/play.webp" alt="play" class="img-fluid mr20">
                  <div class="text-capitalize ">
                     <div class="f-md-42 f-24 w700 white-clr">
                        110+
                     </div>
                     <div class="f-18 f-md-20 w500 white-clr">
                        Million Visitors
                     </div>
                  </div>
               </div>
            </div>

            <div class="col mt20 mt-md0">
               <div class="d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                  <div class="text-capitalize ">
                     <div class="f-md-42 f-24 w700 white-clr">
                        160,000+
                     </div>
                     <div class="f-18 f-md-20 w500 white-clr">
                        Conversions
                     </div>
                  </div>
               </div>
            </div>

         </div>
         <div class="row">
            <div class="f-28 f-md-45 lh140 white-clr w600 text-center mt20 mt-md70">
               It Is The Same Proven Technology That Has:
            </div>
            <ul class="high-converting-list pl0 mt20 f-18 f-md-20 lh150 w400 white-clr">
               <li> <span class="w600 yellow-clr">Saved thousands of dollars </span> that many users would have paid to monthly services etc.&nbsp;</li>
               <li> <span class="w600 yellow-clr">Saved huge amounts of traffic that many of the marketers would have lost </span>  due to slow-loading & low-converting pages & E-stores </li>
               <li> <span class="w600 yellow-clr">And of course, generated tons of sales, commissions and profits</span>  with beautifully designed landing pages & websites </li>
            </ul>
         </div>
         <div class="row">
            <div class="col-12 col-md-12 mt20">
               <div class="f-28 f-md-45 lh140 w700 text-center white-clr">
                  It's Totally MASSIVE.
               </div>
               <img src="https://cdn.oppyotest.com/launches/sellero/special/images/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto mt10">
               <div class="mt20 f-20 f-md-22 lh140 w400 text-center white-clr">
                  Now it's your turn to use it for your own business to skyrocket engagement & profits at a fraction of the cost during this 6 days launch special deal. 
               </div>
            </div>
         </div>
      </div>  
      <!-- <img src="https://cdn.oppyotest.com/launches/sellero/special/images/laptop-man.png" class="img-fluid d-none that-man" alt="Man">

      <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
         <div class="col-12 mx-auto col-md-3 z-index9">
            <img src="https://cdn.oppyotest.com/launches/sellero/special/images/laptop.webp" class="img-fluid d-block mx-auto">
         </div>
         <div class="col-12 col-md-9 mt-md0 mt30">
            <div class="problme-area-shape">
               <div class="f-20 f-md-28 w400 lh150 black-clr">
                  In simple terms…
               </div>
               <div class="f-20 f-md-28 w400 lh140 black-clr">
                  <span class="red-clr w700">EVERY BUSINESS</span> NEEDS TO GO ONLINE To Reach Their Customers! Yes! EVERYONE
               </div>
            </div>
         </div>
     </div> -->

   </div>
</div>
<!-- thatsall-section End -->
      <!-- Demo Section Start -->
      <div class="demo-section" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center white-clr">
                  Check Out Sellero In Action! 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/down-arrow.webp" class="img-fluid d-block mx-auto vert-move" alt="arrow">
               </div>
               <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">
                  <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://sellero.oppyo.com/video/embed/bx13h4xkpz" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="Demo Video"></iframe>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                    Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Demo Section End -->

      <!-- Testimonial Section Start -->
      <div class="testimonial-section2">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  And Here's What Some More <br class="d-none d-md-block">
                  Happy Users Say About Sellero... 
               </div>
            </div>

            <div class="row row-cols-md-2 row-cols-1 gx4 mt-md80 mt0">
            
            <div class="col mt-md120">
                 <div class="single-testimonial">
                     <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/logic-beam.webp" class="img-fluid d-block mx-auto img-rounded" alt="Logicbeam">
                      </div>
                    <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Logicbeam  </div>
                    <div class="stars mt10">
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                    </div>
                    <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                    <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     "I was hesitant at first, but SELLERO exceeded my expectations. 
                    </p>
                    <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     I launched my services in minutes, and the results were amazing. 
                    </p>
                    <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                     It's a must-have tool for anyone looking to grow their business quickly."
                    </p>
                 </div>
              </div>

            
              <div class="col mt-md120">
                 <div class="single-testimonial">
                    <div class="st-img">
                    <img src="https://cdn.oppyotest.com/launches/sellero/special/images/imreview-squad.webp" class="img-fluid d-block mx-auto" alt="IMReview Squad">
                     </div>
                    <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> IMReview Squad  </div>
                    <div class="stars mt10">
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                    </div>
                    <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                    <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     "I've tried multiple platforms before, but SELLERO takes the cake. 
                    </p>
                    <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                     <span class="w600">It's user-friendly, efficient, Result oriented</span> and saved me from paying expensive monthly fees. 
                    </p>
                    <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I highly recommend SELLERO to anyone wanting to monetize their agency or PLR products effortlessly."
                    </p>
                 </div>
              </div>
              <div class="col mt-md120 mx-auto">
               <div class="single-testimonial mt-md50 mt10">
                  <div class="st-img">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/brett.webp" class="img-fluid d-block mx-auto" alt="Brett Ingram">
                   </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Brett Ingram </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     "As a busy entrepreneur, time is of the essence. <span class="w600">SELLERO allowed me to launch my courses without any technical hassles. </span>
                  </p>
                  <p class="mt10 f-18 f-md-20 lh140 w400 text-center">
                     It's a time-saver, and the fact that there are no monthly fees is a huge bonus."
                  </p>
               </div>
            </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!-- Platform Section Start -->
      <div class="platform-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="stats-title f-24 f-md-36 w600 lh140">
                    <div class="stats-gradient text-capitalize">
                     Sellero Is Built For Entrepreneurs & Marketers Of All Levels </div>
                  </div>
               </div>
               <!-- <div class="col-12 f-28 f-md-45 w700 lh140 black-clr mt20 mt-md50 text-center">
                  Help Them Start & Grow Online–Completely  <br class="d-none d-md-block"> Hassle-Free, All from 1 Platform 
               </div> -->
            </div>
            <div class="row row-cols-1 row-cols-md-3 ">
               <div class="col-12 relative">                  
                  <div class="row mt40 mt-md100 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class=" black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Ever Bought A Software or Course?</span><br>
                           <span class="w400 f-20 lh140 mt15"> With Agency/Reseller/White label/PLR rights? <br>
                           Don't let it stay in your hard disk and waste the value it can deliver to others. Now is the time to GO online and Unlock the Potential of Your Purchased Products to Quick Start</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats1.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>

                  <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Have any digital Arsenal ? </span><br>
                           <span class="w400 f-20 lh140 mt15">Have you Ever bought Any E-book Creator ,Video Creator, or any other Marketing Content Creator.
                           Amazing, it's time to List all of them & Leverage the Power of Sellero to unlock untapped revenue streams.</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>

                  <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6  order-md-2">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Got A Product or Service?</span><br>
                           <span class="w400 f-20 lh140 mt15">Have you created a good product or service that can give any value to your customers? <br>
                           Great – Now is the time to launch it online. Let us take this out to the next level and make you profits. Got A Product or Service?  </span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0  order-md-1">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats3.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
                  <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Don't Have A Product at All? </span><br>
                           <span class="w400 f-20 lh140 mt15">No problem! This cannot stop you from being successful. Today we are giving you 20+ RED HOT courses to sell online. Just plug and play and start selling those right away.  </span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats4.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>

                  <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6  order-md-2">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Got Something to Teach? </span><br>
                           <span class="w400 f-20 lh140 mt15">Awesome! Help others with your knowledge. Create a membership with your content and start selling now. Make additional or full-time online income.  </span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0  order-md-1">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats5.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
                  <div class="row mt40 mt-md100 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Are You Selling on JVZoo /ClickBank/WarriorPlus? </span><br>
                           <span class="w400 f-20 lh140 mt15">Deliver your digital downloads, files and courses inside the login secured members area. Simply integrate Sellero and Automate your delivery process as well as make it 100% secure.</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats6.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>

                  <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="black-clr">
                           <span class="w600 f-md-28 f-22 lh140">Want to Kickstart Your Agency/Freelancing Business? </span><br>
                           <span class="w400 f-20 lh140 mt15">According to Nasdaq, 95% of the sales will happen online by 2030. So, help local business get online. Sell High-In-Demand Services To Your Clients For BIG Profits. </span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/stats7.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/spe-line-1.webp" class="img-fluid d-none d-md-block mx-auto sep-lines">
               </div>
            </div>    
            <div class="row align-items-center">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/arrow.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md20">
                  <div class="platform-box">
                     <div class="col-12 blue-clr f-22 f-md-32 w700 text-center">
                        Sellero is Built to Help You Launch a SOLID Business in 2023 & beyond. 
                     </div>
                     <div class="row mt20 mt-md50 align-items-center">
                        <div class="col-12 col-md-4">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/like.webp" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-8 mt20 mt-md0">
                           <div class="w400 f-20 f-md-22 w700 lh140 black-clr">    
                              Nothing To Download or Install<br><br>
                              No Prior Experience Needed. Zero Learning Curve <br><br>
                              Newbie-Friendly - Created Keeping Both First Timers' And <br class="d-none d-md-block">
                              Part-Timers In Mind
                           </div>
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>  
         </div>
      </div>
      <!-- Platform Section End -->

     
      <!-- Features Headline Start -->
      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 white-clr">
                     Here Are Ground - Breaking Features That <br class="d-none d-md-block"> Make 
                     Sellero A Cut Above The Rest                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Headline End -->
      <!-- Feature Section Start -->
      <div class="features-one">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 relative">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Complete All-In-One Online E-Selling Platform For Entrepreneurs & Marketers
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                     No more relying on costly service providers.  Sellero brings together everything you need to  <span class="w600">sell your digital products, courses, services, or any purchased Agency or PLR Rights software. </span>
                     <br><br>
                      From creating websites and stores to integrating payment solutions, showcasing your services, and managing product delivery, Sellero has you covered. All you need to do is just let this technology into action, and we'll take care of the rest., so you can focus on your business.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f1.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Create And Manage Unlimited Digital Products, Courses & Services
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                     Unlock Limitless Growth Potential with Sellero, <span class="w600">Experience the freedom to expand without limitations.</span>
                     <br><br>
                      Sellero empowers you to skyrocket your sales and profits by offering unlimited digital products, courses, and services to hungry clients. Connect with your audience for extended periods and watch your business thrive like never before.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f2.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>

      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-center">
                     Launch Fast - Create Stunning Websites/Stores In Any Niche
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f3.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  Effortlessly Create Captivating Websites and Stores with Sellero.
                  <span class="w600">Revolutionize the way you build engaging online platforms. With Sellero, choosing a template and creating a highly converting website takes only minutes.</span>
                  <br><br>
                   Tailor-made for your business or client, our templates are designed to captivate specific audiences and entice them with irresistible offers. Say goodbye to complex website creation and hello to simplified success with Sellero.
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Create Beautiful Membership Sites to Securely Deliver Products & Files
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Achieve Business Objectives with Sellero's <span class="w600">Membership Site Builder -</span>
                     <br><br>
                     Take control of your content delivery and connect with your audience like never before. Create private areas to deliver products, showcase expertise, launch courses, or share passions with ease. 
                     <br><br>
                     With Sellero, you have all the tools you need to <span class="w600"> build membership sites that align with your goals,</span> eliminating the need for third-party dependencies. Start unlocking your full potential today.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f4.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Sell Unlimited Products - Accept Payments Directly In Your Account
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Sell Unlimited Products and Take Control of Your Online Business - Sellero empowers you to unleash your business potential by offering unlimited product sales. 
                     <br><br>
                     What's more, you gain complete control by seamlessly <span class="w600">accepting payments from your global audience through PayPal and Stripe. </span> Grow your business comprehensively and thrive in every aspect with Sellero.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f5.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow1.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr">
                     Smart-Checkout Links - Receive Payments from Social Media, Emails & Any Page
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Effortless Payment Collection with Smart-Checkout Links.
                     Skip the hassle of invoices and streamline your payment process with Sellero's smart-checkout links. <span class="w600">Collect payments swiftly and efficiently on your website, social media, or any online platform.</span>
                     <br><br>
                     Enable your customers to simply click and pay for items or services using their credit card or bank account. With Sellero, receiving payments from social media, emails, or any page has never been easier.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f6.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Get 20+ RED HOT, Done- For-You Products To Quick Start
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Accelerate Your Online Marketing with 20+ Ready-to-Go, High-Converting Products. <span class="w600">Simplify your product selection process with Sellero's curated collection of 20+ top-performing, high-converting products.</span>
                     <br><br>
                      We've done the hard work for you, ensuring that you have a powerful arsenal to maximize your sales without getting overwhelmed by countless options. Get a quick start and unleash your marketing potential with Sellero's carefully chosen offerings.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f7.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>

      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     100+ Battle-Tested Templates To Build High Converting & Fast-Loading Landing Pages
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                     You get Mobile optimized and elegant templates for almost all type of marketing campaigns to <span class="w600">get everything MAX - max attention, max leads, max sales and more importantly max profits.</span>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 list-gap mt0">
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8a.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Lead Generation Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Potential Buyer Leads
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8b.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Sales Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Sales & Profits
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8c.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Secure Download Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Customer Satisfaction
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8d.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Coming Soon Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Curiosity For Your Offers
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8e.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Webinar Registration Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Registrants for Your Webinars
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8f.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Product Review & Bonus Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Earn MAX Commissions
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8g.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Event Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Bookings To Your Events
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8h.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Website Home Page-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Get MAX Exposure For Your Brand
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8i.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Lead Nurturing Pages-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Convert Max Leads into customers by educating them & a proper CTA
                  </div>
               </div>
               <div class="col text-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f8j.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
                  <div class="f-20 f-md-28 w600 lh140 black-clr text-capitalize mt20">
                     Create Any Type of Page-
                  </div>
                  <div class="f-16 f-md-20 w400 lh140 mt5 black-clr">
                     Deal Page, thank you, portfolio, contact us, legal pages & much more
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Fully Drag and Drop WYSIWYG Page Editor
                     that Requires Zero Designing or Tech Skills
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  A Next generation, pixel perfect & drag & drop editor to create whatever you want on page and wherever you want without even 1 Pixel error. We have reinvented page editor which is not like old school bootstrap editor that set your elements without your control.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f9.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                  We have designed the builder completely newbie friendly to accomplish task at pro level. Change font, colour, link, elements & much more effortlessly. Here're few-
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 list-gaps mt0">
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr1.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Image/Logo
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        <span class="w600">Upload Any Image and Edit it,</span> Even you Can use this Image in Future for any other Campaign
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr2.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Video
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        Show Any Video on Page with <span class="w600">Professional Inbuilt HLS Video Player</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr3.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Button
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Show effective CTA that <span class="w600">entices your visitors to click, sign up, read and buy.</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr4.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Section
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        <span class="w600">Edit and add a new section in 1 click.</span> You can customize it separately for Mobile and Desktop Visitors
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr5.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Headline/Text
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Create Engaging Headline with <span class="w600">Advanced Typography Editing Technology</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr6.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Countdown Timer
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Ticking Timers are legitimately one of the most effective ways to <span class="w600">create scarcity and generate more sales for you.</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr7.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Form
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">
                        Use our DFY optin-forms and <span class="w600">send leads directly to Your favourite autoresponder</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr8.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Shapes
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        Draw any Box, Circle or lines to <span class="w600">differentiate any part which helps you to get more focused by your visitors</span>
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="fea-white">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/fr9.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-22 f-md-26 w600 lh140 black-clr text-capitalize">
                        Social
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 grey-clr">                                  
                        <span class="w600">
                        Add social media icons anywhere on your site and connect visitors with brand or ask them to share your pages for viral traffic.</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-18 f-md-20 w600 lh140  black-clr">
                  And much more…you can build almost anything from scratch using this next generation editor in few minutes.
               </div>
            </div>
         </div>
      </div>

      <div class="features-three">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Inbuilt Lead Management System For Effective Contacts Management In Automation
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Tap into ready to use & beautiful Lead forms in 8 different colours. Use these forms to <span class="w600">collect maximum leads & keep them organized in your very own lead management panel.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f10.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Intelligent Analytics To Measure The Performance - Know Exactly What's Working And What's Not.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 mt-md40 black-clr">
                     <span class="w600"> Know your numbers -</span> what's performing well and what simply is not working.
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f11.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w400 lh140  black-clr">
                     <span class="w600">Checkout unique visitors, conversions stats in nice graphs on the analytics page</span> to build strategy & improve your marketing campaigns to get more Returns and Profits.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="features-twelve">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w600 lh140 white-clr text-capitalize features-title">
                     Connect Sellero With Your Favourite Tools. 20+ Integrations With Autoresponders And Other Service
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f12.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     Setup integration in few clicks and send all your leads into your favorite autoresponders for prompt communication.<span class="w600"> Send a series of personalized email messages to new leads at pre-defined interval.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="features-one">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     Create Fast-Loading, Mobile Ready Pages To Engage Every Single Mobile Visitor
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Each website & landing page created with this Sellero is <span class="w600">100% mobile-ready & loads ultra-fast on ANY mobile device</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f13.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     SEO Friendly & Social Media Optimized Sites and Pages For More Traffic
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     You get 100% SEO pages <span class="w600">to get better SERP rankings and additional search traffic.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f14.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow1.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     128 Bit Secured, SSL Encryption For Maximum Security To Your Files, Data And Websites
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     Normally you'd have to pay extra for a SSL certificate, but not with us. You're not only getting encryption more secure than Ft. Knox with Sellero, but you're getting that same SSL Encryption for every website you ever create with your account. Normally, this would run you thousands. But we'll hook you up when you sign up today at no additional charge.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f15.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow2.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                  Manage All The Pages, Products, Customers Hassle- Free,All In Single Dashboard
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     This lets you manage everything effortlessly. <span class="w600">Use our never offered before customized drag & drop business central dashboard.</span>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f16.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/proven-arrow1.webp" class="img-fluid d-none mx-auto d-md-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-32 w600 lh140 black-clr text-capitalize features-title">
                     No Domain and NO Hosting is Needed
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 black-clr">
                     <span class="w600"> With Sellero, You're 100% covered-</span>
                     No worries of monthly hosting bills, domains and websites. You simply create engaging and high-converting websites, Membership sites etc with Sellero deploy them and see the magic rolling in.
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/f17.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Feature Section End -->
<section class="more-feature-sec">
         <div class="container">
            <div class="row">
                <div class="col-12 text-center f-22 f-md-45 w700 lh140 purple-gradient text-capitalize">
                     Here Are Some More Features
                  </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="row g-0">
                      <!-- first -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-rb boxborder top-first ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k1.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Round-the-Clock Support
                              </div>
                          </div>
                      </div>
                      <!-- second -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-rb boxborder ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k2.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  No Coding, Design or Technical
                                  <br class="visible-lg "> Skills Required
                              </div>
                          </div>
                      </div>
                      <!-- third -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-b boxborder ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k3.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Regular Updates
                              </div>
                          </div>
                      </div>
                      <!-- fourth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-r boxborder ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k4.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Complete Step-by-Step Video
                                  <br class="visible-lg ">Training and Tutorials Included
                              </div>
                          </div>
                      </div>
                      <!-- fifth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder-r boxborder ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k5.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Newbie Friendly &amp; Easy To Use
                              </div>
                          </div>
                      </div>
                      <!-- sixth -->
                      <div class="col-12 col-md-4 ">
                          <div class="boxbg boxborder ">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/k6.webp " class="img-fluid d-block mx-auto ">
                              <div class="f-20 f-md-22 w500 lh150 mt15 mt-md40 text-center black-clr ">
                                  Limited Commercial License
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
         </div>
      </section>
      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- Compare Table Section Start -->
      <section class="comp-table">
         <div class="comparetable-section">
            <div class="container ">
               <div class="row ">
                  <div class="col-12 f-28 f-md-45 w600 lh150 text-center ">
                     There Is No Competition!
                  </div>
                  <div class="col-12">
                     <div class="f-md-40 f-24 w400 black-clr text-center ">
                        No Other Technology Comes Even Close to Sellero
                     </div>
                     <div class="row g-0 mt70 mt-md100 d-none d-md-flex">
                        <div class="col-md-3 col-3">
                           <div class="fist-row">
                              <ul class="f-md-17 f-16 w500 lh110">
                                 <li class="f-md-28 f-16 w600 justify-content-start justify-content-md-center">Features</li>
                                 <li class="f-md-28 w600">Starting Price </li>
                                 <li>All-in-One Online Selling &amp; Marketing Platform</li>
                                 <li>Visitors/Month </li>
                                 <li>No. of Landing Pages</li>
                                 <li> Add and Manage No. Of Products</li>
                                 <li>Sell Courses, Memberships, Services, Digital Downloads or Physical Products - All from 1 Platfrom</li>
                                 <li>Create a Business Website with Store and Customizable Header, Footer &amp; Menu</li>
                                 <li>Create Membership Sites</li>
                                 <li>Done-For-You Products </li>
                                 <li>Lead Management System with Lead Scoring</li>
                                 <li>Drag &amp; Drop WYSIWYG Page Editor </li>
                                 <li>Seamless Integration with ClickBank, JVZoo &amp; Warrior Plus?</li>
                                 <li>Professional Landing Page Templates</li>
                                 <li>Create Unlimited Templates from Scratch</li>
                                 <li> Mobile-friendly Templates / Mobile-specific Pages</li>
                                 <li>Restore 5 Previous Version of Pages</li>
                                 <li>Inbuilt SEO for Funnel and Pages </li>
                                 <li>Professional Inbuilt HLS Video Player</li>
                                 <li>Automatic 128 Bit SSL Encryption</li>
                                 <li>Custom Domain, Sub-domains</li>
                                 <li>Advanced Project &amp; Campaigns Management</li>
                                 <li>Page &amp; Funnel Hosting</li>
                                 <li>Layers &amp; In-line Page Editing</li>
                                 <li>Countdown Timer</li>
                                 <li>A/B Testing for Pages</li>
                                 <li>Work colloboration - Downloading &amp; Sharing to Team/Clients</li>
                                 <li>Store and Manage Files in Advanced Media Drive</li>
                                 <li>Optimized Images, videos and Files for Landing Pages and Funnels</li>
                                 <li>Accurate Audiences Behaviorial Analysis</li>
                                 <li>Team &amp; Client Management</li>
                                 <li>Conversion Statistics for All of Your Pages</li>
                                 <li>Customized Drag &amp; Drop Dashboard</li>
                                 <li>OTP Enabled Login</li>
                                 <li>Accurate Analysis for Team Member's Activities</li>
                                 <li>Pixabay, Pexels, Shutterstocks Integration</li>
                                 <li>Drive Integration</li>
                                 <li>Autoresponder, Webinar, CRM Integrations</li>
                                 <li>Premium Customer Support</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-9 col-md-9">
                           <div class="row flex-nowrap gx-0">
                              <div class="col">
                                 <div class="second-row">
                                    <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                                       <li class="f-md-20 f-16 white-clr">
                                          <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:85px;">
                                             <defs>
                                                <style>
                                                   .cls-1 {
                                                      fill: #fff;
                                                   }
                                             
                                                   .cls-2 {
                                                      fill: #1fe29f;
                                                   }
                                             
                                                   .cls-3 {
                                                      fill: #7764ff;
                                                   }
                                                </style>
                                             </defs>
                                             <g id="Layer_1-2" data-name="Layer 1">
                                                <g>
                                                   <g>
                                                      <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                                                      <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                                                      <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                                                      <g>
                                                         <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                                                         <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                                                      </g>
                                                   </g>
                                                   <g>
                                                      <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                                                      <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                                      <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                                      <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                                      <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                                      <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                                                      <g>
                                                         <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                                                         <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                                                         <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                                                      </g>
                                                   </g>
                                                </g>
                                             </g>
                                          </svg>
                                       </li>
                                       <li class="f-md-28 w600" style="line-height:36px;"> $37 <br>OneTime</li>
                                       <li class="w600 f-md-20">
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li class="f-md-20">
                                          Full Free-Form
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptwhitetick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                       <li></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col">
                                 <div class="third-row">
                                    <ul class="f-md-16 f-14 w500 lh120">
                                       <li class="f-md-26 f-16 w600"><span>Click Funnels</span></li>
                                       <li class="f-md-24 w600">$97/Month</li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li class="f-md-20">
                                          20K Visitors/Month
                                       </li>
                                       <li class="f-md-20">
                                          100
                                       </li>
                                       <li class="f-md-20">Limited</li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li class="f-md-20">Block Based</li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>Manual Cloudflare integration required</li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col">
                                 <div class="forth-row">
                                    <ul class="f-md-16 f-14 w500 lh120">
                                       <li class="f-md-26 f-16 w600"><span>Shopify</span></li>
                                       <li class="f-md-24 w600">$29/Month</li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="Cross">
                                       </li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="Cross">
                                       </li>
                                       <li class="f-md-20">
                                          Limited
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li class="f-md-20">Block Based</li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col">
                                 <div class="fifth-row">
                                    <ul class="f-md-16 f-14 w500">
                                       <li class="f-md-26 f-16 w600"><span>Lead Pages</span></li>
                                       <li class="w600 f-md-24"> $25/Month </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li class="f-md-20">
                                          Unlimited
                                       </li>
                                       <li class="f-md-20">
                                          Limited
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li class="f-md-20 w500">
                                          Block Based
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li class="f-md-20 w500">
                                          Page Only
                                       </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                       <li>
                                          <img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right">
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col">
                                 <div class="sixth-row">
                                    <ul class="f-md-16 f-14 ">
                                       <li class="f-md-26 f-16 w600"><span>Convertri</span></li>
                                       <li class="w600 f-md-24"> $53/Month </li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li class="f-md-20 w500">25K visitor/Month</li>
                                       <li class="f-md-20 w500">500</li>
                                       <li class="f-md-20 w500 ">Limited</li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li class="f-md-20 w500">Full Free Form</li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="Arrow Right"></li>
                                       <li><img src="https://cdn.oppyotest.com/launches/sellero/special/images/ptcross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
         
                     <div class="row d-md-none mt30">
                        <div class="col-12">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/table.webp" class="img-fluid mx-auto d-block" alt="table">
                        </div>
                     </div>
         
                     <div class="mt-md30 mt20 f-18 f-md-20 w400 lh150 "><span class="w600 ">Note :</span> 
                        All the features mentioned in the above table are bifurcated in different upgrade options according to the need of individual users and the features that you will get with purchase of Sellero START or PRO AGENCY plan are mentioned in the pricing table below on this page.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Compare Table Section End -->

      <!-- no-comparison Section Start -->
      <section class="no-comparison">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 lh140 black-clr">
                     There Is <span class="w700 purple-gradient">NO Comparison</span>  Of Futuristic <br class="d-none d-md-block"> Sellero Technology...
                     </span>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7 order-md-2">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     Correct! There's no match to the power-packed Sellero because it's built with years of experience, designing, coding, debugging & real user testing to <span class="w600"> get you guaranteed results.</span> 
                     <br><br>
                     <span class="w600"> Sellero brings you speed & ease of use on a silver platter.</span> It is highly optimized to build only high-converting and fast-loading Sites to literally crush your competition.
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/quickfunnel-thumbsup.webp" alt="Sellero Thumbsup" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh140 black-clr">
                     <span class="w600"> So, grab Sellero today for this low one-time price because, after launch, it will never be available again at this price.</span>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- no-comparison Section End -->

      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- thatsnotall-section Start -->
      <div class="thatsnotall-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mx-auto">
                  <div class="f-24 f-md-50 w600 white-clr red-brush m-0">
                     That's Not All
                  </div>
                  <div class="f-28 f-md-38 w600 lh140 black-clr mt20 mt-md30">
                  With Sellero, You Can Also Tap Into The Very Fast Growing $398 Billion Info-training And E-learning Industry.
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 black-clr mt20 mt-md30">
                  Just Upload Your Own or Any Good Purchased White-label/PLR Rights Courses & Start Selling Right Away.
                     <br><br>
                     Set An Automated Traffic Source with Cost-Effective Paid Ads.
                  </div>
                  <div class="f-22 f-md-34 w600 lh140 black-clr mt20 mt-md30 thats-shape">
                  Enjoy Non-Stop Traffic, Leads, Sales & Profits 24/7, <br class="d-none d-md-block"> 365 Days a Year - 100% Handsfree.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-12 d-flex align-items-center">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/no-sign.png" class="img-fluid d-block mx-auto">
                  <div class="f-md-45 f-28 w600 text-center lh140 red-clr">
                  Expertise Or Technical Know How Needed!
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 f-20 f-md-24 text-center text-md-start w400 lh160 black-clr mt20 mt-md50">
               You can share your knowledge! Teach others what you know...<br> from Yoga to Weight-loss Exercises, Body Building, Healthy Cooking, Wine-Making, Chocolate & Cake Baking, Gardening, Fixing Computers, Laptops, Mobile Shortcuts, Reviews, Tips & Tricks, Etiquettes, Hospitality, Self-Grooming, Personality-Developments, Arts & Crafts, Farming, Car or Bike Repair & Modifications, Investments, Banking, Foreign Languages.
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <img src="https://cdn.oppyotest.com/launches/sellero/special/images/thatnotall-img.webp" class="img-fluid d-block mx-auto">
           </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-3 mx-auto">
                 <img src="https://cdn.oppyotest.com/launches/sellero/special/images/thatnotall-boy.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-9">
                  <div class="f-md-45 f-28 w600 text-center lh140 black-clr">
                     Yes, Sell Anything & Everything...  <br class="d-none d-md-block"> No Limitations, No Profit Sharing! 
                  </div>
               </div>
            </div>
         </div>
      </div>



            <div class="license-section">
               <div class="container ">
                  <div class="row ">
                     <div class="col-12 f-28 f-md-45 w400 white-clr text-center lh140">
                     Alert! FREE Limited Time Upgrade:<br class="d-none d-md-block"> 
                        <span class="w600">Get Commercial License to Use SELLERO To Create An Incredible Income Serving Your Customers!</span>
                     </div>
                     <div class="col-12 mt20 mt-md30 f-md-22 f-18 w400 lh140 white-clr text-center ">
                     As we have shown you, there are tons of businesses which enter the online market every day - yoga gurus, fitness centers or anyone else who wants to grow online. They desperately need your services & are eager to pay you BIG for them.
                     </div>
                  </div>
                  <div class="row align-items-center mt20 ">
                     <div class="col-12 col-md-6">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/agency-license-img.webp" class="img-fluid d-block mx-auto img-animation" alt="Agency License">
                     </div>
                     <div class="col-12 col-md-6 mt20">
                        <div class="f-md-22 f-18 w400 lh140 white-clr ">
                           Boost your client's business by supercharging their websites, pages, funnels and membership sites, create &amp; sell e-learning channels, and help businesses improve their profits with fast loading videos right from your home.
                           <br><br>
                           They will pay top dollar to you when you deliver top notch services to them. We've taken care of everything so that you can deliver those services simply and easily.
                           <br><br>
                           <span class="w600">Note:</span> This special Agency licence is being included in the Pro offer ONLY for this launch. Take advantage of it now because it will never be offered again.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="thatsnotall-section">
               <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="huge-shape">
                     <div class="col-12">
                        <div class="f-md-38 f-28 w600 lh140 text-center white-clr">
                           Sellero Brings You <br class="d-none d-md-block"> A HUGE Opportunity on A Silver Platter!
                        </div>
                     </div>
                     <div class="col-12 mt-md50 mt20">
                        <div class="row align-items-center">
                           <div class="col-12 col-md-6">
                              <img src="https://cdn.oppyotest.com/launches/sellero/special/images/horn.webp" class="img-fluid d-block mx-auto" alt="HUGE Opportunity">
                           </div>
                           <div class="col-12 col-md-6 f-md-32 f-24 w600 lh140 white-clr mt20 mt-md0">
                              You just need to capitalize on it the easy way and blow away the competition.
                           </div>
                        </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <!-- thatsnotall-section End -->


 <!-- Silver Platter Section End -->
    <div class="potential-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-45 f-24 w600 text-center black-clr">
                        Here's A <span class="purple-gradient">Never-Ending List Of Businesses Ready</span><br class="d-none d-md-block"> To Pay You For Your Online Marketing Services...
                    </div>
                </div>
            </div>
            <div class="row row-cols-2 row-cols-md-3 gap30 justify-content-center mt0 mt-md-70">
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n1.webp" class="img-fluid mx-auto d-block" alt="Business Coaches">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Business Coaches 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n2.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">                 
                         Affiliate Marketers
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n3.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         E-Com Sellers
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n4.webp" class="img-fluid mx-auto d-block" alt="All Info">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         All Info  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n5.webp" class="img-fluid mx-auto d-block" alt="Gym">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Gym 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n6.webp" class="img-fluid mx-auto d-block" alt="Music Classes">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Music Classes
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n7.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Sports Clubs
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n8.webp" class="img-fluid mx-auto d-block" alt="Bars">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Bars  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n9.webp" class="img-fluid mx-auto d-block" alt="Restaurants">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Restaurants 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n10.webp" class="img-fluid mx-auto d-block" alt="Hotels">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Hotels
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n11.webp" class="img-fluid mx-auto d-block" alt="Schools">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Schools
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n12.webp" class="img-fluid mx-auto d-block" alt="Churches">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Churches  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n13.webp" class="img-fluid mx-auto d-block" alt="Taxi Services">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Taxi Services 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n14.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Garage Owners
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n15.webp" class="img-fluid mx-auto d-block" alt="Dentists">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Dentists
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n16.webp" class="img-fluid mx-auto d-block" alt="Carpenters">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Carpenters  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n17.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Chiropractors 
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n18.webp" class="img-fluid mx-auto d-block" alt="LockSmiths">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Locksmiths
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n19.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Home Tutors
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n20.webp" class="img-fluid mx-auto d-block" alt="Real-Estate">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Real-Estate  
                      </div>
                   </div>
                </div>
                <div class="col">
                   <div class="feature-list-box">
                      <img src="https://cdn.oppyotest.com/launches/sellero/special/images/n21.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
                      <div class="f-18 f-md-20 w500 lh140 mt15">
                         Lawyers 
                      </div>
                   </div>
                </div>
             </div>
        </div>
    </div>
    
    <!-----Proven Technology------>
    
    
      <!-- Cta Btn Sec Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                   Free Commercial License + A Low One-Time Price
                  </div>
                  <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Btn Sec End -->

      <!-- Bonus Section Start -->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="f-18 f-md-22 w400 lh140 text-center">
                     When You Grab Your Sellero Account Today, You'll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Live Training - 0-10k a Month With Sellero (First 1000 buyers only - $1000 Value)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Start Your Own SIX Figure Business In FEW Minutes. This awesome LIVE training will help you to build a SIX FIGURE Business PLUS THREE lucky attendees will be selected randomly to win $100 cash each! And there will be a Live Q & A Session at the end of the training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #2
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on How To Start Online Coaching Business
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Learn how to start online coaching business sell information you're passionate about. Combine this useful info with Sellero Reloaded, and get best results for your business like you always aspired.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Lead Generation Video Workshop
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Get proven tips and tricks on how to drive laser targeted leads to your offers and boost your email list in a cost effective manner. When used with Sellero Reloaded, it reaps long term business benefits.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on How To Make Money With Affiliate Marketing
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Complete video tutorials you can discover how to make money with affiliate marketing - even if you are a complete beginner. Use this proven information along with Sellero Reloaded, and see results flowing in like a pro.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #5
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Video Training on How to Boost Your Online Sales
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              The web makes it possible to connect with a gigantic audience of billions of people and to provide them with all kinds of products in an entirely automated matter. If you can create or find a great product and then share it with the right people. There are people out there who are genuinely making hundreds of thousands of dollars every month from this kind of strategy. With this 10-part video course you will discover how to boost your own product sales and build your audience.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="https://cdn.oppyotest.com/launches/sellero/special/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w600 white-clr lh240 text-center ">
                        That's A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w600">$3300</span>
                     </div>
                  </div>
               </div>
            </div>

            <!-- <div class="row mt20 mt-md50">
               <div class="col-12 f-24 f-md-41 w400 black-clr text-center lh140">
                   Alert! FREE Limited Time Upgrade:
               </div>
               <div class="col-12 f-24 f-md-41 w600 black-clr text-center lh140 mt10">
                  Get Commercial License to Use SELLERO To Create An Incredible Income Serving Your Customers!  
               </div>
               <div class="col-12 mt20 mt-md30 f-md-22 f-20 w400 lh150 black-clr text-center ">
                  As we have shown you, there are tons of businesses that enter the online market every day - yoga gurus, fitness centers  or anyone else who wants to grow online. They desperately need your services & are eager to pay you BIG for them.  
               </div>
           </div> -->
         </div>
      </div>
      <!---Bonus Section End-->

      <!-- amazing-sound -->
      <!-- <div class="amazing-sound">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="col-12 text-center">
                     <span class="f-md-45 f-28 lh140 w700 white-clr heading-design">WOW! That Sounds An Amazing Deal
                     </span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="f-24 f-md-36 w400 lh140 text-center">
                     But “I'm Just Getting Started Online. Can Sellero<br class="d-none d-md-block">  Help Me Build An Online Business Too?”
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7">
                  <div class="f-md-20 f-18 w400 lh140 black-clr ">
                      <span class="f-md-45 f-28 lh140 w600 text-left black-clr">Absolutely,</span>
                     <br><br>
                     It does not matter whether you have an established business, or you are just getting started online, … Sellero Opens-up a HUGE Business Opportunity For YOU!
                     <br><br><span class="w600">As we are giving you 20+ HOT Done-For-You Products so you can Start Selling them immediately.</span>  <br><br>
                     Our Step-by-Step Training and Dedicated Customer Success Team will help You Start & Grow Your Business in no time. PLUS
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0 ">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/sounds-great-img.png" alt="Sounds Great" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div> -->
      <!-- amazing-sound-end -->

      <!-- License Section Start -->
      <!-- <div class="license-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w400 white-clr text-center lh140">
                  This Very LIMITED Agency License*<br class="d-none d-md-block"> Will Allow You To <span class="w600">Tap Into A UNIQUE Opportunity To Enter $200 Billion Freelancing Industry</span>
               </div>
               <div class="col-12 mt20 mt-md30 f-md-20 f-18 w600 lh140 white-clr text-center ">
                  As we have shown you, there are tons of businesses that need your services & eager to pay you monthly for your services.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/agency-license-img.webp" class="img-fluid d-block mx-auto img-animation" alt="Agency License">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <div class="f-md-20 f-18 w400 lh140 white-clr ">
                     Boost your client's business by supercharging their websites, pages, funnels and membership sites, create & sell e-learning channels, and help businesses improve their profits with fast loading videos right from your home.
                     <br><br>
                     They will pay top dollar to you when you deliver top notch services to them. We've taken care of everything so that you can deliver those services simply and easily.
                     <br><br>
                     <span class="w600">Note:</span> This special Agency licence is being included in the Pro offer ONLY for this launch. Take advantage of it now because it will never be offered again.
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- License Section End -->

      <!-- Guarantee Section Start -->
      <div class="noneed-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-20 f-md-22 w400 lh140 text-center">
                     Just 7 Minutes it Takes to Start Profiting Online…
                  </div>
                  <div class="f-md-45 f-28 w700 black-clr lh140 text-center mt15">
                     With SELLERO, Turn Your Worries Into <br class="d-none d-md-block">
                     A Profitable Opportunity
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 w400 lh140">
                           <ul class="noneed-list pl0">
                              <li><span class="w600">No need to pay monthly</span> for expensive hosting & software services</li>
                              <li class="w600">No more profit sharing or transaction fee.</li>
                              <li><span class="w600">No need to lose your traffic & leads</span> with 3rd party marketplaces- EVERYTHING is in your control!</li>
                              <li><span class="w600">No need to spare a thought</span> for building website, store, or e-learning sites!</li>
                              <li><span class="w600">No need to worry</span> about lack of technical or design skills</li>
                              <li><span class="w600">No more complicated & time-consuming processes.</span> It is an easy 3 steps & your products are ready to get published within minutes.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/worry.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 f-18 f-md-20 w400 lh140 mt20">
                        And… Finally say YES to living a LAPTOP lifestyle that gives you an ability to spend more time with your family and have complete financial FREEDOM.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 white-clr text-center col-12 mb-md40">Test Drive It Risk FREE For 30 Days
               </div>
               <div class="col-md-6 col-12 mt15 mt-md0">
                  <div class="f-18 w400 lh140 white-clr"><span class="w600">Your purchase is secured with our 30-day money back guarantee.</span>
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your clients risk free. If you face any Issue or don't get results you desired after using it, just raise a ticket on support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <img src="https://cdn.oppyotest.com/launches/sellero/special/images/riskfree-img.webp " class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section" id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-18 f-md-20 w400 text-center lh140">
                     Take Advantage of Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 text-center mt10 ">
                     Get SELLERO For a Low <br class="d-none d-md-block ">One-Time- Price, No Monthly Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 black-clr">
                     Use Coupon Code <span class="blue-clr w600">"SELL3"</span> for an Additional <span class="blue-clr w600 "> $3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50 ">
                  <div class="row gx-5">
                     <div class="col-12 col-md-6 ">
                        <div class="table-wrap ">
                           <div class="table-head text-center ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                                 <defs>
                                   <style>
                                     .cls-1s {
                                       fill: #1fe29f;
                                     }
                               
                                     .cls-2s {
                                       fill: #121733;
                                     }
                               
                                     .cls-3s {
                                       fill: #7764ff;
                                     }
                                   </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                   <g>
                                     <g>
                                       <path class="cls-3s" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                                       <path class="cls-3s" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                                       <path class="cls-3s" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                                       <g>
                                         <circle class="cls-3s" cx="130.39" cy="198.16" r="10.38"></circle>
                                         <circle class="cls-3s" cx="75.08" cy="198.16" r="10.38"></circle>
                                       </g>
                                     </g>
                                     <g>
                                       <path class="cls-2s" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                                       <path class="cls-2s" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                       <path class="cls-2s" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                       <path class="cls-2s" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                                       <path class="cls-2s" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                                       <path class="cls-2s" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                                       <g>
                                         <path class="cls-2s" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                                         <polygon class="cls-1s" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                                         <path class="cls-1s" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </g>
                              </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 personal-shape white-clr">Start</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list pl0 f-18 lh140 w400 black-clr">
                                 <li>Complete All-In-One E-Selling Platform</li>
                                 <li>Create And Manage Upto 10 Business Sites/Domains</li>
                                 <li>10,000 Visitors/Month</li>
                                 <li>Sell Unlimited Products - Digital Products, Physical Goods, And Services</li>
                                 <li>Quick-Start Your Online Business With 20+ Done-For-You Products</li>
                                 <li>Create & Manage Unlimited Products</li>
                                 <li>Launch FAST - Create Beautiful Websites & Stores for Your Business</li>
                                 <li>Create Beautiful Memberships To Deliver Products Securely</li>
                                 <li>Create UNLIMITED Beautiful Fast-Loading Landing Pages Easily</li>
                                 <li>Accept Payments Through Paypal & Stripe With Zero Fees</li>
                                 <li>Generate & Manage Upto 10,000 Leads</li>
                                 <li>Smart-Checkout Links - Directly Receive Payments From Social Media, Emails And On Any Page.</li>
                                 <li>Seamless Integration to Sell on Clickbank, JVZoo & WarriorPlus & Deliver Products On Automation.</li>
                                 <li>Fully Drag & Drop Advanced WYSIWYG Page Editor</li>
                                 <li>100+ DFY Proven Converting, Mobile Responsive & Ready-To-Go Landing Page Templates</li>
                                 <li>Create Unlimited Page Templates From Scratch</li>
                                 <li>Manage Leads Effortlessly & Make The Most From Them With Our Powerful Lead Management Feature.</li>
                                 <li>Cutting-Edge Integration With 20+ Autoresponders To Send Emails To Your Subscribers On Automation</li>
                                 <li>Mobile Friendly Fast Loading Websites & Pages To Capture All Mobile Visitors</li>
                                 <li>Manage All The Pages, Products, Customers Hassle- Free, All In Single Dashboard</li>
                                 <li>Inbuilt SEO Management For Website And Landing Pages</li>
                                 <li>128-Bit SSL Encryption For Maximum Security Of Your Data & Files</li>
                                 <li>100% GDPR And CAN-SPAM Compliant</li>
                                 <li>Customized Drag & Drop Business Central Dashboard</li>
                                 <li>Completely Cloud-Based - No Domain, Hosting Or Installation Required</li>
                                 <li>Mastermind FB Group</li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training</li>
                                 <li>Premium Customer Support</li>
                                 <li>30 Days MoneyBack Guarantee</li>
                                 <li class="cross-sign">Commercial License Included</li>
                                 <li class="cross-sign">Use For Your Clients</li>
                                 <li class="cross-sign">Provide High In Demand Services</li>
                                 <li class="headline f-md-24">Fast Action Bonuses</li>
                                 <li class="cross-sign">Bonus #1 - Live Training</li>
                                 <li class="cross-sign">Bonus #2 - Video Training To Start E-Learning/Course Selling Business</li>
                                 <li class="cross-sign">Bonus #3 - Lead Generation Workshop</li>
                                 <li class="cross-sign">Bonus #4 - Training On How To Make Money Through Affiliate Markeitng</li>
                                 <li class="cross-sign">Bonus #5 - How to Boost Your Online Sales</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
                                 <a href="https://www.jvzoo.com/b/108915/396707/2"><img src="https://i.jvzoo.com/108915/396707/2" alt="Sellero Personal" border="0" class="img-fluid d-block" /></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                                 <defs>
                                   <style>
                                     .cls-1 {
                                       fill: #fff;
                                     }
                               
                                     .cls-2 {
                                       fill: #1fe29f;
                                     }
                               
                                     .cls-3 {
                                       fill: #7764ff;
                                     }
                                   </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                   <g>
                                     <g>
                                       <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                                       <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                                       <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                                       <g>
                                         <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                                         <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                                       </g>
                                     </g>
                                     <g>
                                       <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                                       <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                       <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                       <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                                       <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                                       <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                                       <g>
                                         <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                                         <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                                         <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                                       </g>
                                     </g>
                                   </g>
                                 </g>
                              </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 commercial-shape white-clr">Commercial</div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 lh140 w400 white-clr">
                              <li>Complete All-In-One E-Selling Platform</li>
                              <li>Create And Manage Upto 50 Business Sites/Domains</li>
                              <li>Unlimited Visitors/Month</li>
                              <li>Sell Unlimited Products - Digital Products, Physical Goods, And Services</li>
                              <li>Quick-Start Your Online Business With 20+ Done-For-You Products</li>
                              <li>Create & Manage Unlimited Products</li>
                              <li>Launch FAST - Create Beautiful Websites & Stores for Your Business</li>
                              <li>Create Beautiful Memberships To Deliver Products Securely</li>
                              <li>Create UNLIMITED Beautiful Fast-Loading Landing Pages Easily</li>
                              <li>Accept Payments Through Paypal & Stripe With Zero Fees</li>
                              <li>Generate & Manage Upto 30,000 Leads</li>
                              <li>Smart-Checkout Links - Directly Receive Payments From Social Media, Emails And On Any Page.</li>
                              <li>Seamless Integration to Sell on ClickBank JVZoo, WarriorPlus & Deliver Products On Automation.</li>
                              <li>Fully Drag & Drop Advanced WYSIWYG Page Editor</li>
                              <li>100+ DFY Proven Converting, Mobile Responsive & Ready-To-Go Landing Page Templates</li>
                              <li>Create Unlimited Page Templates From Scratch</li>
                              <li>Manage Leads Effortlessly & Make The Most From Them With Our Powerful Lead Management Feature.</li>
                              <li>Cutting-Edge Integration With 20+ Autoresponders To Send Emails To Your Subscribers On Automation</li>
                              <li>Mobile Friendly Fast Loading Websites & Pages To Capture All Mobile Visitors</li>
                              <li>Manage All The Pages, Products, Customers Hassle- Free, All In Single Dashboard.</li>
                              <li>Inbuilt SEO Management For Website And Landing Pages</li>
                              <li>128-Bit SSL Encryption For Maximum Security Of Your Data & Files</li>
                              <li>100% GDPR And CAN-SPAM Compliant</li>
                              <li>Customized Drag & Drop Business Central Dashboard</li>
                              <li>Completely Cloud-Based - No Domain, Hosting Or Installation Required</li>
                              <li>Mastermind FB Group</li>
                              <li>Easy And Intuitive To Use Software With Step By Step Video Training</li>
                              <li>Premium Customer Support</li>
                              <li>30 Days MoneyBack Guarantee</li>
                              <li>Commercial License Included</li>
                              <li>Use For Your Clients As Well</li>
                              <li>Provide High In Demand Services</li>
                              <li class="headline f-md-24">Fast Action Bonuses</li>
                              <li>Bonus #1 - Live Training</li>
                              <li>Bonus #2 - Video Training To Start E-Learning/Course Selling Business</li>
                              <li>Bonus #3 - Lead Generation Workshop</li>
                              <li>Bonus #4 - Training On How To Make Money Through Affiliate Markeitng</li>
                              <li>Bonus #5 - How to Boost Your Online Sales</li>

                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://www.jvzoo.com/b/108915/396539/2"><img src="https://i.jvzoo.com/108915/396539/2" alt="Sellero Commercial" border="0" class="img-fluid d-block mx-auto" /></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Agency License Is ONLY Available With The Purchase Of Agency Plan
                  </div>
               </div> -->
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->

      <!-- Contact Sec Start -->
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              Thanks for checking out Sellero!
                           </div>
                        </div>
                     </div>
                     <div class="row mt20 mt-md80 justify-content-between">
                        <div class="col-12 col-md-8 mx-auto">
                           <div class="row">
                              <div class="col-12 col-md-6">
                                 <div class="contact-shape">
                                    <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/amit-pareek.webp" class="img-fluid d-block mx-auto minus ls-is-cached lazyloaded" alt="Amit Pareek" src="https://cdn.oppyotest.com/launches/sellero/jv/images/amit-pareek.webp">
                                    <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Dr. Amit Pareek
                                    </div>
                                 </div>
                              </div>
                              <div class="col-12 col-md-6 mt20 mt-md0">
                                 <div class="contact-shape">
                                    <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp" class="img-fluid d-block mx-auto minus ls-is-cached lazyloaded" alt="Er. Atul Kumar" src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp">
                                    <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Er. Atul Kumar
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Contact Sec End -->

      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr">
                  Frequently Asked <span class="gradient-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row ">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do I need to download or install Sellero somewhere?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NO! You just create an account online and you can get started immediately. Sellero is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. And it works across all browsers and all devices including Windows and Mac.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">                              
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don't offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you did everything before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140">
                              Is Sellero compliant with all guidelines & compliances?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party services which are not a part of Sellero while choosing it for your business.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              What is the duration of service with this Sellero launch special deal?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary renewal to our founder members for buying from us early.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              How is Sellero different from other available tools in the market?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Well, we have a nice comparison chart with other service providers. We won't like to boast much about our software, but we can assure you that this is a cutting-edge technology that will enable you to create and sell stunning niche websites at such a low introductory price.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its 100% optional & not mandatory for working with Sellero. Those are recommended if you want to multiply your benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Will I get any training or support for my questions?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member's area. You will also get Premium Customer Support so you never get stuck or have any issues.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Is Sellero Windows and Mac compatible?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We've already stated that Sellero is a web-based solution. So, it runs directly on the web and works across all browsers and all devices.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh140 ">If you have any query, simply visit our  <a href="mailto:support@oppyo.com" class="blue-clr ">support@oppyo.com</a></div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->

      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 passport-content">
                  <div class="text-center">
                     <div class="f-45 f-md-45 lh140 w600 text-center black-clr caveat final-shape">
                        FINAL CALL
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0 mb20 mb-md50">
                     You Still Have The Opportunity To Take Your Game To The Next Level… <br> Remember, It's Your Make Or Break Time!
                  </div>
                  <!-- CTA Btn Section Start -->
                  <div class="col-12">
                     <div class="f-22 f-md-32 w700 lh140 text-center white-clr">
                      Free Commercial License + A Low One-Time Price
                     </div>
                     <div class="f-20 f-md-22 lh140 w400 text-center mt10 white-clr">
                        Use Coupon Code <span class="w600 green-clr">"SELL3"</span> for an Additional <span class="w600 green-clr">$3 Discount</span>
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn">Get Instant Access To Sellero</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyotest.com/launches/sellero/special/images/payment-options-white.webp" class="img-fluid mx-auto d-block" alt="visa">
                     </div>
                  </div>
                  <!-- CTA Btn Section End -->
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #1fe29f;
                         }
                   
                         .cls-3 {
                           fill: #7764ff;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"/>
                           <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"/>
                           <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"/>
                           <g>
                             <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"/>
                             <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"/>
                           </g>
                         </g>
                         <g>
                           <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"/>
                           <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                           <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"/>
                           <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"/>
                           <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"/>
                           <g>
                             <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"/>
                             <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"/>
                             <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-20 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
                  <ul class="footer-ul w400 f-md-20 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   
   <!-- timer --->
   <script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            showRemaining();
            //tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               //timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-45 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-45 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-45 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-45 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
            }
         
         }


      //timerBegin(59 * 60); 




      function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 59*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         timerBegin(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();



      </script>
            <!--- timer end-->


   </body>
</html>
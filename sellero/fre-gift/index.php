<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>Sellero | Webinar Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="Sellero | Webinar Gift">
    <meta name="description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta name="keywords" content="Sellero | Webinar Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Sellero | Webinar Gift">
    <meta property="og:description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta property="og:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Sellero | Webinar Gift">
    <meta property="twitter:description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta property="twitter:image" content="https://www.sellero.co/webinar-gift/thumbnail.png">

   <!-- Font-Family -->
    <link rel="icon" href="https://cdn.oppyotest.com/launches/sellero/common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Work+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="../common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	
	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://sellero.co/special/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
			<div class="col-12 text-center">
                    <div class="col-12 white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 green-clr">Dr. Amit Pareek's</span> for SuperVIP Customers
                    </div>
                    <div class="uy col-12 mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <b class="f-20 f-md-24 purple-clr">Congratulations!</b> you have also <b><span class="purple-clr">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "AMITVIP" $10 OFF</span></b> for my Lightning Fast Software <b><span class="purple-clr">"VidHostPro"</span></b> launch on <br class="d-none d-md-block"> <span class="purple-clr w700">03rd May 2023  @ 11:00 AM EST</span>
                        </div>

                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>

                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.oppyo.com/hc/en-us" target="_blank" class="purple-clr">https://support.oppyo.com/</a>
                        </div>
                    </div>
                </div>
               <div class="col-12 mt-md50 mt20 text-center">
			   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                    <defs>
                      <style>
                        .cls-1 {fill: #fff;}
                        .cls-2 {fill: #1fe29f;}
                        .cls-3 {fill: #7764ff;}
                      </style>
                    </defs>
                    <g id="Layer_1-2" data-name="Layer 1">
                      <g>
                        <g>
                          <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                          <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                          <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                          <g>
                            <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                            <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                          </g>
                        </g>
                        <g>
                          <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                          <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                          <g>
                            <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                            <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                            <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                 </svg>
               </div>
				<div class="col-12 mt-md50 mt20 head-design relative">
				<div class="gametext d-none d-md-block">
                     Cutting Edge Technology&nbsp;
                   </div>
					<div class="f-md-50 f-28 w700 text-center black-clr lh140">
					Sell Courses, Products & Agency Services With Done-for-You Website <span class="w500"> In Next 7 Minutes Flat.</span>
					</div>
				</div>
                   <div class="col-12 mt-md40 mt20 f-22 f-md-26 w500 text-center lh140 white-clr text-capitalize">
				   Sell your Own or White Labelled/Reseller/PLR Licensed Products |
				   <br class="d-none d-md-block"> Preloaded with 40+ Ready to Use Products to Start Selling Right Away 
                   </div>
				   <div class="col-12 white-clr text-center">
                   <div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
				   Download Your Free Gifts Below
                   </div>
                   <div class="mt10 f-20 f-md-32 lh160 ">
                       <span class="tde">Enjoy Your Extra Bonus Below</span>
                   </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
               </div>
            </div>
         </div>
      
	<!-- Proudly Section End -->


	<!-- Bonus #1 Section Start -->
	<div class="section-bonus mt30 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">Google Ads Mastery</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  	Google AdWords is one of the most powerful advertising tools ever created. It deals with millions of searches by internet users every day.
								<br><br>
								It gives business owners a unique opportunity to convert many of these people into business leads and customers.
								<br><br>
								This guide will show you EXACTLY how to set up your Google AdWords campaigns properly, so you get more clicks, cheaper clicks, slash your ad costs, and maximize your return on investment (ROI).
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
				
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row align-content-center">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
						   <div class="col-12 ">
								<div class="bonus-title-black-bg">
					   				<div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 2</div>
								</div>
							</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Camping Niche Software
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
							  Dominate Your Competition in Your Niche!
								<br><br>
								If you are a niche marketer, doing keyword research is a must to learn more about your niche and at the same time, know the topics around the niche.
								<br><br>
								But sometimes, it can be a pain if you manage several niche sites. That's why most successful niche marketes used softwares to help them automate the process.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Keyword Tool
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  	Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of...
								<br><br>
								The purpose of this is to generate multiple combinations of keywords which you can further filter and then create a final "Master" list. The list can either be saved or exported as a file. This is one program you definitely need to use in order to fully appreciate!
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1a01IaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
			 	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
						   <div class="col-12 xstext1">
					<div class="bonus-title-black-bg">
				   		<div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 4</div>
					</div>
			 	</div>
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Auto Content Pro
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
							  A Quick And Easy Way To Get Loads Of Search Engine Friendly, Automatically Updated Content On Your Websites - All Delivered On Autopilot!
                       <br><br>
                       Get Free Automatically Updated Article Content - simply visit the appropriate article directory site, select a few options and then paste the supplied snippet of code into your web page. Instantly you get automatically updated content shown on your web page!
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWtwT1ZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Secret 4 Figure Affiliate Promotion
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							Learn the Secrets of the 4-Figure Affiliate Promotion!
							<br><br>
							If you want to make money online, affiliate marketing could be one of the best way to do it. Well, if you have been doing it for awhile and you still don't have good results in your campaign, trying other marketing technique could lead you to the answer you keep asking.
							<br><br>
							The good thing is that, the answer that you are looking for is inside this product which you are about to learn the secret strategies that successful affiliate marketers are doing on their promotion.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->


	<!-- Bonus #6 Section Start -->
	<div class="section-bonus mt30 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block" alt="Bonus6">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">Abstract Image Collection </div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							    Ready to use abstract images for your needs!
								<br><br>
								Images like photos and digital graphics are few of the media that is very useful in to many people. Image is also a good media to attract people to your business both online and offline.
								<br><br>
								Inside this product is a bundle of abstract graphic images that you can use as website background or a quote background that you can post online.
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TlZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 Section End -->

	<!-- Bonus #7 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
				
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row align-content-center">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
						   <div class="col-12 ">
								<div class="bonus-title-black-bg">
					   				<div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 7</div>
								</div>
							</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Easy Video Sales Pages
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
							    Create video sales pages using a proven, winning formula! Simple sucessful formula for quick sales pages all the time!
								<br><br>
								Sales page plays a very big role in converting your website visitors into buyers. If you create a sales page for granted, you will just waste your time, effort and money in selling your services or products you offer.
								<br><br>
								In this amazing software, you are about to have those qualifications and expect a huge sale in your offers.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TmEzaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 Section End -->

	<!-- Bonus #8 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Content Spin Bot 
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  Revolutionary New Software Creates Unique Articles For Better Search Engine Rankings, More Vistors And Ultimately Make Money Online!
							  <br><br>
							  We've been helping THOUSANDS Of Webmasters & Marketers Start & Grow their own Internet Business... Today Is YOUR Turn!
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1Tk1IaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 Section End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
			 	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
						   <div class="col-12 xstext1">
					<div class="bonus-title-black-bg">
				   		<div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 9</div>
					</div>
			 	</div>
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Sales Bot Generator 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
							  Unleash These 24 Hour Online Sales Agents & Immediately Skyrocket Conversions, Sales & Profits!
                       <br><br>
                       Sales Bot Generator is a simple little script you can use to set up your personal exit traffic salesmen. As soon as they get a sense someone is about to abandon your site they jump into action, giving your visitors a second chance to interact with your site and take action on your offers. You can generate unlimited such salesmen and put them on any webpage you want.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TlJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-content-center">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
						   <div class="col-12">
								<div class="bonus-title-bg">
				   				<div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
							</div>
			 			</div>
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md30">
							  Start Your Online Presence
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							Get Your First Website Online Today!
							<br><br>
							Finally the problem that so many people that are beginning on the internet have struggled with has been solved! Learn how you can easily get your first website online. This video series is targeted to the beginner. no prior knowledge is needed. get your first website online today!
							<br><br>
							The information has been laid out for you in five easy to follow videos. Just follow the instructions in these videos and you will have your first site up within hours!
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1T1JYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #10 End -->
	</div>
</div>



	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container">
           <div class="row">
              <div class="col-12 text-center">
                 <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 858.33 208.54" style="max-height:60px;">
                    <defs>
                      <style>
                        .cls-1 {fill: #fff;}
                        .cls-2 {fill: #1fe29f;}
                        .cls-3 {fill: #7764ff;}
                      </style>
                    </defs>
                    <g id="Layer_1-2" data-name="Layer 1">
                      <g>
                        <g>
                          <path class="cls-3" d="m178.75,60.35H19.64c-12.13,0-21.36,10.89-19.36,22.86H.27c3.37,20.19,23.4,33.09,43.17,27.79l23.58-6.32c12.8-3.43,24.5,8.28,21.08,21.08l-1.22,4.56c-6.46,24.13,11.72,47.82,36.69,47.82h42.09c9.6,0,17.79-6.94,19.36-16.4l13.09-78.52c1.99-11.97-7.23-22.86-19.36-22.86Z"></path>
                          <path class="cls-3" d="m59.85,122.63l-39.14,10.49c-7.51,2.01-8.51,12.26-1.53,15.68l14.06,6.89c1.67.82,3.03,2.18,3.85,3.85l6.89,14.06c3.42,6.98,13.67,5.98,15.68-1.53l10.49-39.14c1.68-6.26-4.05-11.98-10.3-10.3Z"></path>
                          <path class="cls-3" d="m135.09,51.08c-4.13,0-7.48-3.35-7.48-7.48v-.24c0-15.67-12.75-28.42-28.42-28.42s-28.42,12.75-28.42,28.42v.24c0,4.13-3.35,7.48-7.48,7.48s-7.48-3.35-7.48-7.48v-.24C55.82,19.46,75.28,0,99.19,0s43.37,19.46,43.37,43.37v.24c0,4.13-3.35,7.48-7.48,7.48Z"></path>
                          <g>
                            <circle class="cls-3" cx="130.39" cy="198.16" r="10.38"></circle>
                            <circle class="cls-3" cx="75.08" cy="198.16" r="10.38"></circle>
                          </g>
                        </g>
                        <g>
                          <path class="cls-1" d="m286.96,178.57c-8.53,0-17.03-1.6-25.5-4.8-8.47-3.2-16.03-7.87-22.7-14l16.8-20.2c4.67,4,9.87,7.27,15.6,9.8,5.73,2.53,11.27,3.8,16.6,3.8,6.13,0,10.7-1.13,13.7-3.4,3-2.27,4.5-5.33,4.5-9.2,0-4.13-1.7-7.17-5.1-9.1-3.4-1.93-7.97-4.1-13.7-6.5l-17-7.2c-4.4-1.87-8.6-4.37-12.6-7.5-4-3.13-7.27-7.03-9.8-11.7-2.53-4.67-3.8-10.13-3.8-16.4,0-7.2,1.97-13.73,5.9-19.6,3.93-5.87,9.4-10.53,16.4-14,7-3.47,15.03-5.2,24.1-5.2,7.46,0,14.93,1.47,22.4,4.4,7.47,2.93,14,7.2,19.6,12.8l-15,18.6c-4.27-3.33-8.54-5.9-12.8-7.7-4.27-1.8-9-2.7-14.2-2.7s-9.1,1.03-12.1,3.1c-3,2.07-4.5,4.97-4.5,8.7,0,4,1.9,7,5.7,9s8.5,4.13,14.1,6.4l16.8,6.8c7.87,3.2,14.13,7.6,18.8,13.2,4.67,5.6,7,13,7,22.2,0,7.2-1.93,13.87-5.8,20-3.87,6.13-9.47,11.07-16.8,14.8-7.33,3.73-16.2,5.6-26.6,5.6Z"></path>
                          <path class="cls-1" d="m400.36,178.57c-9.47,0-18-2.07-25.6-6.2s-13.6-10.07-18-17.8c-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.47-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-1" d="m486.56,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.46-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-1" d="m543.76,178.57c-10.13,0-17.17-3.03-21.1-9.1-3.93-6.07-5.9-14.1-5.9-24.1V35.97h29.4v110.6c0,3.07.57,5.2,1.7,6.4,1.13,1.2,2.3,1.8,3.5,1.8.67,0,1.23-.03,1.7-.1.47-.07,1.1-.17,1.9-.3l3.6,21.8c-1.6.67-3.63,1.23-6.1,1.7-2.47.47-5.37.7-8.7.7Z"></path>
                          <path class="cls-1" d="m618.36,178.57c-9.47,0-18-2.07-25.6-6.2-7.6-4.13-13.6-10.07-18-17.8-4.4-7.73-6.6-17.07-6.6-28s2.23-20.07,6.7-27.8c4.46-7.73,10.3-13.7,17.5-17.9,7.2-4.2,14.73-6.3,22.6-6.3,9.47,0,17.3,2.1,23.5,6.3s10.87,9.87,14,17c3.13,7.13,4.7,15.23,4.7,24.3,0,2.53-.13,5.03-.4,7.5-.27,2.47-.53,4.3-.8,5.5h-59.4c1.33,7.2,4.33,12.5,9,15.9,4.67,3.4,10.27,5.1,16.8,5.1,7.07,0,14.2-2.2,21.4-6.6l9.8,17.8c-5.07,3.47-10.73,6.2-17,8.2-6.27,2-12.33,3-18.2,3Zm-22-62.8h35.8c0-5.47-1.3-9.97-3.9-13.5-2.6-3.53-6.83-5.3-12.7-5.3-4.53,0-8.6,1.57-12.2,4.7-3.6,3.13-5.93,7.83-7,14.1Z"></path>
                          <path class="cls-1" d="m677.56,176.17v-99.2h24l2,17.4h.8c3.6-6.67,7.93-11.63,13-14.9,5.07-3.27,10.13-4.9,15.2-4.9,2.8,0,5.13.17,7,.5,1.87.33,3.47.83,4.8,1.5l-4.8,25.4c-1.73-.53-3.43-.93-5.1-1.2-1.67-.27-3.57-.4-5.7-.4-3.73,0-7.63,1.37-11.7,4.1-4.07,2.73-7.43,7.43-10.1,14.1v57.6h-29.4Z"></path>
                          <g>
                            <path class="cls-1" d="m858.33,126.35v.23c0,28.71-23.27,51.99-51.99,51.99s-51.99-23.28-51.99-51.99,23.28-51.99,51.99-51.99h.21v19.56h-.21c-17.91,0-32.43,14.52-32.43,32.43s14.52,32.44,32.43,32.44,32.44-14.53,32.44-32.44v-.23h19.55Z"></path>
                            <polygon class="cls-2" points="858.31 74.57 858.31 114.01 838.78 114.01 838.78 94.1 818.87 94.1 818.87 74.57 858.31 74.57"></polygon>
                            <path class="cls-2" d="m826.37,126.55c0,11.06-8.97,20.02-20.03,20.02s-20.02-8.96-20.02-20.02,8.96-20.03,20.02-20.03c.07,0,.14,0,.21.01,10.9.1,19.71,8.92,19.81,19.82.01.07.01.13.01.2Z"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                 </svg>
                 <br><br><br>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                 <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © Sellero 2023</div>
                 <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                    <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                    <li><a href="http://sellero.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                 </ul>
              </div>
           </div>
        </div>
     </div>
    <!--Footer Section End -->


</body>
</html>

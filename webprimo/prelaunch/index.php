<!Doctype html>
<html>

<head>
    <title>Prelaunch Special Webinar | WebPrimo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
    <meta name="title" content="WebPrimo | Prelaunch">
	<meta name="description" content="WebPrimo">
	<meta name="keywords" content="WebPrimo">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

    <!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="WebPrimo | Prelaunch">
	<meta property="og:description" content="WebPrimo">
	<meta property="og:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="WebPrimo | Prelaunch">
	<meta property="twitter:description" content="WebPrimo">
	<meta property="twitter:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
	<link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
    <script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>
	<script src="../common_assets/js/bootstrap.min.js"></script>   
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css" >
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css">
	
    <!-- End -->
<script>
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
var first = getUrlVars()["aid"];
//alert(first);
document.addEventListener('DOMContentLoaded', (event) => {
document.getElementById('awf_field_aid').setAttribute('value',first);
})
//document.getElementById('myField').value = first;
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MMDW8FD');</script>
<!-- End Google Tag Manager -->


</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- New Timer  Start-->
      <?php
         $date = 'April 01 2022 9:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->

<div id="templateBody">
	<div id="space-parent">
		<!-- Header Section Start -->
		<div class="space-section banner-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="text-center">
							<img src="assets/images/logo.png" class="img-fluid">
						</div>
						<!-- <div class="col-12 p0 f-md-24 f-20 w600 text-center  post-heading mt20 mt-md50 lh140 lh140">
							Register for your Coursova launch special FREE webinar & get assured gift + 10 free licenses
						</div> -->
						<div class="mt20 mt-md50 text-center">
							<div class="f-20 f-md-24 w400 text-center white-clr lh150 mt20 mt-md50">
								Register for WebPrimo Pre-Launch Webinar & Get an Assured Gift + 10 Free Licenses
							</div>
							<div class="mt5">
                     <img src="assets/images/post-head-sep.png" class="img-fluid mx-auto d-block">
                  </div>
						 </div>
						<!-- <div class="mt-md20 mt15 col-12 p0">
							<img src="assets/images/head-sep.png" class="mx-auto img-responsive" alt="seperator"/>
						</div> -->
						<div class="mt-md15 mt15">
							<div class="text-center">
								<div class="col-12 mt20 mt-md30 f-md-45 f-28 w700 lh150 text-center white-clr">
									Discover How to Create Premium Websites For Any Business & Tap Into $284 Billion Website Building Industry With Zero Monthly Fee Ever…
								</div>
							</div>
						</div>
						<div class=" mt20 mb15 mb-sm10 px0 text-center">
							<div class="f-md-22 f-20 w400 white-clr text-center">
								Discover how I have sold over $9 Mn in products & earned $4Mn in affiliate commissions by doing it all from one single dashboard and without any tech hassles.
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-12 col-md-7 mt20 mt-md70">
								<!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
									<iframe src="https://Coursova.dotcompal.com/video/embed/9bo529lat3" style="width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
								</div> -->
								<div class="text-center">
									<img src="assets/images/video-bg.png" class="img-fluid">
								</div>
							</div>
							<div class="col-12 col-md-5 mt20 mt-md40">
								<div class="register-form-back">
									<div class="">
										<div class="f-md-30 f-24 w700 lh140 text-center black-clr spartan-font" editabletype="text" style="z-index: 10;">
											Register for Free Training
										</div>
										<div class="f-md-19 f-20 text-center w400 lh150 mb15 mt8" editabletype="text" style="z-index: 10;">
										  <span class="w700">On 1<sup>st</sup> April, 10 AM EST.</span> <br>
										  Book Your Seat now (Limited to 100 Users)
										</div>
									</div>
									<!-- Form Code -->
									<div class="">

								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
								  <div style="display: none;">
									 <input type="hidden" name="meta_web_form_id" value="1888856473" />
									 <input type="hidden" name="meta_split_id" value="" />
									 <input type="hidden" name="listname" value="awlist6245382" />
									 <input type="hidden" name="redirect" value="https://www.webprimo.co/prelaunch-thankyou" id="redirect_52558e786ab7eccaaca582825e3b2496" />
									 <input type="hidden" name="meta_adtracking" value="Prelaunch_Web_Form" />
									 <input type="hidden" name="meta_message" value="1" />
									 <input type="hidden" name="meta_required" value="name,email" />
									 <input type="hidden" name="meta_tooltip" value="" />
								  </div>
								  <div id="af-form-1888856473" class="af-form">
									 <div id="af-body-1888856473" class="af-body af-standards">
										<div class="af-element">
										   <label class="previewLabel" for="awf_field-113865671"></label>
										   <div class="af-textWrap ">
											  <div class="input-type" style="z-index: 11;">
												 <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
												 <input id="awf_field-113865671" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
											  </div>
										   </div>
										   <div class="af-clear bottom-margin"></div>
										</div>
										<div class="af-element">
										   <label class="previewLabel" for="awf_field-113865672"> </label>
										   <div class="af-textWrap">
											  <div class="input-type" style="z-index: 11;">
												 <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
												 <input class="text form-control custom-input input-field" id="awf_field-113865672" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
											  </div>
										   </div>
										   <div class="af-clear bottom-margin"></div>
										</div>
										<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
									   
										 <div class="af-element" style="padding:0px;">
										   <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
										   <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
										   <div class="af-clear"></div>
										</div>
										
									   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn f-22 f-md-26" style="padding:0;">
										   <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
										   <div class="af-clear bottom-margin"></div>
										</div>
									 </div>
								  </div>
								  <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jBwcHBysbCzszA==" alt="" /></div>
							   </form>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Section End -->

		<!-- Prize Section Start -->
		<div class="space-section prize-section">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-5 mx-auto mb-sm50 mb20 timer-block text-center p-md0 px15">
						<div class="f-md-22 f-20 w500 lh140 px0 px-sm15">
							Hurry Up! Free Offer Going Away in… 
						</div>
						<div class="col-12 mt-md20 mt20">
							<div class="countdown counter-black">
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
								<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr mt15">
						Register for FREE Training and WIN Gifts
						</div>
					</div>
					<div class="col-12 col-md-8 mx-auto">
						<div class="f-md-22 f-20 w500 lh150 mt10 mt-md15">
						My 10+ Years of experience is packaged in this state-of-art 1 hour webinar where you’ll learn:
						</div>
						<div class="f-md-22 f-20 w300 lh150 p0 mt20 mt-md40">
							<ul class="list-style lh150">
                                <li>How I sold over $9Mn in digital products online and <span class="w500">you can follow the same to build a profitable business online.</span></li>
                             <li>How you can Tap into the fastest growing website building industry <span class="w500">without any tech hassles.</span></li>
                            <li>
							A Sneak-Peak of WebPrimo to Create Premium Websites For Any Business In Any Niche Without Any Hassles<span class="w500">–From One Platform</span>
							</li>
							 <li>During This Launch Special Deal, <span class="w500">Get All Benefits at Limited Low One-Time-Fee.</span> </li>
							 <li>Lastly, everyone who attend this session will get an assured gift from us plus <span class="w500">we are giving 10 licenses of our Premium solution - WebPrimo</span></li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-10 mx-auto mt25 mt-md85 p0" id="form">
						<div class="auto_responder_form inp-phold formbg col-12">
							<div class="f-20 f-md-23 w400 text-center lh180 black-clr">
								Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
								<span class="f-22 f-md-32 w700 text-center black-clr" contenteditable="false">1<sup contenteditable="false">st</sup> April, 10 am Est.(100 Seats Only)</span>
							</div>
							<div class="row">
								<div class="col-12 col-md-6 mx-auto  p-md0 px15 my20 timer-block text-center">
									<div class="col-md-12 col-12 f-md-22 f-20 w500 lh140 px0 px-sm15">
										Hurry Up! Free Offer Going Away in… 
									</div>
									<div class="col-12 col-md-10 col-md-offset-0 p0 mt-md20 mt20">
										<div class="countdown counter-black">
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
											<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
										</div>
									</div>
								</div>
							</div>
							<!-- Aweber Form Code -->
							<div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
								
								<!-- Aweber Form Code -->
								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="1888856473" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6245382" />
                                 <input type="hidden" name="redirect" value="https://www.webprimo.co/prelaunch-thankyou" id="redirect_52558e786ab7eccaaca582825e3b2496" />
                                 <input type="hidden" name="meta_adtracking" value="Prelaunch_Web_Form" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-1888856473" class="af-form">
                                 <div id="af-body-1888856473" class="af-body af-standards">
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-113865671"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-113865671" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-113865672"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-113865672" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <!--<div class="af-element">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>-->
									
								   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn1 f-22 f-md-21 width-form" style="padding-top:5px;">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                             <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jBwcHBysbCzszA==" alt="" /></div>
                           </form>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Prize Section End -->	
		 <!--Footer Section Start -->
            <div class="space-section footer-section">
                <div class="container">
				<div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/logo.png" class="img-fluid d-block mx-auto"> 
                  <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © WebPrimo</div>
                  <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
                </div>
            </div>
            <!--Footer Section End -->
	</div>
</div>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-1888856473').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1888856473")) {
                document.getElementById("af-form-1888856473").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1888856473")) {
                document.getElementById("af-body-1888856473").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1888856473")) {
                document.getElementById("af-header-1888856473").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1888856473")) {
                document.getElementById("af-footer-1888856473").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->

<!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
</body>

</html>
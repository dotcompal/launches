<html>
   <head>
      <title>JV Page - Webprimo JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WebPrimo | JV">
      <meta name="description" content="WebPrimo">
      <meta name="keywords" content="WebPrimo">
      <meta property="og:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WebPrimo | JV">
      <meta property="og:description" content="WebPrimo">
      <meta property="og:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WebPrimo | JV">
      <meta property="twitter:description" content="WebPrimo">
      <meta property="twitter:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/m-style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <!-- End -->
      <script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/ false;
             if (!IE) {
                 return;
             }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1059432944")) {
                     document.getElementById("af-form-1059432944").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1059432944")) {
                     document.getElementById("af-body-1059432944").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1059432944")) {
                     document.getElementById("af-header-1059432944").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1059432944")) {
                     document.getElementById("af-footer-1059432944").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MMDW8FD');</script>
	<!-- End Google Tag Manager -->

   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'April 01 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
	  
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

       <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
                  <img src="assets/images/logo.png" class="img-responsive">
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                     <ul class="leader-ul f-md-18 f-16 w500 white-clr">
                        <li>
                           <a href="https://docs.google.com/document/d/1xUJXWdQNCOdvKuyj4LMiLrBs9PtdUgSO/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">JV Doc</a>
                        </li>
                        <li>|</li>
						<li>
                           <a href="https://docs.google.com/document/d/1IcfcH0diVdkAF1kW-Hgd-rZnGve77vB_IbgtSVcC-zg/edit?usp=sharing" target="_blank">Swipes</a>
                        </li>
						<li>|</li>
                        <li>
                           <a href="#funnel">Funnel</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/379455" class="affiliate-link-btn ml-md15 mt10 mt-md0">Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="f-20 f-md-24 w600 text-center white-clr lh140 text-uppercase">
                     Coming Up with a Big Bang Bundle Offer and $12,000 in JV Prizes
                  </div>
				  <div class="mt5">
					<img src="assets/images/post-head-sep.png" class="img-fluid mx-auto d-block">
				  </div>
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-md-45 f-28 w600 text-center white-clr lh150">
                     Promote JVZOO’s First<br class="d-lg-block d-none"> <span class="highlihgt-heading">Hybrid WordPress Website Builder Framework</span> That Creates Premium Websites for Any Business & Niche in Next 7 Minutes FLAT...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
                  Easily create & sell websites for big profits to Retail Stores, Ecom Stores, Book Shops, Coaches,<br class="d-lg-block d-none"> Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, & many other Niches
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-md-8 col-12 min-md-video-width-left">
					 <!-- <img src="assets/images/dummy-video.png" class="img-fluid d-block mx-auto"> -->
                        <div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://webprimo.dotcompal.com/video/embed/kprvpit59t" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                            box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 min-md-video-width-right mt20 mt-md0">
                        <img src="assets/images/date.png" class="img-fluid d-block mx-auto">
                        <div class="countdown counter-black mt20 mt-md30">
                           <div class="timer-label text-center"><span class="f-40 f-md-40 lh100 timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-40 lh100 timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-40 lh100 timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-40 lh100 timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
		   </div>
	   </div>
   </div>
	<div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-18 f-md-20 lh140 w400">
                           <ul class="header-bordered-list pl0 orange-list">
                              <li><span class="w600">Install and Sell</span> Elegant, Highly Professional and Conversion Focused Websites. </li>
							  <li>Super Customizable Suite with <span class="w600">Over 200+ Templates and 2000+ Possible Combination</span> to Instantly Create Website for Any Niche</li>
							  <li>Built on Most <span class="w600">Advance Word Press Frame Work</span> to Make is Super Easy for Marketers & Newbies with No Coding or Tech Skills Required.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-18 f-md-20 lh140 w400">
                           <ul class="header-bordered-list pl0 orange-list">
                              <li>Seamless <span class="w600">Woo Commerce Integration</span> to accept payment for your product & services</li>
                              <li>Pre-Loaded with Tons of <span class="w600">Editable Local Niche Marketing Graphics & Videos, Social Media Graphics, Logo Kit and Royalty free Stock Images & Videos.</span></li>
                              <li><span class="w600">Commercial License Included</span> So Your Buyers Can Sell Services And Make Profit Big Time.</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="live-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
						<div class="white-clr f-md-50 f-20 d-block mb0 lh140 w700 text-center">
							Subscribe To Our JV List<br class="d-lg-block d-none"> 
							<span class="f-20 f-md-30 w600 text-center white-clr lh140 mt5 d-lg-inline-block">
								and Be The First to Know Our Special Contest Events and Discounts
							</span>
						</div>
                     <!-- Aweber Form Code -->
                    <div class="mt15 mt-md50">
						<form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
							<div style="display: none;">
								<input type="hidden" name="meta_web_form_id" value="2033102032" />
								<input type="hidden" name="meta_split_id" value="" />
								<input type="hidden" name="listname" value="awlist6241145" />
								<input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_3defea21bdd383fbaa4ec7d3da25996d" />

								<input type="hidden" name="meta_adtracking" value="My_Web_Form" />
								<input type="hidden" name="meta_message" value="1" />
								<input type="hidden" name="meta_required" value="name,email" />

								<input type="hidden" name="meta_tooltip" value="" />
							</div>
							<div id="af-form-2033102032" class="af-form">
								<div id="af-body-2033102032" class="af-body af-standards row justify-content-center">
									<div class="af-element col-md-4">
										<label class="previewLabel" for="awf_field-113826580" style="display:none;">Name: </label>
										<div class="af-textWrap mb15 mb-md15 input-type">
										<input id="awf_field-113826580" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
										</div>
										<div class="af-clear"></div>
									</div>
									<div class="af-element mb15 mb-md25  col-md-4">
										<label class="previewLabel" for="awf_field-113826581" style="display:none;">Email: </label>
										<div class="af-textWrap input-type">
											<input class="text frm-ctr-popup form-control input-field" id="awf_field-113826581" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
										</div><div class="af-clear"></div>
									</div>
									<div class="af-element buttonContainer button-type popup-btn white-clr col-md-4">
										<input name="submit" class="submit f-20 f-md-22 white-clr center-block" type="submit" value="Subscribe for JV Updates" tabindex="502" />
										<div class="af-clear"></div>
									</div>
								</div>
							</div>
							<div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TAzMzIwMTAzMTA==" alt="" /></div>
						</form>
                        <!-- Aweber Form Code -->
                    </div>
                  </div>
                  <!-- <div class="col-12">
                     <img src="assets/images/form-shadow.png" class="img-fluid d-block mx-auto" />
                  </div> -->
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
                  <div class="f-24 f-md-32 w600 text-center lh140">
                     <span class="">Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
                  </div>
				</div>
				<div class="col-md-12 mx-auto">
					<div class="row justify-content-center align-items-center">
						<div class="col-12 col-md-3  mt-md19 mt15">
							<img src="assets/images/jvzoo.png" class="img-fluid d-block mx-auto" />
						</div>
						<div class="col-12 col-md-5 affiliate-btn  mt-md19 mt15">
							<a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/379455" class="f-24 f-md-30 mx-auto">Request Affiliate Link</a>
						</div>
					</div>
				</div>
            </div>
         </div>
      </div>
      <!-- launch-special -->
      <!-- exciting-launch -->
		<div class="exciting-launch">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="f-md-42 f-28 black-clr text-center w700 lh140">
							This Exciting Launch Event Is Divided Into 2 Phases
						</div>
					</div>
					<div class="col-12 mt-md60 mt30">
						<div class="row align-items-center">
							<div class="col-md-6 col-12 order-md-6">
								<!-- <div class="phase-bg1">
									<div class="f-md-45 f-28 w600 white-clr lh140 text-center white-clr">
										Pre Launch
									</div>
									<div class="f-md-28 f-24 text-center w500 white-clr">
										(With Webinar)
									</div>
									<div class="f-md-24 f-18 mt-md20 mt15 text-center w400 lh160 white-clr">
										<span class="w400">29<sup>th</sup> March'22</span>, 10:00 AM EST to <br>
										<span class="w400">1<sup>st</sup> April'22</span>, 9:00 AM EST
									</div>
								</div> -->
								<img src="assets/images/phase1.png" class="img-fluid d-block mx-auto" />
							</div>
							<div class="col-md-6 col-12 p-md0 mt-md0 mt20 order-md-0">
								<div class="f-md-36 f-24 lh140 w600">
									To Make You Max Commissions
								</div>
								<ul class="launch-tick1 pl-md25 pl20 f-md-24 f-18 mb0 mt-md30 mt20">
									<li>All Leads Are Hardcoded</li>
									<li>Exciting $2000 Pre-Launch Contest</li>
									<li>We'll Re-Market Your Leads Heavily</li>
									<li>Pitch Bundle Offer on webinars.</li>
								</ul>
							</div>
						</div>
						<div class="row mt-md10 mt20 align-items-center">
							<div class="col-md-6 col-12">
								<img src="assets/images/phase2.png" class="img-fluid d-block mx-auto" />
							</div>
							<div class="col-md-6 col-12 mt-md0 mt20">
								<ul class="launch-tick1 pl-md25 pl20 f-md-24 f-18 mb0">
									<li>High in Demand Product with Top Conversion</li>
									<li>Deep Funnel to Make You Double Digit EPCs</li>
									<li>Earn upto $358/Sale</li>
									<li>Huge $10K JV Prizes</li>
									<li>We'll Re-Market Your Visitors Heavily</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-12">
                  <div class="f-md-45 f-28 lh140 w700 white-clr heading-design text-center">
                     Hello Awesome JV's
                  </div>
                  <div class="f-md-18 f-18 lh150 w400 mt-md40 mt15 black-clr">
                     It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner Achal Goswami (Entrepreneur & Internet Marketer) & Atul Pareek (Internet Marketer & JV Manager).
                     <br>
                     <br>We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates.
                     <br>
                     <br> With the combined experience of 20+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for your business website and online presence need under one dashboard.
                     <br>
                     <br> "WebPrimo", is a revolutionary technology to create websites for any business or niche, so you can use it for your own purpose or can even start your own profitable website creation agency from a single dashboard.
                     <br><br> Ultimately, your subscribers can <span class="w600">Tap into the $284 Billon Website Building Industry</span> and start a full-blown local website agency
					<br><br> Check out the incredible features of the amazing creation that will blow away your mind. And we guarantee that this offer will convert like Hot Cakes starting from 1st April'22 at 10:00 AM EST! Get Ready!!
                  </div>
               </div>
               <div class="col-md-6 col-12">
                  <div class="row">
                     <div class="col-md-12 mt20 mt-md0">
                        <img src="assets/images/amit-pareek.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md50">
                        <img src="assets/images/achal-goswami.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md50">
                        <img src="assets/images/atul-parrek.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-md-18 f-18 lh150 w700 mt-md50 mt20 text-center">
                  Also, here are some stats from our previous launches:
               </div>
            </div>
            <div class="row mt-md25 mt20">
               <div class="col-12 col-md-6 f-18 f-md-18 lh140 w500">
                  <ul class="pl0 no-orange-list">
                     <li>Over 100 Pick of The Day Awards</li>
                     <li>Over $1.5Mn In Affiliate Sales for Partners</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 f-18 f-md-18 lh140 w500">
                  <ul class="pl0 no-orange-list">
                     <li>Top 10 Affiliate & Seller (High-Performance Leader)</li>
                     <li>Always in Top-10 of JVZoo Top Sellers</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-md-36 f-24 w500 white-clr lh140">
                     Presenting…
                  </div>
               </div>
			   <div class="col-12 mt-md35 mt20">
				<img src="assets/images/big-logo.png" class="img-fluid mx-auto d-block">
			   </div>
               <div class="col-12 f-md-30 f-24 mt-md35 mt20 w600 text-center black-clr lh140">
                  JVZOO’s First Hybrid WordPress Website Builder Framework To Create Elegant & Highly Professional Premium Websites For Any Business & Niche In Next 7 Minutes FLAT.
               </div>
			   <div class="col-12 f-18 f-md-22 mt-md30 mt20 w400 text-center black-clr lh140">
					Included with Agency License to Start Your Own Website & Marketing Agency and Get Profit from Local Buyers Like Doctors, Attorneys, Restaurants, Architect and More.
			   </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex flex-wrap" style="align-items:end;">
                     <div class="col-12 col-xl-10 mx-auto px-xl15 px0">
                        <div class="">
                           <img src="assets/images/product-image.png" class="img-fluid d-xl-block d-none mx-auto margin-bottom-15">
                           <img src="assets/images/product-image-xxs.png" class="img-fluid d-block d-xl-none mx-auto margin-bottom-15">
                        </div>
						
                     </div>
                     <!-- <div class="col-md-12 col-12 mt20 mt-md30">
                        <div class="proudly-list-bg">
                           <div class="d-flex">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Create UnlimitedWebsites and Ecom Stores.
                              </div>
                           </div>
                           <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Ready to Use Highly Customizable 200+ Templates with 2000+ Possible Combinations.
                              </div>
                           </div>
                           <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Create an Elegant, Highly Professional, and Lightning-Fast Website.
                              </div>
                           </div>
                           <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Built on Most Advance Word Press Framework to Make it Super Easy with No Tech Hassles.
                              </div>
                           </div>
                           <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Agency License Included so use for your own purpose or Start a Website Building and Marketing Agency.  
                              </div>
                           </div>
						   <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Seamless Woo Commerce Integration to Accept Payments for Online Selling and Ecom Stores.
                              </div>
                           </div>
						   <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 100% Mobile Responsive and Fully SEO Optimized Website to get targeted traffic from all 360° Directions Seamlessly. 
                              </div>
                           </div>
						   <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Done-For-You Graphics Templates, Social Media Graphics, Logo Kit, E-book Covers, Business Cards, Borchers, and Marketing Videos in 20+ Local Niches
                              </div>
                           </div>
						   <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                              <div class="f-20 f-md-24 w500 black-clr lh140">
                                 Tons of Royalty Free Stock Images and Videos with pexels integration.
                              </div>
                           </div>
                        </div>
                     </div> -->
                  </div>
               </div>
            </div>
         </div>
         <!-- Demo video link -->
         <!-- <div class="demo-link-wrapper mt20 mt-md0">
            <div class="link-text align-items-center">
               <div class="link-body text-right">
                  <div class="mr15 text-uppercase f-20 f-md-28 w700">Watch <br>Product Demo</div>
               </div>
               <div class="link-right">
                  <a class="demo-link" href="#" data-toggle="modal" data-target="#myModal">
                     <div class="demo-link-bg"><span class="video-link-overlay"></span></div>
                     <span class="icon fa fa-play"></span>
                  </a>
               </div>
            </div>
         </div> -->
      </div>
      <!-- next-generation end -->
	  <!-- FEATURE LIST SECTION START -->
		<div class="feature-sec">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6 col-12 f-md-32 f-20">
						Create <br class="d-lg-block d-none"><span class="w600">Unlimited Websites and Ecom Stores.</span>
					</div>
					<div class="col-md-6 col-12 mt-md0 mt20">
						<img src="assets/images/fe1.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 order-md-6 f-md-32 f-20">
						Ready to Use <br class="d-lg-block d-none"><span class="w600">Highly Customizable 200+ Templates with 2000+ Possible Combinations.</span>
					</div>
					<div class="col-md-6 col-12 order-md-0 mt-md0 mt20">
						<img src="assets/images/fe2.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 f-md-32 f-20">
						Create an <br class="d-lg-block d-none"><span class="w600">Elegant, Highly Professional,<br class="d-lg-block d-none"> and Lightning-Fast Website.</span>
					</div>
					<div class="col-md-6 col-12 mt-md0 mt20">
						<img src="assets/images/fe3.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 order-md-6 f-md-32 f-20">
						Built on Most Advance <br class="d-lg-block d-none"><span class="w600">Word Press Framework to Make it Super Easy with No Tech<br class="d-lg-block d-none"> Hassles.</span>
					</div>
					<div class="col-md-6 col-12 order-md-0 mt-md0 mt20">
						<img src="assets/images/fe4.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 f-md-32 f-20">
						<span class="w600">Agency License Included</span> so use for your own purpose or Start a Website Building and Marketing Agency. 
					</div>
					<div class="col-md-6 col-12 mt-md0 mt20">
						<img src="assets/images/fe5.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 order-md-6 f-md-32 f-20">
						Seamless <br class="d-lg-block d-none"><span class="w600">Woo Commerce Integration to Accept Payments</span> for Online Selling and Ecom Stores.
					</div>
					<div class="col-md-6 col-12 order-md-0 mt-md0 mt20">
						<img src="assets/images/fe6.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 f-md-32 f-20">
						<span class="w600">100% Mobile Responsive and Fully SEO Optimized Website</span> to get targeted traffic from all 360° Directions Seamlessly. 
					</div>
					<div class="col-md-6 col-12 mt-md0 mt20">
						<img src="assets/images/fe7.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 order-md-6 f-md-32 f-20">
						Done-For-You<br class="d-lg-block d-none"> <span class="w600">Graphics Templates, Social Media Graphics, Logo Kit, E-book Covers, Business Cards, Borchers, and Marketing Videos in 20+ Local Niches</span>
					</div>
					<div class="col-md-6 col-12 order-md-0 mt-md0 mt20">
						<img src="assets/images/fe8.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row align-items-center mt-md100 mt30">
					<div class="col-md-6 col-12 f-md-32 f-20">
						Tons of Royalty<br class="d-lg-block d-none"> <span class="w600"> Free Stock Images and Videos with pexels integration.</span>
					</div>
					<div class="col-md-6 col-12 mt-md0 mt20">
						<img src="assets/images/fe9.png" class="img-fluid mx-auto d-block">
					</div>
				</div>
				<div class="row m0 mt-md100 mt30">
					<div class="col-12 feature-icon-block">
						<div class="row">
							<div class="col-12">
								<img src="assets/images/feature-list1.png" class="img-fluid mx-auto d-lg-block d-none">
								<img src="assets/images/feature-mobile-view.png" class="img-fluid mx-auto d-lg-none d-block">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- DEMO SECTION START -->
		<div class="demo-sec">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="f-md-45 f-28 w600 lh140 text-center white-clr">
							<span class="demo-bg">Watch The Demo</span>
						</div>
						<div class="f-md-45 f-28 w600 lh140 text-center white-clr mt-md20 mt15">
							<span class="w500 "> Discover How Easy & Powerful It Is</span>
						</div>
					</div>
					<div class="col-12 col-md-8 mx-auto mt-md40 mt20">
						<img src="assets/images/dummy-video.png" class="img-fluid d-block mx-auto">
							<!-- <div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://kyza.dotcompal.com/video/embed/kxcu6m1l9g" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<!-- DEMO SECTION END -->
		<!-- POTENTIAL SECTION START -->
		<div class="potential-sec">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<div class="f-md-45 f-24 w600 text-center white-clr">
							Here’s a List of 
						</div>
						<div class="f-md-50 f-24 w700 lh140 text-center white-clr niche-bg">
							All Potential Niche
						</div>
						<div class="f-md-45 f-24 w600 text-center white-clr">
							You Can Start Getting Clients & Collecting Checks
						</div>
					</div>
				</div>
				<div class="row mt-md70 mt30">
					<div class="col-md-3 col-6">
						<div class="">
							<img src="assets/images/pn1.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Architect
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt0">
						<div class="">
							<img src="assets/images/pn2.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Financial Adviser
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn3.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Painters & Decorators
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn4.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Counsellors
						</div>
					</div>
				</div>
				<div class="row mt-md50 mt0">
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn5.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Divorce Lawyers
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn6.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Carpet Cleaner
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn7.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Hotels
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn8.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Car Rental & Taxi
						</div>
					</div>
				</div>
				<div class="row mt-md50 mt0">
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn9.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Florist
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn10.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Attorney
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn11.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Lock Smith
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn12.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Real Estate
						</div>
					</div>
				</div>
				<div class="row mt-md50 mt0">
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn13.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Home Security
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn14.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Plumbers
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn15.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Chiropractors
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn16.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Dentists
						</div>
					</div>
				</div>
				<div class="row mt-md50 mt0">
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn17.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Home Tutors
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn18.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Dermatologists
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn19.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Veterinarians
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn20.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Motor Garage
						</div>
					</div>
				</div>
				<div class="row mt-md50 mt0">
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn21.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Carpenters
						</div>
					</div>
					<div class="col-md-3 col-6 mt-md0 mt30">
						<div class="">
							<img src="assets/images/pn22.png" class="img-fluid mx-auto d-block">
						</div>
						<div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
							Computer Repair
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- POTENTIAL SECTION END -->
	  <!-- FEATURE LIST SECTION END -->
       <!-- <div class="tons-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 px-md0 f-md-45 f-28 w700 text-center  black-clr lh140">
                  Here are Some More Features
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="row">
                     <div class="col-md-4 col-12">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Feature Rich Slider
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f2.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Fully Functional CMS
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                             Appointment Booking Functionality
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="row">
                     <div class="col-md-4 col-12">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f4.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12 f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              CTA Management
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12 f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Google MAP
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg min-height-first">
                           <div class="icon-height">
                              <img src="assets/images/f6.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12 f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Inbuilt Social Media Tool
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="row">
                     <div class="col-md-4 col-12">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f7.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Analytics Ready
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f8.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Inbuilt Lead Management
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f9.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Advance AR Integration
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="row">
                     <div class="col-md-4 col-12">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f10.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Custom CSS for Design
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f11.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12 f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                             Ready to Use Short Code
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="features-list-bg">
                           <div class="icon-height">
                              <img src="assets/images/f12.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="col-12 f-md-26 f-22 w600 mt20 text-center black-clr lh140">
                              Fully Customizable Typography
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
				<div class="col-12 mt-md30 mt20">
					<div class="row">
						<div class="col-md-4 col-12">
							<div class="features-list-bg">
								<div class="icon-height">
									<img src="assets/images/f10.png" class="img-fluid d-block mx-auto">
								</div>
								<div class="col-12  f-md-26 f-22 w600 mt20 text-center black-clr lh140">
									Width Flexible Website Layout
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
         </div>
      </div> -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section1">
         <div class="container ">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-80 f-40 w700 text-center blue-clr lh140 mt-md10">
                  $12000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-22 f-20 w400 lh140 mt20 text-center">
                  Contest Starts from 29th March’22 at 10:00 AM EST and Ends at 1st April’22 at 10:00 AM EST
               </div>
               <div class="col-12 f-md-22 f-20 w400 lh140 mt10 text-center">
                  (Get Flat <span class="w700">$0.50c</span> For Every Lead You Send for <span class="w700">Pre-Launch Webinar</span>)
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/contest-img1.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="assets/images/contest-img2.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-22 lh140 mt-md50 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads)
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/prize-img1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/prize-img2.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w600 lh140 mt20 mt-md50 black-clr">
                  *Contest Policies:
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt10">
                  1. Team of maximum two is allowed. <br> 2. To be eligible to win one of the sales leaderboard prizes, you must have made commissions equal to or greater than the value of the prize. If this criterion is not met, then you will
                  be eligible for the next prize.
               </div>
               <div class="col-12 mt-md0 mt30 "></div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md55">
                  <img src="assets/images/prize-img3.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w400 lh140">
                     <span class="f-18 f-md-20 w600 lh140">Note:</span>
                     <br>We will announce the winners on 7th April & Prizes will be distributed through Payoneer from 8th April Onwards.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 black-clr converting-shape">
                     Our Solid Track Record of Launching Top <br class="d-lg-block d-none"> Converting Products
                  </div>
               </div>
               <!-- <div class="col-12 f-20 f-md-22 lh140 mt20 w400 text-center black-clr ">
                  Dr Amit Pareek's Team, Achal Goswami and Simon Warner are Top Vendors with 6-Figure Launches Consecutively from 2017, 2018, 2019, 2020, 2021 & 2022
               </div> -->
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 black-clr mt20 mt-md70">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs.<br><br> So, if you have a top-notch product with top conversions and that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
					<div class="logos-effect">
						<img src="assets/images/logos.png" class="img-fluid d-block mx-auto">
					</div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                     <span class="query-bg">Have any Query?</span> <span class="w500"> Contact us Anytime</span>
                  </div>
				  <!-- <div class="f-sm-24 f-20 w400 mt15 mt-sm25 lh140 text-center white-clr">
					   If you have anything to ask, we are simply one click away. Get access to JV Tools:   Swipes, Banners, Bonuses and Bonus Template from above link on this page
						So to use this awesome piece of technology, just ask us for a review  account and play with it to get best results.
					</div> -->
               </div>
            </div>
            <div class="row mt20 mt-md50 ">
               <div class="col-md-4 col-12 text-center">
                  <img src="assets/images/amit-pareek-sir.png" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-32 w700 mt15 mt-md20 lh140 text-center white-clr">
                     Dr Amit Pareek
                  </div>
                  <div class="f-16 w500 lh140 text-center white-clr">
                     (Techpreneur & Marketer)
                  </div>
                  <div class="col-12 mt30 d-flex justify-content-center">
                     <a href="skype:amit.pareek77" class="link-text">
                        <div class="col-12 ">
                           <img src="assets/images/skype.png" class="center-block">
                        </div>
                     </a>
                     <a href="http://facebook.com/Dr.AmitPareek" class="link-text">
                     <img src="assets/images/am-fb.png" class="center-block">
                     </a>
                  </div>
               </div>
               <div class="col-md-4 col-12 text-center mt20 mt-md0">
                  <img src="assets/images/achal-goswami-sir.png" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                     Achal Goswami
                  </div>
                  <div class="f-16 w500 lh140 text-center white-clr" >
                     (Entrepreneur & Internet Marketer)
                  </div>
                  <div class="col-12 mt30 d-flex justify-content-center">
                     <a href="skype:live:.cid.78f368e20e6d5afa" class="link-text">
						 <div class="col-12 ">
							<img src="assets/images/skype.png" class="center-block">
						</div>
                     </a>
                     <a href="https://www.facebook.com/dcp.ambassador.achal/" class="link-text">
                     <img src="assets/images/am-fb.png" class="center-block">
                     </a>
                  </div>
                  <div class="col-12 mt15">
                  </div>
               </div>
               <div class="col-md-4 col-12 text-center mt20 mt-md0">
                  <img src="assets/images/atul-parrek-sir.png" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                     Atul Pareek
                  </div>
                  <div class="f-16 w500  lh140 text-center white-clr">
                     (Entrepreneur & JV Manager)
                  </div>
                  <div class="col-12 mt30 d-flex justify-content-center">
                     <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
						 <div class="col-12 ">
							<img src="assets/images/skype.png" class="center-block">
						 </div>
                     </a>
                     <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text">
                     <img src="assets/images/am-fb.png" class="center-block">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container px-md-15">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center black-clr">
                  Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 f-md-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below:
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
				 <ul class="b-tick1 pl0 m0 f-md-16 f-16 lh140 w400">
					<li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No
					   exceptions will be entertained. 
					</li>
					<li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed
					   from our system with immediate effect. 
					</li>
					<li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
					<li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
					</li>
					<li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>
				 </ul>
			  </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/logo.png" class="img-fluid d-block mx-auto"> 
                  <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh140 white-clr text-xs-center">Copyright © WebPrimo</div>
                  <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
	  
	  <script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-2033102032').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-2033102032")) {
                document.getElementById("af-form-2033102032").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-2033102032")) {
                document.getElementById("af-body-2033102032").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-2033102032")) {
                document.getElementById("af-header-2033102032").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-2033102032")) {
                document.getElementById("af-footer-2033102032").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
         <a href="javascript:void(0);" data-dismiss="modal" class="close-link">&times;</a>
         <div class="modal-dialog help-video-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body ">
                  <div class="col-12 responsive-video ">
                     <iframe src="https://kyza.dotcompal.com/video/embed/kxcu6m1l9g" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
<!DOCTYPE html>
<html>

<head>
    <title>Bonus Landing Page Generator</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="WebPrimo Special Bonuses">
    <meta name="description" content="WebPrimo Special Bonuses">
    <meta name="keywords" content="WebPrimo Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WebPrimo Special Bonuses">
    <meta property="og:description" content="WebPrimo Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WebPrimo Special Bonuses">
    <meta property="twitter:description" content="WebPrimo Special Bonuses">
    <meta property="twitter:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">

	
    <!-- Device & IE Compatibility Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!--Load Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!--Load External CSS -->
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
    <link rel="stylesheet" href="assets/css/generator.css" type="text/css" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MMDW8FD');</script>
<!-- End Google Tag Manager -->

</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="greysection">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <img src="assets/images/logo-black.png" class="img-fluid mx-auto d-block logo">
                </div>
            </div>
        </div>
    </div>

    <div class="whitesection">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12 f-md-45 f-28 w700 text-center black-clr lh150">
                    Here's How You Can Generate Your Own Bonus Page Easily
                </div>
            </div>
        </div>
    </div>

    <div class="formsection">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm-6 col-12 mb30 mb-sm0">
                    <img src="assets/images/product-box.png" class="img-fluid mx-auto d-block mt8 xsmt4">
                </div>

                <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                <?php

                if(isset($_POST['submit'])) 
                {
	               $name=$_REQUEST['name'];
	               $afflink=$_REQUEST['afflink'];
                	//$picname=$_REQUEST['pic'];
                	/*$tmpname=$_FILES['pic']['tmp_name'];
                	$type=$_FILES['pic']['type'];
                	$size=$_FILES['pic']['size']/1024;
                	$rand=rand(1111111,9999999999);*/
	           if($afflink=="")
	           {
		
                echo 'Please fill the details.';
	           }
               
               else
               
               {
			
			     /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
			     {
				    echo "Please upload image file (size must be less than 1 MB)";	
			     }
		  	   else
			 {*/
				//$filename=$rand."-".$picname;
				//move_uploaded_file($tmpname,"images/".$filename);
	           $url="https://www.webprimo.co/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
				echo "<a target='_blank' href=".$url.">".$url."</a><br>";
				//header("Location:$url");
			//}	
	   }
	}
?>
				<div class="col-md-6 col-12">
					<form class="row" action="" method="post" enctype="multipart/form-data">
						<div class="col-12 form_area padding0">
							<div class="col-md-10 col-sm-12 col-12 clear">
								<input type="text" name="name" placeholder="Your Name..." required>
							</div>

							<div class="col-md-10 col-sm-12 col-12 mt20 xsmt4 clear">
								<input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
							</div>

							<div class="col-md-10 col-sm-12 col-12 f-24 f-sm-30 white-clr center-block mt10 mt-sm20 w500 clear">
								<input type="submit" value="Generate Page" name="submit" class="f-sm-30 f-24" />
							</div>
						</div>
					</form>
				</div>
            </div>
        </div>
    </div>

    <!--Footer Section Start -->
            <div class="space-section footer-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <div> <img src="assets/images/white-logo.png" class="img-fluid mx-auto d-block"> </div>
                            <div editabletype="text" class="f-16 f-sm-18 w300 mt20 lh150 white-clr text-center" style="z-index: 10;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                        </div>
                        <div class="col-sm-3 col-12">
                            <div editabletype="text" class="f-16 f-sm-18 w300 mt10 mt-sm40 lh150 white-clr d-left-m-center" style="z-index: 10;">Copyright © WebPrimo</div>
                        </div>
                        <div class="col-sm-9 col-12 mt10 mt-sm40 d-right-m-center">
                            <ul class="footer-ul f-sm-18 w300 lh150 f-16 white-clr text-right">
								<li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                                <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer Section End -->

    <!-- Footer Section End -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKWBVSR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>

</html>
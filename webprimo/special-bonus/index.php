<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="WebPrimo Special Bonuses">
    <meta name="description" content="WebPrimo Special Bonuses">
    <meta name="keywords" content="WebPrimo Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WebPrimo Special Bonuses">
    <meta property="og:description" content="WebPrimo Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WebPrimo Special Bonuses">
    <meta property="twitter:description" content="WebPrimo Special Bonuses">
    <meta property="twitter:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">

	<title>WebPrimo Special Bonuses</title>
	<!-- Shortcut Icon  -->
	<link rel="shortcut icon" href="assets/images/favicon.png"/>
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style-bottom.css">
	<link rel="stylesheet" type="text/css" href="assets/css/m-style.css">
	<!-- Font Family CDN Load Links -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Javascript File Load --><script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>
	<script src="../common_assets/js/bootstrap.min.js"></script>
	<!-- Buy Button Lazy load Script -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MMDW8FD');</script>
	<!-- End Google Tag Manager -->



	<script>
	$(document).ready(function() {
	/* Every time the window is scrolled ... */
	$(window).scroll(function() {
		/* Check the location of each desired element */
		$('.hideme').each(function(i) {
			var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			var bottom_of_window = $(window).scrollTop() + $(window).height();
			/* If the object is completely visible in the window, fade it it */
			if ((bottom_of_window - bottom_of_object) > -200) {
				$(this).animate({
					'opacity': '1'
				}, 300);
			}
		});
	});
	});
	</script>
	<!-- Smooth Scrolling Script -->
	<script>
	$(document).ready(function() {
	// Add smooth scrolling to all links
	$("a").on('click', function(event) {
	
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();
	
			// Store hash
			var hash = this.hash;
	
			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function() {
	
				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
		} // End if
	});
	});
	</script>


</head>
<body>

	<!-- New Timer  Start-->
	<?php
		$date = 'April 05 2022 02:00 PM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
		?>
	<?php
		} else {
			echo "Times Up";
		}
		?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://cutt.ly/5DTw6tX';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 white-clr">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus for &nbsp; <img src="assets/images/white-logo.png" class="mt15 mt-md0">
                    	</div>
                    </div>

                    <div class="p0 mt20 mt-md40">
                        <div class="f-20 f-md-24 w600 text-center lh150 white-clr">
							<span class="w600">Grab My 20 Exclusive Bonuses </span>Before the Deal Ends…
						</div>
						<div class="mt5">
							<img src="assets/images/post-head-sep.png" class="img-fluid mx-auto d-block">
						</div>
                    </div>

                    <div class="mt20 mt-md40 p0">
						<div class="">
							<div class="f-md-45 f-28 w600 text-center white-clr lh150 line-center worksans">
							This Game-Changing Website Builder Lets You Create & Sell Beautiful Local WordPress Websites & Ecom Stores for Any Business in Just 7 Minutes for BIG Profits</div>
						</div>
                    </div>

                    <div class="text-center mt20 mt-sm30 p0">
                        <div class="f-md-22 f-20 lh160 w400 white-clr">
							All From Single Dashboard Without Any Tech Skills and Zero Monthly Fees
						</div>
                    </div>

                </div>
            </div>

            <div class="row mt20 mt-md30">
                <div class="col-12 col-md-10 mx-auto">
					<!-- <img src="assets/images/product-box.png" class="img-fluid mx-auto d-block"> -->
                    <div class="video-frame">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://webprimo.dotcompal.com/video/embed/9lia1m4j73" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
						</div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">
					Use Coupon Code <span class="w800 red-clr">“WEBEARLY”</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->
	<div class="second-header-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center ">
                    <div class="f-24 f-md-38 w700 lh150 black-clr text-center">
                      Create Stunning Websites & Stores For
                    </div>
                    <div class="f-28 f-md-50 w700 lh150 white-clr text-center any-shape mt-md20 mt20">
                        Any Business In Just 3 Simple Steps… 
                    </div>
                </div>
            </div>
            <div class="row mt50 mt-md65 justify-content-md-between mx-0">
                <div class="col-12 col-md-3 step-shape pb-md0 pb10">
                    <div class="step-shape1 f-md-28 f-22 w500 white-clr lh150 text-center">
                        STEP 1
                    </div>
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                       Choose 
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step1.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Select the Niche-Specific Theme & Template from proven converting 200+ stunning done-for-you Business Templates. 
                    </div>
                </div>
                <div class="col-12 col-md-3 mt50 mt-md50 relative step-shape pb20">
                    <!-- Arrow icon Images -->
                    <div class="d-none d-md-block ">
                        <img src="assets/images/right-arrow-icon.png " class="img-fluid d-block right-arrow-icon1 ">
                    </div>
                    <!-- Arrow icon Images End -->
                    <div class="step-shape1 f-md-28 f-22 w500 white-clr lh150 text-center">
                        STEP 2
                    </div>
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                        Customize
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step2.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Upload logo, choose from 2000+ possible design combinations & customize a bit in just a few seconds. 
                    </div>
                </div>
                <div class="col-12 col-md-3 mt50 mt-md0 relative step-shape pb10">
                    <!-- Arrow icon Images -->
                    <div class="d-none d-md-block ">
                        <img src="assets/images/right-top.png " class="img-fluid d-block right-arrow-icon2 ">
                    </div>
                    <!-- Arrow icon Images End -->
                    <div class="step-shape1 f-md-28 f-22 w500 white-clr lh150 text-center">
                        STEP 3
                    </div>
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                       Publish, Sell,  <br class="d-none d-md-block"> and Profit  
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step3.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Now, just publish your website with a click to use it for your business or sell website development & maintenance services in 100+ niches. 
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="f-20 f-md-24 w600 text-center black-clr lh150 ">
                        And that’s it! 
                    </div>
                    <div class="f-18 f-md-20 w400 text-center black-clr lh150 mt5">
                      You’ll be able to pull in serious income from your own offers or by  <br class="d-none d-md-block">helping clients get in front of their target market in no time! 
                    </div>
                </div>
            </div>
			<div class="row mt-md100 mt20">
                <div class="col-12 f-24 f-md-36 w600 text-center black-clr lh150">
                    WebPrimo Is Built for Everyone Across All Levels 
                </div>
            </div>
            <div class="row mt-md40 mt0">
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon1.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                       No Prior Experience Needed 
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon2.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                        Simple But Powerful Builder - <br class="d-none d-md-block"> No Coding At All 
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon3.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                        Newbie-Friendly with No Learning Curve 
                    </div>
                </div>
            </div>

        </div>
    </div>
	  
	  <!--2. Second Section End -->
       <!-- <div class="built-section">
         <div class="container">
            <div class="row ">
               <div class="col-12 f-md-45 f-28 w700 lh150 black-clr text-center">
                  WEBPRIMO Is Built For Everyone Across All Levels
               </div>
            </div>
            <div class="row mt-md45 mt20">
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon1.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     No Prior Experience Needed. <br class="d-none d-md-block"> Zero Learning Curve
                  </div>
               </div>
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon2.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     Simple But Powerful Builder with No Coding or Tech Hassel
                  </div>
               </div>
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon3.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     Newbie-Friendly - Created Keeping Both First Timers’ and Part-Timers in Mind
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      
      <!-------Bussiness Section-------->
      <div class="business-new-section ">
         <div class="container ">
            <div class="row">
                <div class="col-12 f-md-45 f-28 w600 white-clr lh150 text-center">
                   No Matter What Type of Business 
                </div>
				 <div class="col-12 f-md-20 f-20 w400 white-clr lh150 text-center mt20">
                  WebPrimo is a One Stop Solution to Create & Sell Stunning Websites, Stores, Blog, Local Site or A Service Marketplace In 100+ Niches Quick and Easy  
                </div>
            </div>
		
			  <div class="row mt20 mt-md30">
				<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
				  <div class="carousel-inner">
					<div class="carousel-item active">
					 <div class="slide-box">
						   <img src="./assets/images/slide1.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide2.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide3.png" alt="" class="mx-auto img-fluid d-block">
						</div>
					</div>
					<div class="carousel-item">
						<div class="slide-box">
						   <img src="./assets/images/slide4.png" alt="" class="mx-auto img-fluid d-block">
						    <img src="./assets/images/slide2.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide3.png" alt="" class="mx-auto img-fluid d-block">
						</div>
					</div>
				  </div>
				</div>
			  </div>
      </div>
	  </div>

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">
					Use Coupon Code <span class="w800 red-clr">“WEBEARLY”</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->

	<!-- Testimonial Section Start -->
      <div class="testimonial-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh150 black-clr text-center ">
                  See What Our Early Users & Backdoor <br class="d-none d-md-block"> Customers Are Saying About WebPrimo!
               </div>
            </div>
            <!-- <div class="row mt25 mt-md75 align-items-center ">
               <div class="col-12 col-md-10 pr-md5 f-18 f-md-20 w300 lh150 ">
                  <div class=" testi-block1 ">
                     <span class="w500 ">WebPrimo is something fresh and really exciting.</span> It's so easy to use, and the training included makes it even easier to take complete control of your business without falling prey to money sucking third
                     parties.
                     <br><br> I love the fact that it enables you to drive targeted traffic to your offers without actually being a technical nerd...all without any special skills. Absolutely brilliant guys. Keep up the great work…
                  </div>
               </div>
               <div class="col-md-2 pl-md20 mt20 mt-md0 ">
                  <img src="assets/images/david-kirby.png " class="img-fluid d-block mx-auto testi-radius " />
                  <div class="f-18 f-md-20 w600 lh150 black-clr text-center mt15 ">David Kirby</div>
               </div>
            </div>
            <div class="row mt30 mt-md70 align-items-center ">
               <div class="col-md-10 f-18 f-md-20 w300 lh150 order-md-2 ">
                  <div class=" testi-block1 ">
                     I don't write testimonials very often but couldn't help but write one after seeing the immense value you're going to get out of WebPrimo. This saves a lot of time and money as it <span class="w500 ">includes everything that one needs to be a part of the ever-growing website building and online service selling industry</span>                        without doing anything myself! Something that you must give a serious look…
                  </div>
               </div>
               <div class="col-md-2 pr-md20 mt20 mt-md0 order-md-1 ">
                  <img src="assets/images/john-zakaria.png " class="img-fluid d-block mx-auto testi-radius " />
                  <div class="f-18 f-md-20 w600 lh150 black-clr text-center mt15 ">John Zakaria</div>
               </div>
            </div> -->
			<div class="row  mt25 mt-md75 align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/testi-img1.png" alt="" class="img-fluid d-block mx-auto">
                    <img src="./assets/images/testi-img2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/testi-img3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->		
	
	<!-- Over Section Start -->
      <div class="over-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="./assets/images/over-img.png" alt="" class="img-fluid d-block mx-auto">
                  <img src="./assets/images/siteefy.png" alt="" class="img-fluid d-block mx-auto mt-md50 mt20">
               </div>
               <div class="col-12 mt-md50 mt20 f-md-26 f-22 w500 white-clr lh150 text-center">
                    Hopefully you also are sick and tired of being told that every business needs to switch to digital to survive this never-ending pandemic! And that opens a door to 
                </div>
                <div class="col-12 mt-md30 mt20 f-md-54 f-28 w700 white-clr lh150 text-center">
                    <div class="over-shape">
                        BIG $284 Billion Opportunity.
                    </div>
                </div>
            </div>
         </div>
      </div>
      <!-- Over Section End -->
	  
	  <!-- Research Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh150 black-clr text-center">
                     I am sure in this Digital Era, having a website for any business is not an option anymore…
                  </div>
                  <div class="f-28 f-md-45 w700 orange-clr1  text-center my10">
                     It’s a Compulsion Today!
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr text-center">
                     Because no business owner would like to lose those visitors & big sales from all the digital sources.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 f-24 f-md-34 w600 lh150 black-clr text-center">
                  As When You Have an SEO & Mobile Optimized Website Then You
                  Seamlessly Get Customers from All 360-Degree Directions.
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/f-image.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-24 f-md-34 w600 lh150 black-clr text-center mt30 mt-md50">
                  And You Drive Non-Stop Traffic, Leads & Profits <br class="d-none d-md-block">
                  24 by 7 - 100% Hands-Free
               </div>
               <div class="col-12 mx-auto col-md-10 mt20 mt-md50 ">
                  <img src="assets/images/h-image.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh150 black-clr text-center mt20 mt-md50"><span class="w600">So, when it comes to doing a Business, Websites are the Present and The Future! </span> <br class="d-none d-md-block">In Easy Words, Every Business Need a Website Today!
                </div>
            </div>
         </div>
      </div>
      <!-- Research Section End -->
	  <!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container mb-md60 mb30">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">
					Use Coupon Code <span class="w800 red-clr">“WEBEARLY”</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->
	  <!--Proudly Introducing Start -->
      <div class="next-gen-sec" id="product">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-md-42 f-28 w700 white-clr lh150 text-uppercase">
                     Introducing...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 ">
                  <img src="assets/images/intro-logo.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 mt-md45 mt30 f-24 f-md-30 w600 lh150 black-clr text-center">
                  World’s No. 1 Pro Website Builder That Let You Create and Sell Websites with Zero Hassles!
               </div>
               <div class="col-md-12 col-12 mt-md70 mt20 ">
                  <img src="assets/images/next-gen-img.png " class="img-fluid d-block mx-auto " />
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 col-12 mt-md0 mt20 px-md15 ">
                  <div class="row m0 p-feature py20">
                     <div class="col-md-2 p0 img-bot">
                        <img src="assets/images/ng1.png " class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-10 mt-md0 mt10">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">Create Any Type Of Site</div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">Create Stunning Local Sites, Marketplace, E-Com Stores, Affiliate Niche Sites or Blogs Easily.</div>
                     </div>
                  </div>
                  <div class="row m0 mt-md25 mt20 p-feature py10">
                     <div class="col-md-2 p0 img-bot2">
                        <img src="assets/images/ng2.png " class="img-fluid d-block mx-auto ">
                     </div>
                     <div class="col-md-10 col-12 pl-md10 mt-md0 mt10">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">Super-Customizable & Easy To Use</div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">World’s No. 1 WordPress Framework With 30+ Stunning Themes And 200+ DFY Templates To Create Site In Any Niche
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 px-md15 ">
                  <div class="row m0 p-feature py20">
                        <div class="col-md-2 p0 img-bot">
                            <img src="assets/images/ng4.png " class="img-fluid d-block mx-auto ">
                        </div>
                        <div class="col-md-10 col-12 pl-md10 mt-md0 mt10 ">
                            <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
                                No Worries of Paying Monthly
                            </div>
                            <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">
                                During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                            </div>
                        </div>
                    </div>
                  <div class="row m0 mt-md25 mt20 p-feature py20">
                     <div class="col-md-2 p0 img-bot">
                        <img src="assets/images/ng3.png " class="img-fluid d-block mx-auto ">
                     </div>
                     <div class="col-md-10 col-12 pl-md0 mt-md0 mt10 ">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
                           No Limits - Use for Yourself Or Clients
                        </div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">Sell Your Own Products, Services or Affiliate Offers or Charge Your Clients for Elegant Websites.</div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 mx-auto">
                  <div class="row m0 p-feature py20 mt-md25 mt20">
					<div class="col-md-2 p0 img-bot">
						<img src="assets/images/ng5.png " class="img-fluid d-block mx-auto ">
					</div>
					<div class="col-md-10 col-12 pl-md10 mt-md0 mt10 ">
						<div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
							50+ More Features
						</div>
						<div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">
							We’ve Left No Stone Unturned to Give You an Unmatched Experience
						</div>
					</div>
				</div>
               </div>
               <div class="col-12 f-20 f-md-22 w400 lh150 mt20 mt-md70 text-center black-clr ">
                  It has never been easier to create professional, SEO-optimized, and all-device friendly business websites that drive customers 360 degree but with WebPrimo, it is EASY & FAST.<br><br> Replace your old school website builder with an
                  amazing WordPress Framework that helps ANYONE create stunning, lightning fast and clean websites.
               </div>
            </div>
         </div>
      </div>
      <!--Proudly Introducing End -->
	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700">When You Purchase WebPrimo, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.png" class="img-fluid mx-auto d-block">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">Magic Video Templates Review Pack</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>There’s no secret in the fact that videos are the #1 source to boost customer attention& engagement.</li>
								<li>Using Magic Video, you can easily create your own animated video in just minutes with 100+slides to choose from.</li>
								<li>This Bonus with WebPrimo’s DFY marketing materials will instantly change your marketing presentation to be more customer-centric & drive better results from your campaigns.</li>						
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
					   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-7">
							<img src="assets/images/bonus2.png" class="img-fluid mx-auto d-block">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color mt20 mt-md0">
					  			Facebook Webinar Pro Plugin
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this powerful WordPress plugin you can create amazing Facebook webinar landing pages & drive maximum traction for your offers.</li>
								<li>Using this Bonus, you can collect unlimited leads inside WordPress and shoot them targeted </li>
								<li>emails on automation using WebPrimo’s Autoresponder Integration</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			WordPress Website Security
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This specific training course is designed to help you understand how to secure and protect your valuable WordPress site.</li>
								<li>This Bonus is a great asset to use with WebPrimo as it helps to secure your business website made for marketing purposes, payment transactions & various business operations.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-7">
					  		<img src="assets/images/bonus4.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-0">
					 	 	<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Audio Compression Magic
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Magically reduces the file size of your digital audio products up to an astonishing 82% while maintaining top sound quality.</li>
								<li>Use this bonus to deliver light sized high quality professional marketing voice overs given in WebPrimo. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Lead Book Generator
					  		</div>
							<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this powerful plugin, you can easily integrate Facebook Lead Ads with your autoresponder and have your leads added to your mailing list automatically!</li>
								<li>Use this Bonus with Webprimo’s Autoresponder Integration for building your list and leveraging your sales quickly with Facebook.</li>
							</ul>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 red-clr">"WEBEARLY"</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 mx-auto col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown counter-black">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-7">
					  		<img src="assets/images/bonus6.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			WP Profit Doubler
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This powerful WordPress Plugin allows you to make a second offer to those visitors who are leaving without any purchase. </li>
								<li>Use this Bonus with WebPrimo’s Sales & Email Templates to double-down profit from your abandoned leads.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus7.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Turbo Tube Engage Pro
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This Software helps you to make your videos Interactive with just a few simple clicks.</li>			
								<li>Use this Bonus to transform your Video Commercials given in WebPrimo into a powerful list building & profit-making machine by improving your viewers’ interactivity.</li>			
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus8.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			BIZ Landing Page Plugin
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This is an easy-to-use WordPress Plugin that creates Social-Powered Business Landing Pages in seconds.</li>
								<li>This Bonus when utilized with WebPrimo’sDFY Themes and Marketing Templates will ease your way to build professional websites within seconds. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
					</div>
				</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
						<div class="col-md-5 col-12">
							<img src="assets/images/bonus9.png" class="img-fluid mx-auto d-block ">
						</div>
						<div class="col-md-7 col-12 mt20 mt-md0">
							<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
								Power Tools Video Site Builder
							</div>
							<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this software you can instantly create your own complete money-making video site featuring AdSense and Amazon ads with 120 profitable videos sourced from YouTube. </li>
								<li>Use this Bonus with DFY Videos given in WebPrimo to start your own video sites using WebPrimo DFY Themes & monetize them by showing ads on them.</li>
							</ul>
						</div>
					</div>
				</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus10.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Chatbot Marketing Mastery
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>In this guide, you’re going to learn more about how chatbots can be used for marketing, and whether chatbots are a good fit for your business.</li>
								<li>This Bonus will help you to use the free chatbot WordPress plugins efficiently with DFY Sales Chat/Call Script given with WebPrimo. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 red-clr">"WEBEARLY"</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 mx-auto col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown counter-black">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Search Marketing 2.0
					  		</div>
					 		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Professionally designed lead capture package that comes withnecessary tools, powerful media, and complete email marketing course.</li>
								<li>This Bonus will help you create high quality Search Engine Optimized text and graphic content along with the Web Content given in WebPrimo.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus12.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Customer Loyalty Magnet
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>5-Day Training on how to build customer loyalty through incentives.</li>
								<li>Get Email Newsletter Series, Lead Capture Webpage and Graphics to use Customer Loyalty Magnet.</li>
								<li>Use this Bonus with WebPrimo as a plus to increase your overall revenue by capturing high quality leads with huge conversions.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus13.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			250 HTML Templates WP Themes and Graphics
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Get complete package of 120 HTML Templates, 10 Sales Graphics Packs, 60+ WP Themes, 17 CSS Content Template, 40 Header Images, 4 Video Squeeze Pages & many more.</li>
								<li>This Bonus when used with DFY themes of WebPrimo will create multiple websites with various features & graphics in all different niches.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus14.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			WP Email Timer Plus
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even inside your emails</li>
								<li>This Bonus will increase your sales conversions up to 4x when used on the Websites build using WebPrimo Themes. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->

	

	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus15.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
						  		Social Pop-Ups Plugin
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this plugin you can create your own social media pop-ups for your WordPress blog or Website.</li>
								<li>This Bonus is must to have in WebPrimo to capture your visitor’s attention by showing your social media popups along with social sharing icons at footer & below the posts.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 red-clr">"WEBEARLY"</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Agency Licence
</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 col-12 mx-auto mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown counter-black">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus16.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			WP In-Content Popup Pro
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</li>
								<li>Use this with WebPrimo to trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your products or services</li>				
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	  	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus17.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			WP Survey Creator
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This is a WordPress Plugin that creates surveys with different types of questions with customizable presentation on your website.</li>
								<li>This unique bonus when combined with WebPrimo’s analytics feature, will help you target your clients efficiently with better understanding of consumer behavior.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus18.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Stakk Review Pack
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This is a Mail Pop-Up Plugin</li>
								<li>Use this Bonus along with high converting sales & email swipes given with WebPrimo to show Popups right inside your mails. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				  		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus19.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					 	 	<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Auto Support Bot
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Auto Support Bot is A 24/7 Live Chatbot on Your Websites and Video Pages.</li>
								<li>Use this Bonus in VidVee to Automate your Customer Support Service by showing 24x7 Live Chatbot Popup.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-7 col-12">
					  		<img src="assets/images/bonus20.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-0 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
					  			Exit Pop Pro
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Exit Pop Pro is a WordPress Plugin that will show instant exit popup to the visitor on exiting the website. </li>
								<li>Use this Bonus along with WebPrimo’s Call-To-Action feature to show highly effective Exit Popup.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 orange-clr">$3275!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh140 w400">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
				</div>
				<!-- Timer -->
				<div class="col-md-8 col-md-10 mx-auto col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="strip_footer clear mt20 mt-md40">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<img src="assets/images/white-logo.png" alt="logo" class="img-fluid mx-auto d-block">
				</div>
                <div class="col-md-12 col-md-12 col-12 f-16 f-md-18 w300 mt20 lh150 white-clr text-center">
                   Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
                </div>
                <div class="col-lg-12 col-md-12 col-md-12 col-12">
					<div class="row">
						<div class="col-md-3 col-12 f-16 f-md-18 w400 w300 mt10 mt-md40 lh150 white-clr d-left-m-center">Copyright © WebPrimo</div>
						<div class="col-md-9 col-12 f-md-18 w300 f-16 white-clr mt10 mt-md40 xstext-center d-right-m-center">
							<a href="https://support.bizomart.com/">Contact</a>&nbsp;&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/privacy-policy.html">Privacy</a>&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/terms-of-service.html">T&amp;C</a>&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/disclaimer.html">Disclaimer</a>&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/gdpr.html">GDPR</a>&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/dmca.html">DMCA</a>&nbsp;|&nbsp;
							<a href="http://webprimo.co/legal/anti-spam.html">Anti-Spam</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop px0">	
					<div class="col-12 px0">
						<img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 px0">
						<div class="f-20 f-md-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="hidden-xs"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<!-- End Google Tag Manager (noscript) -->
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
		if (f.fbq) return;
		n = f.fbq = function() {
			n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq) f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
		}(window,
		document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1777584378755780');
		fbq('track', 'PageView');
	</script>
  	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	<!-- Google Code for Remarketing Tag -->
	<!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  	<div style="display:none;">
	 	<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 748114601;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			var google_user_id = '<unique user id>';
			/* ]]> */
	 	</script>
	 	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 	<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
			</div>
	 	</noscript>
  	</div>
	<!-- Google Code for Remarketing Tag -->
	<!-- timer --->
  	<?php
	 	if ($now < $exp_date) {
	?>

  	<script type="text/javascript">
		// Count down milliseconds = server_end - server_now = client_end - client_now
		var server_end = <?php echo $exp_date; ?> * 1000;
		var server_now = <?php echo time(); ?> * 1000;
		var client_now = new Date().getTime();
		var end = server_end - server_now + client_now; // this is the real end time
		
		var noob = $('.countdown').length;
		
		var _second = 1000;
		var _minute = _second * 60;
		var _hour = _minute * 60;
		var _day = _hour * 24
		var timer;
		
		function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
			return;
		}
		
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);
		if (days < 10) {
			days = "0" + days;
		}
		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var i;
		var countdown = document.getElementsByClassName('countdown');
		for (i = 0; i < noob; i++) {
			countdown[i].innerHTML = '';
		
			if (days) {
				countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
			}
		
			countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
		}
		
		}
		timer = setInterval(showRemaining, 1000);
  	</script>
  	<?php
	 	} else {
	 	echo "Times Up";
	 	}
	?>
  <!--- timer end-->
  
  <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKWBVSR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
</body>
</html>

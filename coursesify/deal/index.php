<!Doctype html>
<html>
   <head>
      <title>Coursesify Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Coursesify | Deal">
      <meta name="description" content="Coursesify">
      <meta name="keywords" content="Coursesify">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.coursesify.com/deal/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Coursesify | Deal">
      <meta property="og:description" content="Coursesify">
      <meta property="og:image" content="https://www.coursesify.com/deal/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Coursesify | Deal">
      <meta property="twitter:description" content="Coursesify">
      <meta property="twitter:image" content="https://www.coursesify.com/deal/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'coursesify';
allowedDomain = 'coursesify.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>

   </head>
   <body>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center relative">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-14 f-md-16 w400 white-clr text-md-end text-center relative ">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Buy Now</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading f-16 f-md-18 w700 lh160 white-clr">
                     Copy The Secret Formula That Made us $525/Day <br class="d-none d-md-block">
                     by Selling Courses on Automation
                    </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-36 f-28 w600 text-center white-clr lh150 relative">
               Brand New Academy Site Builder with <span class="w900 yellow-clr"> Advance Course Selling System, Progress Bar, Ratings & Certificate, </span> <span class="under">Preloaded with 400+ Video Courses and E-books</span> 
                  </span> 
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 text-center lh150 white-clr2 relative">
                  DFY Courses to Start Selling Right Away | No Course Creation | No Camera or Editing | <br class="d-none d-md-block"> No Tech Hassles | Even A 100% Beginner or Camera SHY Can Start. 
                  </div>
               </div>
               <div class="col-md-10 mx-auto col-12 mt-md50 mt20">
                  <!-- <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video">
                     <iframe src="https://coursesify.dotcompal.com/video/embed/rb1d7ll1t8" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20 relative">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED 
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 ">
                  <div class="f-16 f-md-16 lh160 w400">
                     <ul class="kaptick pl0">
                        <li><span class="w700">Create Beautiful Mobile Responsive Academy Sites</span> With Marketplace, Blog & Members Area within Minutes </li>
                        <li><span class="w700">Choose From 400+ HD Video Trainings in 30+ HOT Topics</span> to Start Selling & Making Profit Instantly</li>
                        <li><span class="w700">Create Your Own Courses in ANY Niche –</span> Add Unlimited Lessons, Videos and E-books </li>
                        <li>Made For Newbies & Experienced Marketers- <span class="w700">Even Camera-Shy People Can Start Selling Courses Right Away</span></li>
                        <li><span class="w700">Accept Payments Directly in Your Account</span> With PayPal, Stripe, JVzoo, ClickBank, Warrior Plus Etc </li>
                        <li>No Traffic, Leads, or Profit Sharing With Any 3rd Party Platform<span class="w700"> - Keep 100% Profits & Control On Your Business </span></li>
                        <li><span class="w700">Get Started Immediately -</span> Launch Your First Academy Site with Ready-To-Sell Courses In NO Time</li>
                        <li><span class="w700">Have Unlimited Earning Potential –</span>Teach Anything or Everything Quick & Easy </li>
                        <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online… </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-16 f-md-16 lh160 w400">
                     <ul class="kaptick pl0">
								<li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses </li>
                        <li><span class="w700">In-Built Ready To Use Affiliate System</span> to Boost your Sales & Profits</li>
                        <li><span class="w700">100% SEO & Mobile Responsive</span> Academy Site & Marketplace </li>
                        <li>Easy And Intuitive To Use Software With<span class="w700">Step By Step Video Training</span></li>
						      <li><span class="w700">Rating & Review</span> to Engage & Grow users Via passive leads </li>
                        <li><span class="w700"> Tangential Benefit to Help Agency simultaneously </span>sell video courses & products on site. </li>
                        <li> Progress Bar to track the <span class="w700"> progress of the course </span>  </li>
                        <li><span class="w700"> 30+ Fully Editable </span>Certificates for each successful course completion</li>
                        <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <!--2. Second Section End -->
      <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- host section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 white-clr text-center ">					
               Start Your Own Profitable E-Learning Business in Just 3 Simple Steps… 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 1
                     </div>
                     <img src="assets/images/upload-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Create/Choose Course 
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 2
                     </div>
                     <img src="assets/images/customize-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Add Payment Options
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 3
                     </div>
                     <img src="assets/images/publish-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                         Publish & Profit 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- host section end -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  See The Sales We Got with A Simple<br class="d-none d-md-block"> Academy Site Created Recently
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                   And, We’re Making an Average $525 In Profits<br class="d-none d-md-block"> Each & Every Day
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
    
      <!-- Video Testimonial -->
      <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 text-center black-clr"> 
                  Even Beta Users Are Getting EXCITING Results by Selling Our Done-For-You Courses
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-1.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-2.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-4.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <!-- Video Testimonial end -->
      <div class="leraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <img src="assets/images/e-learning-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-30 f-24 lh140 w600 text-center black-clr mt20 mt-md50">And it is Absolutely Booming Post this Covid-19 Pandemic… 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w500 lh140 text-center">
                  The Global Pandemic has shut doors for many Schools, Universities, and Training Institutes. But in this Period, E-Learning has witnessed a positive trend and opened hundreds of Windows to many E-learning platforms… 
                  </div>
                  <div class="mt20 mt-md50">
                     <img src="assets/images/proof3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 text-center mt20 mt-md30">              
                  And this tremendous growth in student enrolments, shows a HUGE Opportunity! 
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-32 w600 lh140 text-center black-clr">Yes, It’s Very High In-Demand! </div>
               </div>
               <div class="col-12 mt20 f-18 f-md-20 w500 lh140 text-center">
                  And there is no limit in sight! – You can create &amp; sell courses on any topic and in any niche like thousands of others are selling on sites like Udemy<sup>TM</sup>, Skillshare<sup>TM</sup>, Udacity<sup>TM</sup>, Coursera<sup>TM</sup>, and More! 
               </div>
            </div>
         </div>
      </div>

      <!-- Stil No Compettion Section Start -->
      <div class="still-there-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-42 w700 lh140 text-center  black-clr">
                     Look At These People Generating Six and Seven Figures Just By Selling Simple Courses Online 
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt20 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/smith.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Len Smith is making <span class="w700">$90K every month </span>- selling his Copywriting Secrets Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12 order-md-2">
                     <img src="assets/images/john-omar.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0 order-md-1">
                     <div class="col-md-6 col-8 ms-md-0 mx-auto line-5" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        John Omar and Eliot Arntz made <span class="w700">$700K in a month</span> by Selling their iOS App Development Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/rob-percival.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Rob Percival makes <span class="w700">$150K every month</span> - selling the same programming course to new students
                     </div>
                  </div>
               </div>
               <div class="row mt30 mt-md65">
                  <div class="f-md-24 f-20 w400 lh160 black-clr  text-center">
                     <span class="w700 f-md-36 f-22 w400 lh140"> But You’re Probably Wondering?</span><br> “Can I Make A Full-Time Income Selling My Knowledge?”
                  </div>
               </div>
         </div>
      </div>
      <!-- Stil No Compettion Section End -->
      <div class="eleraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-20 f-md-28 w500 lh160 text-center">
                  <span class="w700 f-24 f-md-36"> Of Course, You Can!</span><br>  See How Newbies Made Millions from E-Learning Business During Lockdown 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
			    <div class="col-12 col-md-6 mt20 mt-md0 p-md-0">
                  <img src="assets/images/proofimg1.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                  <img src="assets/images/proofimg2.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg3.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg4.png" class="img-fluid d-block mx-auto">
               </div>
               </div>
               </div>
               <div class="col-12 w600 f-md-28 w500 lh140 text-center mt20 mt-md30">
               So, It is the perfect time to get in and profit from this exponential growth! 
               </div>
            </div>
            <div class="row mt30 mt-md70">
               <div class="col-12 text-center">
                  <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                     <div class="f-28 f-md-45 w800 lh140 text-center white-clr">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12  f-20 f-md-24 lh150 w600 text-center black-clr lh150">
                  It can be stated that…<br>
                  <span class="mt10 d-block">
                  E-Learning and Online Course Selling is the Biggest Income Opportunity Right Now! 
                  </span>
               </div>
               <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh150 mt20 mt-md30">
                  So, How would you like to Get Your Share?
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="new-dcp-team-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-24 f-20 w600 lh150 black-clr">
                  Dear Struggling Marketer, 
                  </div>
                  <div class="f-md-32 f-24 w700 lh150 blue-clr mt10">
                  From the desk of Ayush Jain and Pranshu Gupta 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-md-20 f-18 w400 lh150 black-clr ">
                        I’m Ayush Jain along with my friend Pranshu Gupta. 
                           <br><br>
                           We are happily living the internet lifestyle since last 10 years with full flexibility of working from anywhere at any time. <br><br>
                        </div>
                        <div class="mt20">
                           <div class="f-md-20 f-18 w400 lh150 black-clr ">
                           And while we don’t say this to brag, <br>
                              <span class="w700">we do want to make one thing clear: </span><br><br>
                              <span class="w700 f-20 f-md-22">E learning is a multi-billion-dollar industry and… </span><br><br>
                              Yes,<br>
                               <span class="w700">It’s rapidly growing by leaps and bounds every year</span> and even during the post pandemic era. 
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/ayush-pranshu.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
    <!-- potential Section Start -->
    <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-42 f-28 w600 lh140 text-center white-clr">
                     No Matter What Niche, You can Teach Anything and Everything Online Including...
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt30">
               <div class="col-md-3 col-6">
                  <div class="">
                     <img src="assets/images/pn1.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Programming
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt0">
                  <div class="">
                     <img src="assets/images/pn2.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     School/College<br> Courses
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn3.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Photography
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn4.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Dining Etiquette
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn5.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Yoga
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn6.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Gardening
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn7.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Makeup Hacks
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn8.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Graphic Designing
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn9.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Weight Loss
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn10.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Pet Care
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn11.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Body Building
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn12.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Cooking
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn13.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Personality Development
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn14.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Video Editing
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn15.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Home Decor
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn16.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Baking
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn17.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Wine Making
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn18.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Car Repairing
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn19.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Foreign Languages
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn20.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Digital Marketing
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn21.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Accounting
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn22.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Dancing
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 f-md-20 f-18 w500 mt20 mt-md50 lh140 text-center white-clr">
               To encash in multiple niches, we recently started 3 more academy sites using <br class="d-none d-md-block"> Coursesify & amazed with the results… 
               </div>
            </div>
         </div>
      </div>
      <!-- potential Section End -->
      
      <div class="earned-money-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-42 w500 lh160 text-center">
                  Wala! <span class="w700">We Made $3,660 In Sales in Last 15 Days…</span> Following This Proven System
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/proof6.png" class="img-fluid d-block mx-auto">
                  <img src="assets/images/proof5.png" class="img-fluid d-block mx-auto mt20">
               </div>
            </div>
         </div>
      </div>
<!---------f99----------->
<div class="eleraning-section">
   <div class="container">
      <div class="row">
      <div class="col-12 f-md-32 f-24 w500 lh150 text-center black-clr">
               <span class="w700">Every Random Joe & Jane Can Get Results Like These</span><br class="d-none d-md-block"> WITHOUT Quitting Their Current Job, WITHOUT Any Prior Skills Or WITHOUT Risking Their Hard Earned Money. 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md30">
                  <img src="assets/images/earned-money.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-20 f-18 w400 lh150 text-center black-clr mt20 mt-md30">
                  <span class="w600">Yep, you read that right. </span><br><br>
                  Now, even you can start getting REAL profits like this easily <span class="w600">IF YOU DO IT RIGHT!  It’s a REAL DEAL but if go in wrong direction, it can waste your months of time & 100s of dollars.  </span>
               </div>
      </div>
   </div>
</div>
<!---------f99----------->
      <!-- Problem Section -->
      <div class="probelm-bg-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 col-md-10 text-center mx-auto">
                  <div class="row align-items-center">
                     <div class="col-md-2">
                        <div class="d-none d-lg-block">
                           <img src="assets/images/sad-emoji1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-8 f-md-45 f-28 lh140 w700 black-clr  text-center">
                     Here’s The Problem!
                     </div>
                     <div class="col-md-2 ">
                        <div class="d-none d-lg-block">
                           <img src="assets/images/sad-emoji1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 problme-shape">
                  <div class="f-24 f-md-36 w700 lh150 yellow-clr text-center">
                     Creating Engaging Video Trainings <br class="d-none d-md-block"> Is PAINFUL &amp; Time-Consuming
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 white-clr mt20 mt-md30">
                     <ul class="noneed-listing5 pl0">
                        <li><span class="w600">You need to research & plan daily.</span> And staying up-to-date with latest topics needs lots of effort </li>
                        <li><span class="w600">You Need To Be On Camera.</span> This can be a major blocker in case you are shy or introvert and have a fear that people would judge and laugh at your videos. </li>
                        <li><span class="w600">You need to learn COMPLEX video &amp; audio editing skills.</span> Most software are complex and difficult to learn, especially if you are a non-technical guy like us. </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-7 mt-md50">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Buying Expensive Equipment Can Leave Your Back Accounts Dry
                  </div>
                  <div class="f-20 f-md-21 w400 lh150 black-clr mt10">
                     To even get started with first video training, you need expensive equipment, like a
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing6 pl0">
                        <li class="w600">Nice camera, </li>
                        <li class="w600">Microphone, and </li>
                        <li><span class="w600">Video-audio editing software</span> That would cost you THOUSANDS of dollars. </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5">
                  <img src="assets/images/need-img.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 f-24 f-md-36 w700 lh150 black-clr">Not All E-learning Platforms Are Created Equal...</div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-20 f-md-22 w400 lh150 black-clr">
                     <ul class="noneed-listing6 pl0">
                        <li><span class="w600">Pay Profit Share or Fixed Monthly Fee.</span>
                           You must pay a huge share of your profits as fixed or percent fees
                        </li>
                        <li><span class="w600">Lose Traffic.</span>
                           You have no control over your hard-earned traffic, and your visitors gets distracted with your competitors offers
                        </li>
                        <li><span class="w600">A Lot of Competition.</span>
                           It is hard to engage people who checks for the reviews or star ratings and prefer to take courses of established competitors
                        </li>
                        <li><span class="w600">Lose Leads.</span>
                           You get no leads which means you lose any future opportunities to connect with them and offer more services
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-12 col-md-5 order-md-2">
                  <img src="assets/images/problem1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 corder-md-1 mt20 mt-md0">
                  <div class="f-24 f-md-36 w700 lh150 black-clr text-md-start text-center black-clr text-capitalize">
                     Now question comes in mind, why not to setup your own online platform?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh150 black-clr">
                     <ul class="noneed-listing6 pl0">
                        <li><span class="w600">Big Learning Curve.</span>
                           To learn marketing, designing, coding & testing to build a robust platform
                        </li>
                        <li><span class="w600">Buy Multiple Apps & Hire Freelancers.</span>
                           For website, membership site & cart & also waste money & time with inefficient developers
                        </li>
                        <li><span class="w600">Buy Domain & Hosting.</span>
                           Pay Recurring fees to domain, Hosting & video streaming companies
                        </li>
                        <li><span class="w600">Continuous Monitoring.</span>
                           To ensure that payments & training are up & running properly
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 f-md-32 f-22 w600 black-clr lh140 mt20 mt-md30 text-center">And that’s the REASON, 85% of the entrepreneurs leave their business in dreams or within just the first year of starting. 
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Problem Section End-->
      <!-- Well, not anymore Section Start-->
      <div class="anymore-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 f-md-45 f-28 lh140 w700 text-center black-clr">But not anymore!</div>
               <div class="col-12 f-md-20 f-18 w500 mt20 mt-md30 lh140 text-center ">After years of learning, planning, designing, coding, debugging,<br class="d-none d-md-block"> as well as months of real user testing…
                  <br><br> We are proud to release our solution that will make creating and selling <br class="d-none d-md-block">courses easier and faster than ever…
               </div>
            </div>
         </div>
      </div>
      <!-- Well, not anymore Section End-->
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
               <div class="col-12 text-center mt70 mt-md70">
                  <div class="prdly-pres f-24 w700 text-center white-clr">
                     Proudly Introducing…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="max-height:120px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
               </div>
               <div class="f-md-30 f-22 w700 yellow-clr lh150 col-12 mt-md30 mt20 text-center">
               1-Click Academy Builder That Creates and Sells Courses Online with Zero Tech Hassles. It Also Comes Preloaded with 400+ HD Video Trainings Ready to Sell in Next 60 Seconds 
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
            </div>
         </div>
      </div>
      <div class="steps-section2">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-md-42 f-28 lh140 w700 black-clr  text-center" >
                     <span class="blue-clr">Start Your Own Profitable E-Learning Business</span><br class="d-none d-md-block"> In Just 3 Simple Steps… 
                  </div>
               </div>
               <div class="col-12">
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP1:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                              Create or Choose a Course 
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                              Add your own video lessons, e-books, or choose from 400+ HD Video Trainings in 30+ done-for-you video courses. 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6   mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP2:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                                 Add Payment Options
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                                 Select your payment gateways to accept payments directly in your accounts. (All major options included)
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1  mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP3:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                                 Publish & Profits
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                                 Publish your courses on your own branded e-learning site and keep 100% of the leads and profits in your pocket.
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Client Video -->
      <div class="client-video-section" id="productdemo">
         <div class="container  ">
            <div class="row">
               <div class="col-12 f-md-45 f-28 lh140 w700 text-center black-clr">
                  Watch Coursesify In Action:
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md60">
                  <!-- <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video video-shadow">
                     <iframe src="https://coursesify.dotcompal.com/video/embed/zfngwe86m2" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>   
               <div class="col-12 mt20 mt-md70">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cilent Video end -->
      
      <!-- Video Testimonial -->
      <div class="testimonial-section1">
         <div class="container  ">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w500 text-center black-clr">And Here's What Some More <br class="d-none d-md-block">
                     <span class="w700">Happy Users Say About Coursesify</span>
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-5.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-6.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-7.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-8.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
      <!-- Video Testimonial end -->
      <!-- Feature 1 Section Start -->
      <div class="feature-section" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140  yellow-clr text-center">
                     Here Are The GROUND - BREAKING Features That Makes Coursesify A CUT ABOVE The Rest
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-24 f-md-36 w600 white-clr lh140">
                     Build A Beautiful Academy Site In A Few Minutes
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt30 mt-md30 white-clr">
                  Building An Online Academy just got faster and easier. Simply use our state-of-the-art technology to get started today.
               </div>
               <div class="col-12 mt-md50 mt20">
                  <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 white-clr mt-md40 mt30">
                  Everything you need to run a profitable six or seven figure pro level academy site is already included. Just tap in and profit.
               </div>
            </div>
         </div>
      </div>
      <!-- Feature 1 Section End -->
      <div class="ftr2-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn1.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Elegant Marketplace
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn2.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Engaging Blog
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn3.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Membership Site For All Your Students
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Line 2 -->
               <div class="col-12 mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn4.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt-md15 mt20 text-center black-clr  lh140">
                           Academy Home
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn5.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Beautiful Slider
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn6.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Multiple Color Themes
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Line 3 -->
               <div class="col-12  mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn7.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Attractive Header & Menu
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn8.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt-md50 mt20 text-center black-clr  lh140">
                           Login with Social Media
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn9.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Static Pages
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Line 4 -->
               <div class="col-12 mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn10.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Google Map
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn11.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           SEO & Mobile Ready
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn12.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Social Media Ready
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- section 3 -->
      <div class="secc-10">
         <div class="container">
            <div class="row">
               <div class="col-12 f-24 f-md-36  text-center w600 black-clr lh140">
                  Quickly Sell Courses That Attract Tons of Buyers.<br class="d-none d-md-block">Coursesify Makes This Child's Play.
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 black-clr lh140 text-xs-center">
                           Create Your Courses– Add Unlimited Lessons (Video, E-Book & Reports)
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/fnn13.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-28 w600 black-clr lh140 text-xs-center">
                           List & Sell Courses On Your Own Marketplace.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/fnn14.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Create Courses On Any Topic While Becoming An Authority In Any Niche
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/f10.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12  mt30 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                        Flexible Pricing and Payment Options: Accept Payments with your favorite Payment Gateway PayPal and Stripe, or integrate with marketplaces like JVZoo, ClickBank, Warrior Plus, etc   
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/set-courses.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Course Progress Bar for your Students so that they can track the progress of the course they are perusing.   
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/course-progress.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12  mt30 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Course Completion Certificate so that you can issue certificates to your students on completing the course.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/course-complete.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Review & Rating on Course Pages to get the review for each course individually   
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/set-review.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ftr8-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36  w600 lh140 text-center black-clr">
                     You’ll Have 100% Control Of Your Business. 
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 text-center black-clr mt20 mt-md30">
                     Coursesify Lets You Keep All Your Leads, Traffic and Profits <br class="d-none d-md-block">Without Sharing A Dime With 3rd Party Marketplaces..
                  </div>
                  <div class="mt-md40 mt20 f-18 f-md-20 w400 lh140 black-clr">
                     Whenever you sell courses through 3rd party platforms you pay fixed fees while also fighting against other competitors on the same marketplace.<br><br>
                     What’s worse is that some of these same marketplaces keeps the leads and buyers for themselves while profiting off your hard work.<br><br>
                     And you don’t get any payback for future products they promote with the leads you supplied!
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                  <div class="mt-md60 mt20 f-18 f-md-20 w400 lh140 black-clr">
                     But with Coursesify you can bypass all of that. <span class="w600"> You won’t have to pay an additional fee and every single lead you get will be yours to keep forever.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="secc-11">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-24 w500 lh140 text-center yellow-clr">
                  No Course Of Your Own To Sell? No Problem! 
                  </div>
                  <div class="mt-md30 mt20 f-24 f-md-36  w600 lh140 text-center white-clr">
                  You Can Get Started Quickly With 400+ HD Video Trainings Packed in 30+ Done-For-You Courses on 30+ RED Hot Topics That Come Included With Coursesify. 
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 white-clr">
                     We want to make sure you have the best possible chance to succeed with Coursesify.  That’s why we’re including these hot and in demand 400+ video trainings. You do not need to write a single word or be on camera to start selling online. <br><br>
                     All the sales materials are included.  Just add your logo, pricing, and buy buttons and start selling in an instant! Take a look at what’s included!
                  </div>
               </div>
               <div class="col-12 col-md-6 mt80">
                  <div class="t-block1">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t1.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                           400+ HD Video Trainings on 30+ Hot Topics 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt80">
                  <div class="t-block2">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t2.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              Training Guides & PDF Reports
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt-md100 mt80 clear">
                  <div class="t-block3">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t3.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              High Converting Sales Material
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt-md100 mt80">
                  <div class="t-block4">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t4.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              Affiliate System
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="personalised">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36  w600 lh140 text-center">
                     Separate Personalized Membership Sites For<br class="d-none d-md-block">All Your Students 
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 text-center">
                     Coursesify also includes an intuitive members area to suit your brand. Your students will be able to easily access their courses, <span class="w600">check their support tickets, previous purchases as well as purchase any additional course materials with 1 click.</span>						  
                  </div>
                  <div class="mt-md30 mt20 f-20 f-md-24 w600 lh140 black-clr">                              
                     Here’s a look at what’s all included:
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm1.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Clean Members Dashboard
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm2.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Check Courses Progress
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm3.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Learn on Any Device, Anywhere and Anytime
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row 1 -->
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block ">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm4.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Support System Included
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row 2 -->
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block ">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm5.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Upsell To More Courses Easily.
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row 3 -->
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm6.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Automatic Payment History & Email Notifications
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ar-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-6 col-12">
                        <div class="f-24 f-md-36  w600 lh140 black-clr">
                           Your Academy Will Load Correctly On Every Device And Is 100% Mobile Responsive.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0">
                        <img src="assets/images/mobile-responsive.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="more-seo">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-6 col-12 order-md-2">
                        <div class="f-24 f-md-36 black-clr w600 lh140">
                           Engage & Convert  More of Your SEO & Social Traffic, into Paying Customers
                        </div>
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                        <img src="assets/images/more-seo-img.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="last-app">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36 black-clr  w600 lh140 text-center">
                     Coursesify Is The LAST App You Will Ever Need To Start Dominating The HUGE E-Learning Market.
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 text-center">
                     Coursesify gives you a complete system to create courses, sell them online as well as <span class="w600">manage entire business through 1 central dashboard. </span><br><br>
                     <span class="w600">All the heavy lifting is done for you. You only need to integrate your knowledge into Coursesify and you can create amazing courses easily that attract sales like a magnet on steroids.</span>
                  </div>
               </div>
               <!-- Col 1 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block1 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           All-In-One Platform
                        </div>
                     </div>
                  </div>
                  <!-- block2 -->
                  <div class="c-block2 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Reports & Analytics
                        </div>
                     </div>
                  </div>
                  <!-- block3 -->
                  <div class="c-block3 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           SSL Secured Subdomains
                        </div>
                     </div>
                  </div>
                  <!-- block4 -->
                  <div class="c-block4 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c4.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Members & Lead Management
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Col 2 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block5 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c5.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           1 Click Product Creation
                        </div>
                     </div>
                  </div>
                  <!-- block2 -->
                  <div class="c-block6 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c6.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Custom Domain
                        </div>
                     </div>
                  </div>
                  <!-- block3 -->
                  <div class="c-block7 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c7.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Ticket Management System
                        </div>
                     </div>
                  </div>
                  <!-- block4 -->
                  <div class="c-block8 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c8.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           100% GDPR And CAN-SPAM Compliant
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Col 3 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block9 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c9.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Sales Page Builder with Editor
                        </div>
                     </div>
                  </div>
                  <!-- block2 -->
                  <div class="c-block10 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c10.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           FREE Hosting & Video Player
                        </div>
                     </div>
                  </div>
                  <!-- block3 -->
                  <div class="c-block11 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c11.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Automated Email Notifications
                        </div>
                     </div>
                  </div>
                  <!-- block4 -->
                  <div class="c-block12 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c12.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           FREE Courses to Build List
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="more-seo">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12 order-md-2">
                        <div class="f-24 f-md-36 black-clr w600 lh140">
                           Seamless Integrations With TOP Autoresponders
                        </div>
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                        <img src="assets/images/seamless.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ar-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12">
                        <div class="f-24 f-md-36  black-clr w600 lh140">
                           Easy and Intuitive To Use Software with Step by Step<br class="d-none d-md-block">Video Training
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0">
                        <img src="assets/images/f23.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features End-->
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                      <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="table-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 black-clr">
                     We Literally Have no Competition
                  </div>
               </div>
            </div>
            <div class="row g-0 mt70 mt-md100">
               <div class="col-md-4 col-4">
                  <div class="fist-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-32 f-16 w700 justify-content-start justify-content-md-center">Features</li>
                        <li class="f-md-18 w600">Starting Pricing </li>
                        <li>Transaction Fees</li>
                        <li class="text-start">400+ HD Video Trainings Packed in 30+ Done-For-You Courses </li>
                        <li>Free Hosting for Courses</li>
                        <li> No. of Courses</li>
                        <li>Add Your Own Courses</li>
                        <li>Instant payouts</li>
                        <li>Active Members </li>
                        <li>UNLIMITED Sub-Domains</li>
                        <li>No Hosting & Domain Hassles</li>
                        <li>Video Based Tutorials </li>
                        <li>PDF Training Guide</li>
                        <li>Hot and Evergreen Products</li>
                        <li>DFY 400+ Blogs </li>
                        <li>Add Your Own Blogs </li>
                        <li>100% Mobile Responsive</li>
                        <li> Pre-Installed Lead Generation Page</li>
                        <li>Inbuilt Ticket System</li>
                        <li>Dedicated Members Area  </li>
                        <li>No Profit Sharing</li>
                        <li>No monthly fees or additional charges</li>
                        <li>Pre-Installed & Self-Hosted FE Pages</li>
                        <li>Newbie Friendly Video Training</li>
                        <li>Exclusive Bonuses</li>
                        <li class="text-start">Media Library to Manage Your Doc, PDF and videos</li>
                        <li class="text-start">1 Million+ Stock Images to Boost your Sales and Profits</li>
                        <li>FAST servers</li>
                        <li class="text-start">100% Unique & Latest Content on The Topic</li>
                        <li>In-Built SEO Settings</li>
                        <li>Elegant and eye-pleasing website</li>
                        <li class="text-start">Autoresponder, Webinar and CRM Integration</li>
                        <li>One time fees</li>
                        <li class="text-start">Complete Step-By-Step Video Training & Tutorials Included</li>
                        <li> Premium Support</li>
                        <li>Live Chat - Customer Support</li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="second-row">
                     <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                        <li class="f-md-20 f-16 white-clr">
                        <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="max-height:45px;">
                        <defs>
                           <style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style>
                        </defs>
                        <path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path>
                        <path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path>
                        <path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path>
                        <path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path>
                        <path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path>
                        <path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path>
                        <path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path>
                        <path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path>
                        <path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path>
                        <polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon>
                        <polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon>
                        <polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon>
                        <path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path>
                        <path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path>
                     </svg>
                        </li>
                        <li class="f-md-18 w600"> $19 <br>One Time</li>
                        <li>0% </li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="third-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-20 f-16 w700"><span>Udemy</span></li>
                        <li class="f-md-18 w600">Free Premium <br> Account</li>
                        <li> 30-40% Each Transaction</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="forth-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-20 f-16 w700"><span>Teachable</span></li>
                        <li class="f-md-18 w600">$29/Month</li>
                        <li>5% Each transaction</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="fifth-row">
                     <ul class="f-md-16 f-14">
                        <li class="f-md-20 f-16 w700"><span>Kajabi</span></li>
                        <li class="w600"> $119/Month </li>
                        <li>0%</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>5</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>1000</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="imagine-sec pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-12">
                  <img src="assets/images/imagine-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md80 mb-md0 mb30">
                  <div class="f-18 f-md-20 w400 lh150 black-clr">
                     ... ! A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br> <b>That’s EXACTLY what we’ve designed Coursesify to do for you.</b><br><br>                    So, if you want to build super engaging academy websites packed with HD Video Trainings on Hot Topics at the push of a button, then get viral social traffic automatically and convert it into SALES, all from start to finish, then Coursesify is made for you! 
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="need-marketing">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-32 w700 lh150 black-clr">
                     So, With Coursesify you can create and sell as many courses online as you want and start making money instantly <span class="red-clr">without…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 f-md-20 w500 lh150 black-clr">
                           <ul class="noneed-listing pl0">
                              <li>Sharing your hard-earned profits</li>
                              <li>Paying monthly fees to 3rd party platforms</li>
                              <li>Spending money repetitively on hosting and domains</li>
                              <li>Hiring expensive developers</li>
                              <li>Any coding or technical skills</li>
                              <li>The hassle of getting courses approved on 3rd party platforms</li>
                              <li>Writing long sales letters or making sales videos</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="assets/images/platfrom.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="look-shape">
                     <div class="f-22 f-md-30 w600 lh150 black-clr">
                        Look, it doesn't matter who you are or what you're doing.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 black-clr mt10">
                     If you want to finally start a profitable online business that’s proven to work, make the<br class="d-none d-md-block"> money that you want and live in a way you dream of, Coursesify is for you. 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center px30 px-md0">
                  <div class="but-design">
                     <div class="f-28 f-md-45 w700 lh140 text-center white-clr skew-normal">
                        We're not done yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md20">
                  <div class="f-22 f-md-24 w500 lh150 text-center">
                     When You Grab Your Coursesify Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">
                              Bonus Course Package with Resell Rights
                           </div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Master Resale Rights for the Latest eBooks, Courses & Tools!A Terrific Range of Products to Enjoy for Yourself and Sell to your customers with CourseSify and make huge profits
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/limited-bonus.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Niche Graphics Set</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">Discover how you can make more profits using one little trick.Why would a prospect buy the Courses from you when he could buy them from one of many other people? Well, that's a problem...And the solution is a very simple one: be different. Create a Beautiful & Branded Academy Site with the help of this Niche Graphics Set 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/limited-bonus.webp" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Video Creation Suite to Create Promo Videos and Courses </div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Show Your Customers EXACTLY What You Want Them to See and Sky-Rocket Your Sales Through the Roof! Wow, Every Visitor with Stunning, Top-Notch Multi-Media Streaming Video Presentations and Convert Each of Them into Buying Customers!
                              <br><br>
                              Video Creation Suite goes a step further; you can record yourself via web cam as you explain and demonstrate click-by-click exactly how something works right on your desktop. Then produce an embedded web page ready to publish on your website in one step. Just imagine the effect this will have on your current business or even a new venture whether in boosting sales or for support purposes.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/limited-bonus.webp" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">
                              Brainstorm Domain Generator
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Amazingly EASY Keyword Domain Brainstorming Tool Will Instantly Uncover and Register the Hottest, Most Profitable & Up-To-The-Minute Keyword Laden Domains With the ability to add them instantly to your WHM.
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/limited-bonus.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--Bonus value Section start -->
      <div class="bonus-section-value">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 mx-auto">
                  <img src="assets/images/thats-a-total.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt-md100 mt20">
                  <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
            </div>
         </div>
      </div>
      <!--Bonus value Section ends -->
      <div class="license-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 w500 black-clr  text-center lh140">

                  Also Get Our LIMITED Commercial License* That Will Allow You To <span class="w700">Tap Into A UNIQUE Opportunity </span>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5">
                  <img src="assets/images/commercial-image.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt20 mt-md0">
                  <div class="f-md-20 f-18 w400 lh140">
                     <span class="w600">As we have shown you, there are tons of businesses – yoga gurus, fitness centers, anybody who want to start an online business businesses and much more…</span> that need your services &amp; eager to pay you
                     monthly for your services.<br><br>
                     Build their branded academy to boost their authority and profits by letting them sell their video courses online. They will pay top dollar to you when you deliver top notch services to them. We’ve taken care of everything so that you can deliver those
                     services simply and easily.
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-md-20 f-18 w400 lh140">
                     <span class="w600">Note: </span>This special commercial license is available ONLY during this launch. Take advantage of it now because it will never be offered again.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section -->
      <div class="noneed-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-20 f-md-24 w400 lh150 text-center">
                  Only 3 easy steps are all it takes to start selling courses online... 
                  </div>
                  <div class="f-md-38 f-28 w700 black-clr lh150 text-center mt20 mt-md40">
                  With Coursesify, Say Goodbye to All Your Worries 
                  </div>
                  <div class="noneed-shape mt20 mt-md40">
                     <div class="f-20 f-md-20 w400 lh150">
                        <ul class="noneed-list2 pl0">
                           <li><span class="w700">No need to pay monthly</span> for expensive course creation services</li>
                           <li><span class="w700">No need to worry</span> about sharing profits or transaction fee.</li>
                           <li><span class="w700">No need to lose</span> your traffic & leads with 3rd party marketplaces– EVERYTHING is in your control!</li>
                           <li><span class="w700">No need to spare a thought</span> for a domain or building e-learning sites!</li>
                           <li><span class="w700">No need to worry </span>about lack of technical or design skills</li>
                           <li><span class="w700">No need to worry about</span> complicated & time- consuming processes. It is an easy 3 steps & your courses are ready to get published within minutes.</li>
                           <li class="happy"><span class="w700">And…say YES Now to living a LAPTOP lifestyle</span> that gives you an ability to spend more time with your family and have complete financial FREEDOM.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section End-->
      <!-- Table Section Start -->
      <div class="table-section" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-24 w600 text-center lh150 black-clr">
                     Take Advantage Of Our Limited Time Deal 
                  </div>
                  <div class="f-md-45 f-28 w700 lh150 text-center mt10">
                     Get Coursesify For A Low One-Time- 
                     <br class="d-none d-md-block"> Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 relative">
                           <div class="table-head1">
                              <div>
                                 <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="max-height:80px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                              </div>
                              <div class="w600 f-md-24 f-22 lh120 text-center text-uppercase mt15 mt-md30 white-clr">Premium</div>
                           </div>
                           <div class="">
                              <ul class="table-list1 pl0 f-16 f-md-16 w400 lh150 white-clr">
                                 <li><span class="w600">No. Of Lessons- Unlimited </span></li>
                                 <li><span class="w600">No. Of Academies – 50</span></li>
                                 <li><span class="w600">Done-For-You 400+ HD Video Trainings Packed in 30+ RED HOT Courses</span></li>
                                 <li><span class="w600">List & Sell Your Courses On Your Own Academy Site.</span></li>
                                 <li><span class="w600">Accept Payments With All Top Platforms – PayPal, Jvzoo, ClickBank, Warrior Plus, PayDotCom etc.</span></li>
                                 <li>Create Beautiful Academy Sites With Marketplace, Blog & Members Area In Few Minutes</li>
                                 <li>No Traffic, Leads Or Profit Sharing With Any 3rd Party Marketplaces- Have 100% Control On Your Business.</li>
                                 <li>Ready To Use Affiliate System </li>
                                 <li>Get 30+ DFY Funnels to Start Selling Right Away </li>
                                 <li>Manage Leads With Inbuilt Lead Management System </li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training </li>
                                 <li>100% Newbie Friendly & Fully Cloud Based Software</li>
                                 <li>Live Chat - Customer Support </li>
                                 <li><span class="w600">Commercial License Included </span></li>
                                 <li>Use For Your Clients </li>
                                 <li>Provide High In Demand Services </li>
                                 <li>Make 5K-10K/Month Extra with Commercial License </li>
                                 <li class="headline">BONUSES WORTH $2465 FREE!!!</li>
                                 <li>Limited Time Bonus #1 – Bonus Course Package with Resell Rights</li>
                                 <li>Limited Time Bonus #2 – Niche Graphics Set</li>
                                 <li>Limited Time Bonus #3 – Video Creation Suite to Create Promo Videos and Courses</li>
                                 <li>Limited Time Bonus #4 – Brainstorm Domain Generator</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6"><img src="https://warriorplus.com/o2/btn/fn100011001/rbhnkl/p7ss9l/329387" class="img-fluid d-block mx-auto" border="0"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Table Section End -->
      <!-- Guarantee Section Start -->
      <div class="guarantee-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w500 lh150 white-clr text-center">
                  We’re Backing Everything Up with An Iron Clad...
                  <span class="w700 orange-clr f-md-45 f-28">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
               <div class="col-md-8 col-12 order-md-2 offset-md-0">
                  <div class="f-md-20 f-18 lh150 w400 white-clr text-xs-center">
                     I'm 100% confident that Coursesify will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.<br><br> If you concluded that, HONESTLY nothing of this
                     has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!<br><br>
                     <span class="">Note:</span> For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!<br><br> I am considering your money to be kept safe on the table between us and
                     waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-4 order-md-1 col-12 mt20 mt-md0">
                  <img src="assets/images/mbg.png" class="img-fluid d-block mx-auto wh300">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Awesome Section Start -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally <span class="w600">help you build beautiful academy site with RED HOT courses ready-to-sell - without wasting tons of money! 
                     </span> <br><br> <span class="w600">So, take action now... and I promise you won't be disappointed! </span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
               <div class="col-12 w600 f-md-28 f-22 text-start mt20 mt-md100">To your success</div>
               <div class="col-12  col-md-8 mx-auto mt20 mt-md30">
                  <div class="row align-items-md-baseline">
                     <div class="col-md-6 col-12 text-center">
                        <img src="assets/images/ayush-jain.png" class="img-fluid d-block mx-auto">
                        <div class="f-22 w700 lh150 text-center black-clr mt15">
                           Ayush Jain
                        </div>
                        <div class="f-15 lh150 w500 text-center black-clr">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/pranshu-gupta.png" class="img-fluid d-block mx-auto">
                        <div class="f-22 w700 lh150 text-center black-clr mt15">
                           Pranshu Gupta
                        </div>
                        <div class="f-15 lh150 w500 text-center black-clr">
                           (Internet Marketer &amp; Product Creator)
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">P.S- You can try "Coursesify" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an
                     absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.
                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping academy sites packed with RED HOT courses ready-to-sell and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles. 
                     <br><br>
                     <span class="w600">P.S.S Don't Procrastinate - Get your copy of Coursesify! </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to
                     miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Awesome Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  Frequently Asked Questions
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I need to download or install Coursesify somewhere?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! You just create an account online and you can get started immediately. Coursesify is 100% web-based platform hosted on the cloud.<br>
                              This means you never have to download anything ever. And It works across all browsers and all devices including Windows and Mac.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you tried it before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its 100% optional & not mandatory for working with Coursesify. Those are recommended if you want to multiply your benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support for my questions?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member’s area. You will also get live chat - customer support so you never get stuck or have any issues.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Coursesify compliant with all guidelines & compliances?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party services which is not a part of Coursesify while choosing it for your business.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What is the duration of service with this Coursesify launch special deal?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary renewal to our founder members for buying from us early.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How is Coursesify is different from other available tools in the market?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, we have a nice comparison chart with other service providers. We won’t like to boast much about our software, but we can assure you that this is a cutting edge technology that will enable you to create and sell courses on your pro academy site at such a low introductory price.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Coursesify Windows and Mac compatible?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We’ve already stated that Coursesify is fully cloud-based. So, it runs directly on the web and works across all browsers and all devices.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/rbhnkl/p7ss9l/wyfhw6">Get Instant Access To Coursesify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer></script><div class="wplus_spdisclaimer"></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © Coursesify 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://coursesify.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>
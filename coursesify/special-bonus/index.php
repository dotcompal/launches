<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Coursesify Special Bonuses">
    <meta name="description" content="Coursesify Special Bonuses">
    <meta name="keywords" content="Coursesify Special Bonuses">
    <meta property="og:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Ayush Jain">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Coursesify Special Bonuses">
    <meta property="og:description" content="Coursesify Special Bonuses">
    <meta property="og:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Coursesify Special Bonuses">
    <meta property="twitter:description" content="Coursesify Special Bonuses">
    <meta property="twitter:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">


<title>Coursesify  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


  <!-- New Timer  Start-->
  <?php
	 $date = 'Dec 01 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/cs71kk/0';
	 $_GET['name'] = 'Ayush Jain';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="yellow-clr w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-16 f-md-18 w700 lh160 white-clr">
						Grab My 20 Exclusive Bonuses Before the Deal Ends…
					</div>
                </div>

				<div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
						<div class="f-md-40 f-28 w500 text-center white-clr lh160">					
						Revealing A Brand New Academy Site Builder That <span class="under yellow-clr w800">Makes $525/ Day Over & Over Again with Advance Course Selling System</span>  In Next 60 Seconds
						</div>
				</div>
				<div class="col-md-12 col-12 text-center mt20 mt-md30">
					<div class="f-18 f-md-20 w500 text-center lh160 white-clr">Watch My Quick Review Video</div>
				</div>
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block">
                       
					   <!-- <div class="responsive-video">
						   <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
					   </div> -->
                       
                    </div>

            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
				<div class="f-16 f-md-16 lh160 w400">
				<ul class="kaptick pl0">
								<li><span class="w700">Create Beautiful Mobile Responsive Academy Sites</span> With Marketplace, Blog & Members Area within Minutes </li>
						<li><span class="w700">Choose From 400+ HD Video Trainings in 30+ HOT Topics</span> to Start Selling & Making Profit Instantly</li>
						<li><span class="w700">Create Your Own Courses in ANY Niche –</span> Add Unlimited Lessons, Videos and E-books </li>
						<li>Made For Newbies & Experienced Marketers- <span class="w700">Even Camera-Shy People Can Start Selling Courses Right Away</span></li>
						<li><span class="w700">Accept Payments Directly in Your Account</span> With PayPal, Stripe, JVzoo, ClickBank, Warrior Plus Etc </li>
						<li>No Traffic, Leads, or Profit Sharing With Any 3rd Party Platform<span class="w700"> - Keep 100% Profits & Control On Your Business </span></li>
						<li><span class="w700">Get Started Immediately -</span> Launch Your First Academy Site with Ready-To-Sell Courses In NO Time</li>
						<li><span class="w700">Have Unlimited Earning Potential –</span>Teach Anything or Everything Quick & Easy </li>
                        <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online… </li>
                        
                                </ul>
				</div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="f-16 f-md-16 lh160 w400">
							<ul class="kaptick pl0">
								<li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses </li>
                        <li><span class="w700">In-Built Ready To Use Affiliate System</span> to Boost your Sales & Profits</li>
                        <li><span class="w700">100% SEO & Mobile Responsive</span> Academy Site & Marketplace </li>
                        <li>Easy And Intuitive To Use Software With<span class="w700">Step By Step Video Training</span></li>
						<li><span class="w700">Rating & Review</span> to Engage & Grow users Via passive leads </li>
                        <li><span class="w700"> Tangential Benefit to Help Agency simultaneously </span>sell video courses & products on site. </li>
                        <li>Progress Bar for each <span class="w700"> Successful course completion </span>  </li>
                        <li><span class="w700"> 30+ Fully Editable </span>Done for you Certificates</li>
                        <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY </li>
                                </ul>
                            </div>
                        </div>
            </div>
            <div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 lite-black-clr">"AYUSHONLY"</span> with <span class="w800 lite-black-clr">$5 discount</span> </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
	  <!-- Step Section End -->
	  <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 white-clr text-center ">					
               Start Your Own Profitable E-Learning Business in Just 3 Simple Steps… 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 1
                     </div>
                     <img src="assets/images/upload-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Create/Choose Course 
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 2
                     </div>
                     <img src="assets/images/customize-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Add Payment Options
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 3
                     </div>
                     <img src="assets/images/publish-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                         Publish & Profit 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  See The Sales They Got with A Simple<br class="d-none d-md-block"> Academy Site Created Recently
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                   And, They're Making an Average $525 In Profits<br class="d-none d-md-block"> Each & Every Day
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
    
	<!---proof section end---->
	  <!-- Video Testimonial -->
	  <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 text-center black-clr"> 
                  Even Beta Users Are Getting EXCITING Results by Selling Done-For-You Courses
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-1.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-2.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-4.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
<!-- Video Testimonial -->
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"AYUSHONLY"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	<div class="leraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <img src="assets/images/e-learning-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-30 f-24 lh140 w600 text-center black-clr mt20 mt-md50">And it is Absolutely Booming Post this Covid-19 Pandemic… 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w500 lh140 text-center">
                  The Global Pandemic has shut doors for many Schools, Universities, and Training Institutes. But in this Period, E-Learning has witnessed a positive trend and opened hundreds of Windows to many E-learning platforms… 
                  </div>
                  <div class="mt20 mt-md50">
                     <img src="assets/images/proof3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 text-center mt20 mt-md30">              
                  And this tremendous growth in student enrolments, shows a HUGE Opportunity! 
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-32 w600 lh140 text-center black-clr">Yes, It's Very High In-Demand! </div>
               </div>
               <div class="col-12 mt20 f-18 f-md-20 w500 lh140 text-center">
                  And there is no limit in sight! – You can create &amp; sell courses on any topic and in any niche like thousands of others are selling on sites like Udemy<sup>TM</sup>, Skillshare<sup>TM</sup>, Udacity<sup>TM</sup>, Coursera<sup>TM</sup>, and More! 
               </div>
            </div>
         </div>
      </div>

   
	 <!-- Stil No Compettion Section Start -->
	 <div class="still-there-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-42 w700 lh140 text-center  black-clr">
                     Look At These People Generating Six and Seven Figures Just By Selling Simple Courses Online 
                  </div>
               </div>
               <div class="row mt-md70 mt20 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/smith.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Len Smith is making <span class="w700">$90K every month </span>- selling his Copywriting Secrets Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12 order-md-2">
                     <img src="assets/images/john-omar.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0 order-md-1">
                     <div class="col-md-6 col-8 ms-md-0 mx-auto line-5" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        John Omar and Eliot Arntz made <span class="w700">$700K in a month</span> by Selling their iOS App Development Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/rob-percival.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Rob Percival makes <span class="w700">$150K every month</span> - selling the same programming course to new students
                     </div>
                  </div>
               </div>
               <div class="row mt30 mt-md65">
                  <div class="f-md-24 f-20 w400 lh160 black-clr  text-center">
                     <span class="w700 f-md-36 f-22 w400 lh140"> But You're Probably Wondering?</span><br> “Can I Make A Full-Time Income Selling My Knowledge?”
                  </div>
               </div>
            </div>
         </div>
      </div>
            <!-- Stil No Compettion Section End -->
      <!-- Suport Section Start -->
	  <div class="eleraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-20 f-md-28 w500 lh160 text-center">
                  <span class="w700 f-24 f-md-36"> Of Course, You Can!</span><br>  See How Newbies Made Millions from E-Learning Business During Lockdown 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
			    <div class="col-12 col-md-6 mt20 mt-md0 p-md-0">
                  <img src="assets/images/proofimg1.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                  <img src="assets/images/proofimg2.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg3.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg4.png" class="img-fluid d-block mx-auto">
               </div>
               </div>
               </div>
               <div class="col-12 w600 f-md-28 w500 lh140 text-center mt20 mt-md30">
               So, It is the perfect time to get in and profit from this exponential growth! 
               </div>
            </div>
            <div class="row mt30 mt-md70">
               <div class="col-12 text-center">
                  <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                     <div class="f-28 f-md-45 w800 lh140 text-center white-clr">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12  f-20 f-md-24 lh150 w600 text-center black-clr lh150">
                  It can be stated that…<br>
                  <span class="mt10 d-block">
                  E-Learning and Online Course Selling is the Biggest Income Opportunity Right Now! 
                  </span>
               </div>
               <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh150 mt20 mt-md30">
                  So, How would you like to Get Your Share?
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Suport Section End -->
  
	  <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
                </div>
                <div class="col-12 text-center mt70 mt-md70">
                    <div class="prdly-pres f-24 w700 text-center white-clr">
                        Proudly Introducing…
                    </div>
                </div>
                <div class="col-12 mt-md50 mt20 text-center">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="max-height:120px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                </div>
                <div class="f-md-30 f-22 w700 yellow-clr lh150 col-12 mt-md30 mt20 text-center">
					
					1-Click Academy Builder Software That Creates and <br class="d-none d-md-block">  Sell Courses Online with Zero Tech Hassles and  Makes<br class="d-none d-md-block">  $525/Day Over and Over Again 
                </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                <div class="col-12 col-md-10 mx-auto">
                    <img src="assets/images/product-image.png" class="img-fluid d-block mx-auto margin-bottom-15">
                </div>
                <!--<div class="col-md-5 col-12 mt20 mt-md0">
                    <div class="proudly-list-bg">
                        <ul class="proudly-tick pl0 m0 f-16 w500 black-clr lh150 text-capitalize">
                            <li><span class="w700">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                            <li><span class="w700">Create Beautiful and Proven Converting</span> E-Learning Sites 
                            </li>
                            <li><span class="w700">Preloaded with 700 HD Video Training and 40 DFY Sales Funnels </span>to Start Selling Instantly and Keep 100% of Profit</li>
                            <li><span class="w700">Don't Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                            <li><span class="w700">Commercial License Included</span> </li>
                            <li><span class="w700">50+ More Features</span></li>
                        </ul>

                    </div>
                </div>-->
            </div>


        </div>
    </div>
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Coursesify, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Affiliate Commissions Smart Links V2.0
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Easily create "Recommends" Style Affiliate Links Plus Track The Performance Of Each Link With The Link Tracking Software Included With SmartLinks V2.0.</li>
						<li>It tremendously reduces the possibility of prospects "Hijacking & bypassing" your affiliate link. (That equals more money in your pocket!)</li>
						<li>The "Recommends" link looks professional and gives your prospects the impression that you are just recommending a product, therefore, they will not hesitate to look into your recommendation!</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  WP Instant Décor
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Instantly Increase Your Conversion Rate By Decorating Your WP Blog With This Incredibly Useful Plugin! </li> 
						<li>With Just A Few Clicks Of Your Mouse You Can Decorate Your Blog And Finally Get The Results You Deserve!</li>
						<li>If your niche blogs are simple, plain, and boring… people are going to run for the hills QUICK. There's only ONE single thing to put into your hand so that you can stop this nonsense right now.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  eWriterPro
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Everything You Need to Create Beautiful Professional Quality eBooks at the Touch of a Button. </li>
						<li>If you are looking for a way to ethically make money on the internet, then the creation of information products is by far the easiest and most rewarding way! </li>
						<li>Now, stop thinking and build your website with this software and use content curation with Coursesify to boost rankings like you always wanted.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Feedback Analyzer Pro Version 2.0 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  If you want to research eBay sellers before you buy or create attractive income generating feedback ads then Feedback Analyzer Pro can help you.<br><br>
					  <b>With this software you can...</b><br><br>
					  	<li>Use the NEW ad creator tool to create highly attractive feedback ads you can insert into your ebay auctions...</li>
						<li>Instantly separate the positive, neutral and negative comments into separate easy-to-read tables for any ebay member...</li>
						<li>Extract feedback comments from 4 different eBay sites: ebay.com, ebay.com.au, ebay.ca, ebay.co.uk</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Payment EzyCash
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  The Hassle-Free way of putting Cash in your Pocket :
					  	<li>- can be easily integrated into any website.</li>
						<li>- is the most hassle-free and cost-effective way of doing it</li>
						<li>- avoid manual hassle of receiving your commission / charges</li>
						<li>- just send an email to your JV partner or your client with your one-stop payment url</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"AYUSHONLY"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Traffic Hurricane Pro V2.0
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>What Would You Say, If We Could Show You A Way To Literally Drive Thousands of Laser Targeted Visitors to Your Website Everyday - At No Cost To You - And Still Make A Boat Load Of Money Even If Not One Of Those Visitors Actually Bought Your Product!</li>
						<li> Well up until now, this top traffic generation technology has been something that ONLY the top money earners on the internet knew about! </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Easy Web Visitor Counter
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Here's A Quick And Easy Way To Discover Exactly How Many People Are Visiting Each Of Your Web Pages</li>
						<li>Easy Web Visitor Counter is an easy to use tool for tracking the number of visitors to a web page or set of web pages.</li>
						<li>Simply upload the Easy Web Visitor Counter script to your web host and add a small amount of code to the web pages.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Content Spin Bot
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Revolutionary New Software Creates Unique Articles For Better Search Engine Rankings, More Vistors And Ultimately Make Money Online!</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">					 
							  Affiliate Video Brander 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Now YOU can use our ridiculously easy video tool to make your affiliate commissions explode through the roof and here's the most ingenious part - you DON'T even have to make your own videos!</li>
								<li>Affiliate Marketers Have Been Kept Out-of-the-Loop And Prevented From Prospering With Online Video! Up to this point, video has been for the makers of the videos who control them and show them to put money into their own pockets only. But with Affiliate Video Brander, huge video powered commissions await you and you DON'T even have to create your own videos or mess with difficult Flash files or HTML programs. All you have to do is click a few buttons and our software does the rest!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Membership Site Manager
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Now you can generate recurring income month after month with a membership site - without paying thousands for a management script! It's All Possible And Inexpensive With A Hot New Script Called Membership Site Manager.</li>
								<li>Just think! You'll be up and running with your own membership site on the Internet so fast it will make your head spin watching all those recurring funds come in!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

<!-- CTA Button Section Start -->
<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"AYUSHONLY"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

<!-- Bonus #11 start -->
<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus11.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  CB Niche Builder 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  Learn how YOU can quickly and easily discover high profit affiliate products in the ClickBank.com Marketplace and build instant niche sites, ready to make money for you as soon as possible!<br><br>
							  <b>With user-friendly CB Niche Builder software tool, you can:</b>
							  	<li>Instantly find high profit niches by gaining world view access to the ClickBank.com marketplace.</li>
								<li>Rank your niche sites high in major Search Engines results for keywords that sell! Leverage on Search Engine Optimization.</li>
								<li>Prevention from having some of your rightful affiliate commissions stolen using my unique Affiliate Cloaking Website Generator!</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus12.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Affiliate Link Defender
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
 								<li>Redirect your cloaked links to any site of your choice so that others won't even notice it's an affiliate link and you'll still be credited for your commissions!</li>
								<li>Bypass merchants' squeeze pages and send your prospects directly to their sales pages or even your order page, which eliminates a step and increases your chances of making a sale</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus13.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Redirection Rocket 2.0 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Turn Those Long Nasty Affiliate Links Into Powerful, Search Engine Friendly Tracking Links Guaranteed To Increase Your Clickthroughs Up To 300%! In Less Than 10 Minutes You Could Have The Redirection Rocket 2.0 Script On Your Website & Less Than 10 Seconds Later Your Links Will Look Like The Pros!</li>
								<li>Using Redirection Rocket 2.0 you can attach a flyover optin code from getresponse or aweber to any campaign, and when someone goes to the link you are referring them to your popover shows on the other persons page! AND the cookies for tracking are STILL set. This is a powerful listbuilding tool.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus14.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   WP Stealth Links
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Instantly Make More Money Using The wpStealth Plugin To Mask Your Affiliate Links And Landing Pages With Point And Click Ease!</li>
								<li>wpStealth Easily Cloaks Your Affiliate Links And Landing Pages!</li>
								<li>This will keep "commission thieves" from stealing YOUR money.</li>
								<li>It will also build links back to YOUR site, instead of bit.ly or others.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus15.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Analyze Buzz 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Analyze Competition In Just Seconds! Here's What This Keyword Analysis Tool</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->



	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"AYUSHONLY"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus16.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  PayPal Cart for WP 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>"PayPal Cart for WP" turns any wordpress blog into a paypal profits machine.</b>
								It creates a simple, easy to install and use, <b>powerful, effective e-commerce solution! </b><br><br>
								<b>Features:</b>
							  	<li>Generates Paypal 'Buy Now' & 'Donate' buttons on the fly!</li>
								<li>Can create a complete, fully customizable Paypal shopping cart in minutes!</li>
								<li>Turns their blog into a automated Paypal profits machine!</li>		
								<li>Is incredibly simple to set up and use – no technical knowledge is necessary!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->


	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus17.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Sales Motivator Pro 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Now You Can Easily Motivate Your Website Visitors To Buy Your Products!</b>
							  	<li>Every online business owners and affiliate marketers want to make out of the traffic that they generated to their website. This is the fact that gathering traffic to your website is sometimes time-consuming and other issues out there.</li>
								<li>The thing is that you want to convert those visitors into buyers and in order to do that you must also create some strategies on how you are going to approach them or attract their eyes to click on the buy now button of your product pages.</li>
								<li>In most cases, the most popular online retails store called Amazon.com has already applied this technique and if you want this to apply into your website, you should also have this amazing feature.</li>
			   				</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus18.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  WP Profit Page Creator 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Discover a Brand New and Highly Profitable WP Plugin that Once You Fire it Up, Will Generate You Endless Sales!</b><br><br>
							  	<li>If you are an online entrepreneur and you want to make so much profits out from your internet business, this amazing WordPress plugin is a huge help to you.</li>
								<li>What this software does is that, it will create SEO friendly money making WordPress pages almost every with a little content.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus19.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Smart Agent Pro 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Skyrocket Your Conversions, Sales And Profits With Your Own Automated Sales Assistants, Providing 24/7 Support On All Your Websites!</li>
								<li>You might already heard the saying that to become successful, you need to study what successful people do and model their actions!</li>
								<li>Well if you've been following the Internet Marketing scene lately you'll have noticed the growing popularity of those little 'exit chat agent' scripts. All the big name 'guru's' seem to be using them on all their sites.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->


	<!-- Bonus #20 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus20.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Auto Support Bot
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Your Visitors 24/7 Live Chat Support on Your Websites, Even While You're Sleeping</b><br><br>
							 	<li>Most Software iPacks activate your web browser to show a popup when you use the software. The popups help to fund the development cost of the individual Software iPacks, which means that you can usually buy Software iPacks at lower cost than normal software.</li>
								<li>Only a single popup will appear each time you use the software. The web addresses shown are embedded in the .sip file - so the creators of the Software iPack Player have no control of the websites that are displayed in the popups.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That's Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-12 text-center">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	
	  <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
					<svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					<div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script>
					<div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
				</div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © Coursesify 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.webp" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop ">	
					<div class="col-12 ">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 ">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Coursesify Special Bonuses">
    <meta name="description" content="Coursesify Special Bonuses">
    <meta name="keywords" content="Coursesify Special Bonuses">
    <meta property="og:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Atul">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Coursesify Special Bonuses">
    <meta property="og:description" content="Coursesify Special Bonuses">
    <meta property="og:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Coursesify Special Bonuses">
    <meta property="twitter:description" content="Coursesify Special Bonuses">
    <meta property="twitter:image" content="https://www.coursesify.com/special-bonus/thumbnail.png">


<title>Coursesify  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'Dec 01 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/p3wpzx/0';
	 $_GET['name'] = 'Atul';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row"> 
				
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr mt-md50 mt 20">
						<span class="w600 yellow-clr"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-16 f-md-18 w700 lh160 white-clr">
						Grab My 20 Exclusive Bonuses Before the Deal Ends…
					</div>
                </div>

				<div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
				<div class="f-md-40 f-28 w500 text-center white-clr lh160">					
						Revealing A Brand New Academy Site Builder That <span class="under yellow-clr w800">Makes $525/ Day Over & Over Again with Advance Course Selling System</span>  In Next 60 Seconds
						</div>
				</div>
				<div class="col-md-12 col-12 text-center mt20 mt-md30">
					<div class="f-18 f-md-20 w500 text-center lh160 white-clr">Watch My Quick Review Video</div>
				</div>
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block">
                       
						<!-- <div class="responsive-video">
                            <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> -->
                       
                    </div>

            </div>
        </div>
    </div>
	
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
                            <div class="f-16 f-md-16 lh160 w400">
                                <ul class="kaptick pl0">
								<li><span class="w700">Create Beautiful Mobile Responsive Academy Sites</span> With Marketplace, Blog & Members Area within Minutes </li>
						<li><span class="w700">Choose From 400+ HD Video Trainings in 30+ HOT Topics</span> to Start Selling & Making Profit Instantly</li>
						<li><span class="w700">Create Your Own Courses in ANY Niche –</span> Add Unlimited Lessons, Videos and E-books </li>
						<li>Made For Newbies & Experienced Marketers- <span class="w700">Even Camera-Shy People Can Start Selling Courses Right Away</span></li>
						<li><span class="w700">Accept Payments Directly in Your Account</span> With PayPal, Stripe, JVzoo, ClickBank, Warrior Plus Etc </li>
						<li>No Traffic, Leads, or Profit Sharing With Any 3rd Party Platform<span class="w700"> - Keep 100% Profits & Control On Your Business </span></li>
						<li><span class="w700">Get Started Immediately -</span> Launch Your First Academy Site with Ready-To-Sell Courses In NO Time</li>
						<li><span class="w700">Have Unlimited Earning Potential –</span>Teach Anything or Everything Quick & Easy </li>
                        <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online… </li>
                        
                                </ul>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="f-16 f-md-16 lh160 w400">
                                <ul class="kaptick pl0">
								<li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses </li>
                        <li><span class="w700">In-Built Ready To Use Affiliate System</span> to Boost your Sales & Profits</li>
                        <li><span class="w700">100% SEO & Mobile Responsive</span> Academy Site & Marketplace </li>
                        <li>Easy And Intuitive To Use Software With<span class="w700">Step By Step Video Training</span></li>
						<li><span class="w700">Rating & Review</span> to Engage & Grow users Via passive leads </li>
                        <li><span class="w700"> Tangential Benefit to Help Agency simultaneously </span>sell video courses & products on site. </li>
                        <li>Progress Bar for each <span class="w700"> Successful course completion </span>  </li>
                        <li><span class="w700"> 30+ Fully Editable </span>Done for you Certificates</li>
                        <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY </li>
                                </ul>
                            </div>
                        </div>
            </div>
            <div class="row mt20 mt-md70">
            <!-- CTA Btn Section Start -->
            <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 lite-black-clr">"ATULVIP5"</span> with <span class="w800 lite-black-clr">$5 discount</span> </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
  <!-- Step Section End -->
  <!-- Step Section End -->
  <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 white-clr text-center ">					
               Start Your Own Profitable E-Learning Business in Just 3 Simple Steps… 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 1
                     </div>
                     <img src="assets/images/upload-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Create/Choose Course 
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 2
                     </div>
                     <img src="assets/images/customize-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Add Payment Options
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 3
                     </div>
                     <img src="assets/images/publish-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                         Publish & Profit 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  See The Sales We Got with A Simple<br class="d-none d-md-block"> Academy Site Created Recently
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                   And, We’re Making an Average $525 In Profits<br class="d-none d-md-block"> Each & Every Day
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
    
	<!---proof section end---->
	  <!-- Video Testimonial -->
	  <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 text-center black-clr"> 
                  Even Beta Users Are Getting EXCITING Results by Selling Our Done-For-You Courses
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-1.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-2.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-4.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
<!-- Video Testimonial -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ATULVIP5"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	<div class="leraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <img src="assets/images/e-learning-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-30 f-24 lh140 w600 text-center black-clr mt20 mt-md50">And it is Absolutely Booming Post this Covid-19 Pandemic… 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w500 lh140 text-center">
                  The Global Pandemic has shut doors for many Schools, Universities, and Training Institutes. But in this Period, E-Learning has witnessed a positive trend and opened hundreds of Windows to many E-learning platforms… 
                  </div>
                  <div class="mt20 mt-md50">
                     <img src="assets/images/proof3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 text-center mt20 mt-md30">              
                  And this tremendous growth in student enrolments, shows a HUGE Opportunity! 
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-32 w600 lh140 text-center black-clr">Yes, It’s Very High In-Demand! </div>
               </div>
               <div class="col-12 mt20 f-18 f-md-20 w500 lh140 text-center">
                  And there is no limit in sight! – You can create &amp; sell courses on any topic and in any niche like thousands of others are selling on sites like Udemy<sup>TM</sup>, Skillshare<sup>TM</sup>, Udacity<sup>TM</sup>, Coursera<sup>TM</sup>, and More! 
               </div>
            </div>
         </div>
      </div>

   
	 <!-- Stil No Compettion Section Start -->
	 <div class="still-there-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-42 w700 lh140 text-center  black-clr">
                     Look At These People Generating Six and Seven Figures Just By Selling Simple Courses Online 
                  </div>
               </div>
               <div class="row mt-md70 mt20 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/smith.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Len Smith is making <span class="w700">$90K every month </span>- selling his Copywriting Secrets Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12 order-md-2">
                     <img src="assets/images/john-omar.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0 order-md-1">
                     <div class="col-md-6 col-8 ms-md-0 mx-auto line-5" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        John Omar and Eliot Arntz made <span class="w700">$700K in a month</span> by Selling their iOS App Development Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/rob-percival.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Rob Percival makes <span class="w700">$150K every month</span> - selling the same programming course to new students
                     </div>
                  </div>
               </div>
               <div class="row mt30 mt-md65">
                  <div class="f-md-24 f-20 w400 lh160 black-clr  text-center">
                     <span class="w700 f-md-36 f-22 w400 lh140"> But You’re Probably Wondering?</span><br> “Can I Make A Full-Time Income Selling My Knowledge?”
                  </div>
               </div>
            </div>
         </div>
      </div>
            <!-- Stil No Compettion Section End -->
      <!-- Suport Section Start -->
	  <div class="eleraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-20 f-md-28 w500 lh160 text-center">
                  <span class="w700 f-24 f-md-36"> Of Course, You Can!</span><br>  See How Newbies Made Millions from E-Learning Business During Lockdown 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
			    <div class="col-12 col-md-6 mt20 mt-md0 p-md-0">
                  <img src="assets/images/proofimg1.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                  <img src="assets/images/proofimg2.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg3.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg4.png" class="img-fluid d-block mx-auto">
               </div>
               </div>
               </div>
               <div class="col-12 w600 f-md-28 w500 lh140 text-center mt20 mt-md30">
               So, It is the perfect time to get in and profit from this exponential growth! 
               </div>
            </div>
            <div class="row mt30 mt-md70">
               <div class="col-12 text-center">
                  <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                     <div class="f-28 f-md-45 w800 lh140 text-center white-clr">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12  f-20 f-md-24 lh150 w600 text-center black-clr lh150">
                  It can be stated that…<br>
                  <span class="mt10 d-block">
                  E-Learning and Online Course Selling is the Biggest Income Opportunity Right Now! 
                  </span>
               </div>
               <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh150 mt20 mt-md30">
                  So, How would you like to Get Your Share?
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Suport Section End -->
  
	  <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
                </div>
                <div class="col-12 text-center mt70 mt-md70">
                    <div class="prdly-pres f-24 w700 text-center white-clr">
                        Proudly Introducing...
                    </div>
                </div>
                <div class="col-12 mt-md50 mt20 text-center">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="max-height:120px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                </div>
                <div class="f-md-30 f-22 w700 yellow-clr lh150 col-12 mt-md30 mt20 text-center">
					
					1-Click Academy Builder Software That Creates and <br class="d-none d-md-block">  Sell Courses Online with Zero Tech Hassles and  Makes Us<br class="d-none d-md-block">  $525/Day Over and Over Again 
                </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                <div class="col-12 col-md-10 mx-auto">
                    <img src="assets/images/product-image.png" class="img-fluid d-block mx-auto margin-bottom-15">
                </div>
                <!--<div class="col-md-5 col-12 mt20 mt-md0">
                    <div class="proudly-list-bg">
                        <ul class="proudly-tick pl0 m0 f-16 w500 black-clr lh150 text-capitalize">
                            <li><span class="w700">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                            <li><span class="w700">Create Beautiful and Proven Converting</span> E-Learning Sites 
                            </li>
                            <li><span class="w700">Preloaded with 700 HD Video Training and 40 DFY Sales Funnels </span>to Start Selling Instantly and Keep 100% of Profit</li>
                            <li><span class="w700">Don’t Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                            <li><span class="w700">Commercial License Included</span> </li>
                            <li><span class="w700">50+ More Features</span></li>
                        </ul>

                    </div>
                </div>-->
            </div>


        </div>
    </div>
	<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : NinjaKash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="headernj-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mt-md7">
							<img src="assets/images/njlogo.png" class="img-fluid mx-auto d-block">
						</div>
					</div>
					<div class="col-12 col-md-12 text-center">
						<div class="mt20 mt-md50">
							<div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-nj">
								Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="mt-md30 mt20">
							<div class="f-28 f-md-50 w500 text-center white-clr lh140">
							
							<span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
							</div>
						</div>
						<div class="f-22 f-md-28 w600 text-center orange lh140 mt-md30 mt20">
							
							NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
							
						</div>
					</div>
				   <div class="col-12 col-md-12">
						<div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left mt20 mt-md50">
                     <div class="col-12 responsive-video">
                        <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div> 

                  </div>
							
							<div class="col-md-4 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md40 mt20 min-md-video-width-right">
								<ul class="list-head pl0 m0">
									<li>Kickstart with 50 Hand-Picked Products</li>
									<li>Promote Any Offer in Any niche HANDS FREE</li>
									<li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
									<li>Drive TONS OF Social &amp; Viral traffic</li>
									<li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
									<li>No Monthly Fees…EVER</li>
								</ul>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
      <div class="">
			<div class="container-fluid p0">
				<img src="assets/images/ninjakash-grandb1.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb2.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb3.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
			</div> 
		</div>
   <!-------Exclusive Bonus----------->
<!-- Ninjakash -->


<!-- Trenzy -->
    <!-------Exclusive Bonus----------->
	<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Trenzy.io
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->

<div class="mybanner22 relative">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-12 mx-auto">
			<img src="assets/images/logo1.png" class="img-fluid d-block mx-auto" >
		</div>
	</div>
</div>



<div class="container mycontainer">
	<div class="row">
		<div class="col-md-12 col-12 text-center ">
			<span class="f-18 f-md-22 w700 w700 text-center prehead mt20 mt-md40">Time to get over old boring content and cash into the latest trending topics in 2019</span>
		</div>
	</div>
</div>
<div class="container mycontainer">
	<div class="row">
		<div class="col-md-12 col-12 text-center mt20 mt-md40 white-clr ">
			<div class="f-25 f-md-44 lh140 w400 textshadow text-capitalize">
				<span class="w700">REVEALED:</span> Breakthrough Software That Harnesses <span class="tzcolor w700">The Power of Hot Trending Topics for NON-STOP Buyer Traffic &amp; Monetizes It 24x7 in 3 Simple Clicks</span>
			</div>
		</div>
		<div class="col-md-12 col-12 text-center f-16 f-md-18 lh150 w400 white-clr my30">
			Curate and Publish FREE Trending Content from Top Authority sites for Unlimited Traffic and Steady PASSIVE Income...
		</div>
	</div>
	<div class="row align-items-center">
		<div class="col-md-7 col-12 mt2 xsmt3">	
			<div class="responsive-video">
			<iframe src="https://trenzy.dotcompal.com/video/embed/tacjdigs1g?rel=0&amp;wmode=transparent&amp;autoplay=1&amp;controls=0&amp;showinfo=0" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>  
		</div>
	</div>
	<div class="col-md-5 col-12">
		<div class="f-md-26 f-20 lh150 w600 white-clr">Launch Your Profit Pulling Trending Websites in 3 Simple Steps</div>
			<div class="step-bg10 mt30">
				<img class="img-fluid d-block mx-auto" src="assets/images/tb1.png">
				<div>
					<span class="f-md-22 f-18 w600 lh150 white-clr"><span class="yellow2">Step #1</span> Find and Publish Hot Trending Content in Seconds </span><br>
					<span class="f-16 w400 lh150 white-clr"> Just enter any keyword in any niche and find latest hot trending content
				</div>
			</div>


			<div class="step-bg10 mt30">
				<img class="img-fluid d-block mx-auto" src="assets/images/tb2.png">
				<div>
    				<span class="f-md-22 f-18 w600 lh150 white-clr"><span class="yellow2">Step #2</span> Monetize the Trend with Best Offer </span><br>
					<span class="f-16 w400 lh150 white-clr"> Let AI tool put the best offer and monetize it for you 24x7
				</div>
			</div>

	<div class="step-bg10 mt30">
		<img class="img-fluid d-block mx-auto" src="assets/images/tb3.png">
		<div>
    		<span class="f-md-22 f-18 w600 lh150 white-clr"><span class="yellow2">Step #3</span> Sit Back and Relax While it Works for You 24x7 </span><br>
			<span class="f-16 w400 lh150 white-clr"> Trenzy will keep finding top trending content continously. Just start enjoying more traffic and commissions
		</div>
	</div>
	</div>
</div>	
</div>
</div>	

<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/trenzy.io-ss.png">
            <source media="(min-width:320px)" srcset="assets/images/trenzy.io-ss-mview.png">
            <img src="assets/images/trenzy.io-ss.png" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
<!-- Trenzy.io -->

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : MailZilo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->

	<div class="header-section-mailzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12  text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0z{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0z" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0z" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0z" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0z" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0z" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0z" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0z" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0z" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0z" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-mailzilo f-md-22 f-16 w500 lh140 white-clr">
                  Are you Sick &amp; Tired of Paying 100s of Dollars To Old School Autoresponders That Never Deliver?
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
               Get 4X Email Opens &amp; Traffic Using <span class="yellow-clr">the Powerful Tagging Based Email Marketing Platform &amp; 90,000+ Done-For-You Proven Swipes</span> with No Monthly Fee Ever!
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading-mailzilo f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  100% Newbie Friendly Platform to Collect &amp; Manage Leads | Send Unlimited Emails <br class="d-none d-md-block">to Get Tons of Traffic, Affiliate Commissions, &amp; Sales on Autopilot
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                        <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://mailzilo.dotcompal.com/video/embed/caepa779qt" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                        
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="key-features-bg-mailzilo">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li><span class="list-highlight">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="list-highlight">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li><span class="list-highlight">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="list-highlight">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="list-highlight">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="list-highlight">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="list-highlight">100% Control</span> on your online business </li>
                              <li><span class="list-highlight">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="list-highlight">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="list-highlight">100+ High Converting</span> Templates for Webforms &amp; Email </li>
                              <li><span class="list-highlight">GDPR &amp; Can-Spam Compliant</span></li>
                              <li><span class="list-highlight">100% Newbie Friendly</span> &amp; Easy to Use</li>
                              <li><span class="list-highlight">Commercial License Included</span></li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn px0 d-block">Get Instant Access To MailZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/mailzilo-ss.png">
            <source media="(min-width:320px)" srcset="assets/images/mailzilo-mview.png">
            <img src="assets/images/mailzilo-ss.png" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : WebPrimo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-------Exclusive Bonus Ends----------->
      <!--WebPrimo Header Section Start -->
      <div class="WebPrimo-header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto" /></div>
               <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                  <div>
                     The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                  </div>
                  <div class="mt5">
                     <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-md-45 f-28 w600 text-center white-clr lh150">
                     First Ever on JVZoo…  <br class="d-none d-md-block">
                     <span class="w700 f-md-40 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites & Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
                  Easily create & sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches & 100+ other niches...
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                        <div class="col-12 responsive-video">
                           <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="WebPrimo-key-features-bg">
                           <ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
                              <li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
                              <li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
                              <li>Create Elegant, Fast loading & SEO Friendly Website within 7 Minutes</li>
                              <li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
                              <li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
                              <li>Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="WebPrimo-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row webprimo-header-list-block">
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li><span class="w600">Create Ultra-Fast Loading & Beautiful Websites</li>
                              <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w600">For Any Business Need.</span>
                              </li>
                              <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
                              </li>
                              <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
                              <li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
                              <li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
                              <li><span class="w600">Analytics & Remarketing Ready</span> Websites</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                              <li>Customize and Update Themes on Live Website Easily</li>
                              <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                              <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                              <li><span class="w600">Accept Payments</span> For Your Services & Products With Seamless WooCommerce Integration</li>
                              <li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12  header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--WebPrimo Header Section End -->
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!--WebPrimo ends-->


<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #5 : Entertainerz
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-------Exclusive Bonus Ends----------->
	  <div class="header-section-enter">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-12 text-center ">
                        <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:80px">
                           <defs>
                              <style>.cls-1a{fill:url(#linear-gradient-2);}.cls-2b{fill:#fff;}.cls-3c{fill:#001730;}.cls-4d{fill:url(#linear-gradient-3);}.cls-5e{fill:url(#linear-gradient);}</style>
                              <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#bf003b"></stop>
                                 <stop offset="1" stop-color="#e23940"></stop>
                              </linearGradient>
                              <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#f38227"></stop>
                                 <stop offset="1" stop-color="#fbb826"></stop>
                              </linearGradient>
                              <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#3369e9"></stop>
                                 <stop offset="1" stop-color="#002b7f"></stop>
                              </linearGradient>
                           </defs>
                           <path class="cls-2b" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"></path>
                           <path class="cls-2b" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"></path>
                           <g>
                              <path class="cls-2b" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"></path>
                              <path class="cls-2b" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                              <path class="cls-2b" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                              <path class="cls-2b" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                              <path class="cls-2b" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                              <path class="cls-2b" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                              <path class="cls-2b" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"></path>
                              <path class="cls-2b" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"></path>
                              <path class="cls-2b" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                              <path class="cls-2b" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                              <path class="cls-2b" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                              <path class="cls-2b" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"></path>
                           </g>
                           <path class="cls-2b" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"></path>
                           <path class="cls-5e" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"></path>
                           <path class="cls-1a" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"></path>
                           <path class="cls-4d" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"></path>
                           <path class="cls-2b" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"></path>
                           <path class="cls-3c" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="f-md-20 f-18 w600 lh150 white-clr">
                  <span class="orange-clr">Legally Exploit a Little-Known Loophole</span> to Instantly Get Access to <br class="d-none d-md-block"> 800 million+ YouTube Videos &amp; Entertainment News…
                  </div>
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/preheadline.webp" class="img-fluid d-block mx-auto">
               </div>
               <!-- <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               3-Click SIMPLE Software <span class="orange-clr w800">Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment Sites</span> in Just 60 Seconds…
               </div> -->
               <!-- <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               SUPER SIMPLE Software <span class="orange-clr w800">Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment Sites</span> in Just 7 Minutes…
               </div> -->
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               SUPER SIMPLE Software <span class="orange-clr w800"> Makes Us $528/Day Creating AUTO-Updating &amp; Traffic-Pulling Entertainment News Sites </span> in Just 60 Seconds…
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 white-clr">
               No Content Creation. No Camera. No Tech Hassles Ever... 100% Beginner Friendly!
               </div>
            </div>
            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/proudly.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                  <iframe src="https://entertainerz.dotcompal.com/video/embed/u1o4dj3t3c" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                 
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w500 white-clr">
                     <li>Legally Use from 800 million+ Trending Content Created by Others</li>
                     <li>Set And Forget System with Single Keyword</li>
                     <li>AI-Powered Software Puts Most Profitable Links on Your Websites</li>
                     <li>Built-In Automated Traffic Generating System</li>
                     <li>Complete Social Media Automation</li>
                     <li>100% SEO Friendly Website And Built-In Remarketing System</li>
                     <li>Automatically Translate Your Sites In 15+ Language According For More Traffic</li>
                     <li>Integration With Major Autoresponders And Social Media Apps</li>
                     <li>In-Built Content Spinner To Make Your Content Fresh</li>
                     <li>Make 5K-10K With Commercial License</li>
                     <li>A-Z Complete Video Training Is Included</li>
                     <li>Limited-Time Special Bonuses Worth $2285 If You Buy Today</li>
                     </ul>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
	  <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/entertainerz-ss.webp">
            <source media="(min-width:320px)" srcset="assets/images/entertainerz-ss-mview.webp">
            <img src="assets/images/entertainerz-ss.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Coursesify, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	
	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Affiliate Commissions Smart Links V2.0
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Easily create "Recommends" Style Affiliate Links Plus Track The Performance Of Each Link With The Link Tracking Software Included With SmartLinks V2.0.</li>
						<li>It tremendously reduces the possibility of prospects "Hijacking & bypassing" your affiliate link. (That equals more money in your pocket!)</li>
						<li>The "Recommends" link looks professional and gives your prospects the impression that you are just recommending a product, therefore, they will not hesitate to look into your recommendation!</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  WP Instant Décor
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Instantly Increase Your Conversion Rate By Decorating Your WP Blog With This Incredibly Useful Plugin! </li> 
						<li>With Just A Few Clicks Of Your Mouse You Can Decorate Your Blog And Finally Get The Results You Deserve!</li>
						<li>If your niche blogs are simple, plain, and boring… people are going to run for the hills QUICK. There's only ONE single thing to put into your hand so that you can stop this nonsense right now.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  eWriterPro
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Everything You Need to Create Beautiful Professional Quality eBooks at the Touch of a Button. </li>
						<li>If you are looking for a way to ethically make money on the internet, then the creation of information products is by far the easiest and most rewarding way! </li>
						<li>Now, stop thinking and build your website with this software and use content curation with Coursesify to boost rankings like you always wanted.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Feedback Analyzer Pro Version 2.0 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  If you want to research eBay sellers before you buy or create attractive income generating feedback ads then Feedback Analyzer Pro can help you.<br><br>
					  <b>With this software you can...</b><br><br>
					  	<li>Use the NEW ad creator tool to create highly attractive feedback ads you can insert into your ebay auctions...</li>
						<li>Instantly separate the positive, neutral and negative comments into separate easy-to-read tables for any ebay member...</li>
						<li>Extract feedback comments from 4 different eBay sites: ebay.com, ebay.com.au, ebay.ca, ebay.co.uk</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Payment EzyCash
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  The Hassle-Free way of putting Cash in your Pocket :
					  	<li>- can be easily integrated into any website.</li>
						<li>- is the most hassle-free and cost-effective way of doing it</li>
						<li>- avoid manual hassle of receiving your commission / charges</li>
						<li>- just send an email to your JV partner or your client with your one-stop payment url</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ATULVIP5"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Traffic Hurricane Pro V2.0
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>What Would You Say, If We Could Show You A Way To Literally Drive Thousands of Laser Targeted Visitors to Your Website Everyday - At No Cost To You - And Still Make A Boat Load Of Money Even If Not One Of Those Visitors Actually Bought Your Product!</li>
						<li> Well up until now, this top traffic generation technology has been something that ONLY the top money earners on the internet knew about! </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Easy Web Visitor Counter
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Here's A Quick And Easy Way To Discover Exactly How Many People Are Visiting Each Of Your Web Pages</li>
						<li>Easy Web Visitor Counter is an easy to use tool for tracking the number of visitors to a web page or set of web pages.</li>
						<li>Simply upload the Easy Web Visitor Counter script to your web host and add a small amount of code to the web pages.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  Content Spin Bot
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Revolutionary New Software Creates Unique Articles For Better Search Engine Rankings, More Vistors And Ultimately Make Money Online!</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">					 
							  Affiliate Video Brander 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Now YOU can use our ridiculously easy video tool to make your affiliate commissions explode through the roof and here's the most ingenious part - you DON'T even have to make your own videos!</li>
								<li>Affiliate Marketers Have Been Kept Out-of-the-Loop And Prevented From Prospering With Online Video! Up to this point, video has been for the makers of the videos who control them and show them to put money into their own pockets only. But with Affiliate Video Brander, huge video powered commissions await you and you DON'T even have to create your own videos or mess with difficult Flash files or HTML programs. All you have to do is click a few buttons and our software does the rest!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Membership Site Manager
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Now you can generate recurring income month after month with a membership site - without paying thousands for a management script! It's All Possible And Inexpensive With A Hot New Script Called Membership Site Manager.</li>
								<li>Just think! You'll be up and running with your own membership site on the Internet so fast it will make your head spin watching all those recurring funds come in!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ATULVIP5"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->


<!-- Bonus #11 start -->
<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus11.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  CB Niche Builder 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  Learn how YOU can quickly and easily discover high profit affiliate products in the ClickBank.com Marketplace and build instant niche sites, ready to make money for you as soon as possible!<br><br>
							  <b>With user-friendly CB Niche Builder software tool, you can:</b>
							  	<li>Instantly find high profit niches by gaining world view access to the ClickBank.com marketplace.</li>
								<li>Rank your niche sites high in major Search Engines results for keywords that sell! Leverage on Search Engine Optimization.</li>
								<li>Prevention from having some of your rightful affiliate commissions stolen using my unique Affiliate Cloaking Website Generator!</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus12.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Affiliate Link Defender
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
 								<li>Redirect your cloaked links to any site of your choice so that others won't even notice it's an affiliate link and you'll still be credited for your commissions!</li>
								<li>Bypass merchants' squeeze pages and send your prospects directly to their sales pages or even your order page, which eliminates a step and increases your chances of making a sale</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus13.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Redirection Rocket 2.0 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Turn Those Long Nasty Affiliate Links Into Powerful, Search Engine Friendly Tracking Links Guaranteed To Increase Your Clickthroughs Up To 300%! In Less Than 10 Minutes You Could Have The Redirection Rocket 2.0 Script On Your Website & Less Than 10 Seconds Later Your Links Will Look Like The Pros!</li>
								<li>Using Redirection Rocket 2.0 you can attach a flyover optin code from getresponse or aweber to any campaign, and when someone goes to the link you are referring them to your popover shows on the other persons page! AND the cookies for tracking are STILL set. This is a powerful listbuilding tool.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus14.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   WP Stealth Links
					  		</div>      
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Instantly Make More Money Using The wpStealth Plugin To Mask Your Affiliate Links And Landing Pages With Point And Click Ease!</li>
								<li>wpStealth Easily Cloaks Your Affiliate Links And Landing Pages!</li>
								<li>This will keep "commission thieves" from stealing YOUR money.</li>
								<li>It will also build links back to YOUR site, instead of bit.ly or others.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus15.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Analyze Buzz 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Analyze Competition In Just Seconds! Here's What This Keyword Analysis Tool</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ATULVIP5"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus16.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  PayPal Cart for WP 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>"PayPal Cart for WP" turns any wordpress blog into a paypal profits machine.</b>
								It creates a simple, easy to install and use, <b>powerful, effective e-commerce solution! </b><br><br>
								<b>Features:</b>
							  	<li>Generates Paypal 'Buy Now' & 'Donate' buttons on the fly!</li>
								<li>Can create a complete, fully customizable Paypal shopping cart in minutes!</li>
								<li>Turns their blog into a automated Paypal profits machine!</li>		
								<li>Is incredibly simple to set up and use – no technical knowledge is necessary!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->


	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus17.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  Sales Motivator Pro 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Now You Can Easily Motivate Your Website Visitors To Buy Your Products!</b>
							  	<li>Every online business owners and affiliate marketers want to make out of the traffic that they generated to their website. This is the fact that gathering traffic to your website is sometimes time-consuming and other issues out there.</li>
								<li>The thing is that you want to convert those visitors into buyers and in order to do that you must also create some strategies on how you are going to approach them or attract their eyes to click on the buy now button of your product pages.</li>
								<li>In most cases, the most popular online retails store called Amazon.com has already applied this technique and if you want this to apply into your website, you should also have this amazing feature.</li>
			   				</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus18.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  WP Profit Page Creator 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Discover a Brand New and Highly Profitable WP Plugin that Once You Fire it Up, Will Generate You Endless Sales!</b><br><br>
							  	<li>If you are an online entrepreneur and you want to make so much profits out from your internet business, this amazing WordPress plugin is a huge help to you.</li>
								<li>What this software does is that, it will create SEO friendly money making WordPress pages almost every with a little content.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus19.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Smart Agent Pro 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Skyrocket Your Conversions, Sales And Profits With Your Own Automated Sales Assistants, Providing 24/7 Support On All Your Websites!</li>
								<li>You might already heard the saying that to become successful, you need to study what successful people do and model their actions!</li>
								<li>Well if you've been following the Internet Marketing scene lately you'll have noticed the growing popularity of those little 'exit chat agent' scripts. All the big name 'guru's' seem to be using them on all their sites.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->


	<!-- Bonus #20 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus20.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Auto Support Bot
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<b>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Your Visitors 24/7 Live Chat Support on Your Websites, Even While You're Sleeping</b><br><br>
							 	<li>Most Software iPacks activate your web browser to show a popup when you use the software. The popups help to fund the development cost of the individual Software iPacks, which means that you can usually buy Software iPacks at lower cost than normal software.</li>
								<li>Only a single popup will appear each time you use the software. The web addresses shown are embedded in the .sip file - so the creators of the Software iPack Player have no control of the websites that are displayed in the popups.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->
	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-12 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Coursesify + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
                <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">
					Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
				</div>
				<div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
			</div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © Coursesify 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://coursesify.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop ">	
					<div class="col-12 ">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 ">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

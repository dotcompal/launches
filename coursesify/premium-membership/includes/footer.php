<!----------- Footer Section-------------->
<footer class="footer-section">
	<div class="container container-1170">
		<div class="row">
		
			<!--- Footer Logo & Links ----->
			<div class="col-12 col-md-5 text-center">
				<div class="col-12 p0 clearfix">
				<!-- <img src="https://cdn.staticdcp.com/apps/website/assets/images/footer-logo.png" class="img-fluid "> -->
				<div class="d-block mx-auto float-md-left">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 214.51 43.96" style="enable-background:new 0 0 214.51 43.96; width:100%; max-height:25px;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FE6E09;}
	.st1{fill:#FFFFFF;}
</style>
<g>
	<g>
		<path d="M55.08,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
			C62.4,38.73,55.08,31.73,55.08,21.98z M81.02,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58s3.56,8.58,8.42,8.58
			C77.5,30.55,81.02,26.97,81.02,21.98z"></path>
		<path d="M120.05,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C115.02,6.03,120.05,10.87,120.05,17.55z
			 M111.02,17.57c0-2.27-1.52-3.87-3.84-3.87h-3.72v7.74h3.72C109.5,21.44,111.02,19.84,111.02,17.57z"></path>
		<path d="M149.44,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C144.4,6.03,149.44,10.87,149.44,17.55z
			 M140.41,17.57c0-2.27-1.53-3.87-3.84-3.87h-3.72v7.74h3.72C138.88,21.44,140.41,19.84,140.41,17.57z"></path>
		<path d="M169.23,25.45v12.47h-8.95v-12.5l-11.06-19.4h9.75l5.83,11.53l5.83-11.53h9.75L169.23,25.45z"></path>
		<path d="M179.42,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
			C186.74,38.73,179.42,31.73,179.42,21.98z M205.36,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58
			s3.56,8.58,8.42,8.58C201.84,30.55,205.36,26.97,205.36,21.98z"></path>
	</g>
	<g>
		<path class="st0" d="M43.96,39.27V2.45C43.96,1.1,42.86,0,41.5,0L4.69,0C2.5,0,1.41,2.64,2.95,4.19l11.38,11.38L2.81,27.09
			c-3.75,3.75-3.75,9.82,0,13.57l0.49,0.49c3.75,3.75,9.82,3.75,13.57,0l11.52-11.52L39.77,41C41.31,42.55,43.96,41.45,43.96,39.27z
			"></path>
		<circle class="st1" cx="22.2" cy="21.76" r="9.94"></circle>
	</g>
</g>
</svg>
				</div>
				</div>	
				<p class="f-14 f-lg-16 w400 d-flex align-items-center justify-content-center justify-content-md-start mt15 mt-md25">Copyright <span class="f-20">&nbsp;&copy;&nbsp;</span> 2022, Saglus Info Pvt Ltd. </p>
			</div>
			<div class="col-12 col-md-7 d-flex align-items-end justify-content-center justify-content-md-end mt10 mt-md0 text-center pl-md0">
				<ul class="footer-bottom-links f-14 f-lg-16 w400">
					<li><a target="_blank" href="https://www.oppyo.com/terms-condition" title="Terms & Conditions">Terms & Conditions</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.oppyo.com/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.oppyo.com/dmca" title="DMCA Policy">DMCA Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.oppyo.com/disclaimer" title="Cookies">Disclaimer</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<!----------- Footer Section end ------------->

<!-- Top Scroll Button start--->
	<a href="#" id="back-to-top" title="Back to top">
	<i class="icon-upper-dropdown" aria-hidden="true"></i>
	</a>
<!-- Top Scroll Button end --->

<!----------- Loader Section start ------------->
<div class="d-jhnone d-none" style="position: fixed; height: 100%; background: rgba(0,0,0,0.7); z-index: 99999; left: 0; right: 0; bottom: 0; right: 0;">
	<i class="icon-marketingsolution icon-loader"></i>
	<div class="page-loader"></div>
</div>
<!----------- Loader Section end ------------->

<!------------- Alerts Div start------->
<div class="alert-positions d-none">
<div class="alert alert-success-box alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon-dot-tick f-18"></i></span> </div>
          <div class="d-flex flex-wrap f-14">Well Done ! You Successfully read this alert message. </div>
        </div>
      </div>
		
      <div class="alert alert-primary-box alert-dismissible f-14">
         <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon icon-notification f-10"></i></span></div>
          <div class="d-flex flex-wrap">Heads Up ! This alert needs your attention, but it’s not super
            important. This could be an Informatial alert.</div>
        </div>
      </div>
		
      <div class="alert alert-primary-box alert-dismissible f-14 ">
         <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon icon-notification f-10"></i></span></div>
          <div class="d-flex flex-wrap">You Got a new message.. <a href="#" class="white-clr t-decoration-none"><u>Click here</u></a>&nbsp; to read </div>
        </div>
      </div>
		
      <div class="alert alert-primary-box1 f-14">
	    <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"><i class="icon-dot f-6 opacity20"></i> <i class="icon-dot f-6 opacity60"></i> <i class="icon-dot f-6"></i></span></div>
          <div class="d-flex flex-wrap">Just Finishing things ! Please wait...</div>
        </div>
      </div>
		
      <div class="alert alert-warning-box alert-dismissible f-14">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="fa fa-exclamation-triangle f-10"></i></span></div>
          <div class="d-flex flex-wrap">Warning ! Better check yourself, you’re not looking too good.</div>
        </div>
      </div>
		
      <div class="alert alert-danger-box alert-dismissible f-14">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="fa fa-exclamation-circle"></i></span></div>
          <div class="d-flex flex-wrap">Oh Snap ! Change a few things up and try submitting again.</div>
        </div>
      </div>
 </div>
<!------------- Alerts Div end--------->
<!----  Cookie consent popup section start --->
<div class="cookie-consent d-none">
<div class="container container-1170">
<div class="row align-items-center">
	<div class="col-12 col-md-8 col-lg-9 text-md-left text-center">
		<p class="f-14 w300 lh150 l-gblue-clr">DotcomPal uses cookies to provide you best online experience and making interactions with our website easy and meaningful.<br class="d-none d-xl-block">You can know more about our cookie policy <a href="privacy-policy.php" class="m-blue-clr w400">here.</a>
		</p>
	</div>	
	<div class="col-12 col-md-4 col-lg-3 text-md-right text-center mt15 mt-md0">
		<a href="javascript:" class="base-btn blue-btn mr7">Accept</a>		
		<a href="javascript:" class="base-btn white-outline-btn">Decline</a>		
	</div>
</div>	
</div>
</div>
<!----  Cookie consent popup section end ---->

<!---- Common Jquery Files ------> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/jquery/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/popper.min.js"></script> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap.min.js"></script> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap-select.js"></script>

<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/owl.carousel.min.css"/>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/owl.carousel.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/carousel-slider.js"></script>

<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/step-carousel.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/script.js"></script>

</body>
</html>

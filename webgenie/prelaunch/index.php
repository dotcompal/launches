<!Doctype html>
<html>
   <head>
      <title>WebGenie Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WebGenie Prelaunch">
      <meta name="description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta name="keywords" content="WebGenie">
      <meta property="og:image" content="https://www.webgenie.live/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WebGenie Prelaunch">
      <meta property="og:description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta property="og:image" content="https://www.webgenie.live/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WebGenie Prelaunch">
      <meta property="twitter:description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta property="twitter:image" content="https://www.webgenie.live/prelaunch/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script>
         function getUrlVars() {
             var vars = [],
                 hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for (var i = 0; i < hashes.length; i++) {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
                 document.getElementById('awf_field_aid').setAttribute('value', first);
             })
             //document.getElementById('myField').value = first;
      </script>
   </head>
   <body>
      <!--Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 70px;">
                        <defs>
                           <style>
                              .cls-1 {fill: #fff;}
                              .cls-2 {fill: #fc8548;}
                              .cls-3 {fill: #15c6fc;}
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-16 f-md-18 w400 white-clr text-md-end text-center ">
                     <li>
                        <a href="#book-seat1" class="affiliate-link-btn">Subscribe To EarlyBird VIP List</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-md-21 f-20 w500 lh140 white-clr">
                  Tap into This HUGE Part-Time or Full Time Income Opportunity with NO Extra Efforts!
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh140 text-shadow">
                  A Brand New WordPress Website Builder Preloaded with <span class="orange-clr">400+ themes Creates Beautiful Websites</span> in Just 60 Seconds
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w500 text-center lh140 white-clr text-capitalize">
               Easily Create & Sell Website In 100+ Niches | Create Local Business Websites, Affiliate Sites & Ecom Stores | 100% Newbie Friendly
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-8 col-12 mx-auto">
			   <img src="assets/images/product-box.webp" alt="ProductBOX " class="img-fluid d-block mx-auto" />
                  <!--<div class="responsive-video">
                     <iframe src="https://webpull.dotcompal.com/video/embed/9vxq6c88sw" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>-->
               </div>
            </div>
         </div>
      </div>
      <!--Header Section End -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 z-index-9">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                        <li><span class="w600">Tap Into Fast-Growing $284 Billion Website </span>Creation Industry to Make Easy Income</li>
                              <li> <span class="w600">Create Beautiful, Fast loading, SEO Friendly, and 100% Mobile Responsive Website</span> in Just 60 Seconds
                              </li>
                              <li> <span class="w600">Instantly Create Local Business Website, E-com Sites, and Blogs</span> for any Business or Niche
                              </li>
                              <li> <span class="w600">Accept Online Payments For Your Services & Products</span> with Seamless WooCommerce Integration</li>
                              <li>
                                 <span class="w600">
                                    Commercial License Included to Build an Incredible Income Helping Clients!
                              </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li> <span class="w600">Help Local Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</span> and They Will Happily Bank You Huge Profits</li>
                           <li> <span class="w600">reloaded with 400+ Done For You Themes</span> P for Local and Online Niches with Ready Content</li>
                           <li> <span class="w600">Customize and Update Themes on Live Website</span> Easily without Touching Any Code</li>
                           <li><span class="w600">Analytics & Remarketing Ready Website</span></li>
                           <li><span class="w600">100% Newbie-Friendly, No Tech Skills Needed, No Monthly Fees Ever.</span></li>
                        </ul>
                     </div>
                     <!-- <div class="col-12 mt10 mt-md20">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li class="w600">PLUS, FREE COMMERCIAL LICENSE IF YOU ACT TODAY!</li>
                        </ul>
                     </div> -->
                  </div>
               </div>
            </div> 
         </div>
      </div>
      <!---CTA-->
      <div class="cta-section">
         <div class="container">
           <div class="row">
               <div class="col-12">
                  <!-- <div class="f-22 f-md-28 w700 lh150 text-center white-clr">
                     Limited Free Commercial License. No Monthly Fee
                  </div>
                  <div class="f-18 f-md-20 lh150 w500 text-center mt10 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"WebGenie" </span> for Additional <span class="w700 orange-clr">$3 Discount</span> on Entire Funnel
                  </div> -->
                  <div class="row">
                     <div class="col-md-8 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#book-seat1" class="cta-link-btn">Subscribe To EarlyBird VIP List</a>
                     </div>
                  </div>
                  <div class="col-12 mx-auto mt20 mt-md40 f-18 f-md-20 w400 white-clr text-left lh160 text-md-start">
                    <ul class="ul-tick-icon1 p0 m0">
                        <li>Don't Pay $17/M. Get All Benefits For $17 One Time Only</li>
                        <li>Also Get A Chance To Win 10 FREE Copies</li>
                    </ul>
                </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form------------->

     <div class="form-sec">
         <div class="container">
            <div class="row mt20 mt-md70 mb20 mb-md70">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-24 f-md-38 w700 text-center lh140 black-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar 							
                     </div>
                     <div class="col-12 f-md-20 f-16 w600 lh140 mx-auto my20 text-center">
                        Register Now! And Join Us Live On May 14th, 10:00 AM EST
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                        <!-- Aweber Form Code -->
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="644064923">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6300013">
                              <input type="hidden" name="redirect" value="https://www.webgenie.live/prelaunch-thankyou" id="redirect_58a45706413675054653f49159d070a3">
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-644064923" class="af-form">
                              <div id="af-body-644064923" class="af-body af-standards">
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114379323"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114379323" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114379324"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114379324" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Subscribe To EarlyBird VIP List">
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt=""></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form Section------------->
      <!-- FAQ Section End -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                              .cls-1 {fill: #fff;}
                              .cls-2 {fill: #fc8548;}
                              .cls-3 {fill: #15c6fc;}
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                  <div class="f-14 f-md-16 w400 mt30 mt-md40 lh140 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebGenie 2022</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      
      <!-- Back to top button -->
      <a id="button">
         <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
            <g>
               <g>
                  <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
               </g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
         </svg>
      </a>

      <!-- scroll Top to Bottom -->
      <script>
         var btn = $('#button');

         $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
               btn.addClass('show');
            } else {
               btn.removeClass('show');
            }
         });

         btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
               scrollTop: 0
            }, '300');
         });
      </script>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-644064923').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-644064923")) {
                document.getElementById("af-form-644064923").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-644064923")) {
                document.getElementById("af-body-644064923").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-644064923")) {
                document.getElementById("af-header-644064923").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-644064923")) {
                document.getElementById("af-footer-644064923").className = "af-footer af-quirksMode";
            }
        }
    })();
</script>

   </body>
</html>
﻿<!-- OPPYO Header Include File -->
<?php include 'includes/deal-header.php'; ?>
<!-- OPPYO Header Include File end -->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/owl.carousel.min.css" />
<link rel="stylesheet" href="assets/css/carousel-slider.css" />
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/slick.css"/>
<link rel="stylesheet" href="assets/css/layout.css" />
<link rel="stylesheet" href="assets/css/header.css" />
<link rel="stylesheet" href="assets/css/pricing.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<script>

jQuery(document).ready(function($){
	//hide the subtle gradient layer (.pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
	checkScrolling($('.pricing-body'));
	$(window).on('resize', function(){
		window.requestAnimationFrame(function(){checkScrolling($('.pricing-body'))});
	});
	$('.pricing-body').on('scroll', function(){ 
		var selected = $(this);
		window.requestAnimationFrame(function(){checkScrolling(selected)});
	});

	function checkScrolling(tables){
		tables.each(function(){
			var table= $(this),
				totalTableWidth = parseInt(table.children('.pricing-features').width()),
		 		tableViewport = parseInt(table.width());
			if( table.scrollLeft() >= totalTableWidth - tableViewport -1 ) {
				table.parent('li').addClass('is-ended');
			} else {
				table.parent('li').removeClass('is-ended');
			}
		});
	}

	//switch from quaterly to annual pricing tables
	bouncy_filter($('.pricing-container'));

	function bouncy_filter(container) {
		container.each(function(){
			var pricing_table = $(this);
			var filter_list_container = pricing_table.children('.pricing-switcher'),
				filter_radios = filter_list_container.find('input[type="radio"]'),
				pricing_table_wrapper = pricing_table.find('.pricing-wrapper');

			//store pricing table items
			var table_elements = {};
			filter_radios.each(function(){
				var filter_type = $(this).val();
				table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="'+filter_type+'"]');
			});

			//detect input change event
			filter_radios.on('change', function(event){
				event.preventDefault();
				//detect which radio input item was checked
				var selected_filter = $(event.target).val();

				//give higher z-index to the pricing table items selected by the radio input
				show_selected_items(table_elements[selected_filter]);

				//rotate each pricing-wrapper 
				//at the end of the animation hide the not-selected pricing tables and rotate back the .pricing-wrapper
				
				if( !Modernizr.cssanimations ) {
					hide_not_selected_items(table_elements, selected_filter);
					pricing_table_wrapper.removeClass('is-switched');
				} else {
					pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {		
						hide_not_selected_items(table_elements, selected_filter);
						pricing_table_wrapper.removeClass('is-switched');
						//change rotation direction if .pricing-list has the .bounce-invert class
						if(pricing_table.find('.pricing-list').hasClass('bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
					});
				}
			});
		});
	}
	function show_selected_items(selected_elements) {
		selected_elements.addClass('is-selected');
	}

	function hide_not_selected_items(table_containers, filter) {
		$.each(table_containers, function(key, value){
	  		if ( key != filter ) {	
				$(this).removeClass('is-visible is-selected').addClass('is-hidden');

			} else {
				$(this).addClass('is-visible').removeClass('is-hidden is-selected');
			}
		});
	}
});



	</script>
<!-- welcome Banner Section -->
<!-- Banner Section -->
<section class="deal-page-banner">
	<div class="container container-1170">
		<div class="row">
			<div class="col-12 text-center mt50 mt-lg70">
				<p class="f-20 f-md-30 lh140 w400 pre-title">
					UNLEASH The FULL POWER Of WebGenie With OPPYO!
				</p>
				<h1 class="f-30 f-md-36 f-xl-48 firasans-font lh140 w400 title mt15 mt-md25">			
					Get <span class="w600">One-Time ACCESS</span> to ALL 20+ Premium OPPYO Apps For a<span class="w600"> LOW ONE TIME PRICE…</span>
				</h1>
				<p class="f-16 f-md-18 lh180 w400 pre-title mt10 mt-md20">
					
				
				SAVE UP TO $16,000 EVERY YEAR  <span class="px5">|</span> GET UNLIMITED GROWTH  <span class="px5">|</span> EASY INTEGRATIONS
				</p>
				
				<!--<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/deal-page/banner-image.png" class="img-fluid d-bloc mx-auto mt20 mt-md30 mt-xl50" alt="Banner">-->
				
				
			</div>
			
			<div class="col-12 col-lg-10 mx-auto mt-md35 mt20">		
			<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/product.png" class="img-fluid d-bloc mx-auto mt-md35 mt20" alt="Banner">			
					<!--<div class="embed-responsive embed-responsive-16by9 video-border-color">
						<iframe _ngcontent-serverapp-c1="" allowfullscreen="" class="embed-responsive-item" id="preview-player" scrolling="no" src="https://sellerspal.dotcompal.com/video/embed/wbkbrs68rn"></iframe>
					</div>-->
			</div>
			<div class="col-12 f-md-25 f-16 lh160 w400 firasans-font mt20 mt-md30 mt-xl50 text-center">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer!
			<br class="d-block d-md-none d-xl-block">So Don’t Miss It…
			</div>
	</div>
	
</section>
<!-- Banner section end-->
<!-- OPPYO Old Way section -->	
<section class="old-way-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
					The “Old Way” Of Building Your Online Business
				</h1>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-12 col-lg-10 mx-auto">
				<div class="mt20 mt-md30 mt-lg55 old-way-block">
					<div class="d-flex align-items-md-center">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Must purchase multiple apps and spend time managing them </h4>
							<p class="description">Most business owners use over 20 software & services to run their online business. The result is a complicated (and expensive) mess that wastes a lot of time and decreases your effectiveness.</p>
						</div>
					</div>
					<div class="d-flex align-items-md-center mt15 mt-md30">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Information overload & complex technical stuff  </h4>
							<p class="description">You need to learn multiple apps resulting in information overload and confusion. You are also expected to have the technical knowledge to use these apps.</p>
						</div>
					</div>
					<div class="d-flex align-items-md-center mt15 mt-md30">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Apps can work slowly and be difficult to connect one with another.</h4>
							<p class="description">Is your online business duct-taped together with different software? Connecting all these apps is difficult and you lose important data, intelligence and output that slows your growth.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt15 mt-md30 mt-lg60">
			<div class="col-12 text-center">
				<h3 class="home-page-subtitle">What if you could transform your business with a single app?</h3>
				<p class="home-page-title mt15 mt-md35">
				Never waste your time, money, and energy again dealing with multiple apps. Invest that time and energy to do more business and <br class="d-none d-xl-block">bring more growth from a single platform.
				</p>
			</div>
		</div>
	</div>	
</section>	
<!-- OPPYO Old Way section end -->
<!-- Say Hello to OPPYO section -->	
<section class="say-hello-section">
	<div class="container container-1170">
		<div class="row">
			<!-- Heading -->
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
					Say Hello to OPPYO
				</h1>
				<p class="home-page-sub-heading mt10 mt-md25">OPPYO is more than just building funnels & landing pages, websites and sending emails. With OPPYO, everything lives in one place so you can get started quickly and build more, sell more, and grow more online.</p>
			</div>
			<div class="col-12 col-md-10 mx-auto mt25 mt-md0 mt-lg0 d-none d-md-block">
				<img src="assets/images/features-img.webp" class="img-fluid d-block mx-auto feature-img" alt="Feature">
				<img src="assets/images/logo.png" alt="funnel" class="img-fluid logo">
				<img src="assets/images/fe1.png" alt="funnel" class="img-fluid fe1">
				<img src="assets/images/fe2.png" alt="funnel" class="img-fluid fe2">
				<img src="assets/images/fe3.png" alt="funnel" class="img-fluid fe3">
				<img src="assets/images/fe4.png" alt="funnel" class="img-fluid fe4">
				<img src="assets/images/fe5.png" alt="funnel" class="img-fluid fe5">
				<img src="assets/images/fe6.png" alt="funnel" class="img-fluid fe6">
				<img src="assets/images/fe7.png" alt="funnel" class="img-fluid fe7">
				<img src="assets/images/fe8.png" alt="funnel" class="img-fluid fe8">
			</div>
			<div class="col-12 col-md-10 mx-auto mt25 mt-md0 mt-lg0 ">
				<img src="assets/images/features-img-mview.png" class="img-fluid mx-auto d-block d-md-none" alt="Feature">
			</div>
		</div>
	</div>	
</section>	
<!-- Say Hello to OPPYO section end -->	
<!-- With OPPYO section -->
<section class="white-blue-bg">
	<div class="container container-1170">
		<div class="row no-gutters">
			<div class="col-12">
				<div class="with-dotcompal-block">
					<div class="row">
						<!-- Heading -->
						<div class="col-12 text-center">
							<h1 class="home-page-heading">
								With OPPYO, Online Business Is Redefined 
							</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pr-xl25">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="assets/images/save-more.png" class="img-fluid w-100 min-w-40" alt="Save More">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">SAVE TIME & EFFORT</p>
									<h4 class="title mt10 mt-md12">Everything you need,<br class="d-none d-xl-block"> all in one place.</h4>
									<p class="description mt10 mt-md15">OPPYO’s all-in-one integrated platform helps you start, manage, and grow your entire online business from one place. Save your time, money and energy and do more business online.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pl-xl25">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="assets/images/bring-more.png" class="img-fluid w-100 min-w-40" alt="Bring More">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">BRING MORE GROWTH</p>
									<h4 class="title mt10 mt-md12">At Every Stage Of Customer Lifecycle</h4>
									<p class="description mt10 mt-md15">OPPYO brings you <span class="w700">Fortune 500 Companies’ growth strategy and technology in an easy way </span>
									to get you more conversions, more sales, and more growth.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pl-xl25">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="assets/images/start.png" class="img-fluid w-100 min-w-44" alt="Start">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">START - QUICK & EASY</p>
									<h4 class="title mt10 mt-md12">Easy Platform with <br class="d-none d-xl-block"> Top-Notch Support</h4>
									<p class="description mt10 mt-md15">OPPYO is made for business owners like yourself and very easy to use. Our 24*7 support team is available to answer your questions & our FREE training will help you get started quickly & easily.</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12 col-md-7 col-xl-5 mx-auto mt20 mt-md30 mt-xl50 text-center">
							<!-- Button -->
							<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start px-xl30">Upgrade to OPPYO One-Time</a>
						</div>
						<div class="col-12 text-center mt10 mt-md15">
							<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>		
<!-- With OPPYO section End -->
<!-- In just 6 months section -->
<section class="online-entrepreneurs-bg">
	<div class="container container-1170">
		<div class="row text-center">
			<div class="col-12">
				<h2 class="home-page-title-white black-clr">In just 15 months, OPPYO has enabled 27,400+ Entrepreneurs to...</h2>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Build</h6>
					<h1 class="heading yellow">44,500</h1>
					<p class="description mt10 mt-md18">Websites, pages & <br> membership sites</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Serve</h6>
					<h1 class="heading cerise">107 Million</h1>
					<p class="description mt10 mt-md18">Website & marketing <br> campaigns visits</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Play</h6>
					<h1 class="heading java">52  Million</h1>
					<p class="description mt10 mt-md18">Minutes of fast videos on <br> their sites and pages</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Generate</h6>
					<h1 class="heading blue">190K+</h1>
					<p class="description mt10 mt-md18"><span class="w700">Conversions</span> from 4 million landing pages views</p>
				</div>
			</div>
			<div class="col-12 mt25 mt-md30 mt-lg60">
					<p class="white-clr f-16 f-md-20 lh160 black-clr"><span class="w700">OPPYO is growing exponentially like a charm.</span> 
					Business owners are loving it because OPPYO enables them with all the tools, proven growth strategies, and support needed in a single platform like a Silent Business Partner to simplify their online business. 
					<br>
					<br>
					<span class="w700">We are on mission to empower 100K businesses in 2023 and become the NO. 1 online business platform for entrepreneurs like yourself.</span></p>
			</div>
		</div>
	</div>
</section>		
<!-- In just 6 months section End -->
<!-- More Business section -->
<section class="more-business-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div id="buildonlinepresence1" class="d-block d-md-none"></div>
				<h2 class="home-page-title-grey">Start online and Grow your online business</h2>
				<h1 class="home-page-heading mt-md10 mt8">
					Everything You Need, All In One Place
				</h1>				
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">BUILD</h3>
				<h2 class="featurs-heading mt10 mt-md15">Website & Membership Sites</h2>
				<p class="home-page-title mt10 mt-md20">
					<span class="w700">Build your online presence</span> by creating your marketing website with domain and free hosting
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-businesswebsite feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Create a professional business <span class="w700">Website</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-freehosting feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Free OPPYO <span class="w700">Domain & Hosting.</span> Also connect your own custom domain
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-customsubdomain feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Build membership sites</span> (multi-level deep) to deliver products and courses.
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-friendlytemplates feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">400+ mobile-ready templates</span> with drag and drop editor.
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-buildmultilevel feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Quick-Start</span> with a free logo & migration assistance from Team OPPYO
						</p>
					</div>
				</div>
				
				<a id="sellproducts" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO  One-Time" href="#buynow">
					Upgrade to OPPYO  One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/build-img.gif" class="img-fluid d-block mx-auto" alt="Build">
			</div>
		</div>
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 col-lg-5 offset-lg-1 order-md-2">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">SELL</h3>
				<h2 class="featurs-heading mt10 mt-md15">Products & Accept Payments</h2>
				<p class="home-page-title mt10 mt-md20">
					<span class="w700">Sell products, services, and courses online.</span> Deliver in beautiful membership sites.
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-yourproducts feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Sell products</span>, courses, memberships, services, or physical goods
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sellunlimited feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Accept payments</span> worldwide using PayPal & Stripe. 
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-manageorders feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Sell unlimited. ZERO OPPYO fee</span> (limited offer)
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-feeonsales feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Deliver securely</span> with Memberships & MyDrive
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sellmodule feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Boost Sales</span> - Use upsells, cross-sell, discount coupons or create offers
						</p>
					</div>
				</div>
				
				<a id="growyourbusiness" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO  One-Time" href="#buynow">
					Upgrade to OPPYO One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/sell-img.png" class="img-fluid d-block mx-auto" alt="Sell">
				
			</div>
			<div id="growyourbusiness1" class="d-block d-md-none"></div>
		</div>
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">Grow</h3>
				<h2 class="featurs-heading mt10 mt-md15">All-In-One Integrated Marketing Solution</h2>
				<p class="home-page-title mt10 mt-md20">
					Attract, engage, and convert customers and <br class="d-none d-xl-block"><span class="w700">grow your business online.</span>
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-landingpages1 feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							High converting <span class="w700">Landing pages</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-video-help feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Fast <span class="w700">Video hosting and Player</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class=" icon-salesfunnels feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Funnels</span>/Customer Journey
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sendemails1 feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Send Emails</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-dynamicpopups feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Dynamic <span class="w700">popups, bars, and notification boxes</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-salesfunnels feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">A/B test</span> emails, pages, and popups
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-deepanalytics feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Deep Analytics</span>
						</p>
					</div>
				</div>
				
				<a id="manageeverything" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO  One-Time" href="#buynow">
					Upgrade to OPPYO  One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/grow-img.gif" class="img-fluid d-block mx-auto" alt="Grow">
			</div>
			<div id="manageeverything1" class="d-block d-md-none"></div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100" >
			<div class="col-12 col-md-6 col-lg-5 offset-lg-1 order-md-2">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">MANAGE</h3>
				<h2 class="featurs-heading mt10 mt-md15">Audience, Team, Integrations & More</h2>
				<p class="home-page-title mt10 mt-md20">
					Manage everything – Leads, customers, businesses, team, integrations & more
				</p>
				<div class="d-flex mt15 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-addmanagecontacts feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Manage Unlimited Contacts</span> (Leads & Customers)
						</p>
					</div>
				</div>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-smartaudience feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Smart Audience <span class="w700">Segments </span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-advancedleadmanagement feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Easy lead management</span> – with lists, tags & lead scoring
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-managemediacontent feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Manage Media Content</span> (store, share and deliver) with MyDrive
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-integrationsandourapi feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Connect with all major Apps <span class="w700">with 40+ Integrations & </span> OPPYO API
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-singledashboard feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Manage all your <span class="w700">businesses and team</span> easily in single dashboard
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/manage-img.png" class="img-fluid d-block mx-auto" alt="Manage-Img">
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt-md30 mt-xl90">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->
<!-- FEATURE SECTION -->
<section class="feature-section benefits-section" id="bringmoregrowth">
<div class="container container-1170">
	<!-- Heading -->
	<div class="row">
		<div class="col-12 text-center">
			<h2 class="home-page-title-grey">Our growth strategy & all-in-one integrated marketing apps </h2>
			<h1 class="home-page-heading mt-md10 mt8">
				Bring More Growth At Every Customer Touchpoint
			</h1>
		</div>
	</div>
	<!-- Desktop View -->
	<div class="col-12 p0 d-none d-md-block">
		<!-- Steps Carousel -->
		<div id="step-carousel-slider" class="owl-carousel owl-theme mt15 mt-md30 mt-lg55 px-xl55">
			<!--1. Steps Heading -->
			<div class="item">
				<span class="step-shape f-50 f-md-24 f-lg-50"><i class="icon-moreengagement"></i></span>
				<h5>More Engagement</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--2. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moreconversions f-40 f-md-22 f-lg-40"></i></span>
				<h5>More Leads</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--3. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moreinvestmentback"></i></span>
				<h5>More Engaging Follow-ups</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--4. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moresales"></i></span>
				<h5>More Sales & Growth</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--5. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moregrowth"></i></span>
				<h5>More Happy Customers</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
		</div>
		<!-- Carousel Slider -->
		<div id="carousel-steps-container" class="owl-carousel owl-theme mt15 mt-md30 mt-lg70">
			<!--1. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Customer engagement at scale </h3>
						</div>
						<h1 class="feature-heading">Attract & Engage More Visitors</h1>
						<p class="home-page-title mt10 mt-md15">
							Set your online business up for success by engaging your visitors with relevant messages and offers using OPPYO Popups, Bars and Notification Boxes. It enables you to present customized offers according to behaviour, timer-based deals, lead forms and many more options to engage more visitors on your website and pages. <br><br>
							Use OPPYO Fast loading Videos & player (sales, demos, and testimonials videos) to ensure that your visitors see and hear your message loud and clear, preventing bouncing.<br><br>
							Furthermore, send follow up emails to your leads based on their activity on your website. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="assets/images/step1.png" class="img-fluid d-block mx-auto step-image" alt="Step1">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
					</div>
				</div>
			</div>
			<!--2. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Convert more traffic</h3>
						</div>
						<h1 class="feature-heading">Capture More Targeted Leads</h1>
						<p class="home-page-title mt10 mt-md15">
							OPPYO helps you capture more targeted leads from your paid and organic traffic around the clock. Create simple lead funnels to generate new leads, follow up with them even after they leave your page or website!<br><br>
							Use OPPYO high converting lead pages and website pop-ups and sticky bars with inbuilt lead forms to capture more leads, register more people for your webinars, increase subscribers to your blogs/newsletters, etc. and it help you convert them while they are hot!
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="assets/images/step2.png" class="img-fluid d-block mx-auto step-image" alt="Step2">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
					</div>
				</div>
			</div>
			<!--3. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Nurture & close prospects</h3>
						</div>
						<h1 class="feature-heading">Send Better Follow-up Emails</h1>
						<p class="home-page-title mt10 mt-md15">
							Harness the power of trigger-based follow up email sequence to nurture and activate your leads and convert them into paying customers. <br><br>
							OPPYO Mail enables you to send special offers, loyalty bonuses, training material, updates, newsletters, etc. to your leads based on their interaction with your website, landing pages, and email communication. And keep them coming back for more!
							Also get access to customizable email automation workflow templates with OPPYO. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="assets/images/step4.png" class="img-fluid d-block mx-auto step-image" alt="Step4">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
					</div>
				</div>
			</div>
			<!--4. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Boost your sales </h3>
						</div>
						<h1 class="feature-heading">Sell More. More Quickly</h1>
						<p class="home-page-title mt10 mt-md15">
							Move more deals through your marketing funnel, using personalized customer journeys based on user data and behaviour.<br><br>
							With OPPYO, create high converting offer pages or complete high converting sales funnels with upsells and down-sells that are designed to help you convert traffic through each step of the sales process and convert them from “visitors” to “buyers” faster.
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block mt5 mt-md0">
						<img src="assets/images/step3.png" class="img-fluid d-block mx-auto step-image" alt="Step3">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
					</div>
				</div>
			</div>
			<!--5. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Delight your customers</h3>
						</div>
						<h1 class="feature-heading">From Customers to Your Brand Advocates</h1>
						<p class="home-page-title mt10 mt-md15">
							Convert your valued customers to brand advocates by delighting them with after-sales services delivered using OPPYO like – fast loading training videos, deliver their digital products inside membership sites, secured documents & files, etc. <br><br>
							Testimonial videos, social proof videos, case studies, etc can further help you increase your credibility with prospects and customers alike, as well as help you get word of mouth publicity. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="assets/images/step5.png" class="img-fluid d-block mx-auto step-image" alt="Step5">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Mobile View -->
	<div class="col-12 p0 d-bolck d-md-none mt20 mt-md0">
	<div class="slider-nav">
		<div>
			<span class="xs-step-shape"><i class="icon-moreengagement"></i></span>
			<h5>More Engagement</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moreconversions"></i></span>
			<h5>More Leads</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moreinvestmentback"></i></span>
			<h5>More Engaging Follow-ups</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moresales"></i></span>
			<h5>More Sales & Growth</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moregrowth"></i></span>
			<h5>More Happy Customers</h5>
		</div>
	</div>
	<div class="slider-for mt20">
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Customer engagement at scale </h3>
					<h1 class="feature-heading">Attract & Engage More Visitors</h1>
					<p class="home-page-title mt10">
					Set your online business up for success by engaging your visitors with relevant messages and offers using OPPYO Popups, Bars and Notification Boxes. It enables you to present customized offers according to behaviour, timer-based deals, lead forms and many more options to engage more visitors on your website and pages. <br><br>
					Use OPPYO Fast loading Videos & player (sales, demos, and testimonials videos) to ensure that your visitors see and hear your message loud and clear, preventing bouncing.<br><br>
					Furthermore, send follow up emails to your leads based on their activity on your website. 
					</p>
					<img src="assets/images/step1.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step1">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15 p0">
					<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Convert more traffic</h3>
					<h1 class="feature-heading">Capture More Targeted Leads</h1>
					<p class="home-page-title mt10">
						OPPYO helps you capture more targeted leads from your paid and organic traffic around the clock. Create simple lead funnels to generate new leads, follow up with them even after they leave your page or website!<br><br>
						Use OPPYO high converting lead pages and website pop-ups and sticky bars with inbuilt lead forms to capture more leads, 
						register more people for your webinars, increase subscribers to your blogs/newsletters, etc. and to help you convert them while they are hot!
					</p>
					<img src="assets/images/step2.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step2">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title"> Nurture & close prospects</h3>
					<h1 class="feature-heading">Send Better Follow-up Emails</h1>
					<p class="home-page-title mt10">
						Harness the power of trigger-based follow up email sequences to nurture and activate your leads and convert them into paying customers.<br><br> 
						OPPYO Mail enables you to send special offers, loyalty bonuses, training material, updates, newsletters, etc. to your leads based on their interaction with your website, landing pages, and email communication. And keep them coming back for more!<br><br>
						Also get access to customizable email automation workflow templates with OPPYO. 
					</p>
					<img src="assets/images/step3.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step3">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Boost your sales</h3>
					<h1 class="feature-heading">Sell More. More Quickly</h1>
					<p class="home-page-title mt10">
						Move more deals through your marketing funnel, using personalized customer journeys based on user data and behaviour.<br><br>
						With OPPYO, create high converting offer pages or complete high converting sales funnels with upsells and down-sells that are designed to help you convert traffic through each step of the sales process and convert them from “visitors” to “buyers” faster.
					</p>
					<img src="assets/images/step4.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step4">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Delight your customers</h3>
					<h1 class="feature-heading">From Customers to Your Brand Advocates</h1>
					<p class="home-page-title mt10">
						Convert your valued customers to brand advocates by delighting them with after-sales services delivered using OPPYO like – fast loading training videos, deliver their digital products inside membership sites, secured documents & files, etc. <br><br>
						Testimonial videos, social proof videos, case studies, etc can further help you increase your credibility with prospects and customers alike, as well as help you get word of mouth publicity. 
					</p>
					<img src="assets/images/step5.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step5">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
</section>
<!-- FEATURE SECTION END  -->

<section class="comp-table">
         <div class="comparetable-section ">
		 <div class="one-time">
		 <h2 class="f-md-28 f-sm-22 w500 lh150 text-center white-clr ">Not only does OPPYO help you build and grow your business  <br class="d-none d-xl-block"> 
              from a single platform without any tech hassles, but it also helps your bottom line.
            </h2>
					<div class="f-md-50 f-sm-26 w700 lh150 text-center white-clr ">
					OPPYO Founders One-Time Access Replaces All Subscriptions
						</div>
						<h2 class="f-md-28 f-sm-20 w400 lh150 text-center white-clr">CALCULATE YOUR SAVINGS</h2>
				  </div>
            <div class="container ">
               <div class="row ">
			   <div class="col-12 d-block d-md-none mx-auto">
			   <img src="assets/images/mview.png" class="img-fluid d-block mx-auto" alt="mview">
						</div>
                  <div class="col-12 mt10 d-none d-md-block mx-auto">
                     <div class="row mx-auto pricing-row">
                        <div class="col-md-4 col-4 pd0">
                           <div class="fist-row">
                              <ul class="f-md-18 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700 justify-content-start">Platform</li>
                                 <li>Website Builder</li>
                                 <li>Page Builder</li>
                                 <li>Shopping Cart</li>
                                 <li>Membership CMS</li>
                                 <li>Video Hosting</li>
                                 <li>Email Automation</li>
                                 <li>Ecommerce</li>
                                 <li>Popups & Bars</li>
                                 <li>Drive</li>
                                 <li>Funnel Builder</li>
                                 <li>Audience</li>
                                 <li>Analytics</li>
                                 <li>A/B Testing</li>
                                 <li>QR Code</li>
                                 <li>ADA Finder</li>
                                 <li>Restaurant</li>
                                 <li>Reservation</li>
                                 <li></li>
                                 <li>Webinars</li>
                                 <li>Messaging</li>
                                 <li>Blog</li>
                                 <li>Helpdesk</li>
                                 <li>Course</li>
                                 <li>Appointments</li>
                                 <li>Calendar</li>
                                 <li>Affiliates</li>
                                 <li>Survey </li>
                                 <li>Marketplace</li>
								 <li> - </li>
                              </ul>
                           </div>
                        </div>
                 
                        <div class="col-md-4 col-2 pd0">
                           <div class="second-row ">
                              <ul class="f-md-16 f-14 w500 lh120">
								<li><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 214.51 43.96" style="enable-background:new 0 0 214.51 43.96; width:100%; max-height:30px;" xml:space="preserve" >
								<style type="text/css">
									.st0{fill:#FE6E09;}
									.st1{fill:#FFFFFF;}
								</style>
								<g>
									<g>
										<path d="M55.08,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
											C62.4,38.73,55.08,31.73,55.08,21.98z M81.02,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58s3.56,8.58,8.42,8.58
											C77.5,30.55,81.02,26.97,81.02,21.98z"></path>
										<path d="M120.05,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C115.02,6.03,120.05,10.87,120.05,17.55z
											M111.02,17.57c0-2.27-1.52-3.87-3.84-3.87h-3.72v7.74h3.72C109.5,21.44,111.02,19.84,111.02,17.57z"></path>
										<path d="M149.44,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C144.4,6.03,149.44,10.87,149.44,17.55z
											M140.41,17.57c0-2.27-1.53-3.87-3.84-3.87h-3.72v7.74h3.72C138.88,21.44,140.41,19.84,140.41,17.57z"></path>
										<path d="M169.23,25.45v12.47h-8.95v-12.5l-11.06-19.4h9.75l5.83,11.53l5.83-11.53h9.75L169.23,25.45z"></path>
										<path d="M179.42,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
											C186.74,38.73,179.42,31.73,179.42,21.98z M205.36,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58
											s3.56,8.58,8.42,8.58C201.84,30.55,205.36,26.97,205.36,21.98z"></path>
									</g>
									<g>
										<path class="st0" d="M43.96,39.27V2.45C43.96,1.1,42.86,0,41.5,0L4.69,0C2.5,0,1.41,2.64,2.95,4.19l11.38,11.38L2.81,27.09
											c-3.75,3.75-3.75,9.82,0,13.57l0.49,0.49c3.75,3.75,9.82,3.75,13.57,0l11.52-11.52L39.77,41C41.31,42.55,43.96,41.45,43.96,39.27z
											"></path>
										<circle class="st1" cx="22.2" cy="21.76" r="9.94"></circle>
									</g>
								</g>
								</svg></li>
                                 <!-- <li class="f-md-28 f-16 w700"><span>OPPYO Solution</span></li> -->
                                 <li>OPPYO Websites</li>
                                 <li>OPPYO Landing Pages</li>
                                 <li>OPPYO Sell</li>
                                 <li>OPPYO Memberships</li>
                                 <li>OPPYO Videos</li>
                                 <li>OPPYO Mail</li>
                                 <li>OPPYO Sell (Physical Goods)</li>
                                 <li>OPPYO Popups, Bars & Notifications</li>
                                 <li>OPPYO MyDrive</li>
                                 <li>OPPYO Funnels/Customer Journey</li>
                                 <li>OPPYO Audience</li>
                                 <li>OPPYO Analytics</li>
                                 <li>OPPYO A/B Testing</li>
                                 <li>OPPYO QR Code Generator</li>
                                 <li>OPPYO ADA Finder & Fixer</li>
                                 <li>OPPYO Restaurant Builder</li>
                                 <li>OPPYO Reservation System</li>
                                 <li class="orange-bg">Future Coming Apps In 2023</li>
                                 <li>OPPYO AutoWebinars</li>
                                 <li>OPPYO SMS</li>
                                 <li>OPPYO Blog</li>
                                 <li>OPPYO Helpdesk</li>
                                 <li>OPPYO Course Builder</li>
                                 <li>OPPYO Appointments</li>
                                 <li>OPPYO Calendar</li>
                                 <li>OPPYO Affiliates</li>
                                 <li>OPPYO Survey</li>
                                 <li>OPPYO Marketplace</li>
								 <li class="text-center  black-clr">
									<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/svq2f4" class="price-btn active firasans-font ">
										<span class="w600 black-clr f-26">Get Everything for a Low One-Time Price of Only</span>	
										<strong class="f-40 f-xs-25 orange-clr">@$697</strong>
									</a>
								</li>
						
                              </ul>
							  
                           </div>
                        </div>
                        <div class="col-md-4 col-2 pd0">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Replaces</span></li>
                                 <li>Wix/Squarespace $480/Year</li>
                                 <li>Kartra - $3564/Year</li>
                                 <li>SamCart - $2388/Year</li>
                                 <li>Kajabi - $3828/Year</li>
                                 <li>Wistia - $1188/Year</li>
                                 <li>ActiveCampaign $7,188/Year</li>
                                 <li>Shopify - $948/Year</li>
                                 <li>OptinMonster - $600/Year</li>
                                 <li>Amazon S3/Dropbox - $1200/Year</li>
                                 <li>ClickFunnels $3,558/Year</li>
                                 <li>-</li>
                                 <li>-</li>
                                 <li>Growth Book - $240/Year</li>
                                 <li>QRFY - $20,244/Year</li>
                                 <li>EqualWeb - $468/Year</li>
                                 <li>-</li>
                                 <li>Calendly $144/Year</li>
                                 <li></li>
                                 <li>GoToWebinar/EverWebinar $4,080/Year</li>
                                 <li>Twilio</li>
                                 <li>WordPress</li>
                                 <li>Zendesk</li>
                                 <li>Teachable</li>
                                 <li>Appointy</li>
                                 <li>Calendly</li>
                                 <li>Tapfiliate</li>
                                 <li>SurveyMonkey</li>
                                 <li>-</li>
								 <li>
									<strong class="f-40 f-xs-25 text-center">$49,878<span class="f-15">/Year</span></strong> 
									
								</li>
                              </ul>
                           </div>
                        </div>
						<div class="col-12">
                        <div class="text-center w600 f-24 f-xs-25">Plus more OPPYO apps will come in 2023 to save you more money, time and efforts.</div>
						</div>
                     </div>
                     
                  </div>
				  <div class="row align-items-center mt20 mt-md30 mx10">
			<div class="col-12 col-xl-10 offset-xl-1 f-14 lh160">
				<strong>Note*</strong> All Prices for above companies were verified on Jan, 6th 2023 on their websites. Their prices may change at anytime and the above prices are only for comparison.
				Savings for your business depends on your size and growth so calculate your own savings for yourself.
			</div>
		</div>
                  
               </div>
			   <div class="row align-items-center mt20 mt-md30 mt-lg80">
         <div class="col-12">
            <h2 class="home-page-title-grey text-center">With the One-Time Founders' Deal, <u>you’re saving over $48,000 yearly</u> !!!!</h2>
            <h1 class="home-page-heading mt-md10 mt8 text-center">
               Now that is one heck of an investment, isn’t it…?
            </h1>
            <p class="home-page-title f-md-22 mt10 mt-md20 text-center">OPPYO is unrivalled in its offerings. With the special One-Time Founders' Deal, you get access to 20+ premium OPPYO Apps at an UNMATCHED one-time pricing of $697 only.
            </p>
         </div>
            </div>
			
      </div>
         </div>
      </section>
<!-- CALCULATE YOUR SAVINGS section -->




<div class="pricing-container">
	
	<div class="container">
	<div class="col-12 text-center">
					<div id="buildonlinepresence1" class="d-block d-md-none"></div>
					<h1 class="home-page-heading" id="buynow">
						Upgrade to OPPYO 
					</h1>		
					<h2 class="home-page-title-grey mt-md10 mt8">(Grab Limited One-Time Deal!)</h2>
				</div>
		<div class="pricing-wapper mt30 mt-md50">

		<div class="row">
						<div class="col-md-4 mt20">
						
						<header class="pricing-header">
						<div class="pricing-table-header">
                            <div class="f-15 text-black w500">OPPYO Monthly</div>
                        </div>
							<div class="price">
								<span class="currency">$</span>
								<span class="value">77</span>
								<span class="duration">Month</span>
							</div>
							<div class="f-14 d-gblue-clr mt10"> Billed monthly</div>
							<div class="pricing-table-box">
							<div class="pricing-buy-btn-wrap">							
							<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/dt6z2s" target="_blank"  class="price-btn firasans-font blue-btn">Buy Now</a>
						</div></div>
						</header>
						
						</div>

						

						<div class="col-md-4 mt20">
						<header class="pricing-header">
						<div class="pricing-table-header">
                            <div class="f-15 text-black w500">OPPYO One-Time</div>
                        </div>
							<div class="price">
								<span class="currency">$</span>
								<span class="value">697</span>
								<span class="duration">One-Time</span>
							</div>
							<div class="f-14 d-gblue-clr mt-sm10"><span class="orange-clr w700"> Save $49,878 </span> | Get 70% Discount
								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Only $697 for One-Time service - Billed Onetime only" class="price-info-icon ml5">
									<i class="icon-infotooltip"></i>
								</a>
							</div>
							<div class="pricing-table-box">
							<div class="pricing-buy-btn-wrap">							
							<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/svq2f4" target="_blank"  class="price-btn firasans-font blue-btn">Buy Now</a>
						</div></div>
						</header>
					
						
						</div>


						<div class="col-md-4 mt20">
						<header class="pricing-header">
						<div class="pricing-table-header">
                            <div class="f-15 text-black w500">OPPYO 3 Installments</div>
                        </div>
							<div class="price">
								<span class="currency">$</span>
								<span class="value">247</span>
								<span class="duration">Month</span>
							</div>
							<div class="f-14 d-gblue-clr mt10"> Billed monthly for 3 months </div>
							<div class="pricing-table-box">
							<div class="pricing-buy-btn-wrap">							
							<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/qxxrkg"  target="_blank" class="price-btn firasans-font blue-btn">Buy Now</a>
						</div></div>
						</header>
					
						</div>	
		</div>


			<div class="row">
				<div class="col-md-6">
					<div class="pricing-table-content mt20 mt-md30">
						<div class="price-table-heading firasans-font">
							<span class="black-clr w600 f-22 ">Build Your Online Business</span>
						</div>
						<ul class="features pricing-first-row pt-sm20">
							<li><strong>Unlimited</strong> Visits &amp; Leads</li>
							<li><strong>Unlimited</strong> Contacts</li>
							<li><strong>Unlimited</strong> Websites</li>
							<li><strong>Unlimited</strong> Landing Pages </li>
							<li><strong>400</strong> Mobile Ready Templates</li>
							<li>Send <strong>300,000</strong> emails/month</li>
							<li><strong>Unlimited</strong> Team Members</li>
							<li><strong>Priority Support Via Call,</strong> Chat, Email &amp; Tickets</li>
						</ul>
							<div class="price-table-heading firasans-font">
								<span class="blac-clr w600 f-22">Grow Your Business</span>
							</div>
							<ul class="features w400 pt-sm20 pricing-second-row">
                                <li><strong> Advanced </strong>Business Website </li>
                                <li><strong>Advanced</strong> Membership Sites</li>
								<li>Create &amp; Sell <strong>Unlimited</strong> Products</li>
								<li>0% fee on sales <strong>(Limited Time)</strong></li>
								<li><strong>Cross-Sell, Uspell, Offers &amp; Coupon</strong></li>
								<li><strong>Unlimited</strong> Landing Pages</li>
								<li><strong>Unlimited</strong> Email Campaigns</li>
								<li>Host Up To <strong>100 Videos</strong></li>
								<li> Unlimited Pop Ups, Bars &amp; Notification Boxes</li>
								<li><strong>Unlimited</strong> Funnels/Customer Journey <strong>(All Types)</strong></li>
								</ul>
							</div>
						</div>
					<div class="col-md-6">
						<div class="pricing-table-content mt20 mt-md30">
							<ul class="features w400 pt-sm20 pricing-four-row f-22">
								<li>Advanced Analytics </li>
								<li><strong>Advanced</strong> Audience Management</li>
								<li><strong>Unlimited</strong> Audience Segments</li>
								<li><strong>Upload Contacts</strong></li>
								<li>A/B Test for Pages, Emails and Popups</li>
								<li>Dynamics Popups</li>
								<li><strong>Video Playlist</strong></li>
								<li><strong>Audience Journey Details With Timeline</strong></li>
                            </ul>
							<div class="price-table-heading  firasans-font mt-4">
								<span class="black-clr w600 f-22">Other Features</span>
							</div>
							<ul class="features w400 pt-sm20 pricing-third-row">
								<li><strong>Advanced</strong> MyDrive</li>
                                <li><strong>Remove our logo from emails &amp; Add Your Logo on videos</strong></li>
								<li><strong>50</strong> GB Storage, <strong>200</strong> GB Bandwidth/ month </a>
                                </li>
								<li><strong>Double commission for OPPYO Affiliate program</strong></li>
								<li><strong>OPPYO Agency </strong></li>
								<li>40+ Integrations with<strong> Advanced integrations API</strong></li>
								<li>
                                  <strong>Founder's Special - Quick start bonuses</strong> 1 free custom domain, 1 logo creation and your first online business &amp; marketing campaign setup assistance from Team OPPYO
                                </li>
								<li>
                                  <strong> Founders Only Access to Upcoming Apps </strong>Advanced Cart, Affiliate Manager, Auto Webinars, SMS, Blog, HelpDesk, Course Builder,
									Appointments, Calendar, Ecommerce, Survey, OPPYO Marketplace &amp; More…
                                </li>
                            </ul>
						</div>
					</div>
				</div>
	

				<div class="row no-gutters md-bottom-button">                
                <div class="col-12 col-md-4 mt20">
                    <div class="pricing-table-bottom">
					<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/dt6z2s"><img src="https://warriorplus.com/o2/btn/fn200011000/f7zqn7/zm2f47/349724" alt="WebGenie Premium Monthly Membership" border="0" class="mx-auto d-block img-fluid"></a>
					<div class="f-14 black-clr mt10 w700 text-center"> Billed monthly</div>
                    </div>
                </div>
               
				<div class="col-12 col-md-4 mt20">
                    <div class="pricing-table-bottom">			
					<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/svq2f4"><img src="https://warriorplus.com/o2/btn/fn200011000/f7zqn7/zm2f47/349723" alt="WebGenie Premium One-Time Plan" border="0" class="mx-auto d-block img-fluid"></a>
					<div class="f-14 black-clr mt10 w700 text-center"> Billed One-Time </div>
					<div class="f-14 black-clr w700 mt-sm10 text-center"><span class="orange-clr w700"> Save $49,878 </span> | Get 70% Discount
								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Only $697 for One-Time service - Billed Onetime only" class="price-info-icon ml5">
									<i class="icon-infotooltip"></i>
								</a>
							</div>
                    </div>
                </div>
				 <div class="col-12 col-md-4 mt20">
                    <div class="pricing-table-bottom">   				 
					<a href="https://warriorplus.com/o2/buy/f7zqn7/zm2f47/qxxrkg"><img src="https://warriorplus.com/o2/btn/fn200011000/f7zqn7/zm2f47/349725" alt="OPPYO Premium Membership (3 Instalments)" border="0" class="mx-auto d-block img-fluid"></a>
					<div class="f-14 black-clr mt10 w700 text-center"> Billed monthly for 3 months </div>
                    </div>
                </div>
            </div>


	</div>
	</div>
						</div>




































<!-- FREE TRAINING SECTION -->
<section class="free-training-section grey-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="home-page-title-grey">That’s Not All</h2>
				<h1 class="home-page-heading mt-md10 mt8">
					All Serious Entrepreneurs Who Secure This Founder’s Special Deal Today Will Also Get These Fast Action Bonuses:
				</h1>
				<p class="home-page-title mt15 mt-md40">
					YES, if you are one of the early action takers who upgrade to the Founders' Special Deal today, you not only secure your access to OPPYO, but also get these very limited bonuses.
				</p>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="assets/images/training-img1.png" class="img-fluid d-block mx-auto" alt="Training1">
				<h4 class="title mt15 mt-md25">1 Free Custom <br class="d-none d-xl-block"> Domain <span class="w400">for 1 year</span></h4>
			</div>
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/training-img2.png" class="img-fluid d-block mx-auto" alt="Training2">
				<h4 class="title mt15 mt-md25"> 1 Custom & <br class="d-none d-xl-block"> Professional Logo <br class="d-none d-xl-block"> <span class="w400">For Your Business</span></h4>
			</div>
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/training-img3.png" class="img-fluid d-block mx-auto" alt="Training3">
				<h4 class="title mt15 mt-md25">1-To-1 Assistance</h4>
				<p class="f-16 f-md-20 lh160 w400 mt10 mt-md15">
					Migration help; launch your first online business & marketing campaign setup assistance from Team OPPYO
				</p>
			</div>
		</div>
		<div class="row text-center">
			<!--<div class="col-12 mt20 mt-md30 mt-lg65">
				<p class="f-16 f-md-20 lh160 w400">
					If you ever need an extra hand, we also have an amazing, FREE quick setup service (migration help & online business setup) to help you at any step along the way. We can help you set up your logo, domain, offers, funnels, and marketing campaigns in OPPYO easily. We also provide marketing expertise as and when required so you never get stuck.
				</p>
			</div>-->
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md30 mt-xl60">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
			</div>
		</div>
	</div>
</section>		
<!-- FREE TRAINING SECTION END -->

<!-- More Business section -->
<section class="more-business-section" id="allsolutions">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div class="d-block d-md-none"></div>
				<h1 class="home-page-heading text-capitalize">
				Why This Crazy LOW Pricing?
				</h1>
				<h2 class="home-page-title-grey mt-md10 mt8">
				Great deals come seldom and we want to see you succeed with us.   <br class="d-none d-xl-block"> 
				Therfore, we want your support to make this a reality and, in turn and especially since this is still in BETA, we want to save you a huge amount of money, time and effort. 
				</h2>
								
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 col-lg-6 order-md-2">				
				<p class="home-page-title">
				OPPYO has been used and thoroughly tested by 27,400 + entrepreneurs like yourself over the last 15 months. Yes, we are still in beta as we are continuously adding new apps and solutions. Our team of more than 40 full time and inhouse developers is working round the clock to get OPPYO fully loaded. More and more features and tools are being released every month so we need your support in using it, testing it, and improving it by suggesting features while we, in turn, help you save tons of time and money.
                 <br><br>
					to join OPPYO at the ground level, by becoming a <span class="w700">FOUNDING MEMBER </span>and taking advantage of the  <span class="w700">One-Time FOUNDER'S DEAL</span> and getting access to all the features OPPYO has to offer now, as well as in the future.
				</p>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/great-deals.png" class="img-fluid d-block mx-auto" alt="Great-Deal">
				
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->
<!-- More Business section -->
<section class="feature-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div id="buildonlinepresence1" class="d-block d-md-none"></div>				
				<h1 class="home-page-heading">				
					Also, If You Upgrade To The OPPYO <br class="d-none d-xl-block">
One-Time Deal Today

				</h1>				
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">			
				<p class="home-page-title mt10 mt-md20">
					As soon as you join OPPYO, you become a Founding Member AND automatically become an affiliate. 
<br><br>
Just let people know about OPPYO and you will receive 50% commissions on all your referrals FOREVER! (Usually 30%) 
<br><br>
We will do the heavy lifting while you enjoy the revenue.
<br><br>
Upgrade now to earn more from each referral.
				</p>

			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/lifetime-deal.png" class="img-fluid d-block mx-auto" alt="Life Time Deal">
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md30 mt-xl90">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->

<!-- BUSINESS OWNERS START -->
<section class="business-owners">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
				Here’s Why 27,400+ Online Entrepreneurs <br class="d-none d-lg-block"> Switched To OPPYO?
				</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<p class="testi-description mt15 mt-md25 text-left">The simplicity and ease-of-use, along with the ability to have full control on my marketing is the foremost reason for me to like and recommend OPPYO.</p>
					<hr class="orange-line">
					<div class="row align-items-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/joe-lervolino.png" class="img-fluid d-block testi-image" alt="Jeo Lervolino">
					<p class="f-14 f-md-16 w600 lh140 text-left"> Joe Iervolino</p>
						</div>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<p class="testi-description mt15 mt-md25 text-left">After getting OPPYO, I quickly, smartly and easily acheived our goals with no hassles at all. </p>
					<hr class="orange-line">
					<div class="row align-items-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/dennis-mcmanus.png" class="img-fluid d-block testi-image" alt="Dennis Mamanus">
					<p class="f-14 f-md-16 w600 lh140 text-left"> Dennis McManus </p>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<p class="testi-description mt15 mt-md25 text-left">
					OPPYO is fast, easy and 100% bankable. I am really impressed by the quality I received from OPPYO Solutions. I have got a good e-commerce site for my products. My revenue has increased because of OPPYO and I definitely recommend OPPYO.</p>
					<hr class="orange-line">
					<div class="row align-items-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/lyall-stichbury.png" class="img-fluid d-block testi-image" alt="Lyall Stichbury">
					<p class="f-14 f-md-16 w600 lh140 text-left"> Lyall Stichbury</p>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<p class="testi-description mt15 mt-md25 text-left">
						OPPYO customer success team hats off to you all!! 
						I have never had such a wonderful support experience from any other service provider. The team has always been available to understand and answer any concern or issue I might be facing, no matter the time, no matter the complexity of the problem, without giving any excuses or making me wait for long holds. Marvellous experience.  </p>
					<hr class="orange-line">
					<div class="row align-items-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/testi-img4.png" class="img-fluid d-block testi-image" alt="Testimonial-Image">
					<p class="f-14 f-md-16 w600 lh140 text-left"> Darnell </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- BUSINESS OWNERS END -->
<!-- Frequently Asked Questions Section -->
<section class="section-padding pt0">
<div class="container container-1170">
   <!-- Heading -->
   <div class="row align-items-center">
      <div class="col-12 col-md-8 col-lg-8 text-center">
         <h1 class="heading-text">
            Frequently Asked Questions
         </h1>
      </div>
	  <div class="col-12 col-md-4 col-lg-4 mr-auto">
                  <img src="assets/images/support-img.png" class="img-fluid d-block mx-auto" alt="Support">
                  <!-- <p class="f-16 f-md-18 w700 d-mblue-clr mr-auto"><i>World Class<br>24/7 Support</i></p> -->
               </div>
   </div>
   <div class="row mt30 mt-md80">
      <div class="col-12">
         <!-- FAQ's -->
         <div id="faq-accordion" class="faq-accordion">
            <div class="row">
              
			   <!-- FAQ Accordion for DeskTop -->
               <div class="col-12 col-md-12 col-lg-12 d-none d-lg-block">				
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <!-- FAQ 1 -->
                        <div class="card">
                           <div class="card-header" data-toggle="collapse" data-target="#collapseOne">
                              <a class="card-link">
								What do you mean by All-In-One Growth Platform?
                              </a>
                           </div>
                           <div id="collapseOne" class="collapse show" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
                                    <span class="w600">All-In-One Growth Platform </span>is an integrated marketing solution which contains all the tools and training to help you build and grow your business. These tools include, but are not limited to, landing page and website builders, email automation, video hosting and player, customer journeys/funnels, etc. 
                                    <br><br>
                                    OPPYO is the first of its kind All-In-One Growth Platform.
                                 </p>
                              </div>
                           </div>
                        </div>						
                        <!-- FAQ 3 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFour" >
                              <a class="card-link">What happens if I exceed my free plan limits?</a>
                           </div>
                           <div id="collapseFour" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									In case you have exceeded your limit of resources in the free plan you have a choice to upgrade to any of the 3 
									customized pricing plans, OPPYO Monthly, OPPYO One-Time and OPPYO 3 Installments based on your business needs. You can find the information <a href="#buynow" class="w600 here"> HERE </a>
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 5 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSeven" >
                              <a class="card-link">Can I switch my plan anytime?</a>
                           </div>
                           <div id="collapseSeven" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									OPPYO’s promise to you is that we grow when you grow! Hence, you have complete freedom to upgrade/downgrade your plans anytime you want as per your business needs. 
                                 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 7 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseNine">
                              <a class="card-link">What Payment methods do you accept?</a>
                           </div>
                           <div id="collapseNine" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 We accept all popular credit/debit cards, PayPal, online wallets, etc.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 9 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseeleven">
                              <a class="card-link">Is there any setup cost? Hidden charges?</a>
                           </div>
                           <div id="collapseeleven" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									What you see is what you get! There are no hidden charges or setup costs associated with OPPYO. 
								 </p>
                              </div>
                           </div>
                        </div> 
						<!-- FAQ 11 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsethirteen">
                              <a class="card-link">Will my data be private and safe?</a>
                           </div>
                           <div id="collapsethirteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									OPPYO uses SSL encryption and two step authentications to ensure that your data remains private and safe.
								 </p>
                              </div>
                           </div>
                        </div>  
						<!-- FAQ 13 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsefifteen">
                              <a class="card-link">Have more questions?</a>
                           </div>
                           <div id="collapsefifteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You can head to our community by <a href="https://support.dotcompal.com/hc/en-us/community/topics" target="_blank" class="p-blue-clr">clicking here</a> and ask any question that you might be having. 
								 </p>
                              </div>
                           </div>
                        </div>    
					 </div>
                     <div class="col-12 col-md-6">
                       <!-- FAQ 2 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseThree" >
                              <a class="card-link">Does OPPYO integrate with third party platforms?</a>
                           </div>
                           <div id="collapseThree" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Yes OPPYO can be integrated with more than 50+ 3rd party tools and apps.
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 4 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSix" >
                              <a class="card-link">Is OPPYO FREE plan really free for One-Time?</a>
                           </div>
                           <div id="collapseSix" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Absolutely. Once you have created your free account you will have access to your projects and creatives forever. We never break up with our Pals. 
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 6 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseEight" >
                              <a class="card-link">What’s included in my Free Plan?</a>
                           </div>
                           <div id="collapseEight" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									With the free account you get access to create 10 landing pages, Host 5 videos, community support and many more features. You can check <a href="https://www.oppyo.com/pricing" class="w600 here" target="_blank">this link </a>to see all features that are available with OPPYO free plan.
								 </p>
                              </div>
                           </div>
                        </div>
					   <!-- FAQ 8 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseten">
                              <a class="card-link">Do I need to enter my credit card info to sign up for the Free Account?</a>
                           </div>
                           <div id="collapseten" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 You do not need a credit card to sign up for the OPPYO Free Account.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 10 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsetwelve">
                              <a class="card-link">Can I pay in installments?</a>
                           </div>
                           <div id="collapsetwelve" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Currently installments are only available for the Founders' Deal Plan.
                                    <br>
                                    You're biggest savings is with the One Time Payment for just $697, but you can choose monthly and installment payments, too. 
                                    <br>
                                    You can check <a href="#buynow" class="w600 here">this link </a> for more info.
								 </p>
                              </div>
                           </div>
                        </div>  
						<!-- FAQ 12 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsefourteen">
                              <a class="card-link">What is your cancellation policy?</a>
                           </div>
                           <div id="collapsefourteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									If for some reason you are not satisfied with the service, we offer a no questions asked, 14 days money-back guarantee*.
								 </p>
                              </div>
                           </div>
                        </div>  
						</div>
                  </div>
               </div>
			   <!-- FAQ Accordion for Mobile -->
			   <div class="col-12 col-md-10 col-lg-10 mt30 mt-md0 d-lg-none">			   
				  <div class="row">
					<div class="col-12">
                        <!-- FAQ 1 -->
                        <div class="card">
                           <div class="card-header" data-toggle="collapse" data-target="#collapseOne1">
                              <a class="card-link">
							  What do you mean by all-in-one growth platform?
							  </a>
                           </div>
                           <div id="collapseOne1" class="collapse show" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
                                    <span class="w600">All-In-One Growth Platform </span>is an integrated marketing solution which contains all the tools and training to help you build and grow your business. These tools include, but are not limited to, landing page and website builders, email automation, video hosting and player, customer journeys/funnels, etc. 
                                    <br><br>
                                    OPPYO is the first of its kind All-In-One Growth Platform.
                                 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 2-->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseTwo2" >
                              <a class="card-link">Does OPPYO integrate with third party platforms?</a>
                           </div>
                           <div id="collapseTwo2" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Yes OPPYO can be integrated with more than 50+ 3rd party tools and apps.
								 </p>
                              </div>
                           </div>
                        </div>						
						<!-- FAQ 3 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFour4" >
                              <a class="card-link">What happens if I exceed my free plan limits?</a>
                           </div>
                           <div id="collapseFour4" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									In case you have exceeded your limit of resources in the free plan you have a choice to upgrade to any of the 3 
									customized pricing plans, OPPYO Monthly, OPPYO One-Time and OPPYO 3 Installments based on your business needs. You can find the information <a href="#buynow" class="w600 here"> HERE </a>
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 4 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFive5" >
                              <a class="card-link">Is OPPYO FREE plan really free for One-Time?</a>
                           </div>
                           <div id="collapseFive5" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Absolutely. Once you have created your free account you will have access to your projects and creatives forever. We never break up with our Pals. 
								 </p>
                              </div>
                           </div>
                        </div>
						 <!-- FAQ 5 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSix6">
                              <a class="card-link">Can I switch my plan anytime?</a>
                           </div>
                           <div id="collapseSix6" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 OPPYO’s promise to you is that we grow when you grow! Hence, you have complete freedom to upgrade/downgrade your plans anytime you want as per your business needs.
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 6 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSeven7" >
                              <a class="card-link">What’s included in my Free Plan?</a>
                           </div>
                           <div id="collapseSeven7" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									With the free account you get access to create 10 landing pages, Host 5 videos, community support and many more features. You can check <a href="https://www.oppyo.com/pricing" class="w600 here" target="_blank">this link </a>to see all features that are available with OPPYO free plan.
								 </p>
                              </div>
                           </div>
                        </div>						
                        <!-- FAQ 7 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseNine9" >
                              <a class="card-link">What Payment methods do you accept?</a>
                           </div>
                           <div id="collapseNine9" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									We accept all popular credit/debit cards, PayPal, online wallets, etc.
								 </p>
                              </div>
                           </div>
                        </div> 
						<!-- FAQ 8 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse8" >
                              <a class="card-link">Do I need to enter my credit card info to sign up for the Free Account?</a>
                           </div>
                           <div id="collapse8" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You do not need a credit card to sign up for the OPPYO Free Account.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 9 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse9" >
                              <a class="card-link">Is there any setup cost? Hidden charges?</a>
                           </div>
                           <div id="collapse9" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									What you see is what you get! There are no hidden charges or setup costs associated with OPPYO.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 10 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse10" >
                              <a class="card-link">Can I pay in instalments?</a>
                           </div>
                           <div id="collapse10" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Currently installments are only available for the Founders' Deal Plan.
                                    <br>
                                    You're biggest savings is with the One Time Payment for just $697, but you can choose monthly and installment payments, too. 
                                    <br>
                                    You can check <a href="#buynow" class="w600 here">this link </a> for more info.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 11 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse11" >
                              <a class="card-link">Will my data be private and safe?</a>
                           </div>
                           <div id="collapse11" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                <p class="description">
									OPPYO uses SSL encryption and two step authentications to ensure that your data remains private and safe.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 12 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse12">
                              <a class="card-link">What is your cancellation policy?</a>
                           </div>
                           <div id="collapse12" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									If for some reason you are not satisfied with the service, we offer a no questions asked, 14 days money-back guarantee*.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 13 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse13">
                              <a class="card-link">Have more questions?</a>
                           </div>
                           <div id="collapse13" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You can head to our community by <a href="https://support.dotcompal.com/hc/en-us/community/topics" target="_blank" class="p-blue-clr">clicking here</a> and ask any question that you might be having. 
								 </p>
                              </div>
                           </div>
                        </div>
                     </div>
				  </div>
			   </div>
            </div>
         </div>
      </div>
   </div>
</div>
</section>
<!-- Frequently Asked Questions Section end-->
<!-- Ready To Grow Section -->
<section class="support-section">
<div class="container container-1170">
	<div class="row text-center">
		<div class="col-12">
			<h1 class="support-section-heading">Are you ready to start online and grow?</h1>
		</div>
		<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md25">
			<!-- Button -->
			<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO One-Time</a>
		</div>
		<div class="col-12 mt15 mt-md20">
			<p class="f-md-16 f-14 lh140 w400">This Is WebGenie Customers' Only Exclusive Founder Special Limited Time Offer! Grab It Now!</p>
		</div>
		 <div class="col-12 mt10 mt-md15">
				<a href="https://warriorplus.com/o/nothanks/zm2f47"  target="_blank" class="black-clr">No Thank</a>
			</div>
<br><br>
<script  type="text/javascript" src="https://warriorplus.com/o2/disclaimer/f7zqn7" defer></script><div class="wplus_spdisclaimer f-16 w300 mt20 lh140  text-center"> </div>
		</div>
</div>
</section>
<!-- Ready To Grow Section end-->

<!-- OPPYO Footer Include File-->
<?php include 'includes/footer.php'; ?>
<!-- OPPYO Footer Include File end -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="WebGenie Free Gift">
    <meta name="description" content="WebGenie Free Gift">
    <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.webgenie.live/fre-git/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WebGenie Free Gift">
    <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.webgenie.live/fre-git/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WebGenie Free Gift">
    <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="twitter:image" content="https://www.webgenie.live/fre-git/thumbnail.png">

	<title>WebGenie Free Gift</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" type="text/css" href="assets/css/style.css">
   <script src="../common_assets/js/jquery.min.js"></script>

	
</head>
<body>

	<div class="proudly-sec" id="product">
         <div class="container">
         <div class="row">
                <div class="col-12 text-center">
                    <div class="col-12 white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 red-gradient text-shadow">Pranshu Gupta's</span> for SuperVIP Customers
                    </div>
                    <div class="uy col-12 mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <b class="f-20 f-md-24 red-gradient text-shadow">Congratulations!</b> you have also <b><span class="red-gradient text-shadow">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "PRANSHUWEB" $5 OFF</span></b> for my Lightning Fast Software <b><span class="red-gradient text-shadow">"WebGenie"</span></b> launch on <br class="d-none d-md-block"> <span class="red-gradient text-shadow w700">21st May 2023  @ 11:00 AM EST</span>
                        </div>

                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>

                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com/hc/en-us" target="_blank">https://support.bizomart.com/hc/en-us</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
               
               <div class="col-12 mt-md30 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                        .cls-1 {fill: #fff;}
                        .cls-2 {fill: #fc8548;}
                        .cls-3 {fill: #15c6fc;}
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
               </div>
               <div class="col-12 mt30 mt-md50 white-clr text-center">
                   	<div class="col-12 f-md-50 f-28 w700 text-center white-clr lh150">                
				   		World's First A.I Bot Builds Us <span class="red-gradient text-shadow"> DFY Websites Prefilled With Smoking Hot Content… </span>
                    </div>
                   	<div class="f-md-22 f-18 w400 text-center lh150 mt30 white-clr">
					    Instantly Leverage $1.3 Trillion Platform With A Click Of A Button… <br class="d-none d-md-block">
						Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
                   	</div>
                   	<div class="f-md-45 f-28 lh160 w700 mt30 ">
                       <span class="white-clr">Download</span> Your <span class="white-clr">Free Gifts</span> Below
                   	</div>
                   	<div class="mt10 f-20 f-md-26 lh160 ">
                       <span class="tde">Enjoy Your Extra Bonus Below</span>
                   	</div>
               	</div>
               	<div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
               	</div>
            </div>
         </div>
      
	<!-- Proudly Section End -->


	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">Simple Social Expandable </div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							This Wordpress plugin adds social network buttons and five social sharing buttons such Facebook, Google Plus, Twitter, and Pinterest to your WP blog!
							<br><br>
							Simple Social Expandable adds social network buttons, five social sharing buttons, such as: Facebook 'Like it!', Google plus on '+1', Twitter share and Pinterest Pin it.	
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/1Hq3RsSCSSGC1CYT-seWzvA-KP9XcwKq9/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
					   <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1 mt20 mt-md0">
							  Turbo GIF Animator
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  Make Animated Images in Less Than a Minute!<br>
							  If you are digital marketer, chances are you may already had use graphics in promoting your products or services online.
							  <br><br>
							  The thing is that, one of the best type of images that engage more viewers in social media is the images that are moving or simply an animated images in GIF format.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://drive.google.com/file/d/1-wC81hW9Z7RBglrDbrOefHLrLU0hbsBC/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Leadership Strategies
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  <b>Everyone has had the experience of working for a not-so-great boss.</b><br><br>
							  While it’s common for people to be promoted into management when they excel in non-leadership positions, the truth is that a lot of the people who get those promotions don’t have leadership skills they need to effectively manage their team
								<br>
								However, those skills can be easily learn.
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/1g3-ECrWsrWQrJlodGHw9jCbcYe5aGUBb/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Shopify Traffic 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.
								  <br><br>
							  With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you have little budget, you can start an online shop on a small scale.
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://drive.google.com/file/d/1HNVBU25OAbrd9k6OMOlq9H_FJduQ9WVl/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Consulting Wizardy 
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							If you’re looking to make some fast cash, or you’re interested in building a long-term sustainable business, consulting is one of the most lucrative opportunities available online.
							<br>
							The dictionary defines consultant as: “a person who provides expert advice professionally.”  
							<br><br>
							As a consultant or coach, you’ll be responsible for guiding your students or clients through a learning curve until they’ve accomplished a specific goal. 
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://drive.google.com/file/d/1Inc6plbk415o7nc-3xXoItwa9tCW2oNO/view?usp=share_link " target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->
<div class="container">
   <div class="row">
               
               <div class="col-12 mt-md30 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                        .cls-1 {fill: #fff;}
                        .cls-2 {fill: #fc8548;}
                        .cls-3 {fill: #15c6fc;}
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
               </div>
               <div class="col-12 mt30 mt-md50 white-clr text-center">
                   
                   <div class="col-12 f-md-50 f-28 w700 text-center white-clr lh150">                
				   World's First A.I Bot Builds Us <span class="red-gradient text-shadow"> DFY Websites Prefilled With Smoking Hot Content… </span>
                    </div>
                   <div class="f-md-22 f-18 w400 text-center lh150 mt30 white-clr">
				   Instantly Leverage $1.3 Trillion Platform With A Click Of A Button… <br class="d-none d-md-block"> 
				   Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
                   </div>
                   <div class="f-md-45 f-28 lh160 w700 mt30 ">
                       <span class="white-clr">Download</span> Your <span class="white-clr">Free Gifts</span> Below
                   </div>
                   <div class="mt10 f-20 f-md-26 lh160 ">
                       <span class="tde">Enjoy Your Extra Bonus Below</span>
                   </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
               </div>
            </div>
            </div>
	<!-- Bonus #6 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Digital Empire 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  If you’ve been looking for a fast and easy way to build a profitable online business, look no further than digital products.
							<br><br>
							Digital products are not only easy to create, but they can be easily replicated and turned into multiple product lines that fuel an unlimited number of sales funnels. 
							<br><br>
							They’re also consistently in demand, easy to deliver, and if you never want to write a line of content yourself, you can affordably outsource everything!
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/1T0rABQiisyWkSCstdL6eIEcE7SOcy3Bq/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Google Ads Mastery
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							Google AdWords is one of the most powerful advertising tools ever created. It deals with millions of searches by internet users every day.
							<br><br>
							It gives business owners a unique opportunity to convert many of these people into business leads and customers.
							<br><br>
							This guide will show you EXACTLY how to set up your Google AdWords campaigns properly, so you get more clicks, cheaper clicks, slash your ad costs, and maximize your return on investment (ROI).
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/1uq2ukF-oICSI9lYvMZIl335LekmUidTC/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							  Content Spin Bot 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  Revolutionary New Software Creates Unique Articles For Better Search Engine Rankings, More Vistors And Ultimately Make Money Online!
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/1JrpZCm16fmC7NK7GnbbiclnV-_JIIN97/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
					</div>
				</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
						<div class="col-md-5 col-12">
							<img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
						</div>
						<div class="col-md-7 col-12 mt20 mt-md0">
							<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1">
							Sales Bot Generator 
							</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							Unleash These 24 Hour Online Sales Agents & Immediately Skyrocket Conversions, Sales & Profits!
							<br><br>
							Sales Bot Generator is a simple little script you can use to set up your personal exit traffic salesmen. As soon as they get a sense someone is about to abandon your site they jump into action, giving your visitors a second chance to interact with your site and take action on your offers. You can generate unlimited such salesmen and put them on any webpage you want. Simply add one block of code to your webpage and your salesman is ready to go to work. This is extremely easy to do, only takes a couple of minutes and full directions are provided.
							</ul>
                     	<div class="f-md-22 w700 lh160 d-btn">
                     		<a href="https://drive.google.com/file/d/1YAm29oRZ_FBeEThoVTQKpyzFU4uCrq-E/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
						</div>
					</div>
				</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color text-shadow-1 ">
							  RPI Check Software
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
							  Ranking Videos In YouTube And Google Has Evolved But Most Video Marketers Have NOT Evolved With It! In Fact, Most Marketers Are Doing It All Wrong!
                        <br>
                        Video marketing has been proven to be one of the best way to generate money on the internet. As for many bloggers and internet marketers, video marketing is also one of the best channel to attract traffic to their websites.
                        <br><br>
                        The thing is that the internet evolved so fast as well as the technology and the principles. And if you don't go down with the flow of the latest technologies, surely we will get left behind.
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://drive.google.com/file/d/18UjfxwlhGAMjty7S9VMBsqAXriUu2XeI/view?usp=share_link" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div></div>
	<!-- Bonus #10 End -->

	<!-- Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                        .cls-1 {fill: #fff;}
                        .cls-2 {fill: #fc8548;}
                        .cls-3 {fill: #15c6fc;}
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
						<br><br><br>
               </div>
			   <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WebGenie</div>

                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webgenie.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->


</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
   <title>WebGenie Special</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="WebGenie Special">
   <meta name="description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta name="keywords" content="WebGenie Special">
   <meta property="og:image" content="https://www.webgenie.live/special/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Pranshu Gupta">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WebGenie Special">
   <meta property="og:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta property="og:image" content="https://www.webgenie.live/special/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WebGenie Special">
   <meta property="twitter:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content">
   <meta property="twitter:image" content="https://www.webgenie.live/special/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/preview/css/style.css" type="text/css" />
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/preview/css/timer.css" type="text/css" />
   <script src="https://cdn.oppyotest.com/launches/webgenie/common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
   <script>
      (function(w, i, d, g, e, t, s) {
         if (window.businessDomain != undefined) {
            console.log("Your page have duplicate embed code. Please check it.");
            return false;
         }
         businessDomain = 'webgenie';
         allowedDomain = 'webgenie.live';
         if (!window.location.hostname.includes(allowedDomain)) {
            console.log("Your page have not authorized. Please check it.");
            return false;
         }
         console.log("Your script is ready...");
         w[d] = w[d] || [];
         t = i.createElement(g);
         t.async = 1;
         t.src = e;
         s = i.getElementsByTagName(g)[0];
         s.parentNode.insertBefore(t, s);
      })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
   </script>
   <!-- New Timer  Start-->
   <?php
         $date = 'May 22 2024 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         //echo $now."<br>";
         //echo $exp_date."<br>";
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
   
</head>

<body>
    
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-3 text-md-left text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"/>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"/>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"/>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"/>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"/>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"/>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"/>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"/>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"/>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            <div class="col-md-7">
               <ul class="leader-ul f-md-20 f-18 w500 white-clr text-md-end text-center">
                  <li><a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                  <li><a href="#demo" class="white-clr t-decoration-none">Demo</a><span class="pl9 white-clr d-md-none">|</span></li>
                  <li class="d-md-none"><a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="white-clr t-decoration-none">Buy Now</a></li>
               </ul>
            </div>
            <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block">
               <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v">Buy Now</a>
            </div>
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                  We Exploited ChatGPT4 To Create A Better And Smarter AI Model...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot <span class="orange-gradient"> Builds Us DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $578.34 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="f-18 f-md-20 w600 lh140 text-center white-clr blue-design">
                     Watch How We Deploy Profitable AI-Managed Websites That Makes Us Over $17,349 Monthly In Less Than 30 Seconds…
                     </div>
                     <div class="col-12 mt20 mt-md30">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://webgenie.oppyo.com/video/embed/9xupab3svh" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="border-dashed">
                           <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                              First 99 Action Taker Get Instant Access To Our Top 5 <br class="d-none d-md-block"> Profitable DFY Websites For <span class="orange-gradient underline w700">FREE</span> 
                           </div>
                           <div class="f-18 f-md-20 w500 lh140 text-center yellow-clr">
                              (Average User Saw 475% Increase In Profit <span class="w700">worth $1,997</span>)
                           </div>
                        </div>
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20 mt-md30">
                           Get WebGenie For Low One Time Fee… <br class="d-none d-md-block"> Just Pay Once Instead Of <s>Monthly</s> 
                        </div>
                        <div class="row">
                           <div class="col-md-12 mx-auto col-12 mt20 mt-md30 text-center ">
                              <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn px0 d-block">>>> Get Instant Access To WebGenie >>> </a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp" alt="Compaitable-Gurantee1" class="img-fluid d-block mx-auto " >
                        </div>
                     </div>

                     <div class="col-12 mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li><span class="w600">100% Of Beta Testers Made At Least $100 Within 24 Hours Of Using WebGenie.</li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                           <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li>Get Up And Running In 30 Seconds Or Less </li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Step Section Start -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to WebGenie App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step1.gif " class="img-fluid d-block mx-auto" alt="Step 1" style="border-radius:20px;     border: 1px solid #15c6fc;">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish & Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Step Sction End -->

   <!-- Cta Section -->
   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">>>> Get Instant Access To WebGenie  <<<</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0 " alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0 " alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Cta Section End -->


   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
                  WebGenie Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with WebGenie… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     WebGenie eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With WebGenie you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because WebGenie is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using WebGenie
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->


   <!-- Cta Section -->
   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">>>> Get Instant Access To WebGenie  <<<</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto " alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Cta Section End -->

   <!--Testimonial Section Start -->
   <div class="testimonial-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Join Hundreds Of Users Who Uses WebGenie Daily To Get FAST RESULTS
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
               (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/rodney-paul.webp" class="img-fluid d-block mx-auto " alt="Rodney Paul">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Rodney Paul </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I created many websites before. So I’m not new to the concept… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     But damn, this app is something else
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I used to spend a few days to get the website ready… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And then hire a content writer
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I can’t believe that WebGenie does all of that in less than a minute
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/alex-holland.webp" class="img-fluid d-block mx-auto " alt="Alex Holland">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Alex Holland  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     You guys created a beast. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I now have 12 websites up and running
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     Each one of them is making me money now 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And im planning to double those sites over the weekend
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     After all, it takes me what? 5 minutes 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                    But i make so much more money now
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/tom-garfield.webp" class="img-fluid d-block mx-auto " alt="Tom Garfield">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Tom Garfield  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     This is by far the easiest and most reliable AI app that i ever bought 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And i have quite a few. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     Thanks guys for allowing me to be one of the beta testers
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Stuart Johnson">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Stuart Johnson  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  I started my freelance website developer journey 4 months back, but being a newbie, I was struggling in creating a good website. But thanks for the early access to WEBPULL team, now <span class="w600">
                  I can make a stunning, highly professional website quick & easy and able to serve more clients who happily pay me $4000 - $5000 each.
                  </span>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div> 
   <!--Testimonial Section End -->

   <!--Are You Ready Section Start -->
   <div class="areyouready-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Are You Ready For An A.I. Revolution?
            </div>
            <div class="col-12 col-md-7 mx-auto f-28 f-md-38 lh140 w600 text-center white-clr red-brush mt20">
               ChatGPT4 And AI Pay Us Daily…
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               There is nothing that AI can't do in the digital world… It's the most powerful technology that exists now… And WebGenie is the only app on the market…<br><br>
               <span class="w600 lh120">
               That harnessed that power, and created something that makes us money day after day
            </span>
            </div>
            <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof1.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               Every day we make money without doing any work… And not pocket change…<br><br>
               <span class="w600 lh120">
               Last month alone we made a bit over $17,000 with WebGenie
               </span>
            </div>
            <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof2.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Are You Ready Section Ends -->


   <!--Profitable Section Start -->
   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="easy-text f-28 f-md-65 w700 lh140 white-clr mx-auto">Easy</div>
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center mt20">
                  We Created <u><span class="w600">PROFITABLE</span></u> Websites…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  It's pretty straightforward…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We have dozens of websites across multiple niches…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All of them work to make us money…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               The best part is, we don't have to do anything
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't promote, run ads, create content… or any of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They work on complete autopilot to make us money like this…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-ss.webp" alt="screenshort" class="mx-auto d-block img-fluid mt20 "><br>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if you think creating a website is “old news”
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Let me explain something, my friend…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-img.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Profitable Section Ends -->

   <!--Don't Have a Website Section Start -->
   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  If You Don't Have A Website,<br>
                  <span class="red-clr w700">
                     You Can't Make Money!
                  </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md40">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
                  It's very simple…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If you don't have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You are going to face a very hard time trying to make money online…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  I mean, that's the idea of the Internet to start with…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  To have websites, and users can view it, and use it…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  When was the last time you saw a successful business…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or even an online guru…
               </div>
               <div class="f-18 f-md-20 w600 lh140 mt20 black-clr text-center text-md-start">
                  Without a website?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Every one of them MUST have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even we…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You're reading this letter now on a website we own…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/website-img.webp" alt="Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
      </div>
   <!--Don't Have a Website Section Ends -->


   <!--Take a look Section Start -->
   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center mt20">
                  I Mean… Take A Look At This…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
                  There are about 1.13 Billion WEBSITE ON THE INTERNET IN 2023
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  According to Forbes.com…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               There is over A BILLION website on the internet…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Why do you think there are that many?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               There must be a reason, <span class="w600">Right? </span> 
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               And there is no sign of slowing down…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Because according to Forbes also…
               </div>
            </div>
            <div class="col-12 col-md-6">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/billion-website.webp" alt="Billion Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/website-internet.webp" alt="Website Internet" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
               A new website built every three seconds
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Every 3 seconds… can you believe it?
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Finally…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               If you think that websites are not essential to make money…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Take a look at this…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/new-website.webp" alt="New Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/website-buils.webp" alt="Website Buils" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="col-12 mt20 mt-md50 text-center">
            <div class="f-18 f-md-38 w600 lh140 white-clr text-center red-head">
               71% of business have a  website in 2023
            </div>
         </div>
      </div>
   </div>
   <!--Take a look Section Ends -->

   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->
   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  But… Creating A Website Is <span class="red-clr w700">NOT Easy!</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  I'm sure you have tried it before…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or at least have a general idea of how it works…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If not, let me tell you…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  It's a very difficult, and demanding task…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Here, let me break it down for you…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/not-easy-img.webp" alt="Not Easy" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Ends -->

   <!-- Requires Section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 w400 lh140 black-clr text-center text-md-start">
               It Requires <span class="w700">HUGE Techincal Knowledge…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There is a reason why professionals can charge you up to $20,000 for a website creation…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It requires years of experience…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Knowledge of multiple programming languages…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And a vast knowledge of server management…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And to the average guy like you and me…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's very… VERY complicated… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's why many people try to start a website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And abandon it midway…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or worse, create a horrible, slow, and non-functional website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/knowledge-img.webp" alt="Knowledge" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Requires Section Ends -->


   <!-- Content Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
               And If That Wasn't Enough… <span class="w600">You Need Content For That Website…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               I'm sure you have tried it before…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Or at least have a general idea of how it works…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               If not, let me tell you…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               It's a very difficult, and demanding task…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Here, let me break it down for you…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-img.webp" alt="Content" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Content Section Ends -->    
    
    <!-- Expensive Section Starts -->
   <div class="blue-section">
      <div class="container">
      <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
                     Creating Content Is <span class="red-clr w700">EXPENSIVE…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               A successful website needs high-quality content…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And let me tell you this…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               A quality article will cost you at least $100 to get done…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So imagine if you wanna create a website with just 100 articles (which is very low)
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's not counting
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               &#x2022; &nbsp;&nbsp;   Proofreading<br>
               &#x2022; &nbsp;&nbsp;   Editing<br>
               &#x2022; &nbsp;&nbsp;   Formatting<br>
               &#x2022; &nbsp;&nbsp;   Designing<br>
               &#x2022; &nbsp;&nbsp;   Publishing<br>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And still, that's for just one batch of articles…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The same for videos, pictures, infographics, and even podcasts…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/expensive-img.webp" alt="Expensive" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Requires Section Ends -->


   <!-- Its Even Harder Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
               And If You Decided To Do Yourself…
               </div>
               <div class="f-18 f-md-50 w700 lh140 black-clr text-center text-md-start">
               It's Even Harder…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt10 black-clr text-center text-md-start">
               You will need to do tons of research…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               And spend hours each day for months…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Writing, editing, and formatting…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               And all of that is just for one website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Imagine if you wanna build maybe 10?
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               It's just not feasible…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/its-even-harder-img.webp" alt="Even Harder" class="mx-auto d-block img-fluid ">
               </div>
         </div>
      </div>
   </div>
<!-- Its Even Harder Section Ends -->



<!-- You Still Need Traffic Section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-7">
               <div class="f-18 f-md-28 w600 lh140 black-clr text-center text-md-start">
               And After All Of That…
               </div>
               <div class="f-18 f-md-50 w700 lh140 black-clr text-center text-md-start">
               You Still Need Traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If somehow you managed to do all of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You will still need to spend 10x more that time and money...
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To get traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Doesn't matter if you're going for free or paid traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The paid one will cost you a fortune to just test it…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You will need at least $10,000 set aside for that purpose…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And who knows if its going to work or not…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And the free methods like SEO are no joy either…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It takes months (sometimes years) to see any significant results from it…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/need-traffic-img.webp" alt="Need Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- You Still Need Traffic Section Ends -->

   <!-- Beginner Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-38 w700 lh140 mt20 black-clr text-center text-md-start">
               It’s Just Not Easy For A Beginner…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               There are a lot of moving parts…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               A lot of skills that you need to learn first…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               I mean, yea it's still wildly profile…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               But need a lot of dedication and hard work…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/beginner-img.webp" alt="Beginner" class="mx-auto d-block img-fluid ">
               </div>
         </div>
      </div>
   </div>
   <!-- Beginner Section Ends -->

   <!-- Another Way Section Starts -->
   <div class="another-way-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 white-clr text-center">
                  But What If I Told You <span class="orange-gradient w600">There Is Another Way…?</span>
               </div>
               <div class="f-18 f-md-26 w400 lh140 white-clr text-center">
                  A way that will let you skip all of that, A way that will put you in front of everyone else…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="imagine-box">
               <div class="row align-items-center">
                  <div class="col-12 col-md-4">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/imagine-man.webp" alt="Imagine Man" class="mx-auto d-block img-fluid ">
                  </div>
                  <div class="col-12 col-md-8">
                     <div class="f-22 f-md-32 w400 lh140 black-clr text-center text-md-start">
                        Imagine if you can do all of what we talked about above…
                     </div>
                     <div class="clicks-box mt20">
                        <div class="f-22 f-md-38 w700 lh140 text-center orange-gradient click">
                           But with just 3 clicks…
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <div class="f-28 f-md-50 w600 lh140 white-clr text-center">
                  That’s it
               </div>
               <div class="f-18 f-md-26 w400 lh140 white-clr text-center ">
                  And it makes us daily commissions like this
               </div>
               <div class="arrow-down-2 mt20 mt-md40">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof3.webp" alt="Proof" class="mx-auto d-block img-fluid ">
               </div>
               <div class="arrow-down-3">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof4.webp" alt="Proof" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Another Way Section Ends -->


   <!-- Work For You Section Starts -->
   <div class="workforyou-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  A Way That Does 99% Of The Work For You
               </div>
               <div class="f-18 f-md-38 w400 lh140 black-clr text-center">
                  With WebGenie your work is done for you by AI
               </div>
               <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20 mt-md50">
                  You don’t need to:
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dont-need1.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Create or setup any website
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dont-need2.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Write any content
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dont-need3.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 " >
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Design anything
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dont-need4.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                  Run ads or work on SEO
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12 text-center mt20 mt-md50">
               <div class="done-text f-28 f-md-50 w700 lh140 black-clr mx-auto">All of that is done for you…</div>
                  <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                     Every day we use WebGenie to make money like this:
                  </div>
                  <div class="proof-arrow mt20 mt-md40">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof5.webp" alt="No Content Writing" class="img-fluid ">
                  </div>
                  <div class="col-12">
                     <div class="f-28 f-md-42 w600 lh140 black-clr text-center mt20">
                        WITHOUT doing any of the work…
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <!-- Work For You Section Ends -->

   <!-- Got You Covered Start -->
   <div class="got-you-covered-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
               Even If You Have Nothing To Sell, <span class="w600">WebGenie Got You Covered…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Doesn’t matter if you have a product or not…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Because WebGenie comes with countless DFY offers…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That you can start using within seconds…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               And earn up to $997 per sale… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Without waiting for approval…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Paid directly to your bank account…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/got-you-covered-img.webp" alt="Got Covered" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Got You Covered Ends -->

   <!-- Works Globally Section Starts -->
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-38 w400 lh140 mt20 black-clr text-center text-md-start">
                  It Doesn’t Matter Where You Live, <span class="w600">WebGenie Works Globally…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Doesn’t matter if you’re in the US, India, Or in the middle of the desert…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  As long as you have a working internet connection…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  You can use <span class="w600">WebGenie</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  And create dozens of profitable websites…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/works-globally-img.webp" alt="Work Globally" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Works Globally Section Ends -->

   <!-- With Few Clicks Section Starts -->
   <div class="with-few-clicks-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
               Imagine Creating A Stunning And Profitable Website <span class="w700"> With A Few Clicks…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
               That’s all it takes…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               A few clicks, nothing more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We eliminated the need for any kind of technical experience…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we also eliminated the need to hire or buy any external help…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With our fool-proof system…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And one can create a website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/few-clicks-img.webp" alt="Few Clicks" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- With Few Clicks Section Ends -->


   <!-- Dominates Section Starts -->
   <div class="dominates-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  WebGenie Allowed Us Dominate The Entire WORLD Market…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  Here is what makes WebGenie really powerful… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  It doesn’t only work with English content or English markets…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  WebGenie is capable of creating and operating <span class="w600">websites in 28 different languages…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  That way, you can dominate any market in any country…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  And since WebGenie will do everything for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  You don’t even need to speak that language.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dominate-img.webp" alt="Dominate" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Dominates Section Ends -->


   <!-- AI Agent section Starts -->
   <div class="blue-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 w400 lh140 black-clr text-center text-md-start">
               WebGenie Will Be <br><span class="w700 f-md-50">Your A.I. Agent Too…</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On each website… WebGenie will have a customer chat box…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Where your viewer can chat and engage with a ChatGPT4-like bot…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To get help on anything they might need in your market…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               This alone is worth its weight in gold…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ai-agent-img.webp" alt="AI Agent" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- AI Agent section Starts -->

<!-- Zero Monthly Fees Setion Starts -->
  <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-38 w600 lh140 black-clr text-center text-md-start">
                  ZERO Monthly Fees…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Paying monthly for an app isn’t fair…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  Especially for someone who is just starting out…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  This is why we decided to remove that with WebGenie
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  We are offering you the full power of AI with WebGenie
               </div>
               <div class="f-18 f-md-22 w600 lh140 mt20 black-clr text-center text-md-start">
                  For a small one-time fee…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/zero-monthly-img.webp" alt="Zero Monthly" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Zero Monthly Fees Setion Ends -->


   <!-- Webgenie In Action Section Starts -->
   <div class="in-action-section" id="demo">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 white-clr text-center">
               Watch WebGenie In Action, And See What It Can Do For You…
               </div>
               <div class="mt20 mt-md50">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blue-arrow.webp" alt="Product Box" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-md-10 col-12 mx-auto mt20 mt-md50">
                     <div class="col-12">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://webgenie.oppyo.com/video/embed/3zg7sd8966" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20 mt-md30">
                           Get WebGenie For Low One Time Fee…
                        </div>
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
                        </div>
                        <div class="row">
                           <div class="col-md-12 mx-auto col-12 mt20 mt-md30 text-center ">
                              <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn px0 d-block">&gt;&gt;&gt; Get Instant Access To WebGenie &gt;&gt;&gt; </a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee1" class="img-fluid d-block mx-auto ">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   <!-- Webgenie In Action Section Ends -->

   <!-- Regardless Section Start -->
   <div class="regardless-experience">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  WebGenie Is For Everyone! <br class="d-none d-md-block">
                  Regardless Of Your Experience…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  It doesn’t matter if you have made money online before or not…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It doesn’t matter if you know how a website is made or not…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  As long as you can follow fool-proof instructions…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And can click 3 clicks with your mouse…
               </div>
               <div class="f-20 f-md22 w600 lh140 blue-clr mt20">
                  You’re all set…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/regardless-man.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Regardless Section End -->

   <!-- Hey There Section Start-->
   <div class="hi-their-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart Team
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It’s <span class="w600">Pranshu</span> along with <span class="w600">Bizomart Team.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I’m a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It’s the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change… 
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>

         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day, I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I’m not telling you this to brag… Not at all… I’m telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Hey There Section End -->

   <div class="goal-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  You see…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Making money online is good any everything… 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  But what’s 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 <span class="w600">I will help 100 new people…</span> 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  So far, it’s been going GREAT…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  I’ve managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/goal.webp" alt="Women" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="success-story-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I’m Here To Help You My Next <br class="d-none d-md-block">
                  Success Story…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  Listen, if you’re reading this page right now…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It means one thing, you’re dedicated to making money online…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members… 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/successful-man.webp" alt="SuccessFull Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- Demo Section Start -->
   <div class="demo-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-12 mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using WebGenie</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  We turn it on, and that’s pretty much it. As simple as that…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline">  $27,548.65 on one of our affiliate accounts…</span> 
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/arrow-down-3.webp" alt="Demo Proof" class="d-block img-fluid  mt20 ">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid  ">
            </div>
         </div>
      </div>
   </div>
   <!-- Demo Section End -->

   <!--Proudly Introducing Start -->
   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-38 w700 cyan-clr lh140 presenting-head">
                  Proudly Presenting…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 120px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"/>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"/>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"/>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"/>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"/>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"/>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"/>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"/>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"/>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            <div class="text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
               Tap into $1.3 Trillion Platform In 3 Steps…
               </div>
            </div>
            <div class="f-md-50 f-28 w700 white-clr lh140 col-12 mt20 mt-md30 text-center">
               World's AI App That Builds DFY AI Websites For Us And Monetizes It On Autopilot
            </div>
            <div class="col-12 mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Makes Us $578.34 Per Day…
               </div> 
            </div>
            <div class="f-22 f-md-28 lh140 w700 text-center white-clr mt20 mt-md30">
               Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-10 mx-auto">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-box.webp" class="img-fluid d-block mx-auto " alt="Product">
            </div>
         </div>
      </div>
   </div>
   <!--Proudly Introducing End -->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Here Is What <span class="orange-line">WebGenie Is Capable Of Doing…</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Fully Functional And Stunning Website With A Click
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about hiring web designers and paying thousands of dollars in fees…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With WebGenie you get all of that done for you with just a click…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  All you need is to choose your niche, and you are good to go.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/stunning-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Human-Like AI Content
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you generate, will be automatically filled with UNIQUE and most importantly human-like content… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This content is generated by the power of AI to do one thing… 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/human-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate All Content In 28 Languages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Localize all your content automatically in 28 different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  WebGenie will automatically translate all content into different languages…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  So it doesn't matter what country you are trying to target, WebGenie got you covered.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-languages.webp" alt="Content Language" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/attention-sucking.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI Auto Comment On Post & Pages
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Let AI engage with each comment you get on your behalf
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With this feature, visitors will get instant answer for their queries
               </div>
             <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Driving more traffic and more engagement to your site
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/optimize-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Automatically Publish Content From ChatGPT
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  WebGenie does everything for you on autopilot…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Even publishing your content…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It will automatically do all the work for you with ChatGPT4…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And publish the content according to your schedule…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/publish-content.mp4" type="video/mp4">
               </video>
            </div>
         </div>

      </div>
   </div>

   <!-- Dfy Section Start -->
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
                  500+ DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
                  Choose between over 500 DFY stunning templates in all niches possible… 
                  With one click you will be able to change your website design, With zero downtime.
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="capability-sec2">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you create is 100% mobile optimized
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  There are no glitches, no errors…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with WebGenie AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let WebGenie do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Integration With WooCommerce
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Selling physical products?
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  No problem, WebGenie integrates with wooCommerce in seconds…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And it will allow you to operate your store with no hassle…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/integration-woocommerce.webp" alt="Integration With WooCommerce" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In CRM
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Manage all your leads from one central dashboard…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  WebGenie comes with a powerful CRM that will easily do all the heavy lifting for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/crm.webp" alt="Built-In CRM" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Appointment Booking
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Does your business requires setting appointments?
               </div>
               <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">
                  Easy.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Let WebGenie does that for you on autopilot with its powerful appointment-setting features
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of WebGenie will come with a commercial license, so you can use it on your client website too.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>

      </div>
   </div>


   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="conquar-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 text-center">
                  Conquer ANY Niche…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  It doesn't matter what niche you are trying to get into…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  WebGenie will let you dominate ANY niche in any country…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  All it takes is just a few clicks…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                  And you will have a STUNNING and most importantly…
               </div>
               <div class="f-20 f-md-22 w400 mt20 text-center text-md-start">
                 <span class="w700">PROFITABLE…</span> website…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/conquer-girl.webp" alt="Conquer" class="mx-auto img-fluid d-block">
            </div>
         </div>
         <div class="regardless-sec">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w400 text-center white-clr dotted-border">
                     WebGenie Is For You…
                  </div>
                  <div class="f-28 f-md-50 w700 mt20 text-center orange-gradient">
                     Regardless Of What You Are Doing…
                  </div>
                  <div class="f-20 f-md-22 w400 lh140 text-center white-clr mt10">
                     Choose between over 500 DFY stunning templates in all niches possible… <br class="d-none d-md-block">
                     With one click you will be able to change your website design, With zero downtime.
                  </div>
                  <div class="f-20 f-md-22 w700 lh140 text-center white-clr mt10">
                     It work for…
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="no-need-setup">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr"><u> NO More</u></span>  Tech Setup…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Creating a website is a must for any business…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The issue is, it's not easy…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You need great technical knowledge in order to create a website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if you decided to hire someoen to do it for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will end up paying thousands of dollars in fees…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And even after that, it won't even be just like you wanted…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  They will charge you extra for any tiny edits you wanna do
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  But with WebGenie, You will skip all of that…
               </div>
               <ul class="cross-list pl0 m0 f-18 f-md-20 w400 black-clr lh140  mt10">
                  <li>No Website Creation</li>
                  <li>No Programming Required</li>
                  <li>No Complicated Setup</li>
                  <li>No Hiring Anyone</li>
               </ul>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Within seconds, WebGenie will create you a unique, and stunning website in any niche, in any language.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-setup.webp" alt="No Setup" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="chatgpt-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  ChatGPT4 And AI Changed All Of That…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No, all you need is AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't need anything else.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if you know how to use it, you are set to financial freedom, my friend…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  WebGenie leverages the BEST AI model known…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  To give you all that power in your hands… On complete autopilot…
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  By using it, it allowed us to turn our account from this:
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  [$0 bank account]
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20 arrow-down">
                  To this
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof.webp" alt="Proof SS" class="mx-auto d-block img-fluid mt20 mt-md24 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  In just a few days…
               </div>
               <div class="text-center text-md-start">
                  <div class="smile-text f-20 f-md-22 w400 lh140 white-clr mt20 mt-md24">Sounds good, huh?</div>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/chatgpt.webp" alt="Chatgpt" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="work-sec"> 
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span>  Does It Work?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We can spend HOURS explaining how everything inside WebGenie works…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's a complicated AI model…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But the thing is, you don't have to learn any of that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Since you are not doing anything in that process…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's all done for you on autopilot…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We spent years developing this robust AI model…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And today, we are giving it to you on a silver platter…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               WebGenie is so powerful…It gets us results <span class="w600">EVERY SINGLE DAY</span> 
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/work-men.webp" alt="Work Men" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="different-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span> Is It Different?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  That's an easy one… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  WebGenie is the only app on the market that does it all for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have to lift a finger to see results with WebGenie…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It will start with creating your website, filling it with content, promoting it… and even monetizing it for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  All of that happens in the background with <span class="w600">zero human interference…</span> 
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/different-girl.webp" alt="Different" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="powerfull-ai">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-38 w400 text-center black-clr dotted-border">
                  With WebGenie…
               </div>
               <div class="f-28 f-md-50 w700 lh140 mt11 black-clr">
               Everything Is Done For You By The Most <br class="d-none d-md-block">Powerful AI Model Ever Created…
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-websites.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Websites</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-content.webp" alt="DFY Content" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Content</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-translation.webp" alt="DFY Translation" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Translation</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-designs.webp" alt="DFY Designs" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Designs</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-seo-optimization.webp" alt="DFY SEO Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY SEO Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-mobile-optimization.webp" alt="DFY Mobile Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Mobile Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-traffic.webp" alt="DFY Traffic" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Traffic</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-social-media-syndication.webp" alt="DFY Social Media Syndication" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Social Media Syndication</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-crm.webp" alt="DFY CRM" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY CRM</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-appointment-schedule.webp" alt="DFY Appointment Schedule" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Appointment Schedule</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-templates.webp" alt="DFY Templates" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Templates</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/dfy-woocomerce-integration.webp" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY WooComerce Integration</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <!-- Step Section Start -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to WebGenie App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step1.gif " class="img-fluid d-block mx-auto" alt="Step 1" style="border-radius:20px;     border: 1px solid #15c6fc;">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish & Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Step Sction End -->

   <!-- Need Section Start -->
   <div class="need-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                 <span class="left-line">Everything You Need</span> Is Included…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  WebGenie App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  The app that is responsible for everything… 
                  Get 100% full access to our state-of-the-art AI app… 
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-app.webp" alt="WebGenie App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  AI Content Generator
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  With one click, fill your entire website with human-like content in any niche, and in over 28 languages.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ai-content-generator.webp" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  WebGenie Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  You don't have to worry about traffic any more, let AI handle that for you.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-built-in-traffic.webp" alt="WebGenie Built-In Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  WebGenie Mobile Optimizer
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  Make your website and your client's website mobile-ready with just a click…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-mobile-optimizer.webp" alt="WebGenie Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  WebGenie Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  This will allow you to also operate WebGenie, even from your mobile phone…  <br>
                  Whether it's an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $497)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-mobile-edition.webp" alt="WebGenie Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  Training videos
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  There is NOTHING missing in this training… Everything you need to know is explained in IMMENSE details.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/training-videos.webp" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  Have a question? Just reach out to us and our team will do their best to fix your problem in no time.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/world-class-support.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Need Section End -->

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="get-better-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr red-line">
               But It Gets Better…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  By getting access to WebGenie
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will immediately unlock access to our custom-made bonuses…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this pack exclusively for WebGenie
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  Our goal was simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Give fast action takers the advantage…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  By giving them whatever they need to achieve 10x the results… In half the time…
               </div>
            </div>
            <div class="col-12 col-md-6">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/better-man.webp" alt="Better Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <!-- Bonus Section Start -->
   <div class="bonus-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Wordpress Automation Secrets
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Automation is the key to success for every successful marketer today. If you're able to save your time and use it for other productive tasks, then you're on the right track.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     This 8-part video course is designed to show you exactly how you can free up your time by quickly and easily automating tedious and boring tasks within your WordPress site!
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Lead Generation On Demand
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     So, to help you to build your leads and make the max from them, this comprehensive guide lays down proven tips and tricks on how you can create lead generation on demand.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Video Training on How To Create a Profitable Authority Blog in Any Niche
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Want to learn how to create a profitable authority blog in any niche that converts, then you're at the right place.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     This useful package helps to boost your authority by giving useful tricks on how to create a profitable blog the right way. Use it and scale your business to the next level.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                     Social Media Traffic Streams
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                     Social media is a popular way for businesses to engage with their target audiences. But getting people to your website through social media engagement can be tricky.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                     So, don't hang around! This stunning guide will teach you how you can successfully drive traffic from social media to your website.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus-box.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section End -->

   <div class="join-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  Join The Future… <br class="d-none d-md-block">
                  Join WebGenie
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Aren't you tired of wasting your money and time……on half-complete apps?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you are…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Then this is your chance to finally jump on something that has been proven to work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Not just for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But for <span class="w600">HUNDREDS</span> of beta-testers…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  With %100 success rate…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We're confident to say that WebGenie is the BEST app on the market right now…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/join-webgenie.webp" alt="Better Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="know-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
                  Wanna Know <span class="red-clr">How Much It Costs?</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  WebGenie does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I make enough money from using WebGenie.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So I don't need to charge you that.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I use it on a daily basis… So I don't see why not I shouldn't give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I'm willing to give you full access to WebGenie.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  For a fraction of the price.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Less than the price of a cheap dinner.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Which is enough for me to cover the cost of the servers running WebGenie.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/costs.webp" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="last-long-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start white-clr">
                  But That Won't Last Long…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20 mt-md30">
                  The last thing on earth I want is to get WebGenie saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  So, sadly I'll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  After that, I'm raising the price to <span class="w600 yellow-clr">$997 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
                  So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/money-scale.webp" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="decide-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr decide-line">
                  Can't Decide?
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/decide-line.webp" alt="Red Line" class="d-block img-fluid ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Listen…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's really simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  WebGenie is for you <span class="w600">PERIOD</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Right now, you have the option to change your life With just a few clicks
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/confused-girl.webp" alt="Confused Girl" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="get-better-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-24 f-md-38 w400 lh140 text-center black-clr">
                  We Will Pay You To Fail With WebGenie
               </div>
               <div class="f-28 f-md-45 w700 lh140 text-center black-clr mt10">
                  Our 30 Days Iron Clad <span class="need-head white-clr">Money Back Guarantee</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  We trust our app blindly…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We know it works, after all we been using it for a year… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And not just us… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But hey, I know you probably don’t know me… and you may be hesitant… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And I understand that. A bit of skepticism is always healthy… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I can help… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Here is the deal, get access to WebGenie now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Use it, and enjoy its features to the fullest… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if for any reason you don’t think WebGenie is worth its weight in gold… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just send us a message to our 24/7 customer support… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  And we will refund every single penny back to you… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No questions asked… Not just that… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We will send you $300 as a gift for wasting your time.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Worst case scenario, you get WebGenie and don’t make any money
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 <u> You will still get paid $300 for trying it out. </u>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/money-back-guarantee.webp" alt="Money Back Guarantee" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <!-- Pricing Table Start -->
   <div class="pricing-table-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  Pricing Table
               </div>
            </div>
            <div class="col-12 col-md-7 mx-auto text-center mt20 mt-md30">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with WebGenie, You’re Getting :
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>Deploy Stunning Websites by Leveraging ChatGPT - <span class="w600">Worth $997/Month</span> </li>
                        <li>AI Content Generator - Generate Top-Notch Content on Automation - <span class="w600">Worth $997</span></li>
                        <li>Fully SEO-Optimized Websites in 25+ Languages- Worth $297</li>
                        <li>WebGenie Built-In Traffic - <span class="w600">Worth $997</span></li>
                        <li>500+ Eye-Catching Done-For-You Themes - <span class="w600">Worth $997</span></li>
                        <li>Sell Unlimited Products, Services or Promote Affiliates Offers – <span class="w600">Worth $297</span></li>
                        <li>Generate Attention-Sucking Images from AI - <span class="w600">Worth $297</span></li>
                        <li>Get More Viral Traffic with Built-In Social Media Tools (Worth $297)</li>
                        <li>Live Chat from ChatGPT4 AI BOT To Engage with Your Customer (Worth $297)</li>
                        <li>100% Mobile Optimizer (Worth $497)</li>
                        <li>WebGenie Mobile EDITION (Worth $497)</li>
                        <li>Built-In CRM To Manage All Your Leads <span class="w600"> (Worth $297)</span></li>
                        <li>100% Newbie Friendly - <span class="w600"> Beyond A Value</span></li>
                        <li>Step-By-Step Training videos (Worth $997)</li>
                        <li>Round-The-Clock Support (Priceless – It’s Worth A LOT)</li>
                        <li>Commercial License - <span class="w600"> Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $7,458.34 FREE!!! </li>
                        <li>Wordpress Automation Secrets</li>
                        <li>Lead Generation On Demand</li>
                        <li>Video Training on How To Create a Profitable Authority Blog in Any Niche </li>
                        <li>Social Media Traffic Streams</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $6,795.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn mt20">Grab WebGenie Now</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Pricing Table End -->

   <!-- CTA Btn Start-->

   <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="get-better-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  Remember…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/remember-line.webp" alt="Red Line" class="d-block img-fluid">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After that, you will have to pay $997/mo for it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  While it will still be worth it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/remember.webp" alt="Remember" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="know-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  So, Are you ready yet?
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ready-line.webp" alt="Red Line" class="d-block mx-auto img-fluid mt5 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You reading this far into the page means one thing..
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  You're interested.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The bad news is, it will sell out FAST So you need to act now.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ready.webp" alt="Ready" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

      <!-- CTA Btn Start-->

      <div class="cta-section-new">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
               <div class="row align-items-center mt20 mt-md50">
                  <div class="col-md-7 mx-auto col-12 text-center ">
                     <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
                     <div class="mt20 mt-md30 ">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee">
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0">
                     <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                           <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn End-->

   <!-- Limited Time Start -->

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Limited Time End -->

   <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  We'll see you inside,
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5 ">
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart Team
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start">
                  <span class="w600">PS:</span>  If you act now, you will instantly receive [bonuses] worth over <span class="w600">$7,458.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span> There is nothing else required for you to start earning with WebGenieNo hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span>  Remember, you're protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                  We will send you <span class="red-clr">$300 out of our own pockets just for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>  If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                  And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <!-- FAQ Section Start -->
   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started?     
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $997/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to WebGenie.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, WebGenie is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried WebGenie and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of WebGenie at a one-time fee 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- FAQ Section End -->
   <!--final section-->
   <div class="final-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 text-center">
               <div class="f-45 f-md-45 lh140 w700 white-clr final-shape">
                  FINAL CALL
               </div>
               <div class="f-22 f-md-32 lh140 w600 white-clr mt20 mt-md30">
                  You Still Have the Opportunity To Raise Your Game… <br> Remember, It's Your Make-or-Break Time!
               </div>
            </div>
         </div>
         <div class="row mt20">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get WebGenie For Low One Time Fee…
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                 <span class="w700">Just Pay Once</span> Instead Of <s>Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7 mx-auto col-12 text-center ">
               <a href="https://warriorplus.com/o2/buy/f7zqn7/jj9mkz/hxwl6v" class="cta-link-btn ">&gt;&gt;&gt; Get Instant Access To WebGenie  &lt;&lt;&lt;</a>
               <div class="mt20 mt-md30 ">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/compaitable-gurantee1.webp " class="img-fluid d-block mx-auto" alt="Compaitable Gurantee1">
               </div>
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   </div>
   <!--final section-->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,458.34 Bonuses Package Expires When Timer Hits ZERO
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"/>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"/>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"/>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"/>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"/>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"/>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"/>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"/>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"/>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
               <div class="f-14 f-md-16 w500 mt20 lh150 white-clr text-center text-md-start" style="color:#7d8398;"><span class="w700">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                   <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/f7zqn7" defer></script><div class="wplus_spdisclaimer"> </div> 
               </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebGenie 2023</div>
               <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="https://warriorplus.com/o2/disclaimer/f7zqn7" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section End -->
   <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?> 
    <!-- Exit Popup and Timer End -->
   
   <!-- timer --->
   <?php
   
         if ($now < $exp_date) {
             
         ?>
      <script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-45 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-45 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-45 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
            }
         
         }


         timerBegin(59 * 60); 

      </script>
      <?php
         } else {
         //echo "Times Up";
         }
         ?>
      <!--- timer end-->
      
      <script type="text/javascript" src="https://warriorplus.com/o2/js/jj9mkz"></script>
      
</body>

</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="WebGenie Special Bonuses">
   <meta name="description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Pranshu Gupta">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WebGenie Special Bonuses">
   <meta property="og:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta property="og:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WebGenie Special Bonuses">
   <meta property="twitter:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta property="twitter:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <title>WebGenie Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'May 24 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/nsv9vx/0';
      $_GET['name'] = 'Pranshu Gupta';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }
                        
                              .cls-2 {
                                 fill: #fc8548;
                              }
                        
                              .cls-3 {
                                 fill: #15c6fc;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="pre-heading f-20 f-md-21 w500 white-clr lh140">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot Builds Us <span class="orange-gradient">DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $867.54 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               Instantly Leverage $1.3 Trillion Platform With A Click Of A Button…
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills - No Experience - No Hidden Fees - No BS
            </div>
            </div>
         </div>
         <div class="row mt20 mb-md40">
            <div class="col-12 col-md-10 mx-auto">
                <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> 
               <!-- <div class="responsive-video">
                  <iframe src="https://webpull.dotcompal.com/video/embed/26sp46ptcb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div> -->
            </div>
         </div>
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT… </li>
                           <li>All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content </li>
                           <li>Instant High Convertring Traffic For 100% Free </li>
                           <li>100% Of Beta Testers Made  At Least $100 Within 24 Hours Of Using WebGenie. </li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                           <li>We Let AI Do All The Work For Us.</li>
                           <li>Get Up And Running In 30 Seconds Or Less</li>
                           <li>Instantly Tap Into $1.3 Trillion Platform </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4 </li>
                           <li>99.99% Up Time Guaranteed </li>
                           <li>ZERO Upfront Cost</li>
                           <li>30 Days Money-Back <br class="d-none d-md-block">Guarantee</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt30 mt-md60">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black black-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-black black-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>

  
<!-- Step Section Start -->
<div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to WebGenie App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step1.gif " class="img-fluid d-block mx-auto" alt="Step 1" style="border-radius:20px;     border: 1px solid #15c6fc;">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish &amp; Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Step Sction End -->


   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   
   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
                  WebGenie Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with WebGenie… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     WebGenie eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With WebGenie you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because WebGenie is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using WebGenie
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->


     <!--Testimonial Section Start -->
     <div class="testimonial-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Join Hundreds Of Users Who Uses WebGenie Daily To Get FAST RESULTS
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
               (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/rodney-paul.webp" class="img-fluid d-block mx-auto " alt="Rodney Paul">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Rodney Paul </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I created many websites before. So I’m not new to the concept… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     But damn, this app is something else
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I used to spend a few days to get the website ready… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And then hire a content writer
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I can’t believe that WebGenie does all of that in less than a minute
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/alex-holland.webp" class="img-fluid d-block mx-auto " alt="Alex Holland">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Alex Holland  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     You guys created a beast. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I now have 12 websites up and running
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     Each one of them is making me money now 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And im planning to double those sites over the weekend
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     After all, it takes me what? 5 minutes 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                    But i make so much more money now
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/tom-garfield.webp" class="img-fluid d-block mx-auto " alt="Tom Garfield">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Tom Garfield  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     This is by far the easiest and most reliable AI app that i ever bought 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And i have quite a few. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     Thanks guys for allowing me to be one of the beta testers
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Stuart Johnson">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Stuart Johnson  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  I started my freelance website developer journey 4 months back, but being a newbie, I was struggling in creating a good website. But thanks for the early access to WEBPULL team, now <span class="w600">
                  I can make a stunning, highly professional website quick &amp; easy and able to serve more clients who happily pay me $4000 - $5000 each.
                  </span>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Testimonial Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">18</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
  
   <!--Profitable Section Start -->
   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="easy-text f-28 f-md-65 w700 lh140 mx-auto">Easy</div>
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center mt20">
                  We Created <u><span class="w600">PROFITABLE</span></u> Websites…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  It's pretty straightforward…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We have dozens of websites across multiple niches…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All of them work to make us money…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The best part is, we don't have to do anything
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't promote, run ads, create content… or any of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They work on complete autopilot to make us money like this…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-ss.webp" alt="screenshort" class="mx-auto d-block img-fluid mt20 "><br>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if you think creating a website is “old news”
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Let me explain something, my friend…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-img.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Profitable Section Ends -->

   <!--Don't Have a Website Section Start -->
   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  If You Don't Have A Website,<br>
                  <span class="red-clr w700">
                     You Can't Make Money!
                  </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md40">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
                  It's very simple…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If you don't have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You are going to face a very hard time trying to make money online…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  I mean, that's the idea of the Internet to start with…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  To have websites, and users can view it, and use it…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  When was the last time you saw a successful business…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or even an online guru…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Without a website?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Every one of them MUST have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even we…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You're reading this letter now on a website we own…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/website-img.webp" alt="Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
      </div>
   <!--Don't Have a Website Section Ends -->


   <!--Take a look Section Start -->
   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center mt20">
                  I Mean… Take A Look At This…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
                  There are about 1.13 Billion WEBSITE ON THE INTERNET IN 2023
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
                  According to Forbes.com…
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               There is over A BILLION website on the internet…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Why do you think there are that many?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               There must be a reason, right? 
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               And there is no sign of slowing down…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Because according to Forbes also…
               </div>
            </div>
            <div class="col-12 col-md-6">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/frame.webp" alt="Frame" class="mx-auto d-block img-fluid ">
            </div>
         </div>


         <div class="row align-items-center mt20 mt-md60">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start">
               A new website built every three seconds
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Every 3 seconds… can you believe it?
               </div>
               <div class="f-18 f-md-22 w400 lh140 mt20 black-clr text-center text-md-start">
               Finally…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               If you think that websites are not essential to make money…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
               Take a look at this…
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/frame.webp" alt="Frame" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="col-12 mt20 mt-md50">
         <div class="f-18 f-md-38 w600 lh140 black-clr text-center">
            71% of business have a  website in 2023
         </div>
         </div>
      </div>
   </div>
   <!--Take a look Section Ends -->


   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->

   <!--Proudly Introducing Start -->
   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-38 w700 cyan-clr lh140 presenting-head">
                  Proudly Presenting…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 120px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"/>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"/>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"/>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"/>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"/>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"/>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"/>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"/>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"/>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            <div class="text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
               Tap into $1.3 Trillion Platform In 3 Steps…
               </div>
            </div>
            <div class="f-md-50 f-28 w700 white-clr lh140 col-12 mt20 mt-md30 text-center">
               World's AI App That Builds DFY AI Websites For Us And Monetizes It On Autopilot
            </div>
            <div class="col-12 mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Makes Us $867.54 Per Day…
               </div> 
            </div>
            <div class="f-22 f-md-28 lh140 w700 text-center white-clr mt20 mt-md30">
               Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-10 mx-auto">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-box.webp" class="img-fluid d-block mx-auto " alt="Product">
            </div>
         </div>
      </div>
   </div>
   <!--Proudly Introducing End -->
   <!-------Exclusive Bonus----------->
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase WebGenie, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Meteora WordPress Theme </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Your Hands On A Killer, Out-of-the-box Wordpress Theme That's Versatile, Easy To Use And Extremely Customizable… Create An Impactful Good Impression On Your Clients So That They Keep Doing Business With You Over And Over Again!</li>
                        <li>Traffic is very important if you want to keep your website breathing for a living. But what is traffic if it will not become profitable to experience the luxery of fortune. </li>
                        <li>That's why, website design and user experience play a HUGE role in converting your visitors or leads into customers that might keep on coming back for more. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                     100+ WebPage Templates
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Why would you want to pay a lot for a website when you can use these 100+ Webpage templates to create professional looking websites for only pennies! This is one of the best deals on webpage templates ever put together!</li>
                        <li>You're sure to find more than one design you like and here you can have them all for one super low price! 100+ Web templates allow you to have your website up and running in a matter of minutes! Mix and match for unlimited possibilities! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course!</li>
                        <li>Search marketing is the process of generating traffic and gaining visibility from search engines like Google, Bing and Yahoo through paid and unpaid strategies. This includes generating traffic through organic or free listings as well as buying traffic through paid search listings on ad networks like Google AdWords. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Rapid Product Creation 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course! (5 Days Training On How To Create Info Products At Lightning Speed.)</li>
                        <li>There is a lot of information on the web on how to create your own products but not many of these sources provide a clear and concise step by step plan so that you can get your product out there and making money in the shortest time possible! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Latest Humans Stock Images
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</li>
                        <li>If you have your own online business or you are a freelancer that is hired by an online entrepreneur to expand his business, doing social media marketing is the best step to get started.</li>
                        <li>The fact is that many have said that this new marketing innovation is so powerful that would be a huge help to boost your sales to the roof.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social pop up widget for your WordPress blog! Take advantage of this technique to improve your social conversions. </li>
                        <li>This product allows your people to conveniently follow,like or subscribe to your social media page to keep informed about updates and new releases. </li>
                        <li>You will be confident when you make changes to your like pop-up box. This is super fun to use and fun for your readers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Membership Plugin 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The effortless way to create professional sites in Wordpress using your favorite membership plugin!</li>
                        <li>With this plugin you can build a beautiful and robust membership sites. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Unique Exit Popup
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Grab the Attention of Your Visitors When They Are About to Leave Your Website!</li>
                        <li>Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?</li>
                        <li>Well, inside this product is a software that will change the game of this issue. This plugin will engage and get the attention of your readers that is about to leave your page and offer them something valuable from your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     My Blog Announcer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Software that automatically pings blog search engines to get your blogs indexed much faster in the search engines. </li>
                        <li>Just add your blog URLs and then add or remove ping services (the program comes pre-loaded with several services). Whenever you update your blog content, the program will automatically run all blogs to all ping services with the click of a button.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Keyword Ninja
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of... </li>
                        <li>This is keyword software that finds synonyms and gets keyword data from Overture. It also gets related keywords from sites listed on Google and Yahoo. You can either enter a "starting" keyword manually or download an existing keyword list.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Cloaker Shadow
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stop losing your hard earned commissions to nasty affiliate thieves and add credibility while promoting others.</li>
                        <li>Hide your ugly affiliate links and boost your click through rate by at least 200%</li>
                        <li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Database Magic 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>An Easy Solution for Creating, Backing Up and Restoring Cpanel Databases - Create/Backup/Restore Databases without Logging into Cpanel - You can decide the database name, username, and password... -OR- You can let the program generate them at random!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Content Shrinker 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Have You Ever Wished There were a way to Make Your Content Inside an I-Frame Search Engine Readable? Can you just imagine the possibilities for your Adsense and Content sites if this were possible? Wish No Longer! A New Software Program Creates a Content Box Like an I-frame with one HUGE difference, It is Search Engine Readable!</li>
                        <li>The Content Shrinker Panel lets you control EVERY aspect of your scrollable box, you can change border styles, colors, backgrounds, fonts, etc.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Installation Tips & Tricks
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn how easy it is to set up a website using the free platform WordPress!</li>
                        <li>If you are a blogger chances are you want to use WordPress as your blogging platform which can give you the freedom to maximize your marketing effort.</li>
                        <li>The challenge now is that if you haven't had the experience of setting up your first WordPress Website, you may find it difficult or challenging that will eventually consume you more time.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Site Speed Secrets
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Everybody hates slow websites. So, why haven't you done anything about your slow site? </li>
                        <li>Don't you realize that it's putting people off from visiting your site and checking out everything that you have to offer?</li>
                        <li>It doesn't matter if you've got a well-designed site or a life-changing product.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"WebGenie"</span> for an Additional <span class="w700 orange-gradient">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Content Auditor
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Effortless Data Exports Make it Easy to Manage and Promote Your Top Posts & Pages! Easy to use plugin will export a customized summary of all your Wordpress blog content!</li>
                        <li>If you are a blogger, SEO or an online business owner, auditing your blog content may not be necessary but if you want to scale marketing asset as well as know the progress of your result, doing it may have a huge help to know what step you are going to do next.</li>
                        <li>The thing is that auditing your whole website asset can also be a pain in the ass and time-consuming.<li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Security Mastery 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Secure Your Online Business Empire Using this Amazing WordPress Security Techniques and More!</li>
                        <li>If you are an online business owner, chances are you also have a website. And most of the time, one content management system that is frequently used - wordpress.</li>
                        <li>WordPress is a very powerful tool and is also one of the target by hackers to take down your online business.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Theme Army 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a Mail Pop-Up Plugin</li>
                        <li>Use this Bonus along with high converting sales & email swipes given with WebGenie to show Popups right inside your mails.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WordPress Ad Creator 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Instantly Create an Ad Using WordPress Ad Creator!</li>
                        <li>Traffic is very important to ones blog or website. And because of that website owners and online entrepreneurs do their part to find those leads or traffic online whether via SEO, Social Media Marketing or Pay Per Click Advertisement.</li>
                        <li>If you want to create an ad inside your WordPress dashboard, using this amazing plugin is a huge help to you.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Visitors To Your WordPress Blog 24/7 Live Chat Support - Even While You Sleep!</li>
                        <li>Traffic is nothing if this will not convert into paying customers. The thing is that, even if your website is online 24/7, if no one will interact with your website visitor, they will just go away somewhere else and never got back again.</li>
                        <li>The good news is that inside this product is a tool that will connect you to your visitors by having a Live Support chat box in your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That's Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-gradient">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }
                        
                              .cls-2 {
                                 fill: #fc8548;
                              }
                        
                              .cls-3 {
                                 fill: #15c6fc;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebGenie 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="WebGenie Special Bonuses">
   <meta name="description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">    
   <meta name="author" content="Atul Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WebGenie Special Bonuses">
   <meta property="og:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta property="og:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WebGenie Special Bonuses">
   <meta property="twitter:description" content="World's First A.I Bot Builds Us DFY Websites Prefilled With Smoking Hot Content…">
   <meta property="twitter:image" content="https://www.webgenie.live/special-bonus/thumbnail.png">
   <title>WebGenie Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'May 24 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/p24k7b/0';
      $_GET['name'] = 'Atul Pareek';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }
                        
                              .cls-2 {
                                 fill: #fc8548;
                              }
                        
                              .cls-3 {
                                 fill: #15c6fc;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="pre-heading f-20 f-md-21 w500 white-clr lh140">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot Builds Us <span class="orange-gradient">DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $867.54 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               Instantly Leverage $1.3 Trillion Platform With A Click Of A Button…
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills - No Experience - No Hidden Fees - No BS
            </div>
            </div>
         </div>
         <div class="row mt20 mt-md30 mb20 mb-md30">
            <div class="col-12 col-md-10 mx-auto">
            <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> 
               <!-- <div class="responsive-video">
                  <iframe src="https://webpull.dotcompal.com/video/embed/26sp46ptcb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div> -->
            </div>
         </div>
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT… </li>
                           <li>All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content </li>
                           <li>Instant High Convertring Traffic For 100% Free </li>
                           <li>100% Of Beta Testers Made  At Least $100 Within 24 Hours Of Using WebGenie. </li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                           <li>We Let AI Do All The Work For Us.</li>
                           <li>Get Up And Running In 30 Seconds Or Less</li>
                           <li>Instantly Tap Into $1.3 Trillion Platform </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4 </li>
                           <li>99.99% Up Time Guaranteed </li>
                           <li>ZERO Upfront Cost</li>
                           <li>30 Days Money-Back <br class="d-none d-md-block">Guarantee</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt30 mt-md60">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black black-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-black black-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>

  
<!-- Step Section Start -->
<div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Dominating ANY Niche With DFY AI Websites…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Login to WebGenie App to get access to your personalised dashboard and start right away.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step1.gif " class="img-fluid d-block mx-auto" alt="Step 1" style="border-radius:20px;     border: 1px solid #15c6fc;">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Create
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just Enter Your Keyword Or Select Your Niche, And Let AI Create Stunning And Smoking Hot Website For You.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Publish &amp; Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just sit back and watch thousands of clicks come to your newly created stunning AI website…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Step Sction End -->


   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   
   <!--Profit Wall Section Start -->
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
                  WebGenie Made It Fail-Proof To Create A <br class="d-none d-md-block"> Profitable Website With A.I.
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with WebGenie… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     WebGenie eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With WebGenie you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with WebGenie…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because WebGenie is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using WebGenie
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Profit Wall Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->


     <!--Testimonial Section Start -->
     <div class="testimonial-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Join Hundreds Of Users Who Uses WebGenie Daily To Get FAST RESULTS
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
               (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/rodney-paul.webp" class="img-fluid d-block mx-auto " alt="Rodney Paul">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Rodney Paul </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I created many websites before. So I’m not new to the concept… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     But damn, this app is something else
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I used to spend a few days to get the website ready… 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And then hire a content writer
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I can’t believe that WebGenie does all of that in less than a minute
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/alex-holland.webp" class="img-fluid d-block mx-auto " alt="Alex Holland">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Alex Holland  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     You guys created a beast. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     I now have 12 websites up and running
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center"> 
                     Each one of them is making me money now 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And im planning to double those sites over the weekend
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     After all, it takes me what? 5 minutes 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                    But i make so much more money now
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/tom-garfield.webp" class="img-fluid d-block mx-auto " alt="Tom Garfield">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Tom Garfield  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w600 text-center">
                     This is by far the easiest and most reliable AI app that i ever bought 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     And i have quite a few. 
                  </p>
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center"> 
                     Thanks guys for allowing me to be one of the beta testers
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Stuart Johnson">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Stuart Johnson  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  I started my freelance website developer journey 4 months back, but being a newbie, I was struggling in creating a good website. But thanks for the early access to WEBPULL team, now <span class="w600">
                  I can make a stunning, highly professional website quick &amp; easy and able to serve more clients who happily pay me $4000 - $5000 each.
                  </span>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Testimonial Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">18</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
  
   <!--Profitable Section Start -->
   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="easy-text f-28 f-md-65 w700 lh140 mx-auto">Easy</div>
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center mt20">
                  We Created <u><span class="w600">PROFITABLE</span></u> Websites…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  It's pretty straightforward…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We have dozens of websites across multiple niches…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All of them work to make us money…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The best part is, we don't have to do anything
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't promote, run ads, create content… or any of that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They work on complete autopilot to make us money like this…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-ss.webp" alt="screenshort" class="mx-auto d-block img-fluid mt20 "><br>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if you think creating a website is “old news”
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Let me explain something, my friend…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/profitable-img.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!--Profitable Section Ends -->

   <!--Don't Have a Website Section Start -->
   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center mt20">
                  If You Don't Have A Website,<br>
                  <span class="red-clr w700">
                     You Can't Make Money!
                  </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md40">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-28 w600 lh140 mt20 black-clr text-center text-md-start">
                  It's very simple…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  If you don't have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You are going to face a very hard time trying to make money online…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  I mean, that's the idea of the Internet to start with…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  To have websites, and users can view it, and use it…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  When was the last time you saw a successful business…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Or even an online guru…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Without a website?
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Every one of them MUST have a website…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even we…
               </div>
               <div class="f-18 f-md-20 w400 lh140 mt20 black-clr text-center text-md-start">
                  You're reading this letter now on a website we own…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/website-img.webp" alt="Website" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
      </div>
   <!--Don't Have a Website Section Ends -->



   <div class="blue-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  And Not Just Views…
               </div>
               <div class="f-22 f-md-38 w400 lh140 black-clr text-center">
                  People Are Buying From Websites Every Second…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               People love to spend money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Buying products, courses, recipe books, and so much more…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               How do you think they spend that money?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Exactly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               On a website…
               </div>
               <div class="f-18 f-md-32 w600 lh140 black-clr text-center text-md-start mt20">
               It's A $9.5 TRILLION Business…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's how much people spend on websites EVERY YEAR…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's a REALLY huge number…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's almost half the entire budget of the United States Of America…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can get a piece of that pie today…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/laptop-man.webp" alt="Laptop Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Not Easy Section Start-->

   <!--Proudly Introducing Start -->
   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-38 w700 cyan-clr lh140 presenting-head">
                  Proudly Presenting…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 120px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"/>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"/>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"/>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"/>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"/>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"/>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"/>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"/>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"/>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"/>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"/>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            <div class="text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
               Tap into $1.3 Trillion Platform In 3 Steps…
               </div>
            </div>
            <div class="f-md-50 f-28 w700 white-clr lh140 col-12 mt20 mt-md30 text-center">
               World's AI App That Builds DFY AI Websites For Us And Monetizes It On Autopilot
            </div>
            <div class="col-12 mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Makes Us $867.54 Per Day…
               </div> 
            </div>
            <div class="f-22 f-md-28 lh140 w700 text-center white-clr mt20 mt-md30">
               Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-10 mx-auto">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-box.webp" class="img-fluid d-block mx-auto " alt="Product">
            </div>
         </div>
      </div>
   </div>
   <!--Proudly Introducing End -->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : MailerGPT
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="header-section-mailergpt">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 799.35 150" style="max-height: 60px;"><defs><style>
                  .cls-1m{fill:#fff;}
                  .cls-2m{fill:url(#radial-gradient);}
                  .cls-3m{fill:url(#radial-gradient-2);}
                  .cls-4m{fill:url(#radial-gradient-3);}
               </style>
               <radialGradient id="radial-gradient" cx="-4189.93" cy="-2350.54" r="86.7" gradientTransform="translate(11116.77 6183.53) scale(2.64)" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#18e6ff"></stop>
                  <stop offset="0.99" stop-color="#005fed"></stop>
               </radialGradient><radialGradient id="radial-gradient-2" cx="-4185.92" cy="-2344.98" r="79.41" xlink:href="#radial-gradient"></radialGradient><radialGradient id="radial-gradient-3" cx="-4189.93" cy="-2350.54" r="86.7" xlink:href="#radial-gradient"></radialGradient>
            </defs>
            <title>MailerGPT Logo White0</title>
            <g id="Layer_2" data-name="Layer 2">
               <g id="Layer_1-2" data-name="Layer 1">
                  <path class="cls-1m" d="M212.66,51.53l34.51,82.58,34.64-82.58h10.54V145.1h-7.91V104.35l.65-41.77L250.25,145.1h-6.1L209.44,62.9l.65,41.2v41h-7.91V51.53Z"></path>
                  <path class="cls-1m" d="M358.16,145.1a36.19,36.19,0,0,1-1.48-9.7,26.88,26.88,0,0,1-10.32,8.13,31.66,31.66,0,0,1-13.27,2.86q-10,0-16.22-5.6a18.19,18.19,0,0,1-6.21-14.13,18.61,18.61,0,0,1,8.46-16.07q8.44-5.91,23.55-5.91h13.95V96.77q0-7.45-4.6-11.73t-13.4-4.27q-8,0-13.3,4.11t-5.27,9.9l-7.71-.07q0-8.28,7.71-14.36t19-6.07q11.63,0,18.34,5.81t6.91,16.23v32.91q0,10.08,2.12,15.1v.77ZM334,139.57a25.88,25.88,0,0,0,13.79-3.72,22,22,0,0,0,8.84-10V110.59H342.86q-11.5.14-18,4.21T318.38,126a12.44,12.44,0,0,0,4.33,9.71Q327.06,139.57,334,139.57Z"></path>
                  <path class="cls-1m" d="M384.31,55.48a5.16,5.16,0,0,1,1.42-3.67,5,5,0,0,1,3.85-1.5,5.14,5.14,0,0,1,3.89,1.5,5.06,5.06,0,0,1,1.45,3.67,5,5,0,0,1-1.45,3.64,5.2,5.2,0,0,1-3.89,1.47,5,5,0,0,1-5.27-5.11Zm9.07,89.62h-7.72V75.56h7.72Z"></path>
                  <path class="cls-1m" d="M422.87,145.1h-7.71V46.39h7.71Z"></path>
                  <path class="cls-1m" d="M470.69,146.39a30.19,30.19,0,0,1-16-4.37,29.8,29.8,0,0,1-11.15-12.18,38,38,0,0,1-4-17.52v-2.76a41,41,0,0,1,3.89-18.06,31,31,0,0,1,10.83-12.63,26.74,26.74,0,0,1,15-4.59q12.66,0,20.08,8.64t7.42,23.62v4.31H447.17v1.47q0,11.84,6.78,19.7A21.56,21.56,0,0,0,471,139.89a25.09,25.09,0,0,0,10.9-2.24,24.21,24.21,0,0,0,8.57-7.2l4.82,3.66Q486.82,146.38,470.69,146.39Zm-1.41-65.56a19.19,19.19,0,0,0-14.62,6.37q-6,6.36-7.23,17.09h41.71v-.83q-.33-10-5.72-16.33A17.74,17.74,0,0,0,469.28,80.83Z"></path>
                  <path class="cls-1m" d="M543.89,82.12a28.69,28.69,0,0,0-5.2-.45,18.77,18.77,0,0,0-12.18,4q-5,4-7.11,11.66V145.1h-7.64V75.56h7.52l.12,11.06Q525.51,74.28,539,74.28a12.74,12.74,0,0,1,5.07.83Z"></path>
                  <path class="cls-1m" d="M628.4,133.28q-5.21,6.22-14.71,9.67a61.73,61.73,0,0,1-21.08,3.44,41.73,41.73,0,0,1-21.31-5.31,35.35,35.35,0,0,1-14.14-15.39q-5-10.08-5.11-23.71V95.61q0-14,4.73-24.26A35,35,0,0,1,570.4,55.67a39.26,39.26,0,0,1,20.86-5.43q16.63,0,26,7.94t11.12,23.1H609.64q-1.29-8-5.69-11.76c-2.94-2.48-7-3.73-12.12-3.73q-9.83,0-15,7.4t-5.21,22v6q0,14.72,5.6,22.24t16.38,7.52q10.86,0,15.49-4.63V110.14H591.58V95.94H628.4Z"></path>
                  <path class="cls-1m" d="M663.3,112.13v33H644V51.53h36.5a42.21,42.21,0,0,1,18.54,3.85,28.41,28.41,0,0,1,12.31,11,30.57,30.57,0,0,1,4.31,16.16q0,13.75-9.42,21.69t-26.06,7.94Zm0-15.62h17.22q7.65,0,11.67-3.59t4-10.29q0-6.87-4-11.12T681,67.14H663.3Z"></path>
                  <path class="cls-1m" d="M799.35,67.14H770.69v78H751.41v-78H723.13V51.53h76.22Z"></path>
                  <path class="cls-2m" d="M92.86,32.17c5.11,5.73,11,7.59,17.59,6-4.14-2.22-6.8-5.1-6.78-9.17l5.24-3.15c1.33-2.54,1.46-9.68.55-12.87L104,9.67c.13-4.06,2.9-6.87,7.11-9C104.6-1.11,98.65.6,93.34,6.19Q73.63,3.45,57.88,17.66a1.4,1.4,0,0,0,0,2.11Q73.07,34.39,92.86,32.17Zm-8.61-19a6.16,6.16,0,1,1-6.15,6.15A6.15,6.15,0,0,1,84.25,13.2Z"></path>
                  <path class="cls-3m" d="M133.69,114.83l.23,20.51H99A292.25,292.25,0,0,1,63,147.05q-5.85,1.59-11.83,2.95h89.44a6.38,6.38,0,0,0,6.38-6.38l-.43-39.45A156.27,156.27,0,0,1,133.69,114.83Z"></path>
                  <path class="cls-4m" d="M168.93,32.62v0a17.57,17.57,0,0,0-5-6.11l-.11-.1h0c-9.77-7.68-28.58-7.75-51.22-6.27,25.59,3.46,41.29,10.53,44.71,22.11h0l0,.11q.15.52.27,1c1.78,7.49-.11,15.16-5.27,23.56h0a40.61,40.61,0,0,1-4.56,6.34c-.47.54-1,1.09-1.46,1.64l-.2-20.09c0-3.43-2.24-5.28-5.5-5.83H73.81l-.3.35L73.22,49H6.38c-3.26.55-5.5,2.4-5.5,5.83L0,143.62A6.38,6.38,0,0,0,6.38,150H19.13A279.18,279.18,0,0,0,52,143.11c46-11.88,82.86-31.9,106.13-61.27l0,0c1.51-1.92,2.9-3.81,4.19-5.66l0-.06h0c.82-1.18,1.59-2.36,2.32-3.52h0l0,0,.12-.25c.64-1,1.23-2,1.8-3a46.51,46.51,0,0,0,4.33-12c.29-1.41.53-2.78.71-4.13,0-.12,0-.24,0-.36C172.81,44.72,171.81,37.91,168.93,32.62ZM73.51,63.11h45.67L73.51,98.94,27.85,63.11Zm-60.4,72.23h0l.61-65.18L73.51,117l59.8-46.8.16,17.11C106.7,110.18,61.23,135.16,13.11,135.34Z"></path>
               </g></g></svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-20 f-20 w600 text-center white-clr pre-heading lh150">					
                  Stop Paying 100s Of Dollars Monthly to Old-School Email Marketing Services
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center"> 
                  <div class="f-md-45 f-28 w700 white-clr lh150">  
                  
                       <span class="blue-gradient-mailer w700">Brand New App </span> Writes &amp; Send Unlimited Profit-Pulling Emails To Unlimited Subscribers For YOU...  <span class="brush-mailer w700"> All With NO Monthly Fee Ever.</span>
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class=" f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Finally! 4X Your Email Opens, Clicks, &amp; Traffic. Promote Affiliate Offers And Earn Commissions, Sell More Products &amp; Services Online And Build Customer Relationship With Follow-up Emails.
                  </div>
               </div>
               
               <div class="col-12 mt20 mt-md60">
                  <div class="row ">
                     <div class="col-md-7 col-12">
                        <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="video-box">
                         <div class="col-12 responsive-video">
                           <iframe src="https://mailergpt.dotcompal.com/video/embed/xdphj6h2hr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                           </div>
                        
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                                
                              <li> Just Enter a Keyword <span class="list-highlight"> &amp; MailerGPT A.I. Research and Writes Email Copies for You </span></li>
                              <li> <span class="list-highlight"> Send Beautiful Emails  </span> for Sales, Promos, Product Updates and More ...</li> 
                              <li><span class="list-highlight">	Promote Affiliate Offers and Earn Commissions– </span> No Product or Service Required </li>                           
                              <li> <span class="list-highlight"> AI Enabled Smart Tagging </span> for Lead Personalization &amp; Traffic </li> 
                              <li><span class="list-highlight"> Upload Unlimited Subscribers</span> with Zero Restrictions </li>
                              <li> <span class="list-highlight"> 	100% Control </span> on Your Online Business </li>
                              <li> <span class="list-highlight"> 100+ Stunning Done-For-You Templates </span> for All Your List Building Needs  </li>
                              <li> <span class="list-highlight"> 	100% Cloud Based </span>No Special Skills Required. No Recurring Fee </li>                            
                              <li> <span class="list-highlight"> FREE Commercial License - </span> Build an Incredible Income Offering Services To Clients! </li> 
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/mailergpt-ss1.png">
            <source media="(min-width:320px)" srcset="assets/images/mailergpt-ss1-mview.png" style="width:100%" class="vidvee-mview">
            <img src="assets/images/mailergpt-ss1.png" alt="Vidvee Steps" class="img-fluid">
         </picture>
      </div>
   </div>

   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Vidvee
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Vidvee Section------>
   <div class="vidvee-header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="assets/images/vidvee.logo.png" alt="" class="d-block mx-auto img-fluid">
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class="vidvee-pre-heading f-md-24 f-20 w600 lh130">
                  1 Click Exploits YouTube Loophole to Legally Use Their 800 Million+<br class="d-none d-md-block">
                  Awesome Videos for Making Us $528 Every Day Again &amp; Again...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 text-center">
               <div class="f-md-50 f-30 w500 white-clr lh130 line-center">
                  Breakthrough Software <span class="w700 vidvee-orange-clr">Creates Self-Growing Beautiful Video Channels Packed With RED-HOT Videos And Drives FREE Autopilot Traffic On Any Topic </span>
                  In Just 7 Minutes Flat…
               </div>
               <div class="mt-md25 mt20 f-20 f-md-24 w500 text-center lh130 white-clr">
                  No Content Creation. No Camera or Editing. No Tech Hassles Ever... 100% Beginner Friendly!
               </div>
               <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                  <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                     <div class="col-12 responsive-video">
                        <iframe src="https://vidvee.dotcompal.com/video/embed/xcz62wags4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                                                box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md40 vidvee-key-features-bg">
                     <div class="row flex-wrap">
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                              <li>Complete Video &amp; YouTube Business Builder</li>
                              <li>Ground-Breaking Method Brings Unlimited Traffic To Any Offer, Page Or Link</li>
                              <li>Tap Into 800 Million YouTube Videos &amp; Make Any Video Yours Legally</li>
                              <li>Build Beautiful &amp; Branded Video Channels on Any Topic in Any Niche Within Minutes</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                              <li>Can Upload, Host &amp; Play Your Own Videos</li>
                              <li>100% FREE Viral, Social &amp; SEO Traffic (Powerful)</li>
                              <li>Make Tons of Sales, Amazon/Affiliate Commissions &amp; Leads.</li>
                              <li><u>FREE Commercial License</u> - Build An Incredible Income Offering Services &amp; Keep 100%!</li>
                           </ul>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/vidvee-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/vidvee-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/no-doubt-vidvee.webp">
            <source media="(min-width:320px)" srcset="assets/images/no-doubt-mview-vidvee.webp">
            <img src="assets/images/no-doubt-vidvee.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/proudly-vidvee.webp">
            <source media="(min-width:320px)" srcset="assets/images/proudly-mview-vidvee.webp">
            <img src="assets/images/proudly-vidvee.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Vidvee Section------>
   <!----------Buziffy ---------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Buzzify
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS
                  If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="buz-header-section">
      <div class="container">
         <div class="row">

            <div class="col-12">
               <div class="row">
                  <div class="col-md-3 text-center mx-auto">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                        <style type="text/css">
                           .st0 {
                              fill: #FFFFFF;
                           }
                        </style>
                        <g>
                           <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                           <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                           <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                           <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                           <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                           <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                           <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                        </g>
                     </svg>
                  </div>
               </div>
            </div>
            <!-- <div class="col-12 mt20 mt-md50"> <img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto"></div> -->
            <div class="col-12 text-center lh140 mt20 mt-md30">
               <div class="pre-heading-b f-md-20 f-18 w600 lh140">
                  <div class="skew-con d-flex gap20 align-items-center ">
                     It’s Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh140">
               Breakthrough 3-Click Software Uses a <span class="under yellow-clr w700"> Secret Method to Make Us $528/Day Over and Over</span> Again Using the Power of Trending Content &amp; Videos
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh140 yellow-clr">
               All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
               <div class="col-12 responsive-video">
                  <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="bsecond-section">
      <div class="container">
         <div class="row">
            <div class="col-12 p-md0">
               <div class="col-12 key-features-bg-b d-flex align-items-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Creates Beautiful &amp; Self-Updating Sites </span>with Hot Trending Content &amp; Videos </li>
                           <li class="w600">Built-In 1-Click Traffic Generating System </li>
                           <li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
                           <li>Legally Use Other’s Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
                           <li>1-Click Social Media Automation – <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
                           <li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
                           <li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Set and Forget System</span> With Single Keyword – Set Rules To Find &amp; Publish Trending Posts</li>
                           <li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
                           <li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients &amp; Charge Them</li>
                           <li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
                           <li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
                           <li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
                           <li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/b3d.webp">
            <source media="(min-width:320px)" srcset="assets/images/b3m.webp">
            <img src="assets/images/b3d.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/bnd.webp">
            <source media="(min-width:320px)" srcset="assets/images/bnm.webp">
            <img src="assets/images/bnd.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/bpd.webp">
            <source media="(min-width:320px)" srcset="assets/images/bpm.webp">
            <img src="assets/images/bpd.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!----------Buziffy ---------->
   <!------Ninjakash--------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : Ninja Kash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  My Recent Launch Ninja Kash - With Reseller License (worth $11999)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="grand-logo-bg">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <img src="assets/images/logo-gr.webp" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>
   <div class="header-section-n">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="mt20 mt-md50">
                  <div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-n">
                     Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
                  </div>
               </div>
            </div>
            <div class="col-12">
               <div class="col-12 mt-md30 mt20">
                  <div class="f-28 f-md-50 w500 text-center white-clr lh140">
                     <span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
                  </div>
               </div>
               <div class="col-12 f-22 f-md-28 w600 text-center  orange lh140 mt-md30 mt20">
                  NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-7 col-12 mt-md40 mt20 px-md15 min-md-video-width-left">
               <!--<div>
                  <img src="assets/images/video-bg.png" class="img-responsive center-block"> 
                     </div>-->
               <div class="col-12 mt-md15">
                  <div class="col-12 responsive-video">
                     <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 f-20 f-md-20 lh140 w400 white-clr mt-md40 mt20 pl-md15 min-md-video-width-right">
               <ul class="list-head-b pl0 m0">
                  <li>Kickstart with 50 Hand-Picked Products</li>
                  <li>Promote Any Offer in Any niche HANDS FREE</li>
                  <li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
                  <li>Drive TONS OF Social &amp; Viral traffic</li>
                  <li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
                  <li>No Monthly Fees…EVER</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/gr2.webp">
            <source media="(min-width:320px)" srcset="assets/images/gr2a.webp">
            <img src="assets/images/gr2.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/gr1.webp">
            <source media="(min-width:320px)" srcset="assets/images/gr1a.webp">
            <img src="assets/images/gr1.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Ninjakash--------->


   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #5 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED–A Breakthrough Software That<span class="w700 orange-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 grey-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w700">Create Multiple UNIQUE Job Sites</span> – Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w700">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w700">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w700">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w700">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
                        <li><span class="w700">Get Started Immediately</span> – Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w700">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>

                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-steps-mview.webp">
            <img src="assets/images/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-big-job-mview.webp">
            <img src="assets/images/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-impressive-mview.webp">
            <img src="assets/images/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-proudly-presenting-mview.webp">
            <img src="assets/images/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>

   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase WebGenie, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Meteora WordPress Theme </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Your Hands On A Killer, Out-of-the-box Wordpress Theme That's Versatile, Easy To Use And Extremely Customizable… Create An Impactful Good Impression On Your Clients So That They Keep Doing Business With You Over And Over Again!</li>
                        <li>Traffic is very important if you want to keep your website breathing for a living. But what is traffic if it will not become profitable to experience the luxery of fortune. </li>
                        <li>That's why, website design and user experience play a HUGE role in converting your visitors or leads into customers that might keep on coming back for more. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                     100+ WebPage Templates
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Why would you want to pay a lot for a website when you can use these 100+ Webpage templates to create professional looking websites for only pennies! This is one of the best deals on webpage templates ever put together!</li>
                        <li>You're sure to find more than one design you like and here you can have them all for one super low price! 100+ Web templates allow you to have your website up and running in a matter of minutes! Mix and match for unlimited possibilities! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course!</li>
                        <li>Search marketing is the process of generating traffic and gaining visibility from search engines like Google, Bing and Yahoo through paid and unpaid strategies. This includes generating traffic through organic or free listings as well as buying traffic through paid search listings on ad networks like Google AdWords. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Rapid Product Creation 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture system that comes with complete website and email course! (5 Days Training On How To Create Info Products At Lightning Speed.)</li>
                        <li>There is a lot of information on the web on how to create your own products but not many of these sources provide a clear and concise step by step plan so that you can get your product out there and making money in the shortest time possible! </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Latest Humans Stock Images
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</li>
                        <li>If you have your own online business or you are a freelancer that is hired by an online entrepreneur to expand his business, doing social media marketing is the best step to get started.</li>
                        <li>The fact is that many have said that this new marketing innovation is so powerful that would be a huge help to boost your sales to the roof.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
            <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social pop up widget for your WordPress blog! Take advantage of this technique to improve your social conversions. </li>
                        <li>This product allows your people to conveniently follow,like or subscribe to your social media page to keep informed about updates and new releases. </li>
                        <li>You will be confident when you make changes to your like pop-up box. This is super fun to use and fun for your readers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Membership Plugin 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The effortless way to create professional sites in Wordpress using your favorite membership plugin!</li>
                        <li>With this plugin you can build a beautiful and robust membership sites. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Unique Exit Popup
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Grab the Attention of Your Visitors When They Are About to Leave Your Website!</li>
                        <li>Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?</li>
                        <li>Well, inside this product is a software that will change the game of this issue. This plugin will engage and get the attention of your readers that is about to leave your page and offer them something valuable from your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     My Blog Announcer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Software that automatically pings blog search engines to get your blogs indexed much faster in the search engines. </li>
                        <li>Just add your blog URLs and then add or remove ping services (the program comes pre-loaded with several services). Whenever you update your blog content, the program will automatically run all blogs to all ping services with the click of a button.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Keyword Ninja
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of... </li>
                        <li>This is keyword software that finds synonyms and gets keyword data from Overture. It also gets related keywords from sites listed on Google and Yahoo. You can either enter a "starting" keyword manually or download an existing keyword list.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Cloaker Shadow
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Stop losing your hard earned commissions to nasty affiliate thieves and add credibility while promoting others.</li>
                        <li>Hide your ugly affiliate links and boost your click through rate by at least 200%</li>
                        <li>Protect your affiliate links and keep your hard earned commissions from being stolen from the nasty affiliate thieves</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Database Magic 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>An Easy Solution for Creating, Backing Up and Restoring Cpanel Databases - Create/Backup/Restore Databases without Logging into Cpanel - You can decide the database name, username, and password... -OR- You can let the program generate them at random!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Content Shrinker 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Have You Ever Wished There were a way to Make Your Content Inside an I-Frame Search Engine Readable? Can you just imagine the possibilities for your Adsense and Content sites if this were possible? Wish No Longer! A New Software Program Creates a Content Box Like an I-frame with one HUGE difference, It is Search Engine Readable!</li>
                        <li>The Content Shrinker Panel lets you control EVERY aspect of your scrollable box, you can change border styles, colors, backgrounds, fonts, etc.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Installation Tips & Tricks
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn how easy it is to set up a website using the free platform WordPress!</li>
                        <li>If you are a blogger chances are you want to use WordPress as your blogging platform which can give you the freedom to maximize your marketing effort.</li>
                        <li>The challenge now is that if you haven't had the experience of setting up your first WordPress Website, you may find it difficult or challenging that will eventually consume you more time.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Site Speed Secrets
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Everybody hates slow websites. So, why haven't you done anything about your slow site? </li>
                        <li>Don't you realize that it's putting people off from visiting your site and checking out everything that you have to offer?</li>
                        <li>It doesn't matter if you've got a well-designed site or a life-changing product.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Content Auditor
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Effortless Data Exports Make it Easy to Manage and Promote Your Top Posts & Pages! Easy to use plugin will export a customized summary of all your Wordpress blog content!</li>
                        <li>If you are a blogger, SEO or an online business owner, auditing your blog content may not be necessary but if you want to scale marketing asset as well as know the progress of your result, doing it may have a huge help to know what step you are going to do next.</li>
                        <li>The thing is that auditing your whole website asset can also be a pain in the ass and time-consuming.<li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Security Mastery 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Secure Your Online Business Empire Using this Amazing WordPress Security Techniques and More!</li>
                        <li>If you are an online business owner, chances are you also have a website. And most of the time, one content management system that is frequently used - wordpress.</li>
                        <li>WordPress is a very powerful tool and is also one of the target by hackers to take down your online business.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Theme Army 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a Mail Pop-Up Plugin</li>
                        <li>Use this Bonus along with high converting sales & email swipes given with WebGenie to show Popups right inside your mails.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WordPress Ad Creator 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Instantly Create an Ad Using WordPress Ad Creator!</li>
                        <li>Traffic is very important to ones blog or website. And because of that website owners and online entrepreneurs do their part to find those leads or traffic online whether via SEO, Social Media Marketing or Pay Per Click Advertisement.</li>
                        <li>If you want to create an ad inside your WordPress dashboard, using this amazing plugin is a huge help to you.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     WP Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Visitors To Your WordPress Blog 24/7 Live Chat Support - Even While You Sleep!</li>
                        <li>Traffic is nothing if this will not convert into paying customers. The thing is that, even if your website is online 24/7, if no one will interact with your website visitor, they will just go away somewhere else and never got back again.</li>
                        <li>The good news is that inside this product is a tool that will connect you to your visitors by having a Live Support chat box in your website.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That's Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-gradient">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
         <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center lh120 w700 mt20 mt-md20 orange-gradient">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w500 text-center mt20 black-clr">
                  Use Coupon Code <span class="w700 orange-gradient">"ATULWEB6"</span> for an Additional <span class="w700 orange-gradient">$6 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md30">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebGenie + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }
                        
                              .cls-2 {
                                 fill: #fc8548;
                              }
                        
                              .cls-3 {
                                 fill: #15c6fc;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                                 <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                                 <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                                    <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                                    <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                                    <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                    <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                                    <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                                    <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                                    <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebGenie 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://webgenie.live/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
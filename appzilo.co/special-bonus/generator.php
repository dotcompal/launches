<!DOCTYPE html>
<html>
   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="AppZilo Special Bonuses">
      <meta name="description" content="AppZilo Special Bonuses">
      <meta name="keywords" content="AppZilo Special Bonuses">
      <meta property="og:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="AppZilo Special Bonuses">
      <meta property="og:description" content="AppZilo Special Bonuses">
      <meta property="og:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="AppZilo Special Bonuses">
      <meta property="twitter:description" content="AppZilo Special Bonuses">
      <meta property="twitter:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">	
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>

   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px" xml:space="preserve">
                  <style type="text/css">
                     .st00{fill:#07163A;}
                     .st11{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                     .st22{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                     .st33{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000168823944284911347230000002582526602741899929_);}
                  </style>
                  <g>
	            <g>
                  <path class="st00" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                     c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                     c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z
                     M242,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                     c3.6,3.7,7.9,5.6,13,5.6s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
                  <path class="st00" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                     c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                     c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                     M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                     c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
                  <path class="st00" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
                  <path class="st00" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                     c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                     l0-84.2L505.3,36.7z"/>
                  <path class="st00" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
                  <path class="st00" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                     c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                     c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                     C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                     c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"
                     />
               </g>
               <g>
                  <path class="st11" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                     c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                     <stop  offset="0.5251" style="stop-color:#2089EA"/>
                     <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
                     <stop  offset="0.7855" style="stop-color:#1751C0"/>
                     <stop  offset="1" style="stop-color:#0D1793"/>
                  </linearGradient>
                  <path class="st22" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                     c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                  
                     <linearGradient id="SVGID_00000091705051100733353190000005027636122510766246_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                     <stop  offset="0.3237" style="stop-color:#F4833F"/>
                     <stop  offset="0.6981" style="stop-color:#F39242"/>
                     <stop  offset="1" style="stop-color:#F2A246"/>
                  </linearGradient>
                  <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000091705051100733353190000005027636122510766246_);" d="
                     M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                     c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                     c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
               </g>
            </g>
            </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>

      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
               <?php
                  if(isset($_POST['submit'])) 
                  {
                     $name=$_REQUEST['name'];
                     $afflink=$_REQUEST['afflink'];
                     //$picname=$_REQUEST['pic'];
                     /*$tmpname=$_FILES['pic']['tmp_name'];
                     $type=$_FILES['pic']['type'];
                     $size=$_FILES['pic']['size']/1024;
                     $rand=rand(1111111,9999999999);*/
                     if($afflink=="")
                     {
                        echo 'Please fill the details.';
                     }
                     else
                     {
                        /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                        {
                           echo "Please upload image file (size must be less than 1 MB)";	
                        }
                        else
                        {*/
                           //$filename=$rand."-".$picname;
                           //move_uploaded_file($tmpname,"images/".$filename);
                           $url="https://appzilo.co/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                           echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                           //header("Location:$url");
                        //}	
                     }
                  }
               ?>
               <form class="row" action="" method="post" enctype="multipart/form-data">
                  <div class="col-12 form_area ">
                     <div class="col-12 clear">
                        <input type="text" name="name" placeholder="Your Name..." required>
                     </div>

                     <div class="col-12 mt20 clear">
                        <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                     </div>

                     <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                        <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

   <!--Footer Section Start -->
   <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height: 50px;" xml:space="preserve">
               <style type="text/css">
                   .st0{fill:#FFFFFF;}
                   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
               </style>
               <g>
                   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                       c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                       s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
                   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                        M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                       c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
                   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
                   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                       c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                       l0-84.2L505.3,36.7z"/>
                   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
                   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                       c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                       c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                       C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                       c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
               </g>
               <g>
                   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                       <stop  offset="0.5251" style="stop-color:#2089EA"/>
                       <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
                       <stop  offset="0.7855" style="stop-color:#1751C0"/>
                       <stop  offset="1" style="stop-color:#0D1793"/>
                   </linearGradient>
                   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                   
                       <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                       <stop  offset="0.3237" style="stop-color:#F4833F"/>
                       <stop  offset="0.6981" style="stop-color:#F39242"/>
                       <stop  offset="1" style="stop-color:#F2A246"/>
                   </linearGradient>
                   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                       M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                       c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                       c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
               </g>
               </svg>
               
                    <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © AppZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   <!--Footer Section End -->
</body>
</html>
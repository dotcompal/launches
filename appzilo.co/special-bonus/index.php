<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="AppZilo Bonuses">
      <meta name="description" content="AppZilo Bonuses">
      <meta name="keywords" content="Revealing: Promote Brand New 1-Click Mobile App Builder That Lets You Create Unlimited Mobile Apps For iOS & Android">
      <meta property="og:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="AppZilo Bonuses">
      <meta property="og:description" content="Revealing: Promote Brand New 1-Click Mobile App Builder That Lets You Create Unlimited Mobile Apps For iOS & Android">
      <meta property="og:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="AppZilo Bonuses">
      <meta property="twitter:description" content="Revealing: Promote Brand New 1-Click Mobile App Builder That Lets You Create Unlimited Mobile Apps For iOS & Android">
      <meta property="twitter:image" content="https://www.appzilo.co/special-bonus/thumbnail.png">
      <title>AppZilo Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'October 18 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/trzvk1/0';
         $_GET['name'] = 'Ayush Jain';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
               <style type="text/css">
                   .st0{fill:#FFFFFF;}
                   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
               </style>
               <g>
                   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                       c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                       s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                        M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                       c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                       c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                       l0-84.2L505.3,36.7z"></path>
                   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                       c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                       c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                       C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                       c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
               </g>
               <g>
                   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                       <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                       <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                       <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                       <stop offset="1" style="stop-color:#0D1793"></stop>
                   </linearGradient>
                   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   
                       <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                       <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                       <stop offset="0.6981" style="stop-color:#F39242"></stop>
                       <stop offset="1" style="stop-color:#F2A246"></stop>
                   </linearGradient>
                   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                       M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                       c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                       c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
               </g>
               </svg>
                     </div>
                  </div>
                  <div class="preheadline f-md-20 f-18 w600 lh150 mt20 mt-md40">
						   <div class="skew-con d-flex gap20 align-items-center ">
                        <span>Grab My 15 Exclusive Bonuses Before the Deal Ends…</span>
						      
						   </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-42 f-28 w600 text-center white-clr lh140">
                     <span class="cyan-clr"> Revealing: </span>
                     World's Fastest 1-Click Software That <span class="yellow-clr">Creates Unlimited Mobile Apps For iOS & Android <br class="d-none d-md-block"> In 10,000+ Niches </span> 
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                  <div class="col-12 responsive-video border-video">
                        <iframe src="https://appzilo.dotcompal.com/video/embed/e3jbukvizz" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> 
                   <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

            <!-- header List Section Start -->
            <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w600">
                           <ul class="bonus-list pl0 m0">
                              <li>Create Unlimited IOS & Android Apps</li>
                              <li>Comes With Simple Drag n Drop Editor</li>
                              <li>Sell Limitless Apps to Online & Local Businesses</li>
                              <li>Fastest and bug-free delivery</li>
                              <li>Turn Any Website Into Fully Fledged Apps in Seconds</li>
                              <li>No App Store & Play Store Approval</li>
                              <li>Comes With Pre-Built Templates</li>
                              <li>No Technical Skills Required</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w600">
                           <ul class="bonus-list pl0 m0">
                              <li>Google Ads Monetization</li>
                              <li>100% Newbie Friendly</li>
                              <li>One Time Price and Use Forever</li>
                              <li>Work For ANY Business in ANY Niche</li>
                              <li>Comes With Unlimited Commercial License</li>
                              <li>Ultra-Fast Customer Support</li>
                              <li>$200 Refund If Doesn't Work for You</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- header List Section End -->

      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                 
                  Create Your Own Apps &amp; Your Own App Development Agency <span class="blue-clr"> in Just 3 Easy Steps </span>
               </div>
               <div class="col-12 mt30 mt-md80 relative">
                  <div class="row align-items-center gaping">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="step-box">
                           <img src="assets/images/step1.webp" class="img-fluid">
                           <div class="f-28 f-md-40 w600 lh140 mt15">
                              Login
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt15">
                              Login To Our User-Friendly Cloud-Based Software!
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
                  <div class="row align-items-center mt30 mt-md80 gaping">
                     <div class="col-12 col-md-6">
                        <div class="step-box">
                           <img src="assets/images/step2.webp" class="img-fluid">
                           <div class="f-28 f-md-40 w600 lh140 mt15">
                              Select
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt15">
                              Select the design for your Mobile App. Choose format, colors, logo, text as per your brand identity. It's super easy.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 mt20 mt-md0">
                        <img src="assets/images/step-two.webp " class="img-fluid  mx-auto">
                     </div>
                  </div>
                  <div class="row align-items-center mt30 mt-md80 gaping">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="step-box">
                           <img src="assets/images/step3.webp" class="img-fluid">
                           <div class="f-28 f-md-40 w600 lh140 mt15">
                              Publish, Sell, and Profit
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt15">
                              Publish Your App With 1 Click Straight To iOS/ Android &amp; Offer App Development as a professional service in 10,000+ niches with fastest and bug-free delivery.
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
                  <img src="assets/images/line.webp" class="step-line d-none d-md-block">
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AppZilo"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">25</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">03</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
   <div class="premium-sec">
      <div class="container">
         <div class="row">
         <div class="col-12 ">
                  <div class="f-28 f-md-45 w500 lh140 black-clr text-center">
                  See My REAL Earning from Selling Mobile Apps <br class="d-none d-md-block"><span class="blue-clr w700">Built with AppZilo (In 90 Seconds)</span>
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
         </div>
         <div class="row mt40 mt-md80 gx-md-5">
            <div class="col-12 f-md-40 f-28 w500 lh140 black-clr text-center">
            Look At Some More Real-Life Proofs <br>
               <span class="w700">New Freelancers &amp; Agencies are Making Tons of Money</span>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-md-3 mt-md80 mt20">
            <div class="col">
               <img src="assets/images/boomer-smith.webp" alt="" class="img-fluid d-block mx-auto">

            </div>
            <div class="col mt30 mt-md0">
               <img src="assets/images/brain.webp" alt="" class="img-fluid d-block mx-auto">


            </div>
            <div class="col mt30 mt-md0">
               <img src="assets/images/bright.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            
         </div>

      </div>
   </div>
     
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AppZilo"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">32</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">19</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w500 lh140 black-clr text-center">
               And Here're What Top Marketers Say About <br>
               <span class="w700">Our Product Launches &amp; Software Solutions</span>
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     AppZilo is one of the Best Android &amp; iOS Mobile App Builders I have come across. Being a freelancer, it was never easy for me to build mobile applications for my clients from different businesses. And I was able to serve hardly 2-3 clients monthly, and cost more due to the various plugins I must buy.<br><br>
                     <span class="w600">But DANG! AppZilo has changed it over, and now I am serving 20-25 clients per month and making up to $17,000 monthly,</span> with no additional cost of app development, and that is 4X more profits for me.
                     </p>
                     <div class="client-info">
                        <img src="assets/images/client1.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        John Warner
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     AppZilo helped me to create my first real estate Mobile Application in just 30 minutes and the best part is that I am not a tech geek.<br><br>
                     <span class="w600">Now, I'm able to get more FREE traffic &amp; targeted leads online through my App. It helped me reach my prospects at a much lower cost &amp; boosted profits by almost 3 times.</span> This is something that I really won't forget in my life.
                     </p>
                     <div class="client-info">
                        <img src="assets/images/client2.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Chuk Akspan 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     I started my freelance mobile app developer journey 4 months back, but being a newbie, I was struggling in creating a good Applications for Android and iOS.
                     <br><br>But thanks for early access to the AppZilo team, now <span class="w600">I can make a stunning, highly professional Mobile Application quick &amp; easy and able to serve more clients who happily pay me $1000 - $1500 each.</span>
                     </p>
                     <div class="client-info">
                        <img src="assets/images/client3.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Stuart Johnson
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     If someone told me that one can develop as many as 20-25 Mobile Apps in a month for their clients, I'd laugh them off. Because being an App Developer for 5 years, I know how much it takes to create even a single Application.  <br><br>
                     <span class="w600">But after getting beta access to AppZilo, I'm truly stunned. I can create a Professional Android &amp; iOS Application within minutes. Now I can make 4X more profits by serving more and more clients.</span>
                     </p>
                     <div class="client-info">
                        <img src="assets/images/client4.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        David Shaw
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->
      <!--  Email Marketing Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row"><div class="col-12 text-center">
            <div class="title-box f-24 f-md-36 w700 black-clr lh140">Internet Has Changed (For Good!)</div>
            </div></div>
            <div class="row align-items-center mt20 mt-md60">
               <div class="col-12 col-md-6 order-md-2 text-center text-md-start">                 
                  <div class="f-18 f-md-20 w400 lh140">
                  A lot gone were the days when having a website was enough for online presence. <br><br>
In this era of when 3.5 billion people use smartphones and 2/3rd spends are done using mobile phones, having a mobile optimized site is not enough. <br><br>
According to the 2021 US Mobile App Report, <span class="w700">Users spend 87 percent of their time on mobile apps</span> compared to mobile web use that is just 13 percent. <br><br>
In fact, according to Apple, in 2013 1.25 million apps were registered in the Apple app store and accounted for 50 billion downloads and <span class="w700">$5 billion paid to developers.</span>

                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/internet.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md80">
               <div class="col-12 col-md-6  text-center text-md-start">
                  <div class="title-box f-24 f-md-36 w700 black-clr lh140">Ask Yourself!</div>
                  <div class="f-18 f-md-20 w400 lh140 mt15">
                  Wasn't it 5 minutes back when you checked your phone for the email you received, or that notification from Uber or maybe Tinder)?<br><br>
                  We all are likely to check our phones every 10-15 minutes or just tap on notification and take action be it a taxi booking, shopping, online ticket purchasing, or anything.<br><br>
                  Right now, you're likely missing out on one of the most important marketing pieces for any business going forward.

                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/yourself-img.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>


         </div>
      </div>

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                  Presenting… </div>
               </div>
               <div class="col-12 mt-md20 mt20 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:100px;" xml:space="preserve">
               <style type="text/css">
                   .st0{fill:#FFFFFF;}
                   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
               </style>
               <g>
                   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                       c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                       s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                        M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                       c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                       c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                       l0-84.2L505.3,36.7z"></path>
                   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                       c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                       c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                       C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                       c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
               </g>
               <g>
                   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                       <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                       <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                       <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                       <stop offset="1" style="stop-color:#0D1793"></stop>
                   </linearGradient>
                   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   
                       <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                       <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                       <stop offset="0.6981" style="stop-color:#F39242"></stop>
                       <stop offset="1" style="stop-color:#F2A246"></stop>
                   </linearGradient>
                   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                       M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                       c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                       c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
               </g>
               </svg>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="f-md-36 f-24 w700 text-center white-clr lh140">
                  World's Fastest 1-Click Cloud Mobile App Builder That Lets Anyone Create &amp; Sell Unlimited Mobile Apps For iOS &amp; Android In In Just 90 Seconds from Scratch…
                  </div>
               </div>
              
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>            
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AppZilo"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">44</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase AppZilo, You Also Get <br class="hidden-xs"> Instant Access To These 15 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Funnel Hacker
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>If you are an affiliate marketer or digital product owner who aims to have a hugely successful product launch, having an effective sales funnel will help you close more sales to your product. </li>
						      <li>The good news though is that this product will guide you on how to make your sales funnel for the first time & boost its performance effectively in your internet marketing career. </li>
						      <li>This bonus when combined with NinjaKash proves to be a great resource for every success hungry marketer. </li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Insider Guide Template - Sales Page Funnel
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Having high converting sales funnels is one of the most important aspects for every business owner. They help a great deal in converting random prospects into lifetime happy buyers. </li>
						 <li>Keeping this in mind, this exclusive bonus comes loaded with effective strategies to create sales funnels for any audience in any niche & boost profits using them.   </li>
						 <li>You can easily use this bonus software with NinjaKash and create a big business booster to secure best results with greater traffic and more profit. </li>
					 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Funnel Profit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Are you looking for information on WarriorPlus or confused whether WarriorPlus is a legitimate company or not? </li>
						      <li>To solve your doubt, we have come up with a video series that will explain to you what Warrior Plus is and everything you need to know about WarriorPlus funnels. </li>
						      <li>Now, stop thinking and build your affiliate funnels with this software and use with NinjaKash to boost rankings like you always wanted. </li>
					       </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Setup A Video Sales Funnel
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Video sales funnels play a very important role in converting your website visitors into buyers. All you need is to create an engaging one. </li>
							   <li>If you want some proven formulas to create an engaging video sales funnels with which you can expect a huge sale in your offers, this bonus guide will help you learn them. </li>
							   <li>So, what are you waiting for? Combine this package with countless benefits that you get with NinjaKash and take your profits to the next level. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Chatbot Marketing Mastery
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover the very best tools for creating your own custom chatbot without any programming knowledge! </li>
                        <li>With sites like Facebook and Kik opening their platforms to automated messaging for companies, chatbots have really exploded in popularity. Facebook went from zero chatbots in February 2016 to 18,000 by July of the same year.  </li>
                        <li>You've probably seen chatbots in action. They are on all sorts of websites, from major retail chains to mobile phone service providers and many other types of sites and apps. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AppZilo"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Solopreneur Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Being a solopreneur is something that most marketers aspire for, but can't convert that into reality as there are tons of key aspects involved. </li>
                        <li>So, in order to guide you smoothly in the process, we're giving this useful package that helps you become a solopreneur & take complete control of our life. </li>
                        <li>This bonus is a great add-on to the affiliate marketing prowess that NinjaKash possesses and will take your affiliate marketing benefits to the next level. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Make First $100 On The Web
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Making the first dollar online is one of the most important events for every success hungry affiliate marketer. There are tons of gurus and strategies available, but who to trust becomes the most critical decision. </li>
                        <li>Fortunately enough, we're offering this bonus that comes  loaded with battle tested techniques to make your first $100 & scale it to reach cerebral heights. </li>
                        <li>So, get in active mode and use this bonus with NinjaKash to intensify your growth prospects like never before. </li>			
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Outsourcing Software Development
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Small Business Owners Guide To Outsourcing Software Development Successfully!</li>
                        <li>Here is a freelancing scenario that plays out every day. A small business owner understands the importance of having a mobile web presence. They are familiar with the fact that more people search the web on mobile now than they do on traditional desktop computers. </li>
                        <li>That trend grows year after year, so this business savvy entrepreneur decides that having a mobile application is a serious need for their company.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        SnapChat Marketing For Income
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Snapchat is the Fastest Growing Social Media Platform for Driving Targeted Buyer Traffic for Your Offers Without Investing a Fortune! </li>
                        <li>The growing use of the internet has prompted a shift in the nature in which businesses seek to promote their products and services. The use of social media has also gone a long way in increasing internet participation amongst people across all divides. </li>
                        <li>One of the many social media platforms called Snapchat is a multimedia mobile application focused on image messaging. Internet users, especially social media users, have devised ingenious ways of drawing traffic to respected sites for a number of reasons. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Peak Productivity 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This bonus comes with exclusive techniques that are used by top business leaders to boost productivity of their business empire & get known amongst the best. </li>
						       <li>When combined with NinjaKash, this bonus becomes a lethal combination and boosts traffic and profits hands down. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AppZilo"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        SEO Split Testing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>58% B2B marketers accept the fact that SEO produces more leads than any other marketing strategy. So, if you're also looking to use SEO to take your business to the next level, you're at the right place today. </li>
                        <li>With this bonus, you can lean premium SEO hacks & use them to get higher traffic for your offers. </li>
                        <li>So, what are you waiting for? Use the funnel creation techniques mentioned in NinjaKash & drive more traffic on your offers using this bonus. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Lead Generation On Demand
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Driving targeted leads is one of the most crucial aspects for all success hungry marketers. If not given adequate attention, it can give dire consequences. </li>
                        <li>Keeping this in mind, here's a premium bonus to help you drive targeted leads on demand & boost your list like never before.  </li>
                        <li>Now what are you waiting for? Start building your list with NinjaKash and use this package to force them to take action and get best results from your affiliate marketing efforts. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Copywriting Influence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Copywriting holds a great deal of importance for every business owner as copy is the fact of every offer. If not adhered to in an attentive manner, it proves to be fatal in the long run. </li>
						      <li>Now here's the good news for you. With this exciting bonus, creating high converting sales copies &using them to boost sales just got faster & easier. </li>
						      <li>So, stop being a thinker & use these tricks with NinjaKash to grow your affiliate marketing business in a complete manner. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        100 Software Creation Ideas
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Latest PLR Report on 100 Software Creation Ideas!</li>
                        <li>SaaS is one of the latest platform to offer services online. This is a huge for people who are not techie in installing softwares because it is in the cloud. That's why if you are planning to build your apps, the information inside is what you need to get started. </li>
                        <li>As this ebook will give you 100 software creation ideas. You'll get tons of ideas for computer software, mobile/social networking apps, web site scripts and blog plug ins. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Smartphone Apps Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get All The Support And Guidance You Need To Be A Success At Dominating Apps!</li>
                        <li>A good place to get ideas on phone apps is in social forums that target mobile users and developers. Many ideas as well as complaints are found in these forums. The complaints from users are great in terms of feedback as they will tell you what not to do with your app. </li>
                        <li>Developers will also tell you what you should avoid when creating your app due to design complexity and programming tool restrictions. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 15 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab AppZilo + My 15 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
               <style type="text/css">
                   .st0{fill:#FFFFFF;}
                   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
               </style>
               <g>
                   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                       c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                       s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                       c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                       c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                        M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                       c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                       c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                       l0-84.2L505.3,36.7z"></path>
                   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                       c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                       c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                       C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                       c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
               </g>
               <g>
                   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                       <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                       <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                       <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                       <stop offset="1" style="stop-color:#0D1793"></stop>
                   </linearGradient>
                   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                       c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                   
                       <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                       <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                       <stop offset="0.6981" style="stop-color:#F39242"></stop>
                       <stop offset="1" style="stop-color:#F2A246"></stop>
                   </linearGradient>
                   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                       M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                       c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                       c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
               </g>
               </svg>
               
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
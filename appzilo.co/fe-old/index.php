<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>AppZilo - Special Deal</title>
		<!-- Favicon-->
		<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
		<!-- Font Awesome icons (free version)-->
		<script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
		<!-- Google fonts-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Architects+Daughter&display=swap" rel="stylesheet">
		<!-- Core theme CSS (includes Bootstrap)-->
		<link href="../common_assets/css/general.css" rel="stylesheet" />
		<link href="css/styles.css" rel="stylesheet" />
		<link href="css/pages.css" rel="stylesheet" />
		<link href="css/custom.css" rel="stylesheet" />
		<style type="text/css">
			.border_set{
				display: inline-block;
				height: 8px;
				border-radius: 10px;
				width: 230px;
				margin: 0 auto;
				background-color: #02042B;
			}
		</style>

	</head>
	<body id="page-top">
		<!-- Masthead-->
		<header class="masthead text-center">
			<div class="home1 pb50">
				<div class="container">
					<div class="text-center mb20">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
					</div>
					<div class="shadow1">
						<h1 class="lg24 xs14 mb0">World’s #1 & Fastest 1-Click iOS/Android Mobile App Builder <span class="color_red">AT A LOW, ONE TIME PRICE!</span></h1>
					</div>
					<h1 class="color_white lg48 mt30 xs14 color_white">Brand New 1-Click Cloud Based Mobile App Builder <span class="color_orange font-weight-normal">LETS YOU CREATE UNLIMITED MOBILE APPS FOR IOS & ANDROID</span></h1>
					<div class="row">
						<div class="col-sm-8 offset-sm-2">
					<h2 class="lg26 mb30 xs10 color_white bg13 mt40">Sell Unlimited Apps To Online Clients & Local Business With Commercial License</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-7 pt20">
							<div class="video_box  xspt20 mb30">
								<iframe src="https://player.vimeo.com/video/555840515?autoplay=1&loop=1&autopause=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="content_box">
								<div class="text-left color_white lg18 xs10 font-weight-bold">
									<table class="table">
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Create Unlimited iOs & Android Apps </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Comes With Simple Drag n Drop Editor </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Offer App Development as a professional service with </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">fastest and bug-free delivery.</td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">No App Store & Play Store Approval </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Turn Any Website Into Fully Fedge  iOS & Android Apps</td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Comes With Pre Built Templates </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">No Technical Skills Required</td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Google Ads Monetization </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">100% Newbie Friendly </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">No One Time Price and Use Forever </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Work For Any Niche </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">Comes With Unlimited Commercial License </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">24*7 Ultra Fast Support </td>
										</tr>
										<tr>
											<td><i class="far fa-check-square"></i></td>
											<td class="color_white">$200 Refund If Doesn’t Work For You</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</header>
		<section>
			<div class="home1_footer">
				<div class="footer_content  text-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<p class=" lg24 xs16 font_al"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June  DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_3.png" class="img-fluid">
							</div>
							<div class="col-sm-5">
								<div class="box_timer">
									<div class="row color_white">
										<div class="col-sm-5 col-4 text-right">
											<i class="fas fa-hourglass-half"></i>
										</div>
										<div class="col-sm-7 col-8 text-left">
											<h2 class="lg44 xs14">HURRY!</h2>
											<p class="lg20 xs12">Closing Forever In...</p>
										</div>
									</div>
									<div class="box_time">
										<h2 class="color_yellow"><span class="hours">02</span>:<span class="minutes">03</span>:<span class="seconds">16</span></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box_btn mt50">
						<ul>
							<li class="text-right">
								<img src="img/error.png" class="img-fluid">
							</li>
							<li>
								<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>


		<section>
			<div class="home2 pt70 xspt30 xsb20 pb50">
				<div class="container">

					<div class="content">

						<div class="mt50 xsmt20">
							<h3 class=" color_blue text-center lg50 xs21">You Are 3-Simple Steps Away To Create Your Own iOS/Android Apps & <span class="color_red font-weight-normal">Your Own App Development Agency</span></h3>
						</div>
					</div>

					<div class="content mt50">
						<div class="row ">
							<div class="col-sm-5 col-9">
								<div class="img_box ">
									<img src="img/home2_1.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3">
								<div class="text-center " style="color: #0d3f72;">
									<h3 class="lg28 xs18 mb0 pt50">STEP</h3>
									<h4 class="lg60 xs24">01</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
                                <div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 col-12">
								<div class="box_text text-center xsmt30 xsmb30">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 xs14 mt30 xsmt10">Login To Our User Friendly Cloud-Based Software!</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-9 order-sm-3">
								<div class="img_box ">
									<img src="img/home2_2.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 order-sm-2 col-3">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="lg28 mb0 xs18 pt20 xspt70">STEP</h3>
									<h4 class="lg60 xs24">02</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 order-sm-1 pt70 xspt10">
								<div class="box_text text-center xsmt30 xsmb30">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 mt30 xs14 xsmt10">Select the design for your Mobile App. Choose format, colors, logo, text as per your brand identity.</p>
								</div>
							</div>
						</div>
						<div class="row mb50">
							<div class="col-sm-5 col-9">
								<div class="img_box ">
									<img src="img/home2_3.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="lg28 mb0 pt20 xs18 xspt70">STEP</h3>
									<h4 class="lg60 xs24">03</h4>
								</div>

							</div>
							<div class="col-sm-5 pt70 xspt10">
								<div class="box_text text-center">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 xs14 mt30">Publish Your App With 1 Click Straight To iOS/ Android & Offer App Development as a professional service with fastest and bug-free delivery.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section>
			<div class="home3 pt70 xspt30 ">
				<div class="container">

					<div class="content">

						<div class="mt50 xsmt20">
							<h3 class=" color_white text-center lg48 font-weight-normal xs21">SEE Big App Agencies Are <span class="color_orange font-weight-bold">Now Using AppZilo</span> For Creating iOS & Android Apps…</h3>
						</div>
						<div class="mt50 text-center">
							<img src="img/home3_1.png" class="img-fluid">
						</div>
					</div>

				</div>
			</div>
		</section>
		<section>
			<div class="home4 pt100 xspt30 xsb20 pb50">
				<div class="container">

					<div class="content">

						<div class="mt50 xsmt20">
							<h3 class=" color_white text-center lg48 xs21">Even We Used AppZilo To Generate</h3>
							<h3 class=" color_blue text-center lg48 xs21"><b>$17,975</b> In Just 7 Days</h3>
						</div>
						<div class="mt50 text-center">
							<img src="img/home4_1.png" class="img-fluid">
						</div>
					</div>

				</div>
			</div>
		</section>

		<section>
			<div class="home5 pt70 xspt30 ">
				<div class="container">
					<div class="content">
						<h2 class="lg36 color_white text-center">And we are not alone, <span class="color_orange">150+ TOP Agencies and more than 20000+ customers</span> are successfully using it and loving it. Till Now <span class="color_orange"> AppZilo Has Developed 40,792 Apps. IT’S HUGE!!</span></h2>
					</div>
				</div>
				<div class="footer_content mt50 text-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June  DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							<div class="col-sm-5">
								<div class="box_timer">
									<div class="row color_white">
										<div class="col-sm-5 col-4 text-right">
											<i class="fas fa-hourglass-half"></i>
										</div>
										<div class="col-sm-7 col-8 text-left">
											<h2 class="lg44 xs14">HURRY!</h2>
											<p class="lg20 xs12">Closing Forever In...</p>
										</div>
									</div>
									<div class="box_time">
										<h2 class="color_yellow"><span class="hours">02</span>:<span class="minutes">03</span>:<span class="seconds">16</span></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box_btn mt50">
						<ul>
							<li class="text-right">
								<img src="img/error.png" class="img-fluid">
							</li>
							<li>
								<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home6 pt70 pb50 xspt30 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="color_white lg36 text-center">And Here’re What Some More Happy <br>Users Say About AppZilo</h2>
					</div>
					<div class="content mt80 pt30">
						<div class="row">
							<div class="col-sm-10 offset-sm-1">
								<div class="item">
									<div class="text-center">
										<img src="img/abderazzak.png" class="img-fluid rounded-circle">
									</div>
									<p class="lg26 text-center">"Ayush Jain had promoted a lot of my Previous Launches and on each one sent me many sales! Looking forward to him and his team's upcoming Launch. 
											No doubt you guys will crush it with this great product"</p>
									<div class="">
										<h2 class="lg26 text-center">Amit Gaikwad</h2>
									</div>
								</div>
							</div>
							<div class="col-sm-10 offset-sm-1">
								<div class="item">
									<div class="text-center">
										<img src="img/abderazzak.png" class="img-fluid rounded-circle">
									</div>
									<p class="lg28 text-center">"Ayush Jain had promoted my Previous Launches and as of now he sent me many sales and yes it's awesome! I wish all the best for their upcoming Launch. I am damn sure you guys crushed it together"</p>
									<div class="">
										<h2 class="lg26 text-center">Misan Morrison</h2>
									</div>
								</div>
							</div>
							<div class="col-sm-10 offset-sm-1">
								<div class="item">
									<div class="text-center">
										<img src="img/abderazzak.png" class="img-fluid rounded-circle">
									</div>
									<p class="lg28 text-center">"Akshat is a really passionate Marketer, He bring some amazing products to market. Their next product AppZilo is launching on 11th June, I’ve checked it and looks very impressive . I’m sure it'll be a sure fire winner, Best of luck guys."</p>
									<div class="">
										<h2 class="lg26 text-center">Declan Mc</h2>
									</div>
								</div>
							</div>
							<div class="col-sm-10 offset-sm-1">
								<div class="item">
									<div class="text-center">
										<img src="img/abderazzak.png" class="img-fluid rounded-circle">
									</div>
									<p class="lg28 text-center">"Ayush Jain is a very Talented Marketer in this industry. Also he is a great Product Creator &amp; Affiliate Marketer . He is always supporting us and other too. I highly recommended him. All the very best for your upcoming launch AppZilo"</p>
									<div class="">
										<h2 class="lg26 text-center">David Kirby</h2>
									</div>
								</div>
							</div>
						</div>					   
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home7 ">
				<div class="container">
					<div class="content">
						<div class="row">
							<div class="col-sm-7">
								<img src="img/home7_1.png" class="img-fluid">
							</div>
							<div class="col-sm-5">
								<h2 class="lg36 text-center">Internet has changed</h2>
								<p class="lg20">A lot Gone were the days when having a website was enough for online presence. In this era of when 3.5 billion people use smartphones and 2/3rd spends are done using mobile phones, having a mobile optimized site is not enough. </p>
								<p class="lg20">According to the 2017 US Mobile App Report, Users spend 87 percent of their time on mobile apps compared to mobile web use that is just 13 percent. On top of that, half of digital media usage time is spent using smartphone apps </p>
							</div>
						</div>
						<div class="row mt50">
							<div class="col-sm-7 order-sm-2">
								<img src="img/home7_2.png" class="img-fluid">
							</div>
							<div class="col-sm-5 order-sm-1">
								<h2 class="lg36 text-center">Ask Yourself! </h2>
								<p class="lg20">Wasn’t it 5 minutes back when you checked your phone for the email you received, or that notification from Uber or maybe Tinder ;)? </p>
								<p class="lg20">We all are likely to check our phones every 10-15 minutes or just tap on notification and take action be it a taxi booking, shopping, online ticket purchasing, or anything. </p>
								<p class="lg20">Right now, you’re likely missing out on one of the most important marketing pieces for any business going forward.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home8 pt70 pb50 xspt30 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="lg50 xs22 text-center">Every Business Deserves An App, <span class="color_white">Period!</span></h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_1.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">eCommerce Store</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_2.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Reatiler</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_3.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Saloon</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_4.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Tutors</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_5.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Fitness trainer</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_6.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Influencers</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_7.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Doctors</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_8.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Lawyers</h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home8_9.png" class="img-fluid">
									<h3 class="lg30 xs16 text-center mt20">Restaurants</h3>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="home9">
				<div class="header pt70 xspt30">
					<div class="container">
						<h2 class="text-center lg50 xs24 color_white"><span>Still Confused!?</span></h2>
						<h3 class="text-center color_orange lg36 font-weight-normal">Let Us Hand You The Biggest Money-making <br>Opportunity On Earth...</h3>
						<p class="text-center lg24 color_white">Imagine if minutes from now you could start getting paid big fat $1,000+ sales straight to your bank account… from hungry businesses that can’t get enough of your services. </p>
						<p class="text-center lg24 color_white">On top of that, imagine if you had access to a $700B+ industry that is THRIVING in the current economy, so you could get virtually unlimited, untapped traffic 24/7 and sell any product or service.</p>
						<p class="text-center lg24 color_white"> Finally, imagine if you could press a single button and have any offer BLASTED directly to your customers’ phones and lock-screens… complete with that sweet notification “PING!”</p>
						<p class="text-center lg24 color_white">That’s right – a virtually unlimited, untapped 24/7 traffic stream that maximizes your profit with zero work for you.</p>
						<h3 class="text-center color_orange lg36  font-italic">AND THIS IS ALL...</h3>
					</div>
				</div>
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home9_1.png" class="img-fluid">
									<h2 class="lg36 font-italic mt20">100% Newbie Friendly</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home9_2.png" class="img-fluid">
									<h2 class="lg36 font-italic mt20">Done for youy</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home9_3.png" class="img-fluid">
									<h2 class="lg36 font-italic mt20">$200 Refund if it doesn’t work for you (it will!)</h2>
								</div>
							</div>
						</div>
						<div class="content_box">
							<h2 class="text-center lg48 color_red"><img src="img/smiley1.png" class="img-fluid mr-2">HOW...?!<img src="img/smiley.png" class="img-fluid ml-2"></h2>
							<p class="lg36 text-center">Simple - By tapping into the fastest growing industry <br>on Earth right now!</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="home10 pt70 xspt30 pb50 xspb20">
				<div class="header">
					<div class="container">
						<h2 class="lg52 text-center color_white">Mobile Apps are ruling the <br>engagement game</h2>
					</div>
				</div>
				<div class="content">
					<div class="row1">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 offset-sm-6">
									<h2 class="color_white lg36 ">Fast Speed</h2>
									<p class="color_white lg24 mt30">Mobile apps are supremely fast and 3X more faster loading than regular websites. I don’t need to explain how important it is for your business but let’s look at the number</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row2">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 ">
									<h2 class="color_white lg36 ">Engaging</h2>
									<p class="color_white lg24 mt30">Once an app is installed, it’s here to stay and most likely to be visited again and again. With Push notifications you can literally pull back your visitors, visually force them to see what you want to show and make them buy what you offer </p>
									<p class="color_white lg24 mt30">Unlike old school SMS or Emails with only text or emojis stacking over and creating a junk in inbox, Push notifications have over 95% open rate with priority action</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home11 pt70 xspt30 pb50 xspb20">
				<div class="header">
					<div class="container">
						<h2 class="text-center lg52 xs26">Why not everyone has <br>an App already?</h2>
						
					</div>
				</div>
				<div class="content">
					<h2 class="text-center lg36 xs18 color_white font-weight-normal">JUST 1% OF COMPANIES CURRENTLY HAVE <br>THEIR OWN MOBILE APPS...!</h2>
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<h2 class="lg36 mt80">Common Issues: </h2>
								<ul class="mt30 lg24 color_white mt50">
									<li>Most of people don’t even know about this futuristic piece of tech</li>
									<li>Not everyone can spend 100s or even 1000s of dollars on development for PWAs</li>
									<li>They don’t want to hire a team of developers and designers</li>
									<li>Find it hard to install heavy software and plugins</li>
									<li>Don’t realize how powerful it can be to directly connect almost all of the visitors</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home12 pt70 xspt30">
				<div class="container">
					<div class="content">
						<h2 class="lg42 xs20 text-center"><span>Looking For Solution ?</span></h2>
						<h3 class="color_white lg36 xs18 text-center mt50 mb50">PROUDLY PRESENTING</h3>
						<div class="row">
							<div class="offset-sm-2 col-sm-8">
								<img src="img/product.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home13 pt70 xspt30">
				<div class="container">
					<div class="header">
						<h2 class="lg52 xs26 text-center">See How We Created an App in</h2>
						<h2 class="lg52 xs26 color_white text-center">Less than 90 Seconds</h2>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-8 offset-sm-2">
								<div class="video_box  xspt20 mb30">
								<iframe src="https://player.vimeo.com/video/555840515" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="home13b pt100">
					<div class="footer_content mt50 text-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June  DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							<div class="col-sm-5">
								<div class="box_timer">
									<div class="row color_white">
										<div class="col-sm-5 col-4 text-right">
											<i class="fas fa-hourglass-half"></i>
										</div>
										<div class="col-sm-7 col-8 text-left">
											<h2 class="lg44 xs14">HURRY!</h2>
											<p class="lg20 xs12">Closing Forever In...</p>
										</div>
									</div>
									<div class="box_time">
										<h2 class="color_yellow"><span class="hours">02</span>:<span class="minutes">03</span>:<span class="seconds">16</span></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box_btn mt50">
						<ul>
							<li class="text-right">
								<img src="img/error.png" class="img-fluid">
							</li>
							<li>
								<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
							</li>
						</ul>
					</div>
				</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home2 pt70 xspt30 xsb20 pb50">
				<div class="container">

					<div class="content">

						<div class="mt50 xsmt20">
							<h3 class=" color_blue text-center lg50 xs21">You Are 3-Simple Steps Away To Create Your Own iOS/Android Apps & <span class="color_red font-weight-normal">Your Own App Development Agency</span></h3>
						</div>
					</div>

					<div class="content mt50">
						<div class="row ">
							<div class="col-sm-5 col-9">
								<div class="img_box ">
									<img src="img/home2_1.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3">
								<div class="text-center " style="color: #0d3f72;">
									<h3 class="lg28 xs18 mb0 pt50">STEP</h3>
									<h4 class="lg60 xs24">01</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
                                <div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 col-12">
								<div class="box_text text-center xsmt30 xsmb30">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 xs14 mt30 xsmt10">Login To Our User Friendly Cloud-Based Software!</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-9 order-sm-3">
								<div class="img_box ">
									<img src="img/home2_2.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 order-sm-2 col-3">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="lg28 mb0 xs18 pt20 xspt70">STEP</h3>
									<h4 class="lg60 xs24">02</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 order-sm-1 pt70 xspt10">
								<div class="box_text text-center xsmt30 xsmb30">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 mt30 xs14 xsmt10">Select the design for your Mobile App. Choose format, colors, logo, text as per your brand identity.</p>
								</div>
							</div>
						</div>
						<div class="row mb50">
							<div class="col-sm-5 col-9">
								<div class="img_box ">
									<img src="img/home2_3.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="lg28 mb0 pt20 xs18 xspt70">STEP</h3>
									<h4 class="lg60 xs24">03</h4>
								</div>

							</div>
							<div class="col-sm-5 pt70 xspt10">
								<div class="box_text text-center">
									<img src="img/home2_12.png" class="img-fluid">
									<p class="color_blue lg24 xs14 mt30">Publish Your App With 1 Click Straight To iOS/ Android & Offer App Development as a professional service with fastest and bug-free delivery.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home14 pt70 pb50 xspt30 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="color_white lg52 xs26 text-center">That's Not All with AppZilo <br>You Can Also Tap Into so many features</h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_1.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Cloud Based Software Creates Unlimited iOS & Android Mobile Apps</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_2.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">IOS And Android Compatible Gets You Published On The Biggest Markets On EARTH</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_3.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Drag & Drop Editor Lets You Tweak Everything With ZERO Coding Involved</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_4.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Awesome DFY Breath-Taking App Templates In Any Local & Online Niche</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_5.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">COMMERCIAL LICENSE To Sell These Apps To Clients Or Build Apps For Them From Scratch</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_6.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Turn ANY Website (Yours Or Clients) Into Fully-Fledged Mobile Apps</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_7.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">No Need To Pay Or Register A Developer Account</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_8.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Designed By Marketers For Marketers</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_9.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Super Reliable Technology</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_10.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Built-In Training To Get Your Apps Published And Generating $1,000+/App</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_11.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">Send Unlimited App Notifications To Customers Phones And Lock Screens</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_12.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">SSL Encryption Keeps Your Apps Secure</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_13.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">SEO Optimization Gets Your Apps To The Top Of Search Results</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_14.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">24/7 “White Glove” Support</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="item mb50 text-center">
									<img src="img/home14_15.png" class="img-fluid">
									<h2 class="lg24 xs12 mt20 text-center color_white font-weight-normal">And So Much More</h2>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>

		<section>
			<div class="home15 pt70 xspt30 ">
				<div class="container">
					<div class="header text-center">
						<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
							Get Instant Access to
						</a>
					</div>
					<div class="content mt50">
						<h2 class="lg52 xs26 color_blue"><span class="color_red">AppZilo Today</span> the only time it’s ever going <br>to be available for a one-time, Low Fee!</h2>
					</div>
				</div>
				<div class="home15b pt50 mt50">
					<div class="footer_content  text-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-7">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June  DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							<div class="col-sm-5">
								<div class="box_timer">
									<div class="row color_white">
										<div class="col-sm-5 col-4 text-right">
											<i class="fas fa-hourglass-half"></i>
										</div>
										<div class="col-sm-7 col-8 text-left">
											<h2 class="lg44 xs14">HURRY!</h2>
											<p class="lg20 xs12">Closing Forever In...</p>
										</div>
									</div>
									<div class="box_time">
										<h2 class="color_yellow"><span class="hours">02</span>:<span class="minutes">03</span>:<span class="seconds">16</span></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box_btn mt50">
						<ul>
							<li class="text-right">
								<img src="img/error.png" class="img-fluid">
							</li>
							<li>
								<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home16 pt70 pb50 xspt30 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="lg48 xs24 text-center color_white">WITH AppZilo, WE ARE GENERATING</h2>
						<h2 class="lg48 xs24  text-center  font-weight-normal font-italic">INCOME LIKE THIS IN EVERY MONTH...!</h2>
					</div>
					<div class="content mt50 text-center">
						<img src="img/home16_1.png" class="img-fluid">
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home17 pt70 pb50 xspt30 xspb20 ">
				<div class="container">
					<div class="content">
						<h2 class="color_white lg36 text-center">WHAT MORE REAL MOBILE APP & ONLINE <br>EXPERTS HAVE TO SAY ABOUT IT…</h2>
						<div class="mt50 text-center">
							<img src="img/home17_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
      <div class="home18  xspt60" >
        <div class="container">
        	<div class="header text-center color_blue">
            <h2 class="lg28 xs14">Sounds like Deal?<br>! But we made it even more mouth-watering! </h2>
            <h2 class="mt30 xs14 xsmt10">Check Out these Bonuses You'll Get for <br>FREE If You GRAB THIS PROFITABLE BUSINESS OPPORTUNITY Today. </h2> 
          </div>
          <div class="content mt50">
            <div class="item">
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-3 col-6 offset-3 offset-sm-0">
                  <div class="img_box">
                    <img src="img/1.png" class="img-fluid">
                  </div>
                </div>
                <div class="col-sm-7 pt30">
                  <h2 class="lg30 bg_yellow2 xs14 "> CashKing Pro</h2>
                  <p class="lg20 xsmt0 mt20 xs10 xsmb0 ">Are You Unable To Articulate Your Business Concept? Is Your Website Not Generating The Kind Of Traffic You Desire? Get On Top Of The Search Engine Results Page With Expert Content! Make your website traffic increase manifold by putting in expert PLR content!</p>
                  <p class="lg28 xs13 mt20 xsmt10 "><b>Value - $227 </b></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-sm-1 order-sm-1"></div>
                <div class="col-sm-3 col-6 offset-sm-0 offset-3 order-sm-3">
                    <div class="img_box xsmt50">
                        <img src="img/2.png" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-7 order-sm-2 pt30">
                    <h2 class="lg30 bg_yellow2 xs14 ">Smach The Cash Secrets</h2>
                    <p class="lg20 xsmt0 mt20 xs10 xsmb0 ">You will learn 30 different ways of how you can be making money online. Sell your product in a package deal with other web businesses. You can both advertise it and split the profits.</p>
                    <p class="lg28 xs13 mt20 xsmt10 "><b>Value - $667 </b></p>
                </div>                
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-3 col-6 offset-3 offset-sm-0">
                  <div class="img_box xsmt50">
                      <img src="img/3.png" class="img-fluid">
                  </div>
                </div>
                <div class="col-sm-7 pt30">
                  <h2 class="lg30 bg_yellow2 xs14 "> 100+ Marketing Emails</h2>
                  <p class="lg20 xsmt0 mt20 xs10 xsmb0 ">Your complete "fill in the blank" autoresponder series you can use for ALL your internet marketing lists and is geared toward TEN main areas of your business!</p>
                  <p class="lg28 xs13 mt20 xsmt10 "><b>Value - $567 </b></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-sm-1 order-sm-1"></div>
                <div class="col-sm-3 col-6 offset-sm-0 offset-3 order-sm-3">
                    <div class="img_box xsmt50">
                        <img src="img/4.png" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-7 order-sm-2 pt30">
                  <h2 class="lg30 bg_yellow2 xs14 "> 80% Discount on ALL Upgrades</h2>
                  <p class="lg20 xsmt0 mt20 xs10 xsmb0 ">Get 80% instant discount on purchase of All Upgrades. This is very exclusive bonus which will be taken down soon.</p>
                  <p class="lg28 xs13 mt20 xsmt10 "><b>Value - $297 </b></p>
                </div>
                
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-3 col-6 offset-3 offset-sm-0">
                  <div class="img_box xsmt50">
                      <img src="img/5.png" class="img-fluid">
                  </div>
                </div>
                <div class="col-sm-7 pt30">
                  <h2 class="lg30 bg_yellow2 xs14 "> 30 Agency License</h2>
                  <p class="lg20 xsmt0 mt20 xs10 xsmb0 ">You have full rights to use this software. <br><br>You can use it for anyone whether for individuals or for companies. Generate massive free traffic to yourself and for others as well.</p>
                  <p class="lg28 xs13 mt20 xsmt10 "><b>Value - $997 </b></p>
                </div>
              </div>
            </div>
          </div>
          <div class="header text-center color_white">
            <h2 class="mt30 xsmt10 xs14">Total value of these bonuses are $5857 <br>But Today you will take all these bonuses home for FREE! </h2>
          </div>
        </div>
        <div class="home15b pt50 mt50">
	      	<div class="footer_content  text-center">
	          <div class="container">
	            <div class="row">
	              <div class="col-sm-7">
	                <p class="lg24 xs12 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June Discount</p>
									<button class="btn_custom_set buy_btn">
	                    Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
	                </button>                       
	                <p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
	                <img src="img/home1_2.png" class="img-fluid">
	              </div>
	              <div class="col-sm-5">
									<div class="box_timer">
										<div class="row color_white">
											<div class="col-sm-5 col-4 text-right">
												<i class="fas fa-hourglass-half"></i>
											</div>
											<div class="col-sm-7 col-8 text-left">
												<h2 class="lg44 xs14">HURRY!</h2>
												<p class="lg20 xs12">Closing Forever In...</p>
											</div>
										</div>
										<div class="box_time">
											<h2 class="color_yellow"><span class="hours">00</span>:<span class="minutes">56</span>:<span class="seconds">23</span></h2>
										</div>
									</div>
								</div>
	            </div>
	          </div>
	          <div class="box_btn mt50">
							<ul>
								<li class="text-right">
									<img src="img/error.png" class="img-fluid">
								</li>
								<li>
									<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
								</li>
							</ul>
						</div>
	        </div>
	      </div>
      </div>
      
  	</section>

	  <div class="table-section" id="buynow">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get AppZilo for A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 w500 text-center lh140 black-clr mt15">
                                 <li>Groundbreaking Email Marketing Technology</li>
                                 <li>100% Cloud-Based Technology</li>
                                 <li>Unlimited Emails with Free SMTP</li>
                                 <li>Schedule Emails Anytime with a push of a button</li>
                                 <li>Smart Tagging &amp; Segmenting</li>
                                 <li>List Management</li>
                                 <li>Automation and Autoresponder</li>
                                 <li>Inbuilt Text &amp; Inline Editor </li>
                                 <li>Stunning Optin Forms</li>
                                 <li>Ready-To-Use Templates</li>
                                 <li>CAN SPAM compliant </li>
                                 <li>Commercial Use License </li>
                                 <li>Beginner Friendly</li>
                                 <li>Fully Automated. 100% Hands-Free</li>                                
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1"><img src="https://warriorplus.com/o2/btn/fn200011000/xgwy6m/y4pjnq/320688" class="img-fluid d-block mx-auto"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div>

  	<section>
  		<div class="home19 pt70 xspt30 ">
  			<div class="container">
  				<div class="text-center">
  					<img src="img/home19_1.png" class="img-fluid">
  				</div>
  			</div>
  			<div class="home15b pt50 mt50">
	      	<div class="footer_content  text-center">
	          <div class="container">
	            <div class="row">
	              <div class="col-sm-7">
	                <p class="lg24 xs12 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - 81% June Discount</p>
									<button class="btn_custom_set buy_btn">
	                    Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
	                </button>     
	                <p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
	                <img src="img/home1_2.png" class="img-fluid">
	              </div>
	              <div class="col-sm-5">
									<div class="box_timer">
										<div class="row color_white">
											<div class="col-sm-5 col-4 text-right">
												<i class="fas fa-hourglass-half"></i>
											</div>
											<div class="col-sm-7 col-8 text-left">
												<h2 class="lg44 xs14">HURRY!</h2>
												<p class="lg20 xs12">Closing Forever In...</p>
											</div>
										</div>
										<div class="box_time">
											<h2 class="color_yellow"><span class="hours">00</span>:<span class="minutes">56</span>:<span class="seconds">23</span></h2>
										</div>
									</div>
								</div>
	            </div>
	          </div>
	          <div class="box_btn mt50">
							<ul>
								<li class="text-right">
									<img src="img/error.png" class="img-fluid">
								</li>
								<li>
									<p class="text-left color_white lg18 mb-0 xs10"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br> Now To Claim Your <u class="lg20 xs11"><i>81% DISCOUNT</i></u> Or Come Later &amp; Pay More</p>
								</li>
							</ul>
						</div>
	        </div>
	      </div>
  		</div>
  	</section>

	  <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
  	

  	<section>
  		<div class="home21 pt70 xspt30 pb50 xspb20">
    		<div class="container">
  				<div class="row">
  					<div class="col-sm-10 offset-sm-1">
  						<div class="content_box">
	  						<h2 class="text-center lg50 mt50">Many Showers Of <span style="color: #fccc00">Success</span></h2>
	  						<div class="img_box">
	  							<div class="row">
	  								<div class="col-sm-3 offset-sm-3">
											<div class="text-center">
												<img src="img/ayush-jain.png" class="img-fluid rounded-circle" style="max-width: 150px;">
												<h2 class=" lg18 mt20">Ayush Jain</h2>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="text-center">
												<img src="img/pranshu-gupta.png" class="img-fluid rounded-circle" style="max-width: 150px;">
												<h2 class=" lg18 mt20">Pranshu</h2>
											</div>
										</div>
	  							</div>
	  						</div>
	  						<p class=" mt30 lg36 text-center">P.S. Get AppZilo – World’s First & Fastest 1-Click iOS & Android Mobile App Builder, while we’re excited to offer unrestricted access for a single price … We can only do that during this Limited launch. </p>
	  						<p class=" lg36 text-center">As soon as launch closes, the price WILL increase to a monthly membership. Get everything now for a low one-time fee … Or risk missing out & pay much more at MONTHLY price later. The decision is yours</p>
	  					</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>
  	<section>
  		<div class="home22 pt70 pb50 ">
				<div class="container">
					<div class="header">
            <h2 class="lg42 text-center color_blue">Frequently <span style="color: #ee1539;">Asked</span> Questions</h2><br>
					</div>
					<div class="content">
						<ul>
							<li>
								<h2 class="lg26">Q.1 Is this a desktop software? Does it work on a PC?</h2>
								<p class="lg18">This is a Cloud-Based App that doesn’t require any Installation. Simply access it from anywhere, anytime in the world just as long as you have an internet connection!</p>
							</li>
							<li>
								<h2 class="lg26">Q.2 I already have such a tool. What do I do?</h2>
								<p class="lg18">You don’t have such a tool. What AppZilo does…there’s no other tool out there that does that. We checked before we got started. Sure, you June have a tool for creating videos – but what you’re getting today is just extraordinary. </p>
								<p class="lg18">My sincere advice to you would be to cancel your existing subscription and get grandfathered access to AppZilo at a low one-time price right now.</p>
							</li>
							<li>
								<h2 class="lg26">Q.3 Do I need video creation or editing experience?</h2>
								<p class="lg18">None at all! The dashboard & canvas make creating custom videos literally point & click simple. We’ve tested this interface with brand new beginners to ensure it’s 1-2-3 simple!</p>
							</li>
							<li>
								<h2 class="lg26">Q.4 Can I create & sell videos to clients?</h2>
								<p class="lg18">Yes! You can create an unlimited amount of videos and sell them to businesses when you purchase our Commercial License package during the launch period. There are no limits. Set the price you want to charge and keep 100% of the profits. You will never have to pay us any royalties or additional fees on the money you make</p>
							</li>
							<li>
								<h2 class="lg26">Q.5 Are there any training videos included?</h2>
								<p class="lg18">Yes! Even though AppZilo is simple and easy to use, we make it even easier for you with step-by-step training tutorials to get up and running even faster.<br>
							</p></li>
							<li>
								<h2 class="lg26">Q.6 How much do updates cost?</h2>
								<p class="lg18">Yes! We are constantly working on updating the software and providing you with the latest patches. When you purchase AppZilo updates are automatic and provided free of charge.<br>
							</p></li>
							<li>
								<h2 class="lg26">Q.7 How do I get support?</h2>
								<p class="lg18">Easy! Just email us  and we will gladly answer any questions you June have. <br>
							</p></li>
						</ul>
					</div>
				</div>
			</div>
  	</section>
  		


		<!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->

		<!--<section>
      <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title"><i class="fas fa-hand-paper"></i> WAIT!!!<br>
                          <span>Don't Leave Empty Handed!</span>
                      </h5>

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <h2><u>Copy This Code</u> "AppZilo5" </h2><h3>To Get Your <span><u>$5 Discount</u> Now!</span></h3>
                      <div class="button text-center">
                          <a href="https://warriorplus.com/o2/buy/qrmz2j/qd725w/rck3rw" class="btn_custom_set1" target="_blank">Yes! - Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>


				<!-- Bootstrap core JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
		<!-- Third party plugin JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		<!-- Contact form JS-->
		<script src="assets/mail/jqBootstrapValidation.js"></script>
		<script src="assets/mail/contact_me.js"></script>
		<!-- Core theme JS-->
		<!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
		<script type="text/javascript" src="jquery.exit-modal.js"></script>
		<script src="js/scripts.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){

				var timer;

				var exitModalParams = {
				    numberToShown:                  5,
				    callbackOnModalShow:            function() {
				        var counter = $('.exit-modal').data('exitModal').showCounter;
				        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
				    },
				    callbackOnModalShown:           function() {
				        timer = setTimeout(function(){
				            // window.location.href = "http://www.jqueryscript.net";
				        }, 4000)
				    },
				    callbackOnModalHide:            function() {
				        clearTimeout(timer);
				    }
				}

				$('.destroy-exit-modal').on("click", function(e){
					e.preventDefault();
					if($('.exit-modal').data('exit-modal')) {
						$(".initialized-state").hide();
						$(".destroyed-state").show();
					}
					$('.exit-modal').exitModal('hideModal');
					$('.exit-modal').exitModal('destroy');
					$(".initialized").hide();
				});

				// $('.init-exit-modal').on('click', function(e){
					// e.preventDefault();
					$('.exit-modal').exitModal(exitModalParams);
					if($('.exit-modal').data('exit-modal')) {
						$(".destroyed-state").hide();
						$(".initialized-state").show();
					}
				// });

				$('.close-exit-modal').on('click', function(e){
					e.preventDefault();
					$('.exit-modal').exitModal('hideModal');
				});

			});
			$(".buy_btn").click(function() {
				$('html, body').animate({
					scrollTop: $("#take_look").offset().top
				}, 1000);
			});
			function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading')
				.find(".more-less")
				.toggleClass('glyphicon-plus glyphicon-minus');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);
		</script>
		<?php
		$rand = rand(21000,30000);
			$today = date('Y-m-d H:i:s');
			$date = date('M d, Y H:i:s', strtotime($today)+$rand);
		?>
		<script type="text/javascript">
			countdownTimeStart();
			function countdownTimeStart(){
				var countDownDate = new Date("<?= $date;?>").getTime();
				var x = setInterval(function() {
					var now = new Date().getTime();

					// Find the distance between now an the count down date
					var distance = countDownDate - now;
					
					// Time calculations for days, hours, minutes and seconds
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					
					// Output the result in an element with id="demo"
					times = hours + "h "
					+ minutes + "m " + seconds + "s ";

					if (hours <= 9) 
					{
						hours = '0'+hours;
					}
					if (minutes <= 9) 
					{
						minutes = '0'+minutes;
					}
					if (seconds <= 9) 
					{
						seconds = '0'+seconds;
					}
					$('.hours').html(hours);
					$('.minutes').html(minutes);
					$('.seconds').html(seconds);

					// console.log(times);
					
					// If the count down is over, write some text 
					if (distance < 0) {
						clearInterval(x);
						document.getElementById("demo").innerHTML = "EXPIRED";
					}
				}, 1000);
			}
		</script>
	</body>
</html>
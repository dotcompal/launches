<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>AppZilo - DFY</title>
		<!-- Favicon-->
		<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
		<!-- Font Awesome icons (free version)-->
		<!-- Font Awesome icons (free version)-->
		<script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
		<!-- Google fonts-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">

		<!-- Core theme CSS (includes Bootstrap)-->
		<link href="css/styles.css" rel="stylesheet" />
		<link href="css/custom.css" rel="stylesheet" />
		<style type="text/css">
			.border_set{
				display: inline-block;
				height: 8px;
				border-radius: 10px;
				width: 230px;
				margin: 0 auto;
				background-color: #02042B;
			}
		</style>

	</head>
	<body id="page-top">
		<!-- Navigation-->
		
		<!-- Masthead-->
		<header class="masthead text-white text-center">		
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				</div>
			</div>
		</div>
			<div class="home1 xsmt40 mt40">
				<div class="container">
					<div class="pre-heading"><div class="font-weight-bold color_white xs16 lg22 xs18">Get ready to make over $589+ by offering these 50+ DFY Products!</div></div>
					
					<h2 class="font-weight-bold color_white lg44 mt30 xs28">Get <span class="yellow-clr">Access to 24/7 Income Stream </span>  to Reap Heavy Profit with High-performance DFY 50 Products</h2>
					<div class="post-heading">
						<h2 class="font-weight-bold color_white bg2 xs18 lg24 mb30 xs18">Unlock Zero Work and  Profit Formula</h2>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="video_box pt20 xspt20 mb30">
								<img src="img/product-box.webp" class="img-fluid">
								<!-- <iframe title="vimeo-player" src="https://player.vimeo.com/video/549195473" width="540" height="315" frameborder="0" allowfullscreen></iframe>	-->				
							</div> 
						</div>
						<div class="col-sm-6 xspt20 pt70">
							<div class="points text-left lg20 xs18 lg24">
								<table class="table">
									<tr>
										<td style="color: #003AFD;"><i class="fas fa-check-circle"></i></td>
										<td class="color_white">100% Easy To Sell </td>
									</tr>
									<tr>
										<td style="color: #003AFD;"><i class="fas fa-check-circle"></i></td>
										<td class="color_white">$100 Refund If This Doesn’t Work For You </td>
									</tr>
									<tr>
										<td style="color: #003AFD;"><i class="fas fa-check-circle"></i></td>
										<td class="color_white">DFY Everything For You </td>
									</tr>
									<tr>
										<td style="color: #003AFD;"><i class="fas fa-check-circle"></i></td>
										<td class="color_white">Make Money Like Big Boys </td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs18 font_al white-clr">(Get AppZilo  at one-time Price right away, because we're going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs16 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs18">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs18"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		<section>
			<div class="home2 pt70 pb50">
				<div class="container">
					<div class="header">
						<div class="row">
							<div class="col-sm-5">
								<img src="img/home2_1.png" class="img-fluid">
							</div>
							<div class="col-sm-7">
								<div class="text_box mt100">
									<img src="img/home2_5.png" class="img-fluid">
									<h2 class="font-weight-normal xs22 lg32 xs22">Here’s how you can simply</h2>
									<h2 class="font-weight-normal xs22 lg32 xs22">Triple your Income with</h2>
									<h3 class="font-weight-normal lg46 xs24">AppZilo</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-12">
								<h2 class="font-weight-normal xs28 lg42 text-center">As of now you’ve already know</h2>
								<h2 class="xs28 lg42 text-center">The Power of AppZilo...</h2>
								<h2 class="font-weight-normal xs24 lg36 text-center mb50">But What if you can Make more money with it?</h2>
								<h3 class="xs22 lg32 bg3 color_white text-center mb50">Let’s face it the biggest rewards come from selling your own software apps</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="box text-center">
									<img src="img/home2_2.png" class="img-fluid">
									<p class="mt30 xs18">Start charging Individuals and businesses to use these professional Software projects.</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center">
									<img src="img/home2_3.png" class="img-fluid">
									<p class="mt30 xs18">Just Sit-back and relax while making huge profits by selling Softwares</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center">
									<img src="img/home2_4.png" class="img-fluid">
									<p class="mt30 xs18">Offer a DFY Bundle of Software or charge individually for each project.</p>
								</div>
							</div>
						</div>
						
					</div>
					<div class="footer_text mt50 xsmt20">
						<div class="row">
							<div class="col-sm-8 offset-sm-2">
								<div class="box_set">
									<h2 class="text-center lg24 xs18 font-weight-normal">Here's the Earning That We're reaping by Selling 50 DFY Products & Now you can also replicate this formula and make money like this TOO!</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="text-center mt50">
									<img src="img/home2_6.png" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</section>

		<section>
			<div class="home4 pt70">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs28 lg42 xs20">We Will Take Care Of Everything For You...</h2>
						<h2 class="text-center xs24 lg36 xs18">Here are the benefits you are getting</h2>
						<h3 class="text-center xs28 lg42 xs20" style="color: #E44300;">Today when Upgrading to DFY Version</h3>
					</div>
					<div class="content mt30">
						<div class="row">
							<div class="col-sm-2 offset-sm-1">
								<div class="item">
									<img src="img/home4_1.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Get 50 ready to sell Done-for-you Software</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_2.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">All Software comes with Reseller License</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_3.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">You can use these same softwarefor your own use</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_4.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Get a full-fledge working business model</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_5.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Get dedicated Support team to mange your client's ticket connection</h3>
								</div>
							</div>	
						</div>
						<div class="row">
							<div class="col-sm-2 offset-sm-3">
								<div class="item">
									<img src="img/home4_6.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">We will handle support for you</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_7.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Get all the sales-material to sell your offer</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_8.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Get commercial license and charge others to use your software</h3>
								</div>
							</div>	
						</div>
						<div class="row">
							<div class="col-sm-2 offset-sm-4">
								<div class="item">
									<img src="img/home4_9.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Search Engine Optimized Online platform for business</h3>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="item">
									<img src="img/home4_10.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">Don't loose traffic with mobile optimized web platform</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-2 offset-sm-5">
								<div class="item">
									<img src="img/home4_11.png" class="img-fluid">
									<h3 class="font-weight-normal lg16 text-center mt20">And Much More...</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</section>
		<div class="cta-button mt50 text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs18 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs16 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs18">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs18"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		<section>
			<div  class="home8 pt70">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs28 lg42 bg4 color_white mb50 font-weight-normal">Here are some <b>Done-For-You Products</b> for starting your very own <b>Software Agency</b> </h2>
						<div class="row">
							<div class="col-sm-10 offset-sm-1">
								<img src="img/home8_1.png" class="img-fluid mb50">
								<div class="box_set mb50">
									<h2 class="text-center lg24 xs18 font-weight-normal">Simple & Sorted : With AppZilo DFY Version You’ll Get 100% Commissions and Your Very Own Software Agency and Business Generating Results Like Below..</h2>
								</div>
								<img src="img/home8_2.png" class="img-fluid">
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</section>
		<div class="cta-button mt50 text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs18 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs16 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs18">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs18"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		<section>
			<div class="home9 pt70 pb50">
				<div class="container">
					<div class="content">
						<img src="img/testimonials.png" class="img-fluid">
					</div>
				</div>
			</div>
		</section>

		<div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>

	<section id="take_look">
      <div class="home18 pt70">
          <div class="container">
          	<div class="header mb70">
							<h2 class="color_yellow xs28 lg42 text-center mt30">Let's Do A Quick Recap!</h2>
						</div>
            <div class="row">
                <div class="col-12 mx-auto">
                <img src="img/logo.png" class="img-fluid d-block mx-auto logo1">
                </div>
                <div class="col-md-8 mx-auto">
                    <div class="content text-center">
                        
                        <div class="list mt30 text-left">
                            <ul>
                                <li class="lg24 xs20">50 DFY Products For Sell <div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">All Comes With Reseller License <div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">You Can also use and install for your clients as well<div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">DFY Software Business in 1-Click <div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">Comes with all sales materials like<br> sales pages, email swipes etc <div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">We will handle all support <div class="bg_gradent"></div></li>
                                <li class="lg24 xs20">Per month software will add for sell </li>

                            </ul>
                        </div>
						<div class="table-btn ">
                            <div class="hideme-button">
                              <a href="https://warriorplus.com/o2/buy/lbptdw/hwtfm1/yd31c4"><img src="https://warriorplus.com/o2/btn/fn200011000/lbptdw/hwtfm1/324991" class="img-fluid d-block mx-auto"></a>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
          </div>
		  <div class="col-sm-12">
									<div class="text-center mt10">
									<a href="https://warriorplus.com/o/nothanks/hwtfm1" class="nothank">
										No Thanks
									</a>									
								</div>
			</div>
		</div>
	</div>
   	</section>

	  
		<!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->
 <section>
            <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"> <i class="fas fa-hand-paper"></i> WAIT!!!<br>
                                <span>Don't Leave Empty Handed!</span>
                            </h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2><u>Copy This Code</u> "AppZilo" </h2><h3>To Get Your <span><u>$3 Discount</u> Now!</span></h3>
                            <div class="button text-center mt30">
                                <a href="https://warriorplus.com/o2/buy/lbptdw/hwtfm1/yd31c4" class="btn_custom_set1" target="_blank">Yes! - Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
        <script type="text/javascript" src="jquery.exit-modal.js"></script>
        <script src="js/scripts.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var timer;

                var exitModalParams = {
                    numberToShown:                  5,
                    callbackOnModalShow:            function() {
                        var counter = $('.exit-modal').data('exitModal').showCounter;
                        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
                    },
                    callbackOnModalShown:           function() {
                        timer = setTimeout(function(){
                            // window.location.href = "http://www.jqueryscript.net";
                        }, 4000)
                    },
                    callbackOnModalHide:            function() {
                        clearTimeout(timer);
                    }
                }

                $('.destroy-exit-modal').on("click", function(e){
                    e.preventDefault();
                    if($('.exit-modal').data('exit-modal')) {
                        $(".initialized-state").hide();
                        $(".destroyed-state").show();
                    }
                    $('.exit-modal').exitModal('hideModal');
                    $('.exit-modal').exitModal('destroy');
                    $(".initialized").hide();
                });

                // $('.init-exit-modal').on('click', function(e){
                    // e.preventDefault();
                    $('.exit-modal').exitModal(exitModalParams);
                    if($('.exit-modal').data('exit-modal')) {
                        $(".destroyed-state").hide();
                        $(".initialized-state").show();
                    }
                // });

                $('.close-exit-modal').on('click', function(e){
                    e.preventDefault();
                    $('.exit-modal').exitModal('hideModal');
                });

            });
            $(".buy_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $("#warrior_plus").offset().top
                }, 1000);
            });
            function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
        </script>
        <?php
        $rand = rand(21000,30000);
            $today = date('Y-m-d H:i:s');
            $date = date('M d, Y H:i:s', strtotime($today)+$rand);
        ?>
        <script type="text/javascript">
            countdownTimeStart();
            function countdownTimeStart(){
                var countDownDate = new Date("<?= $date;?>").getTime();
                var x = setInterval(function() {
                    var now = new Date().getTime();

                    // Find the distance between now an the count down date
                    var distance = countDownDate - now;
                    
                    // Time calculations for days, hours, minutes and seconds
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    
                    // Output the result in an element with id="demo"
                    times = hours + "h "
                    + minutes + "m " + seconds + "s ";

                    if (hours <= 9) 
                    {
                        hours = '0'+hours;
                    }
                    if (minutes <= 9) 
                    {
                        minutes = '0'+minutes;
                    }
                    if (seconds <= 9) 
                    {
                        seconds = '0'+seconds;
                    }
                    $('.hours').html(hours);
                    $('.minutes').html(minutes);
                    $('.seconds').html(seconds);

                    // console.log(times);
                    
                    // If the count down is over, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("demo").innerHTML = "EXPIRED";
                    }
                }, 1000);
            }
        </script>
    </body>
</html>

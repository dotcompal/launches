<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>AppZilo - Enterprise</title>
        <!-- Favicon-->
        <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/custom.css" rel="stylesheet" />
        <link href="css/general.css" rel="stylesheet" />
        <style type="text/css">
            .border_set{
                display: inline-block;
                height: 8px;
                border-radius: 10px;
                width: 230px;
                margin: 0 auto;
                background-color: #02042B;
            }
        </style>
    </head>
    <body id="page-top">

        <section>
            <div class="home1 pt20 ">
                <div class="container">
                    <div class="header">
                        <div class="text-center" style="width: 100%;">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
                        </div>
                        <h2 class="text-center color_white lg24 mt30">Expand Your Reach to More than 3 Billion Potential Customers with over 90% open rate</h2>
                        <h2 class="color_white f-28 f-md-48 text-center mt30">
                            <span class="bg_orange">World’s Most Powerful Messaging </span>
                        </h2>
                        <h2 class="color_white text-center f-md-36 f-28 mt30">Cloud Based Platform that combines the</h2>
                        <h2 class="color_white f-md-32 f-28 pt20"><span class="bg_red"><span>Power of Messenger, SMS and Email with Zero Monthly FEE</span></span></h2>
                        <h2 class="color_white f-md-32 f-28 pt20">AppZilo Allows You to Make Money from Every Single Platform that your potential customers are on…. All under one hood</h2>
                    </div>
                    <div class="content mt50">
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8">
                            <img src="img/product-box.webp" class="img-fluid">
                                <!-- <div class="video">
                                    <script src="https://fast.wistia.com/embed/medias/8re078ro38.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_8re078ro38 videoFoam=true" style="height:100%;position:relative;width:100%"><div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;"><img src="https://fast.wistia.com/embed/medias/8re078ro38/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs12 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr mt30" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs10 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
                <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <section>
            <div class="home2 pt50">
                <div class="container">
                    <div class="content">
                        <div class="img_box text-center">
                            <img src="img/home2_1.png" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section style="background-color: #ffffff;">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
              <path fill="#F3F3F3" fill-opacity="1" d="M0,96L80,90.7C160,85,320,75,480,106.7C640,139,800,213,960,224C1120,235,1280,181,1360,154.7L1440,128L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path>
            </svg>
            <div class="home3" style="background-color: #f3f3f3;">
                <div class="container">
                    <div class="header">
                        <div class="row align-items-center">
                            <div class="col-sm-5">
                                <div class="text-center mt10">
                                    <img src="img/home3_1.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-sm-7 mt0 mt-md40">
                                <h2 class="color_blue lg36"> Messenger  is the latest Marketing Platform with</h2> 
                                <p class=" f-22 pt10">1.3 Billion People Use Messenger Actively on Daily Basis</p>
                                <p class=" f-22 pt10">Messenger marketing leads to 70% better open ratethan email marketing</p>
                                <p class=" f-22 pt10">75% of customers say they'd rather message withbusinesses than email or call.</p>
                                <p class="f-22 pt10">With 80% open rates and 25% CTR,Messenger beats every other channel</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="home4 pt20 ">
                <div class="container">
                    <div class="header">
                        <div class="row align-items-center">
                            <div class="col-sm-7">
                                <h2 class="color_blue f-36" style="color: #0076FF;">SMS Are Highest Priority Notifications with</h2> 
                                <p class="color_white f-22 pt10">Whopping 98% Open Rate</p>
                                <p class="color_white f-22 pt10">White not everyone has smartphone, 5 Billion Peoplein world can receive and send SMS</p>
                                <p class="color_white f-22 pt10">90% of SMS are read within 3 minutes</p>
                                <p class="color_white f-22 pt10">The Click Through Rate for SMS ads is 19%</p>
                            </div>
                            <div class="col-sm-5">
                                <div class="text-center mt50">
                                    <img src="img/home4_2.png" class="img-fluid">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="home5 pt20 pb50" style="background-color: #f3f3f3;">
                <div class="container">
                    <div class="header">
                        <div class="row align-items-center">
                            <div class="col-sm-5">
                                <div class="text-center mt50">
                                    <img src="img/home5_2.png" class="img-fluid">
                                </div>
                            </div>
                            <div class="col-sm-7">
                            <h2 class="color_blue f-36">Email Marketing Still<br class="d-none d-md-block "> Crushes with</h2> 
                           
                            <p class="f-22 pt10">293.6 Billions Emails are sent everyday</p>
                            <p class="f-22 pt10">For ever $1 Spend on email marketing returnsan average of $42</p>
                            <p class="f-22 pt10">59% of marketers cite emails as their numberone source of ROI</p>
                            <p class="f-22 pt10">58% of consumers check theiremail before doing anythingelse online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section style="background-color: #f3f3f3;">
            <div class="home6 pb50">
                <div class="container">
                    <div class="header">
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-5">
                                <div class="heading">
                                    <h2 class="text-center color_white lg24 mb-0">Messenger Marketing</h2>
                                </div>
                                <div class="list">
                                    <ul class="color_white text-center">
                                        <li><h3>Build Unlimited Messenger List in 1 Click</h3></li>
                                        <li><h3>Send Unlimited Messages to Unlimited Subscribers </h3></li>
                                        <li><h3>Send Media Rich Messages including files, images, Audios, Videos and more</h3></li>
                                        <li><h3>Setup Messenger bots to reply24*7 Automatically</h3></li>
                                        <li><h3>Send Promotional and Marketing Messages</h3></li>
                                        <li><h3>Build multiple ecommerce stores inside facebook messengerwith payment system</h3></li>
                                        <li><h3>Auto comment on the page’spost to increase the engagement</h3></li>
                                        <li><h3>Turn post commenters intomessenger leads</h3></li>
                                        <li><h3>Collect Emails, Phone, Birthdays ight inside messenger chat easily than ever</h3></li>
                                        <li><h3>Integrate with your favorite email autoresponders</h3></li>
                                        <li><h3>Never miss a single message reply instantly and get more traffic and sales</h3></li>
                                        <li><h3>Quick reply with user emailand call us option</h3></li>
                                        <li><h3>Create messenger follow-up sequence</h3></li>
                                        <li><h3>Customize bot setting with welcome message, get started button,chat with human , unsubscribe button,resubscribe button.</h3></li>
                                        <li><h3>Get social media engagementby scheduling the posts on top platforms</h3></li>

                                        <!-- <li><div class="line_box"></div></li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="heading">
                                    <h2 class="text-center color_white lg24 mb-0">SMS Marketing</h2>
                                </div>
                                <div class="list">
                                    <ul class="color_white text-center">
                                        <li><h3>Import Unlimited Phone Number swithin second</h3></li>
                                        <li><h3>Create unlimited Phone Lists</h3></li>
                                        <li><h3>Create SMS Follow Up Sequence</h3></li>
                                        <li><h3>Integrate with Most popular SMS services in 1 click</h3></li>
                                        <li><h3>Campaign scheduling fortrue automation</h3></li>
                                        <li><h3>SMS Notification During Phone OPT-IN Inside Messenger</h3></li>
                                        <!-- <li><div class="line_box"></div></li> -->
                                    </ul>
                                </div>
                                <div class="heading">
                                    <h2 class="text-center color_white lg24 mb-0">Email Marketing</h2>
                                </div>
                                <div class="list">
                                    <ul class="color_white text-center">
                                        <li><h3>Import unlimited Email Subscribers List</h3></li>
                                        <li><h3>Integrate with Your Favorite SMTP Provider</h3></li>
                                        <li><h3>Broadcast unlimited emails</h3></li>
                                        <li><h3>Integrate with Mailchimp, ActiveCampaign,SendinBlue, Mautic and more</h3></li>
                                        <li><h3>Details reporting of broadcastsand autoresponders</h3></li>
                                        <!-- <li><div class="line_box"></div></li> -->
                                    </ul>
                                </div>
                                <div class="heading">
                                    <h2 class="text-center color_white lg24 mb-0">More Tools</h2>
                                </div>
                                <div class="list">
                                    <ul class="color_white text-center">
                                        <li><h3>Website Comparison tool</h3></li>
                                        <li><h3>Place, website finder for local businesses</h3></li>
                                        <!-- <li><div class="line_box"></div></li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <h2 class="lg28 text-center mt50 color_white">I N T R O D U C I N G</h2>
                        <div class="text-center" style="width: 100%;">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" class="mt20"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
                        </div>
                        <div class="text-center mt-4" style="width: 100%;">
                            <img src="img/product-box.webp" class="img-fluid">
                        </div>
                        
                    </div>
                </div>
            </div>
           
        </section>
        <div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs12 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr mt20 " href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs10 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
                <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <section>
            <div class="home7 pt70">
                <div class="container">
                    <div class="header">
                        <h2 class="lg42 text-center">Create an</h2>
                        <h2 class="text-center lg36 mt30 white-clr"><span class="bg_orange"><span>Omnichannel Profit Experience</span></span></h2>
                    </div>
                    <div class="content mt50">
                        <h3 class="text-center">It doesn’t matter what niche you are in or what kind of product or service you sell. This will work for everyone.</h3>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_1.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Affiliate Marketing</h2>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_2.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Ecomm Stores</h2>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_3.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Local Businesses</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_4.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Consultant Agencies</h2>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_5.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Bloggers</h2>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item text-center mt30">
                                    <img src="img/home7_6.png" class="img-fluid" style="max-width: 100px;">
                                    <h2 class="lg24 mt20">Video Marketers</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_footer mt50">
                        <h2 class="text-center">So what are you waiting for?</h2><br><br>
                    </div>
                </div>
                
            </div>
        </section>
        <div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs12 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr mt20" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs10 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
                <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b class="lg20 xs11">FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <section>
            <div class="home8 pt70">
                <div class="container">
                    <div class="header">
                        <h2 class=" lg42 text-center">5 Fast Action Free Bonuses <u>Worth $2755</u></h2>
                    </div>
                    <div class="content mt50">
                        <div class="item">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-4">
                                    <div class="img_box">
                                        <img src="img/bonus-box.webp" class="img-fluid">
                                    </div>
                                    
                                </div>
                                <div class="col-sm-7 pt30">
                                    <h2 class="lg30 bg_yellow2"> Training For Start Email Marketing</h2>
                                    <p class="lg20  mt20">By taking action NOW, you can speed-up your learning and get best results using Email Marketing with our easy and pin-point accurate video training.</p>
                                    <p class="lg28 text-right mt20 "><b>Value - $227 </b></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row align-items-center">
                            <div class="col-sm-4 order-md-2">
                                    <div class="img_box">
                                        <img src="img/bonus-box.webp" class="img-fluid">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-7 pt30 order-md-1">
                                    <h2 class="lg30 bg_yellow2"> Email Marketing Cheat Sheet</h2>
                                    <p class="lg20  mt20">Here's How You Can Turn a ‘Dead List’ Into a Rabid Buyers List And Increase Sales By A Huge Leap. Why You Should Build Your Own Email List Now Strengthen Connections.</p>
                                    <p class="lg28 text-left mt20 "><b>Value - $667 </b></p>
                                </div>
                                
                            </div>
                        </div>
                        <div class="item">
                            <div class="row align-items-center">
                                
                                <div class="col-sm-4">
                                    <div class="img_box">
                                        <img src="img/bonus-box.webp" class="img-fluid">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-7 pt30">
                                    <h2 class="lg30 bg_yellow2"> 100+ Marketing Emails</h2>
                                    <p class="lg20  mt20">Your complete "fill in the blank" autoresponder series you can use for ALL your internet marketing lists and is geared toward TEN main areas of your business!</p>
                                    <p class="lg28 text-right mt20"><b>Value - $567 </b></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row align-items-center">
                            <div class="col-sm-4 order-md-2">
                                    <div class="img_box">
                                        <img src="img/bonus-box.webp" class="img-fluid">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-7 pt30 order-md-1">
                                    <h2 class="lg30 bg_yellow2"> 80% Discount on ALL Upgrades</h2>
                                    <p class="lg20 mt20">Get 80% instant discount on purchase of All Upgrades. This is very exclusive bonus which will be taken down soon.</p>
                                    <p class="lg28 text-left mt20"><b>Value - $297 </b></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row align-items-center">
                                <div class="col-sm-4">
                                    <div class="img_box">
                                        <img src="img/bonus-box.webp" class="img-fluid">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-7 pt30">
                                    <h2 class="lg30 bg_yellow2"> 30 Agency License</h2>
                                    <p class="lg20 mt20">You have full rights to use this software. <br><br>You can use it for anyone whether for individuals or for companies. Generate massive free traffic to yourself and for others as well.</p>
                                    <p class="lg28 text-right mt20"><b>Value - $997 </b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

        </section>

        <div class="home9 pt70 pb50" id="take_look">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"></path>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop offset="0.5251" style="stop-color:#2089EA"></stop>
						   <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
						   <stop offset="0.7855" style="stop-color:#1751C0"></stop>
						   <stop offset="1" style="stop-color:#0D1793"></stop>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop offset="0.3237" style="stop-color:#F4833F"></stop>
						   <stop offset="0.6981" style="stop-color:#F39242"></stop>
						   <stop offset="1" style="stop-color:#F2A246"></stop>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
				   </g>
				   </svg>
                        </div>
                        <div class="col-12 col-md-6 mx-auto mt20 mt-md50">
                            
                            <div class="content text-center mt20 xspt40 pt40 xspb40 pb40">
                            <a href="https://warriorplus.com/o2/buy/lbptdw/m140t6/kxx2k8"><img src="https://warriorplus.com/o2/btn/fn200011000/lbptdw/m140t6/324990" class="img-fluid d-block mx-auto"></a>
                            </div>

                        </div>
                        
                    </div>
                    <div class="footer_content text-center" id="warrior_plus">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <a href="https://warriorplus.com/o/nothanks/m140t6" class="nothank">
                                    No Thanks
                                </a>                       
                            </div>
                        </div>
                    </div>  
                </div>
                </div>
				
              
                </div>


        <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
        <section>
            <div class="home10 pt70">
                <div class="container">
                    <div class="header">
                        <h2 class="f-28 f-md-42 text-center">Frequently asked questions</h2>
                        <p class="text-center f-22 pt30">The rise of mobile devices transforms the way we consume information entirely <br> and the world's most elevant channels such as Facebook.</p>
                    </div>
                    <div class="content pt30">
                        <ul>
                            <li>
                                <h2 class="lg26">Q.1 Do I need any hosting or website for this ?</h2>
                                <p class="lg18">NO!! You don't need any hosting or website for this. AppZilo is a fully Cloud-Based Software</p>
                            </li>
                            <li>
                                <h2 class="lg26">Q.2 Is it Beginner friendly?</h2>
                                <p class="lg18">Yes, it is completely beginner-friendly and we did all the hard work for you. SO, there is not much manual work for you. And also we added a Free tutorial to get started.</p>
                            </li>
                            <li>
                                <h2 class="lg26">Q.3 Do I Need Any Tech Skills Or Experience For using this tool ?</h2>
                                <p class="lg18">No!! We have step by step tutorials for this so you don't need to do anything else</p>
                            </li>
                            <li>
                                <h2 class="lg26">Q.4 Is There A Money Back Guarantee?</h2>
                                <p class="lg18">Yes, you are covered with 30 Days Money-back Guarantee. There is absolutely no risk when you act now.</p>
                            </li>
                            <li>
                                <h2 class="lg26">Q.5 how do I get Instant Access?</h2>
                                <p class="lg18">Click the Below Button and Lock Your Discount now.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </section>


        <!-- Copyright Section-->
        <!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->
        <section>
            <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"> <i class="fas fa-hand-paper"></i> WAIT!!!<br>
                                <span>Don't Leave Empty Handed!</span>
                            </h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2><u>Copy This Code</u> "AppZilo" </h2><h3>To Get Your <span><u>$3 Discount</u> Now!</span></h3>
                            <div class="button text-center mt-4">
                                <a href="https://warriorplus.com/o2/buy/lbptdw/m140t6/kxx2k8" class="btn_custom_set1" target="_blank">Yes! - Please Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
        <script type="text/javascript" src="jquery.exit-modal.js"></script>
        <script src="js/scripts.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var timer;

                var exitModalParams = {
                    numberToShown:                  5,
                    callbackOnModalShow:            function() {
                        var counter = $('.exit-modal').data('exitModal').showCounter;
                        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
                    },
                    callbackOnModalShown:           function() {
                        timer = setTimeout(function(){
                            // window.location.href = "http://www.jqueryscript.net";
                        }, 4000)
                    },
                    callbackOnModalHide:            function() {
                        clearTimeout(timer);
                    }
                }

                $('.destroy-exit-modal').on("click", function(e){
                    e.preventDefault();
                    if($('.exit-modal').data('exit-modal')) {
                        $(".initialized-state").hide();
                        $(".destroyed-state").show();
                    }
                    $('.exit-modal').exitModal('hideModal');
                    $('.exit-modal').exitModal('destroy');
                    $(".initialized").hide();
                });

                // $('.init-exit-modal').on('click', function(e){
                    // e.preventDefault();
                    $('.exit-modal').exitModal(exitModalParams);
                    if($('.exit-modal').data('exit-modal')) {
                        $(".destroyed-state").hide();
                        $(".initialized-state").show();
                    }
                // });

                $('.close-exit-modal').on('click', function(e){
                    e.preventDefault();
                    $('.exit-modal').exitModal('hideModal');
                });

            });
            $(".buy_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $("#warrior_plus").offset().top
                }, 1000);
            });
            function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
        </script>
        <?php
        $rand = rand(21000,30000);
            $today = date('Y-m-d H:i:s');
            $date = date('M d, Y H:i:s', strtotime($today)+$rand);
        ?>
        <script type="text/javascript">
            countdownTimeStart();
            function countdownTimeStart(){
                var countDownDate = new Date("<?= $date;?>").getTime();
                var x = setInterval(function() {
                    var now = new Date().getTime();

                    // Find the distance between now an the count down date
                    var distance = countDownDate - now;
                    
                    // Time calculations for days, hours, minutes and seconds
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    
                    // Output the result in an element with id="demo"
                    times = hours + "h "
                    + minutes + "m " + seconds + "s ";

                    if (hours <= 9) 
                    {
                        hours = '0'+hours;
                    }
                    if (minutes <= 9) 
                    {
                        minutes = '0'+minutes;
                    }
                    if (seconds <= 9) 
                    {
                        seconds = '0'+seconds;
                    }
                    $('.hours').html(hours);
                    $('.minutes').html(minutes);
                    $('.seconds').html(seconds);

                    // console.log(times);
                    
                    // If the count down is over, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("demo").innerHTML = "EXPIRED";
                    }
                }, 1000);
            }
        </script>
    </body>
</html>

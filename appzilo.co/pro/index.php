<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>AppZilo - Pro</title>
		<!-- Favicon-->
		<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
		<!-- Font Awesome icons (free version)-->
		<script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
		<!-- Google fonts-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Architects+Daughter&display=swap" rel="stylesheet">
		<!-- Core theme CSS (includes Bootstrap)-->
		<link href="css/styles.css" rel="stylesheet" />
		<link href="css/pages.css" rel="stylesheet" />
		<link href="css/custom.css" rel="stylesheet" />
		<link href="css/general.css" rel="stylesheet" />
		<style type="text/css">
			.border_set{
				display: inline-block;
				height: 8px;
				border-radius: 10px;
				width: 230px;
				margin: 0 auto;
				background-color: #02042B;
			}
		</style>

	</head>
	<body id="page-top">
		<!-- Masthead-->
		<header class="masthead text-center">
			<div class="home1">
				<div class="container">
					<div class="text-center mb20">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
					</div>
					<div class="pre-heading mt30">
						<h1 class="mb0 f-16 f-md-22 text-white">Congratulations! Your order is almost complete.  But before you get started...</h1>
					</div>
					<h2 class="color_white lg44 mt30 xs28">Remove All Limitations to Go Unlimited & <span class="yellow-clr"><u> Supercharge Your AppZilo Account to Get 3X More Profits Faster & Easier </u></span> with This PRO Upgrade</h2>
						<div class="post-heading mt30 text-text-capitalize">
							<h2 class="lg24 xs18 mb0 text-white">REMOVE LIMITS - UNLOCK ALL FEATURES- REMOVE AppZilo BRANDING</h2>
						</div>
					<div class="row mt30">
						<div class="col-12 col-md-10 mx-auto">
						<div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://appzilo.dotcompal.com/video/embed/5wh2kxcosg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
							<!-- <img src="img/product.webp" class="img-fluid d-block mx-auto"> -->
						</div>

					</div>
				</div>
			</div>
		</header>
		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>


		<section>
			<div class="home2 pt70 xspt30 pb50 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="text-center lg62 xs30 ">Congratulations!</h2>
						<h2 class="text-center lg40 xs20 ">on Getting <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px" xml:space="preserve">
<style type="text/css">
	.st00{fill:#07163A;}
	.st11{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
	.st22{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
	.st33{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000168823944284911347230000002582526602741899929_);}
</style>
<g>
	<g>
		<path class="st00" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
			c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
			c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z
			 M242,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
			c3.6,3.7,7.9,5.6,13,5.6s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
		<path class="st00" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
			c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
			c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
			 M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
			c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
		<path class="st00" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
		<path class="st00" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
			c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
			l0-84.2L505.3,36.7z"/>
		<path class="st00" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
		<path class="st00" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
			c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
			c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
			C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
			c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"
			/>
	</g>
	<g>
		<path class="st11" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
			c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
			<stop  offset="0.5251" style="stop-color:#2089EA"/>
			<stop  offset="0.6138" style="stop-color:#1D7ADE"/>
			<stop  offset="0.7855" style="stop-color:#1751C0"/>
			<stop  offset="1" style="stop-color:#0D1793"/>
		</linearGradient>
		<path class="st22" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
			c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
		
			<linearGradient id="SVGID_00000091705051100733353190000005027636122510766246_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
			<stop  offset="0.3237" style="stop-color:#F4833F"/>
			<stop  offset="0.6981" style="stop-color:#F39242"/>
			<stop  offset="1" style="stop-color:#F2A246"/>
		</linearGradient>
		<path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000091705051100733353190000005027636122510766246_);" d="
			M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
			c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
			c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
	</g>
</g>
</svg>
</h2>
					</div>
					<div class="content mt50">
						<p class="f-md-22 f-20 text-center">You’ve just picked yourself up one HOT launch deal. </p><br><br>
						<p class="f-md-22 f-20 text-center">You’ve already set yourself up with permanent access at one time cost to the World’s #1 & Fastest 1-Click iOS & Android Mobile App Builder Platform... In the next few minutes, you should receive an email with your login information... </p><br><br>
						<p class="f-md-22 f-20 text-center">BUT!!! Before you get to the members area, we have a special offer for you that will help you take your profits to a whole new level…</p>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home3 pt70 xspt30 pb50 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="color_red text-center lg62 xs30">INTRODUCING</h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-8 offset-sm-2">
								<img src="img/product.webp" class="img-fluid">
							</div>
						</div>
						<h2 class="lg46 mt50 xs22 text-center">With next level <span class="color_red">UNLIMITED PRO</span> features all directly available within your AppZilo dashboard</h2>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

  	

  	<section>
			<div class="home4 pt70 xspt30">
				<div class="container">
					<div class="header">
						<h2 class="text-center f-md-60 f-28"><span class="color_white">Here's Why 97.8% Of AppZilo <br class="d-none d-md-block">Users </span>Upgraded To PRO</h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_1.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #1</h2>
									<h3 class="text-center f-24 f-md-36">Remove AppZilo Branding</h3>
									<p class="color_white f-20 f-md-24 text-center">Add your own brandings in your iOS & Android app. Earn 2X with your own Branding and Business Agency.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_2.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #2</h2>
									<h3 class="text-center f-24 f-md-36">Drag n Drop Website Builder</h3>
									<p class="color_white f-20 f-md-24 text-center">Create UNLIMITED web pages with easy to use and mobile friendly web page page builder and get more traffic with Websites as well.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_3.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #3</h2>
									<h3 class="text-center f-24 f-md-36">30+ High Converting Website Templates</h3>
									<p class="color_white f-20 f-md-24 text-center">These are 30+ handpicked high converting website templates which are proven to perform & give you best results.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_4.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #4</h2>
									<h3 class="text-center f-24 f-md-36">Customizable push notification messages</h3>
									<p class="color_white f-20 f-md-24 text-center">We will give you high converting pre-written push-notification messages which users can customize</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_5.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #5</h2>
									<h3 class="text-center f-24 f-md-36">15 high converting push notification templates </h3>
									<p class="color_white f-20 f-md-24 text-center">These are 15 handpicked high converting push notification messages which are proven to perform & give you best results.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_6.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #6</h2>
									<h3 class="text-center f-24 f-md-36">Bulk push notification scheduling</h3>
									<p class="color_white f-20 f-md-24 text-center">Ability to send bulk push notification to all subscribers at once. drive massive traffic to your sales pages, websites & offers.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_7.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #7</h2>
									<h3 class="text-center f-24 f-md-36">Welcome push notification</h3>
									<p class="color_white f-20 f-md-24 text-center">Your App Will Automatically Send a Welcome Push Message to Anyone who installs the App</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_8.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #8</h2>
									<h3 class="text-center f-24 f-md-36">Customizable Menu bar</h3>
									<p class="color_white f-20 f-md-24 text-center">We will create a Mobile Responsive Menu bar for your site which can be customized to give your users a more comfortable App Browsing Experience.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_9.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #9</h2>
									<h3 class="text-center f-24 f-md-36">QR Code for Sharing Your App</h3>
									<p class="color_white f-20 f-md-24 text-center">Auto generate QR code that you can share on social media or offline, just scan and anyone can install your brand new Mobile app.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_10.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #10</h2>
									<h3 class="text-center f-24 f-md-36">Facebook Retargeting Pixel</h3>
									<p class="color_white f-20 f-md-24 text-center">Just enter your pixel ID and all your mobile app visitors will be targeted for facebook retargeting.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_11.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #11</h2>
									<h3 class="text-center f-24 f-md-36">Convert eComm Store into an Awesome App</h3>
									<p class="color_white f-20 f-md-24 text-center">Convert any Shopify or WooCommerce Store into Progressive web app.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-1">
								<img src="img/home4_12.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-2">
								<div class="item pt50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #12</h2>
									<h3 class="text-center f-24 f-md-36">Google AMP Pages</h3>
									<p class="color_white f-20 f-md-24 text-center">Convert Your Sites and Apps to AMP and start enjoying higher google rankings and more traffic.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 text-center order-sm-2">
								<img src="img/home4_13.png" class="img-fluid">
							</div>
							<div class="col-sm-6 order-sm-1">
								<div class="item pt50 mb50">
									<h2 class="text-center f-24 f-md-36">Pro Upgrade #13</h2>
									<h3 class="text-center f-24 f-md-36">Instant Article</h3>
									<p class="color_white f-20 f-md-24 text-center">Combine the power of Apps and FB Instant Articles for more social traffic and even monetization</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home5  pb50 xspb20">
				<div class="container">
					<div class="header">
						<h2 class="f-md-48 f-32 color_white text-center"><span>Plus UNLOCK UNLIMITED ACCESS</span></h2>
						<h3 class="font-weight-normal color_white f-md-42 f-32 text-center xsmt20 mt40">To ALL AppZilo FEATURES <span style="color:#fccc00;">(USE WITHOUT LIMITS)</span></h3>
					</div>
					<div class="content mt30 ">
						<div class="text-left color_white xs20 lg24 font-weight-bold">
							<table class="table">
								<tr>
									<td><i class="far fa-check-square"></i></td>
									<td class="color_white">UNLIMITED WEB HOSTING </td>
								</tr>
								<tr>
									<td><i class="far fa-check-square"></i></td>
									<td class="color_white">UNLIMITED CLIENTS  </td>
								</tr>
								<tr>
									<td><i class="far fa-check-square"></i></td>
									<td class="color_white">UNLIMITED TRAFFIC & SALES UNLIMITED REVENUE </td>
								</tr>
								<tr>
									<td><i class="far fa-check-square"></i></td>
									<td class="color_white">20000+ Stock Media Assets With Reseller License</td>
								</tr>
								
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

		<section>
      <div class="bonus-section">
        <div class="container">
        	<div class="header text-center color_blue">
            <h2 class="f-md-28 f-18">Sounds like Deal?<br>! But we made it even more mouth-watering! </h2>
            <h2 class="f-md-30 f-18 xsmt10">Check Out these Bonuses You'll Get for <br>FREE If You GRAB THIS PROFITABLE BUSINESS OPPORTUNITY Today. </h2> 
          </div>
          <div class="content mt50">
            <div class="item">
              <div class="row align-items-center">   
			  <div class="col-md-6 col-12">
                  <h2 class="f-md-30 f-18 bg_yellow2 xs14"> CashKing Pro</h2>
                  <p class="f-md-20 f-18 xsmt0 mt20 xs10 xsmb0">Are You Unable To Articulate Your Business Concept? Is Your Website Not Generating The Kind Of Traffic You Desire? Get On Top Of The Search Engine Results Page With Expert Content! Make your website traffic increase manifold by putting in expert PLR content!</p>
                  <p class="f-md-28 f-18 mt20 xsmt10 color_white"><b>Value - $227 </b></p>
                </div>             
                <div class="col-md-6 col-12 pt30">
                  <div class="img_box">
                    <img src="img/bonus-box.webp" class="img-fluid mx-auto d-block">
                  </div>
                </div>
                
              </div>
            </div>
            <div class="item mt30 mt-md50">
              <div class="row align-items-center">    
			  <div class="col-md-6 col-12 order-md-2">
                    <h2 class="f-md-30 f-18 bg_yellow2 xs14">Smach The Cash Secrets</h2>
                    <p class="f-md-20 f-18 xsmt0 mt20 xs10 xsmb0">You will learn 30 different ways of how you can be making money online. Sell your product in a package deal with other web businesses. You can both advertise it and split the profits.</p>
                    <p class="f-md-28 f-18 xs13 mt20 xsmt10"><b>Value - $667 </b></p>
                </div>   
                <div class="col-md-6 col-12 order-md-1 pt30">
                    <div class="img_box">
                        <img src="img/bonus-box.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                            
              </div>
            </div>
            <div class="item  mt30 mt-md50">
              <div class="row align-items-center">               
                <div class="col-md-6 col-12">
                  <h2 class="f-md-30 f-18 bg_yellow2 xs14"> 100+ Marketing Emails</h2>
                  <p class="f-md-20 f-18 xsmt0 mt20 xs10 xsmb0">Your complete "fill in the blank" autoresponder series you can use for ALL your internet marketing lists and is geared toward TEN main areas of your business!</p>
                  <p class="f-md-28 f-18 xs13 mt20 xsmt10"><b>Value - $567 </b></p>
                </div>
				<div class="col-md-6 col-12 pt30">
                  <div class="img_box">
                      <img src="img/bonus-box.webp" class="img-fluid mx-auto d-block">
                  </div>
                </div>
              </div>
            </div>
            <div class="item mt30 mt-md50">
              <div class="row align-items-center">
			  <div class="col-md-6 col-12 order-md-2">
                  <h2 class="f-md-30 f-18 bg_yellow2 xs14"> 80% Discount on ALL Upgrades</h2>
                  <p class="f-md-20 f-18 xsmt0 mt20 xs10 xsmb0">Get 80% instant discount on purchase of All Upgrades. This is very exclusive bonus which will be taken down soon.</p>
                  <p class="f-md-28 f-18 xs13 mt20 xsmt10"><b>Value - $297 </b></p>
                </div>
                <div class="col-md-6 col-12 order-md-1 pt30">
                    <div class="img_box">
                        <img src="img/bonus-box.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                
                
              </div>
            </div>
            <div class="item mt30 mt-md50">
              <div class="row align-items-center">
			  <div class="col-md-6 col-12">
                  <h2 class="f-md-30 f-18 bg_yellow2 xs14">30 Agency License</h2>
                  <p class="f-md-20 f-18 xsmt0 mt20 xs10 xsmb0">You have full rights to use this software. <br><br>You can use it for anyone whether for individuals or for companies. Generate massive free traffic to yourself and for others as well.</p>
                  <p class="f-md-28 f-18 xs13 mt20 xsmt10"><b>Value - $997 </b></p>
                </div>
                <div class="col-md-6 col-12 pt30">
                  <div class="img_box">
                      <img src="img/bonus-box.webp" class="img-fluid mx-auto d-block">
                  </div>
                </div>
               
              </div>
            </div>
          </div>
          <div class="header text-center">
            <h2 class="mt30 xsmt10 f-18 f-md-24">Total value of these bonuses are $5857 <br class="d-none d-md-block">But Today you will take all these bonuses home for FREE! </h2>
          </div>
        </div>

  	</section>

  	<div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-44 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>


	  <div class="table-section" id="take_look">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="color_blue text-center lg36 xs22">
				  Here's Everything You're Getting
                  </div>
                  <div class="xs28 lg46 w600 lh140 text-center mt10 ">
				  With AppZilo Today!
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 w500 text-center lh140 black-clr mt15">
                                 <li>REMOVE ALL LIMITATIONS</li>
                                 <li>REMOVE APPZILO BRANDING 	</li>
                                 <li>COMES WITH CLOUD-BASED WEBSITE	BUILDER</li>
                                 <li>30+ WEBSITE TEMPLATES</li>
                                 <li>CUSTOM PUSH NOTIFICATIONS MESSAGES </li>
                                 <li>15+ HIGH CONVERTING PUSH NOTIFICATION TEMPLATES</li>
                                 <li>BULK PUSH NOTIFICATION SCHEDULING </li>
                                 <li>WELCOME PUSH NOTIFICATION</li>
                                 <li>CUSTOMIZABLE PUSH NOTIFICATION </li>
                                 <li>QR CODE FEATURE</li>
                                 <li>FACEBOOK RETARGETING</li>
								 <li>GOOGLE AMP PAGES</li>
								 <li>INSTANT ARTICLES</li>
								 <li>CONVERT ECOM STORES INTO AN APP</li>
								 <li>5 FAST ACTION BONUSES</li>
          
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
							  <a href="https://warriorplus.com/o2/buy/lbptdw/pdn0h3/ctwk2q"><img src="https://warriorplus.com/o2/btn/fn200011000/lbptdw/pdn0h3/324986" class="img-fluid d-block mx-auto"></a>
                              </div>
                           </div>
						  
                        </div>
                     </div>
					 <div class="col-12 text-center"><a href="https://warriorplus.com/o/nothanks/pdn0h3" class="nothank">No Thanks</a></div>
                  </div>
               </div>

            </div>
         </div>
      </div>

  


		<!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->


			<section>
      <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title"><i class="fas fa-hand-paper"></i> WAIT!!!<br>
                          <span>Don't Leave Empty Handed!</span>
                      </h5>

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <h2><u>Copy This Code</u> "AppZilo" </h2><h3>To Get Your <span><u>$3 Discount</u> Now!</span></h3>
                      <div class="button text-center mt-4">
                          <a href="https://warriorplus.com/o2/buy/lbptdw/pdn0h3/ctwk2q" class="btn_custom_set1" target="_blank">Yes! - Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>


		<!-- Bootstrap core JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
		<!-- Third party plugin JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		<!-- Contact form JS-->
		<script src="assets/mail/jqBootstrapValidation.js"></script>
		<script src="assets/mail/contact_me.js"></script>
		<!-- Core theme JS-->
		<!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
		<script type="text/javascript" src="jquery.exit-modal.js"></script>
		<script src="js/scripts.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){

				var timer;

				var exitModalParams = {
				    numberToShown:                  5,
				    callbackOnModalShow:            function() {
				        var counter = $('.exit-modal').data('exitModal').showCounter;
				        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
				    },
				    callbackOnModalShown:           function() {
				        timer = setTimeout(function(){
				            // window.location.href = "http://www.jqueryscript.net";
				        }, 4000)
				    },
				    callbackOnModalHide:            function() {
				        clearTimeout(timer);
				    }
				}

				$('.destroy-exit-modal').on("click", function(e){
					e.preventDefault();
					if($('.exit-modal').data('exit-modal')) {
						$(".initialized-state").hide();
						$(".destroyed-state").show();
					}
					$('.exit-modal').exitModal('hideModal');
					$('.exit-modal').exitModal('destroy');
					$(".initialized").hide();
				});

				// $('.init-exit-modal').on('click', function(e){
					// e.preventDefault();
					$('.exit-modal').exitModal(exitModalParams);
					if($('.exit-modal').data('exit-modal')) {
						$(".destroyed-state").hide();
						$(".initialized-state").show();
					}
				// });

				$('.close-exit-modal').on('click', function(e){
					e.preventDefault();
					$('.exit-modal').exitModal('hideModal');
				});

			});
			$(".buy_btn").click(function() {
				$('html, body').animate({
					scrollTop: $("#take_look").offset().top
				}, 1000);
			});
			function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading')
				.find(".more-less")
				.toggleClass('glyphicon-plus glyphicon-minus');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);
		</script>
		<?php
		$rand = rand(21000,30000);
			$today = date('Y-m-d H:i:s');
			$date = date('M d, Y H:i:s', strtotime($today)+$rand);
		?>
		<script type="text/javascript">
			countdownTimeStart();
			function countdownTimeStart(){
				var countDownDate = new Date("<?= $date;?>").getTime();
				var x = setInterval(function() {
					var now = new Date().getTime();

					// Find the distance between now an the count down date
					var distance = countDownDate - now;
					
					// Time calculations for days, hours, minutes and seconds
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					
					// Output the result in an element with id="demo"
					times = hours + "h "
					+ minutes + "m " + seconds + "s ";

					if (hours <= 9) 
					{
						hours = '0'+hours;
					}
					if (minutes <= 9) 
					{
						minutes = '0'+minutes;
					}
					if (seconds <= 9) 
					{
						seconds = '0'+seconds;
					}
					$('.hours').html(hours);
					$('.minutes').html(minutes);
					$('.seconds').html(seconds);

					// console.log(times);
					
					// If the count down is over, write some text 
					if (distance < 0) {
						clearInterval(x);
						document.getElementById("demo").innerHTML = "EXPIRED";
					}
				}, 1000);
			}
		</script>

		
	</body>
</html>
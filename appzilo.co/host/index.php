<?php  $version = '1';?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<title>AppZilo - Host</title>
		<!-- Favicon-->
		<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
		<!-- Font Awesome icons (free version)-->
		<script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
		<!-- Google fonts-->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Architects+Daughter&display=swap" rel="stylesheet">
		<!-- Core theme CSS (includes Bootstrap)-->
		<link href="css/styles.css" rel="stylesheet" />
		<link href="css/custom.css?v=<?php echo $version?>" rel="stylesheet" />
		<link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />
		<link href="https://unpkg.com/@videojs/themes@1/dist/fantasy/index.css" rel="stylesheet">
		<script src="https://cdn.letimpact.com/js/main/f376ff0edc47bdc4a6ff9a31835d2a8a.js"></script>
		<style type="text/css">
			.border_set{
				display: inline-block;
				height: 8px;
				border-radius: 10px;
				width: 230px;
				margin: 0 auto;
				background-color: #02042B;
			}
		</style>

	</head>
	<body id="page-top">
		<!-- Masthead-->
		<header class="masthead text-white text-center" >
			<div class="home1 ">
				<div class="container">
					<div class="text-center mb20 mb-md40">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
					</div>

					<h1 class=" color_white lg30 mb30 xs16">Top-Marketers are quickly moving away from YouTube, Vimeo & Wistia to this…</h1>
					<h2 class=" lg44 mb0 xs24 font-weight-normal"> Skyrocket Your Profits With </h2>
					<h2 class=" xs30 lg50 mb0 xs24"> <span class="color_yellow">Futuristic Video Marketing Technology </span>That HOSTS, PLAY & MARKETS Your Videos</h2>
					<h2 class="lg26 mb30 xs16 color_blue bg13 mt40">Beautifully Play Unlimited Sales Videos,  Affiliate Review, Demo Or Customer Training Videos On Any Website, Pages & Devices Without Any Delay Or loss of quality!</h2>

					<div class="row xsmt20 mt80">
						<div class="col-sm-6">
							
							<div class="video_box">
								<img src="img/common-box.webp" class="img-fluid d-block mx-auto">
								<!--<iframe width="746" height="384" src="https://www.youtube.com/embed/mboIx4L0Eeg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
								<!-- <video
                                id="my-video"
                                class="video-js vjs-theme-fantasy vjs-16-9 vjs-controls-enabled"
                                controls
                                autoplay
                                preload="auto"
                                width="640"
                                height="564"
                                data-setup='{}'
                              >
                                <source src="https://f002.backblazeb2.com/file/klipse/Klipse-fe-video.mp4" type="video/mp4" />
                                <p class="vjs-no-js">
                                  To view this video please enable JavaScript, and consider upgrading to a
                                  web browser that
                                  <a href="https://videojs.com/html5-video-support/" target="_blank"
                                    >supports HTML5 video</a
                                  >
                                </p>
                              </video> -->
							</div>
						</div>
						<div class="col-sm-6 xsmt20 mt0">
							<div class="">
								<div class="text-left color_white lg20 xs18">
									<table class="table">
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">Upload, Store & Publish all your videos in just 1-click!</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">Loom-like Video & Screen Recording</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">20000+ Premium HQ Stock Assets</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">Tap Into Huge 82% Of Overall Traffic On Internet</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">In-Built Thumbnail and Image Designing Feature</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">Totally Control 100% Of Your Video Traffic & Channels</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">100% Beginner Friendly, ZERO Coding, Design or technical skills!</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">Pay Once & Use LIFETIME</td>
										</tr>
										<tr>
											<td><i class="far fa-check-circle"></i></td>
											<td class="color_white">$200 Refund if this doesn’t work for you</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</header>
		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>


		<section >
			<div class="home2 xspt40 pt70">
				<div class="container">
					<div class="content ">
						<div class="">
							<h3 class=" text-center xs30 lg50 xs30" style="color: #23085c;"><span class="color_white">You are 3-Simple steps away to</span> start your own Video Hosting & Marketing Agency</h3>
						</div>
					</div>

					<div class="content xspt0 pt70">
						<div class="row ">
							<div class="col-sm-5 col-12">
								<div class="img_box ">
									<img src="img/home2_1.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3 d-none d-md-block">
								<div class="text-center " style="color: #0d3f72;">
									<h3 class="xs22 lg28 xs18 mb0 pt50">STEP</h3>
									<h4 class="lg60 xs24">01</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
                <div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 col-12">
								<div class="box_text text-center xsmt0 xsmb30">
									<h2 class="color_blue">Upload</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Drag and drop videos from your computer to AppZilo.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-12 order-sm-3">
								<div class="img_box ">
									<img src="img/home2_2.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 order-sm-2 col-3 d-none d-md-block">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="xs22 lg28 mb0 xs18 pt20 xspt70">STEP</h3>
									<h4 class="lg60 xs24">02</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 order-sm-1 col-12 pt70 xspt10">
								<div class="box_text text-center xsmt0 xsmb30">
									<h2 class="color_blue">Customize</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Customize Your videos in just a few clicks & use them for your brand or to monetize it</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-12">
								<div class="img_box ">
									<img src="img/home2_3.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3 d-none d-md-block">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="xs22 lg28 mb0 pt20 xs18 xspt70">STEP</h3>
									<h4 class="lg60 xs24">03</h4>
								</div>

							</div>
							<div class="col-sm-5 col-12 pt70 xspt10">
								<div class="box_text text-center">
									<h2 class="color_blue">Publish & Monetize!</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Publish your HD videos within seconds in 1-Click. And Enjoy the power of lightning-fast videos to Skyrocket your conversions & profits</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>	
			<div class="home3 pt70 pb50">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 xs30 lg50 color_white font-weight-normal">Already over Hundreds of customers have shifted to <b class="color_yellow">AppZilo due to its never-ending perks.</b>  </h2>
						<h2 class="text-center xs22 lg32 color_white font-weight-normal">The Only Software You Need To Host, Play & Market Your Videos To Boost Your Conversions, Sales & Customer Satisfaction.</h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-6">
								<div class="points">
									<div class="text-left color_white lg20 xs18">
										<table class="table">
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Next Generation Video Hosting Technology</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Loom-like Video & Screen Recording</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">100% Mobile Responsive Video Pages & Player</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Complete Video Management</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Advanced An alytics</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Highly-Sophisticated Transcoding/ Encoding Technology</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Over 30+ attracting  features to captivate your audience.</td>
											</tr>
											
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="points">
									<div class="text-left color_white lg20 xs18">
										<table class="table">
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Free Website Hosting included - up to 250 Gb bandwidth/month</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Upload unlimited sales, Demo, training, client prospecting videos</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">20000+ Premium HQ Stock Assets</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Create Unlimited Playlists & Channels</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Ultimate Full 4k & HDR Support</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white">Play videos on any website, landing page, online shop or membership site in just 3 easy steps</td>
											</tr>
											<tr>
												<td><i class="far fa-check-circle"></i></td>
												<td class="color_white"> 1-Click Embed & Share Feature</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-10 offset-sm-1">
								<div class="box text-center xs22 lg32 font-weight-bold">
									PLUS, FREE COMMERCIAL LICENCE  IF YOU ACT TODAY
								</div>
							</div>
						</div>
						
					</div>
				</div>				
			</div>
			
		</section>
		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!-- 
		<section>
			<div class="home4 pt70 pb50">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 xs30 lg50 font-weight-normal color_white">SEE <b style="color: #fccc00;">Big Marketers Are Now <br> Using AppZilo</b> to Host…</h2>

						<div class="text-center mt50">
							<img src="img/klipse-testimonials.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<section class="pb50">
			<div class="home5 xspt40 pt100 ">
				<div class="container">
					<div class="header">
						<h2 class="color_white xs30 xs30 lg50 text-center">Even We Used AppZilo To <b style="color: #23085c;">Generate $17,975 In Just 7 Days</b></h2>
						<div class="text-center mt50">
							<img src="img/home5_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

		<!-- <section>
			<div class="home7 pt70 pb50">
				<div class="container">
					<div class="header">
						<h2 style="color: #23085c;" class="text-center xs30 lg50 ">And here’re what some more <br><span style="color: #ff3b72">happy users say about AppZilo</span></h2>
					</div>
					<div class="content mt100">
						<div class="row">
							<div class="col-sm-6">
								<div class="box text-center mb100">
									<img src="img/u1.png" class="img-fluid rounded-circle">
									<p class="mt30 lg22">AppZilo is very easy to use and it has very easy to understand dashboard. I'm using AppZilo for hosting my training videos and tutorials and it's very fast ! I'm enjoying working with it. Good Job </p>
									<h2>Kyla James</h2>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="box text-center mb100">
									<img src="img/u2.png" class="img-fluid rounded-circle">
									<p class="mt30 lg22">AppZilo is a great option for anyone looking to take complete control of their video marketing campaigns.</p>
									<h2>Lisa M.</h2>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="box text-center mb100">
									<img src="img/u3.png" class="img-fluid rounded-circle">
									<p class="mt30 lg22">The best part is it comes with Unlimited hosting & marketing for 1-time price only. Good Job Guys !</p>
									<h2>Adam F. </h2>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="box text-center mb100">
									<img src="img/u4.png" class="img-fluid rounded-circle">
									<p class="mt30 lg22">I got beta access of AppZilo and I am truly amazed with it's lightening FAST video hosting. I can host and market unlimited videos ! <br><br>5 star for this one!!</p>
									<h2>Simon Kim</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		<section >
			<div class="home8 pt70 xspb40 pb100">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 offset-sm-1">
							<div class="header">
								<h2 class="xs22 lg36">Hey There!</h2>
								<div class="row">
									<div class="col-sm-3">
										<div class="text-center">
											<img src="img/ayush-jain.png" class="img-fluid rounded-circle" style="max-width: 150px;">
											<h2 class=" lg18 mt20">Ayush Jain</h2>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="text-center">
											<img src="img/pranshu-gupta.png" class="img-fluid rounded-circle" style="max-width: 150px;">
											<h2 class="lg18 mt20">Pranshu Gupta</h2>
										</div>
									</div>
								</div>
								<h2 class="lg24 xs20 font-weight-normal mt50">Ayush Jain here with my partner Pranshu Gupta and we're directly going to tell you that if you are not using videos in your Business, you are missing out on tremendous profits. <br><br>Videos are the MOST POWERFUL form of Content. Also Video will dominate the internet. <br><br>Video will be the primary medium for how internet users will consume information. </h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home9 pt70 pb50">
				<div class="container">
					<div class="header">
						<h2 class="xs22 lg32 color_white text-center">Here are the reasons why Videos are very <br>Important for your Business</h2>
					</div>
					<div class="content mt50">
						<div class="row">
							<div class="col-sm-10 offset-sm-1">
								<div class="box">
									<div class="row mt30">
										<div class="col-sm-1">
											<h3 class="text-right" style="color: #ff3b72;">01</h3>
										</div>
										<div class="col-sm-10">
											<h2 class="lg26" style="color: #23085c;">Grow Revenue</h2>
											<p>Marketers who use video are growing company revenue 49% faster year-over-year</p>
										</div>
									</div>
									<div class="row mt30">
										<div class="col-sm-1">
											<h3 class="text-right" style="color: #ff3b72;">02</h3>
										</div>
										<div class="col-sm-10">
											<h2 class="lg26" style="color: #23085c;">Influence Buying Decisions</h2>
											<p>A whopping 90% of customers say that product video helps them make buying decisions, says Forbes, and 64% say that watching a video makes them more likely to buy</p>
										</div>
									</div>
									<div class="row mt30">
										<div class="col-sm-1">
											<h3 class="text-right" style="color: #ff3b72;">03</h3>
										</div>
										<div class="col-sm-10">
											<h2 class="lg26" style="color: #23085c;">Give the People What They Want</h2>
											<p>From brands, people prefer video content over emails, newsletters, social images, social videos, blog posts, and downloadable content (like PDFs)</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box_2">
							<h2 class="xs30 xs30 lg50 text-center color_white font-weight-normal mt50">We personally have earned and helped brands earn <span style="color: #fccc00;">6 figure profits using</span> <u style="color: #ff3b72;">ONLY VIDEOS.</u></h2>
							<h2 class="xs30 xs30 lg50 text-center color_white mt50">And, it’s <span style="color: #fccc00;">ONLY going to get BETTER.</span></h2>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section >
			<div class="home21 xspt40 pt100 pb50">
				<div class="container">
					<div class="header pt30">
						<h2 class="xs30 xs30 lg50 text-center font-weight-normal text-white">Even The Huge <i style="color: #fccc00">$398 Billion</i> E-Learning<br>Industry is Served Using Videos.</h2>

						<div class="text-center mt50">
							<img src="img/home21_1.png" class="img-fluid">
						</div>
						<div class="text-center mt50">
							<img src="img/home21_2.png" class="img-fluid">
						</div>
					</div>
					<div class="content mt50">
						<h2 class="color_white xs24 xs22 lg36 text-center">Top E-Learning Sites like Coursera, TreeHouse, Udemy, CodeAcademy  Udacity &, SkillShare are Using Videos to Sell & <i style="color: #fccc00;"> Deliver Courses and Bank Millions</i></h2>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home22 pt70 pb50">
				<div class="container">
					<div class="header">
						<div class="row">
							<div class="offset-sm-1 col-sm-10">
								<h2 style="color: #ff3b72;" class="text-center xs30 xs30 lg50">And it’s not done yet here because</h2>
								<p class="xs24 xs22 lg36 text-center color_white">People are not enjoying all the videos personally even they love to share with others on Facebook, Twitter, Instagram, TikTok, LinkedIn, WhatsApp, Snapchat, and other networking sites and modes</p>
								<div class="box text-center">
									<img src="img/social.png" class="img-fluid">
								</div>
								<p  class="xs24 xs22 lg36 text-center color_white mt50">Your shared videos could go viral producing Non-Stop Traffic, Leads, <i style="color: #fccc00;">Sales & Profits 24/7</i>, 365 Days a Year <br>100% Hands Free!</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home23 xspt40 pt100 pb50">
				<div class="container">
					<div class="header">
						<h2 class="xs30 xs30 lg50 text-center color_white"><span style="color: #23085c;">But Using A Video Can Quickly Turn </span><br>Into A Headache For MOST Marketers…</h2>
						<h3 class="text-center xs30 xs30 lg50"><span>REASON</span></h3>
						<h4 class="font-italic text-center xs22 lg32" style="color: #23085c;">Uploading a video to the same hosting where your	website is hosted. And chances are pretty high that you face one or more of these challenges on a daily basis…</h4>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="fas fa-eye-slash"></i>
									</div>
									<p class="lg26 color_blue">Limited Views and Earnings. You are paying over $600 for platforms like Vimeo, Wistia etc. every year. That’s <b><i>SUPER EXPENSIVE</i></b></p>
								</div>
							</div>	
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="fab fa-youtube"></i>
									</div>
									<p class="lg26 color_blue">YouTube’s never-ending ‘Skip Ad’ feature and it’s frustrating your traffic</p>
								</div>
							</div>	
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="far fa-copy"></i>
									</div>
									<p class="lg26 color_blue">Some Marketers are simply copying your content</p>
								</div>
							</div>	
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="fas fa-traffic-light"></i>
									</div>
									<p class="lg26 color_blue">You’re getting Non-related or fake traffic to your videos… People are  looking for just something to entertain them</p>
								</div>
							</div>	
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="fas fa-tv"></i>
									</div>
									<p class="lg26 color_blue">Your Customers want to see your videos but they only see a <b><i>BLACK SCREEN</i></b></p>
								</div>
							</div>	
							<div class="col-sm-4">
								<div class="box text-center">
									<div class="icon lg26">
										<i class="fas fa-spinner"></i>
									</div>
									<p class="lg26 color_blue">Your Customers are facing Buffering issues in every seconds</p>
								</div>
							</div>
							<div class="col-sm-8 offset-sm-2">
								<div class="box text-center">
									<h2 style="color: #ff3b72;" class="xs30 lg50">The List Goes On And On…</h2>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home24 xspt40 pt100 pb50">
				<div class="container">
					<div class="header">
						<h2 class="xs30 xs30 lg50 text-center" style=" color: #fccc00;">Imagine If You’re Being Able To…</h2>
						<p class="xs22 lg32 mt30 text-center text-white" ><i>Host, Manage & Publish THOUSANDS of HD videos without any uploading issues or buffering issues, without worrying about reaching any uploading limits , Views Limits and being forced to pay every month for your Business</i></p>

						<h3 class="text-center xs30 xs30 lg50 color_white mt40"><span>Proudly Presenting</span></h3>
						<div class="text-center mt100">
							<div class="row">
								<div class="offset-sm-2 col-sm-8" style="padding-left: 80px;">
									<img src="img/common-box.webp" class="img-fluid d-block mx-auto">
								</div>
							</div>
						</div>
					</div> 
					<div class="content">
						<h2 style="color: #fccc00" class="xs30 lg50 text-center mt50 mb-4">AppZilo is a Game Changer</h2>
						<div class="row">
							<div class="col-sm-6">
								<div class="box xs22 lg36">
									<i class="far fa-times-circle"></i> No buffering<br>
									<i class="far fa-times-circle"></i>	No Delay<br>
									<i class="far fa-times-circle"></i>	One Time Fee<br>
									<i class="far fa-times-circle"></i>	Fully CloudBased System<br>
									<i class="far fa-times-circle"></i>	Beginner Friendly <br>
									<i class="far fa-times-circle"></i>	No Ads

								</div>
							</div>
							<div class="col-sm-6">
								<p class="xs24 xs22 lg36 color_white pt20">PERFECT All-In-One Video Hosting & Marketing Platform With Built-In LIVE Transcoding Technology... That Will Easily Drives Unlimited Traffic, Leads & Sales With No Skills Required</p>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</section>

		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

	  <section >
			<div class="home2 xspt40 pt70">
				<div class="container">
					<div class="content ">
						<div class="">
							<h3 class=" text-center xs30 lg50 xs30" style="color: #23085c;"><span class="color_white">You are 3-Simple steps away to</span> start your own Video Hosting & Marketing Agency</h3>
						</div>
					</div>

					<div class="content xspt0 pt70">
						<div class="row ">
							<div class="col-sm-5 col-12">
								<div class="img_box ">
									<img src="img/home2_1.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3 d-none d-md-block">
								<div class="text-center " style="color: #0d3f72;">
									<h3 class="xs22 lg28 xs18 mb0 pt50">STEP</h3>
									<h4 class="lg60 xs24">01</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
                <div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 col-12">
								<div class="box_text text-center xsmt0 xsmb30">
									<h2 class="color_blue">Upload</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Drag and drop videos from your computer to AppZilo.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-12 order-sm-3">
								<div class="img_box ">
									<img src="img/home2_2.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 order-sm-2 col-3 d-none d-md-block">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="xs22 lg28 mb0 xs18 pt20 xspt70">STEP</h3>
									<h4 class="lg60 xs24">02</h4>
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-5 order-sm-1 col-12 pt70 xspt10">
								<div class="box_text text-center xsmt0 xsmb30">
									<h2 class="color_blue">Customize</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Customize Your videos in just a few clicks & use them for your brand or to monetize it</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-12">
								<div class="img_box ">
									<img src="img/home2_3.svg" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-2 col-3 d-none d-md-block">
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="img_border text-center">
									<img src="img/img_border.png" class="img-fluid">
								</div>
								<div class="text-center" style="color: #0d3f72;">
									<h3 class="xs22 lg28 mb0 pt20 xs18 xspt70">STEP</h3>
									<h4 class="lg60 xs24">03</h4>
								</div>

							</div>
							<div class="col-sm-5 col-12 pt70 xspt10">
								<div class="box_text text-center">
									<h2 class="color_blue">Publish & Monetize!</h2>
									<p class="color_blue lg24 xs18 mt30 xsmt10">Publish your HD videos within seconds in 1-Click. And Enjoy the power of lightning-fast videos to Skyrocket your conversions & profits</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- <section>	
			<div class="home3 pt70">
				<div class="container">
					<div class="header">
						<div class="text-center">
							<img src="img/klipse-testimonials.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
			
		</section> -->

		<section>
			<div class="home25">
				<div class="container">
					<div class="header">
						<h2 class="color_blue xs30 lg50 text-center font-weight-normal">Use <b>AppZilo</b> To Host & Publish</h2>
					</div>
					<div class="content xspt0 pt50">
						<div class="row">
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_1.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_2.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_3.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_4.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_5.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_6.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_7.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_8.png" class="img-fluid">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home25_9.png" class="img-fluid">
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home26 pt40 pb50">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 lg50 color_white ">This <b style="color: #fccc00;">Demo </b>Is Sure To Blow Your Mind!</h2></br>
						<div class="row">
							<div class="col-sm-8 offset-sm-2">
								<div class="text-center">
									<!--<iframe width="560" height="315" src="https://www.youtube.com/embed/rWJqvoEqOa4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
									<!-- <video
                                        id="my-video"
                                        class="video-js vjs-theme-fantasy vjs-16-9"
                                        controls
                                        preload="auto"
                                        width="640"
                                        height="564"
                                        data-setup="{}"
                                      >
                                        <source src="https://f002.backblazeb2.com/file/klipse/New+videoe.mp4" type="video/mp4" />
                                        <p class="vjs-no-js">
                                          To view this video please enable JavaScript, and consider upgrading to a
                                          web browser that
                                          <a href="https://videojs.com/html5-video-support/" target="_blank"
                                            >supports HTML5 video</a
                                          >
                                        </p>
                                    </video> -->

									<img src="img/common-box.webp" class="img-fluid d-block mx-auto" alt="">
									
								</div>
								<div class="mt40">
								<p class="xs22 lg32 color_white font-italic text-center "><b style="color: #fccc00;">AppZilo</b> Has Got Everything It Takes To Make You The #1 Video Marketing & Hosting Agency</p>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>			
		</section>

		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

		<section>
			<div class="home27 pt100 pb100">
				<div class="container">
					<div class="header">
						<h2 class="xs24 xs22 lg36 text-center">That’s Not All</h2>
						<h3 class="color_blue xs24 xs22 lg36
						text-center mt30">with AppZilo You Can Also Tap <br>Into so many features</h3>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_1.png" class="img-fluid">
									<h2>Host Unlimited Videos</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_2.png" class="img-fluid">
									<h2>Ad-Free Videos To Skyrocket Engagement</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_3.png" class="img-fluid">
									<h2>SEO Friendly</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_4.png" class="img-fluid">
									<h2>Create Playlist/ Channels</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_5.png" class="img-fluid">
									<h2>Unlimited Audience </h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_6.png" class="img-fluid">
									<h2>Unlimited Views</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_7.png" class="img-fluid">
									<h2>Unlimited Lead Finds With Lead Finder Tool</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_8.png" class="img-fluid">
									<h2>30GB Storage</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_9.png" class="img-fluid">
									<h2>250GB Free Web Hosting</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_10.png" class="img-fluid">
									<h2>Ultra fast Bandwidth</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_11.png" class="img-fluid">
									<h2>Ultra Light & Attractive Player that works on any Device</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_12.png" class="img-fluid">
									<h2>Embed anywhere with embed code Feature</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_13.png" class="img-fluid">
									<h2>DFY HQ Video Templates </h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_14.png" class="img-fluid">
									<h2>Drag n Drop Image Editor</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_15.png" class="img-fluid">
									<h2>Support all types of videos format</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_16.png" class="img-fluid">
									<h2>Fully SEO optimized videos & Video Pages</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_17.png" class="img-fluid">
									<h2>Go Viral With Social Sharing Feature</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_18.png" class="img-fluid">
									<h2>Step by Step Video Training </h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_19.png" class="img-fluid">
									<h2>24*7 Ultra Fast Dedicated Support </h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_20.png" class="img-fluid">
									<h2>Newbie friendly & Cloud based software </h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box mb30">
									<img src="img/home27_21.png" class="img-fluid">
									<h2>Commercial License </h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home28 pt100 pb100">
				<div class="container">
					<div class="header pb50">
						<h2 class="text-center color_white xs30 lg50">Here’s A Huge List Of <span style="color: #fccc00;">All Business<br> That Need</span> This Amazing Software Today</h2>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_1.png" class="img-fluid">
									<h2 class="color_blue lg26">Business coaches</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_2.png" class="img-fluid">
									<h2 class="color_blue lg26">Affiliate marketers</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_3.png" class="img-fluid">
									<h2 class="color_blue lg26">Ecom Sellers</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_4.png" class="img-fluid">
									<h2 class="color_blue lg26">Digital Marketers</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_5.png" class="img-fluid">
									<h2 class="color_blue lg26">Software Seller</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_6.png" class="img-fluid">
									<h2 class="color_blue lg26">Game Seller</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_7.png" class="img-fluid">
									<h2 class="color_blue lg26">Video Editors</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_8.png" class="img-fluid">
									<h2 class="color_blue lg26"> Freelancers</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_9.png" class="img-fluid">
									<h2 class="color_blue lg26">Gym</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_10.png" class="img-fluid">
									<h2 class="color_blue lg26">Music Classes</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_11.png" class="img-fluid">
									<h2 class="color_blue lg26">Bars</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_12.png" class="img-fluid">
									<h2 class="color_blue lg26">Schools</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_13.png" class="img-fluid">
									<h2 class="color_blue lg26">Doctors</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_14.png" class="img-fluid">
									<h2 class="color_blue lg26">Cars</h2>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box text-center mb30">
									<img src="img/home28_15.png" class="img-fluid">
									<h2 class="color_blue lg26">Real Estate</h2>
								</div>
							</div>
							<div class="col-sm-12">
								<h2 style="color: rgb(252, 204, 0);" class="xs30 lg50 mt30 font-italic text-center">Many More….</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home29 xspt40 pt100 xspb40 pb100">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 lg50 color_blue">AppZilo Integrates With Some</h2>
						<h2 class="text-center xs30 lg50 color_white">Famous Business Platforms & Autorespond</h2>
						<div class="mt50 pb50 text-center">
							<img src="img/home29_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home30 xspt40 pt100 pb50">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 lg50 color_white">AppZilo Integrates With Some</h2>
						<h2 class="text-center xs30 lg50 " style="color: #fccc00;">Famous Business Platforms & Autorespond</h2>
						<div class="mt50 text-center">
							<img src="img/home30_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="home31 xspt40 pt100 pb50">
				<div class="container">
					<div class="header">
						<h2 class="text-center xs30 lg50 color_blue">We Literally Have <span style="color: #ee1539;">No Competition</span></h2>
						<div class="mt50 text-center">
							<img src="img/home31_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
	
		</section>

		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		

		<section>
      <div class="home11 bonus-section" style="background-color:#000000;">
        <div class="container">
        	<div class="header text-center color_white">
            <h2 class="xs22 lg28">Sounds like Deal?<br>! But we made it even more mouth-watering! </h2>
            <h2 class="mt30 xs22 lg28">Check Out these Bonuses You'll Get for <br>FREE If You GRAB THIS PROFITABLE BUSINESS OPPORTUNITY Today. </h2> 
          </div>
          <div class="content mt50">
            <div class="item">
              <div class="row align-items-center">
			  <div class="col-12 col-md-6">
                  <h2 class="lg30 bg_yellow2 color_white"> CashKing Pro</h2>
                  <p class="lg20 color_white mt20">Are You Unable To Articulate Your Business Concept? Is Your Website Not Generating The Kind Of Traffic You Desire? Get On Top Of The Search Engine Results Page With Expert Content! Make your website traffic increase manifold by putting in expert PLR content!</p>
                  <p class="xs22 lg28 mt20 color_yellow"><b>Value - $227 </b></p>
                </div>
                <div class="col-12 col-md-6 xspt30 pt0">
                  <div class="img_box">
                    <img src="img/limited-time.webp" class="img-fluid">
                  </div>
                </div>
                
              </div>
            </div>
            <div class="item mt50">
              <div class="row align-items-center">
			  <div class="col-12 col-md-6 order-md-2">
                    <h2 class="lg30 bg_yellow2 color_white">Smach The Cash Secrets</h2>
                    <p class="lg20 color_white mt20">You will learn 30 different ways of how you can be making money online. Sell your product in a package deal with other web businesses. You can both advertise it and split the profits.</p>
                    <p class="xs22 lg28 text-left mt20 color_yellow"><b>Value - $667 </b></p>
                </div>
				<div class="col-12 col-md-6 xspt30 pt0 order-md-1">
                    <div class="img_box">
					<img src="img/limited-time.webp" class="img-fluid d-block mx-auto">
                    </div>
                </div>
              </div>
            </div>
            <div class="item mt50">
              <div class="row align-items-center">
			  <div class="col-12 col-md-6">
                  <h2 class="lg30 bg_yellow2 color_white"> 100+ Marketing Emails</h2>
                  <p class="lg20 color_white mt20">Your complete "fill in the blank" autoresponder series you can use for ALL your internet marketing lists and is geared toward TEN main areas of your business!</p>
                  <p class="xs22 lg28 mt20 color_yellow"><b>Value - $567 </b></p>
                </div>
                <div class="col-12 col-md-6 xspt30 pt0">
                  <div class="img_box">
				  <img src="img/limited-time.webp" class="img-fluid d-block mx-auto">
                  </div>
                </div>
                
              </div>
            </div>
            <div class="item mt50">
              <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-2">
                  <h2 class="lg30 bg_yellow2 color_white"> 80% Discount on ALL Upgrades</h2>
                  <p class="lg20 color_white mt20">Get 80% instant discount on purchase of All Upgrades. This is very exclusive bonus which will be taken down soon.</p>
                  <p class="xs22 lg28 text-left mt20 color_yellow"><b>Value - $297 </b></p>
                </div>
                <div class="col-12 col-md-6 xspt30 pt0 order-md-1">
                  <div class="img_box">
				  <img src="img/limited-time.webp" class="img-fluid d-block mx-auto">
                  </div>
                </div>
              </div>
            </div>
            <div class="item mt50">
              <div class="row align-items-center">
			  <div class="col-12 col-md-6">
                  <h2 class="lg30 bg_yellow2 color_white"> 30 Reseller License</h2>
                  <p class="lg20 color_white mt20">You have full rights to use this software. <br><br>You can use it for anyone whether for individuals or for companies. Generate massive free traffic to yourself and for others as well.</p>
                  <p class="xs22 lg28 mt20 color_yellow"><b>Value - $997 </b></p>
                </div>
                <div class="col-12 col-md-6 xspt30 pt0">
                  <div class="img_box">
				  <img src="img/limited-time.webp" class="img-fluid d-block mx-auto">
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="header text-center color_white">
            <h2 class="mt30">Total value of these bonuses are $5857 <br>But Today you will take all these bonuses home for FREE! </h2>
          </div>
        </div>       
      </div>
  	</section>

  	<div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>

  	<section id="take_look">
  		<div class="home12 pt70 pb50">
  			<div class="container">
  				<div class="row">
              <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <div class="content text-center">
				<svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px" xml:space="preserve">
<style type="text/css">
	.st00{fill:#07163A;}
	.st11{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
	.st22{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
	.st33{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000168823944284911347230000002582526602741899929_);}
</style>
<g>
	<g>
		<path class="st00" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
			c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
			c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z
			 M242,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
			c3.6,3.7,7.9,5.6,13,5.6s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
		<path class="st00" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
			c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
			c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
			 M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
			c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
		<path class="st00" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
		<path class="st00" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
			c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
			l0-84.2L505.3,36.7z"/>
		<path class="st00" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
		<path class="st00" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
			c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
			c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
			C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
			c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"
			/>
	</g>
	<g>
		<path class="st11" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
			c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
			<stop  offset="0.5251" style="stop-color:#2089EA"/>
			<stop  offset="0.6138" style="stop-color:#1D7ADE"/>
			<stop  offset="0.7855" style="stop-color:#1751C0"/>
			<stop  offset="1" style="stop-color:#0D1793"/>
		</linearGradient>
		<path class="st22" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
			c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
		
			<linearGradient id="SVGID_00000091705051100733353190000005027636122510766246_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
			<stop  offset="0.3237" style="stop-color:#F4833F"/>
			<stop  offset="0.6981" style="stop-color:#F39242"/>
			<stop  offset="1" style="stop-color:#F2A246"/>
		</linearGradient>
		<path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000091705051100733353190000005027636122510766246_);" d="
			M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
			c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
			c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
	</g>
</g>
</svg>

                  <div class="list mt30 pb50 text-left">
                    <ul>
                      <li class="lg24 xs18">Brand New AppZilo Software - $297 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">Unlimted Bandwidth - $397 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">20000+ Premium HQ Stock Assets - Worth $197 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">Embed Video on any website - Worth $197 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18"><b>FREE Bonus #1:</b> CashKing Pro $227 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18"><b>FREE Bonus #2:</b> Smach The Cash Secrets $667 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18"><b>FREE Bonus #3:</b> 100+ Marketing Emails $567 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18"><b>FREE Bonus #4:</b> 80% Discount on ALL Upgrades $297 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18"><b>FREE Bonus #5:</b> 30 Reseller License $997 value <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">World Class Support... PRICELESS! <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">60 Day Money-Back Guarantee... PRICELESS! <div class="bg_gradent"></div></li>
                      <li class="lg24 xs18">Get Results From Day One PRICELESS!</li>
                    </ul>
                    <div class="mt50 text-center">
				  						<h2 class="color_blue xs22 lg36">Total Value Of Everything You Get</h2>
				  						<h2 class="xs30 lg50">INSIDE AppZilo TODAY:</h2>
				  					</div>

									  <div class="mt50 text-center">
									  <a href="https://warriorplus.com/o2/buy/lbptdw/zr0n64/ggs7cr"><img src="https://warriorplus.com/o2/btn/fn200011000/lbptdw/zr0n64/324989" class="img-fluid d-block mx-auto"></a>
									  </div>

									 
                  </div>
				  
                </div>
				<div class="mt50 text-center">
									  <a href="https://warriorplus.com/o/nothanks/zr0n64" class="nothank">No Thanks</a>
									  </div>
              </div>
          </div>
        	
        </div>
      </div>
    </section>
    <section>
    	<div class="bonus-section" style="background:#ffffff">
    		<div class="container">
  				<div class="row">
  					<div class="col-sm-10 offset-sm-1">
  						<div class="content_box">
	  						<h2 class="text-center xs30 lg50">Many Showers Of <span style="color: #fccc00">Success</span></h2>
	  						<div class="img_box">
	  							<div class="row">
	  								<div class="col-sm-3 offset-sm-3">
											<div class="text-center">
												<img src="img/ayush-jain.png" class="img-fluid rounded-circle" style="max-width: 150px;">
												<h2 class="lg18 mt20">Ayush Jain</h2>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="text-center">
												<img src="img/pranshu-gupta.png" class="img-fluid rounded-circle" style="max-width: 150px;">
												<h2 class="lg18 mt20">Pranshu Gupta</h2>
											</div>
										</div>
	  							</div>
	  						</div>
	  						<p class="mt30 xs22 lg24">P.S. Get AppZilo – Smart Futuristic Video Profit Technology, while we’re excited to offer unrestricted access for a single price … we can only do that during this Limitedd launch.</p>
	  						<p class="xs22 lg24">As soon as launch closes, the price WILL increase to a monthly membership. Get everything now for a low one-time fee … Or risk missing out & pay much more at  MONTHLY price later. The decision is yours</p>
	  					</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>

  	<section>
			<div class="home13 pt70 pb50 ">
				<div class="container">
					<div class="header">
            <h2 class="xs30 lg42 text-center color_blue">Frequently <span style="color: #ee1539;">Asked</span> Questions</h2><br>
					</div>
					<div class="content">
						<ul>
							<li>
								<h2 class="lg26">Q.1 Is this a desktop software? Does it work on a PC?</h2>
								<p class="lg18">This is a Cloud-Based App that doesn’t require any Installation. Simply access it from anywhere, anytime in the world just as long as you have an internet connection!</p>
							</li>
							<li>
								<h2 class="lg26">Q.2 I already have such a tool. What do I do?</h2>
								<p class="lg18">You don’t have such a tool. What AppZilo does…there’s no other tool out there that does that. We checked before we got started. Sure, you may have a tool for creating videos – but what you’re getting today is just extraordinary.</p>
								<p class="lg18">My sincere advice to you would be to cancel your existing subscription and get grandfathered access to AppZilo at a low one-time price right now.</p>
							</li>
							<li>
								<h2 class="lg26">Q.3 Do I need video creation or editing experience?</h2>
								<p class="lg18">None at all! The dashboard & canvas make creating custom videos literally point & click simple. We’ve tested this interface with brand new beginners to ensure it’s 1-2-3 simple!</p>
							</li>
							<li>
								<h2 class="lg26">Q.4 Can I create & sell videos to clients?</h2>
								<p class="lg18">Yes! You can create an unlimited amount of videos and sell them to businesses when you purchase our Commercial License package during the launch period. There are no limits. Set the price you want to charge and keep 100% of the profits. You will never have to pay us any royalties or additional fees on the money you make</p>
							</li>
							<li>
								<h2 class="lg26">Q.5 Are there any training videos included?</h2>
								<p class="lg18">Yes! Even though AppZilo is simple and easy to use, we make it even easier for you with step-by-step training tutorials to get up and running even faster.<br>
							</li>
							<li>
								<h2 class="lg26">Q.6 How much do updates cost?</h2>
								<p class="lg18">Yes! We are constantly working on updating the software and providing you with the latest patches. When you purchase AppZilo updates are automatic and provided free of charge.<br>
							</li>
							<li>
								<h2 class="lg26">Q.7 How do I get support?</h2>
								<p class="lg18">Easy! Just email us  and we will gladly answer any questions you may have. <br>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="cta-section">
			<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<p class=" lg24 xs16 font_al color_white"><span class="color_yellow"><b>&gt;&gt;</b></span> <b>Grab AppZilo Now</b> - $3 DISCOUNT</p>
								<a class="btn_custom_set font_al buy_btn js-scroll-trigger" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class=" lg18 mt20 xs12 font_al color_white">Special Bonus* "30 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
							
						</div>
					</div>				
			</div>
		</section>

		<div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.png"></div>
                     <div class=""><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u class="lg20 xs11"><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>

		<!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->

		
		<section>
            <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><i class="fas fa-hand-paper"></i> WAIT!!!<br>
                                <span>Don't Leave Empty Handed!</span>
                            </h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2><u>Copy This Code</u> "AppZilo" </h2><h3>To Get Your <span><u>$3 Discount</u> Now!</span></h3>
                            <div class="button text-center mt-4">
                                <a href="https://warriorplus.com/o2/buy/lbptdw/zr0n64/ggs7cr" class="btn_custom_set1" target="_blank">Yes! - Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		<!-- Bootstrap core JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
		<!-- Third party plugin JS-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
		<!-- Contact form JS-->
		<script src="assets/mail/jqBootstrapValidation.js"></script>
		<script src="assets/mail/contact_me.js"></script>
		<!-- Core theme JS-->
		<!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
		<script type="text/javascript" src="jquery.exit-modal.js"></script>
		<script rel="preconnect" src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
		<script src="js/scripts.js"></script>

		<script type="text/javascript">
			$(document).bind("contextmenu",function(e){
			  return false;
	    });
			$(document).ready(function(){

				var timer;

				var exitModalParams = {
				    numberToShown:                  5,
				    callbackOnModalShow:            function() {
				        var counter = $('.exit-modal').data('exitModal').showCounter;
				        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
				    },
				    callbackOnModalShown:           function() {
				        timer = setTimeout(function(){
				            // window.location.href = "http://www.jqueryscript.net";
				        }, 4000)
				    },
				    callbackOnModalHide:            function() {
				        clearTimeout(timer);
				    }
				}

				$('.destroy-exit-modal').on("click", function(e){
					e.preventDefault();
					if($('.exit-modal').data('exit-modal')) {
						$(".initialized-state").hide();
						$(".destroyed-state").show();
					}
					$('.exit-modal').exitModal('hideModal');
					$('.exit-modal').exitModal('destroy');
					$(".initialized").hide();
				});

				// $('.init-exit-modal').on('click', function(e){
					// e.preventDefault();
					$('.exit-modal').exitModal(exitModalParams);
					if($('.exit-modal').data('exit-modal')) {
						$(".destroyed-state").hide();
						$(".initialized-state").show();
					}
				// });

				$('.close-exit-modal').on('click', function(e){
					e.preventDefault();
					$('.exit-modal').exitModal('hideModal');
				});

			});
			$(".buy_btn").click(function() {
				$('html, body').animate({
					scrollTop: $("#take_look").offset().top
				}, 1000);
			});
			function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading')
				.find(".more-less")
				.toggleClass('glyphicon-plus glyphicon-minus');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);
		</script>
		<?php
		$rand = rand(21000,30000);
			$today = date('Y-m-d H:i:s');
			$date = date('M d, Y H:i:s', strtotime($today)+$rand);
		?>
		<script type="text/javascript">
			countdownTimeStart();
			function countdownTimeStart(){
				var countDownDate = new Date("Apr 2, 2021 21:47:25").getTime();
				var x = setInterval(function() {
					var now = new Date().getTime();

					// Find the distance between now an the count down date
					var distance = countDownDate - now;
					
					// Time calculations for days, hours, minutes and seconds
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					
					// Output the result in an element with id="demo"
					times = hours + "h "
					+ minutes + "m " + seconds + "s ";

					if (hours <= 9) 
					{
						hours = '0'+hours;
					}
					if (minutes <= 9) 
					{
						minutes = '0'+minutes;
					}
					if (seconds <= 9) 
					{
						seconds = '0'+seconds;
					}
					$('.hours').html(hours);
					$('.minutes').html(minutes);
					$('.seconds').html(seconds);

					// console.log(times);
					
					// If the count down is over, write some text 
					if (distance < 0) {
						clearInterval(x);
						document.getElementById("demo").innerHTML = "EXPIRED";
					}
				}, 1000);
			}
		</script>

	</body>
</html>
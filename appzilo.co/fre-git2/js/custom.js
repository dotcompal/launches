
var myVar;
function countdown(){
	
		var BigDay = new Date("05 December 2019, 10:00:00 EDT"); 
		var msPerDay = 24 * 60 * 60 * 1000;
	   
		myVar = setInterval(function(){ 
					var today = new Date();
					var timeLeft = (BigDay.getTime() - today.getTime());
					
					var e_daysLeft = timeLeft / msPerDay;
					var daysLeft = Math.floor(e_daysLeft);
					
					var e_hrsLeft = (e_daysLeft - daysLeft)*24;
					var hrsLeft = Math.floor(e_hrsLeft);
					
					var e_minsLeft = (e_hrsLeft - hrsLeft)*60;
					var minsLeft = Math.floor(e_minsLeft);
					
					var e_secsLeft = (e_minsLeft - minsLeft)*60;
					var secsLeft = Math.floor(e_secsLeft);
					
					if (daysLeft < 10 && daysLeft >=0) daysLeft = "0" + daysLeft;
					if (hrsLeft < 10) hrsLeft = "0" + hrsLeft;
					if (minsLeft < 10) minsLeft = "0" + minsLeft;
					if (secsLeft < 10) secsLeft = "0" + secsLeft;
				   
					if(daysLeft < 0 || hrsLeft < 0 ||minsLeft < 0 ||secsLeft < 0 ){
						daysLeft = hrsLeft = minsLeft = secsLeft = "00";
					}
					
					var timeString = "<div class='tablerow'>" + "</span>" + 
					"<div class='col-md-3 col-sm-3 col-xs-3'><div class='hrsbox md40 sm30 xs30 w700'>" + daysLeft + 
					"</div><div class='md14 sm14 xs14 w400 time-textlh'>Days</div>" + 
					"</div>" +					
					 
										 
					"<div class='col-md-3 col-sm-3 col-xs-3 md40 sm30 xs30 w700'><div class='hrsbox'>" + hrsLeft + 
					"</div><div class='md14 sm14 xs14 w400 time-textlh'>Hours</div>" + 
					"</div>" +
										
					 
					"<div class='col-md-3 col-sm-3 col-xs-3 md40 sm30 xs30 w700'><div class='hrsbox'>" + minsLeft + 
					"</div><div class='md14 sm14 xs14 w400 time-textlh'>Minutes</div>" + 
					"</div>" + 
					
										 
					"<div class='col-md-3 col-sm-3 col-xs-3 md40 sm30 xs30 w700'><div class='hrsbox'>" + secsLeft + 
					"</div><div class='md14 sm14 xs14 w400 time-textlh'>Seconds</div></div>"
		;

					$('.hs_hours').html(timeString);
					
				}, 1000);

	
}

$(document).ready(function(e) {
		countdown();
});





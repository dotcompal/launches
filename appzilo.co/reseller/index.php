<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>AppZilo _ Reseller</title>
        <!-- Favicon-->
        <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Alata&display=swap" rel="stylesheet">
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/custom.css" rel="stylesheet" />
        <style type="text/css">
            .border_set{
                display: inline-block;
                height: 8px;
                border-radius: 10px;
                width: 230px;
                margin: 0 auto;
                background-color: #02042B;
            }
        </style>
    </head>
    <body id="page-top">

        <section>
            <div class="home1 pt20 ">
                <div class="container">
                    <div class="header">
                        <div class="text-center" style="width: 100%;">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
                        </div>
                        <h1 class="color_white xs28 lg48 xsmt20 mt40">We’ve brought you <span class="bg_orange"><span>a revolutionary method</span></span></h1>
                        <h1 class="color_white xs28 lg48 xsmt10 mt20">where you can <span class="bg_blue"><span>Build Your Online Business</span></span></h1>
                        <h1 class="color_white xs28 lg44 xsmt10 mt20">worth 6-7 Figure EASILY By<span class="bg_orange"><span>Selling AppZilo Accounts!</span></span></h1>
                        <h2 class="xs22 lg26 mt40 text-center font-weight500" style="color: #ffffff">You just have to Sit Back & Get Paid.. Where We Will Do All The Hardwork FOR YOU!</h2>
                        <h3 class="xs20 lg22 mt10 text-center text-white">Zero Experience Needed | Zero Technical Skills Required | Get 100% Instant Commissions</h3>
                    </div>
                    <div class="content mt50">
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8">
                                <div class="video">
                                <img src="img/product-box.webp" class="img-fluid">
                                    <!-- <iframe title="vimeo-player" src="https://player.vimeo.com/video/549195473" width="560" height="315" frameborder="0" allowfullscreen></iframe>  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs18 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs16 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
                <div class="offer-new">
        <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <section>
            <div class="home2 pt50">
                <div class="container-fluid">
                    <div class="header">
                        <h2 class="color_blue xs28 lg42 text-center mt50">MAKE 6-7 Figures In 3 Easy Steps Using <br><span class="bg_yellow1">AppZilo Reseller License</span></h2>
                    </div>
                    <div class="content mt50">
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="img/home2_1.png" class="img-fluid">
                                </div>
                                <div class="col-sm-8">
                                    <h2 class="pt50 color_blue">Use Our Pre-provided,Tested & High Converting Sales Material (Pages, Funnel, Videos) to Sell AppZilo. </h2>
                                    <p>Use all the similar High converting Sales Page and other material which we use to CONVERT YOU! Get complimentary access to tested and proven marketing material to reap the best Conversions!</p>
                                </div>
                            </div>
                        </div>
                        <div class="item item2">
                            <div class="row">
                                <div class="col-sm-4">
                                     <img src="img/home2_2.png" class="img-fluid">
                                </div>
                                <div class="col-sm-8">
                                    <h2 class="pt50 color_blue">Accept Payments directly In your Paypal or Stripe or Bank Account. </h2>
                                    <p>Charge them monthly, yearly or one-time Fee for just simply reselling them accounts of the most needed tool in the online industry today!</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="img/home2_3.png" class="img-fluid">
                                </div>
                                <div class="col-sm-8">
                                    <h2 class="pt50 color_blue">Create Unlimited accounts for your customers with a Simple 1 Click procedure!</h2>
                                    <p>Simply enter the Email ID of Your New Customer In AppZilo Reseller Panel and Create their Account In Seconds with Just a Click.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="cta-button text-center">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<p class="lg24 xs18 font_al white-clr">(Get AppZilo  at one-time Price right away, because we’re going <br class="d-none d-md-block  white-clr">recurring after the launch period)</p>
								<a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
									Click Here To Get Started <br><span>Grab your copy now before it Expires</span>
								</a>
								<p class="lg18 xs16 mt20 font_al white-clr">Special Bonus* "50 Reseller Licenses" If You Buy Today </p>
								<img src="img/home1_2.png" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
                <div class="offer-new">
        <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="img/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING!</b> <b> After Every Hour The Price Goes Up,</b> So Act <br class="d-none d-md-block"> Now To Claim Your <u><i>$3 DISCOUNT</i></u> Or Come Later &amp; Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
            </div>
        </section>
        <section>
            <div class="home3 pt70 pb50">
                <div class="container">
                    <div class="header">
                        <h2 class="text-center color_red xs28 lg42">Here’s a Cherry On Top for YOU!</h2> 
                        <p class="text-center lg32 color_blue">Our Professionals will handle all the Support Tickets for you.</p>
                        <h3 class="text-center lg24">So,That You Can Simple Concentrate on Marketing, Sales & Making More Profits. </h3>
                        <div class="img_box text-center mt50">
                            <a class="btn_custom_set font_al js-scroll-trigger white-clr" href="#take_look">
                            Click Here To Get Your Reseller Account!<br><span>Grab your copy now, before it Expires</span>
								</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="home4 pt50 pb50">
                <div class="container">
                    <div class="header">
                        <h2 class="text-center color_white xs30 lg48">Here’s what you’ll get inside </h2>
                        <div class="text-center mt30">
                            <h3 class="bg_orange xs30 lg48"><span class="color_white">AppZilo Reseller License</span></h3>
                        </div>
                        <!-- <div class="text-center">
                            <img src="img/logo2.png" class="img-fluid" style="width: 450px;">
                        </div> -->
                    </div>
                    <div class="content mt30">
                        <div class="row">
                            <div class="col-12 col-md-10 text-center mx-auto">
                                <img src="img/product-box.webp" class="img-fluid">
                            </div>
                            
                        </div>
                        <div class="row text-center mt30 mt-md50">
                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_2.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>Get Professionally Written, Customized SalesPages & Other high converting MarketingMaterial to attract high paying prospects andcustomers.</p>
                                </div>
                                
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_3.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>Start making quadruple PROFIT INSTANTLY!</p>
                                </div>
                                
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_4.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>Easy return on investment guaranteed.</p>
                                </div>
                                
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_5.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>Get paid directly in your paypal, Stripe  Bank account etc.</p>
                                </div>
                                
                            </div>

                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_6.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>We’ll handle all your support tickets with24*7 high priority availability by ourExperienced Staff.</p>
                                </div>
                                
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <img src="img/home4_7.png" class="img-fluid">
                                    <p class="color_white xs20 lg22 "><i class="fas fa-check-circle color_yellow"></i>Get 5 Fast Action Hand Picked Bonuses to start with! </p>
                                </div>
                                
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="home5_back" style="background-color: #01002E;">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                  <path fill="#36185A" fill-opacity="1" d="M0,256L48,229.3C96,203,192,149,288,154.7C384,160,480,224,576,218.7C672,213,768,139,864,128C960,117,1056,171,1152,197.3C1248,224,1344,224,1392,224L1440,224L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
                </svg>
                <div class="home5">
                    <div class="container">
                        <div class="header">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="img/home5_2.png" class="img-fluid">
                                </div>
                                <div class="col-sm-8">
                                    <h2 class="text-center pt100 color_white xs24 lg36">We’ve already told you that it’s a once in alifetime opportunity to become anOfficial AppZilo Reseller and join thisTop 5% Club to ever Reap Profit LIKE THIS!</h2>
                                </div>
                            </div>
                        </div>
                        
                        </div>
                        <div class="footer_content pb50">
                            <h3 class="color_yellow text-center">We already told you that this is ONE-TIME CHANCE to grab our Reseller License & make pure Profit in the explosive Email Market <br><br>Jump in the Explosive Email Market with a BOOM! Grab MailJam’s Reseller License Today.</h3>
                        </div>
                    </div>
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                  <path fill="#36185A" fill-opacity="1" d="M0,192L48,192C96,192,192,192,288,181.3C384,171,480,149,576,160C672,171,768,213,864,197.3C960,181,1056,107,1152,85.3C1248,64,1344,96,1392,112L1440,128L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
            </svg>
        </section>
        <section>
            <div class="home6 pb50">
                <div class="container">
                    <div class="header">
                        <h2 class="text-center">To start a business it takes Investment, Hardwork and TIME!<br>But today we totally <span class="bg_orange"><span class="color_white">Fool-Proofed it FOR YOU!</span></span></h2>
                    </div>
                    <div class="content mt50">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="item text-center">
                                    <img src="img/home6_1.png" class="img-fluid">
                                    <p class="lg28">You don’t need to hire <br>Expensive Developers</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="item text-center">
                                    <img src="img/home6_2.png" class="img-fluid">
                                    <p class="lg28">You don’t have to <br>research ‘Anything’ </p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="item text-center">
                                    <img src="img/home6_3.png" class="img-fluid">
                                    <p class="lg28">You don’t need to hire any costlySupport Desk Professional!</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="item text-center">
                                    <img src="img/home6_4.png" class="img-fluid">
                                    <p class="lg28">You don’t have to even pay for any type of sales material like sales pitch, Sales page design, videos, mockups, social posts, complex graphics etc.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140  text-white text-center col-12 mb-4">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block">"30-Day Risk-Free Money Back Guarantee"
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 text-white mt15 ">
                     I'm 100% confident that AppZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>

        <section>
            <div class="home9 pt70 pb50" id="take_look">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
                        </div>
                        <div class="col-12 col-md-8 mx-auto">
                            <div class="content text-center">
                                <div class="list mt50">
                                    <ul>
                                        <li class="lg24">500 Account Licenses <div class="bg_gradent"></div></li>
                                        <li class="lg24">All Sales Materials For Bestest Conversions <div class="bg_gradent"></div></li>
                                        <li class="lg24">We Will Handle Support of Your All Clients <div class="bg_gradent"></div></li>
                                        <li class="lg24">Accept Payment on Paypal, Stripe & Bank Account ETC. <div class="bg_gradent"></div></li>
                                        <li class="lg24">ONE TIME FEE.. NEVER PAY AGAIN <div class="bg_gradent"></div></li>
                                        <li class="lg24">5 Fast Action Hand Picked Bonuses</li>
                                    </ul>
                                </div>
                                <a href="https://warriorplus.com/o2/buy/lbptdw/c50zj9/r5ny49"><img src="https://warriorplus.com/o2/btn/fn200011000/lbptdw/c50zj9/324993" class="img-fluid d-block mx-auto"></a> 
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
				
              
					<center><a href="https://warriorplus.com/o/nothanks/c50zj9" class="nothank">No Thanks</a></center>
                    
                </div>
            </div>
        </section>

        <!-- Copyright Section-->
     <!--Footer Section Start -->
		<div class="footer-section">
			<div class="container ">
				<div class="row">
					<div class="col-12 text-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
				   <style type="text/css">
					   .st0{fill:#FFFFFF;}
					   .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
					   .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
					   .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
				   </style>
				   <g>
					   <path class="st0" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
						   c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
						   s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
					   <path class="st0" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
						   c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
						   c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
							M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
						   c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
					   <path class="st0" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
					   <path class="st0" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
						   c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
						   l0-84.2L505.3,36.7z"/>
					   <path class="st0" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
					   <path class="st0" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
						   c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
						   c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
						   C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
						   c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
				   </g>
				   <g>
					   <path class="st1" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
						   <stop  offset="0.5251" style="stop-color:#2089EA"/>
						   <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
						   <stop  offset="0.7855" style="stop-color:#1751C0"/>
						   <stop  offset="1" style="stop-color:#0D1793"/>
					   </linearGradient>
					   <path class="st2" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
						   c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
					   
						   <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
						   <stop  offset="0.3237" style="stop-color:#F4833F"/>
						   <stop  offset="0.6981" style="stop-color:#F39242"/>
						   <stop  offset="1" style="stop-color:#F2A246"/>
					   </linearGradient>
					   <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
						   M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
						   c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
						   c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
				   </g>
				   </svg>
				   
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
					</div>
					<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
						<div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © AppZilo 2022</div>
						<ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
							<li><a href="https://newaccount1630482523204.freshdesk.com/support/tickets/new" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
							<li><a href="http://appzilo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--Footer Section End -->

        <section>
            <div class="modal exit-modal" tabindex="-1" role="dialog" id="modal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"> <i class="fas fa-hand-paper"></i> WAIT!!!<br>
                                <span>Don't Leave Empty Handed!</span>
                            </h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h2><u>Copy This Code</u> "AppZilo" </h2><h3>To Get Your <span><u>$3 Discount</u> Now!</span></h3>
                            <div class="button text-center mt30">
                                <a href="https://warriorplus.com/o2/buy/lbptdw/c50zj9/r5ny49" class="btn_custom_set1" target="_blank">Yes! - Upgrade My Order Now! <br><span>Limited To  First 100 People, Hurry Act Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

         <!-- Bootstrap core JS-->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
        <script type="text/javascript" src="jquery.exit-modal.js"></script>
        <script src="js/scripts.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var timer;

                var exitModalParams = {
                    numberToShown:                  5,
                    callbackOnModalShow:            function() {
                        var counter = $('.exit-modal').data('exitModal').showCounter;
                        $('.exit-modal .modal-body p').text("Exit modal shown "+counter+" times");
                    },
                    callbackOnModalShown:           function() {
                        timer = setTimeout(function(){
                            // window.location.href = "http://www.jqueryscript.net";
                        }, 4000)
                    },
                    callbackOnModalHide:            function() {
                        clearTimeout(timer);
                    }
                }

                $('.destroy-exit-modal').on("click", function(e){
                    e.preventDefault();
                    if($('.exit-modal').data('exit-modal')) {
                        $(".initialized-state").hide();
                        $(".destroyed-state").show();
                    }
                    $('.exit-modal').exitModal('hideModal');
                    $('.exit-modal').exitModal('destroy');
                    $(".initialized").hide();
                });

                // $('.init-exit-modal').on('click', function(e){
                    // e.preventDefault();
                    $('.exit-modal').exitModal(exitModalParams);
                    if($('.exit-modal').data('exit-modal')) {
                        $(".destroyed-state").hide();
                        $(".initialized-state").show();
                    }
                // });

                $('.close-exit-modal').on('click', function(e){
                    e.preventDefault();
                    $('.exit-modal').exitModal('hideModal');
                });

            });
            $(".buy_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $("#warrior_plus").offset().top
                }, 1000);
            });
            function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
        </script>
        <?php
        $rand = rand(21000,30000);
            $today = date('Y-m-d H:i:s');
            $date = date('M d, Y H:i:s', strtotime($today)+$rand);
        ?>
        <script type="text/javascript">
            countdownTimeStart();
            function countdownTimeStart(){
                var countDownDate = new Date("<?= $date;?>").getTime();
                var x = setInterval(function() {
                    var now = new Date().getTime();

                    // Find the distance between now an the count down date
                    var distance = countDownDate - now;
                    
                    // Time calculations for days, hours, minutes and seconds
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    
                    // Output the result in an element with id="demo"
                    times = hours + "h "
                    + minutes + "m " + seconds + "s ";

                    if (hours <= 9) 
                    {
                        hours = '0'+hours;
                    }
                    if (minutes <= 9) 
                    {
                        minutes = '0'+minutes;
                    }
                    if (seconds <= 9) 
                    {
                        seconds = '0'+seconds;
                    }
                    $('.hours').html(hours);
                    $('.minutes').html(minutes);
                    $('.seconds').html(seconds);

                    // console.log(times);
                    
                    // If the count down is over, write some text 
                    if (distance < 0) {
                        clearInterval(x);
                        document.getElementById("demo").innerHTML = "EXPIRED";
                    }
                }, 1000);
            }
        </script>
    </body>
</html>

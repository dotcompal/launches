<!Doctype html>
<html>
   <head>
      <title>Prelaunch Special Webinar | WebPrimo Plus</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WebPrimo Plus | Prelaunch">
      <meta name="description" content="WebPrimo Plus">
      <meta name="keywords" content="WebPrimo Plus">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WebPrimo Plus | Prelaunch">
      <meta property="og:description" content="WebPrimo Plus">
      <meta property="og:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WebPrimo Plus | Prelaunch">
      <meta property="twitter:description" content="WebPrimo Plus">
      <meta property="twitter:image" content="https://www.webprimo.co/prelaunch/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>   
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css" >
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <!-- End -->
      <script>
         function getUrlVars()
         {
             var vars = [], hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for(var i = 0; i < hashes.length; i++)
             {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value',first);
         })
         //document.getElementById('myField').value = first;
      </script>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-MMDW8FD');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <!-- New Timer  Start-->
      <?php
         $date = 'December 08 2022 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <div id="templateBody">
         <div id="space-parent">
            <!-- Header Section Start -->
            <div class="space-section banner-section">
               <div class="container">
                  <div class="row">
                     <div class="col-12">
                        <div class="text-center">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:50px" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#1F91C9;}
                                 .st1{fill:#9C2341;}
                                 .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                                 .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                                 .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                                 .st5{fill:#FFFFFF;}
                                 .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                              </style>
                              <g>
                                 <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
                                    c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"></path>
                                 <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                                    l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"></path>
                                 <g>
                                    <g>
                                       <g>
                                          <defs>
                                             <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                                298.8,146.9 					"></polygon>
                                          </defs>
                                          <clipPath id="SVGID_2_">
                                             <use xlink:href="#SVGID_1_" style="overflow:visible;"></use>
                                          </clipPath>
                                          <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                             <stop offset="0" style="stop-color:#12539E"></stop>
                                             <stop offset="0.419" style="stop-color:#1F91C9"></stop>
                                             <stop offset="1" style="stop-color:#1F91C9"></stop>
                                          </linearGradient>
                                          <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"></polygon>
                                       </g>
                                    </g>
                                 </g>
                                 <g>
                                    <g>
                                       <g>
                                          <defs>
                                             <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"></polygon>
                                          </defs>
                                          <clipPath id="SVGID_5_">
                                             <use xlink:href="#SVGID_4_" style="overflow:visible;"></use>
                                          </clipPath>
                                          <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                             <stop offset="0" style="stop-color:#6D0C28"></stop>
                                             <stop offset="0.514" style="stop-color:#9C2341"></stop>
                                             <stop offset="1" style="stop-color:#9C2341"></stop>
                                          </linearGradient>
                                          <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"></polygon>
                                       </g>
                                    </g>
                                 </g>
                                 <g>
                                    <g>
                                       <g>
                                          <defs>
                                             <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                                l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                                c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                                l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"></path>
                                          </defs>
                                          <clipPath id="SVGID_8_">
                                             <use xlink:href="#SVGID_7_" style="overflow:visible;"></use>
                                          </clipPath>
                                          <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                             <stop offset="0" style="stop-color:#FD8B25"></stop>
                                             <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                             <stop offset="1" style="stop-color:#EF4900"></stop>
                                          </linearGradient>
                                          <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"></rect>
                                       </g>
                                    </g>
                                 </g>
                                 <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
                                    475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"></polygon>
                                 <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                                    c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                                    c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                                    H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                                    c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                                    c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"></path>
                                 <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                                    c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                                    c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"></path>
                                 <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                                    c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                                    c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"></path>
                                 <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"></rect>
                                 <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
                                    1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"></polygon>
                                 <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                                    c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                                    c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                                    C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                                    c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                                    c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                                    C1425.9,210,1435.8,207.5,1444.3,202.4"></path>
                                 <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                                    <stop offset="0" style="stop-color:#FD8B25"></stop>
                                    <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                    <stop offset="1" style="stop-color:#EF4900"></stop>
                                 </linearGradient>
                                 <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
                                    h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                                    C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"></path>
                              </g>
                           </svg>
                        </div>
                        <div class="mt20 mt-md50 text-center">
                           <div class="f-20 f-md-24 w400 text-center white-clr lh150 pre-headline">
                              Register for WebPrimo Plus Pre-Launch Webinar & Get an Assured Gift + 10 Free Licenses
                           </div>
                           <div class="mt5">
                           </div>
                        </div>
                        <div class="mt-md15 mt15">
                           <div class="text-center">
                              <div class="col-12 mt20 mt-md30 f-md-45 f-28 w700 lh150 text-center white-clr title-bg1">
                                 Discover How to<br class="d-none d-md-block"> <span class="gradient-text"> Create Premium Websites For Any Business</span><br class="d-none d-md-block"> & Tap Into $284 Billion Website Building Industry With Zero Monthly Fee Ever…
                              </div>
                           </div>
                        </div>
                        <div class=" mt20 mb15 mb-sm10 px0 text-center">
                           <div class="col-12 mt20 mt-md50 f-md-24 f-20 w500 white-clr text-center">
                              Discover how I have sold over $9 Mn in products & earned $4Mn in affiliate commissions by doing it all from one single dashboard and without any tech hassles.
                           </div>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="row">
                           <div class="col-12 col-md-7 mt20 mt-md70">
                              <!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
                                 <iframe src="https://Coursova.dotcompal.com/video/embed/9bo529lat3" style="width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                                 </div> -->
                              <div class="text-center">
                                 <img src="assets/images/video-bg.png" class="img-fluid video-img">
                              </div>
                           </div>
                           <div class="col-12 col-md-5 mt20 mt-md40">
                              <div class="register-form-back">
                                 <div class="">
                                    <div class="f-md-30 f-24 w700 lh140 text-center black-clr spartan-font" editabletype="text" style="z-index: 10;">
                                       Register for Free Training
                                    </div>
                                    <div class="f-md-19 f-20 text-center w400 lh150 mb15 mt8" editabletype="text" style="z-index: 10;">
                                       <span class="w700">On 8<sup>th</sup> December, 10 AM EST.</span> <br>
                                       Book Your Seat now (Limited to 100 Users)
                                    </div>
                                 </div>
                                 <!-- Form Code -->
                                 <div class="">
                                    <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                                       <div style="display: none;">
                                          <input type="hidden" name="meta_web_form_id" value="1733350388" />
                                          <input type="hidden" name="meta_split_id" value="" />
                                          <input type="hidden" name="listname" value="awlist6403508" />
                                          <input type="hidden" name="redirect" value="https://www.webprimo.co/prelaunch-thankyou" id="redirect_58d4848568985b501a89a1474abd1957" />
                                          <input type="hidden" name="meta_adtracking" value="Prelaunch_Web_Form" />
                                          <input type="hidden" name="meta_message" value="1" />
                                          <input type="hidden" name="meta_required" value="name,email" />
                                          <input type="hidden" name="meta_tooltip" value="" />
                                       </div>
                                       <div id="af-form-1733350388" class="af-form">
                                          <div id="af-body-1733350388" class="af-body af-standards">
                                             <div class="af-element">
                                                <label class="previewLabel" for="awf_field-115071275"></label>
                                                <div class="af-textWrap ">
                                                   <div class="input-type" style="z-index: 11;">
                                                      <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                                      <input id="awf_field-115071275" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                                   </div>
                                                </div>
                                                <div class="af-clear bottom-margin"></div>
                                             </div>
                                             <div class="af-element">
                                                <label class="previewLabel" for="awf_field-115071276"> </label>
                                                <div class="af-textWrap">
                                                   <div class="input-type" style="z-index: 11;">
                                                      <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                                      <input class="text form-control custom-input input-field" id="awf_field-115071276" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                                   </div>
                                                </div>
                                                <div class="af-clear bottom-margin"></div>
                                             </div>
                                             <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                             <div class="af-element" style="padding:0px;">
                                                <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                                <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                                <div class="af-clear"></div>
                                             </div>
                                             <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn f-22 f-md-26" style="padding:0;">
                                                <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                                <div class="af-clear bottom-margin"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jBwcHBysbCzszA==" alt="" /></div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- Icon Move Start -->
               <div class="website-move"><img src="assets/images/website-repair-icon.png" alt="" class="img-fluid d-none d-xl-block mx-auto web-move"></div>
               <div class="website-repair"><img src="assets/images/website-icon.png" alt="" class="img-fluid d-none d-xl-block mx-auto web-repair"></div>
               <!-- Icon Move End -->
            </div>
            <!-- Header Section End -->
            <!-- Prize Section Start -->
            <div class="space-section prize-section">
               <div class="container">
                  <div class="row">
                     <div class="col-12 col-md-5 mx-auto mb-sm50 mb20 timer-block text-center p-md0 px15">
                        <div class="f-md-22 f-20 w500 lh140 px0 px-sm15">
                           Hurry Up! Free Offer Going Away in… 
                        </div>
                        <div class="col-12 mt-md20 mt20">
                           <div class="countdown counter-black">
                              <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
                              <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
                              <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
                              <div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr mt15">
                           Register for FREE Training and WIN Gifts
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mx-auto">
                        <div class="f-md-22 f-20 w500 lh150 mt10 mt-md15">
                           My 10+ Years of experience is packaged in this state-of-art 1 hour webinar where you’ll learn:
                        </div>
                        <div class="f-md-22 f-20 w300 lh150 p0 mt20 mt-md40">
                           <ul class="list-style lh150">
                              <li>How I sold over $9Mn in digital products online and <span class="w500">you can follow the same to build a profitable business online.</span></li>
                              <li>How you can Tap into the fastest growing website building industry <span class="w500">without any tech hassles.</span></li>
                              <li>
                                 A Sneak-Peak of WebPrimo Plus to Create Premium Websites For Any Business In Any Niche Without Any Hassles<span class="w500">–From One Platform</span>
                              </li>
                              <li>During This Launch Special Deal, <span class="w500">Get All Benefits at Limited Low One-Time-Fee.</span> </li>
                              <li>Lastly, everyone who attend this session will get an assured gift from us plus <span class="w500">we are giving 10 licenses of our Premium solution - WebPrimo Plus</span></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mx-auto mt25 mt-md85 p0" id="form">
                        <div class="auto_responder_form inp-phold formbg col-12">
                           <div class="f-20 f-md-23 w400 text-center lh180 white-clr">
                              Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
                              <span class="f-22 f-md-32 w700 text-center white-clr" contenteditable="false">8<sup contenteditable="false">th</sup> December, 10 am Est.(100 Seats Only)</span>
                           </div>
                           <div class="row">
                              <div class="col-12 col-md-6 mx-auto  p-md0 px15 my20 timer-block text-center">
                                 <div class="col-md-12 col-12 f-md-22 f-20 w500 lh140 px0 px-sm15 white-clr">
                                    Hurry Up! Free Offer Going Away in… 
                                 </div>
                                 <div class="col-12 col-md-10 col-md-offset-0 p0 mt-md20 mt20">
                                    <div class="countdown counter-black">
                                       <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
                                       <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
                                       <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
                                       <div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Aweber Form Code -->
                           <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                              <!-- Aweber Form Code -->
                              <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                                 <div style="display: none;">
                                    <input type="hidden" name="meta_web_form_id" value="1733350388" />
                                    <input type="hidden" name="meta_split_id" value="" />
                                    <input type="hidden" name="listname" value="awlist6403508" />
                                    <input type="hidden" name="redirect" value="https://www.webprimo.co/prelaunch-thankyou" id="redirect_58d4848568985b501a89a1474abd1957" />
                                    <input type="hidden" name="meta_adtracking" value="Prelaunch_Web_Form" />
                                    <input type="hidden" name="meta_message" value="1" />
                                    <input type="hidden" name="meta_required" value="name,email" />
                                    <input type="hidden" name="meta_tooltip" value="" />
                                 </div>
                                 <div id="af-form-1733350388" class="af-form">
                                    <div id="af-body-1733350388" class="af-body af-standards">
                                       <div class="af-element width-form">
                                          <label class="previewLabel" for="awf_field-115071275"></label>
                                          <div class="af-textWrap">
                                             <div class="input-type" style="z-index: 11;">
                                                <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                                <input id="awf_field-115071275" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                             </div>
                                          </div>
                                          <div class="af-clear bottom-margin"></div>
                                       </div>
                                       <div class="af-element width-form">
                                          <label class="previewLabel" for="awf_field-115071276"> </label>
                                          <div class="af-textWrap">
                                             <div class="input-type" style="z-index: 11;">
                                                <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                                <input class="text form-control custom-input input-field" id="awf_field-115071276" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                             </div>
                                          </div>
                                          <div class="af-clear bottom-margin"></div>
                                       </div>
                                       <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                       <!--<div class="af-element">
                                          <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                          <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                          <div class="af-clear"></div>
                                          </div>-->
                                       <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn1 f-22 f-md-21 width-form" style="padding-top:5px;">
                                          <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                          <div class="af-clear bottom-margin"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jBwcHBysbCzszA==" alt="" /></div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Prize Section End -->	
            <!--Footer Section Start -->
            <div class="space-section footer-section">
               <div class="container">
                  <div class="row">
                     <div class="col-12 text-center">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:50px" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#1F91C9;}
                              .st1{fill:#9C2341;}
                              .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                              .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                              .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                              .st5{fill:#FFFFFF;}
                              .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                           </style>
                           <g>
                              <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
                                 c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"></path>
                              <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                                 l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"></path>
                              <g>
                                 <g>
                                    <g>
                                       <defs>
                                          <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                             298.8,146.9 					"></polygon>
                                       </defs>
                                       <clipPath id="SVGID_2_">
                                          <use xlink:href="#SVGID_1_" style="overflow:visible;"></use>
                                       </clipPath>
                                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                          <stop offset="0" style="stop-color:#12539E"></stop>
                                          <stop offset="0.419" style="stop-color:#1F91C9"></stop>
                                          <stop offset="1" style="stop-color:#1F91C9"></stop>
                                       </linearGradient>
                                       <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"></polygon>
                                    </g>
                                 </g>
                              </g>
                              <g>
                                 <g>
                                    <g>
                                       <defs>
                                          <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"></polygon>
                                       </defs>
                                       <clipPath id="SVGID_5_">
                                          <use xlink:href="#SVGID_4_" style="overflow:visible;"></use>
                                       </clipPath>
                                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                          <stop offset="0" style="stop-color:#6D0C28"></stop>
                                          <stop offset="0.514" style="stop-color:#9C2341"></stop>
                                          <stop offset="1" style="stop-color:#9C2341"></stop>
                                       </linearGradient>
                                       <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"></polygon>
                                    </g>
                                 </g>
                              </g>
                              <g>
                                 <g>
                                    <g>
                                       <defs>
                                          <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                             l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                             c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                             l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"></path>
                                       </defs>
                                       <clipPath id="SVGID_8_">
                                          <use xlink:href="#SVGID_7_" style="overflow:visible;"></use>
                                       </clipPath>
                                       <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                          <stop offset="0" style="stop-color:#FD8B25"></stop>
                                          <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                          <stop offset="1" style="stop-color:#EF4900"></stop>
                                       </linearGradient>
                                       <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"></rect>
                                    </g>
                                 </g>
                              </g>
                              <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
                                 475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"></polygon>
                              <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                                 c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                                 c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                                 H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                                 c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                                 c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"></path>
                              <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                                 c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                                 c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"></path>
                              <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                                 c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                                 c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"></path>
                              <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"></rect>
                              <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
                                 1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"></polygon>
                              <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                                 c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                                 c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                                 C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                                 c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                                 c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                                 C1425.9,210,1435.8,207.5,1444.3,202.4"></path>
                              <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                                 <stop offset="0" style="stop-color:#FD8B25"></stop>
                                 <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                 <stop offset="1" style="stop-color:#EF4900"></stop>
                              </linearGradient>
                              <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
                                 h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                                 C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"></path>
                           </g>
                        </svg>
                        <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                     </div>
                     <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                        <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © WebPrimo Plus</div>
                        <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                           <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                           <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--Footer Section End -->
         </div>
      </div>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-1888856473').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1888856473")) {
                     document.getElementById("af-form-1888856473").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1888856473")) {
                     document.getElementById("af-body-1888856473").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1888856473")) {
                     document.getElementById("af-header-1888856473").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1888856473")) {
                     document.getElementById("af-footer-1888856473").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
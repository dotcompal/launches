<html>
   <head>
      <title>JV Page - WebPrimo Plus JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WebPrimo Plus | JV">
      <meta name="description" content="WebPrimo Plus">
      <meta name="keywords" content="WebPrimo Plus">
      <meta property="og:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WebPrimo Plus | JV">
      <meta property="og:description" content="WebPrimo Plus">
      <meta property="og:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WebPrimo Plus | JV">
      <meta property="twitter:description" content="WebPrimo Plus">
      <meta property="twitter:image" content="https://www.webprimo.co/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <!-- End -->
      <script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/ false;
             if (!IE) {
                 return;
             }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1059432944")) {
                     document.getElementById("af-form-1059432944").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1059432944")) {
                     document.getElementById("af-body-1059432944").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1059432944")) {
                     document.getElementById("af-header-1059432944").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1059432944")) {
                     document.getElementById("af-footer-1059432944").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-MMDW8FD');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Dec 15 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div id="particles"></div>  
         <div class="container relative">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	               viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:40px" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#1F91C9;}
                        .st1{fill:#9C2341;}
                        .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                        .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                        .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                        .st5{fill:#FFFFFF;}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                     </style>
                     <g>
	                     <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
		                  c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"/>
	                     <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                        l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"/>
	                     <g>
		                     <g>
			                     <g>
                                 <defs>
                                    <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                       298.8,146.9 					"/>
                                 </defs>
                                 <clipPath id="SVGID_2_">
                                    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                    <stop  offset="0" style="stop-color:#12539E"/>
                                    <stop  offset="0.419" style="stop-color:#1F91C9"/>
                                    <stop  offset="1" style="stop-color:#1F91C9"/>
				                     </linearGradient>
				                     <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"/>
			                     </g>
		                     </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"/>
                                 </defs>
                                 <clipPath id="SVGID_5_">
                                    <use xlink:href="#SVGID_4_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                    <stop  offset="0" style="stop-color:#6D0C28"/>
                                    <stop  offset="0.514" style="stop-color:#9C2341"/>
                                    <stop  offset="1" style="stop-color:#9C2341"/>
				                     </linearGradient>
				                     <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"/>
			                     </g>
                           </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                    l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                    c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                    l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"/>
                                 </defs>
                                 <clipPath id="SVGID_8_">
                                    <use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                    <stop  offset="0" style="stop-color:#FD8B25"/>
                                    <stop  offset="0.581" style="stop-color:#FD8B25"/>
                                    <stop  offset="1" style="stop-color:#EF4900"/>
				                     </linearGradient>
				                     <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"/>
                              </g>
                           </g>
                        </g>
	                     <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
		                  475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"/>
	                     <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                        c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                        c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                        H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                        c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                        c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"/>
	                     <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                        c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                        c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"/>
	                     <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                        c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                        c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"/>
	                     <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"/>
	                     <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
		                  1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"/>
	                     <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                        c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                        c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                        C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                        c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                        c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                        C1425.9,210,1435.8,207.5,1444.3,202.4"/>
	                     <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                           <stop  offset="0" style="stop-color:#FD8B25"/>
                           <stop  offset="0.581" style="stop-color:#FD8B25"/>
                           <stop  offset="1" style="stop-color:#EF4900"/>
	                     </linearGradient>
	                     <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
		                  h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                        C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"/>
                     </g>
                  </svg>

               <div class="d-flex align-items-center flex-wrap justify-content-center justify-content-md-end mt15 mt-md0">
                  <ul class="leader-ul f-md-18 f-16 w500 white-clr">
                     <li>
                        <a href="https://docs.google.com/document/d/141Udj0MGCDcObv3Nt59N1JZP_KHUIHd7/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">JV Docs</a>
                     </li>
                     <li>|</li>
                     <li>
                        <a href="https://docs.google.com/document/d/1TcYP9s45edKRTM5rahX6P_u9XKWduz88/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes & Bonuses</a>
                     </li>
                     <li>|</li>
                     <li>
                        <a href="#funnel">Sales Pages</a>
                     </li>
                  </ul>
                  <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/390274" class="affiliate-link-btn ml-md15 mt10 mt-md0" target="_blank">Grab Your Affiliate Link</a>
               </div>
            </div>
            <div class="col-12 mt20 mt-md70 text-center">
               <div class="f-20 f-md-24 w600 text-center white-clr lh140 text-uppercase pre-headline">
                  Coming Up with a Big Bang Bundle Offer, KILLER Webinar and $10K in JV Prizes
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 ">
               <div class="f-md-42 f-28 w700 text-center white-clr lh150 title-bg1">
                  Promote JVZoo’s First <br class="d-none d-md-block">
                  <span class="gradient-text">Hybrid WordPress Website & Store Builder Framework</span> 
                  That Creates Premium Websites <br class="d-none d-md-block"> for Any Business in Just 7 Minutes...
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 f-md-24 f-20 w500 white-clr text-center">
               Easily create & sell websites for big profits to Retail Stores, Ecom Stores, Book Shops, Coaches, <br class="d-none d-md-block">
               Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, & many other Niches
            </div>
            <div class="col-12 col-md-12 mt20 mt-md80">
               <div class="row align-items-center">
                  <div class="col-md-8 col-12 min-md-video-width-left">
                     <!-- <img src="assets/images/video-thumbnail.webp" class="img-fluid d-block mx-auto"> -->
                     <div class="col-12 responsive-video">
                        <iframe src="https://webprimo2.dotcompal.com/video/embed/91qis1mbb4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
                  </div>
                  <div class="col-md-4 col-12 min-md-video-width-right mt20 mt-md0">
                     <img src="assets/images/date-img.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md90 align-items-center">
            <div class="col-12 col-md-6">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon1.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                           <span class="w600">Install and Sell</span> Elegant, Highly Professional and Conversion Focused Websites. 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon2.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                           Seamless <span class="w600">Woo Commerce Integration</span> to accept payment for your product &amp; services
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md30">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon3.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                           Super Customizable Suite with <span class="w600">Over 200+ Templates and 2000+ Possible Combination</span> to Instantly Create Website for Any Niche
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md30">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon4.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w500 lh140 white-clr">
                           Pre-Loaded with Tons of <span class="w600">Editable Local Niche Marketing Graphics &amp; Videos, Social Media Graphics, Logo Kit and Royalty free Stock Images &amp; Videos.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md30">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon5.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w500 lh140 white-clr">
                           Built on Most <span class="w600">Advance Word Press Frame Work</span> to Make is Super Easy for Marketers &amp; Newbies with No Coding or Tech Skills Required.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md30">
               <div class="col-12 icon-box-inn">
                  <div class="row m-0">
                     <div class="col-md-2 p-0 icon-box-img">
                        <img src="assets/images/iconBg.webp" class="img-fluid mx-auto d-block iconBg" alt="iconBg">  
                        <img src="assets/images/icon6.webp" class="img-fluid mx-auto d-block icon" alt="icon">  
                     </div>        
                     <div class="col-md-10">
                        <div class="f-md-20 f-18 w500 lh140 white-clr">
                           <span class="w600">Commercial License Included</span> So Your Buyers Can Sell Services And Make Profit Big Time.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Icon Move Start -->
      <div class="website-move"><img src="assets/images/website-repair-icon.webp" alt="" class="img-fluid d-none d-xl-block mx-auto web-move"></div> 
      <div class="website-repair"><img src="assets/images/website-icon.webp" alt="" class="img-fluid d-none d-xl-block mx-auto web-repair"></div> 
      <!-- Icon Move End -->
      </div>
      <!-- Header Section End -->

      <div class="live-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
                     <div class="white-clr f-md-50 f-20 d-block mb0 lh140 w700 text-center">
                        Subscribe To Our JV List<br class="d-lg-block d-none"> 
                        <span class="f-20 f-md-30 w600 text-center white-clr lh140 mt5 d-lg-inline-block">
                        and Be The First to Know Our Special Contest Events and Discounts
                        </span>
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt15 mt-md50">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="2033102032" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6241145" />
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_3defea21bdd383fbaa4ec7d3da25996d" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-2033102032" class="af-form">
                              <div id="af-body-2033102032" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-4">
                                    <label class="previewLabel" for="awf_field-113826580" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb15 mb-md15 input-type">
                                       <input id="awf_field-113826580" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb15 mb-md25  col-md-4">
                                    <label class="previewLabel" for="awf_field-113826581" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-113826581" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type popup-btn white-clr col-md-4">
                                    <input name="submit" class="submit f-20 f-md-22 white-clr center-block" type="submit" value="Subscribe for JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TAzMzIwMTAzMTA==" alt="" /></div>
                        </form>
                        <!-- Aweber Form Code -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
                  <div class="f-24 f-md-32 w600 text-center lh140">
                     <span class="">Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3  mt-md19 mt15">
                        <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-12 col-md-5 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/390274" class="f-24 f-md-30 mx-auto" target="_blank">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- launch-special -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-42 f-28 black-clr text-center w700 lh140">
                     This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
               <div class="col-12 mt-md60 mt30">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12 order-md-2">
                        <img src="assets/images/phase1.webp" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 p-md0 mt-md0 mt20 order-md-1">
                        <div class="f-md-32 f-24 lh140 w600">
                           To Make You Max Commissions
                        </div>
                        <ul class="launch-tick1 pl-md25 pl20 f-md-24 f-18 mb0 mt-md30 mt20">
                           <li>All Leads Are Hardcoded</li>
                           <li>Exciting $2000 Pre-Launch Contest</li>
                           <li>We'll Re-Market Your Leads Heavily</li>
                           <li>Pitch Bundle Offer on webinars.</li>
                        </ul>
                     </div>
                  </div>
                  <div class="row mt-md10 mt20 align-items-center">
                     <div class="col-md-6 col-12">
                        <img src="assets/images/phase2.webp" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 mt-md0 mt20">
                        <ul class="launch-tick1 pl-md25 pl20 f-md-24 f-18 mb0">
                           <li>High in Demand Product with Top Conversion</li>
                           <li>Deep Funnel to Make You Double Digit EPCs</li>
                           <li>Earn upto $358/Sale</li>
                           <li>Huge $10K JV Prizes</li>
                           <li>We'll Re-Market Your Visitors Heavily</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 white-clr heading-design text-center">
                     Hello Awesome JV's
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-md-7 col-12">
                  <div class="f-md-20 f-18 lh150 w400 mt-md0 mt15 black-clr">
                     It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner Atul Pareek (Entrepreneur & Product Creator).
                     <br>
                     <br>We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates.
                     <br>
                  </div>
                  <div class="f-md-22 f-20 lh150 w700 mt-md20 mt20">
                     Also, here are some stats from our previous launches:
                  </div>
                  <div class="f-18 f-md-20 lh140 w500 mt20">
                     <ul class="pl0 no-orange-list">
                        <li>Over 100 Pick of The Day Awards</li>
                        <li>Over $1.5Mn In Affiliate Sales for Partners</li>
                        <li>Top 10 Affiliate & Seller (High-Performance Leader)</li>
                        <li>Always in Top-10 of JVZoo Top Sellers</li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-5 col-12">
                  <div class="row d-none d-md-block">
                     <div class="col-md-12 mt20 mt-md0 ">
                        <img src="assets/images/amit-sir-img.webp" class="img-fluid d-block mr-auto">
                     </div>
                     <div class="col-md-12 col-12 mt20 mt-md50">
                        <img src="assets/images/atul-sir-img.webp" class="img-fluid d-block ms-auto">
                     </div>
                  </div>
                  <div class="row d-block d-md-none">
                     <div class="col-md-12 mt20 mt-md0 ">
                        <img src="assets/images/amit-sir-img.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-12 col-12 mt20 mt-md50">
                        <img src="assets/images/atul-sir-img.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt-md25 mt20">
               <div class="col-12 col-md-12 f-18 f-md-20 lh140 w500">
                  With the combined experience of 20+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for your business website and online presence need under one dashboard.
                  <br><br>
                  "WebPrimo Plus", is a revolutionary technology to create websites for any business or niche, so you can use it for your own purpose or can even start your own profitable website creation agency from a single dashboard.
                  <br><br> Ultimately, your subscribers can <span class="w600">Tap into the $284 Billon Website Building Industry</span> and start a full-blown local website agency
                  <br><br> Check out the incredible features of the amazing creation that will blow away your mind. And we guarantee that this offer will convert like Hot Cakes starting from 8th December'22 at 10:00 AM EST! Get Ready!! -->
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-md-36 f-24 w500 white-clr lh140">
                     Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md35 mt20 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	               viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:60px" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#1F91C9;}
                        .st1{fill:#9C2341;}
                        .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                        .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                        .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                        .st5{fill:#FFFFFF;}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                     </style>
                     <g>
	                     <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
		                  c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"/>
	                     <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                        l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"/>
	                     <g>
		                     <g>
			                     <g>
                                 <defs>
                                    <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                       298.8,146.9 					"/>
                                 </defs>
                                 <clipPath id="SVGID_2_">
                                    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                    <stop  offset="0" style="stop-color:#12539E"/>
                                    <stop  offset="0.419" style="stop-color:#1F91C9"/>
                                    <stop  offset="1" style="stop-color:#1F91C9"/>
				                     </linearGradient>
				                     <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"/>
			                     </g>
		                     </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"/>
                                 </defs>
                                 <clipPath id="SVGID_5_">
                                    <use xlink:href="#SVGID_4_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                    <stop  offset="0" style="stop-color:#6D0C28"/>
                                    <stop  offset="0.514" style="stop-color:#9C2341"/>
                                    <stop  offset="1" style="stop-color:#9C2341"/>
				                     </linearGradient>
				                     <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"/>
			                     </g>
                           </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                    l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                    c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                    l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"/>
                                 </defs>
                                 <clipPath id="SVGID_8_">
                                    <use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                    <stop  offset="0" style="stop-color:#FD8B25"/>
                                    <stop  offset="0.581" style="stop-color:#FD8B25"/>
                                    <stop  offset="1" style="stop-color:#EF4900"/>
				                     </linearGradient>
				                     <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"/>
                              </g>
                           </g>
                        </g>
	                     <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
		                  475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"/>
	                     <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                        c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                        c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                        H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                        c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                        c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"/>
	                     <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                        c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                        c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"/>
	                     <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                        c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                        c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"/>
	                     <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"/>
	                     <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
		                  1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"/>
	                     <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                        c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                        c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                        C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                        c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                        c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                        C1425.9,210,1435.8,207.5,1444.3,202.4"/>
	                     <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                           <stop  offset="0" style="stop-color:#FD8B25"/>
                           <stop  offset="0.581" style="stop-color:#FD8B25"/>
                           <stop  offset="1" style="stop-color:#EF4900"/>
	                     </linearGradient>
	                     <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
		                  h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                        C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"/>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-30 f-24 mt-md35 mt20 w600 text-center white-clr lh140">
                  JVZoo’s First Hybrid WordPress Website Builder Framework To Create Elegant & Highly Professional Premium Websites For Any Business & Niche In Next 7 Minutes FLAT.
               </div>
               <div class="col-12 f-18 f-md-22 mt-md30 mt20 w400 text-center white-clr lh140">
                  Included with Agency License to Start Your Own Website & Marketing Agency and Get Profit from Local Buyers Like Doctors, Attorneys, Restaurants, Architect and More.
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex flex-wrap" style="align-items:end;">
                     <div class="col-12 col-xl-10 mx-auto px-xl15 px0">
                        <div class="">
                           <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15 img-animation">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- FEATURE LIST SECTION START -->
      <div class="feature-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12 f-md-32 f-20">
                  Create <br class="d-lg-block d-none"><span class="w600">Unlimited Websites and Ecom Stores.</span>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/fe1.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 order-md-2 f-md-32 f-20">
                  Ready to Use <br class="d-lg-block d-none"><span class="w600">Highly Customizable 200+ Templates with 2000+ Possible Combinations.</span>
               </div>
               <div class="col-md-6 col-12 order-md-1 mt-md0 mt20">
                  <img src="assets/images/fe2.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 f-md-32 f-20">
                  Create an <br class="d-lg-block d-none"><span class="w600">Elegant, Highly Professional,<br class="d-lg-block d-none"> and Lightning-Fast Website.</span>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/fe3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 order-md-2 f-md-32 f-20">
                  Built on Most Advance <br class="d-lg-block d-none"><span class="w600">Word Press Framework to Make it Super Easy with No Tech<br class="d-lg-block d-none"> Hassles.</span>
               </div>
               <div class="col-md-6 col-12 order-md-1 mt-md0 mt20">
                  <img src="assets/images/fe4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 f-md-32 f-20">
                  <span class="w600">Agency License Included</span> so use for your own purpose or Start a Website Building and Marketing Agency. 
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 order-md-2 f-md-32 f-20">
                  Seamless <br class="d-lg-block d-none"><span class="w600">Woo Commerce Integration to Accept Payments</span> for Online Selling and Ecom Stores.
               </div>
               <div class="col-md-6 col-12 order-md-1 mt-md0 mt20">
                  <img src="assets/images/fe6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 f-md-32 f-20">
                  <span class="w600">100% Mobile Responsive and Fully SEO Optimized Website</span> to get targeted traffic from all 360° Directions Seamlessly. 
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/fe7.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 order-md-2 f-md-32 f-20">
                  Done-For-You<br class="d-lg-block d-none"> <span class="w600">Graphics Templates, Social Media Graphics, Logo Kit, E-book Covers, Business Cards, Borchers, and Marketing Videos in 20+ Local Niches</span>
               </div>
               <div class="col-md-6 col-12 order-md-1 mt-md0 mt20">
                  <img src="assets/images/fe8.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md100 mt30">
               <div class="col-md-6 col-12 f-md-32 f-20">
                  Tons of Royalty<br class="d-lg-block d-none"> <span class="w600"> Free Stock Images and Videos with pexels integration.</span>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/fe9.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row m0 mt-md100 mt30">
               <div class="col-12 feature-icon-block">
                  <div class="row">
                     <div class="col-12">
                        <img src="assets/images/feature-list1.webp" class="img-fluid mx-auto d-lg-block d-none">
                        <img src="assets/images/feature-mobile-view.webp" class="img-fluid mx-auto d-lg-none d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="demo-bg">Watch The Demo</span>
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr mt-md20 mt15">
                     <span class="w500 "> Discover How Easy & Powerful It Is</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mx-auto mt-md40 mt20">
                  <!-- <img src="assets/images/video-thumbnail.webp" class="img-fluid d-block mx-auto"> -->
                  <!-- <div class="embed-responsive embed-responsive-16by9">
                     <iframe class="embed-responsive-item" src="https://kyza.dotcompal.com/video/embed/kxcu6m1l9g" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div> -->
                  <div class="responsive-video">
                     <iframe src="https://webprimo.dotcompal.com/video/embed/ydruowx7ha" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 text-center white-clr">
                     Here’s a List of 
                  </div>
                  <div class="f-md-50 f-24 w700 lh140 text-center white-clr niche-bg">
                     All Potential Niche
                  </div>
                  <div class="f-md-45 f-24 w600 text-center white-clr">
                     You Can Start Getting Clients & Collecting Checks
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt30">
               <div class="col-md-3 col-6">
                  <div class="">
                     <img src="assets/images/pn1.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Architect
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt0">
                  <div class="">
                     <img src="assets/images/pn2.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Financial Adviser
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn3.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Painters & Decorators
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn4.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Counsellors
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn5.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Divorce Lawyers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn6.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Carpet Cleaner
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn7.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Hotels
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn8.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Car Rental & Taxi
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn9.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Florist
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn10.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Attorney
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn11.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Lock Smith
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn12.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Real Estate
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn13.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Home Security
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn14.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Plumbers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn15.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Chiropractors
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn16.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Dentists
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn17.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Home Tutors
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn18.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Dermatologists
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn19.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Veterinarians
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn20.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Motor Garage
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn21.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Carpenters
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn22.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Computer Repair
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="prize-inner-sec">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-65 f-30 white-clr text-center w700 lh140">
                              $10,000 Launch Contest
                              <img src="assets/images/launch.webp" alt="Exciting Curve" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                        <div class="col-12 col-md-12 mx-auto mt-md90 mt30">
                           <div class="row gx-8">
                              <div class="col-md-6 col-12 relative">
                                 <img src="assets/images/contest.webp" alt="Contest" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-6 col-12 mt-md100 mt30 relative">
                                 <img src="assets/images/surprise-contest.webp" alt="Surprise Contest" class="img-fluid mx-auto d-block">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 black-clr mt20 mt-md0">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs.<br><br> So, if you have a top-notch product with top conversions and that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              Have any Query? Contact us Anytime
                           </div>
                        </div>
                        <!-- <div class="row"> -->
                           <div class="col-12 col-md-8 mx-auto">
                              <div class="row gx-md-5">
                                 <div class="col-md-6 col-12 text-center">
                                    <div class="contact-shape">
                                       <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Dr Amit Pareek
                                       </div>
                                       <div class="f-14 w400 lh140 text-center white-clr">
                                          (Techpreneur &amp; Marketer)
                                       </div>
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                          <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:amit.pareek77" class="link-text">
                                             <div class="col-12 ">
                                                <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Atul Pareek
                                       </div>
                                       <div class="f-14 w400 lh140 text-center white-clr">
                                          (Entrepreneur &amp; Product Creator)
                                       </div>
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                          <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                             <div class="col-12 ">
                                                <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        <!-- </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container px-md-15">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center black-clr">
                  Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 f-md-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below:
               </div>
               <div class="col-md-12 col-12 px-md-0 px30 mt15 mt-md50">
                  <ul class="b-tick1 pl0 m0 f-md-16 f-16 lh140 w400">
                     <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No
                        exceptions will be entertained. 
                     </li>
                     <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed
                        from our system with immediate effect. 
                     </li>
                     <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
                     <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	               viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:40px" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#1F91C9;}
                        .st1{fill:#9C2341;}
                        .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                        .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                        .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                        .st5{fill:#FFFFFF;}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                     </style>
                     <g>
	                     <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
		                  c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"/>
	                     <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                        l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"/>
	                     <g>
		                     <g>
			                     <g>
                                 <defs>
                                    <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                       298.8,146.9 					"/>
                                 </defs>
                                 <clipPath id="SVGID_2_">
                                    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                    <stop  offset="0" style="stop-color:#12539E"/>
                                    <stop  offset="0.419" style="stop-color:#1F91C9"/>
                                    <stop  offset="1" style="stop-color:#1F91C9"/>
				                     </linearGradient>
				                     <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"/>
			                     </g>
		                     </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"/>
                                 </defs>
                                 <clipPath id="SVGID_5_">
                                    <use xlink:href="#SVGID_4_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                    <stop  offset="0" style="stop-color:#6D0C28"/>
                                    <stop  offset="0.514" style="stop-color:#9C2341"/>
                                    <stop  offset="1" style="stop-color:#9C2341"/>
				                     </linearGradient>
				                     <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"/>
			                     </g>
                           </g>
	                     </g>
                        <g>
                           <g>
                              <g>
                                 <defs>
                                    <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                    l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                    c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                    l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"/>
                                 </defs>
                                 <clipPath id="SVGID_8_">
                                    <use xlink:href="#SVGID_7_"  style="overflow:visible;"/>
                                 </clipPath>
					                  <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                    <stop  offset="0" style="stop-color:#FD8B25"/>
                                    <stop  offset="0.581" style="stop-color:#FD8B25"/>
                                    <stop  offset="1" style="stop-color:#EF4900"/>
				                     </linearGradient>
				                     <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"/>
                              </g>
                           </g>
                        </g>
	                     <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
		                  475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"/>
	                     <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                        c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                        c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                        H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                        c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                        c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"/>
	                     <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                        c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                        c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"/>
	                     <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                        c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                        c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"/>
	                     <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"/>
	                     <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
		                  1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"/>
	                     <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                        c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                        c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                        C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                        c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                        c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                        C1425.9,210,1435.8,207.5,1444.3,202.4"/>
	                     <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                           <stop  offset="0" style="stop-color:#FD8B25"/>
                           <stop  offset="0.581" style="stop-color:#FD8B25"/>
                           <stop  offset="1" style="stop-color:#EF4900"/>
	                     </linearGradient>
	                     <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
		                  h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                        C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"/>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh140 white-clr text-xs-center">Copyright © WebPrimo Plus</div>
                  <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-2033102032').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-2033102032")) {
                     document.getElementById("af-form-2033102032").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-2033102032")) {
                     document.getElementById("af-body-2033102032").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-2033102032")) {
                     document.getElementById("af-header-2033102032").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-2033102032")) {
                     document.getElementById("af-footer-2033102032").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
         <a href="javascript:void(0);" data-dismiss="modal" class="close-link">&times;</a>
         <div class="modal-dialog help-video-modal">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body ">
                  <div class="col-12 responsive-video ">
                     <iframe src="https://kyza.dotcompal.com/video/embed/kxcu6m1l9g" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <script src="assets/js/particle.js"></script>
   </body>
</html>
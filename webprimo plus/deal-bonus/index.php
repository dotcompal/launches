<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="WebPrimo Plus+ Special Bonuses">
    <meta name="description" content="WebPrimo Plus+ Special Bonuses">
    <meta name="keywords" content="WebPrimo Plus+ Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WebPrimo Plus+ Special Bonuses">
    <meta property="og:description" content="WebPrimo Plus+ Special Bonuses">
    <meta property="og:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WebPrimo Plus+ Special Bonuses">
    <meta property="twitter:description" content="WebPrimo Plus+ Special Bonuses">
    <meta property="twitter:image" content="https://www.webprimo.co/special-bonus/thumbnail.png">

	<title>WebPrimo Plus+ Special Bonuses</title>
	<!-- Shortcut Icon  -->
	<link rel="shortcut icon" href="assets/images/favicon.png"/>
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style-bottom.css">
	<link rel="stylesheet" type="text/css" href="assets/css/m-style.css">
	<!-- Font Family CDN Load Links -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Javascript File Load --><script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>
	<script src="../common_assets/js/bootstrap.min.js"></script>
	<!-- Buy Button Lazy load Script -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MMDW8FD');</script>
	<!-- End Google Tag Manager -->



	<script>
	$(document).ready(function() {
	/* Every time the window is scrolled ... */
	$(window).scroll(function() {
		/* Check the location of each desired element */
		$('.hideme').each(function(i) {
			var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			var bottom_of_window = $(window).scrollTop() + $(window).height();
			/* If the object is completely visible in the window, fade it it */
			if ((bottom_of_window - bottom_of_object) > -200) {
				$(this).animate({
					'opacity': '1'
				}, 300);
			}
		});
	});
	});
	</script>
	<!-- Smooth Scrolling Script -->
	<script>
	$(document).ready(function() {
	// Add smooth scrolling to all links
	$("a").on('click', function(event) {
	
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();
	
			// Store hash
			var hash = this.hash;
	
			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function() {
	
				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
		} // End if
	});
	});
	</script>


</head>
<body>

	<!-- New Timer  Start-->
	<?php
		$date = 'December 14 2022 11:00 PM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
		?>
	<?php
		} else {
			echo "Times Up";
		}
		?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://jvz1.com/c/10103/390274/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMDW8FD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 white-clr">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus for &nbsp; 
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:40px" xml:space="preserve">
                        <style type="text/css">
                        .st0{fill:#1F91C9;}
                        .st1{fill:#9C2341;}
                        .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                        .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                        .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                        .st5{fill:#FFFFFF;}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                        </style>
                        <g>
                            <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
                            c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"></path>
                            <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                            l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"></path>
                            <g>
                                <g>
                                    <g>
                                        <defs>
                                            <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                            298.8,146.9"></polygon>
                                        </defs> 
                                        <clipPath id="SVGID_2_">
                                            <use xlink:href="#SVGID_1_" style="overflow:visible;"></use>
                                        </clipPath>
                                        <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                            <stop offset="0" style="stop-color:#12539E"></stop>
                                            <stop offset="0.419" style="stop-color:#1F91C9"></stop>
                                            <stop offset="1" style="stop-color:#1F91C9"></stop>
                                        </linearGradient>
                                        <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"></polygon>
                                    </g>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <g>
                                        <defs>
                                            <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"></polygon>
                                        </defs>
                                        <clipPath id="SVGID_5_">
                                            <use xlink:href="#SVGID_4_" style="overflow:visible;"></use>
                                        </clipPath>
                                        <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                            <stop offset="0" style="stop-color:#6D0C28"></stop>
                                            <stop offset="0.514" style="stop-color:#9C2341"></stop>
                                            <stop offset="1" style="stop-color:#9C2341"></stop>
                                        </linearGradient>
                                        <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"></polygon>
                                    </g>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <g>
                                        <defs>
                                            <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                            l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                            c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                            l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"></path>
                                        </defs>
                                        <clipPath id="SVGID_8_">
                                            <use xlink:href="#SVGID_7_" style="overflow:visible;"></use>
                                        </clipPath>
                                        <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                            <stop offset="0" style="stop-color:#FD8B25"></stop>
                                            <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                            <stop offset="1" style="stop-color:#EF4900"></stop>
                                        </linearGradient>
                                        <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"></rect>
                                    </g>
                                </g>
                            </g>
                            <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
                            475.6,157.4 475.6,137.4 394,137.4 394,75.3 	"></polygon>
                            <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                            c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                            c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                            H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                            c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                            c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"></path>
                            <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                            c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                            c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"></path>
                            <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                            c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                            c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"></path>
                            <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"></rect>
                            <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
                            1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"></polygon>
                            <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                            c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                            c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                            C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                            c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                            c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                            C1425.9,210,1435.8,207.5,1444.3,202.4"></path>
                            <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                                <stop offset="0" style="stop-color:#FD8B25"></stop>
                                <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                <stop offset="1" style="stop-color:#EF4900"></stop>
                            </linearGradient>
                            <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
                            h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                            C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"></path>
                        </g>
                    </svg>
                    	</div>
                    </div>

                    <div class="p0 mt20 mt-md40">
                        <div class="f-20 f-md-24 w600 text-center lh150 white-clr pre-headline">
							<span class="w600">Grab My 20 Exclusive Bonuses </span>Before the Deal Ends…
						</div>
						
                    </div>

                    <div class="mt20 mt-md40 p0">
						<div class="">
							<div class="f-md-45 f-28 w500 text-center white-clr lh150 line-center worksans title-bg1">
							This Game-Changing Website Builder Lets You <span class="gradient-text w700"> Create & Sell Beautiful Local WordPress Websites & Ecom Stores </span>
							 for Any Business in Just 7 Minutes for BIG Profits</div>
						</div>
                    </div>

                    <div class="text-center mt20 mt-sm30 p0">
                        <div class="col-12 mt20 mt-md50 f-md-24 f-20 w500 white-clr text-center">
							All From Single Dashboard Without Any Tech Skills and Zero Monthly Fees
						</div>
                    </div>

                </div>
            </div>

            <div class="row mt20 mt-md30">
                <div class="col-12 col-md-10 mx-auto">
					<!-- <img src="assets/images/product-box.png" class="img-fluid mx-auto d-block"> -->
                    <div class="video-frame">
						<div class="responsive-video">
						<iframe src="https://webprimo2.dotcompal.com/video/embed/4j1braymll" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

						</div>
                    </div> 
                </div>
            </div>
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20 white-clr">
					Use Coupon Code <span class="w800 orange-clr">“WEBEARLY”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn white-clr">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown-white counter-white">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
        </div>
		<!-- Icon Move Start -->
		<div class="star"><img src="assets/images/star-icon.png" alt="" class="img-fluid d-none d-xl-block  mx-auto star-move"></div> 
       <div class="website-move"><img src="assets/images/website-repair-icon.png" alt="" class="img-fluid d-none d-xl-block mx-auto web-move"></div> 
       <div class="website-repair"><img src="assets/images/website-icon.png" alt="" class="img-fluid d-none d-xl-block mx-auto web-repair"></div> 
      <div class="star-bottom"><img src="assets/images/star-icon.png" alt="" class="img-fluid d-none d-xl-block  mx-auto star-move"></div> 
      <!-- Icon Move End -->
    </div>
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container">
			
		</div>
	</div>
	<!-- CTA Button Section End   -->
	<div class="second-header-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center ">
				<div class="f-md-42 f-28 w700 text-center black-clr lh150">Create Stunning Websites &amp; Stores For <br class="d-none d-md-block"> Any Business In Just <span class="gradient-text">3 Simple Steps...</span></div>
                </div>
            </div>
            <div class="row mt50 mt-md65 justify-content-md-between mx-0">
                <div class="col-12 col-md-3 step-shape pb-md0 pb10">
                    
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                       Choose 
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step1.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Select the Niche-Specific Theme & Template from proven converting 200+ stunning done-for-you Business Templates. 
                    </div>
                </div>
                <div class="col-12 col-md-3 mt50 mt-md50 relative step-shape pb20">
                    <!-- Arrow icon Images -->
                    <div class="d-none d-md-block ">
                        <img src="assets/images/right-arrow-icon.png " class="img-fluid d-block right-arrow-icon1 ">
                    </div>
                    <!-- Arrow icon Images End -->
                    <!-- <div class="step-shape1 f-md-28 f-22 w500 white-clr lh150 text-center">
                        STEP 2
                    </div> -->
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                        Customize
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step2.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Upload logo, choose from 2000+ possible design combinations & customize a bit in just a few seconds. 
                    </div>
                </div>
                <div class="col-12 col-md-3 mt50 mt-md0 relative step-shape pb10">
                    <!-- Arrow icon Images -->
                    <div class="d-none d-md-block ">
                        <img src="assets/images/right-top.png " class="img-fluid d-block right-arrow-icon2 ">
                    </div>
                    <!-- Arrow icon Images End -->
                    <!-- <div class="step-shape1 f-md-28 f-22 w500 white-clr lh150 text-center">
                        STEP 3
                    </div> -->
                    <div class="mt15 f-md-28 f-24 w700 lh150 blue-clr text-center">
                       Publish, Sell,  <br class="d-none d-md-block"> and Profit  
                    </div>
                    <div class="step-image mt20">
                        <img src="assets/images/step3.png " class="img-fluid d-block mx-auto ">
                    </div>
                    <div class="f-18 f-md-18 w400 black-clr lh150 mt20 text-center ">
                       Now, just publish your website with a click to use it for your business or sell website development & maintenance services in 100+ niches. 
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="f-20 f-md-24 w600 text-center black-clr lh150 ">
                        And that’s it! 
                    </div>
                    <div class="f-18 f-md-20 w400 text-center black-clr lh150 mt5">
                      You’ll be able to pull in serious income from your own offers or by  <br class="d-none d-md-block">helping clients get in front of their target market in no time! 
                    </div>
                </div>
            </div>
			<div class="row mt-md100 mt20">
                <div class="col-12 f-24 f-md-36 w600 text-center black-clr lh150">
                    WebPrimo Plus Is Built for Everyone Across All Levels 
                </div>
            </div>
            <div class="row mt-md40 mt0">
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon1.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                       No Prior Experience Needed 
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon2.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                        Simple But Powerful Builder - <br class="d-none d-md-block"> No Coding At All 
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon3.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w400 black-clr text-center mt20">
                        Newbie-Friendly with No Learning Curve 
                    </div>
                </div>
            </div>

        </div>
    </div>
	  
	  <!--2. Second Section End -->
       <!-- <div class="built-section">
         <div class="container">
            <div class="row ">
               <div class="col-12 f-md-45 f-28 w700 lh150 black-clr text-center">
                  WEBPRIMO Is Built For Everyone Across All Levels
               </div>
            </div>
            <div class="row mt-md45 mt20">
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon1.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     No Prior Experience Needed. <br class="d-none d-md-block"> Zero Learning Curve
                  </div>
               </div>
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon2.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     Simple But Powerful Builder with No Coding or Tech Hassel
                  </div>
               </div>
               <div class="col-md-4 mt-md0 mt20">
                  <img src="./assets/images/icon3.png" alt="" class="img-fluid d-block mx-auto">
                  <div class="f-md-24 f-22 w400 black-clr text-center mt20">
                     Newbie-Friendly - Created Keeping Both First Timers’ and Part-Timers in Mind
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      
      <!-------Bussiness Section-------->
      <div class="business-new-section ">
         <div class="container ">
            <div class="row">
                <div class="col-12 f-md-45 f-28 w600 white-clr lh150 text-center">
                   No Matter What Type of Business 
                </div>
				 <div class="col-12 f-md-20 f-20 w400 white-clr lh150 text-center mt20">
                  WebPrimo Plus is a One Stop Solution to Create & Sell Stunning Websites, Stores, Blog, Local Site or A Service Marketplace In 100+ Niches Quick and Easy  
                </div>
            </div>
		
			  <div class="row mt20 mt-md30">
				<div id="carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
				  <div class="carousel-inner">
					<div class="carousel-item active">
					 <div class="slide-box">
						   <img src="./assets/images/slide1.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide2.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide3.png" alt="" class="mx-auto img-fluid d-block">
						</div>
					</div>
					<div class="carousel-item">
						<div class="slide-box">
						   <img src="./assets/images/slide4.png" alt="" class="mx-auto img-fluid d-block">
						    <img src="./assets/images/slide2.png" alt="" class="mx-auto img-fluid d-block">
						   <img src="./assets/images/slide3.png" alt="" class="mx-auto img-fluid d-block">
						</div>
					</div>
				  </div>
				</div>
			  </div>
      </div>
	  </div>

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">
					Use Coupon Code <span class="w800 red-clr">“WEBEARLY”</span> for an Additional <span class="w700 red-clr">$3 Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->

	<!-- Testimonial Section Start -->
      <div class="testimonial-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh150 black-clr text-center ">
                  See What Our Early Users & Backdoor <br class="d-none d-md-block"> Customers Are Saying About WebPrimo Plus!
               </div>
            </div>
            <!-- <div class="row mt25 mt-md75 align-items-center ">
               <div class="col-12 col-md-10 pr-md5 f-18 f-md-20 w300 lh150 ">
                  <div class=" testi-block1 ">
                     <span class="w500 ">WebPrimo Plus is something fresh and really exciting.</span> It's so easy to use, and the training included makes it even easier to take complete control of your business without falling prey to money sucking third
                     parties.
                     <br><br> I love the fact that it enables you to drive targeted traffic to your offers without actually being a technical nerd...all without any special skills. Absolutely brilliant guys. Keep up the great work…
                  </div>
               </div>
               <div class="col-md-2 pl-md20 mt20 mt-md0 ">
                  <img src="assets/images/david-kirby.png " class="img-fluid d-block mx-auto testi-radius " />
                  <div class="f-18 f-md-20 w600 lh150 black-clr text-center mt15 ">David Kirby</div>
               </div>
            </div>
            <div class="row mt30 mt-md70 align-items-center ">
               <div class="col-md-10 f-18 f-md-20 w300 lh150 order-md-2 ">
                  <div class=" testi-block1 ">
                     I don't write testimonials very often but couldn't help but write one after seeing the immense value you're going to get out of WebPrimo Plus. This saves a lot of time and money as it <span class="w500 ">includes everything that one needs to be a part of the ever-growing website building and online service selling industry</span>                        without doing anything myself! Something that you must give a serious look…
                  </div>
               </div>
               <div class="col-md-2 pr-md20 mt20 mt-md0 order-md-1 ">
                  <img src="assets/images/john-zakaria.png " class="img-fluid d-block mx-auto testi-radius " />
                  <div class="f-18 f-md-20 w600 lh150 black-clr text-center mt15 ">John Zakaria</div>
               </div>
            </div> -->
			<div class="row  mt25 mt-md75 align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/testi-img1.png" alt="" class="img-fluid d-block mx-auto">
                    <img src="./assets/images/testi-img2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/testi-img3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->		
	
	<!-- Over Section Start -->
      <div class="over-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="./assets/images/over-img.png" alt="" class="img-fluid d-block mx-auto">
                  <img src="./assets/images/siteefy.png" alt="" class="img-fluid d-block mx-auto mt-md50 mt20">
               </div>
               <div class="col-12 mt-md50 mt20 f-md-26 f-22 w500 white-clr lh150 text-center">
                    Hopefully you also are sick and tired of being told that every business needs to switch to digital to survive this never-ending pandemic! And that opens a door to 
                </div>
                <div class="col-12 mt-md30 mt20 f-md-54 f-28 w700 white-clr lh150 text-center">
                    <div class="over-shape">
                        BIG $284 Billion Opportunity.
                    </div>
                </div>
            </div>
         </div>
      </div>
      <!-- Over Section End -->
	  
	  <!-- Research Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh150 black-clr text-center">
                     I am sure in this Digital Era, having a website for any business is not an option anymore…
                  </div>
                  <div class="f-28 f-md-45 w700 orange-clr1  text-center my10">
                     It’s a Compulsion Today!
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr text-center">
                     Because no business owner would like to lose those visitors & big sales from all the digital sources.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 f-24 f-md-34 w600 lh150 black-clr text-center">
                  As When You Have an SEO & Mobile Optimized Website Then You
                  Seamlessly Get Customers from All 360-Degree Directions.
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/f-image.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-24 f-md-34 w600 lh150 black-clr text-center mt30 mt-md50">
                  And You Drive Non-Stop Traffic, Leads & Profits <br class="d-none d-md-block">
                  24 by 7 - 100% Hands-Free
               </div>
               <div class="col-12 mx-auto col-md-10 mt20 mt-md50 ">
                  <img src="assets/images/h-image.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh150 black-clr text-center mt20 mt-md50"><span class="w600">So, when it comes to doing a Business, Websites are the Present and The Future! </span> <br class="d-none d-md-block">In Easy Words, Every Business Need a Website Today!
                </div>
            </div>
         </div>
      </div>
      <!-- Research Section End -->
	  <!-- CTA Button Section Start -->
	<div class="cta-btn-section">
		<div class="container mb-md60 mb30">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt-md60">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20">
					Use Coupon Code <span class="w800 red-clr">“WEBEARLY”</span> for an Additional <span class="w700 red-clr">$3 Discount</span> on Agency Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->
	  <!--Proudly Introducing Start -->
      <div class="next-gen-sec" id="product">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-md-42 f-28 w700 white-clr lh150 text-uppercase">
                     Introducing...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 ">
                  <img src="assets/images/intro-logo.png " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 mt-md45 mt30 f-24 f-md-30 w600 lh150 black-clr text-center ">
                  World’s No. 1 Pro Website Builder That Let You Create and Sell Websites with Zero Hassles!
               </div>
               <div class="col-md-12 col-12 mt-md70 mt20 z-index10">
                  <img src="assets/images/next-gen-img.webp " class="img-fluid d-block mx-auto " />
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-6 col-12 mt-md0 mt20 px-md15 ">
                  <div class="row m0 p-feature py20">
                     <div class="col-md-2 p0 img-bot">
                        <img src="assets/images/ng1.png " class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-10 mt-md0 mt10">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">Create Any Type Of Site</div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">Create Stunning Local Sites, Marketplace, E-Com Stores, Affiliate Niche Sites or Blogs Easily.</div>
                     </div>
                  </div>
                  <div class="row m0 mt-md25 mt20 p-feature py10">
                     <div class="col-md-2 p0 img-bot2">
                        <img src="assets/images/ng2.png " class="img-fluid d-block mx-auto ">
                     </div>
                     <div class="col-md-10 col-12 pl-md10 mt-md0 mt10">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">Super-Customizable & Easy To Use</div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">World’s No. 1 WordPress Framework With 30+ Stunning Themes And 200+ DFY Templates To Create Site In Any Niche
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 px-md15 ">
                  <div class="row m0 p-feature py20">
                        <div class="col-md-2 p0 img-bot">
                            <img src="assets/images/ng4.png " class="img-fluid d-block mx-auto ">
                        </div>
                        <div class="col-md-10 col-12 pl-md10 mt-md0 mt10 ">
                            <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
                                No Worries of Paying Monthly
                            </div>
                            <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">
                                During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                            </div>
                        </div>
                    </div>
                  <div class="row m0 mt-md25 mt20 p-feature py20">
                     <div class="col-md-2 p0 img-bot">
                        <img src="assets/images/ng3.png " class="img-fluid d-block mx-auto ">
                     </div>
                     <div class="col-md-10 col-12 pl-md0 mt-md0 mt10 ">
                        <div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
                           No Limits - Use for Yourself Or Clients
                        </div>
                        <div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">Sell Your Own Products, Services or Affiliate Offers or Charge Your Clients for Elegant Websites.</div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 mx-auto">
                  <div class="row m0 p-feature py20 mt-md25 mt20">
					<div class="col-md-2 p0 img-bot">
						<img src="assets/images/ng5.png " class="img-fluid d-block mx-auto ">
					</div>
					<div class="col-md-10 col-12 pl-md10 mt-md0 mt10 ">
						<div class="f-md-23 f-22 w600 white-clr text-md-left text-center lh150 ">
							50+ More Features
						</div>
						<div class="f-18 f-md-18 w400 white-clr lh160 text-md-left text-center mt8 ">
							We’ve Left No Stone Unturned to Give You an Unmatched Experience
						</div>
					</div>
				</div>
               </div>
               <div class="col-12 f-20 f-md-22 w400 lh150 mt20 mt-md70 text-center black-clr ">
                  It has never been easier to create professional, SEO-optimized, and all-device friendly business websites that drive customers 360 degree but with WebPrimo Plus, it is EASY & FAST.<br><br> Replace your old school website builder with an
                  amazing WordPress Framework that helps ANYONE create stunning, lightning fast and clean websites.
               </div>
            </div>
         </div>
      </div>
      <!--Proudly Introducing End -->
	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700">When You Purchase WebPrimo Plus, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.png" class="img-fluid mx-auto d-block">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">Web Traffic Excellence</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>You can have the best product or the best service in the world but if you have no traffic – it’s all completely worthless.</li>
								<li>This 5 - part video course will help new and experienced marketers generate huge amount of traffic from five different sources.</li>
								<li>Topics covered: Email Traffic, Facebook Traffic, Forum Traffic, Pinterest Traffic, Youtube Traffic. Includes ready sales materials!</li>						
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
					   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus2.png" class="img-fluid mx-auto d-block">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color mt20 mt-md0">
							  X-Treme List Build Plugin
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
							  <b>Start creating engagement with your new leads by making your landing page memorable and enjoyable!</b><br><br>
								<li>Edit every detail on the fly with the simple options panel for each page</li>
								<li>Customize all of the content areas that are designed to be readable</li>
								<li>Choose from a variety of colors for the 'call to action' buttons on your page</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  WP Youtube Leads Plugin
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this plugin you can enhance the user engagement of your Youtube videos and increase your mailing list.</li>
								<li>Integrate any YouTube video and start converting right away. Use the time-stamps with the video to maximize interest and action.</li>
								<li>Create custom headlines and fill out the content the way you want, Customize the color and design to perfect the pesentation.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus4.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					 	 	<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Web Amcom Pro
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche.</b></li>
								<li>Ads are updated automatically every day, so your website always shows the very latest ads for your niche. </li>
								<li>Allthe ads include your Amazon affiliate ID, so you earn commissions on any resulting sales - giving your website an instant profit boost. The software can be used to add Amazon ads to any website in minutes.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Web Baycom Pro
					  		</div>
							<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche.</li>
								<li>These ads update automatically every hour, so your website always shows the very latest auctions for your niche. </li>
								<li>The software uses the eBay auction 'RSS feeds' to get the latest auctions. These provide a self-updating selection of the latest auctions matching any keyword.</li>
							</ul>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section-bg">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"WEBEARLY"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Agency Licence</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 mx-auto col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown-white counter-white">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd white-clr">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd white-clr">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd white-clr">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd white-clr">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus6.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Affiliate List Pro
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<b>With this software you can easily build your own list through your affiliate links - all on autopilot. </b><br><br>
								<li>Adding All Sorts Of Popups To Other People's Sales Pages </li>
								<li>Creating Your Affiliate Link Popups</li>
								<li>Boosting Your Response By Personalizing Your Web Pages</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus7.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  WP Survey Creator
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>WP Survey Creator is a WordPress plugin that allows you to incorporate a survey feature into your WordPress-powered website.</li>			
								<li>With this plugin you can create surveys with different types of questions and control how it appears on the page.</li>		
								<li>What’s more, you’ll have the ability to gather important results and statistics of the answers supplied by the respondents.</li>	
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus8.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Affiliate PDF Brander Software
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Here's How You Can Promote All Your Products And Services On Autopilot Using The Power Of Viral PDF Reports!</b></li>
								<li>If you sell digital products online likely an Ebook in PDF Form, chances are you may offer an affiliate program to make more sales lessen the marketing expenses you may spent. </li>
								<li>So to maximize your affiliate marketing program to the next level, giving the marketing materials to your affiliate marketing partners is a huge advantage to lessen the effort of partners and immediately make more sales along the way.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
					</div>
				</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
						<div class="col-md-5 col-12">
							<img src="assets/images/bonus9.png" class="img-fluid mx-auto d-block ">
						</div>
						<div class="col-md-7 col-12 mt20 mt-md0">
							<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							The Internet Marketer's Toolkit
							</div>
							<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Internet marketing means getting word out on the web and it means creating brilliant content. </li>
								<li>The only problem is that very few people have any idea what makes the web tick. Even if you know the basics of internet marketing, there’s a good chance that you don’t have all of the advanced skills you need to really make any project into a success.</li>
								<li>Learn the basics of internet marketing 101 even if you have no prior experience</li>
							</ul>
						</div>
					</div>
				</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus10.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Power Tools Video Site Builder
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>With this software you can instantly create your own complete moneymaking video site featuring Adsense and Amazon ads, unique web pages and SEO Solutions!</li>
								<li>To create your complete video site, you need to activate the Site Builder Tool, enter a few details into the simple form and click a button. A complete website is instantly built, ready to upload. </li>
								<li>120 videos sourced from YouTube. When you use the software, it fetches the latest selection of most popular videos for this particular niche. So when you build your site, you can be sure it will be fully up to date with the very latest videos.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400 white-clr">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center  lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"WEBEARLY"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Agency Licence</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 mx-auto col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown-white counter-white">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Goal Setting Video Site Builder Software
					  		</div>
					 		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Instantly Create Your Own Complete Moneymaking Video Site Featuring Adsense and Amazon Ads, Unique Web Pages, SEO Solutions and Much More ...Built Automatically in 2 Minutes Flat!</b></li>
								<li>You can't deny the fact that making money online is a really good business model because you may have more time for yourself and to your family as you will be mostly working at the comfort of your own home.</li>
								<li>The thing is that, there are so many online business model that you can try but one thing that you can also try and work on is having a video-based website that you can monetize.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus12.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Backlinks Warrior Software
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Discover An Easy To Use Software Tool That Finds UNLIMITED Relevant Backlinks...With A Few Clicks Of Your Mouse!</b></li>
								<li>All website owners who have the intent to make money online want traffic to their blog, landing page or website offers. </li>
								<li>What if you could get a highly targeted traffic and leads to your business without spending a penny in advertising your business online? Sounds cool right?</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus13.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Backlinks Analyzer Software
					  		</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Discover A Powerful Software That You Can Use Right Now To Instantly Analyze The Quality Of All Your Backlinks... With The Click Of A Mouse!</b></li>
								<li>Backlinks Analyzer is a PC application that will ensure that your backlinks are active, stay active, and that backlink companies deliver on their promises.</li>
								<li>Just because buying backlinks doesn't break the bank it doesn't mean you should be ripped off by paying good money for links that only remain active for a matter of days.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus14.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  RPI Check Software
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li>Ranking Videos In YouTube And Google Has Evolved But Most Video Marketers Have NOT Evolved With It! In Fact, Most Marketers Are Doing It All Wrong!</li>
								<li>Video marketing has been proven to be one of the best way to generate money on the internet. As for many bloggers and internet marketers, video marketing is also one of the best channel to attract traffic to their websites.</li>
								<li>The thing is that the internet evolved so fast as well as the technology and the principles. And if you don't go down with the flow of the latest technologies, surely we will get left behind.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->

	

	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus15.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  WordPress Ad Creator
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Instantly Create an Ad Using WordPress Ad Creator!</b></li>
								<li>Traffic is very important to ones blog or website. And because of that website owners and online entrepreneurs do their part to find those leads or traffic online whether via SEO, Social Media Marketing or Pay Per Click Advertisement.</li>
								<li>If you want to create an ad inside your WordPress dashboard, using this amazing plugin is a huge help to you.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black lh120 w400 white-clr">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"WEBEARLY"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Agency Licence
</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-md-10 col-12 mx-auto mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-md-8 mx-auto col-12 text-center">
					<div class="countdown-white counter-white">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="white-clr f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="white-clr f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus16.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Website Manager Software
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Pretty Much Anyone Doing Anything Online Who Owns Websites or Blogs Can Use This One. People Need Something to Help Themselves Stay Organized!</b></li>
								<li>If you are a blogger, online business owner or SEO practicing building private blog network, managing many blogs is really a pain in the ass.</li>				
								<li>That's why most PBN owners outsource this issue to a Virtual Assistant. But hiring a VA can also be expensive and not reliable sometimes if you've picked the wrong one.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	  	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus17.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Affiliate Marketing Manager Software
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Manage Your Affiliates with Ease Using this Amazing Software!</b></li>
								<li>If you are selling either physical or digital products online, making lots of sales is your priority concern. That's why you are looking for partners or affiliates to join you and give them the authority to sell your products for a percentage of commission.</li>
								<li>But the problem now is that managing your JV's or affiliate can also be time-consuming. Especially in the process of affiliate payout and other relevant procedures.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus18.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Software Profit Mastery
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Make Huge Amount of Money Online By Selling Your Own Software Products!</b></li>
								<li>If you have been in the internet marketing industry for awhile, you may already noticed that a lot of successful online entrepreneurs who are making millions of dollars online are those offering software products.</li>
								<li>You maybe hesitant because you don't know a thing about software and you even don't have the idea of what sell. The good news is that inside this product is a bundle of video tutorial series that will help you get started in profiting software as soon as possible.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				  		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus19.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					 	 	<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  WP Video Page Creator
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>At Last, With Only ONE Click Of Your Mouse And Not Too Much Effort You Can Create Profit-inducing Video Pages Just Like These! </b></li>
								<li>How Would You Like To Look Like An Authority In Front Of Your Viewers, Drive Massive Loads Of Traffic And Rake In Cash With Just A Few Clicks Of Your Mouse?</li>
								<li>WP VIDEO PAGE CREATOR is a magnificent plugin that will allow you to quickly and easily create'video profit pages' in less than a minute.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus20.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-36 w500 lh150 bonus-title-color">
							  Auto Support Bot
							</div>
					  		<ul class="bonus-list f-20 f-md-22 w300 lh150 mt20 mt-md30 p0">
								<li><b>Here's How You Can Use The Same Profit-Boosting Strategy As Many Of The Big Companies, By Offering Your Visitors 24/7 Live Chat Support on Your Websites, Even While You're Sleeping</b> </li>
								<li>Most Software iPacks activate your web browser to show a popup when you use the software. The popups help to fund the development cost of the individual Software iPacks, which means that you can usually buy Software iPacks at lower cost than normal software.</li>
								<li>Only a single popup will appear each time you use the software. The web addresses shown are embedded in the .sip file - so the creators of the Software iPack Player have no control of the websites that are displayed in the popups.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 orange-clr">$3275!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh140 w400">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		 	<div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-md-22 f-18 text-center mt3 black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 red-clr">TAKE ACTION NOW!</div>
					<div class="f-md-24 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Coupon Code <span class="w800 red-clr">"WEBEARLY"</span> for an Additional <span class="w700 red-clr">$3 Discount</span> on Agency Licence</div>
			 	</div>
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt30">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn ">
						<span class="text-center">Grab WebPrimo Plus + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center white-clr">My Exclusive Bonuses Are Expiring in...</h3>
				</div>
				<!-- Timer -->
				<div class="col-md-8 col-md-10 mx-auto col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
		
	<div class="footer-section">
        <div class="container">
           <div class="row">
              <div class="col-12 text-center">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1649.6 243.4" style="enable-background:new 0 0 1649.6 243.4; max-height:40px" xml:space="preserve">
                    <style type="text/css">
                       .st0{fill:#1F91C9;}
                       .st1{fill:#9C2341;}
                       .st2{clip-path:url(#SVGID_2_);fill:url(#SVGID_3_);}
                       .st3{clip-path:url(#SVGID_5_);fill:url(#SVGID_6_);}
                       .st4{clip-path:url(#SVGID_8_);fill:url(#SVGID_9_);}
                       .st5{fill:#FFFFFF;}
                       .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_10_);}
                    </style>
                    <g>
                        <path class="st0" d="M348.7,60.5l-49.9,86.4l-28.7,49.6l-3.6,6.3l-7.3,12.6l-2.3,3.9l-10.3,17.8l-0.1,0.1
                         c-4.9,8.3-16.9,8.3-21.7-0.1l-12.6-21.7l-10.9-18.9l22.1-38.2l1.4-2.4l10.9-18.8l16-27.7l15.7-27.2C284,53.8,320.3,44.1,348.7,60.5"></path>
                        <path class="st1" d="M147.2,196.6l-23.4,40.6c0,0,0,0,0,0c-4.8,8.3-16.9,8.3-21.7,0c0,0,0,0,0,0l-17.1-29.6l-2.7-4.7l-3.6-6.3
                       l-39.4-68.3L0,60.5c9.4-5.4,19.6-8,29.7-8c20.5,0,40.5,10.6,51.5,29.7l11.5,19.9l20.2,35l6.4,11.1L147.2,196.6z"></path>
                        <g>
                            <g>
                                <g>
                                    <defs>
                                        <polygon id="SVGID_1_" points="251.8,109.4 235.8,137.2 224.9,156 223.6,158.4 257,219.3 259.2,215.4 266.5,202.8 270.1,196.6 
                                        298.8,146.9"></polygon>
                                    </defs>
                                    <clipPath id="SVGID_2_">
                                        <use xlink:href="#SVGID_1_" style="overflow:visible;"></use>
                                    </clipPath>
                                    <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="896.3661" y1="626.5156" x2="897.3796" y2="626.5156" gradientTransform="matrix(43.2807 -61.2826 -61.2826 -43.2807 -139.39 82211.6719)">
                                        <stop offset="0" style="stop-color:#12539E"></stop>
                                        <stop offset="0.419" style="stop-color:#1F91C9"></stop>
                                        <stop offset="1" style="stop-color:#1F91C9"></stop>
                                    </linearGradient>
                                    <polygon class="st2" points="171.8,182.8 248.6,74 350.6,146 273.8,254.8"></polygon>
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <g>
                                    <defs>
                                        <polygon id="SVGID_4_" points="39.2,128.3 78.6,196.6 82.2,202.8 84.9,207.5 119.3,148.3 112.9,137.2 92.7,102.2 					"></polygon>
                                    </defs>
                                    <clipPath id="SVGID_5_">
                                        <use xlink:href="#SVGID_4_" style="overflow:visible;"></use>
                                    </clipPath>
                                    <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="911.7501" y1="634.5264" x2="912.7634" y2="634.5264" gradientTransform="matrix(-38.113 -69.3043 -69.3043 38.113 78803.4766 39158.0664)">
                                        <stop offset="0" style="stop-color:#6D0C28"></stop>
                                        <stop offset="0.514" style="stop-color:#9C2341"></stop>
                                        <stop offset="1" style="stop-color:#9C2341"></stop>
                                    </linearGradient>
                                    <polygon class="st3" points="57.8,241.4 -5.4,126.6 100.7,68.3 163.9,183.1"></polygon>
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <g>
                                    <defs>
                                        <path id="SVGID_7_" d="M163.5,49.7l-6.9,12l-5.6,9.7l-7.3,12.6l-3.6,6.3l-27.1,47l-30.7,53.1c-2.3,3.9-2.3,8.7,0,12.5
                                        l19.8,34.3c0,0,0,0,0,0c4.8,8.3,16.9,8.3,21.7,0c0,0,0,0,0,0l23.4-40.6l27.1-47l27.1,47l10.9,18.9l12.6,21.7
                                        c4.8,8.3,16.9,8.4,21.7,0.1l0.1-0.1l12.5-21.7l7.3-12.6c2.3-3.9,2.3-8.7,0-12.5L246.7,156l-10.9-18.9l-27.1-47l-3.6-6.2
                                        l-7.3-12.7l-5.6-9.7l-6.9-12l0,0c-2.4-4.2-6.6-6.3-10.9-6.3C170.1,43.4,165.9,45.5,163.5,49.7"></path>
                                    </defs>
                                    <clipPath id="SVGID_8_">
                                        <use xlink:href="#SVGID_7_" style="overflow:visible;"></use>
                                    </clipPath>
                                    <linearGradient id="SVGID_9_" gradientUnits="userSpaceOnUse" x1="913.7841" y1="612.9113" x2="914.7972" y2="612.9113" gradientTransform="matrix(0 197.4266 197.4266 0 -120830.6328 -180361.9063)">
                                        <stop offset="0" style="stop-color:#FD8B25"></stop>
                                        <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                                        <stop offset="1" style="stop-color:#EF4900"></stop>
                                    </linearGradient>
                                    <rect x="80" y="43.4" class="st4" width="188.8" height="202.1"></rect>
                                </g>
                            </g>
                        </g>
                        <polygon class="st5" points="483.6,75.3 483.6,55.2 369.7,55.2 369.7,241.5 483.6,241.5 483.6,221.5 394,221.5 394,157.4 
                        475.6,157.4 475.6,137.4 394,137.4 394,75.3"></polygon>
                        <path class="st5" d="M645.6,170.5c-3.3-6.6-7.8-12.1-13.3-16.6c-5.6-4.4-11.8-7.2-18.6-8.3c9.8-3.2,17.7-8.4,23.6-15.6
                        c6-7.2,9-16.3,9-27.3c0-8.9-2.2-16.9-6.7-24c-4.5-7.1-11.1-12.8-20-17c-8.9-4.2-19.7-6.3-32.3-6.3h-66.8v186h69.5
                        c12.5,0,23.3-2.2,32.5-6.5c9.2-4.4,16.2-10.3,21-17.9c4.8-7.6,7.2-16.1,7.2-25.5C650.5,184.1,648.9,177.1,645.6,170.5 M572.3,155.9
                        H587c6.1,0,11.6,0.8,16.5,2.4c4.6,1.5,8.7,3.7,12.1,6.6c7.1,6,10.7,14.1,10.7,24.4c0,10.1-3.4,18.1-10.2,23.8
                        c-6.8,5.7-16.2,8.5-28.3,8.5h-43v-65.7v-13.1V75.5h40.3c11.8,0,20.8,2.7,27,8s9.3,12.7,9.3,22.2s-3.1,16.8-9.3,22.2
                        c-2.5,2.1-5.4,3.8-8.7,5.1c-5,1.9-10.9,2.9-17.8,2.9h-13.4L572.3,155.9L572.3,155.9z"></path>
                        <path class="st5" d="M809,140.9c-4.7,8.7-12.1,15.8-22.2,21.1s-22.8,8-38.1,8h-31.3v71.5h-37.4V55.2h68.7c14.4,0,26.7,2.5,36.9,7.5
                        c10.2,5,17.8,11.8,22.8,20.6c5.1,8.7,7.6,18.6,7.6,29.6C816.1,122.8,813.7,132.2,809,140.9 M770.1,132.8c5-4.7,7.5-11.3,7.5-19.9
                        c0-18.2-10.2-27.2-30.5-27.2h-29.6v54.2h29.7C757.5,139.8,765.1,137.5,770.1,132.8"></path>
                        <path class="st5" d="M938.7,241.5l-41.1-72.6h-17.6v72.6h-37.4V55.2h70c14.4,0,26.7,2.5,36.9,7.6c10.2,5.1,17.8,11.9,22.8,20.6
                        c5.1,8.6,7.6,18.3,7.6,29c0,12.3-3.6,23.3-10.7,33.2c-7.1,9.9-17.7,16.7-31.8,20.4l44.6,75.5H938.7z M879.9,140.9h31.3
                        c10.2,0,17.7-2.4,22.7-7.3c5-4.9,7.5-11.7,7.5-20.4c0-8.5-2.5-15.2-7.5-19.9c-5-4.7-12.5-7.1-22.7-7.1h-31.3V140.9z"></path>
                        <rect x="1013.7" y="55.2" class="st5" width="37.4" height="186.3"></rect>
                        <polygon class="st5" points="1291.2,55.2 1291.2,241.5 1253.8,241.5 1253.8,120.3 1203.9,241.5 1175.6,241.5 1125.4,120.3 
                        1125.4,241.5 1088,241.5 1088,55.2 1130.4,55.2 1189.7,193.8 1249,55.2 	"></polygon>
                        <path class="st5" d="M1366.6,231.1c-14.6-8.2-26.2-19.5-34.7-34c-8.6-14.5-12.8-30.9-12.8-49.3c0-18.1,4.3-34.5,12.8-49
                        c8.6-14.5,20.1-25.8,34.7-34c14.6-8.2,30.6-12.3,48.1-12.3c17.6,0,33.7,4.1,48.2,12.3c14.5,8.2,26,19.5,34.5,34
                        c8.5,14.5,12.7,30.8,12.7,49c0,18.3-4.2,34.8-12.7,49.3s-20,25.9-34.6,34c-14.6,8.2-30.6,12.3-48.1,12.3
                        C1397.2,243.4,1381.2,239.3,1366.6,231.1 M1444.3,202.4c8.6-5.1,15.2-12.3,20-21.8c4.8-9.4,7.2-20.4,7.2-32.8
                        c0-12.4-2.4-23.3-7.2-32.7c-4.8-9.3-11.5-16.5-20-21.5c-8.6-5-18.4-7.5-29.7-7.5s-21.2,2.5-29.8,7.5c-8.6,5-15.4,12.1-20.2,21.5
                        c-4.8,9.3-7.2,20.2-7.2,32.7c0,12.5,2.4,23.4,7.2,32.8c4.8,9.4,11.5,16.7,20.2,21.8s18.6,7.6,29.8,7.6
                        C1425.9,210,1435.8,207.5,1444.3,202.4"></path>
                        <linearGradient id="SVGID_10_" gradientUnits="userSpaceOnUse" x1="1519.75" y1="180.94" x2="1649.63" y2="180.94" gradientTransform="matrix(1 0 0 -1 0 246)">
                            <stop offset="0" style="stop-color:#FD8B25"></stop>
                            <stop offset="0.581" style="stop-color:#FD8B25"></stop>
                            <stop offset="1" style="stop-color:#EF4900"></stop>
                        </linearGradient>
                        <path class="st6" d="M1519.8,73.8V56.2c0-4.5,3.6-8.1,8.3-8.1h39.7V8.3c0-4.6,3.6-8.3,8.1-8.3h17.5c4.6,0,8.3,3.7,8.3,8.3v39.8
                        h39.7c4.5,0,8.3,3.7,8.3,8.1v17.6c0,4.6-3.8,8.3-8.3,8.3h-39.7v39.8c0,4.5-3.6,8.3-8.3,8.3h-17.5c-4.5,0-8.1-3.8-8.1-8.3V82.1H1528
                        C1523.4,82.1,1519.8,78.4,1519.8,73.8L1519.8,73.8z"></path>
                    </g>
                </svg>
                <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-16 f-md-18 w300 lh140 white-clr text-xs-center">Copyright © WebPrimo Plus</div>
                    <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://webprimo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop px0">	
					<div class="col-12 px0">
						<img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 px0">
						<div class="f-20 f-md-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="hidden-xs"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<!-- End Google Tag Manager (noscript) -->
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
		if (f.fbq) return;
		n = f.fbq = function() {
			n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq) f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
		}(window,
		document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1777584378755780');
		fbq('track', 'PageView');
	</script>
  	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	<!-- Google Code for Remarketing Tag -->
	<!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  	<div style="display:none;">
	 	<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 748114601;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			var google_user_id = '<unique user id>';
			/* ]]> */
	 	</script>
	 	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 	<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
			</div>
	 	</noscript>
  	</div>
	<!-- Google Code for Remarketing Tag -->
	<!-- timer --->
  	<?php
	 	if ($now < $exp_date) {
	?>

  	<script type="text/javascript">
		// Count down milliseconds = server_end - server_now = client_end - client_now
		var server_end = <?php echo $exp_date; ?> * 1000;
		var server_now = <?php echo time(); ?> * 1000;
		var client_now = new Date().getTime();
		var end = server_end - server_now + client_now; // this is the real end time
		
		var noob = $('.countdown').length;
		
		var _second = 1000;
		var _minute = _second * 60;
		var _hour = _minute * 60;
		var _day = _hour * 24
		var timer;
		
		function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
			return;
		}
		
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);
		if (days < 10) {
			days = "0" + days;
		}
		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var i;
		var countdown = document.getElementsByClassName('countdown');
		for (i = 0; i < noob; i++) {
			countdown[i].innerHTML = '';
		
			if (days) {
				countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
			}
		
			countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
		}
		
		}
		timer = setInterval(showRemaining, 1000);
  	</script>
  	<?php
	 	} else {
	 	echo "Times Up";
	 	}
	?>
  <!--- timer end-->
  
  <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKWBVSR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
</body>
</html>

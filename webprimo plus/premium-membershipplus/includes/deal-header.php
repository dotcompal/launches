<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Oppyo Premium Membership</title>
<meta name="title" content="Oppyo Premium Membership">
<meta name="description" content="Get One-Time Access To ALL 20+ Premium Apps For A Low One Time Price">
<meta name="keywords" content="Oppyo">
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="English">
<meta name="revisit-after" content="1 days">
<meta name="author" content="Dr. Amit Pareek">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.Oppyo.com/customers-only-deal">
<meta property="og:title" content="Oppyo Premium Membership">
<meta property="og:description" content="Get One-Time Access To ALL 20+ Premium Apps For A Low One Time Price">
<meta property="og:image" content="https://cdn.oppyo.com/assets/images/whatnew1.jpg">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://www.Oppyo.com/customers-only-deal">
<meta property="twitter:title" content="Oppyo Premium Membership">
<meta property="twitter:description" content="GGet One-Time Access To ALL 20+ Premium Apps For A Low One Time Price">
<meta property="twitter:image" content="https://cdn.oppyo.com/assets/images/whatnew1.jpg">
<meta name="google" content="notranslate"/>

<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 4 Stylesheet ----->
<?php $basedir = "https://".$_SERVER['SERVER_NAME'] . '/premium-membershipplus/'; ?>

<!-- Favicon Icon -->
<link rel="icon" href="https://cdn.oppyo.com/apps/website/assets/images/favicon.png" type="image/png">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap.min.css">
<!-- Common Css Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/general.css">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/elements.css">
<!-- Bootstrap Select Dropdown Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap-select.css">
<!-- Oppyo Header Css Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/header.css">
<!-- Oppyo Layout Css Stylesheet ----->	
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/layout.css">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/affiliate-panel.css">
<!-- Animation Css Stylesheet ----->	


</head>
<body>



<!---------- Oppyo Login Header ---------->
<!---------- Oppyo Login Header ---------->
<div class="trans-header pricing-header">
	<header class="relative">
		<div class="saglus-header affiliate-panel-header align-self-center d-flex">
			<nav class="navbar navbar-expand w-100 px20 px-md30 saglus-nav">
			   	<a class="navbar-brand m0" href="https://www.Oppyo.com/">
				  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 214.51 43.96" style="enable-background:new 0 0 214.51 43.96; width:100%; max-height:35px;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FE6E09;}
	.st1{fill:#FFFFFF;}
</style>
<g>
	<g>
		<path d="M55.08,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
			C62.4,38.73,55.08,31.73,55.08,21.98z M81.02,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58s3.56,8.58,8.42,8.58
			C77.5,30.55,81.02,26.97,81.02,21.98z"></path>
		<path d="M120.05,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C115.02,6.03,120.05,10.87,120.05,17.55z
			 M111.02,17.57c0-2.27-1.52-3.87-3.84-3.87h-3.72v7.74h3.72C109.5,21.44,111.02,19.84,111.02,17.57z"></path>
		<path d="M149.44,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C144.4,6.03,149.44,10.87,149.44,17.55z
			 M140.41,17.57c0-2.27-1.53-3.87-3.84-3.87h-3.72v7.74h3.72C138.88,21.44,140.41,19.84,140.41,17.57z"></path>
		<path d="M169.23,25.45v12.47h-8.95v-12.5l-11.06-19.4h9.75l5.83,11.53l5.83-11.53h9.75L169.23,25.45z"></path>
		<path d="M179.42,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
			C186.74,38.73,179.42,31.73,179.42,21.98z M205.36,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58
			s3.56,8.58,8.42,8.58C201.84,30.55,205.36,26.97,205.36,21.98z"></path>
	</g>
	<g>
		<path class="st0" d="M43.96,39.27V2.45C43.96,1.1,42.86,0,41.5,0L4.69,0C2.5,0,1.41,2.64,2.95,4.19l11.38,11.38L2.81,27.09
			c-3.75,3.75-3.75,9.82,0,13.57l0.49,0.49c3.75,3.75,9.82,3.75,13.57,0l11.52-11.52L39.77,41C41.31,42.55,43.96,41.45,43.96,39.27z
			"></path>
		<circle class="st1" cx="22.2" cy="21.76" r="9.94"></circle>
	</g>
</g>
</svg>
			 	</a>
				<div class="seprate-line"></div>
		   		<!-- For Mobile -->				
				<!--- Mobile end -->
			    <div class="collapse navbar-collapse new-custom-megamenu" id="navbarSupportedContent">
					<ul class="navbar-nav w-100 justify-content-end">
						<li class="nav-item mr-auto d-none d-md-block">
							<a class="nav-link" href="#" style="pointer-events: none;">
								Customers Only - LIMITED TIME Offer              
							</a>				
						</li>
						<li class="nav-item">
							<a class="base-btn blue-btn" title="Upgrade Now" href="#buynow">
								Buy Now
							</a>
						</li>						
					</ul>
			   </div>
			</nav>
		<!----- Header Right Side Links---------->
		</div>
	</header>
</div>

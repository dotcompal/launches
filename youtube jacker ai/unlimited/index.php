<!Doctype html>
<html>
<head>
    <title>YouTubeJacker Ai Unlimited</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="YouTubeJacker Ai | Pro">
    <meta name="description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta name="keywords" content="YouTubeJacker Ai">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://youtubejackerai.com/pro/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="YouTubeJacker Ai | Pro">
    <meta property="og:description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta property="og:image" content="https://youtubejackerai.com/pro/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="YouTubeJacker Ai | Pro">
    <meta property="twitter:description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta property="twitter:image" content="https://youtubejackerai.com/pro/thumbnail.png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/general.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="../common_assets/js/jquery.min.js"></script>
    <script src="../common_assets/js/popper.min.js"></script>
    <script src="../common_assets/js/bootstrap.min.js"></script>
</head>
<body>
    <header class="d-flex justify-content-center align-items-center">
        <div>
            <img src="assets/images/error.webp" class="img-fluid">
        </div>
        <h1 class="text-white text-center WesFYW03Bold"> Warning: This offer will expire automatically once you leave the page</h1>
	</header> 
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 632.61 146.86" style="max-height:70px;"><defs><style>.cls-y1{fill:url(#linear-gradient);}.cls-y2{fill:#f9be32;}.cls-y3{fill:#111428;}.cls-y4{fill:#231f20;}.cls-y5,.cls-y6,.cls-y7{fill:none;stroke:#000;stroke-miterlimit:10;}.cls-y5{stroke-width:2.27px;}.cls-y6{stroke-width:2.51px;}.cls-y7{stroke-width:1.98px;}.cls-y8{fill:#ed1c24;}.cls-y9{fill:#fff;}</style><linearGradient id="linear-gradient" x1="4.85" y1="13.03" x2="213.19" y2="138.53" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ed2224"/><stop offset="1" stop-color="#cb2026"/></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-y1" d="M109.51,0s41.35.3,60.92,2.12,22.89,3.59,29.74,11,7.24,24.17,8.41,35.81a272.72,272.72,0,0,1,.89,33.66c-.3,8.22-1.37,25.44-2.55,32.39s-2.35,12.52-7.34,19.17-15.94,9.1-28.08,10.08-49.8,2.1-59.78,2.1-55.47-1-68.48-1.71S20.15,142.49,14.18,138,4.59,127.62,2.73,115.48,0,80.36,0,73.61.68,48.76,1.85,37.12,5.76,15.2,12.32,9.82s13.11-7,37.18-8.42S109.51,0,109.51,0Z"/><path class="cls-y2" d="M178.83,143.4c-10.24,1.85-42.49,3.07-74,3.07s-40.51,1.26-73.24-3.07c4.11-7.37,8.8-14.45,13-17.24A107.82,107.82,0,0,1,60,118.35c2.42-1,4.88-2.07,7.25-3-4.64-6-8.19-12.63-8.19-18.07C59,89.56,76,29.42,105.2,29.42c27.48,0,46.17,60.14,46.17,67.83,0,5.57-3.4,12.21-7.84,18.21,8.22,3.33,17.46,7.4,22.35,10.7C170,129,174.72,136,178.83,143.4Z"/><path class="cls-y3" d="M31.62,146.35l.21.51A3.06,3.06,0,0,1,31.62,146.35Z"/><path class="cls-y4" d="M75.31,124.36a80,80,0,0,1-15.34-6c2.42-1,4.88-2.07,7.25-3A83.63,83.63,0,0,0,75.31,124.36Z"/><path class="cls-y4" d="M150.44,118.35a78.79,78.79,0,0,1-14.32,5.72,79.66,79.66,0,0,0,7.41-8.6C145.79,116.38,148.14,117.35,150.44,118.35Z"/><path class="cls-y4" d="M146.1,92.14c0,11.59-23.53,37.92-40.9,37.92-15.39,0-40.9-26.33-40.9-37.92s18.31-21,40.9-21S146.1,80.54,146.1,92.14Z"/><path class="cls-y4" d="M78.16,72.9q27.42-11.22,54.85,0s-16.38-11.22-27.42-11.22S78.16,72.9,78.16,72.9Z"/><polyline class="cls-y5" points="207.8 38.86 178.91 38.86 167.65 33.71"/><path class="cls-y6" d="M161,32.75a3.53,3.53,0,1,1,3.53,3.53A3.52,3.52,0,0,1,161,32.75Z"/><polyline class="cls-y7" points="209.31 84.45 184.08 84.45 174.25 88.95"/><circle class="cls-y6" cx="171.23" cy="90.24" r="3.53"/><line class="cls-y6" x1="209.38" y1="60.57" x2="183.24" y2="60.57"/><path class="cls-y6" d="M176.19,60.67a3.53,3.53,0,1,1,3.53,3.53A3.53,3.53,0,0,1,176.19,60.67Z"/><polyline class="cls-y6" points="1.56 39.26 19.85 39.26 32.25 33.58"/><path class="cls-y6" d="M38.91,32.9a3.53,3.53,0,1,0-3.52,3.52A3.53,3.53,0,0,0,38.91,32.9Z"/><polyline class="cls-y6" points="0.32 84.02 13.19 84.02 25.59 89.7"/><circle class="cls-y6" cx="28.73" cy="90.38" r="3.53"/><line class="cls-y6" x1="0.33" y1="60.71" x2="16.71" y2="60.71"/><path class="cls-y6" d="M23.76,60.81a3.53,3.53,0,1,0-3.52,3.53A3.53,3.53,0,0,0,23.76,60.81Z"/><path class="cls-y8" d="M264.08,10.59h8.78L260,38.14V54.77h-8.13V38.14L239,10.59h8.71l8.13,18.7Z"/><path class="cls-y8" d="M272.29,36.93q0-8.85,4.21-13.67t11.84-4.81q7.71,0,11.89,4.81t4.17,13.67q0,8.77-4.21,13.63c-2.81,3.24-6.75,4.86-11.85,4.86s-9-1.63-11.81-4.89S272.29,42.74,272.29,36.93Zm7.92.43q0,10.85,8.13,10.85t8.14-10.85q0-11.7-8.14-11.71T280.21,37.36Z"/><path class="cls-y8" d="M310.68,19.09h7.85V39.93q0,8.2,6.64,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T310.68,39.93Z"/><path class="cls-y8" d="M363.85,17.73v37h-8.06v-37H344.37V10.59H376.2l-2,7.14Z"/><path class="cls-y8" d="M376.56,19.09h7.85V39.93q0,8.2,6.63,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T376.56,39.93Z"/><path class="cls-y8" d="M420.8,22.23a14.19,14.19,0,0,1,4.39-2.93,15,15,0,0,1,5.46-.85q6.93,0,10.71,4.74t3.78,13.38q0,9.07-4,14t-11.31,4.89A14.29,14.29,0,0,1,420,51.49v3.28H413.1V6h7.7Zm8.28,26q4.21,0,6.18-2.64c1.3-1.76,2-4.55,2-8.35s-.66-6.87-2-8.75-3.35-2.82-6.11-2.82q-8.34,0-8.35,11.42,0,5.72,2,8.43C424.13,47.3,426.23,48.21,429.08,48.21Z"/><path class="cls-y8" d="M479.54,40H458.2q1.07,8.28,9.06,8.28a12.22,12.22,0,0,0,7.93-3l4.06,5.42a18,18,0,0,1-12.63,4.72q-7.71,0-12.1-4.82t-4.38-13.24q0-8.64,4.21-13.77a13.87,13.87,0,0,1,11.34-5.14q8.07,0,11.71,6.28,2.42,4.14,2.42,11.2A37.26,37.26,0,0,1,479.54,40ZM458.2,33.58h13.92q0-3.93-1.65-5.86a6,6,0,0,0-5-2.14Q459.27,25.58,458.2,33.58Z"/><path class="cls-y9" d="M267.82,74.84v40q0,11.69-5.28,17.62t-15.73,5.84H240v-14h5.67q3.44,0,4.94-2.22t1.62-7.23V67.39Z"/><path class="cls-y9" d="M282.93,97.74l-4.67-11.12a40.41,40.41,0,0,1,20.57-5.33q12.1,0,18.06,6.67t5.95,20.34v29.56H309.72V134q-4.56,4.89-13,4.89-9.45,0-15-5.11t-5.56-13.67q0-8.34,5.5-13.34t14.73-5a19.45,19.45,0,0,1,12,3.67c-.38-3.93-1.39-6.73-3.06-8.4s-4.28-2.5-7.84-2.5A40,40,0,0,0,282.93,97.74Zm16.45,16.34q-8.78,0-8.78,5.89,0,3.23,2.06,4.61t6.83,1.39q4.56,0,6.62-1.45a5.17,5.17,0,0,0,2.05-4.55,5,5,0,0,0-2.05-4.5C304.74,114.54,302.49,114.08,299.38,114.08Z"/><path class="cls-y9" d="M377.3,88.84l-6.78,11q-7-4.89-12.89-4.89a10,10,0,0,0-8.4,4.05q-3.06,4.06-3.05,11.18t3.05,11.11q3.06,3.9,8.84,3.89a18.19,18.19,0,0,0,6.56-1.11,30.43,30.43,0,0,0,6.89-4.11l6.78,11a33.62,33.62,0,0,1-9.89,6.17,32.84,32.84,0,0,1-11.34,1.72q-12.22,0-19.17-7.55t-6.95-21Q331,97,338,89.12t19.06-7.83A30.58,30.58,0,0,1,367.68,83,34.42,34.42,0,0,1,377.3,88.84Z"/><path class="cls-y9" d="M400.53,102.41,414.2,82.29h17l-19.34,27.9L436,137.86H416.87l-16.34-19.11v19.11h-15v-76h15Z"/><path class="cls-y9" d="M481.34,116.3H449.88q2.12,9.23,11.78,9.22a16.12,16.12,0,0,0,6.12-1,28.5,28.5,0,0,0,6.33-4.06l7.56,10.12A31.32,31.32,0,0,1,472,137a30.81,30.81,0,0,1-11.34,1.89q-12.11,0-19.12-7.55t-7-20.57q0-13.33,6.73-21.4t17.84-8q13,0,19,10,3.9,6.45,3.89,17.34A65.64,65.64,0,0,1,481.34,116.3Zm-31.68-11.78h17.78a12,12,0,0,0-2.39-7.28A7.43,7.43,0,0,0,459,94.62Q451.77,94.62,449.66,104.52Z"/><path class="cls-y9" d="M505.34,82.29v5.78q4.89-5.89,13.34-6.34h3.22v15h-3.22q-6.55.23-9.17,3.28c-1.74,2-2.65,5.54-2.72,10.5v27.34h-15V82.29Z"/><path class="cls-y2" d="M566.14,124.75l-3.89,13.11H545.69l22.68-68.8H584.7l22.57,68.8H590.71l-3.89-13.11ZM570,111.41h12.9l-6.45-22.12Z"/><path class="cls-y2" d="M619.61,59.17a8.54,8.54,0,0,1,8.67,8.67,8.55,8.55,0,0,1-8.78,8.78,8.73,8.73,0,1,1,.11-17.45Zm13,79.14h-4.33q-8.56,0-12.4-4t-3.83-13.12V82.29h15v37.9a8.86,8.86,0,0,0,.51,3.11c.25.52.83.78,1.72.78h3.33Z"/></g></g></svg>
                </div>
                <div class="col-12 mt20 mt-md50 text-center ">
                    <div class="f-md-22 f-18 w600 lh130 post-heading white-clr">
                        <span>Congratulations! Your order is almost complete. But before you get started...</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 px-md30">
                    <div class="f-md-50 f-30 w500 text-center white-clr lh130">
                        Remove All Limitations To Go Unlimited & Get <span class="yellow-clr w700">5X More Traffic, Commissions & Profits Faster & Easier </span> With This Pro Upgrade
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 f-22 f-md-22 w400 text-center lh130 white-clr">
                    Unlock Pro Features - Create Unlimited Channels, Publish Unlimited Videos, Unlimited Custom Domains, Unlimited Storage & Bandwidth, Pro Level Video Frames, 50+ Proven Templates, Social Gates and 10+ Other Pro Features…
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/product-img.webp" class="img-fluid mx-auto d-block">
                   <!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
                     <iframe src="https://YouTubeJacker Ai.dotcompal.com/video/embed/jpjzgsnfod" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div> -->
                </div>
                <div class=" col-12 col-md-11 mx-auto mt30 mt-md50">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow">Get Instant Access to YouTubeJacker Ai Pro</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-28 f-22 lh130 w400 text-center white-clr">
                    This is An Exclusive Deal for <span class="w500">"YouTubeJacker Ai"</span> Users Only...
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center text-capitalize">
                    <div class="f-22 f-md-26 text-center w400 lh130">
                        You Are About To Witness The RAW Power Of YouTubeJacker Ai In A Way You Have Never Seen It.
                    </div>
                </div>
                <div class="col-12 mt20">
                    <div class="f-md-50 f-30 w500 lh130 text-capitalize text-center black-clr">
                        A Whopping <span class="w700"> 92% Of The YouTubeJacker Ai Members
                     Upgraded  To YouTubeJacker Ai Pro...</span> Here's Why
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start">
                                <img src="assets/images/fe1.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Break Free & Go Limitless- Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers.
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe2.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Create Unlimited Channels in ANY NICHE For Yourself and Clients
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe3.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe4.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe5.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Add Unlimited Custom Domains To Build Your Authority & Credibility
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe6.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Getting Unlimited Storage To Get Your Media Content Delivered Faster
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe7.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Getting Unlimited Bandwidth To Give Best User Experience
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe8.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Drive Unlimited Visitors And Leads For Your Offers
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe9.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Create Unlimited Ads & Get Maximum Affiliate Commissions
                                </div>
                            </div>
							 <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe10.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Get 5 Stunning And Ready-To-Use Fully Customizable Video Players
                                </div>
                            </div>

                            <!--14-->
                        </div>
                        <div class="col-md-6 col-12">

                           
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md0">
                                <img src="assets/images/fe11.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Customize Your Player With 8 Attractive And Eye-Catchy Frames
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe12.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe13.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe14.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Never Offered Before Video Personalization To Boost Conversions
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe15.png" class="img-fluid d-block" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Create A Personalized E-Mail Link of The Video
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe16.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe17.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                   Capture Leads Of Your Visitors When They Interact On Your Videos
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe18.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Maximize ROI From Your Leads With Webinar Integration
                                </div>
                            </div>
							<div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe19.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Unlimited Team Management To Let Your Team Handle All The Manual Work
                                </div>
                            </div>
							<div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe20.png" class="img-fluid d-block" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Get All These Benefits At An Unparalleled Price
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-11 mx-auto col-12 mt20 mt-md60">
                    <div class="f-22 f-md-36 text-center probtn"><a href="#buynow">Get Instant Access to YouTubeJacker Ai Pro</a></div>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->
    <div class="chackout">
        <div class= "container">
            <div class="row"> 
                <div class="col-12 f-35 f-md-46 w600 text-center white-clr ">
                Checkout Real Results That Our Customers Got By Upgrading To Pro Today…
                </div> 
            </div>
            <div class="row mt20 mt-md40">
                <div class="col-12">
                    <img src="assets/images/income.webp" class="img-fluid" alt="">
                </div> 
            </div>         
        </div>               
    </div>                   
    <!--3. Third Section Start -->
    <div class="third-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center black-clr">
                        This Is Your Chance To <span class="w700">Get Unfair Advantage </span> <br class="d-none d-md-block"> Over Your Competition
                    </div>
                    <div class="f-20 f-md-22 w400 mt20 mt-md35 lh140 text-center">
                        <!-- If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results… <span class="w600">what more this pro-upgrade can bring on the table with the power to  Create Unlimited Business, Create Unlimited Video Channels, Add Unlimited Videos In Channels, Get Unlimited Bandwidth and Other Pro Features</span> -->
						If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results… what more this pro-upgrade can bring on the table with the power to Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers and Other Pro Features.
                        <br>
                        <br> I know you're probably very eager to get to your members area and <span class="w600">start using YouTubeJacker Ai and start getting more sales and commissions from your marketing campaigns.</span> However, if you can give me a few minutes
                        I'll show you how to take this system beyond basic features using it to skyrocket your profit.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-50 w700 lh140 white-clr caveat proudly-sec">
                        Presenting…
                     </div>
                    
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 632.61 146.86" style="max-height:70px;">
                  <defs>
                     <style>
                        .cls-j1{fill:url(#linear-gradient);}
                        .cls-j2{fill:#f9be32;}
                        .cls-j3{fill:#111428;}
                        .cls-j4{fill:#231f20;}
                        .cls-j5,.cls-j6,.cls-j7{fill:none;stroke:#000;stroke-miterlimit:10;}
                        .cls-j5{stroke-width:2.27px;}
                        .cls-j6{stroke-width:2.51px;}
                        .cls-j7{stroke-width:1.98px;}
                        .cls-j8{fill:#ed1c24;}
                        .cls-j9{fill:#fff;}
                     </style>
                     <linearGradient id="linear-gradient" x1="4.85" y1="13.03" x2="213.19" y2="138.53" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#ed2224"/>
                        <stop offset="1" stop-color="#cb2026"/>
                     </linearGradient>
                  </defs>
                  <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-j1" d="M109.51,0s41.35.3,60.92,2.12,22.89,3.59,29.74,11,7.24,24.17,8.41,35.81a272.72,272.72,0,0,1,.89,33.66c-.3,8.22-1.37,25.44-2.55,32.39s-2.35,12.52-7.34,19.17-15.94,9.1-28.08,10.08-49.8,2.1-59.78,2.1-55.47-1-68.48-1.71S20.15,142.49,14.18,138,4.59,127.62,2.73,115.48,0,80.36,0,73.61.68,48.76,1.85,37.12,5.76,15.2,12.32,9.82s13.11-7,37.18-8.42S109.51,0,109.51,0Z"/>
                     <path class="cls-j2" d="M178.83,143.4c-10.24,1.85-42.49,3.07-74,3.07s-40.51,1.26-73.24-3.07c4.11-7.37,8.8-14.45,13-17.24A107.82,107.82,0,0,1,60,118.35c2.42-1,4.88-2.07,7.25-3-4.64-6-8.19-12.63-8.19-18.07C59,89.56,76,29.42,105.2,29.42c27.48,0,46.17,60.14,46.17,67.83,0,5.57-3.4,12.21-7.84,18.21,8.22,3.33,17.46,7.4,22.35,10.7C170,129,174.72,136,178.83,143.4Z"/>
                     <path class="cls-j3" d="M31.62,146.35l.21.51A3.06,3.06,0,0,1,31.62,146.35Z"/>
                     <path class="cls-j4" d="M75.31,124.36a80,80,0,0,1-15.34-6c2.42-1,4.88-2.07,7.25-3A83.63,83.63,0,0,0,75.31,124.36Z"/>
                     <path class="cls-j4" d="M150.44,118.35a78.79,78.79,0,0,1-14.32,5.72,79.66,79.66,0,0,0,7.41-8.6C145.79,116.38,148.14,117.35,150.44,118.35Z"/>
                     <path class="cls-j4" d="M146.1,92.14c0,11.59-23.53,37.92-40.9,37.92-15.39,0-40.9-26.33-40.9-37.92s18.31-21,40.9-21S146.1,80.54,146.1,92.14Z"/>
                     <path class="cls-j4" d="M78.16,72.9q27.42-11.22,54.85,0s-16.38-11.22-27.42-11.22S78.16,72.9,78.16,72.9Z"/>
                     <polyline class="cls-j5" points="207.8 38.86 178.91 38.86 167.65 33.71"/>
                     <path class="cls-j6" d="M161,32.75a3.53,3.53,0,1,1,3.53,3.53A3.52,3.52,0,0,1,161,32.75Z"/>
                     <polyline class="cls-j7" points="209.31 84.45 184.08 84.45 174.25 88.95"/>
                     <circle class="cls-j6" cx="171.23" cy="90.24" r="3.53"/>
                     <line class="cls-j6" x1="209.38" y1="60.57" x2="183.24" y2="60.57"/>
                     <path class="cls-j6" d="M176.19,60.67a3.53,3.53,0,1,1,3.53,3.53A3.53,3.53,0,0,1,176.19,60.67Z"/>
                     <polyline class="cls-j6" points="1.56 39.26 19.85 39.26 32.25 33.58"/>
                     <path class="cls-j6" d="M38.91,32.9a3.53,3.53,0,1,0-3.52,3.52A3.53,3.53,0,0,0,38.91,32.9Z"/>
                     <polyline class="cls-j6" points="0.32 84.02 13.19 84.02 25.59 89.7"/>
                     <circle class="cls-j6" cx="28.73" cy="90.38" r="3.53"/>
                     <line class="cls-j6" x1="0.33" y1="60.71" x2="16.71" y2="60.71"/>
                     <path class="cls-j6" d="M23.76,60.81a3.53,3.53,0,1,0-3.52,3.53A3.53,3.53,0,0,0,23.76,60.81Z"/>
                     <path class="cls-j8" d="M264.08,10.59h8.78L260,38.14V54.77h-8.13V38.14L239,10.59h8.71l8.13,18.7Z"/>
                     <path class="cls-j8" d="M272.29,36.93q0-8.85,4.21-13.67t11.84-4.81q7.71,0,11.89,4.81t4.17,13.67q0,8.77-4.21,13.63c-2.81,3.24-6.75,4.86-11.85,4.86s-9-1.63-11.81-4.89S272.29,42.74,272.29,36.93Zm7.92.43q0,10.85,8.13,10.85t8.14-10.85q0-11.7-8.14-11.71T280.21,37.36Z"/>
                     <path class="cls-j8" d="M310.68,19.09h7.85V39.93q0,8.2,6.64,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T310.68,39.93Z"/>
                     <path class="cls-j8" d="M363.85,17.73v37h-8.06v-37H344.37V10.59H376.2l-2,7.14Z"/>
                     <path class="cls-j8" d="M376.56,19.09h7.85V39.93q0,8.2,6.63,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T376.56,39.93Z"/>
                     <path class="cls-j8" d="M420.8,22.23a14.19,14.19,0,0,1,4.39-2.93,15,15,0,0,1,5.46-.85q6.93,0,10.71,4.74t3.78,13.38q0,9.07-4,14t-11.31,4.89A14.29,14.29,0,0,1,420,51.49v3.28H413.1V6h7.7Zm8.28,26q4.21,0,6.18-2.64c1.3-1.76,2-4.55,2-8.35s-.66-6.87-2-8.75-3.35-2.82-6.11-2.82q-8.34,0-8.35,11.42,0,5.72,2,8.43C424.13,47.3,426.23,48.21,429.08,48.21Z"/>
                     <path class="cls-j8" d="M479.54,40H458.2q1.07,8.28,9.06,8.28a12.22,12.22,0,0,0,7.93-3l4.06,5.42a18,18,0,0,1-12.63,4.72q-7.71,0-12.1-4.82t-4.38-13.24q0-8.64,4.21-13.77a13.87,13.87,0,0,1,11.34-5.14q8.07,0,11.71,6.28,2.42,4.14,2.42,11.2A37.26,37.26,0,0,1,479.54,40ZM458.2,33.58h13.92q0-3.93-1.65-5.86a6,6,0,0,0-5-2.14Q459.27,25.58,458.2,33.58Z"/>
                     <path class="cls-j9" d="M267.82,74.84v40q0,11.69-5.28,17.62t-15.73,5.84H240v-14h5.67q3.44,0,4.94-2.22t1.62-7.23V67.39Z"/>
                     <path class="cls-j9" d="M282.93,97.74l-4.67-11.12a40.41,40.41,0,0,1,20.57-5.33q12.1,0,18.06,6.67t5.95,20.34v29.56H309.72V134q-4.56,4.89-13,4.89-9.45,0-15-5.11t-5.56-13.67q0-8.34,5.5-13.34t14.73-5a19.45,19.45,0,0,1,12,3.67c-.38-3.93-1.39-6.73-3.06-8.4s-4.28-2.5-7.84-2.5A40,40,0,0,0,282.93,97.74Zm16.45,16.34q-8.78,0-8.78,5.89,0,3.23,2.06,4.61t6.83,1.39q4.56,0,6.62-1.45a5.17,5.17,0,0,0,2.05-4.55,5,5,0,0,0-2.05-4.5C304.74,114.54,302.49,114.08,299.38,114.08Z"/>
                     <path class="cls-j9" d="M377.3,88.84l-6.78,11q-7-4.89-12.89-4.89a10,10,0,0,0-8.4,4.05q-3.06,4.06-3.05,11.18t3.05,11.11q3.06,3.9,8.84,3.89a18.19,18.19,0,0,0,6.56-1.11,30.43,30.43,0,0,0,6.89-4.11l6.78,11a33.62,33.62,0,0,1-9.89,6.17,32.84,32.84,0,0,1-11.34,1.72q-12.22,0-19.17-7.55t-6.95-21Q331,97,338,89.12t19.06-7.83A30.58,30.58,0,0,1,367.68,83,34.42,34.42,0,0,1,377.3,88.84Z"/>
                     <path class="cls-j9" d="M400.53,102.41,414.2,82.29h17l-19.34,27.9L436,137.86H416.87l-16.34-19.11v19.11h-15v-76h15Z"/>
                     <path class="cls-j9" d="M481.34,116.3H449.88q2.12,9.23,11.78,9.22a16.12,16.12,0,0,0,6.12-1,28.5,28.5,0,0,0,6.33-4.06l7.56,10.12A31.32,31.32,0,0,1,472,137a30.81,30.81,0,0,1-11.34,1.89q-12.11,0-19.12-7.55t-7-20.57q0-13.33,6.73-21.4t17.84-8q13,0,19,10,3.9,6.45,3.89,17.34A65.64,65.64,0,0,1,481.34,116.3Zm-31.68-11.78h17.78a12,12,0,0,0-2.39-7.28A7.43,7.43,0,0,0,459,94.62Q451.77,94.62,449.66,104.52Z"/>
                     <path class="cls-j9" d="M505.34,82.29v5.78q4.89-5.89,13.34-6.34h3.22v15h-3.22q-6.55.23-9.17,3.28c-1.74,2-2.65,5.54-2.72,10.5v27.34h-15V82.29Z"/>
                     <path class="cls-j2" d="M566.14,124.75l-3.89,13.11H545.69l22.68-68.8H584.7l22.57,68.8H590.71l-3.89-13.11ZM570,111.41h12.9l-6.45-22.12Z"/>
                     <path class="cls-j2" d="M619.61,59.17a8.54,8.54,0,0,1,8.67,8.67,8.55,8.55,0,0,1-8.78,8.78,8.73,8.73,0,1,1,.11-17.45Zm13,79.14h-4.33q-8.56,0-12.4-4t-3.83-13.12V82.29h15v37.9a8.86,8.86,0,0,0,.51,3.11c.25.52.83.78,1.72.78h3.33Z"/>
                  </g>
               </g>
            </svg>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md30">
                    <div class="f-22 f-md-42 text-center white-clr lh130 w600">
                        The Pro Features Top Marketers Demand & Need Are Now Available At A Fraction Of The Cost
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md50">
                    <img src="assets/images/product-img.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-11 mx-auto col-12 mt20 mt-md50">
                    <div class="f-18 f-md-36 text-center probtn1">
                        <a href="#buynow" class="text-center">Get Instant Access to YouTubeJacker Ai Pro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center black-clr">
                        Here’s What You’re Getting with <br class="d-none d-md-block"> <span class="w700">This Unfair Advantage Upgrade</span>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md50 ">
                    <div class="f-24 f-md-30 w600 lh130 text-center">
                        Go Limitless As You’re Getting Unlimited Everything
                    </div>
                    <div class="col-12 col-md-12 mt-md50 mt20">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <img src="assets/images/channel.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0 ">
                                <img src="assets/images/video.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/custom-domains.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/storage.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
					<div class="col-12 col-md-12 mt-md50 mt20">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <img src="assets/images/bandwidth.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0 ">
                                <img src="assets/images/traffic.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/affiliate-offers.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md50">
                    No more worrying about anything and focus on growing your business leaps and bounds. <br><br> You’ll have NO LIMITS to what you can do and take your business to the next level. YouTubeJacker Ai Pro upgrade enables you stand out of the crowd,
                    and the best part is that you can <span class="w600">go unlimited for one-time price but ONLY for limited time.</span>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/1.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
						Create Unlimited Channels in ANY NICHE For Yourself and Clients
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Now, establish yourself as an authority in multiple niches or topics by creating unlimited video channels to get maximum traffic, lead & sales hands down.<br>
					Create channels on all your favorite niches...
					<br><br>
					With this feature, you can create AS MANY CHANNELS AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.

                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/2.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    When you upgrade to Pro, you can create UNLIMITED projects inside your business and get viral targeted traffic to boost your profits manifold.
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/3.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Adding Unlimited Videos to get your audience hooked got faster & easier. Now you too can showcase MORE content on 100s of topic or keywords to reach out to wider audience hands-free.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/4.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Custom Domains To Build Your Authority & Credibility
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Build your authority and credibility by showing videos on your own domain with your own LOGO using our unlimited custom domain feature. It also gives you better exposure in search engines.
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/5.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get Unlimited Storage To Get Your Media Content Delivered Faster
                    </div>
                </div>
                <div class="col-12 col-md-6 mx-auto mt20 mt-md40">
                    <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Gone are the days when you had to pay extra for getting unlimited storage.  YouTubeJacker Ai Pro helps to go beyond the extra mile by getting unlimited storage to get your media content delivered faster & easier.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/6.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get Unlimited Bandwidth To Give Best User Experience
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/bandwidth-img.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    We mean every word when we say LIMITLESS. So with the YouTubeJacker Ai Pro edition, you get <span class="w600">UNLIMITED bandwidth to give best user experience without getting worried of any limitations of video size, rich media content and content uploading and downloading bandwidth.</span>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/7.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Drive Unlimited Visitors And Leads For Your Offers
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Driving UNLIMITED visitors on your video pages is a reality now. YouTubeJacker Ai Pro gives you the power to drive unlimited visitors & make the most from them in a hands free manner.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/8.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Ads & Get Maximum Affiliate Commissions
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    YouTubeJacker Ai Pro also enables business owners to maximize their profits & affiliate commissions by creating unlimited ads & driving maximum affiliate commissions’ hassle free.
                </div>
            </div>
        </div>
    </div>
	
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/9.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get 5 stunning and ready-to-use fully customizable video players
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    With this edition, we are giving 5 fully customizable video players, it can show your videos in the most stunning way possible to get best results.<br><br> Click on the customization option, now insert the name of your player, and
                    select player which you want to customize.
                    <br><br> Appearance of video player can be changed by changing the player’s color and player’s text color. There are many more controls are available like center play button, bottom play button, share option, volume controls etc. to
                    make your player beautiful.
                    <br><br> You can change the video actions, by choosing any one from start action and end action. Now click on the save and apply button to go ahead or hit the reset tab to make more changes.
                </div>
            </div>
        </div>
    </div>
	
	 <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/10.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Customize your player with 8 attractive and eye-catchy frames
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Make your videos look enticing and attention grabbing with 8 eye-pleasing video frames that are available only for the customers of this Pro edition. You can pick any frame from the list and your video will be played in the same video frame that you chose.
                        <br>
                        <br> If you choose mobile frame or any other desired screen, then your video will be displayed in the same screen as I am showing you in the video. This is something that will intensify your user engagement level altogether.
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <img src="assets/images/11.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
						Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
					</div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With this feature, you can lock a video anywhere & force the visitors to share it on their social media to watch the FULL video. That way, it can make your video VIRAL and MULTIPLY the traffic with NO EXTRA effort.
<br><br>
It hardly takes 2 minutes in choosing a template and setting up a Social app. You can set Video URL or even your affiliate link to be shared directly on the wall of visitor’s social media. Now you can see the power.
<br><br>
You can do a lot more with it, decision is yours. If anyone find your video useful and embeds the video on their pages, now it works even better as a viral traffic machine. Our social app work right inside the Video and still share your main URL that you set to be shared when someone takes action even on this embedded video.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/12.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md40 ">
                    <div class="f-20 f-md-22 w400 lh130">
                        Oh yes, I am not kidding here. To multiply the benefits of your purchase, we’re offering 50 extra attractive and eye-catchy templates to grab more leads for your offers. No coding, no imagining what looks good and what works, we already did the hard work
                        for you.
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 ">
                    <div class="f-20 f-md-22 w400 lh130 black-clr">
                        You just choose a template, edit the call to action text and image to create a beautiful, crispy and effective Lead app and promo app in seconds. You can set the time at which you want to show them as well.
                        <br>
                        <br> Simply, lock the video and allow it to be watched only when someone fills in the form or allow it to be skipped. We've never offered features like this before, and now this is your only chance to get them.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/13.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Never Offered Before Video Personalization To Boost Conversions
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                      This is something that your customers will love. Now you too can show viewer's name, e-mail address & location inside video on any particular duration.<br><br>

Just imagine how beautiful it would look when you get an email with your own name, location etc. on it.
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/14.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create a personalized e-mail link of the video
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Wanna create a personalized link of the video & share it inside your email to your audience, YouTubeJacker Ai Pro edition has got your back.
                        <br><br> Just use this state of the art technology, & be ready to share your videos to audience scattered globally with zero hassles.
                    </div>
                </div>
            </div>
        </div>
    </div>
   
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/15.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
					With this feature, you can give your video page a unique look & feel by managing various options like page layout, sharing option, related videos etc.
					<br><br>
					You can also insert Google Ads or your own banner ads to boost monetization.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/16.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Capture Leads Of Your Visitors When They Interact On Your Videos
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        This is something that’s gonna take the industry by the storm. No hard work, no contact management system needs to be resorted. With this feature, you will get all your subscribers details in your YouTubeJacker Ai as well as in autoresponder. Whenever your viewer
                        interacts on your videos, they will have to enter their credentials on your video page first and that will get you their name, email address and also show what action they have taken.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/17.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Maximize ROI From Your Leads With Webinar Integration
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f17.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With Pro edition, we are making it easier for you to integrate a webinar like a pro. So, when someone fill the form, they automatically will get registered for webinar & you can get maximum ROI.
                        <br><br> For webinar integration, just put API key inside the box and you are ready to get more registrants without losing a single lead.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/8.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Business
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
  
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/9.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Video Channels
                    </div>
                </div>
                <div class="col-12 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12  f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
   
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/10.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Videos In Channels
                    </div>
                </div>
                <div class="col-12 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12  f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
     -->
    <!-- <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/11.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get UNLIMITED Bandwidth To Give Best User Experience
                    </div>
                </div>
                <div class="col-12 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f4.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    We mean every word when we say LIMITLESS. So with the YouTubeJacker Ai Pro edition, you get <span class="w600">UNLIMITED bandwidth to give best user experience without getting worried of any limitations of video size, rich media content and content uploading and downloading bandwidth. </span>
                </div>
            </div>
        </div>
    </div> -->
  
    
    
    
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <img src="assets/images/16.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Unlock our Advanced Advertisement technology for better monetization with Video Ads, Image Ads, Text ads or Even Show an Html Page Right inside videos
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        This is something that’s gonna take the industry by the storm. No hard work, no contact management system needs to be resorted. With this feature, you will get all your subscribers details in your YouTubeJacker Ai as well as in autoresponder. Whenever your viewer
                        interacts on your videos, they will have to enter their credentials on your video page first and that will get you their name, email address and also show what action they have taken.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/17.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Make more money using Google AdSense or Affiliate Banner on your channel and video pages
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <img src="assets/images/f18.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130 black-clr">
                        With this amazing add on, you can attract more and more customers by inserting Google AdSense and banner ads on your channel/video pages. Everything is already fixed you don’t need to sweat out for anything.
                        <br><br> For inserting google ads you just need to insert the required link inside the box. For putting banner inside the video, you have to insert the URL of the place where you want your customer to get redirected.
                        <br><br> Page layout setting is another great feature available to make changes in the look of the video page by adding tabs like share tab, Email Tab, Embed tab, comment tab and many more. Additionally, you can make these pages
                        public or password protected if you want.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/18.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Maximize ROI From Your Leads With Webinar Integration
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <img src="assets/images/f17.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With Pro edition, we are making it easier for you to integrate a webinar like a pro. So, when someone fill the form, they automatically will get registered for webinar & you can get maximum ROI.
                        <br><br> For webinar integration, just put API key inside the box and you are ready to get more registrants without losing a single lead.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/18.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Unlimited Team Management To Let Your Team Handle All The Manual Work
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/team-member.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Create custom roles, assign privileges and manage your projects and even your client’s projects simply fast and easy for Unlimited Team members.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <img src="assets/images/19.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get All These Benefits At An Unparalleled Price
                    </div>
                </div>
                <div class="col-12 mt-md50">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 col-md-6 order-md-2 mt20 mt-md0">
                            <img src="assets/images/f19.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <div class="f-20 f-md-22 w400 lh130">
                                Ultimately you are saving thousands of dollars monthly with us, & every dollar that you save, adds to your profits. By multiplying engagement & opt-ins, you’re not only saving money,<span class="w600">you’re also taking your business to the next level.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section Starts -->
    <div class="goodreason-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 w500 lh130 text-center">
                        Act Now For <span class="w700">2 Very Good Reasons:</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row">
                        <div class="col-xs-2 col-md-1 pl0 pr10">
                            <div>
                                <img src="assets/images/one.png" class="d-block img-fluid mb-md0 mb15 mx-auto">
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-11">
                            <div class="f-20 f-md-22 w400 lh130">
                                We’re offering YouTubeJacker Ai Pro at a CUSTOMER’S ONLY discount, so you can grab this <span class="w600">game-changing upgrade for a low ONE-TIME fee.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row">
                        <div class="col-xs-2 col-md-1 pl0 pr10">
                            <div>
                                <img src="assets/images/two.png" class="d-block img-fluid mb-md0 mb15 mx-auto">
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-11">
                            <div class="f-20 f-md-22 w400 lh130 mt-md10">
                                <span class="w600">Your only chance to get access is right now,</span> on this page. When this page closes, so does this exclusive offer.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section End -->
    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center white-clr">
                        Let’s Remove All The Risks For You And Secure It With <span class="w700"> <br class="d-none d-md-block"> 30 Days Money Back Guarantee.</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 col-md-4">
                            <img src="assets/images/moneyback.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-8 mt20 mt-md0">
                            <div class="f-md-24 f-20 lh130 w400 white-clr">
                                You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w500">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                                <br><br> <span class="w500">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                                <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->
    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="but-design f-28 f-md-50 w700 lh130 text-center white-clr">
                        But That’s Not All
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                    <div class="f-22 f-md-30 w400  text-center lh130">
                        In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block"> today and start profiting from this opportunity.
                    </div>
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 1
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Utilizing Facebook For Your Online Business <span class="w400 blue-head"> (Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Facebook helps people connect easily with your business. It offers some compelling methods for companies that wish to maximize their business reach. When used properly, it gives best results for your business.
                            <br>
                            <br> Keeping this in mind, here’s an exclusive guide will let you how exactly Facebook works and helps small business become bigger than imagined through the power of social networking.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 2
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            List Building Mojo <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Interested in growing your list, discover 100 techniques to inflame your list subscriber like Specific Date technique, JV discount technique, Give it away technique, subscribe-only technique, plenty of ways technique and Opt-in-auction technique etc.
                            This is an enlightening report which disclose a closer look of list building that you need to know.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 3
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            How To Create A Lead Magnet <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Lead Magnet maximizes the number of targeted leads you are getting for an offer. You can also increase the number of prospects and customers you have joining your list.
                            <br>
                            <br> So, to help you make the most from this opportunity, this ultimate guide will help you find step by step tips for creating your lead magnet and make their best use to maximize your profits.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus4 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 4
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Viral Marketing Stampede <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Inside this bonus, you'll get the exact strategies that highly successful marketers have used for years to target masses without spending a fortune. So, you can easily drive hordes of traffic and get maximum exposure for your offerings. Stop thinking
                            and take action now to get best results.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1  mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus5 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 5
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            The Newbies Guide to Traffic Generation<br class="d-none d-md-block"> <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Online business is rocking & is the best way to reach out to maximum customers without any geographical barrier. And, it’s no surprise that 85% of the people who spend time online also purchase online.
                            <br>
                            <br> Those who are ready to build their own website and make huge profit by drive lots of traffic can get help from this ultimate bonus package. You will surely learn effective methods to drive real traffic to your business
                        </div>
                    </div>
                    <div class="col-12 col-md-6  mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus6 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 6
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Targeted Traffic Mastery <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            The key to success in affiliate business is targeted traffic. Making real money from affiliate program is not that easy. But if you have established your business and have built a good following and repeated traffic, you can make greater profit per month.
                            Quick and targeted traffic can only be acquired via advertisement.
                            <br>
                            <br> Inside this video series, you are about to learn the essential information relevant to setting up a paid advertising campaign
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->
    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-20 f-md-22 w400 lh130 text-center">
                        Yup..! Take action on this while you can as you won’t be seeing this offer again.
                    </div>
                    <div class="f-30 f-md-50 w500 lh130 mt20 text-center black-clr">
                        Today You Can <span class="w700">Get Unrestricted Access to YouTubeJacker Ai</span> For LESS Than the Price of Just One Month’s Membership.
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt30 mt-md70">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div class="tbbg2-text mt0 mt-md0 w600 f-md-50 f-30 text-center" editabletype="text" style="z-index:10;">
                                YouTubeJacker Ai Pro Plan
                            </div>
                        </div>
                        <div class="text-center">
                            <ul class="f-20 f-md-22 w400 lh130 text-center vgreytick mb0 text-capitalize">
                                <li class="odd">
                                    <span class="w600">Break Free & Go Limitless- </span>Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers.
                                </li>
                                <li class="even">
                                    Create Unlimited Channels in ANY NICHE For Yourself and Clients
                                </li>
                                <li class="even">
                                    Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                                </li>
                                <li class="even">
                                    Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                                </li>
                                <li class="odd">
                                    Add Unlimited Custom Domains To Build Your Authority & Credibility
                                </li>
                                <li class="even">
                                   Getting Unlimited Storage To Get Your Media Content Delivered Faster
                                </li>
                                <li class="even">
                                   Getting Unlimited Bandwidth To Give Best User Experience
                                </li>
                                <li class="odd">
                                    Drive Unlimited Visitors And Leads For Your Offers
                                </li>
                                <li class="odd">
                                    Create Unlimited Ads & Get Maximum Affiliate Commissions
                                </li>
                                <li class="odd">
                                    Get 5 Stunning And Ready-To-Use Fully Customizable Video Players
                                </li>
                                <li class="even">
                                    Customize Your Player With 8 Attractive And Eye-Catchy Frames
                                </li>
                                <li class="odd">
                                   Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
                                </li>
                                <li class="odd">
                                    Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                                </li>
                                <li class="even">
                                   Never Offered Before Video Personalization To Boost Conversions
                                </li>
                                <li class="odd">
                                    Create A Personalized E-Mail Link of The Video
                                </li>
                                <li class="odd">
                                    Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                                </li>
                                <li class="even">
                                   Capture Leads Of Your Visitors When They Interact On Your Videos
                                </li>
                                <li class="odd">
                                   Maximize ROI From Your Leads With Webinar Integration
                                </li>
								<li class="even">
                                   Unlimited Team Management To Let Your Team Handle All The Manual Work
                                </li>
                                <li class="odd">
                                   Get All These Benefits At An Unparalleled Price
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="myfeatureslast f-md-25 f-16 w400 text-center lh130 hideme relative mb-md15" editabletype="shape" style="opacity: 1; z-index: 8;">
                            <div class="col-12 p0">
                                <div class="row">
                                    <div class="col-md-6 offset-md-3 col-12  mb-md0 mb30 mb-md0">
                                        <div class="f-md-30 f-20 w600 lh130 mb-md20 mb15 text-center">
                                            One Time Plan
                                        </div>
                                        <div>
                                            <a href="https://warriorplus.com/o2/buy/hz2587/l07g46/wc8nsf"><img src="https://warriorplus.com/o2/btn/fn200011000/hz2587/l07g46/364108" alt="YouTubeJacker Ai Pro One Time Deal" class="img-fluid d-block mx-auto" border="0" /></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 thanks-button text-center">
                  <p><a href="https://warriorplus.com/o2/buy/vq3h1m/df4ftr/h5zgxc"><img src="https://warriorplus.com/o2/btn/fn100011000/vq3h1m/df4ftr/373768"></a></p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
<p> <a class="kapblue f-20 f-md-22 lh130 w400 text-capitalize" href="https://warriorplus.com/o/nothanks/df4ftr" target="_blank"> No Thanks, I Don't Want To Go Unlimited &amp; Get 5x More Traffic, Commissions &amp; Profits Faster &amp; Easier With This Pro Upgrade. Please take me to the next step to get access to my purchase. </a> </p>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->
    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 632.61 146.86" style="max-height:70px;">
                  <defs>
                     <style>
                        .cls-j1{fill:url(#linear-gradient);}
                        .cls-j2{fill:#f9be32;}
                        .cls-j3{fill:#111428;}
                        .cls-j4{fill:#231f20;}
                        .cls-j5,.cls-j6,.cls-j7{fill:none;stroke:#000;stroke-miterlimit:10;}
                        .cls-j5{stroke-width:2.27px;}
                        .cls-j6{stroke-width:2.51px;}
                        .cls-j7{stroke-width:1.98px;}
                        .cls-j8{fill:#ed1c24;}
                        .cls-j9{fill:#fff;}
                     </style>
                     <linearGradient id="linear-gradient" x1="4.85" y1="13.03" x2="213.19" y2="138.53" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#ed2224"/>
                        <stop offset="1" stop-color="#cb2026"/>
                     </linearGradient>
                  </defs>
                  <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-j1" d="M109.51,0s41.35.3,60.92,2.12,22.89,3.59,29.74,11,7.24,24.17,8.41,35.81a272.72,272.72,0,0,1,.89,33.66c-.3,8.22-1.37,25.44-2.55,32.39s-2.35,12.52-7.34,19.17-15.94,9.1-28.08,10.08-49.8,2.1-59.78,2.1-55.47-1-68.48-1.71S20.15,142.49,14.18,138,4.59,127.62,2.73,115.48,0,80.36,0,73.61.68,48.76,1.85,37.12,5.76,15.2,12.32,9.82s13.11-7,37.18-8.42S109.51,0,109.51,0Z"/>
                     <path class="cls-j2" d="M178.83,143.4c-10.24,1.85-42.49,3.07-74,3.07s-40.51,1.26-73.24-3.07c4.11-7.37,8.8-14.45,13-17.24A107.82,107.82,0,0,1,60,118.35c2.42-1,4.88-2.07,7.25-3-4.64-6-8.19-12.63-8.19-18.07C59,89.56,76,29.42,105.2,29.42c27.48,0,46.17,60.14,46.17,67.83,0,5.57-3.4,12.21-7.84,18.21,8.22,3.33,17.46,7.4,22.35,10.7C170,129,174.72,136,178.83,143.4Z"/>
                     <path class="cls-j3" d="M31.62,146.35l.21.51A3.06,3.06,0,0,1,31.62,146.35Z"/>
                     <path class="cls-j4" d="M75.31,124.36a80,80,0,0,1-15.34-6c2.42-1,4.88-2.07,7.25-3A83.63,83.63,0,0,0,75.31,124.36Z"/>
                     <path class="cls-j4" d="M150.44,118.35a78.79,78.79,0,0,1-14.32,5.72,79.66,79.66,0,0,0,7.41-8.6C145.79,116.38,148.14,117.35,150.44,118.35Z"/>
                     <path class="cls-j4" d="M146.1,92.14c0,11.59-23.53,37.92-40.9,37.92-15.39,0-40.9-26.33-40.9-37.92s18.31-21,40.9-21S146.1,80.54,146.1,92.14Z"/>
                     <path class="cls-j4" d="M78.16,72.9q27.42-11.22,54.85,0s-16.38-11.22-27.42-11.22S78.16,72.9,78.16,72.9Z"/>
                     <polyline class="cls-j5" points="207.8 38.86 178.91 38.86 167.65 33.71"/>
                     <path class="cls-j6" d="M161,32.75a3.53,3.53,0,1,1,3.53,3.53A3.52,3.52,0,0,1,161,32.75Z"/>
                     <polyline class="cls-j7" points="209.31 84.45 184.08 84.45 174.25 88.95"/>
                     <circle class="cls-j6" cx="171.23" cy="90.24" r="3.53"/>
                     <line class="cls-j6" x1="209.38" y1="60.57" x2="183.24" y2="60.57"/>
                     <path class="cls-j6" d="M176.19,60.67a3.53,3.53,0,1,1,3.53,3.53A3.53,3.53,0,0,1,176.19,60.67Z"/>
                     <polyline class="cls-j6" points="1.56 39.26 19.85 39.26 32.25 33.58"/>
                     <path class="cls-j6" d="M38.91,32.9a3.53,3.53,0,1,0-3.52,3.52A3.53,3.53,0,0,0,38.91,32.9Z"/>
                     <polyline class="cls-j6" points="0.32 84.02 13.19 84.02 25.59 89.7"/>
                     <circle class="cls-j6" cx="28.73" cy="90.38" r="3.53"/>
                     <line class="cls-j6" x1="0.33" y1="60.71" x2="16.71" y2="60.71"/>
                     <path class="cls-j6" d="M23.76,60.81a3.53,3.53,0,1,0-3.52,3.53A3.53,3.53,0,0,0,23.76,60.81Z"/>
                     <path class="cls-j8" d="M264.08,10.59h8.78L260,38.14V54.77h-8.13V38.14L239,10.59h8.71l8.13,18.7Z"/>
                     <path class="cls-j8" d="M272.29,36.93q0-8.85,4.21-13.67t11.84-4.81q7.71,0,11.89,4.81t4.17,13.67q0,8.77-4.21,13.63c-2.81,3.24-6.75,4.86-11.85,4.86s-9-1.63-11.81-4.89S272.29,42.74,272.29,36.93Zm7.92.43q0,10.85,8.13,10.85t8.14-10.85q0-11.7-8.14-11.71T280.21,37.36Z"/>
                     <path class="cls-j8" d="M310.68,19.09h7.85V39.93q0,8.2,6.64,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T310.68,39.93Z"/>
                     <path class="cls-j8" d="M363.85,17.73v37h-8.06v-37H344.37V10.59H376.2l-2,7.14Z"/>
                     <path class="cls-j8" d="M376.56,19.09h7.85V39.93q0,8.2,6.63,8.21t6.64-8.21V19.09h7.85V39.93q0,15.48-14.56,15.49T376.56,39.93Z"/>
                     <path class="cls-j8" d="M420.8,22.23a14.19,14.19,0,0,1,4.39-2.93,15,15,0,0,1,5.46-.85q6.93,0,10.71,4.74t3.78,13.38q0,9.07-4,14t-11.31,4.89A14.29,14.29,0,0,1,420,51.49v3.28H413.1V6h7.7Zm8.28,26q4.21,0,6.18-2.64c1.3-1.76,2-4.55,2-8.35s-.66-6.87-2-8.75-3.35-2.82-6.11-2.82q-8.34,0-8.35,11.42,0,5.72,2,8.43C424.13,47.3,426.23,48.21,429.08,48.21Z"/>
                     <path class="cls-j8" d="M479.54,40H458.2q1.07,8.28,9.06,8.28a12.22,12.22,0,0,0,7.93-3l4.06,5.42a18,18,0,0,1-12.63,4.72q-7.71,0-12.1-4.82t-4.38-13.24q0-8.64,4.21-13.77a13.87,13.87,0,0,1,11.34-5.14q8.07,0,11.71,6.28,2.42,4.14,2.42,11.2A37.26,37.26,0,0,1,479.54,40ZM458.2,33.58h13.92q0-3.93-1.65-5.86a6,6,0,0,0-5-2.14Q459.27,25.58,458.2,33.58Z"/>
                     <path class="cls-j9" d="M267.82,74.84v40q0,11.69-5.28,17.62t-15.73,5.84H240v-14h5.67q3.44,0,4.94-2.22t1.62-7.23V67.39Z"/>
                     <path class="cls-j9" d="M282.93,97.74l-4.67-11.12a40.41,40.41,0,0,1,20.57-5.33q12.1,0,18.06,6.67t5.95,20.34v29.56H309.72V134q-4.56,4.89-13,4.89-9.45,0-15-5.11t-5.56-13.67q0-8.34,5.5-13.34t14.73-5a19.45,19.45,0,0,1,12,3.67c-.38-3.93-1.39-6.73-3.06-8.4s-4.28-2.5-7.84-2.5A40,40,0,0,0,282.93,97.74Zm16.45,16.34q-8.78,0-8.78,5.89,0,3.23,2.06,4.61t6.83,1.39q4.56,0,6.62-1.45a5.17,5.17,0,0,0,2.05-4.55,5,5,0,0,0-2.05-4.5C304.74,114.54,302.49,114.08,299.38,114.08Z"/>
                     <path class="cls-j9" d="M377.3,88.84l-6.78,11q-7-4.89-12.89-4.89a10,10,0,0,0-8.4,4.05q-3.06,4.06-3.05,11.18t3.05,11.11q3.06,3.9,8.84,3.89a18.19,18.19,0,0,0,6.56-1.11,30.43,30.43,0,0,0,6.89-4.11l6.78,11a33.62,33.62,0,0,1-9.89,6.17,32.84,32.84,0,0,1-11.34,1.72q-12.22,0-19.17-7.55t-6.95-21Q331,97,338,89.12t19.06-7.83A30.58,30.58,0,0,1,367.68,83,34.42,34.42,0,0,1,377.3,88.84Z"/>
                     <path class="cls-j9" d="M400.53,102.41,414.2,82.29h17l-19.34,27.9L436,137.86H416.87l-16.34-19.11v19.11h-15v-76h15Z"/>
                     <path class="cls-j9" d="M481.34,116.3H449.88q2.12,9.23,11.78,9.22a16.12,16.12,0,0,0,6.12-1,28.5,28.5,0,0,0,6.33-4.06l7.56,10.12A31.32,31.32,0,0,1,472,137a30.81,30.81,0,0,1-11.34,1.89q-12.11,0-19.12-7.55t-7-20.57q0-13.33,6.73-21.4t17.84-8q13,0,19,10,3.9,6.45,3.89,17.34A65.64,65.64,0,0,1,481.34,116.3Zm-31.68-11.78h17.78a12,12,0,0,0-2.39-7.28A7.43,7.43,0,0,0,459,94.62Q451.77,94.62,449.66,104.52Z"/>
                     <path class="cls-j9" d="M505.34,82.29v5.78q4.89-5.89,13.34-6.34h3.22v15h-3.22q-6.55.23-9.17,3.28c-1.74,2-2.65,5.54-2.72,10.5v27.34h-15V82.29Z"/>
                     <path class="cls-j2" d="M566.14,124.75l-3.89,13.11H545.69l22.68-68.8H584.7l22.57,68.8H590.71l-3.89-13.11ZM570,111.41h12.9l-6.45-22.12Z"/>
                     <path class="cls-j2" d="M619.61,59.17a8.54,8.54,0,0,1,8.67,8.67,8.55,8.55,0,0,1-8.78,8.78,8.73,8.73,0,1,1,.11-17.45Zm13,79.14h-4.33q-8.56,0-12.4-4t-3.83-13.12V82.29h15v37.9a8.86,8.86,0,0,0,.51,3.11c.25.52.83.78,1.72.78h3.33Z"/>
                  </g>
               </g>
            </svg>
                    <div editabletype="text" class="f-18 f-md-18 w400 mt20 lh130 white-clr text-center">
                      <p>Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </p>
                      <p>&nbsp;</p>
                      <p><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/vq3h1m" defer></script><div class="wplus_spdisclaimer"></div></p>
                    </div>
                    <div class="f-15 f-md-18 w600 lh150 white-clr text-xs-center mt20"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/vq3h1m" defer></script><div class="wplus_spdisclaimer"></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 lh130 white-clr text-xs-center">Copyright © YouTubeJacker Ai 2023</div>
                    <ul class="footer-ul w400 f-18 f-md-20 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="#" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
<div class="underlay"></div>
<div class="modal-wrapper" style="display:block;">
    <div class="modal-bg">
        <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
            X 
        </button>
        <div class="model-header">
    <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
            <div class="f-md-56 f-24 w700 lh130 white-clr">
                WAIT! HOLD ON!!
            </div>
            <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                Don't Leave Empty Handed
            </div>
            </div>
        </div>
        </div>
        <div class="col-12 for-padding">
            <div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
        You've Qualified For An <span class="w700 red-clr"><br> INSTANT $100 DISCOUNT</span>  
            </div>
        <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
            Regular Price <strike>$137</strike>,
            Now Only $37	
        </div>
        <div class="mt-md20 mt10 text-center">
            <a href="https://warriorplus.com/o2/buy/vq3h1m/df4ftr/h5zgxc" class="cta-link-btn1">Grab SPECIAL Discount Now!
            <br>
            <span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
        </div>
            <!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
                Coupon Code EXPIRES IN:
            </div>
            <div class="timer-new">
            <div class="days" class="timer-label">
            <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
            <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
            <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
            <span id="secs" ></span> <span class="text">Sec</span>
            </div>
        </div> -->
    </div>
</div>
</div>
<script type="text/javascript">
    var timeInSecs;
    var ticker;
    function startTimer(secs) {
    timeInSecs = parseInt(secs);
    tick();
    //ticker = setInterval("tick()", 1000);
    }
    function tick() {
    var secs = timeInSecs;
    if (secs > 0) {
    timeInSecs--;
    } else {
    clearInterval(ticker);
    //startTimer(20 * 60); // 4 minutes in seconds
    }
    var days = Math.floor(secs / 86400);
    secs %= 86400;
    var hours = Math.floor(secs / 3600);
    secs %= 3600;
    var mins = Math.floor(secs / 60);
    secs %= 60;
    var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
    document.getElementById("days").innerHTML = days
    document.getElementById("hours").innerHTML = hours
    document.getElementById("mins").innerHTML = mins
    document.getElementById("secs").innerHTML = secs;
    }
    
    //startTimer(60 * 60); 
    // 4 minutes in seconds

    function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
              }
          }
          return "";
    }
    var cnt = 60*60;

    function counter(){
     
       if(getCookie("cnt") > 0){
          cnt = getCookie("cnt");
       }

       cnt -= 1;
       document.cookie = "cnt="+ cnt;

       startTimer(getCookie("cnt"));
       //jQuery("#counter").val(getCookie("cnt"));

       if(cnt>0){
          setTimeout(counter,1000);
       }
    
    }
    
    counter();
    

    </script>-
    <script type="text/javascript">
       var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
       aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
       });
    </script>
    <!-- Exit Popup Ends-->
          <!--- timer end-->
 <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
var checkBox = document.getElementById("check");
var buybutton = document.getElementById("buyButton");
if (checkBox.checked == true){
      buybutton.getAttribute("href");
      buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
  } else {
       buybutton.getAttribute("href");
       buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
    }
}
</script>
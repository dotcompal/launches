<!Doctype html>
<html>

<head>
   <title>YoDrive Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="YoDrive | Prelaunch">
   <meta name="description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta name="keywords" content="YoDrive">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.yodrive.co/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="YoDrive | Prelaunch">
   <meta property="og:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta property="og:image" content="https://www.yodrive.co/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="YoDrive | Prelaunch">
   <meta property="twitter:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta property="twitter:image" content="https://www.yodrive.co/prelaunch/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyo.com/launches/yodrive/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   
</head>

<body>
   
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row align-items-center">
                  <div class="col-md-3 text-center text-md-start">
                 <svg id="Layer_11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px"><defs><style>.cls-11{fill:#fff;}</style></defs><path class="cls-11" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path><path class="cls-11" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path><path class="cls-11" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path><path class="cls-11" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path><path class="cls-11" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path><path class="cls-11" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path><path class="cls-11" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path><path class="cls-11" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path><rect class="cls-11" x="241.72" y="25.45" width="8.53" height="6.34"></rect><path class="cls-11" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path><path class="cls-11" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path></svg>
             </div>
                  <div class="col-md-9 mt20 mt-md5">
                     <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                        <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-12 mt30 mt-md50 text-center">
               <div class="preline f-18 f-md-22 w600 black-clr lh140">
                  <div class="preheadline">
                        Register for One Time Only Value Packed Free Training  & Get an Assured Gift + 10 Free Licenses 
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md50 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
               Discover How to <span class="yellow-clr">Supercharge Your Website and Boost Engagement, Conversions, and Sales</span> to Make All-Time Big Profits in Your Business 
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               <div class="postheadline">
                  In This Free Training, we will also reveal, how we have sold & securely delivered over $7 Mn in products and how you can get started easily without any tech hassles. 
               </div>
            </div>
            </div>
         
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/video-thumb.gif" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                  <div class="calendar-wrap-inline">
                     <div class="f-20 f-md-24 w800 text-center lh180 orange-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">3<sup contenteditable="false">rd</sup> AUGUST, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1847469316" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6308934" />
                              <input type="hidden" name="redirect" value="https://www.yodrive.co/prelaunch-thankyou" id="redirect_2a38bbac12bac425cb9b32c30eea28fb" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1847469316" class="af-form">
                              <div id="af-body-1847469316" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114459410"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114459410" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114459411"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114459411" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- Header Section Start -->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-36 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                        Why Attend This Free Training Webinar & What Will You Learn? 
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                        Our 25+ Years of Experience Is Packaged in This State-Of-Art 1-Hour Webinar Where You’ll Learn: 
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li>How We Have Made Over $7Mn by Selling & Securely Delivering Digital Products Using the Power of YoDrive. You Can Follow the Same to Build a Profitable Business Online.  </li>
                           <li>How you can Supercharge Websites, Apps & Landing Pages. </li>
                           <li>How We Have Saved 1000s Of Dollars Monthly on Money Sucking Expensive Cloud Hosting Platforms and Still Not Getting Desired Results. </li>
                           <li>How You Can Tap into The Fastest Growing E-Learning, Video Marketing & Online Business Without Any Tech Hassles and Make Easy 6 Figures.  </li>
                           <li>A Sneak-Peak Of "YoDrive", A Must-Have Cloud-Based Technology to Host & Deliver UNLIMITED HD Videos, Training, Website Images, & Media Files – All from One Platform </li>
                           <li>During This Launch Special Deal, Get All Benefits at A Limited Low One-Time-Fee. </li>
                           <li>Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us Plus We Are Giving 10 Licenses of Our Premium Solution – YoDrive  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh140 white-clr" editabletype="text" style="z-index:10;">
                     So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh140 mx-auto my20 text-center white-clr">
                     Register Now! And Join Us Live On August 3rd, 10:00 AM EST
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="1847469316">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6308934">
                           <input type="hidden" name="redirect" value="https://www.yodrive.co/prelaunch-thankyou" id="redirect_2a38bbac12bac425cb9b32c30eea28fb">
                           <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-1847469316" class="af-form">
                           <div id="af-body-1847469316" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114459410"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-114459410" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114459411"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-114459411" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px;">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"/>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"/>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"/>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"/>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"/>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"/>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"/>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"/>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"/>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"/>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"/>
                  </svg>
                  <div class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © YoDrive</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->


<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-1847469316').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1847469316")) {
                document.getElementById("af-form-1847469316").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1847469316")) {
                document.getElementById("af-body-1847469316").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1847469316")) {
                document.getElementById("af-header-1847469316").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1847469316")) {
                document.getElementById("af-footer-1847469316").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>
</body>

</html>
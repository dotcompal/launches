<html>
   <head>
      <title>JV Page - YoDrive JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="YoDrive | JV">
      <meta name="description" content="Host & Deliver, Website Images, Files, Training & Play HD Videos at Lightning-Fast Speed">
      <meta name="keywords" content="YoDrive">
      <meta property="og:image" content="https://www.yodrive.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="YoDrive | JV">
      <meta property="og:description" content="Host & Deliver, Website Images, Files, Training & Play HD Videos at Lightning-Fast Speed">
      <meta property="og:image" content="https://www.yodrive.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoDrive | JV">
      <meta property="twitter:description" content="Host & Deliver, Website Images, Files, Training & Play HD Videos at Lightning-Fast Speed">
      <meta property="twitter:image" content="https://www.yodrive.co/jv/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/yodrive/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/yodrive/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'August 3 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 300 76.82" style="max-height: 55px;">
                     <defs>
                        <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:url(#linear-gradient-6);}.cls-3{fill:url(#linear-gradient-5);}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}.cls-6{fill:#231f20;}.cls-7{fill:url(#linear-gradient-4);}</style>
                        <linearGradient id="linear-gradient" x1="0" y1="29.46" x2="106.23" y2="29.46" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#182b55"></stop>
                           <stop offset="1" stop-color="#134b95"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1="52.59" y1="31.65" x2="59.87" y2="31.65" xlink:href="#linear-gradient"></linearGradient>
                        <linearGradient id="linear-gradient-3" x1=".49" y1="52.84" x2="121.73" y2="52.84" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#f37021"></stop>
                           <stop offset="1" stop-color="#f6861f"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-4" x1="28.04" y1="61.74" x2="35.32" y2="61.74" xlink:href="#linear-gradient"></linearGradient>
                        <linearGradient id="linear-gradient-5" x1="78.09" y1="46.85" x2="85.37" y2="46.85" xlink:href="#linear-gradient"></linearGradient>
                        <linearGradient id="linear-gradient-6" x1="241.72" y1="28.62" x2="250.25" y2="28.62" xlink:href="#linear-gradient-3"></linearGradient>
                     </defs>
                     <path class="cls-6" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                     <path class="cls-6" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                     <path class="cls-6" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                     <path class="cls-6" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                     <path class="cls-6" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                     <path class="cls-5" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-4" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                     <path class="cls-7" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-3" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-6" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                     <rect class="cls-2" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                     <path class="cls-6" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                     <path class="cls-6" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                  </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end text-center text-md-end  mt15 mt-md0">
                     <ul class="leader-ul f-16 f-md-16">
                        <li>
                           <a href=" https://docs.google.com/document/d/1Q-IVwXVd2pWkZplMV4oOJ-9oZBZqMXP1/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">JV Docs</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="https://docs.google.com/document/d/1Uph15TdnAT-NAGiiSCPlWso5cBp0zzvv/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">Swipes & Bonuses </a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="#funnel">Sales Pages</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/384700" class="affiliate-link-btn ml-md15 mt10 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-16 f-md-16 w600 white-clr lh140">
                  Join Us on August 3<sup>rd</sup> for Something EXTRAORDINARY!
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-55 f-28 w700 text-center black-clr lh140">
                  Bank In Big by Promoting Lightning-Fast Platform to <span class="orange-clr">Host & Deliver UNLIMITED HD Videos, Trainings, Website Images, & Media Files</span> with NO Monthly Fee Ever...

                  
               </div>
               <div class="col-12 mt15 f-18 f-md-22 w600 text-center lh140 black-clr text-capitalize">                
               (Backed By World’s BIGGEST Architecture)
               </div>
               <div class="col-12 mt10 mt-md10 f-18 f-md-22 w600 text-center lh140 black-clr text-capitalize">
                  <img src="https://cdn.oppyo.com/launches/yodrive/common_assets/images/pre-head1.webp" class="img-fluid d-block mx-auto py15" alt="pre-head1">
                  Convert More Leads & Sales with FAST Videos | Boost Website Speed | Tap Into Fastest Growing <br class="d-none d-md-block">E-Learning, Video Marketing, And Online Business
                  <img src="https://cdn.oppyo.com/launches/yodrive/common_assets/images/pre-head2.webp" class="img-fluid d-block mx-auto py15" alt="pre-head2">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="row">
                     <div class="col-md-8 col-12">
                        <!--<img src="assets/images/video-img.webp" alt="Video Image" class="img-fluid mx-auto d-block">-->
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://yodrive.dotcompal.com/video/embed/jfm7m1yhah" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="calendar-wrap">
                           <img src="assets/images/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                        </div>
                        <div class="clearfix"></div>
                        <div class="countdown counter-black mt15">
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">01</span><br><span class="f-14 w500">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">16</span><br><span class="f-14 w500">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">59</span><br><span class="f-14 w500">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">37</span><br><span class="f-14 w500">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li>
                                 <span class="w600">Must-Have Solution</span> for Every Online Business to <span class="w600"> Boost Website Speed  </span>
                              </li>
                              <li>
                                 Tap into the <span class="w600"> $398B E-learning Industry-</span> Deliver Videos Courses, E-books & PDFs at Lightning-Fast Speed
                              </li>
                              <li>
                                 Deliver All Your Files at <span class="w600">Lightning-Fast Speed with World's Best CDNs</span> 
                              </li>
                              <li>
                                 <span class="w600">Powerful Battle-Tested Architecture</span> Happily Serving 69 Million+ Marketing Files  
                              </li>
                              <li>
                                 <span class="w600">Analytics</span> to Know Your User Engagement on Your Files & Videos
                              </li>
                              <li>
                                 <span class="w600">Unbreakable File Security</span> with SSL & OTP Enabled Login
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li>
                                 <span class="w600">Sell Online-</span>  Deliver Your Digital Products & Client Projects Fast 
                              </li>
                              <li>
                                 <span class="w600">Host, Manage & Publish Unlimited</span>  PDFs, Docs, Audios, Videos, Zip Files, or Any Other Marketing File 
                              </li>
                              <li>
                                 Get Tons of <span class="w600">Leads & Affiliate Sales -</span>  Deliver freebies, Bonuses, etc.  
                              </li>
                              <li>
                                 Protected, Elegant & SEO Optimized <span class="w600">Sharing Pages & Sharing Channels </span> 
                              </li>
                              <li>
                                 Online Back-Up & 30 days <span class="w600">File recovery</span>   
                              </li>
                              <li>
                                 Great Value at <span class="w600"> One-Time Low Price</span> for this ROBUST SOLUTIONS 
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-12">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li>
                                 <span class="w600">Commercial License</span> Included to Serve Your Clients and Make Huge Profits.
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="form-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
                     <div class="f-md-40 f-28 d-block mb0 lh140 w700 text-center white-clr">
                        <span class="w700">Subscribe To Our JV List</span> <br class="d-none d-md-block"> <span class="f-24 f-md-28 w500"> and Be The First to Know Our Special Contest, Events and Discounts</span>
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt15 mt-md50">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="155455963" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6308929" />
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_4253310f57ac84ec2aa7547ada088667" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-155455963" class="af-form">
                              <div id="af-body-155455963" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-4">
                                    <label class="previewLabel" for="awf_field-114459404" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb15 mb-md15 input-type">
                                       <input id="awf_field-114459404" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb15 mb-md25  col-md-4">
                                    <label class="previewLabel" for="awf_field-114459405" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-114459405" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-4">
                                    <input name="submit" class="submit f-20 f-md-22 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jKysLKysnGzM" alt="" /></div>
                        </form>
                        <!-- Aweber Form Code -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
                  <div class="f-24 f-md-30 w600 text-center lh140">
                     <span>Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" alt="Jvzoo"/>
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/384700" class="f-22 f-md-30 w700 mx-auto" target="_blank">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Technology-Section -->
      <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-24 f-md-34 lh140 w600 text-center white-clr">
                     YoDrive is backed by the same technology that has <br class="d-none d-md-block">
                     till now smartly  Served... 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="d-flex align-items-center">
                           <img src="assets/images/cloud.webp" alt="cloud" class="img-fluid mr20">
                           <div class="f-md-34 f-24 w700 text-capitalize white-clr">69Million+
                              <br><span class="f-18 f-md-20 w500">No. of File Views /Downloads</span>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="assets/images/play.webp" alt="play" class="img-fluid mr20">
                           <div class="f-md-34 f-24 w700 text-capitalize white-clr">48Million+
                              <br><span class="f-18 f-md-20 w500">No. Of Video Plays In Minutes</span>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="d-flex align-items-center mt20 mt-md0">
                           <img src="assets/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                           <div class="f-md-34 f-24 w700 text-capitalize white-clr">342750
                              <br><span class="f-18 f-md-20 w500">No. Of Files Hosted</span>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="assets/images/portfolio.webp" alt="Protfolio" class="img-fluid mr20">
                           <div class="f-md-34 f-24 w700 text-capitalize white-clr">26197
                              <br><span class="f-18 f-md-20 w500">Total Businesses Created</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Technology-Section-End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-44 f-28 black-clr text-center w800 lh140">
                     This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
               <div class="col-12 mt-md60 mt30">
                  <div class="row gx-5">
                     <div class="col-md-6 col-12">
                        <div class="exciting-launch-left">
                           <div class="el-inner-left">
                              <div class="p20">
                                 <div class="f-md-40 f-24 lh140 w700 text-center">
                                    Pre Launch
                                 </div>
                                 <div class="f-md-25 f-20 w500 lh140 text-center">
                                    (with Webinar)
                                 </div>
                              </div>
                              <div class="exciting-date">
                                 <div class="f-20 f-md-28 w700 lh140 d-flex justify-content-around white-clr">
                                    1st August'22    
                                    <span class="f-18 f-md-24 w400">10:00 AM EST to</span>
                                 </div>
                                 <div class="f-20 f-md-28 w700 lh140 d-flex justify-content-around mt20  white-clr">
                                    3rd August'22   
                                    <span class="f-18 f-md-24 w400">10:00 AM EST</span>
                                 </div>
                              </div>
                              <div class="exciting-commission">
                                 <div class="f-md-23 f-20 lh140 w700 text-center">
                                    To Make You Max Commissions
                                 </div>
                                 <ul class="exciting-list f-18 f-md-19 w500 lh140 pl0">
                                    <li>All Leads Are Hardcoded</li>
                                    <li>Exciting $2000 Pre-Launch Contest</li>
                                    <li>We'll Re-Market Your Leads Heavily</li>
                                    <li>Pitch Bundle Offer on webinars.</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt-md0 mt20 black-clr">
                        <div class="exciting-launch-right">
                           <div class="el-inner-right">
                              <div class="p20">
                                 <div class="f-md-40 f-24 lh140 w700 text-center">
                                    7 Days Launch <br class="d-none d-md-block">Event
                                 </div>
                                 <!-- <div class="f-md-25 f-20 w500 lh140 text-center">
                                    (with Webinar)
                                    </div> -->
                              </div>
                              <div class="exciting-date-right text-center">
                                 <div class="f-22 f-md-28 w700 lh140 white-clr">
                                    Cart Opens 3<sup>rd</sup> August  <br>
                                    <span class="f-20 f-md-24 w400">at 11:00 AM EST</span>
                                 </div>
                                 <div class="f-22 f-md-28 w700 lh140 mt20  white-clr">
                                    Cart Closes 9<sup>th</sup> August <br>
                                    <span class="f-20 f-md-24 w400">11:59 PM EST</span>
                                 </div>
                              </div>
                              <div class="exciting-commission">
                                 <div class="f-md-23 f-20 lh140 w700 text-center">
                                    Big Opening Contest & Bundle Offer
                                 </div>
                                 <ul class="exciting-list f-18 f-md-19 w500 lh140 pl0">
                                    <li>High in Demand Product with Top Conversion</li>
                                    <li>Deep Funnel to Make You Double-Digit EPCs</li>
                                    <li>Earn up to $354/Sale</li>
                                    <li>Huge $10K JV Prizes</li>
                                    <li>We'll Re-Market Your Visitors Heavily</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-40 f-24 lh140 w700 black-clr heading-design">
                     Hello Awesome JV’s 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-18 f-md-20 lh140 w400 black-clr">
                           It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner  Atul Pareek (Internet Marketer & JV Manager). 
                           <br><br>
                           We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates. 
                           <br><br>
                           With the combined experience of 25+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for all your cloud storage, media content delivery and video hosting needs under one dashboard. <br><br>
                           Check out the incredible features of this amazing technology that will blow away your mind. And we guarantee that this offer will convert like Hot Cakes starting from 3rd August'22 at 11:00 AM EST! Get Ready!!   
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center" data-aos="fade-left">
                        <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto mt20">
                        
                        <img src="assets/images/atul-parrek.webp" class="img-fluid d-block mx-auto mt20">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20">
                  <div class="awesome-feature-shape">
                     <div class="row gx-0">
                        <div class="col-12 f-md-25 f-20 lh140 w700 text-center mb20 mb-md30">
                           Also, here are some stats from our previous launches:
                        </div>
                        <div class="col-12 col-md-6 f-16 f-md-20 lh140 w600">
                           <ul class="awesome-list">
                              <li>Over 100 Pick of The Day Awards</li>
                              <li>Over $4Mn In Affiliate Sales for Partners</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 f-16 f-md-20 lh140 w600 mt10 mt-md0">
                           <ul class="awesome-list">
                              <li>Top 10 Affiliate & Seller (High Performance Leader)</li>
                              <li>Always in Top-10 of JVZoo Top Sellers</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w700 lh140 white-clr">
                  Presenting…
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:100px">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"/>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"/>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"/>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"/>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"/>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"/>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"/>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"/>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"/>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"/>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"/>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  Amazing Cloud-Based Platform to Host, Manage and Deliver Unlimited Images, Files, and Videos at Lightning-Fast Speed with Zero Tech Hassles!  
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="d-flex justify-content-md-start justify-content-center gap-4">
                     <div class="mt5"><img src="assets/images/n1.png"></div>
                     <div class="f-16 f-md-18 lh140 w400 white-clr">
                        <span class="w600 f-22 f-md-26 lh150">Unlimited Everything</span> <br>
                        Host, Manage & Publish Unlimited PDFs, Docs, Audios, Videos, Zip Files or Any Other Marketing File
                     </div>
                  </div>
                  <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                     <div class="mt5"><img src="assets/images/n3.png"></div>
                     <div class="f-16 f-md-18 lh140 w400 white-clr">
                        <span class="w600 f-22 f-md-26 lh150">Robust & Proven Solution</span> <br>
                        Powerful Battle-Tested Architecture Happily Serving 133 Million+ Marketing Files
                     </div>
                  </div>
                  <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                     <div class="mt5"><img src="assets/images/n5.png"></div>
                     <div class="f-16 f-md-18 lh140 w400 white-clr">
                        <span class="w600 f-22 f-md-26 lh150">50+ More Cool Features In Store</span> <br>
                        We’ve Left No Stone Unturned To Give You An Unmatched Experience
                     </div>
                  </div>
                  <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                     <div class="mt5"><img src="assets/images/n2.png"></div>
                     <div class="f-16 f-md-18 lh140 w400 white-clr">
                        <span class="w600 f-22 f-md-26 lh150">Fast & Easy</span> <br>
                        Deliver All Your Files at Lightning Fast Speed with Fast CDNs
                     </div>
                  </div>
                  <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                     <div class="mt5"><img src="assets/images/n4.png"></div>
                     <div class="f-16 f-md-18 lh140 w400 white-clr">
                        <span class="w600 f-22 f-md-26 lh150">No Worries of Paying Monthly</span> <br>
                        During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- FEATURE LIST SECTION START -->
      <div class="feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     YoDrive Is Packed with GROUND - BREAKING Features That Makes It A Cut Above The Rest
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Store & Manage Video Training, Website Images, PDFs, Docs, Audios or Any File.
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  Supercharge Your Websites, Landing Pages, Training & Client Projects By Delivering Images & Files At Lightning Fast Speed
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Accurate Analysis to Know Your User
                  Engagement On Your Files & Folders
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  Publish Training, Files & Folders On Engaging Doc Sharing Channels Those Have Protected
                  Elegant & SEO Optimized Sharing Pages
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="feature-list">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center white-clr">
                  YoDrive Has TONS Of Other Innovative & Never  <br class="d-none d-md-block">Seen Before Features
               </div>
            </div>
            <div class="row gx-5">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr1.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Speed-Up Your Website Speed with <span class="w600">Fast Loading &
                        Optimized Images</span> 
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr2.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Impress Your Customers With <span class="w600">
                        Lightning Fast Trainings Videos & PDF Docs </span> 
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr3.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Manage & Share <span class="w600">Multiple Files & Folders Effortlessly</span></p>
                  </div>
               </div>
            </div>
            <div class="row gx-5">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr4.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Online Back-Up & 30 days File recovery</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr5.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Unbreakable File Security with 
                        <spa class="w600">SSL & OTP Enabled Login</spa>
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block">
                     <p class="description"> <span class="w600">Add Your LOGO on Your Doc Channel</span> To Share Files On Your Branded Pages</p>
                  </div>
               </div>
            </div>
            <div class="row gx-5">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Business Cloud, <span class="w600">Access Files
                        Anytime, Anywhere</span>
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Get Unlimited Viral Traffic & Leads <span class="w600">from File Sharing Page</span> </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Single dashboard <span class="w600">To Manage All Type Of Files -</span>  No Need To Buy Multiple Apps.</p>
                  </div>
               </div>
            </div>
            <div class="row gx-5">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr10.webp" class="img-fluid mx-auto d-block">
                     <p class="description w600">Personal & Business YoDrive</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr11.webp" class="img-fluid mx-auto d-block">
                     <p class="description"> <span class="w600">Track every visitor</span>  on your sharing pages, Segment them according to their behaviour</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr12.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Advanced Integrations <span class="w600">with AR, webinar’s, CRM, pixabay & other file storage services.</span> </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w700 lh140 text-center">
                     <span class="orange-clr">Watch The Demo</span>
                     <br class="d-none d-md-block">
                     Discover How Easy & Powerful It Is
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
                  <!--<img src="assets/images/demo-video-poster.png" class="img-fluid d-block mx-auto">-->
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://yodrive.dotcompal.com/video/embed/c175p67cfh" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> 
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     Here’s a List of All Potential Use Cases<br>
                     YoDrive Can Be Used For...
                  </div>
               </div>
            </div>
            <div class="row gx-5">
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-1.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Digital/ Ecom Products
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-3.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling DFY Templates, Themes, Plugins
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-5.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Running Paid Ads For Lead Generation
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-7.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Recurring Memberships
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-2.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-4.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     All Info Product Sellers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-6.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Delivering Docs & Video Trainings To Customers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-8.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Freelancing Services
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-9.png" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling High Ticket Coachings/ Product Webinars
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-72 f-40 w800 text-center orange-clr lh140">
                  $12000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt20 text-center">
                  Contest Starts on 1st August’22 at 10:00 AM EST and Ends on 3rd August’22 at 10:00 AM EST
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt10 text-center">
                  <!-- (Get Flat <span class="w700">$0.50c</span> For Every Lead You Send for <span class="w700">Pre-Launch Webinar</span>)  -->
                  (Get Flat <span class="w700"> $0.50c </span> For Every Lead You Send for <span class="w700"> Pre-Launch Webinar)</span>
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/contest-img1.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="assets/images/contest-img2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt-md70 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads)
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/prize-img1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <img src="assets/images/prize-img2.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w600 lh100 mt20 mt-md50 mb10 mb-md20 black-clr">
                  *Contest Policies:
               </div>
               <div class="col-12 f-16 f-md-20 w400 lh140">
                  1. Team of a maximum of two is allowed. <br> <span class="mt10 d-block">
                  2. To be eligible to win one of the sales leaderboard prizes, you must have made commissions equal to or greater than the value of the prize. If this criterion is not met, then you will be eligible for the next prize.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md55">
                  <img src="assets/images/prize-img3.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-16 f-md-20 w400 lh140">
                     <span class="w600 d-md-block">Note:</span>
                     We will announce the winners on 11th August & Prizes will be distributed through Payoneer from 12th August Onwards.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 black-clr">
                     Our Solid Track Record of <br class="d-md-block d-none"> <span class="orange-clr"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 orange-clr">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs.<br><br>  
                  So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are just some results from our recent promotions. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     <span class="orange-clr">  Have any Query?</span> Contact us Anytime
                  </div>
               </div>
            </div>
            <div class="row mt50 mt-md70 ">
               <div class="col-md-4 col-12 text-center mx-auto">
                  <div class="contact-shape">
                     <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                     <div class="f-26 f-md-32 w700 lh140 text-center black-clr">
                        Dr Amit Pareek
                     </div>
                     <div class="f-15 w500 lh140 text-center black-clr">
                        (Techpreneur & Marketer)
                     </div>
                     <div class="col-12 mt30 d-flex justify-content-center">
                        <a href="skype:amit.pareek77" class="link-text mr20">
                           <div class="col-12 ">
                              <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                           </div>
                        </a>
                        <a href="http://facebook.com/Dr.AmitPareek" class="link-text">
                        <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                        </a>
                     </div>
                  </div>
               </div>
              
               <div class="col-md-4 col-12 text-center mt50 mt-md0 mx-auto">
                  <div class="contact-shape">
                     <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                     <div class="f-26 f-md-32 w700  lh140 text-center black-clr">
                        Atul Pareek
                     </div>
                     <div class="f-15 w500  lh140 text-center black-clr">
                        (Entrepreneur & JV Manager)
                     </div>
                     <div class="col-12 mt30 d-flex justify-content-center">
                        <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text mr20">
                           <div class="col-12 ">
                              <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                           </div>
                        </a>
                        <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text">
                        <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center">
                  Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below: 
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list pl0 m0 f-16 lh140 w400">
                     <li>
                        Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No exceptions will be entertained.
                     </li>
                     <li>
                        Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed from our system with immediate effect.
                     </li>
                     <li>
                        Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link.
                     </li>
                     <li>
                        Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>
                        We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px;">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"/>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"/>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"/>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"/>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"/>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"/>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"/>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"/>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"/>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"/>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"/>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"/>
                  </svg>
                  <div class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © YoDrive</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    

      <?php
         } else {
         echo "Times Up";
         }
         ?>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-155455963').parentElement.removeAttribute('target');
    }
})();
</script>
<script type="text/javascript">
 
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-155455963")) {
                document.getElementById("af-form-155455963").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-155455963")) {
                document.getElementById("af-body-155455963").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-155455963")) {
                document.getElementById("af-header-155455963").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-155455963")) {
                document.getElementById("af-footer-155455963").className = "af-footer af-quirksMode";
            }
        }
    })();
 
</script>
   </body>
</html>
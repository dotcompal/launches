<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="YoDrive Bonuses">
      <meta name="description" content="YoDrive Bonuses">
      <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="YoDrive Bonuses">
      <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoDrive Bonuses">
      <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="twitter:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <title>YoDrive Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/yodrive/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/yodrive/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'August 03 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/10103/384700/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px"><defs><style>.cls-11{fill:#fff;}</style></defs><path class="cls-11" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path><path class="cls-11" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path><path class="cls-11" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path><path class="cls-11" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path><path class="cls-11" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path><path class="cls-11" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path><path class="cls-11" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path><path class="cls-11" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path><rect class="cls-11" x="241.72" y="25.45" width="8.53" height="6.34"></rect><path class="cls-11" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path><path class="cls-11" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path></svg>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 text-center">
                    <div class="preline f-18 f-md-21 w600 black-clr lh140">
                        <div class="preheadline">
                           Grab My 20 Exclusive Bonuses Before the Deal Ends…
                        </div>
                    </div>
                  </div>
                  <div class="col-12 mt-md50 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
                     Revealing All-in-One Platform To <span class="yellow-clr"> Host & Deliver, Website Images, Files, Training & Play HD Videos </span> at Lightning-Fast Speed At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                    <div class="postheadline">Watch My Quick Review Video</div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-12 col-md-9 mx-auto">
                  <!-- <img src="https://cdn.oppyo.com/launches/yodrive/special/product-image.webp" class="img-fluid d-block mx-auto" alt="Product Box"> -->
                  <div class="responsive-video">
                        <iframe src="https://yodrive.dotcompal.com/video/embed/8xba4prl6n" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                    </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <img src="https://cdn.oppyo.com/launches/yodrive/special/down-arrow.webp" class="img-fluid mx-auto d-block vert-move" alt="Arrow" style="margin-top:-70px;">

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="f-md-40 f-28 w400 lh140 text-center black-clr">
                     Captivate Your Audience with Your Media Content<br class="d-none d-md-block"><span class="w600"> In 3 Easy Steps...</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-md-4 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 1</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/upload.webp " class="img-fluid d-block" alt="step one">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Upload</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Simply drag and drop your files into YoDrive or upload from your PC. It supports almost all type of files - videos, Images, audios, documents etc.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 relative mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 2</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/code.webp " class="img-fluid d-block" alt="step two">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Get File URL</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        YoDrive gives you a single line of code to share your media anywhere after optimizing it according to internet speed &amp; make multiple resolutions for faster delivery on any device.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow1.webp " class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 3</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/publish.webp " class="img-fluid d-block" alt="step three">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Publish &amp; Profit</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Just paste the code on any page or get a secure DFY file sharing URL to publish your content anywhere online to start getting eyeballs and get paid.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“YODRIVE”</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">30&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">43</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w400 lh140 text-capitalize text-center black-clr">
                  “We Guarantee That This Will Be The   <span class="w600"> <br class="d-none d-md-block">Last Cloud Hosting App You’ll Ever Need”</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row header-list-block gx-md-5">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>
                              <span class="w600">Host and Manage All Your Files from One Easy Dashboard –</span> Videos, Web Images, PDFs, Docs, Audio, and Zip Files.
                           </li>
                           <li>
                              <span class="w600">Super-Fast Delivery (After All, Time Is Money!) - </span> This lead to More Engagement, Leads &amp; Sales. 
                           </li>
                           <li>
                              <span class="w600"> Play Sales, Demo &amp; Training Videos in HD</span>  on Any Page, Site, or Members Area.
                           </li>
                           <li>
                              <span class="w600">Tap Into HUGE E-Learning Industry - </span> Deliver Your Videos, Docs, and PDF Training on Done-For-You and Beautiful Doc Sharing Sites &amp; Pages. 
                           </li>
                           <li>
                              Use Our App to Help <span class="w600">Speed-Up Your Website, Landing Pages &amp; Online Shops with Fast Loading &amp; Optimized Images &amp; Videos. </span> 
                           </li>
                           <li>
                              Build Your Online Empire - <span class="w600">Deliver Digital Products Securely, Fast &amp; Easy 
                              </span>
                           </li>
                           <li>
                              <span class="w600">Generate Tons of Leads &amp; Affiliate Sales –  </span> Deliver Freebies &amp; Affiliate Bonuses to Your Subscribers Without a Hitch
                           </li>
                           <li>
                              <span class="w600">Share Your Files Privately </span>  with Your Clients, Customers &amp; Team Members.
                           </li>
                           <li>
                              <span class="w600">Free Hosting </span> Is Included, Unlimited Bandwidth &amp; Upto 250 GB Storage 
                           </li>
                           <li>
                              PLUS, <span class="w600">Limited Time Free Commercial License  </span> (only for today) to Serve Your Clients
                           </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>Elegant, <span class="w600">SEO Optimized &amp; 100% Mobile Responsive </span> File Share Pages </li>
                           <li>Inbuilt <span class="w600">Lead Management System </span></li>
                           <li><span class="w600">Access Files Anytime, Anywhere</span> Directly from The Cloud on Any Device!</li>
                           <li><span class="w600">Highly Encrypted Unbreakable File Security</span> with SSL &amp; OTP Enabled Login </li>
                           <li>30 Days <span class="w600">Online Back-up &amp; File recovery </span> So You Never Lose Your Precious Data and Files. </li>
                           <li><span class="w600">Single Dashboard</span> to Manage All Business Files- No Need to Buy Multiple Apps 
                           </li><li><span class="w600">Manage Files Effortlessly –</span> Share Multiple Files, Full Text Search &amp; File Preview </li>
                           <li>Inbuilt Elegant Video Player with <span class="w600">HDR Support</span> </li>
                           <li><span class="w600">Real-Time Analytics </span>for Every Single File You Upload - See Downloads, Shares, etc.  </li>
                           <li><span class="w600">Easy and Intuitive</span> to Use Software with <span class="w600">Step-by-Step Video Training </span></li>
                           <li><span class="w600">100% Newbie Friendly</span> &amp; Fully Cloud Based Software </li>
                           <li><span class="w600">Live Chat -</span> Customer Support So You Never Get Stuck or Have any Issues </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-24 f-md-40 w400 lh140 text-capitalize text-center black-clr col-12">
                  YoDrive Is So Powerful that We Are Personally Using It  <br class="d-none d-md-block">
                  <span class="f-28 f-md-42 w600">To Run Our 7-Figure Online Business Without A Hitch! </span>
               </div>
               <div class="col-12 f-md-24 f-18 w400 lh140 black-clr text-center mt20 mt-md30">
                  And as a matter of fact, every single video and image on this sales page is powered by YoDrive.  
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof.webp" class="img-fluid d-block mx-auto">
               </div>
             
               <div class="col-12 f-md-36 f-22 w600 lh140 black-clr text-center mt20 mt-md50">
                  We Are Using YoDrive For Our Product Launches and In  <br class="d-none d-md-block">
                  Affiliate Promos and Generated $700K in Last 6 Months  
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!--Technology Section-->
      <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                     YoDrive is a Proven Solution Backed by the same Technology that has Happily Served...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="d-flex align-items-center">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                           <div class=" text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 69Million+
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. of File Views /Downloads
                              </div>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/play.webp" alt="play" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 48Million+
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. Of Video Plays In Minutes
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="d-flex align-items-center mt20 mt-md0">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 342750
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. Of Files Hosted
                              </div>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/portfolio.webp" alt="Protfolio" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 26197
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 Total Businesses Created
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70">
               <div class="f-24 f-md-36 lh140 w600 text-center white-clr">
                  It's Totally MASSIVE.
               </div>
               <img src="https://cdn.oppyo.com/launches/yodrive/special/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto">
               <div class="mt10 f-16 f-md-18 lh140 w400 text-center white-clr">
                  And now it's your turn to power your own business and media without ever worrying about a hefty price tag for cloud storage during this launch special deal.
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“YODRIVE”</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">08&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">11</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer-End -->
            </div>
         </div>
      </div>
      <!--Technology Section End-->

      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-40 f-24 lh140 w400 text-center black-clr">Check Out What Our Beta Testers Are <br class="d-none d-md-block"><span class="f-md-45 f-28 w600">Saying About YoDrive!</span>  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-md-6 p-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t1.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t2.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
               </div>
               <div class="col-md-6 p-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t3.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t4.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <div class="letsface-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <div class="letsface f-28 f-md-45 w600 black-clr lh140">
                        Let’s Face It…
                     </div>
                  </div>
                  <div class="f-18 f-md-20 w400 text-center black-clr lh150 mt20 mt-md30">
                     Showcasing Your Product and Service Through Images &amp; Videos Over the Internet is Need of the Hour
                  </div>
                  <div class="row mt20 mt-md50">
                     <div class="col-12 col-md-7 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/conversion.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Having Media Content Like On Your Website <span class="w600"> Boosts Conversions And Sales By 80% </span>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="col-12 col-md-5 relative">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow.webp" class="img-fluid d-none mx-auto letsface-arrow d-md-block">
                     </div> -->
                  </div>
                  <div class="row mt-md30 mt20">
                     <!-- <div class="col-12 col-md-5 order-md-1 relative">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow1.webp" class="img-fluid d-none d-md-block mx-auto letsface-arrow1">
                     </div> -->
                     <div class="col-12 col-md-7 order-md-2 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/website.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Videos Help You Get <span class="w600"> 41% More Web Traffic From Search Engines. </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-md30 mt20">
                     <div class="col-12 col-md-7 order-md-2 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/click.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Videos Increases Your <span class="w600"> Email Click-Through <br class="d-none d-md-block">Rates By 200-300%  </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <!-- Profit Business -->
      <div class="profit-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w400 black-clr text-center">
                     <span class="w600">Each &amp; Every Profitable Business Online, Has This as Its Backbone.</span>
                     And… If You Want to Run a <br class="d-none d-md-block">Fine-Tuned Business, You'll Need It Too!"
                  </div>
                  <div class="mt20 f-18 f-md-20 lh140 w400 text-center black-clr">
                     Let me show you how BIG companies and TOP marketers are using media content secretly<br class="d-none d-md-block"> to get tons of sales, leads, and better customer satisfaction 
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Top Marketers &amp; Marketing Giants Are Using Videos On Their Landing Pages To Boost Their Sales &amp; Profits.               
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow1.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: relative; top: 115px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/sales.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Even The Huge $398 Billion E-Learning Industry Is Served Using Media Files
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     Top E-Learning Sites Like Udemy, Udacity, etc., Are Using Videos and Other Media Files To Sell &amp; Deliver Courses.
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: relative; top: 70px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/udemy.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     See How World’s BIGGEST E-com Companies Like Amazon, BestBuy Show Product Demo Videos To Increase Their Sales Drastically.                           
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow1.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: relative; top: 50px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/e-com-image.webp" class="img-fluid d-block mx-auto" alt="E-com Companies">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Supercharging Websites, Landing Pages &amp; Blogs              
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     With Ultra-Fast Loading Images To Increase Followership, Reduce Bounce Rates And Enjoy Higher Search Engine Rankings
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: relative; top: 50px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/supercharging.webp" class="img-fluid d-block mx-auto" alt="Supercharging Websites">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Mobile Apps Are No Different            
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     As People Spend Lots of Time Online Watching Videos &amp; Engaging Content.
                  </div>                  
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/mobileapps.webp" class="img-fluid d-block mx-auto" alt="Mobile Apps">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12">
                  <div class="f-22 f-md-28 w600 lh140 black-clr text-center">
                     Affiliates Are Generating More Leads Using Fast Loading Lead Pages, Delivering Freebies, Bonuses &amp; Review Videos and Earning BIG Commissions            
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/affiliates-img.webp" class="img-fluid d-block mx-auto" alt="Affiliates">
               </div>
            </div>
         </div>
      </div>
      <!-- Profit Business End-->

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w600 lh140 white-clr">
                  Presenting…
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:100px">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  Amazing Cloud-Based Platform to Host, Manage and Deliver Unlimited Images, Files, and Videos at Lightning-Fast Speed with Zero Tech Hassles!  
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="proudly-list-bg">
                     <div class="col-12">
                        <ul class="proudly-list f-18 w500 lh140 black-clr">
                           <li>
                              <span class="f-22 f-md-24 w600">Unlimited Everything </span>
                              <div class="f-18 f-md-18 w400 mt5">Host, Manage &amp; Publish Unlimited PDFs, Docs, Audios, Videos, Zip Files or Any Other Marketing File.
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Robust &amp; Proven Solution</span>
                              <div class="f-18 f-md-18 w400 mt5">Powerful Battle-Tested Architecture Happily Serving 133 Million+ Marketing Files
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">50+ More Cool Features In Store</span>
                              <div class="f-18 f-md-18 w400 mt5">We’ve Left No Stone Unturned To Give You An Unmatched Experience
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Fast &amp; Easy</span>
                              <div class="f-18 f-md-18 w400 mt5">Deliver All Your Files at Lightning Fast Speed with Fast CDNs
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">No Worries of Paying Monthly</span>
                              <div class="f-18 f-md-18 w400 mt5">During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End   -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase YoDrive, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Simple Membership Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Want a step by step plan to launch a product?
                           </li>
                           <li>
                              In this course, you’ll get four live sessions taught by an experienced marketer getting your product up and running quickly.
                           </li>
                           <li>
                              Just use this mind-blowing package with <span class="w600">YoDrive</span> and scale your business to the next level.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                           QikMobi Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              One of the core concepts in the digital marketing industry is the sales funnel but sales funnels for most online businesses are actually very complex.
                           </li>
                           <li>
                              This bonus package that will train your subscribers to use sales funnels right and get lots of money to your business.
                           </li>
                           <li>
                              When combined with <span class="w600">YoDrive,</span>  it enhances your marketing results & helps you become the market pro.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Instant Squeeze Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>JVZoo has been one of the best choices for many online entrepreneurs that sold digital products.</li>
                           <li>In this video tutorial, you will learn how to set up an effective sales funnel that converts and will really make you profit.</li>
                           <li>Using this package with <span class="w600">YoDrive</span> will turn your pages and funnels into highly professional attention magnets without spending a fortune.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           WP Video Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              A sales letter can add a personal touch to advertising which is not possible in other forms of publicity and advertising.
                           </li>
                           <li>
                              In this bonus package, you’ll get 100 Sales letters, Guarantee pages, Set of JV letters, squeeze pages and many more that will save you thousands of dollars without talking to a single copywriter.
                           </li>
                           <li>
                              Use it with <span class="w600">YoDrive</span> and turn your squeeze pages into high-converting and effective pages that generate lots of new subscribers.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Instant Content Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Every online business owners and affiliate marketers want to make out of the traffic that they generated to their website but driving traffic to your website is sometimes time-consuming and involves many issues.
                           </li>
                           <li>
                              With this awesome bonus package you can easily motivate your website visitors to buy your products.
                           </li>
                           <li>
                              By combining this package with powerful <span class="w600">YoDrive,</span> you will reap out maximum benefits from your product launches.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“YODRIVE”</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Course Engagement Hacks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Cross-selling involves offering the customer a related product or service. This is one of the best method of marketing other relevant products.
                           </li>
                           <li>
                              You can easily boost your sales with no extra work using the cross sell slider pro desktop software.
                           </li>
                           <li>
                              Now stop thinking and use this with funnel marketing prowess of <span class="w600">YoDrive</span>  to take your business to the next level.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Internet Business Startup Kit Advanced
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Custom logo designs are expensive, and you never know what you get.
                           </li>
                           <li>
                              To reduce the risk, we are giving you a package of ultra-high-quality, brand-new logo templates that will save you thousands of dollars.
                           </li>
                           <li>
                           Combine this package with <span class="w600">YoDrive</span>  to get even more sales and make even more profits from your online business.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Affiliate Marketing Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              If you are a graphic designer or website owner, abstract images are really useful to your own project or to your client's marketing campaign.
                           </li>
                           <li>
                              The good news though is that inside this product is a bundle of high-quality and beautifully designed abstract images that you can use or re-sell today.
                           </li>
                           <li>
                           While using <span class="w600">YoDrive,</span> integrate this with your overall marketing strategy & boost your brand-building efforts in the long run.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Find Your Niche
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Any copy you use in your marketing is a reflection on your company. An effective copywriting have some secrets.
                           </li>
                           <li>
                           With this bonus package, you can learn the deepest, darkest secrets to the Internet's most sought after copywriters who have generated thousands of dollars for every single page they write.
                           </li>
                           <li>Now all you need to do is create your own brand and use lansing page and funnel builder <span class="w600">YoDrive</span> to zoom past your competitors at will.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Create Your Own Job
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Answering queries of your customers is very important if you don’t want them to leave your website.
                           </li>
                           <li>
                           Don’t worry as this chat window works just like a real live chat, but the responses are all fully automated, so you get the benefits of live chat, without having to actually provide live support.
                           </li>
                           <li>
                           This bonus when combined with <span class="w600">YoDrive,</span> becomes an ultimate growth booster for business owners.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“YODRIVE”</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">14</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Work From Home Productivity Video
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Are you trying to find the target consumers for your product or striving to reach out to potential consumers?</li>
                           <li>If yes, then here is a Golden opportunity awaiting you. This video course will enable you to learn a large number of creative and unique ways of lead generation both offline as well as online.</li>
                           <li>Now use this amazing video course along with <span class="w600">YoDrive</span> to intensify your business growth like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Mobile eCommerce Simplified
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Affiliate marketing is a key strategy for every business industry to prosper. But there are more chances to fail if you don’t apply the accurate affiliate marketing strategy.</li>
                           <li>Don’t worry! The affiliate marketing kit solves the problems and hindrances faced by newbies and veteran affiliate marketers.</li>
                           <li>When used with <span class="w600">YoDrive,</span> this package will surely become a top-notch business booster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Email Buzz
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Want to start making money with private label content even if you don't have a website of your own?</li>
                        <li>Now that’s possible! With this report, you will learn fast and easy ways to maximize your profit with PLR.</li>
                        <li>
                        This package is a must-have and when combined with powerful marketing technology <span class="w600">YoDrive,</span> reaps great results for you in the long run.
                        </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        EBook Jackpot Video Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>For being successful, you need to study what successful people do and model their actions.</li>
                           <li>With this software, skyrocket your conversions, sales and profits with your own automated sales assistants, providing 24/7 support on all your websites.</li>
                           <li>This bonus when combined with <span class="w600">YoDrive</span> proves to be a great resource for every success hungry marketer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        100 Software Creation Ideas
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It’s a fact that people love good customer support and that’s why your business need it.</li>
                           <li>With xyber email assistant software’s help you can easily do your customer support with ease using xyber email assistant!</li>
                           <li>So, get in active mode and use this bonus with <span class="w600">YoDrive</span> to intensify your growth prospects like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“YODRIVE”</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">14</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Social Media Automation
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Are you trying to find the target consumers for your product or striving to reach out to potential consumers?</li>
                           <li>If yes, then here is a Golden opportunity awaiting you. This video course will enable you to learn a large number of creative and unique ways of lead generation both offline as well as online.</li>
                           <li>Now use this amazing video course along with <span class="w600">YoDrive</span> to intensify your business growth like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Facebook Ads 101
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Affiliate marketing is a key strategy for every business industry to prosper. But there are more chances to fail if you don’t apply the accurate affiliate marketing strategy.</li>
                           <li>Don’t worry! The affiliate marketing kit solves the problems and hindrances faced by newbies and veteran affiliate marketers.</li>
                           <li>When used with <span class="w600">YoDrive,</span> this package will surely become a top-notch business booster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        CB Niche Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Want to start making money with private label content even if you don't have a website of your own?</li>
                           <li>Now that’s possible! With this report, you will learn fast and easy ways to maximize your profit with PLR.</li>
                           <li>This package is a must-have and when combined with powerful marketing technology <span class="w600">YoDrive,</span> reaps great results for you in the long run.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Web Baycom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>For being successful, you need to study what successful people do and model their actions.</li>
                           <li>With this software, skyrocket your conversions, sales and profits with your own automated sales assistants, providing 24/7 support on all your websites.</li>
                           <li>This bonus when combined with <span class="w600">YoDrive</span> proves to be a great resource for every success hungry marketer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Turbo ECom + Addon PRO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It’s a fact that people love good customer support and that’s why your business need it.</li>
                           <li>With xyber email assistant software’s help you can easily do your customer support with ease using xyber email assistant!</li>
                           <li>So, get in active mode and use this bonus with <span class="w600">YoDrive</span> to intensify your growth prospects like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 orange-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 orange-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 orange-clr">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoDrive + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px;">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                  </svg>
                  <div class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © YoDrive</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
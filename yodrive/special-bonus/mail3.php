<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="YoDrive Bonuses">
      <meta name="description" content="YoDrive Bonuses">
      <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="YoDrive Bonuses">
      <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoDrive Bonuses">
      <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="twitter:image" content="https://www.yodrive.co/special-bonus/thumbnail.png">
      <title>YoDrive Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/yodrive/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/yodrive/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'August 03 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/47069/384700/';
         $_GET['name'] = 'Pranshu';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px"><defs><style>.cls-11{fill:#fff;}</style></defs><path class="cls-11" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path><path class="cls-11" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path><path class="cls-11" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path><path class="cls-11" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path><path class="cls-11" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path><path class="cls-11" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path><path class="cls-11" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path><path class="cls-11" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path><path class="cls-11" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path><rect class="cls-11" x="241.72" y="25.45" width="8.53" height="6.34"></rect><path class="cls-11" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path><path class="cls-11" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path></svg>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 text-center">
                    <div class="preline f-18 f-md-21 w600 black-clr lh140">
                        <div class="preheadline">
                           Grab My 20 Exclusive Bonuses Before the Deal Ends…
                        </div>
                    </div>
                  </div>
                  <div class="col-12 mt-md50 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
                  Revealing All-in-One Platform To <span class="yellow-clr"> Host & Deliver, Website Images, Files, Training & Play HD Videos </span> at Lightning-Fast Speed At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                    <div class="postheadline">Watch My Quick Review Video</div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-12 col-md-9 mx-auto">
                  <!-- <img src="https://cdn.oppyo.com/launches/yodrive/special/product-image.webp" class="img-fluid d-block mx-auto" alt="Product Box"> -->
                  <div class="responsive-video">
                     <iframe src="https://yodrive.dotcompal.com/video/embed/8xba4prl6n" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <img src="https://cdn.oppyo.com/launches/yodrive/special/down-arrow.webp" class="img-fluid mx-auto d-block vert-move" alt="Arrow" style="margin-top:-70px;">

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="f-md-40 f-28 w400 lh140 text-center black-clr">
                     Captivate Your Audience with Your Media Content<br class="d-none d-md-block"><span class="w600"> In 3 Easy Steps...</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-md-4 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 1</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/upload.webp " class="img-fluid d-block" alt="step one">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Upload</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Simply drag and drop your files into YoDrive or upload from your PC. It supports almost all type of files - videos, Images, audios, documents etc.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 relative mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 2</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/code.webp " class="img-fluid d-block" alt="step two">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Get File URL</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        YoDrive gives you a single line of code to share your media anywhere after optimizing it according to internet speed &amp; make multiple resolutions for faster delivery on any device.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow1.webp " class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 3</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/publish.webp " class="img-fluid d-block" alt="step three">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Publish &amp; Profit</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Just paste the code on any page or get a secure DFY file sharing URL to publish your content anywhere online to start getting eyeballs and get paid.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-12 f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 yellow-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">30&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">43</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w400 lh140 text-capitalize text-center black-clr">
                  “We Guarantee That This Will Be The   <span class="w600"> <br class="d-none d-md-block">Last Cloud Hosting App You’ll Ever Need”</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row header-list-block gx-md-5">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>
                              <span class="w600">Host and Manage All Your Files from One Easy Dashboard –</span> Videos, Web Images, PDFs, Docs, Audio, and Zip Files.
                           </li>
                           <li>
                              <span class="w600">Super-Fast Delivery (After All, Time Is Money!) - </span> This lead to More Engagement, Leads &amp; Sales. 
                           </li>
                           <li>
                              <span class="w600"> Play Sales, Demo &amp; Training Videos in HD</span>  on Any Page, Site, or Members Area.
                           </li>
                           <li>
                              <span class="w600">Tap Into HUGE E-Learning Industry - </span> Deliver Your Videos, Docs, and PDF Training on Done-For-You and Beautiful Doc Sharing Sites &amp; Pages. 
                           </li>
                           <li>
                              Use Our App to Help <span class="w600">Speed-Up Your Website, Landing Pages &amp; Online Shops with Fast Loading &amp; Optimized Images &amp; Videos. </span> 
                           </li>
                           <li>
                              Build Your Online Empire - <span class="w600">Deliver Digital Products Securely, Fast &amp; Easy 
                              </span>
                           </li>
                           <li>
                              <span class="w600">Generate Tons of Leads &amp; Affiliate Sales –  </span> Deliver Freebies &amp; Affiliate Bonuses to Your Subscribers Without a Hitch
                           </li>
                           <li>
                              <span class="w600">Share Your Files Privately </span>  with Your Clients, Customers &amp; Team Members.
                           </li>
                           <li>
                              <span class="w600">Free Hosting </span> Is Included, Unlimited Bandwidth &amp; Upto 250 GB Storage 
                           </li>
                           <li>
                              PLUS, <span class="w600">Limited Time Free Commercial License  </span> (only for today) to Serve Your Clients
                           </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>Elegant, <span class="w600">SEO Optimized &amp; 100% Mobile Responsive </span> File Share Pages </li>
                           <li>Inbuilt <span class="w600">Lead Management System </span></li>
                           <li><span class="w600">Access Files Anytime, Anywhere</span> Directly from The Cloud on Any Device!</li>
                           <li><span class="w600">Highly Encrypted Unbreakable File Security</span> with SSL &amp; OTP Enabled Login </li>
                           <li>30 Days <span class="w600">Online Back-up &amp; File recovery </span> So You Never Lose Your Precious Data and Files. </li>
                           <li><span class="w600">Single Dashboard</span> to Manage All Business Files- No Need to Buy Multiple Apps 
                           </li><li><span class="w600">Manage Files Effortlessly –</span> Share Multiple Files, Full Text Search &amp; File Preview </li>
                           <li>Inbuilt Elegant Video Player with <span class="w600">HDR Support</span> </li>
                           <li><span class="w600">Real-Time Analytics </span>for Every Single File You Upload - See Downloads, Shares, etc.  </li>
                           <li><span class="w600">Easy and Intuitive</span> to Use Software with <span class="w600">Step-by-Step Video Training </span></li>
                           <li><span class="w600">100% Newbie Friendly</span> &amp; Fully Cloud Based Software </li>
                           <li><span class="w600">Live Chat -</span> Customer Support So You Never Get Stuck or Have any Issues </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-24 f-md-40 w400 lh140 text-capitalize text-center black-clr col-12">
                  YoDrive Is So Powerful that We Are Personally Using It  <br class="d-none d-md-block">
                  <span class="f-28 f-md-42 w600">To Run Our 7-Figure Online Business Without A Hitch! </span>
               </div>
               <div class="col-12 f-md-24 f-18 w400 lh140 black-clr text-center mt20 mt-md30">
                  And as a matter of fact, every single video and image on this sales page is powered by YoDrive.  
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof.webp" class="img-fluid d-block mx-auto">
               </div>
             
               <div class="col-12 f-md-36 f-22 w600 lh140 black-clr text-center mt20 mt-md50">
                  We Are Using YoDrive For Our Product Launches and In  <br class="d-none d-md-block">
                  Affiliate Promos and Generated $700K in Last 6 Months  
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!--Technology Section-->
      <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                     YoDrive is a Proven Solution Backed by the same Technology that has Happily Served...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row">
                  <div class="col-md-6">
                        <div class="d-flex align-items-center">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                           <div class=" text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 69Million+
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. of File Views /Downloads
                              </div>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/play.webp" alt="play" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 48Million+
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. Of Video Plays In Minutes
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="d-flex align-items-center mt20 mt-md0">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 342750
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 No. Of Files Hosted
                              </div>
                           </div>
                        </div>
                        <div class="d-flex align-items-center mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/portfolio.webp" alt="Protfolio" class="img-fluid mr20">
                           <div class="text-capitalize ">
                              <div class="f-md-42 f-24 w700 yellow-clr">
                                 26197
                              </div>
                              <div class="f-18 f-md-20 w500 white-clr">
                                 Total Businesses Created
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70">
               <div class="f-24 f-md-36 lh140 w600 text-center white-clr">
                  It's Totally MASSIVE.
               </div>
               <img src="https://cdn.oppyo.com/launches/yodrive/special/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto">
               <div class="mt10 f-16 f-md-18 lh140 w400 text-center white-clr">
                  And now it's your turn to power your own business and media without ever worrying about a hefty price tag for cloud storage during this launch special deal.
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-12 f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 yellow-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">08&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">11</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer-End -->
            </div>
         </div>
      </div>
      <!--Technology Section End-->
      
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-40 f-24 lh140 w400 text-center black-clr">Check Out What Our Beta Testers Are <br class="d-none d-md-block"><span class="f-md-45 f-28 w600">Saying About YoDrive!</span>  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-md-6 p-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t1.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t2.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
               </div>
               <div class="col-md-6 p-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t3.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/t4.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <div class="letsface-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <div class="letsface f-28 f-md-45 w600 black-clr lh140">
                        Let’s Face It…
                     </div>
                  </div>
                  <div class="f-18 f-md-20 w400 text-center black-clr lh150 mt20 mt-md30">
                     Showcasing Your Product and Service Through Images &amp; Videos Over the Internet is Need of the Hour
                  </div>
                  <div class="row mt20 mt-md50">
                     <div class="col-12 col-md-7 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/conversion.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Having Media Content Like On Your Website <span class="w600"> Boosts Conversions And Sales By 80% </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-md30 mt20">
                     <div class="col-12 col-md-7 order-md-2 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/website.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Videos Help You Get <span class="w600"> 41% More Web Traffic From Search Engines. </span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-md30 mt20">
                     <div class="col-12 col-md-7 order-md-2 mx-auto">
                        <div class="letsface-box">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/click.webp" class="img-fluid d-block mx-auto">
                           <div class="f-md-22 f-18 w400 lh150 black-clr mx-md20 mt20 mt-md0 text-center text-md-start">
                              Videos Increases Your <span class="w600"> Email Click-Through <br class="d-none d-md-block">Rates By 200-300%  </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Profit Business -->
      <div class="profit-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w400 black-clr text-center">
                     <span class="w600">Each &amp; Every Profitable Business Online, Has This as Its Backbone.</span>
                     And… If You Want to Run a <br class="d-none d-md-block">Fine-Tuned Business, You'll Need It Too!"
                  </div>
                  <div class="mt20 f-18 f-md-20 lh140 w400 text-center black-clr">
                     Let me show you how BIG companies and TOP marketers are using media content secretly<br class="d-none d-md-block"> to get tons of sales, leads, and better customer satisfaction 
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Top Marketers &amp; Marketing Giants Are Using Videos On Their Landing Pages To Boost Their Sales &amp; Profits.               
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow1.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: relative; top: 115px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/sales.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Even The Huge $398 Billion E-Learning Industry Is Served Using Media Files
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     Top E-Learning Sites Like Udemy, Udacity, etc., Are Using Videos and Other Media Files To Sell &amp; Deliver Courses.
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: relative; top: 70px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/udemy.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     See How World’s BIGGEST E-com Companies Like Amazon, BestBuy Show Product Demo Videos To Increase Their Sales Drastically.                           
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow1.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: relative; top: 50px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/e-com-image.webp" class="img-fluid d-block mx-auto" alt="E-com Companies">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Supercharging Websites, Landing Pages &amp; Blogs              
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     With Ultra-Fast Loading Images To Increase Followership, Reduce Bounce Rates And Enjoy Higher Search Engine Rankings
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/letsface-arrow.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: relative; top: 50px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/supercharging.webp" class="img-fluid d-block mx-auto" alt="Supercharging Websites">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w600 lh140 black-clr">
                     Mobile Apps Are No Different            
                  </div>
                  <div class="f-18 w400 lh140 black-clr mt15">
                     As People Spend Lots of Time Online Watching Videos &amp; Engaging Content.
                  </div>                  
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/mobileapps.webp" class="img-fluid d-block mx-auto" alt="Mobile Apps">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12">
                  <div class="f-22 f-md-28 w600 lh140 black-clr text-center">
                     Affiliates Are Generating More Leads Using Fast Loading Lead Pages, Delivering Freebies, Bonuses &amp; Review Videos and Earning BIG Commissions            
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/affiliates-img.webp" class="img-fluid d-block mx-auto" alt="Affiliates">
               </div>
            </div>
         </div>
      </div>
      <!-- Profit Business End-->

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w600 lh140 white-clr">
                  Presenting…
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:100px">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  Amazing Cloud-Based Platform to Host, Manage and Deliver Unlimited Images, Files, and Videos at Lightning-Fast Speed with Zero Tech Hassles!  
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="proudly-list-bg">
                     <div class="col-12">
                        <ul class="proudly-list f-18 w500 lh140 black-clr">
                           <li>
                              <span class="f-22 f-md-24 w600">Unlimited Everything </span>
                              <div class="f-18 f-md-18 w400 mt5">Host, Manage &amp; Publish Unlimited PDFs, Docs, Audios, Videos, Zip Files or Any Other Marketing File.
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Robust &amp; Proven Solution</span>
                              <div class="f-18 f-md-18 w400 mt5">Powerful Battle-Tested Architecture Happily Serving 133 Million+ Marketing Files
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">50+ More Cool Features In Store</span>
                              <div class="f-18 f-md-18 w400 mt5">We’ve Left No Stone Unturned To Give You An Unmatched Experience
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Fast &amp; Easy</span>
                              <div class="f-18 f-md-18 w400 mt5">Deliver All Your Files at Lightning Fast Speed with Fast CDNs
                              </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">No Worries of Paying Monthly</span>
                              <div class="f-18 f-md-18 w400 mt5">During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End   -->

    <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED–A Breakthrough Software That<span class="w700 orange-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 white-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w700">Create Multiple UNIQUE Job Sites</span> – Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w700">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w700">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w700">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w700">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
                        <li><span class="w700">Get Started Immediately</span> – Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w700">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>

                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>



    <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Viddeyo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Viddeyo Section------>

   <!-- Header Viddeyo Start -->

   <div class="viddeyo-header-section">
      <div class="container">
         <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                        <style type="text/css">
                           .st00{fill:#FFFFFF;}
                           .st11{fill:url(#SVGID_1_);}
                        </style>
                        <g>
                           <g>
                              <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                                 l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
                              <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
                              <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                 v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                 c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                                 c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
                              <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                 v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                 c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                                 c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
                              <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                                 V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
                              <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                                 l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                                 l1.31-1.78H808.03z"></path>
                              <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                                 C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                                 c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                           </g>
                        </g>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                           <stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
                           <stop offset="1" style="stop-color:#FC6DAB"></stop>
                        </linearGradient>
                        <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                           c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                           c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                           C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                           c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                           c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                           c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                           c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                           c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                           c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                           c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                           c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                           c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                           c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                           c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                     </svg>
                        </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="viddeyo-preheadline f-18 f-md-20 w400 white-clr lh140">
                     <span class="w600 text-uppercase">Warning:</span> 1000s of Business Websites are Leaking Profit and Sales by Uploading Videos to YouTube
                  </div>
               </div>
               <div class="col-12 mt-md15 mt10 f-md-48 f-28 w400 text-center white-clr lh140">
                  Start Your Own <span class="w700 highlight-viddeyo">Video Hosting Agency with Unlimited Videos with Unlimited Bandwidth</span> at Lightning Fast Speed with No Monthly FEE Ever...
               </div>
               <div class="col-12 mt20 mt-md20 f-md-21 f-18 w400 white-clr text-center lh170">
                  Yes! Total control over your videos with zero delay, no ads and no traffic leak. Commercial License Included
               </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto" alt="proudly"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://launch.oppyo.com/video/embed/xo6b4lwdid" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap">
                     <div class="calendar-wrap-inline">
                           <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w500 black-clr">
                              <li>100% Control on Your Video Traffic &amp; Channels</li>
                              <li>Close More Prospects for Your Agency Services</li>
                              <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                              <li>Tap Into $398 Billion E-Learning Industry</li>
                              <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                              <li>Boost Your Clients Sales–Commercial License Included</li>
                           </ul>
                     </div>
                  </div>
               </div>
         </div>
      </div>
    </div>
    <div class="viddeyo-second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row viddeyo-header-list-block">
                        <div class="col-12 col-md-6">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
                                <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
                                <li><span class="w600">Get Maximum Visitors Engagement</span>–No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
                                <li>Create 100% Mobile &amp; SEO Optimized <span class="w600">Video Channels &amp; Playlists</span></li>
                                <li><span class="w600">Stunning Promo &amp; Social Ads Templates</span> for Monetization &amp; Social Traffic</li>
                                <li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
                                <li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing &amp; Video Agency Industry. </li>
                                <li>Manage All the Videos, Courses, &amp; Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
                                <li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
                                <li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices &amp; Page Builders</span> </li>
                                <li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
                                <li><span class="w600">Completely Cloud-Based</span> &amp; Step-By-Step Video Training Included </li>
                                <li>PLUS, YOU’LL RECEIVE – <span class="w600">A LIMITED COMMERCIAL LICENSE IF YOU BUY TODAY</span> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>
    <!-- Header Viddeyo End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--Viddeyo ends-->

   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : WebPrimo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--WebPrimo Header Section Start -->
<div class="WebPrimo-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto" /></div>
                <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                    <div>
                        The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                    </div>
                    <div class="mt5">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-md-45 f-28 w600 text-center white-clr lh150">
						First Ever on JVZoo…  <br class="d-none d-md-block">
						<span class="w700 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites & Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
					Easily create & sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches & 100+ other niches...
                </div>
				  <div class="col-12 mt20 mt-md40">
				  <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 min-md-video-width-left">

							<div class="col-12 responsive-video">
                                <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                            </div> 

                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                            <div class="WebPrimo-key-features-bg">
							<ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
							<li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
							<li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
							<li>Create Elegant, Fast loading & SEO Friendly Website within 7 Minutes</li>
							<li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
							<li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
							<li>Agency License Included to Build an Incredible Income Helping Clients!</li>
							</ul>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="WebPrimo-list-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row webprimo-header-list-block">
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li><span class="w600">Create Ultra-Fast Loading & Beautiful Websites</li>
                                    <li>Instantly Create Local Sites, E-com Sites, and Blogs–<span class="w600">For Any Business Need.</span>
									</li>
                                    <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
									</li>
                                    <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
									<li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
									<li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
									<li><span class="w600">Analytics & Remarketing Ready</span> Websites</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                                    <li>Customize and Update Themes on Live Website Easily</li>
                                    <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                                    <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                                    <li><span class="w600">Accept Payments</span> For Your Services & Products With Seamless WooCommerce Integration</li>
									<li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>									
                                </ul>
                            </div>
                        </div>
						<div class="col-12  header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
									<li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--WebPrimo Header Section End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--WebPrimo ends-->



   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : Affiliate Ninja Pro
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--- Affiliate Ninja Pro Header Start -->
<!--- Main banner -->
<div class="grand-logo-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-logo.webp" alt="AffiliateNinjaPrologo" class="img-fluid mx-auto d-block">
                    </div>
                </div>
            </div>
        </div>

<div class="AffiliateNinjaPro-banner">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-12">     
         </div>
         <div class="col-12 col-md-12 mt30">
            <div class="col-12 col-md-12 banner-block">
               <div class="text-center f-md-22 f-18 w300 lh130 mt0  white-clr">Are you sick & tired of wasting hours of your time setting up affiliate promos and paying a ton of money getting traffic on them, only to never see them get any sales?
               </div>
               <div class="text-center f-md-35 f-31 lh140 mt20 mt-md30 w700 white-clr">
                  REVEALED: A Cloud-Based App That <span class="lightcolor"> Creates SEO-Optimized Affiliate Funnels to Build You a List & Get Targeted Traffic...</span> 
               </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliate-line.webp" class="img-fluid mx-auto d-block mt20 mt-md40">
               <div class="text-center f-md-22 f-18 lh140 w500 white-clr mt10 ">...Completely done-for-you high converting affiliate funnels…
               </div>
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliate-line.webp" class="img-fluid mx-auto d-block mt10 ">
            </div>
         </div>
      <div class="col-12 col-md-8 mx-auto mt20">
         <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/instablockimg.webp" class="img-fluid mx-auto d-block">
      </div>
   </div>
   </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section1.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section1-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section2.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section2-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section2-mview.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section3.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section3-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/affiliateninjapro-section3.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--AffiliateNinjaPro ends-->
        
      <!-- SociDeck Section Start -->
        <div class="exclusive-bonus">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="white-clr f-24 f-md-36 lh140 w600">
                            Exclusive Bonus #5 : SociDeck
                        </div>
                        <div class="f-18 f-md-20 w500 mt20 white-clr">
                            20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grand-logo-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck.webp" alt="SociDeck Logo" class="img-fluid mx-auto d-block">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="socideck-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mx-auto">
                        <div class="c0l-12 col-md-10 mx-auto">
                            <p class="text-center f-20 f-md-22 socideck-highlite">Stop wasting time &amp; money in just posting on social media without any results…</p>
                        </div>

                        <div class="col-12 col-md-12">
                            <div class="text-center f-28 f-md-45 mt20 lh150">Convert Your Social Conversations into <strong>LEADS</strong> and <strong>SALES</strong> and Drive Tons of Social <strong>TRAFFIC</strong> 
                            </div><br>
                        </div>
                        <div class="col-12 col-md-10 mx-auto">
                            <p class="text-center f-20 f-md-22 lh150 ">Cloud based software. No Complicated Installation. No Hosting. No Technical hassles</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-8 mx-auto mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-productbox.webp" class="img-fluid d-block mx-auto">  
                    </div>
                </div>
            </div>
        </div>

        <div class="gr-1">
            <div class="container-fluid p0">
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-step.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-step-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-step.webp" alt="Steps" style="width:100%;">
                </picture>
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-easy.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-easy-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-easy.webp" alt="Real Result" style="width:100%;">
                </picture>
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-proudly.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/socideck-proudly-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jsocideck-proudly.webp" alt="Real Result" style="width:100%;">
                </picture>
            </div>
        </div>
        <!-- SociDeck Section End -->

        <!-- ProfitFox Section Start -->
        <div class="exclusive-bonus">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="white-clr f-24 f-md-36 lh140 w600">
                            Exclusive Bonus #6 : ProfitFox
                        </div>
                        <div class="f-18 f-md-20 w500 mt20 white-clr">
                            20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grand-logo-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-logo.webp" alt="ProfitFox Logo" class="img-fluid mx-auto d-block">
                    </div>
                </div>
            </div>
        </div>

        <div class="profitfox-banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="f-20 f-md-22 lh140 profitfox-title">Attention: Email Marketers, Affiliate Marketers, E-com Store &amp; WordPress Site Owners…</p>
                        <h1 class="f-28 f-md-43 w800 text-capitalize lh140 mt20 mt-md40 ">Revealed: World’s Most Powerful Notification System That <span class="pink underline-pink">Shows Your Perfect Offer to Visitors According to What They NEED</span> &amp; Generate You More Leads, Sales And Commissions ...
                        </h1>
                    </div>
                        
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/gradient-line.webp" class="img-responsive center-block">
                        <p class="f-18 f-md-22  text-center">
                            Cloud Based Software. No Complicated Installation. No Technical Hassles... 
                        </p>
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/gradient-line.webp" class="img-responsive  center-block">
                    </div>
                        
                    <div class="col-12 mt20 mt-md30">
                        <div class="row">
                            <div class="col-12 col-md-7">
                                <div class="responsive-video mt3 xsmt2">
                                    <object data="https://www.youtube.com/embed/nB-hZNmPioU?autoplay=1&amp;controls=0&amp;rel=0&amp;autohide=1&amp;nologo=1&amp;showinfo=0&amp;frameborder=0&amp;theme=light&amp;modestbranding=1">
                                    </object>            
                                </div>
                            </div>
                            <div class=" col-12 col-md-5">
                                <ul class="f-16 f-md19 profitfox-features lh140 p0 m0">
                                    <li>
                                        Target your audience precisely <span class="w600">on the basis of geo-location, behavior, keywords in URL &amp; much more…</span><hr>
                                    </li>
                                    <li>
                                        <span class="w600">Show your visitors exactly what they WANT</span> &amp; maximize your affiliate commissions &amp; leads<hr>
                                    </li>
                                    <li>
                                        Grab visitors from the neck and <span class="w600">literally force them to take desired action</span><hr>
                                    </li>
                                    <li>
                                        Comes with <span class="w600">ready-to-use and proven</span> lead &amp; promo notification templates<hr>
                                    </li>
                                    <li>
                                        <span class="w600">Use Inbuilt Inline Editor</span> to change text, images, CTA buttons or anything in templates &amp; it’s very easy<hr>
                                    </li>
                                    <li>
                                        <span class="w600">100% cloud based</span> newbie friendly software
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>

        <div class="gr-1">
            <div class="container-fluid p0">
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-steps.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-steps-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-steps.webp" alt="Steps" style="width:100%;">
                </picture>
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-proudly.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-proudly-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/profitfox-proudly.webp" alt="Real Result" style="width:100%;">
                </picture>
            </div>
        </div>

        <!-- ProfitFox Section End -->

        <!-- LeadFunnelCloud Section Start -->

        <div class="exclusive-bonus">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="white-clr f-24 f-md-36 lh140 w600">
                            Exclusive Bonus #7 : LeadFunnelCloud
                        </div>
                        <div class="f-18 f-md-20 w500 mt20 white-clr">
                            20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grand-logo-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/leadfunnelcloud-logo.webp" alt="LeadFunnelCloud Logo" class="img-fluid mx-auto d-block">
                    </div>
                </div>
            </div>
        </div>

        <div class="leadfunnelcloud-banner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <div class=" col-12 col-md-12">
                            <div class="col-12 col-md-12 text-center f-28 f-md-45 lh150 w700 white-clr">
                                Discover a <span class="ltstrip qcolor"><span class="lstripbg2 qcolor">Copy-Paste-System to Build PROVEN</span> <span class="lstripbg3 qcolor">Converting Lead Funnels</span></span> That Gets <u>TRAFFIC, Leads &amp; Sales</u>
                            </div>
                            <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                                <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/phl.webp" class="img-fluid mx-auto d-block">
                                <div class="f-20 f-md-22 white-clr text-center">
                                    Simply use our DFY funnels or create a unique lead-funnel in 3 steps using our funnel builder, inline page editor &amp; ready-to-use templates.
                                </div>
                                <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/phl.webp" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-12 mt-20 mt-md50">
                        <div class="row">
                        <div class="col-md-7 col-12">
                            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/leadfunnelcloud-productbox.webp" class="img-fluid mx-auto d-block" style="max-height: 400px; " alt="LeadFunnelCloud ProductBox">
                        </div>

                        <div class="col-md-5 col-12">
                            <div class="fbox">
                                <ul class="f-15 f-md-16 lh130 w400 pl0 mt0 xsmt5">
                                    <li>
                                        <span class=" f-16 f-md-18 qcolor pcolorline w600">25 Beautiful Ready-To-Use Lead Funnels -</span> Just Add Your Offer Link to start building List.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">Ready-to-Use Follow-up Emails - </span>Build Long-term Relationship with Your Customers.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">100 Proven Converting Products -</span> Promote high paying products from JVzoo, PayDotCom, ClickBank.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">Create 1000s of Unique Lead Funnels :</span> Using 50 Eye-Catchy Lead Templates &amp; Inline page editor
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">Social Traffic Built In :</span> One click sharing to all major social networks.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">FB Messenger Integration - </span>Instantly add FB Messenger Chat Box on Pages.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">GDPR Compliant -</span> Zero Non-Compliance Threats.
                                    </li>
                                    <li>
                                        <span class="f-16 f-md-18 qcolor pcolorline w600">Super-Easy to Use - </span>100% Cloud Based- Nothing to install, host or code
                                    </li>
                                </ul>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="leadcloud-features">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-45 f-28 w600 lh150 text-center">
                        Finally: An Easy-To-Use LeadFunnel Builder That <br class="d-none d-md-block"> Simplifies Funnel Creation & List Building
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <ul class="leadcloud-list f-md-20 f-16 w400">
                        <li>Create beautiful Lead funnels that attract your visitors.</li>
                        <li>Build a Healthy List &amp; also drive social traffic.</li>
                        <li>Connect easily with vendors by sending email &amp; social traffic.</li>
                        <li>Unlimited funnels with No monthly fee ever.</li>
                        <li>You also can use the same technique for your own business.</li>
                        <li>Get better results from same efforts.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="gr-1">
            <div class="container-fluid p0">
                <picture>
                    <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/leadfunnelcloud-steps.webp">
                    <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/leadfunnelcloud-steps-mview.webp">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/leadfunnelcloud-steps.webp" alt="Steps" style="width:100%;">
                </picture>
            </div>
        </div>
        <!-- LeadFunnelCloud Section End -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase YoDrive, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Simple Membership Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Want a step by step plan to launch a product?
                           </li>
                           <li>
                              In this course, you’ll get four live sessions taught by an experienced marketer getting your product up and running quickly.
                           </li>
                           <li>
                              Just use this mind-blowing package with <span class="w600">YoDrive</span> and scale your business to the next level.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                           QikMobi Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              One of the core concepts in the digital marketing industry is the sales funnel but sales funnels for most online businesses are actually very complex.
                           </li>
                           <li>
                              This bonus package that will train your subscribers to use sales funnels right and get lots of money to your business.
                           </li>
                           <li>
                              When combined with <span class="w600">YoDrive,</span>  it enhances your marketing results & helps you become the market pro.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Instant Squeeze Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>JVZoo has been one of the best choices for many online entrepreneurs that sold digital products.</li>
                           <li>In this video tutorial, you will learn how to set up an effective sales funnel that converts and will really make you profit.</li>
                           <li>Using this package with <span class="w600">YoDrive</span> will turn your pages and funnels into highly professional attention magnets without spending a fortune.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           WP Video Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              A sales letter can add a personal touch to advertising which is not possible in other forms of publicity and advertising.
                           </li>
                           <li>
                              In this bonus package, you’ll get 100 Sales letters, Guarantee pages, Set of JV letters, squeeze pages and many more that will save you thousands of dollars without talking to a single copywriter.
                           </li>
                           <li>
                              Use it with <span class="w600">YoDrive</span> and turn your squeeze pages into high-converting and effective pages that generate lots of new subscribers.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Instant Content Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Every online business owners and affiliate marketers want to make out of the traffic that they generated to their website but driving traffic to your website is sometimes time-consuming and involves many issues.
                           </li>
                           <li>
                              With this awesome bonus package you can easily motivate your website visitors to buy your products.
                           </li>
                           <li>
                              By combining this package with powerful <span class="w600">YoDrive,</span> you will reap out maximum benefits from your product launches.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 yellow-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Course Engagement Hacks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Cross-selling involves offering the customer a related product or service. This is one of the best method of marketing other relevant products.
                           </li>
                           <li>
                              You can easily boost your sales with no extra work using the cross sell slider pro desktop software.
                           </li>
                           <li>
                              Now stop thinking and use this with funnel marketing prowess of <span class="w600">YoDrive</span>  to take your business to the next level.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                           Internet Business Startup Kit Advanced
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              Custom logo designs are expensive, and you never know what you get.
                           </li>
                           <li>
                              To reduce the risk, we are giving you a package of ultra-high-quality, brand-new logo templates that will save you thousands of dollars.
                           </li>
                           <li>
                           Combine this package with <span class="w600">YoDrive</span>  to get even more sales and make even more profits from your online business.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Affiliate Marketing Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              If you are a graphic designer or website owner, abstract images are really useful to your own project or to your client's marketing campaign.
                           </li>
                           <li>
                              The good news though is that inside this product is a bundle of high-quality and beautifully designed abstract images that you can use or re-sell today.
                           </li>
                           <li>
                           While using <span class="w600">YoDrive,</span> integrate this with your overall marketing strategy & boost your brand-building efforts in the long run.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Find Your Niche
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Any copy you use in your marketing is a reflection on your company. An effective copywriting have some secrets.
                           </li>
                           <li>
                           With this bonus package, you can learn the deepest, darkest secrets to the Internet's most sought after copywriters who have generated thousands of dollars for every single page they write.
                           </li>
                           <li>Now all you need to do is create your own brand and use lansing page and funnel builder <span class="w600">YoDrive</span> to zoom past your competitors at will.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Create Your Own Job
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Answering queries of your customers is very important if you don’t want them to leave your website.
                           </li>
                           <li>
                           Don’t worry as this chat window works just like a real live chat, but the responses are all fully automated, so you get the benefits of live chat, without having to actually provide live support.
                           </li>
                           <li>
                           This bonus when combined with <span class="w600">YoDrive,</span> becomes an ultimate growth booster for business owners.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-12 f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 yellow-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">14</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Work From Home Productivity Video
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Are you trying to find the target consumers for your product or striving to reach out to potential consumers?</li>
                           <li>If yes, then here is a Golden opportunity awaiting you. This video course will enable you to learn a large number of creative and unique ways of lead generation both offline as well as online.</li>
                           <li>Now use this amazing video course along with <span class="w600">YoDrive</span> to intensify your business growth like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Mobile eCommerce Simplified
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Affiliate marketing is a key strategy for every business industry to prosper. But there are more chances to fail if you don’t apply the accurate affiliate marketing strategy.</li>
                           <li>Don’t worry! The affiliate marketing kit solves the problems and hindrances faced by newbies and veteran affiliate marketers.</li>
                           <li>When used with <span class="w600">YoDrive,</span> this package will surely become a top-notch business booster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Email Buzz
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Want to start making money with private label content even if you don't have a website of your own?</li>
                        <li>Now that’s possible! With this report, you will learn fast and easy ways to maximize your profit with PLR.</li>
                        <li>
                        This package is a must-have and when combined with powerful marketing technology <span class="w600">YoDrive,</span> reaps great results for you in the long run.
                        </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        EBook Jackpot Video Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>For being successful, you need to study what successful people do and model their actions.</li>
                           <li>With this software, skyrocket your conversions, sales and profits with your own automated sales assistants, providing 24/7 support on all your websites.</li>
                           <li>This bonus when combined with <span class="w600">YoDrive</span> proves to be a great resource for every success hungry marketer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        100 Software Creation Ideas
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It’s a fact that people love good customer support and that’s why your business need it.</li>
                           <li>With xyber email assistant software’s help you can easily do your customer support with ease using xyber email assistant!</li>
                           <li>So, get in active mode and use this bonus with <span class="w600">YoDrive</span> to intensify your growth prospects like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 yellow-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-12 f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 yellow-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">07&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">14</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Social Media Automation
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Are you trying to find the target consumers for your product or striving to reach out to potential consumers?</li>
                           <li>If yes, then here is a Golden opportunity awaiting you. This video course will enable you to learn a large number of creative and unique ways of lead generation both offline as well as online.</li>
                           <li>Now use this amazing video course along with <span class="w600">YoDrive</span> to intensify your business growth like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Facebook Ads 101
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Affiliate marketing is a key strategy for every business industry to prosper. But there are more chances to fail if you don’t apply the accurate affiliate marketing strategy.</li>
                           <li>Don’t worry! The affiliate marketing kit solves the problems and hindrances faced by newbies and veteran affiliate marketers.</li>
                           <li>When used with <span class="w600">YoDrive,</span> this package will surely become a top-notch business booster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        CB Niche Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Want to start making money with private label content even if you don't have a website of your own?</li>
                           <li>Now that’s possible! With this report, you will learn fast and easy ways to maximize your profit with PLR.</li>
                           <li>This package is a must-have and when combined with powerful marketing technology <span class="w600">YoDrive,</span> reaps great results for you in the long run.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Web Baycom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>For being successful, you need to study what successful people do and model their actions.</li>
                           <li>With this software, skyrocket your conversions, sales and profits with your own automated sales assistants, providing 24/7 support on all your websites.</li>
                           <li>This bonus when combined with <span class="w600">YoDrive</span> proves to be a great resource for every success hungry marketer.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Turbo ECom + Addon PRO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It’s a fact that people love good customer support and that’s why your business need it.</li>
                           <li>With xyber email assistant software’s help you can easily do your customer support with ease using xyber email assistant!</li>
                           <li>So, get in active mode and use this bonus with <span class="w600">YoDrive</span> to intensify your growth prospects like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 orange-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 orange-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 orange-clr">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">“PRANSHUVIP”</span> for an Additional <span class="w700 blue-clr">15% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz1.com/c/47069/384700/" class="text-center bonusbuy-btn px-md80">
                     Grab YoDrive + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-md-12 f-16 f-md-20 lh150 w500 text-center black-clr mt15 mt-md30">
                  Use Coupon Code <span class="w700 blue-clr">“PRANSHUBUNDLE”</span> for an Additional <span class="w700 blue-clr">$60 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
                  <a href="https://jvz4.com/c/47069/384866/" class="text-center bonusbuy-btn">
                     Grab YoDrive Bundle + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px;">
                     <defs>
                        <style>.cls-1{fill:#fff;}</style>
                     </defs>
                     <path class="cls-1" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                     <path class="cls-1" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                     <path class="cls-1" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                     <path class="cls-1" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                     <path class="cls-1" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                     <path class="cls-1" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                     <path class="cls-1" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                     <path class="cls-1" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                     <rect class="cls-1" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                     <path class="cls-1" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                     <path class="cls-1" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                  </svg>
                  <div class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © YoDrive</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
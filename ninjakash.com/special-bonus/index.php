<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta name="title" content="NinjaKash | Bonuses">
	<meta name="description" content="NinjaKash">
	<meta name="keywords" content="NinjaKash">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta property="og:image" content="https://www.coursova.biz/special-bonus/assets/images/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="NinjaKash | Bonuses">
	<meta property="og:description" content="NinjaKash">
	<meta property="og:image" content="https://www.coursova.biz/special-bonus/assets/images/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="NinjaKash | Bonuses">
	<meta property="twitter:description" content="NinjaKash">
	<meta property="twitter:image" content="https://www.coursova.biz/special-bonus/assets/images/thumbnail.png">


<title>NinjaKash  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="shortcut icon" href="assets/images/favicon.png"/>
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&family=Work+Sans:ital,wght@0,100;0,200;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
<!-- Javascript File Load -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script type='text/javascript' src="assets/js/bootstrap.min.js"></script>
<!-- Buy Button Lazy load Script -->

<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-K95FCV8');</script>

<!-- End Google Tag Manager -->
<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W6LN7FM');</script>
<!-- End Google Tag Manager -->


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W6LN7FM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'February 05 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://cutt.ly/NOn6FtT';
	 $_GET['name'] = 'Er. Ashu Kumar';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center p0">
                    <div class="col-xs-12 text-center">
                    	<div class="f-sm-32 f-20 lh140 w400 white-clr">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus page for &nbsp; <img src="assets/images/logo-white.png" class="mt15 mt-sm0">
                    	</div>
                    </div>

                    <div class="col-xs-12 p0 mt20 mt-sm40">
                        <div class="f-sm-24 f-20 w400 text-center lh150 yellow-bg black-clr"><span class="w600">Grab My 15 Exclusive Bonuses </span>Before the Deal Ends…</div>
                    </div>

                    <div class="col-sm-12 col-xs-12 mt20 mt-sm40 p0">
						<div class="highlight-heading">
							<div class="f-sm-48 f-28 w500 text-center black-clr lh140 line-center worksans rotate-5">Take A Sneak Peek– <span class="w700">How This Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic & Sales on 100% Autopilot</div>
						</div>
                    </div>

                    <div class="col-sm-12 col-xs-12 text-center mt20 mt-sm30 p0">
                        <div class="f-sm-24 f-20 lh150 w500 white-clr">Watch My Quick Review Video</div>
                    </div>
                </div>
            </div>

            <div class="row mt20 mt-sm30">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 p0">
                    <div class="video-frame col-xs-12">
                        <!-- <img src="assets/images/product-box1.png" class="img-responsive center-block"> -->
                       <div class="col-xs-12 p0">
                            <div class="col-xs-12 responsive-video p0">
                              <iframe src="https://ninjakash.dotcompal.com/video/embed/58a6ujojqh" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <!-- <img src="assets/images/video-shadow.png" class="img-responsive center-block hidden-xs"> -->
                </div>
            </div>
        </div>
    </div>


	<!-- Section-1 Start -->
	<div class="section-host">
        <div class="container">
			<div class="row inner-content">
                                            <div class="col-xs-12 px0 px-sm15">
                        <div class="f-sm-50 f-28 w500 lh140 text-capitalize text-center black-clr">
Now Creating Your Proven Affiliate Funnels Is <span class="w700">A SIMPLE 3 Step Method That Goes Like This-</span>
                        </div>
                     </div>
                        <div class="col-xs-12">
                            <div class="row mt30 mt-sm60 d-flex align-items-center flex-wrap">
                                <div class="col-xs-12 col-sm-6 px0 px-sm15">
								<div class="step-bg">
                                                <div class="f-20 f-sm-26 w700 white-clr  text-center lh100 text-nowrap" editabletype="text" style="z-index:10;">
                                                   Step 1
                                                </div>
                                            </div>
                                    <div class="col-sm-12 px0 f-22 f-sm-32 lh140 w700 text-xs-center mt15">
                                       Insert Your Affiliate Link
                                    </div>
                                    <div class="col-xs-12 px0">
                                        <div class="f-20 f-sm-22 w400 black-clr lh140 mt15 text-xs-center">
                                          To get started, choose from 50 hand-picked products, or insert your own affiliate product link in our software.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 px0 px-sm15 mt20 mt-sm0">
                                    <div>
                                        <img src="assets/images/step-1.png" class="img-responsive mx-auto">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt30 mt-sm60 d-flex align-items-center flex-wrap">
                                <div class="col-xs-12 col-sm-6 col-sm-push-6 px0 px-sm15">
								<div class="step-bg">
                                                <div class="f-20 f-sm-26 w700 white-clr  text-center lh100 text-nowrap" editabletype="text" style="z-index:10;">
                                                   Step 2
                                                </div>
                                            </div>
                                    <div class="col-sm-12 px0 f-22 f-sm-32 lh140 w700 text-xs-center mt15">
                                      Choose From Our Beautiful & Proven Converting Affiliate Funnel Templates
                                    </div>
                                    <div class="col-xs-12 px0">
                                        <div class="f-20 f-sm-22 w400 black-clr lh140 mt15 text-xs-center ">
                                         Now, just select a template from our library & and edit it to create your own affiliate funnels.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-sm-pull-6 px0 px-sm15 mt20 mt-sm0">
                                    <div>
                                        <img src="assets/images/step-2.png" class="img-responsive mx-auto">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt30 mt-sm60 d-flex align-items-center flex-wrap">
                                <div class="col-xs-12 col-sm-6  px0 px-sm15">
								<div class="step-bg">
                                                <div class="f-20 f-sm-26 w700 white-clr  text-center lh100 text-nowrap" editabletype="text" style="z-index:10;">
                                                   Step 3
                                                </div>
                                            </div>
                                     <div class="col-sm-12 px0 f-22 f-sm-32 lh140 w700 text-xs-center mt15">
                                      Publish & Enjoy FREE Traffic, Leads & Sales
                                    </div>
                                    <div class="col-xs-12 px0">
                                        <div class="f-20 f-sm-22 w400 black-clr lh140 text-xs-center mt15">
                                         Now, our software put this affiliate funnel on active mode that is a SEO, Social & Viral Traffic Machine. 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 px0 px-sm15 mt20 mt-sm0">
                                    <div>
                                        <img src="assets/images/step-3.png" class="img-responsive mx-auto">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
		</div>
	</div>
	<!-- Section-1 End -->
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt20 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Coupon Code <span class="w800 purple">"ninjakash"</span> for Extra <span class="w700 purple">4% Discount</span> </div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End   -->
	
	<div class="no-installation">
		<div class="container">
			<div class="row inner-content">
         

                        <div class="col-xs-12 col-sm-12 px0">
                            <div class="col-xs-12 col-md-4 col-sm-4 mt20 mt-sm0 px-sm15 px0">
                                <div editabletype="image" style="z-index:10;">
                                    <img src="assets/images/no1.png" class="img-responsive center-block height172">
                                </div>
                                <div class="w600 f-22 f-sm-28 mt20 mt-sm30 black-clr text-center lh140">
                                   No Installation 
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 mt20 mt-sm0 px-sm15 px0">
                                <div editabletype="image" style="z-index:10;">
                                    <img src="assets/images/no2.png" class="img-responsive center-block height172">
                                </div>
                                <div class="w600 f-22 f-sm-28 mt20 mt-sm30 black-clr text-center lh140">
                                   No Prior Knowledge 
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 mt20 mt-sm0 px-sm15 px0">
                                <div editabletype="image" style="z-index:10;">
                                    <img src="assets/images/no3.png" class="img-responsive center-block height172">
                                </div>
                                <div class="w600 f-22 f-sm-28 mt20 mt-sm30 black-clr text-center lh140">
                                    No Techie Stuff
                                </div>
                            </div>
                        </div>
						
						               <div class="col-xs-12 px0 mt20 mt-sm40">
                            <div class="f-20 f-sm-24 w400 text-center black-clr lh140">
                               In Just 5 minutes, you can make your profitable affiliate funnel live to start building a huge list & enjoy commissions coming in your bank account on autopilot.
                            </div>

                        </div>
                        <!-- <div class="col-xs-12 p0 f-24 f-sm-36 w600 lh140 black-clr text-center mt15 mt-sm50">
                            It’s LIGHTER, FASTER and Built with love for EVERYONE who use VIDEOS ONLINE FOR ANY PURPOSE.

                        </div> -->
                    </div>
		</div>
	</div>

	<div class="section-checkout">
		<div class="container px-sm15 px0">
			<div class="row inner-content">
                     <div class="col-xs-12">
                        <div class="f-sm-50 f-28 w700 lh140 text-capitalize text-center black-clr">
                      Checkout The Floods of <u>FREE</u> Traffic <br class="visible-lg">
That NinjaKash Team Is Getting Hands-Free...

                        </div>
                     </div>
					  <div class="col-xs-12 col-sm-10 col-sm-offset-1 mt20 mt-sm50">
                         <img src="assets/images/proof.jpg" class="img-responsive center-block">                        
                   </div>
					  <div class="col-xs-12 mt20 mt-sm80">
                        <div class="f-sm-50 f-28 w700 lh140 text-capitalize text-center black-clr">
Which is <u>Consistently</u> Making Them $500-1000 In Commissions EACH & EVERY DAY

                        </div>
                     </div>
					   <div class="col-xs-12 col-sm-10 col-sm-offset-1 mt20 mt-sm50">
                         <img src="assets/images/proof-01.png" class="img-responsive center-block">
                   </div>
			  </div>
		</div>
	</div>
	
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt20 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Coupon Code <span class="w800 purple">"ninjakash"</span> for Extra <span class="w700 purple">4% Discount</span> </div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Section-3 testimonial Start -->
	<div class="video-testimonial-section">
		<div class="container px-sm15 px0">
			<div class="row inner-content">
                        <div class="col-xs-12">
                            <div class="f-sm-50 f-28 lh140 w700 text-center black-clr">Checkout What NinjaKash  Users Have to SAY</div>
                        </div>
						<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3 col-sm-push-9">      
                                            <img src="assets/images/jason-brooks.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 col-sm-pull-3  mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                          Building Engaging Affiliate Funnels –<span class="w600"> DONE!</span><br>
Driving VIRAL Social Traffic- <span class="w600">DONE!</span><br>
Search Engine Optimization – <span class="w600">DONE!</span><br>
Top-Notch High Converting Products– <span class="w600">That’s DONE as well!</span><br><br>


This software really impressed me with its amazing features. A great option for anyone looking to make the most from affiliate marketing.
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Jason Brooks</div>
								                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
							
							<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3">      
                                            <img src="assets/images/mike-denver.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                         Man, this is <span class="w600">TRULY AWESOME.</span> <br><br>


It creates stunning affiliate funnels within minutes. NinjaKash churns VIRAL traffic, more sales and better rankings for your affiliate business. 
 <br><br>

<span class="w600">Can’t get faster and easier than this folks.</span>
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Mike Denver </div>
                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
							
							<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3 col-sm-push-9">      
                                            <img src="assets/images/keith-gardner.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 col-sm-pull-3  mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                         WOW! I am really blown away with this.... <br><br>


<span class="w600">Very impressive software indeed. I made 2 funnels in about 10 minutes…</span> It was very easy to get started with it…Thank you for making this software.
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Kristie Gardner</div>
								                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
							
						<!-- <div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3 col-sm-push-9">      
                                            <img src="assets/images/abhijit-saha.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 col-sm-pull-3  mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                         NinjaKash- like the name suggests, is the perfect platform to begin with affiliate marketing and drive laser targeted viral traffic. I love the functionalities of this amazing software and would love to use it  to boost my affiliate marketing business… 
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Abhijit Saha</div>
										 <div class="f-18 f-sm-18 w400 lh120 black-clr mt10">(Internet Marketer)</div>
                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
						
							
							<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3">      
                                            <img src="assets/images/atul-pareek.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                         NinjaKash is something that you can’t overlook at any cost. The features you’re getting amazed me completely & I’m sure you’ll also be in love with it. This is something that’ll be of immense worth for your online business… 
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Atul Pareek </div>
										<div class="f-18 f-sm-18 w400 lh120 black-clr mt10">(Internet Marketer)</div>
                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>	
							<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3 col-sm-push-9">      
                                            <img src="assets/images/bill-darr.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 col-sm-pull-3  mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                       NinjaKash is next level quality software. It's so easy to use, and the training included makes it even easier to create stunning funnels in any niche in few minutes. Kudos to the team for creating a solution that can make the complete process fast & easy. I'd say that this is a MUST HAVE technology for marketers… 
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Billy Darr</div>
										<div class="f-18 f-sm-18 w400 lh120 black-clr mt10">(Internet Marketer)</div>
                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
							
							<div class="col-xs-12 mt50 mt-sm80">
                                    <div class="testi-block">
									 <div class="f-20 f-sm-32 w600 shape-testi">NinjaKash Verified User</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12 col-sm-3">      
                                            <img src="assets/images/kundan-choudhary.png" class="img-responsive center-block">
                                    </div>
									  <div class="col-xs-12 col-sm-9 mt20 mt-sm0">
									  <div class="f-20 f-sm-22 w400 lh140">
                                          NinjaKash is a ready-to-use affiliate marketing technology that creates affiliate funnels for any audience in any niche & helps to boost lead generation and sales. This technology will ROCK the market for 2022 & beyond…
                                        </div>
                                        <div class="f-24 f-sm-36 w700 lh120 orange mt20">Kundan Choudhary </div>
										<div class="f-18 f-sm-18 w400 lh120 black-clr mt10">(Six Figures Affiliate Marketer)</div>
                                    </div>
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div> -->
							
                    </div>
		</div>
	</div>
	<!-- Section-3 testimonial End -->
	<div class="space-section thatsall-section1">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">
					 
					 <div class="col-xs-12">
                        <div class="row d-flex align-items-center flex-wrap">							
                            <div class="col-xs-12 col-sm-6">
							<div class="f-sm-22 f-20 w400 lh140 black-clr">
According to Bo Bennet,
                        </div>
                                  <div class="f-24 f-sm-36 w600 lh140 text-xs-center black-clr mt20">
                                 “Affiliate marketing has made millions of ordinary people millionaires.”
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 mt20 mt-sm0">
                                <div class="mt-sm30">
                                    <img src="assets/images/affiliate.png" class="img-responsive center-block">
                                </div>
                            </div>
                        </div>
                        </div>
						
						<div class="col-xs-12 mt80 mt-sm80">
                                    <div class="facts-block text-center">
									 <div class="f-20 f-sm-26 w600 shape-testi1">Here’re some facts:</div>
									 <div class="row d-flex align-items-center flex-wrap">
									<div class="col-xs-12">      
                                            <img src="assets/images/stats-graphic.png" class="img-responsive center-block">
                                    </div>
							
                                    
                                   
                                </div>
                                        
                                    </div>
                               
                            </div>
						
						
                  </div>
               </div>
            </div>
			
			<div class="space-section affiliate-section">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">

					 		
								<div class="col-xs-12 f-20 f-sm-22 w400 lh140 black-clr">
                                 <span class="w600">Bottom line is:</span><br><br>
When you can combine selling top affiliate products, FREE traffic (SEO, Social & viral), and Proven Funnels to convert sales, you’re the fastest way possible.<br>

                                </div>
								
								<div class="col-xs-12 f-20 f-sm-22 w400 lh140 black-clr mt20 mt-sm50">
                                 <img src="assets/images/fb-viral.png" class="img-responsive center-block">

                                </div>
                  </div>
               </div>
            </div>
	<!-- Section-7 Start Intro -->
	<div class="proudly-section relative">
        <div class="container">
            <div class="row inner-content">
                 <div class="col-xs-12 px0 px-sm15 text-center">		
						<div class="black-design1">
                                <div class="f-24 f-sm-26 w600 lh140 text-center white-clr skew-normal">
                                Proudly Presenting...
                                </div>
                            </div>
                            </div>
							 <div class="col-xs-12 mt20 mt-sm70 px0 px-sm15">
                       <img src="assets/images/proudly.png" class="img-responsive center-block">
                        </div>
				   <div class="col-xs-12  px0 px-sm15 f-sm-20 f-20 w400 white-clr lh160 mt-sm30 mt20 text-center">
                          Ultimate game-changing solution that enables you to build highly converting affiliate funnels to build <br class="visible-lg">a list and get search & viral  traffic from top social media platforms.
                        </div>
               </div> 
		</div>   
    </div>
	<!-- Section-7 End --> 
	 <!-- Client Video -->
            <div class="space-section client-video-section" id="productdemo">
                <div class="container">
                    <div class="row inner-content">
                        <div class="col-xs-12">
                            <div class="f-sm-50 f-28 lh140 w700 text-center black-clr" editabletype="text" style="z-index:10;">
                             Watch NinjaKash In Action
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row mt20 mt-sm60">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                   <div class="col-xs-12 responsive-video video-shadow" editabletype="video" style="z-index: 10;">
                                     <iframe src="https://ninjakash.dotcompal.com/video/embed/ga3smhff11" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Cilent Video end -->
			
	
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 heading-bg text-center p0">
					<div class="f-24 f-sm-36 lh140 w700">When You Purchase NinjaKash, You Also Get <br class="hidden-xs"> Instant Access To These 15 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 1</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus1.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 The Funnel Hacker
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						<li>If you are an affiliate marketer or digital product owner who aims to have a hugely successful product launch, having an effective sales funnel will help you close more sales to your product. </li>
						<li>The good news though is that this product will guide you on how to make your sales funnel for the first time & boost its performance effectively in your internet marketing career. </li>
						<li>This bonus when combined with NinjaKash proves to be a great resource for every success hungry marketer. </li>

					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 2</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12 col-sm-push-7">
					  <img src="assets/images/bonus2.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0 col-sm-pull-5">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Insider Guide Template - Sales Page Funnel
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Having high converting sales funnels is one of the most important aspects for every business owner. They help a great deal in converting random prospects into lifetime happy buyers. </li>
						 <li>Keeping this in mind, this exclusive bonus comes loaded with effective strategies to create sales funnels for any audience in any niche & boost profits using them.   </li>
						 <li>You can easily use this bonus software with NinjaKash and create a big business booster to secure best results with greater traffic and more profit. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus3.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 WP Funnel Profit
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Are you looking for information on WarriorPlus or confused whether WarriorPlus is a legitimate company or not? </li>
						 <li>To solve your doubt, we have come up with a video series that will explain to you what Warrior Plus is and everything you need to know about WarriorPlus funnels. </li>
						 <li>Now, stop thinking and build your affiliate funnels with this software and use with NinjaKash to boost rankings like you always wanted. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12 col-sm-push-7">
					  <img src="assets/images/bonus4.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0 col-sm-pull-5">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Setup A Video Sales Funnel
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
							<li>Video sales funnels play a very important role in converting your website visitors into buyers. All you need is to create an engaging one. </li>
							<li>If you want some proven formulas to create an engaging video sales funnels with which you can expect a huge sale in your offers, this bonus guide will help you learn them. </li>
							<li>So, what are you waiting for? Combine this package with countless benefits that you get with NinjaKash and take your profits to the next level. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus5.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Affiliate Terminology
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
							<li>Affiliate marketing is the topmost way to make money online as you make commissions just by promoting products created and marketed by other vendors. </li>
							<li>So, here’s an exciting package that includes proven techniques that helps to become a successful affiliate and stand out from the competition. You will also learn an importance of building a list and how to find products to promote to maximize benefits. </li>
							<li>This bonus is a boon and when combined with NinjaKash, this package becomes a top-notch business booster. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Coupon Code <span class="w800 purple">"ninjakash"</span> for Extra <span class="w700 purple">4% Discount</span> </div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 6</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus6.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Solopreneur Success
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Being a solopreneur is something that most marketers aspire for, but can’t convert that into reality as there are tons of key aspects involved. </li>
						 <li>So, in order to guide you smoothly in the process, we’re giving this useful package that helps you become a solopreneur & take complete control of our life. </li>
						 <li>This bonus is a great add-on to the affiliate marketing prowess that NinjaKash possesses and will take your affiliate marketing benefits to the next level. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus7.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Make First $100 On The Web
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Making the first dollar online is one of the most important events for every success hungry affiliate marketer. There are tons of gurus and strategies available, but who to trust becomes the most critical decision. </li>
						<li>Fortunately enough, we’re offering this bonus that comes  loaded with battle tested techniques to make your first $100 & scale it to reach cerebral heights. </li>
						<li>So, get in active mode and use this bonus with NinjaKash to intensify your growth prospects like never before. </li>				
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus8.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 Ultimate USP
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>You may have the best product or the highest in demand services, but if your business does not have a unique selling proposition, then you’re not heading in the right direction. </li>
						 <li>If you’re also scared how to get out of this menace, it’s time to take a breather. With this unique bonus, you’ll be able to find out the best USP for your offer & stand out from the competitors in a simplified manner. </li>
						 <li>So, what are you waiting for? Combine this package with countless benefits that you get with NinjaKash and take your profits to the next level. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 9</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus9.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 Recurring Income Strategies
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Having a source of recurring income looks easy from outside, but needs lots of effort and smart planning in order to achieve desired results. If you also faced this issue, then it’s time to take a breather. </li>
						 <li>With this package, you can follow desired & battle tested steps to make a passive recurring income source in a hassle free manner. </li>
						 <li>So, get in active mode and use this bonus with NinjaKash to intensify your growth prospects like never before. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 10</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus10.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Peak Productivity 
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>This bonus comes with exclusive techniques that are used by top business leaders to boost productivity of their business empire & get known amongst the best. </li>
						 <li>When combined with NinjaKash, this bonus becomes a lethal combination and boosts traffic and profits hands down. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Coupon Code <span class="w800 purple">"ninjakash"</span> for Extra <span class="w700 purple">4% Discount</span> </div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 11</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus11.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						SEO Split Testing
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>58% B2B marketers accept the fact that SEO produces more leads than any other marketing strategy. So, if you’re also looking to use SEO to take your business to the next level, you’re at the right place today. </li>
						 <li>With this bonus, you can lean premium SEO hacks & use them to get higher traffic for your offers. </li>
						 <li>So, what are you waiting for? Use the funnel creation techniques mentioned in NinjaKash & drive more traffic on your offers using this bonus. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 12</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus12.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 Lead Generation On Demand
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Driving targeted leads is one of the most crucial aspects for all success hungry marketers. If not given adequate attention, it can give dire consequences. </li>
						 <li>Keeping this in mind, here’s a premium bonus to help you drive targeted leads on demand & boost your list like never before.  </li>
						 <li>Now what are you waiting for? Start building your list with NinjaKash and use this package to force them to take action and get best results from your affiliate marketing efforts. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 13</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus13.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Copywriting Influence
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>Copywriting holds a great deal of importance for every business owner as copy is the fact of every offer. If not adhered to in an attentive manner, it proves to be fatal in the long run. </li>
						 <li>Now here’s the good news for you. With this exciting bonus, creating high converting sales copies &using them to boost sales just got faster & easier. </li>
						 <li>So, stop being a thinker & use these tricks with NinjaKash to grow your affiliate marketing business in a complete manner. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 14</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus14.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						Affiliate Advantage
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
						 <li>NinjaKash has been crafted to maximize your affiliate commissions and take your business to the next level. Here, we are giving you an additional booster to enhance its value. </li>
						 <li>With this bonus, you get your hands on proven tested affiliate marketing strategies to promote hot products to hungry audience & boost affiliate commissions in a well-chalked out manner.  </li>
						 <li>Now all you need to do is use these proven techniques and maximize your affiliate commissions like you always wanted. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 15</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus15.png" class="img-responsive center-block">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-32 lh140 w600 bonus-title-color">
						 Conversion Boost
					  </div>
					  <ul class="bonus-list f-sm-23 f-20 lh150 w400 mt20 mt-sm30 p0">
							<li>Boosting conversions for your offers is not an easy task and proves to be quite a pain if you’re a newbie willing to establish yourself. If not given proper attention to, it yields dire consequences for many marketers. </li>
							<li>So, here’s easy to use software that helps you boost your conversions by attracting highly targeted BUYER traffic on a daily basis. Ultimately, you can maximize your affiliate commissions and build your list in a hassle free manner. </li>
							<li>Now what’s the point thinking? Just combine the strategies mentioned here along with funnel creation powers of NinjaKash, and get best results. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Coupon Code <span class="w800 purple">"ninjakash"</span> for Extra <span class="w700 purple">4% Discount</span> </div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-sm10">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <div class="f-sm-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-sm-60 f-40 lh120 w800 yellow-clr">$2375!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12 text-center p0">
                	<div class="f-sm-36 f-25 lh140 w400">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600">My 15 Bonus Products</span> are making it a <br class="hidden-xs"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab NinjaKash + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-sm-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="strip_footer clear mt20 mt-sm40">
        <div class="container">
            <div class="row">
                <img src="assets/images/logo.png" alt="logo" class="img-responsive center-block">
                <div class="col-md-12 col-sm-12 col-xs-12 f-18 f-sm-20 w400 mt15 mt-sm35 lh140 white-clr text-center">
                   Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
                    
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-3 col-xs-12 f-16 f-sm-20 w400 mt10 mt-sm58 lh140 white-clr text-xs-center">Copyright © NinjaKash</div>

                    <div class="col-sm-9 col-xs-12 f-sm-18 w400 f-16 white-clr mt10 mt-sm60 xstext-center text-right">
                        <a href="mailto:support@bizomart.com">Contact</a>&nbsp;&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/privacy-policy.html">Privacy</a>&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/terms-of-service.html">T&amp;C</a>&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/disclaimer.html">Disclaimer</a>&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/gdpr.html">GDPR</a>&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/dmca.html">DMCA</a>&nbsp;|&nbsp;
                        <a href="http://ninjakash.co/legal/anti-spam.html">Anti-Spam</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-responsive center-block mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-xs-12 modal-content pop-bg pop-padding">			
				<div class="col-xs-12 modal-body text-center border-pop px0">	
					<div class="col-xs-12 px0">
						<img src="assets/images/waitimg.png" class="img-responsive center-block">
					</div>
					
					<div class="col-xs-12 text-center mt20 mt-sm25 px0">
						<div class="f-20 f-sm-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="hidden-xs"> FOR YOU</div>
					</div>

					<div class="col-xs-12 mt20 mt-sm20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
  <!-- Facebook Pixel Code -->
  <script>
	 ! function(f, b, e, v, n, t, s) {
	  if (f.fbq) return;
	  n = f.fbq = function() {
		 n.callMethod ?
			 n.callMethod.apply(n, arguments) : n.queue.push(arguments)
	  };
	  if (!f._fbq) f._fbq = n;
	  n.push = n;
	  n.loaded = !0;
	  n.version = '2.0';
	  n.queue = [];
	  t = b.createElement(e);
	  t.async = !0;
	  t.src = v;
	  s = b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t, s)
	 }(window,
	  document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '1777584378755780');
	 fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Code for Remarketing Tag -->
  <!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  <div style="display:none;">
	 <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 748114601;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		var google_user_id = '<unique user id>';
		/* ]]> */
	 </script>
	 <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 <noscript>
		<div style="display:inline;">
		   <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
		</div>
	 </noscript>
  </div>
  <!-- Google Code for Remarketing Tag -->
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

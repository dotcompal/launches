<html>

<head>
   <title>JV Page - Vocalic JV Invite</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="JV Page - Vocalic JV Invite">
   <meta name="description" content="Futuristic A.I Based Technology Create Profit-Pulling Voiceovers and Videos in Any Language & Niche in Just 5 Minutes">
   <meta name="keywords" content="Vocalic">
   <meta property="og:image" content="https://www.vocalic.co/jv/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="JV Page - Vocalic JV Invite">
   <meta property="og:description" content="Futuristic A.I Based Technology Create Profit-Pulling Voiceovers and Videos in Any Language & Niche in Just 5 Minutes">
   <meta property="og:image" content="https://www.vocalic.co/jv/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="JV Page - Vocalic JV Invite">
   <meta property="twitter:description" content="Futuristic A.I Based Technology Create Profit-Pulling Voiceovers and Videos in Any Language & Niche in Just 5 Minutes">
   <meta property="twitter:image" content="https://www.vocalic.co/jv/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyo.com/launches/vocalic/common_assets/images/favicon.png" type="image/png">
   <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vocalic/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/vocalic/common_assets/js/jquery.min.js"></script>
</head>

<body>

   <!-- New Timer  Start-->
   <?php
   $date = 'July 17 2022 10:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: #fff;
                        }

                        .cls-1,
                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-2,
                        .cls-3 {
                           fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                           <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                           <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                        </g>
                        <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                        <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                        <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                     </g>
                  </g>
               </svg>
               <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                  <ul class="leader-ul f-18">
                     <li>
                        <a href="https://docs.google.com/document/d/1lVWhdSn0VWKGyTb9DNcO-SGRIfsnL3yK/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">JV Doc</a>
                     </li>
                     <li>|</li>
                     <li>
                        <a href="https://docs.google.com/document/d/1eyWYNBOxy4sxBRss_fODzJd4mTsomS9V/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">Swipes</a>
                     </li>
                     <li>|</li>
                     <li>
                        <a href="#funnel">Funnel</a>
                     </li>
                  </ul>
                  <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/383509" class="affiliate-link-btn ml-md15 mt10 mt-md0">Get Link</a>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                  <i> Join Us To Promote Vocalic on July 17<sup>th</sup> and Make Upto $363/Sales and $12K JV Prizes</i>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50 f-28 w800 text-center white-clr lh140">
               Futuristic A.I Based Technology Create <br class="d-none d-md-block"> <span class="yellow-clr"> Profit-Pulling Voiceovers and Videos in Any  <br class="d-none d-md-block">Language</span> & Niche in Just 5 Minutes...
            </div>
            <div class="col-12 mt-md35 mt20  text-center">
               <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                  Start Your Own Profitable Agency | Create Videos & Voiceovers in Any Niche | No Camera <br class="d-none d-md-block"> Recording | No Speaking | No Hiring of Anyone | 100% Newbie Friendly with No Tech Hassles
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-8 col-12">
                     <div class="responsive-video">
                        <iframe class="embed-responsive-item" src="https://vocalic.dotcompal.com/video/embed/busnnehojg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                              box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
                     <!-- <img src="https://cdn.oppyo.com/launches/vocalic/jv/video-thumb.webp" alt="Thumbanil" class="img-fluid mx-auto d-block"> -->
                  </div>
                  <div class="col-md-4 col-12 mt20 mt-md0">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                     <div class="clearfix"></div>
                     <div class="countdown counter-black mt20 mt-md30">
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12 w700">01</span><br><span class="f-18 w700">Days</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w700">16</span><br><span class="f-18 w700">Hours</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w700">59</span><br><span class="f-18 w700">Mins</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w700">37</span><br><span class="f-18 w700">Sec</span> </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--------Second Section------->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-10 mx-auto">
               <div class="justify-content-between d-flex">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/line.webp" class="img-fluid d-block" alt="Lines">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/line.webp" class="img-fluid d-block" alt="Lines">
               </div>
            </div>
            <div class="col-12">
               <div class="header-list-block">
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w600 black-clr text-capitalize">
                           <li><span class="w800">Convert any text into a real human-like voiceover</span> in 3 simple steps.</li>
                           <li><span class="w800">Convert any text script into a whiteboard video</span> with voiceover</li>
                           <li><span class="w800">Create video directly from</span> the image URL or upload the image from your device.</li>
                           <li><span class="w800">Add background music to any of your videos.</span></li>
                           <li><span class="w800">Store and securely share unlimited videos,</span> voiceover, and other media with integrated My Drive. </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w600 black-clr text-capitalize">
                           <li class="w800">Create voiceover in 150+ Human & AI voices and 30+ languages. </li>
                           <li>Create videos using <span class="w800">keyword search to find an image and add voiceover. </span> </li>
                           <li><span class="w800">Built-In content spinner to make your scripts unique.</span></li>
                           <li class="w800">100% newbie friendly with no tech hassles</span> </li>
                           <li>Agency license included so your <span class="w800"> buyers can start selling voiceover and video creation</span> services to make huge profits. </span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12">
               <div class="auto_responder_form inp-phold formbg col-12">
                  <div class="w800 f-md-40 f-24 lh140 w700 text-center white-clr">
                     Subscribe To Our JV List
                  </div>
                  <div class="f-md-22 f-18 d-block mb0 lh140 w600 text-center white-clr mt5">
                     and Be The First to Know Our Special Contest, Events and Discounts
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="mt15 mt-md40">
                     <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="1771406004" />
                           <input type="hidden" name="meta_split_id" value="" />
                           <input type="hidden" name="listname" value="awlist6292356" />
                           <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_24cd6414c5be75383f863080ac4673b9" />
                           <input type="hidden" name="meta_adtracking" value="Viddeyo_JV_Signup" />
                           <input type="hidden" name="meta_message" value="1" />
                           <input type="hidden" name="meta_required" value="name,email" />
                           <input type="hidden" name="meta_tooltip" value="" />
                        </div>
                        <div id="af-form-1771406004" class="af-form">
                           <div id="af-body-1771406004" class="af-body af-standards row justify-content-center gx-5">
                              <div class="af-element col-md-6">
                                 <label class="previewLabel" for="awf_field-114309216" style="display:none;">Name: </label>
                                 <div class="af-textWrap mb15 mb-md15 input-type">
                                    <input id="awf_field-114309216" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                 </div>
                                 <div class="af-clear"></div>
                              </div>
                              <div class="af-element mb15 mb-md25  col-md-6">
                                 <label class="previewLabel" for="awf_field-114309217" style="display:none;">Email: </label>
                                 <div class="af-textWrap input-type">
                                    <input class="text frm-ctr-popup form-control input-field" id="awf_field-114309217" type="text" name="email" placeholder="Your Email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                 </div>
                                 <div class="af-clear"></div>
                              </div>
                              <div class="af-element buttonContainer button-type form-btn white-clr col-md-6 mt-md20">
                                 <input name="submit" class="submit f-24 f-md-28 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                 <div class="af-clear"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=nAysbJysbGws" alt="Form" /></div>
                     </form>
                     <!-- Aweber Form Code -->
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
               <div class="f-24 f-md-30 w700 text-center lh140">
                  <span>Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
               </div>
            </div>
            <div class="col-md-12 mx-auto mt20">
               <div class="row justify-content-center align-items-center">
                  <div class="col-12 col-md-3">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/jvzoo.png" class="img-fluid d-block mx-auto" alt="Zvzoo" />
                  </div>
                  <div class="col-12 col-md-6 affiliate-btn  mt-md19 mt15">
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/383509" class="f-22 f-md-30 w700 mx-auto">Request Affiliate Link</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- exciting-launch -->
   <div class="exciting-launch">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-md-40 f-26 white-clr text-center w700 lh140">
                  This Exciting Launch <span class="yellow-clr">Event Is Divided Into 2 Phases</span>
               </div>
            </div>
            <div class="col-12 mt-md60 mt30">
               <div class="row align-items-center">
                  <div class="col-md-6 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/phase1.webp" class="img-fluid d-block mx-auto" alt="Pahse">
                  </div>
                  <div class="col-md-6 col-12 p-md0 mt-md0 mt20 order-md-1 white-clr">
                     <div class="f-md-35 f-22 lh140 w800">
                        To Make You Max Commissions
                     </div>
                     <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md30 mt20 pl20">
                        <li>All Leads Are Hardcoded </li>
                        <li>Exciting $2000 Pre-Launch Contest </li>
                        <li>We'll Re-Market Your Leads Heavily </li>
                        <li>Pitch Bundle Offer on webinars. </li>
                     </ul>
                  </div>
               </div>
               <div class="row mt30 align-items-center">
                  <div class="col-md-6 col-12">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/phase2.webp" class="img-fluid d-block mx-auto" alt="Pahse" />
                  </div>
                  <div class="col-md-6 col-12 mt-md0 mt20 white-clr">
                     <div class="f-md-35 f-22 lh140 w800">
                        Big Opening Contest & Bundle Offer
                     </div>
                     <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md30 mt20 pl20">
                        <li>High in Demand Product with Top Conversion </li>
                        <li>Deep Funnel to Make You Double-Digit EPCs </li>
                        <li>Earn up to $354/Sale </li>
                        <li>Huge $10K JV Prizes </li>
                        <li>We'll Re-Market Your Visitors Heavily</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- exciting-launch end -->
   <div class="hello-awesome">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-7">
               <div class="text-start">
                  <div class="f-md-40 f-22 lh140 w800 blue-clr heading-design">
                     Hello Awesome JV’s
                  </div>
               </div>
               <div class="f-18 f-md-20 lh160 w700 mt20 mt-md40">
                  It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner Atul Pareek (Internet Marketer & JV Manager).<br><br>
                  We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates.
                  <br><br>
                  With the combined experience of 20+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution to get rid of hiring expensive freelancers and 1000's of dollars monthly for voiceover and video creation and your subscribers and can sell these services to make huge profits.
               </div>
            </div>
            <div class="col-md-5 col-12 text-center" data-aos="fade-left">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/amit-pareek.webp" class="img-fluid d-block mx-auto mt20" alt="Dr. Amit">
               <div class="row mt10">
                  
                  <div class="col-md-6 mt-md0 mt10 mx-auto">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/atul-parrek.webp" class="img-fluid d-block mx-auto mt20" alt="Atul">
                  </div>
               </div>
            </div>
         </div>
        
                      
                  <div class="row mt-md50 mt20">
                     <div class="col-12 text-center px30">
                        <div class="shape-skew f-md-25 f-20 lh140 w800 text-center mb20 mb-md30 ">
                           <span class="skew black-clr">
                              Also, here are some stats from our previous launches:
                           </span>
                        </div>
                     </div>                    
                  </div>
                  <div class="row ">
                  <div class="col-12 col-md-11 mx-auto">
                  <div class="row ">
                     <div class="col-12 col-md-5 f-18 f-md-20 lh140 w700">
                        <ul class="awesome-list">
                           <li>Over 100 Pick of The Day Awards</li>
                           <li>Over $4Mn In Affiliate Sales for Partners</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-7 f-18 f-md-20 lh140 w700 mt10 mt-md0">
                        <ul class="awesome-list">
                           <li>Top 10 Affiliate & Seller (High Performance Leader)</li>
                           <li>Always in Top-10 of JVZoo Top Sellers</li>
                        </ul>
                     </div>
                  </div>
                  </div>
                  </div>
           
         
      </div>
   </div>
   <!-- next-generation -->
   <div class="proudly-sec" id="product">
      <div class="container">
         <div class="row">
            <!-- <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/arrow.png" class="img-fluid d-block mx-auto vert-move" alt="Arrow">
            </div> -->
            <div class="col-12 text-center lh140">
               <div class="per-shape f-28 f-md-32 w800">
                  Presenting…
               </div>
            </div>
            <div class="col-12 mt-md40 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:150px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: #fff;
                        }

                        .cls-1,
                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-2,
                        .cls-3 {
                           fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                           <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                           <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                        </g>
                        <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                        <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                        <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                     </g>
                  </g>
               </svg>
            </div>
            <div class="col-12 f-md-50 f-28 mt-md30 mt20 w800 text-center white-clr lh140">
               Breakthrough Technology TO Convert Any <br class="d-none d-md-block"> Text into <span class="yellow-clr">High In Demand Human Sounding Voiceover and Profitable Videos</span> Hands Free.
            </div>
            <div class="col-12 mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/product-image.webp" class="img-fluid d-block mx-auto img-animation" alt="Product">
            </div>
         </div>
      </div>
   </div>
   <!-- next-generation end -->
   <!-- FEATURE LIST SECTION START -->
   <div class="feature-sec">
      <div class="container">
         <div class="row align-items-center" data-aos="fade-left">
            <div class="col-md-6 col-12 ">
               <div class="f-md-26 f-20 lh140 w800 feature-shape">
                  Create Profitable Videos
               </div>
               <ul class="launch-list f-md-20 f-18 w700 mt50 pl20 black-clr">
                  <li>Convert any script into whiteboard videos with voiceover </li>
                  <li>Create videos by finding an image from a keyword
                     search and adding voiceover </li>
                  <li>Create a video using the picture from the internet
                     or uploading from your device </li>
                  <li>Add background music to your videos </li>
               </ul>
            </div>
            <div class="col-md-6 col-12 mt-md0 mt20 gif-icon">
               <video src="https://cdn.oppyo.com/launches/vocalic/jv/f1.mp4" class="feature-border" autoplay loop></video>
            </div>
         </div>
         <div class="row align-items-center mt-md80 mt30 gx-5" data-aos="fade-right">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-md-26 f-20 lh140 w800 feature-shape">
                  AI Based Text To Speech Creator
               </div>
               <ul class="launch-list f-md-20 f-18 w700 mt50 pl20 black-clr">
                  <li>150+ Human and AI Voice in 30+ languages </li>
                  <li>Customized speech rate and volume for voiceover</li>
                  <li>Use voiceover with any video editing software </li>
                  <li>Convert any voiceover into a successful podcast </li>
                  <li>Convert e-books in to audio books</li>
               </ul>
            </div>
            <div class="col-md-6 col-12 mt-md0 mt20 order-md-1 gif-icon1">
               <video src="https://cdn.oppyo.com/launches/vocalic/jv/f2.mp4" class="feature-border" autoplay loop></video>
            </div>
         </div>
         <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
            <div class="col-md-6 col-12">
               <div class="f-md-26 f-20 lh140 w800 feature-shape">
                  Built-in Content Spinner to Make your Script
                  and Content 100% Unique
               </div>
            </div>
            <div class="col-md-6 col-12 mt-md0 mt40 gif-icon">
               <video src="https://cdn.oppyo.com/launches/vocalic/jv/f3.mp4" class="feature-border" autoplay loop></video>
            </div>
         </div>
         <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-md-26 f-20 lh140 w800 feature-shape">
                  Built-In Cloud Integration to Store and Share Unlimited
                  Videos, Voiceover, and other Media Files Securely.
               </div>

            </div>
            <div class="col-md-6 col-12 mt-md0 mt-md0 mt40 order-md-1">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/f4.webp" class="img-fluid mx-auto d-block" alt="Product">
            </div>
         </div>
         <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
            <div class="col-md-6 col-12">
               <div class="f-md-26 f-20 lh140 w800 feature-shape">
                  Agency License Included. So, your buyers can start their
                  own profitable voiceover and video creation agency.
               </div>
            </div>
            <div class="col-md-6 col-12 mt-md0 mt-md0 mt40">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/f5.webp" class="img-fluid mx-auto d-block" alt="Product">
            </div>
         </div>
         <div class="row m0 mt-md100 mt30">
            <div class="col-12 text-center px30">
               <div class="shape-skew f-md-40 f-24 lh140 w800 text-center mb20 mb-md30 ">
                  <span class="skew black-clr">
                     Here are Some More Features
                  </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center">
            <div class="col-md-7">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/product-pc.webp" class="img-fluid mx-auto d-block" alt="Product">
            </div>
            <div class="col-md-5">
               <ul class="more-features f-md-22 f-18 w700 mt50 pl0 pl-md30 black-clr">
                  <li>Video Quality Options </li>
                  <li>Add Background Music</li>
                  <li>Add Your Own Watermark</li>
                  <li>Add Your Logo for Branding</li>
                  <li>3 Mn Stock Images & Videos</li>
                  <li>SEO Optimized Share Pages</li>
                  <li>Video Channel to Market Your Videos</li>
                  <li>Folder Management</li>
                  <li>Capture Unlimited Leads & Audience</li>
                  <li>Custome Domain for Share Pages
                     and Channels</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- DEMO SECTION START -->
   <div class="demo-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-md-40 f-28 w800 lh140 text-center yellow-clr">
                  Watch The Demo
               </div>
               <div class="f-md-35 f-24 w800 lh140 text-center white-clr">
                  Discover How Easy & Powerful It Is
               </div>
            </div>
            <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/demo-video-poster.webp" class="img-fluid d-block mx-auto" alt="Thumbnail">
               <!--<div class="responsive-video">
                     <iframe src="https://Academiyo.dotcompal.com/video/embed/yaa3911t5h" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>-->
            </div>
         </div>
      </div>
   </div>
   <!-- DEMO SECTION END -->
   <div class="feature-list">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-28 w800 lh140 text-center black-clr">
               Here’s a List of All Potential Use Cases <br class="d-none d-md-block">
               Vocalic Can Be Used For...
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe1.webp" class="img-fluid mx-auto d-block icon-position" alt="Sales Videos">
                  <p class="description">Sales Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe2.webp" class="img-fluid mx-auto d-block icon-position" alt="Training Videos">
                  <p class="description">Training Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe3.webp" class="img-fluid mx-auto d-block icon-position" alt="Product Videos">
                  <p class="description">Product Videos</p>
               </div>
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe4.webp" class="img-fluid mx-auto d-block icon-position" alt="Whiteboard Videos">
                  <p class="description">Whiteboard Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe5.webp" class="img-fluid mx-auto d-block icon-position" alt="Walkthrough Videos">
                  <p class="description">Walkthrough Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe6.webp" class="img-fluid mx-auto d-block icon-position" alt="Promotional Ad Videos">
                  <p class="description">Promotional Ad Videos</p>
               </div>
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe7.webp" class="img-fluid mx-auto d-block icon-position" alt="Affiliate Review Videos">
                  <p class="description">Affiliate Review Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe8.webp" class="img-fluid mx-auto d-block icon-position" alt="Movie Reviews">
                  <p class="description">Movie Reviews</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe9.webp" class="img-fluid mx-auto d-block icon-position" alt="Youtube Shorts">
                  <p class="description">Youtube Shorts</p>
               </div>
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe10.webp" class="img-fluid mx-auto d-block icon-position" alt="Webinar Presentation Videos">
                  <p class="description">Webinar Presentation Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe11.webp" class="img-fluid mx-auto d-block icon-position" alt="Business Presentation Videos">
                  <p class="description">Business Presentation Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe12.webp" class="img-fluid mx-auto d-block icon-position" alt="Voiceovers">
                  <p class="description">Voiceovers</p>
               </div>
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe13.webp" class="img-fluid mx-auto d-block icon-position" alt="Podcast">
                  <p class="description">Podcast</p>
               </div>
            </div>
            <div class="col-12 col-md-4 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe14.webp" class="img-fluid mx-auto d-block icon-position" alt="and Much More...">
                  <p class="description">and Much More...</p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="deep-funnel" id="funnel">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-24 w800 text-center lh140">
               Our Deep & High Converting Sales Funnel
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70">
               <div>
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/funnel.webp" class="img-fluid d-block mx-auto" alt="Funnel">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="prize-value">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-45 f-28 w800 text-center white-clr lh140">
               Get Ready to Grab Your Share of
               <span class="yellow-clr"> $12000 JV Prizes</span>
            </div>
         </div>
      </div>
   </div>
   <div class="contest-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-28 w800 text-center black-clr">
               Pre-Launch Lead Contest
            </div>
            <div class="col-12 f-md-18 f-18 w500 lh140 mt20 text-center">
               Contest Starts on 15th July’22 at 10:00 AM EST and Ends on 17th July’22 at 11:00 AM EST
            </div>
            <div class="col-12 f-md-18 f-18 w500 lh140 mt10 text-center">
               (Get Flat <span class="w800">$0.50c</span>  For Every Lead You Send for <span class="w800">Pre-Launch Webinar</span>)
            </div>
            <div class="col-12 col-md-10 mx-auto mt20 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/contest-img1.webp" class="img-fluid d-block mx-auto" alt="Prize">
                  </div>
                  <div class="col-12 col-md-7">
                     <img src="https://cdn.oppyo.com/launches/vocalic/jv/contest-img2.webp" class="img-fluid d-block mx-auto" alt="Prize">
                  </div>
               </div>
            </div>
            <div class="col-12 f-18 f-md-18 lh140 mt-md70 mt20 w500 text-center black-clr">
               *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads)
            </div>
         </div>
      </div>
   </div>
   <div class="prize-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/prize-img1.webp" class="img-fluid d-block mx-auto" alt="Prize">
            </div>
            <div class="col-12 mt20 mt-md70">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/prize-img2.webp" class="img-fluid d-block mx-auto" alt="Prize">
            </div>
            <div class="col-12 f-18 f-md-20 w800 lh100 mt20 mt-md50 mb10 mb-md20 black-clr" alt="Prize">
               *Contest Policies:
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140">
               1. Team of maximum two is allowed.<br> <span class="mt10 d-block">
                  2. To be eligible to win one of the sales leaderboard prizes, you must have made commissions equal to or greater than the value of the prize. If this criterion is not met, then you will be eligible for the next prize.
            </div>
            <div class="col-12 col-md-10 mx-auto mt20 mt-md55">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/prize-img3.webp" class="img-fluid d-block mx-auto" alt="Prize">
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="f-18 f-md-20 w400 lh140">
                  <span class="w800 d-md-block">Note:</span>
                  We will announce the winners on 15th July & Prizes will be distributed through Payoneer from 17th July Onwards..
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="reciprocate-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-md-40 f-28 lh140 w800 black-clr">
                  Our Solid Track Record of <br class="d-none d-md-block"> Launching Top Converting Products
               </div>
            </div>
            <div class="col-12 col-md-12 mt20">
               <img src="https://cdn.oppyo.com/launches/vocalic/jv/product-logo.webp" class="img-fluid d-block mx-auto" alt="Product">
            </div>
            <div class="col-12 text-center mt50">
               <div class="f-md-45 f-28 lh140 w800 white-clr feature-shape">
                  Do We Reciprocate?
               </div>
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 mt50 text-center black-clr">
               We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs. <br><br>
               So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are just some results from our recent promotions.
            </div>
            <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <div class="logos-effect">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/logos.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            <div class="col-12 f-md-28 f-22 lh140 mt20 mt-md50 w800 text-center">
               And The List Goes On And On...
            </div>
         </div>
      </div>
   </div>
   <div class="contact-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                  <span class="w800"> Have any Query?</span> Contact us Anytime
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50 ">
            <div class="col-md-4 col-12 text-center mx-auto">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/amit-pareek-sir.webp" class="img-fluid d-block mx-auto" alt="Dr. Amit">
                  <div class="f-22 f-md-32 w800 mt15 mt-md20 lh140 text-center white-clr">
                     Dr Amit Pareek
                  </div>
                  <div class="f-18 w600 lh140 text-center white-clr">
                     (Techpreneur & Marketer)
                  </div>
                  <div class="col-12 mt30 d-flex justify-content-center">
                     <a href="skype:amit.pareek77" class="link-text mr20">
                        <div class="col-12 ">
                           <img src="https://cdn.oppyo.com/launches/vocalic/jv/skype.webp" class="center-block" alt="Call">
                        </div>
                     </a>
                     <a href="http://facebook.com/Dr.AmitPareek" class="link-text">
                        <img src="https://cdn.oppyo.com/launches/vocalic/jv/am-fb.webp" class="center-block" alt="Call">
                     </a>
                  </div>
               </div>
           
            <div class="col-md-4 col-12 text-center mt30 mt-md0 mx-auto">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/atul-parrek-sir.webp" class="img-fluid d-block mx-auto" alt="Atul Parrek">
                  <div class="f-22 f-md-32 w800 mt15 mt-md20  lh140 text-center white-clr">
                     Atul Pareek
                  </div>
                  <div class="f-18 w600  lh140 text-center white-clr">
                     (Entrepreneur & JV Manager)
                  </div>
                  <div class="col-12 mt30 d-flex justify-content-center">
                     <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text mr20">
                        <div class="col-12 ">
                           <img src="https://cdn.oppyo.com/launches/vocalic/jv/skype.webp" class="center-block" alt="Call">
                        </div>
                     </a>
                     <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text">
                        <img src="https://cdn.oppyo.com/launches/vocalic/jv/am-fb.webp" class="center-block" alt="Call">
                     </a>
                  </div>
            </div>
         </div>
      </div>
   </div>
   <div class="terms-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-28 w800 lh140 text-center">
               Affiliate Promotions Terms & Conditions
            </div>
            <div class="col-md-12 col-12 f-18 f-md-20 w500 lh140 p-md0 mt-md20 mt20  text-center">
               We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below:
            </div>
            <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
               <ul class="terms-list pl0 m0 f-16 lh140 w500">
                  <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No
                     exceptions will be entertained.
                  </li>
                  <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed
                     from our system with immediate effect.
                  </li>
                  <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
                  <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                  </li>
                  <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:60px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: #fff;
                        }

                        .cls-1,
                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-2,
                        .cls-3 {
                           fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                           <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                           <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                        </g>
                        <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                        <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                        <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                     </g>
                  </g>
               </svg>
               <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Vocalic</div>
               <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
<a id="button"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
<g>
	<g>
		<path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z"/>
	</g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg></a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {

   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w800">' + days + '</span><br><span class="f-18 w700">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w800">' + hours + '</span><br><span class="f-18 w700">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg w800">' + minutes + '</span><br><span class="f-18 w700">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 lh100 timerbg w800">' + seconds + '</span><br><span class="f-18 w700">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
            function browserSupportsNewWindows(userAgent) {
               var rules = [
                  'FBIOS',
                  'Twitter for iPhone',
                  'WebView',
                  '(iPhone|iPod|iPad)(?!.*Safari\/)',
                  'Android.*(wv|\.0\.0\.0)'
               ];
               var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
               return !pattern.test(userAgent);
            }

            if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
               document.getElementById('af-form-1771406004').parentElement.removeAttribute('target');
            }
         })();
      </script>
      <script type="text/javascript">
         (function() {
            var IE = /*@cc_on!@*/ false;
            if (!IE) {
               return;
            }
            if (document.compatMode && document.compatMode == 'BackCompat') {
               if (document.getElementById("af-form-1771406004")) {
                  document.getElementById("af-form-1771406004").className = 'af-form af-quirksMode';
               }
               if (document.getElementById("af-body-1771406004")) {
                  document.getElementById("af-body-1771406004").className = "af-body inline af-quirksMode";
               }
               if (document.getElementById("af-header-1771406004")) {
                  document.getElementById("af-header-1771406004").className = "af-header af-quirksMode";
               }
               if (document.getElementById("af-footer-1771406004")) {
                  document.getElementById("af-footer-1771406004").className = "af-footer af-quirksMode";
               }
            }
         })();
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
<script>var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

</script>
</body>

</html>
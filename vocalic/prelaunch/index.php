<!Doctype html>
<html>

<head>
   <title>Vocalic Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="Vocalic | Prelaunch">
   <meta name="description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta name="keywords" content="Vocalic">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.vocalic.co/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="Vocalic | Prelaunch">
   <meta property="og:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta property="og:image" content="https://www.vocalic.co/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="Vocalic | Prelaunch">
   <meta property="twitter:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta property="twitter:image" content="https://www.vocalic.co/prelaunch/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyo.com/launches/vocalic/common_assets/images/favicon.png" type="image/png">
   <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vocalic/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/vocalic/common_assets/js/jquery.min.js"></script>
   <script>
      function getUrlVars() {
         var vars = [],
            hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
         for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
         }
         return vars;
      }
      var first = getUrlVars()["aid"];
      //alert(first);
      document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value', first);
      })
      //document.getElementById('myField').value = first;
   </script>
</head>

<body>
   
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row align-items-center">
                  <div class="col-md-3 text-center text-md-start">
                     <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }

                              .cls-1,
                              .cls-2 {
                                 fill-rule: evenodd;
                              }

                              .cls-2,
                              .cls-3 {
                                 fill: #00aced;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2">
                           <g>
                              <g>
                                 <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                                 <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                                 <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                                 <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                                 <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                              </g>
                              <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                              <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                              <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                              <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                              <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                              <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                              <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                           </g>
                        </g>
                     </svg>
                  </div>
                  <div class="col-md-9 mt20 mt-md5">
                     <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                        <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                  <i> Register for One Time Only Value Packed Free Training & Get an Assured Gift + 10 Free Licenses</i>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50 f-28 w800 text-center white-clr lh140">
               Discover How to Market & Monetize Your Videos And Instantly Captivate Your Audience To Boost Conversions And ROI
            </div>
            <div class="col-12 mt-md35 mt20  text-center">
               <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
               In this Webinar, we will also reveal, how we have sold over $7 mn of digital products using <br class="d-none d-md-block"> the
               power of videos and saved 100’s of dollars monthly on money sucking video hosting apps.
               </div>
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/video-thumb.gif" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                  <div class="calendar-wrap-inline">
                     <div class="f-22 f-md-26 w800 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">17<sup contenteditable="false">th</sup> JULY, 11 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="60767494" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6297358" />
                              <input type="hidden" name="redirect" value="https://www.vocalic.co/prelaunch-thankyou" id="redirect_af0835fd38ddb11f920dda073d20c3a6" />
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-60767494" class="af-form">
                              <div id="af-body-60767494" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114353285"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114353285" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114353286"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114353286" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- Header Section Start -->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-36 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                           Why Attend This Free Training Webinar & What Will You Learn?
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                           Our 20+ Years of experience is packaged in this state-of-art 1-hour webinar where You’ll learn:
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li>How we have sold over $7Mn in digital products using the power of video marketing and you can follow the same to build a profitable business online. </li>
                           <li>How you can Boost your Conversions and Sales by simply using the Power of Video Marketing. </li>
                           <li>How Videos can help to build trust among your audience and help in improving ROI. </li>
                           <li>How you can Tap into the fastest growing video marketing & video agency market without any tech hassles. </li>
                           <li>How we have saved 100s of dollars monthly on money sucking expensive video marketing platforms and still not getting desired results. </li>
                           <li>A Sneak-Peak of "VOCALIC", A BLAZING-FAST Video Hosting, Player & Marketing Technology– All from One Platform </li>
                           <li>During This Launch Special Deal, Get All Benefits at a Limited Low One-Time-Fee. </li>
                           <li>Lastly, everyone who attends this session will get an assured gift from us plus we are giving 10 licenses of our Premium solution - VOCALIC </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Discounted Price Section Start -->
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh140 white-clr" editabletype="text" style="z-index:10;">
                     So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh140 mx-auto my20 text-center white-clr">
                     Register Now! And Join Us Live On July 17th, 11:00 AM EST
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="60767494">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6297358">
                           <input type="hidden" name="redirect" value="https://www.vocalic.co/prelaunch-thankyou" id="redirect_af0835fd38ddb11f920dda073d20c3a6">
                           <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-60767494" class="af-form">
                           <div id="af-body-60767494" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114353285"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-114353285" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114353286"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-114353286" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- Discounted Price Section End -->
      </div>
   </div>
   <!-- Header Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:60px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: #fff;
                        }

                        .cls-1,
                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-2,
                        .cls-3 {
                           fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                           <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                           <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                        </g>
                        <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                        <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                        <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                     </g>
                  </g>
               </svg>
               <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Vocalic</div>
               <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z"/>
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <script>var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

</script>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-60767494').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">

    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-60767494")) {
                document.getElementById("af-form-60767494").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-60767494")) {
                document.getElementById("af-body-60767494").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-60767494")) {
                document.getElementById("af-header-60767494").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-60767494")) {
                document.getElementById("af-footer-60767494").className = "af-footer af-quirksMode";
            }
        }
    })();
</script>
</body>

</html>
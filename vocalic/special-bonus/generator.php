<!DOCTYPE html>
<html>

   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Vocalic Special Bonuses">
      <meta name="description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
      <meta name="keywords" content="Vocalic Special Bonuses">
      <meta property="og:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Vocalic Special Bonuses">
      <meta property="og:description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
      <meta property="og:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Vocalic Special Bonuses">
      <meta property="twitter:description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
      <meta property="twitter:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
      
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/vocalic/common_assets/images/favicon.png" type="image/png">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vocalic/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">

   </head>

   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
			      <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:60px;"><defs><style>.cls1,.cls2{fill-rule:evenodd;}.cls1,.cls3{fill:#00aced;}.cls2{fill:#0f172b;}</style></defs><g id="Layer_1-2"><g><g><path class="cls3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z"/><path class="cls3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z"/><path class="cls3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z"/><path class="cls3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z"/><path class="cls3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z"/></g><path class="cls2" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z"/><path class="cls2" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z"/><path class="cls2" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z"/><path class="cls2" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z"/><path class="cls2" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z"/><path class="cls2" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z"/><path class="cls1" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z"/></g></g></svg>
               </div>
               <div class="col-12 f-md-45 f-28 w800 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>

      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>

               
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                  <?php
                     if(isset($_POST['submit'])) 
                        {
                           $name=$_REQUEST['name'];
                           $afflink=$_REQUEST['afflink'];
                           //$picname=$_REQUEST['pic'];
                           /*$tmpname=$_FILES['pic']['tmp_name'];
                           $type=$_FILES['pic']['type'];
                           $size=$_FILES['pic']['size']/1024;
                           $rand=rand(1111111,9999999999);*/
                           if($afflink=="")
                           {
                              echo 'Please fill the details.';
                           }
                           else
                           {
                              /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                              {
                                 echo "Please upload image file (size must be less than 1 MB)";	
                              }
                              else
                              {*/
                                 //$filename=$rand."-".$picname;
                                 //move_uploaded_file($tmpname,"images/".$filename);
                                 $url="https://www.vocalic.co/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                                 echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                                 //header("Location:$url");
                              //}	
                           }
                        }
                  ?>
					   <form class="row" action="" method="post" enctype="multipart/form-data">
                     <div class="col-12 form_area ">
                        <div class="col-12 clear">
                           <input type="text" name="name" placeholder="Your Name..." required>
                        </div>

                        <div class="col-12 mt20 clear">
                           <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                        </div>

                        <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                           <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                        </div>
						   </div>
					   </form>
				   </div>
            </div>
         </div>
      </div>
    
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:60px;">
                     <defs>
                        <style>
                           .cls-1 {
                              fill: #fff;
                           }

                           .cls-1,
                           .cls-2 {
                              fill-rule: evenodd;
                           }

                           .cls-2,
                           .cls-3 {
                              fill: #00aced;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                              <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                              <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                              <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                              <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           </g>
                           <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                           <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                           <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                           <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                           <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                           <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                           <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                        </g>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Vocalic</div>
                     <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://www.vocalic.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>   
      <!--Footer Section End -->
   </body>

</html>
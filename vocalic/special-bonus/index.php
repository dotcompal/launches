<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="Vocalic Special Bonuses">
   <meta name="description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="Vocalic Special Bonuses">
   <meta property="og:description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
   <meta property="og:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="Vocalic Special Bonuses">
   <meta property="twitter:description" content="Get Premium, Limited Time Bonuses With Your Vocalic Purchase & Make It Even Better">
   <meta property="twitter:image" content="https://www.vocalic.co/special-bonus/thumbnail.png">
   <title>Vocalic Special Bonuses</title>

   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <!-- Shortcut Icon  -->
   <link rel="icon" href="https://cdn.oppyo.com/launches/vocalic/common_assets/images/favicon.png" type="image/png">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vocalic/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/vocalic/common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'July 18 2022 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://jvz2.com/c/10103/383509/';
      $_GET['name'] = 'Dr. Amit Pareek';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                        <defs>
                           <style>
                              .cls-1 {
                                 fill: #fff;
                              }

                              .cls-1,
                              .cls-2 {
                                 fill-rule: evenodd;
                              }

                              .cls-2,
                              .cls-3 {
                                 fill: #00aced;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2">
                           <g>
                              <g>
                                 <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                                 <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                                 <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                                 <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                                 <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                              </g>
                              <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                              <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                              <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                              <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                              <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                              <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                              <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                           </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i> Grab My 20 Exclusive Bonuses Before the Deal Ends… </i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w800 text-center white-clr lh140">
                  Revealing A <span class="yellow-clr">Futuristic A.I. Technology Creates Profit-Pulling Videos and Voiceovers in Any Language & Niche</span> With No Monthly Fee Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                     Watch My Quick Review Video
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md30">
            <div class="col-12 col-md-10 mx-auto">
               <!-- <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
               <div class="responsive-video">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/6w3n52dn8w" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section Start -->
   
   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video & Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create & Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard & Explainer Videos</span></li>     
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>             
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                           
                           <li><span class="w700">Boost Engagement & Sales</span> Using The Power Of Videos</li>
                           <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                           <li><span class="w700">Built-In Content Spinner</span> To Make Your Scripts Unique.</li>
                           <li><span class="w700">Store Your Videos, Voiceover, And Other Media Files</span> With Integrated Mydrive</li>
                           <li><span class="w700">100% Newbie Friendly –</span> No Camera, Audio & Tech Hassles Ever</li>
                           <li><span class="w700">Commercial License Included</span> To Build On Incredible Income Helping Clients</li>
                           <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Header Section End -->

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!-- Step Section -->
   <div class="step-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-24 w800 text-center black-clr lh140">
               Create Stunning Videos & Voiceovers  <br class="d-none d-md-block">
            </div>
            <div class="col-12 f-md-48 f-28 w800 text-center black-clr lh140">
               For You or Clients <span class="blue-clr">in Just 3 Easy Steps</span>
            </div>
         </div>
         <div class="row mt20 mt-md50 align-items-center">
            <div class="col-12 col-md-5">
               <div class="step-shape">Step 1</div>
               <div class="f-22 f-md-28 w800 lh140 blue-clr mt15">Choose</div>
               <div class="f-18 w400 lh150 black-clr mt10">
                  You get access the Vocalic dashboard as soon as you log in.
                  <br><br>
                  Here you can choose whether you want to <span class="w800"> a video, voiceover, podcast, or YouTube Shorts video.</span>
               </div>
               <img src="https://cdn.oppyo.com/launches/vocalic/special/step-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
            </div>
            <div class="col-12 col-md-7 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/step1.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-12 col-md-5 order-md-2">
               <div class="step-shape">Step 2</div>
               <div class="f-22 f-md-28 w800 lh140 blue-clr mt15">Create </div>
               <div class="f-18 w400 lh150 black-clr mt10">
                  Just copy-paste your text script to convert it into real human-like voiceovers & stunning videos in 30+ languages instantly. Or simply use a keyword & let vocalic find beautiful images to use in your video.<br><br>

                  In Vocalic, you can add text, images, watermark, & background music to your videos & easily modulate the tone, pitch, and speed of your voiceover.<br><br>
                  
                  <span class="w800">No camera, no speaking a single word or tech hassles – it’s super easy.</span>
               </div>
               <img src="https://cdn.oppyo.com/launches/vocalic/special/step-arrow1.webp" class="img-fluid d-none d-md-block step-arrow1">
            </div>
            <div class="col-12 col-md-7 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/step2.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-12 col-md-5">
               <div class="step-shape">Step 3</div>
               <div class="f-22 f-md-28 w800 lh140 blue-clr mt15">Publish, Sell and Profit</div>
               <div class="f-18 w400 lh150 black-clr mt10">Now let your profit-pulling videos work for you for passive income over and over again.
               </div>
               <ul class="features-list f-18 w400 lh150 black-clr mt20">
                  <li><span class="w800">Drive more engagement, leads & sales.</span>
                  </li>
                  <li><span class="w800">Sell videos & voiceovers services for big bucks</span> to business owners in 100+ niches.
                  </li>
                  <li>With a click, directly publish videos to your <span class="w800">YouTube channels and drive ton of traffic.</span></li>
                  <li><span class="w800">Or make passive affiliate commissions</span> with product review videos</li>
                  <li class="w800">Possibilities are ENDLESS.
                  </li>
               </ul>
            </div>
            <div class="col-12 col-md-7 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/step3.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>            
      </div>
   </div>
   <!-- Step Section End -->

   <!-- Know Section -->
   <div class="know-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 f-26 f-md-40 w400 lh140 black-clr text-center">
               Look At Some Real-Life Proofs,
            </div>
            <div class="col-12 f-28 f-md-48 w800 lh140 blue-clr text-center mt5">
               New Freelancers & Agencies are Making Tons of Money Creating Videos & Voiceovers for Clients
            </div>
            <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/freelancers.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 col-md-11 mx-auto mt20 mt-md20">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6 relative">
                     <img src="https://cdn.oppyo.com/launches/vocalic/special/arrow1.webp" alt="" class="img-fluid d-none d-md-block ms-md-auto arrow1">
                     <div class="f-22 f-md-28 w700 lh150 black-clr">
                        See, Aaron is charging $250 on Fiver for creating a simple 1-minute voiceover for his clients. 
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="https://cdn.oppyo.com/launches/vocalic/special/fiver.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-11 mx-auto mt20 mt-md20">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6 order-md-2 relative">
                     <img src="https://cdn.oppyo.com/launches/vocalic/special/arrow.webp" alt="" class="img-fluid d-none d-md-block ms-md-auto arrow">
                     <div class="f-22 f-md-28 w700 lh150 black-clr">
                        Also, Paty is charging $500 for<br class="d-none d-md-block"> 5-minute voiceover
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/vocalic/special/charging.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>

         </div>
         
         
         <div class="row mt20 mt-md0 align-items-center">
            <div class="col-12 col-md-7 relative">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/arrow1.webp" alt="" class="img-fluid d-none d-md-block ms-md-auto arrow2">
               <div class="f-20 f-md-28 w500 lh150 black-clr">
                  So, how would you feel If I told you that You too can create professional videos and voiceovers in any niche or language
               </div>
               <div class="f-22 f-md-34 w800 lh150 blue-clr mt20">
                  And charge $100-500 for SIMPLE <br class="d-none d-md-block">60 second of work again and again…
               </div>
               <div class="f-40 f-md-80 w800 lh150 black-clr mt10 mt-md5">
                  Interested?
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/interested.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
      </div>
   </div>
   <!-- Know Section End -->

   <!-- Features Section  -->
   <div class="feature-list">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-40 f-24 w800 text-center black-clr lh140">
               With Vocalic, Easily Create Any Type of Video
            </div>
            <div class="col-12 f-md-48 f-28 w800 text-center black-clr lh140">
               in ANY Niche for You &amp; Your Clients!      
            </div>
            <div class="col-12 f-md-22 f-18 w400 lh140 text-center black-clr mt15">
               Your clients will love you for the sales and growth you deliver them by stunning videos. They will come again,<br class="d-none d-md-block"> and again for more &amp; you build a long-term, sustainable agency business for you!               
            </div>
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe7.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Affiliate Review Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe9.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Youtube Shorts</p>
               </div>
            </div>

            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe1.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Sales Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe4.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Whiteboard Videos</p>
               </div>
            </div>              
            
         </div>
         <div class="row gx-5">
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe3.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Product Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe2.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Training Videos</p>
               </div>
            </div>
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe6.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">Video Ads</p>
               </div>
            </div>
            <div class="col-12 col-md-3 mt30">
               <div class="feature-list-box">
                  <img src="https://cdn.oppyo.com/launches/vocalic/jv/fe14.webp" class="img-fluid mx-auto d-block icon-position">
                  <p class="description">and Much More...</p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Features Section End -->

   <!-- Demo Section  -->
   <div class="demo-sec" id="demo">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-md-48 f-28 w800 lh140 text-center yellow-clr">
                  Watch Vocalic in Action:
               </div>
               <div class="f-md-32 f-22 w800 lh140 text-center white-clr">
                  "Voice Over Is Proudly Created Using Vocalic"
               </div>
            </div>
            <div class="col-12 col-md-10 mx-auto mt-md40 mt20">
               <div class="responsive-video">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/26qdyix57s" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen"
                     allowfullscreen></iframe>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://jvz2.com/c/10103/383509/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">03</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">31</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Demo Section End -->

   <!-- Video Section  -->
   <div class="video-section">
      <div class="container">
         <div class="row">
            <div class="f-md-40 f-24 w800 lh140 text-center black-clr">
               Checkout Some of Whiteboard & Review 
            </div>
            <div class="f-md-48 f-26 w800 lh140 text-center black-clr">
               Videos Created Using Vocalic 
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
            <div class="col">
               <div class="responsive-video border-video">
               <iframe src="https://vocalic.dotcompal.com/video/embed/l3uzek2pzu"
                     style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                     allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col mt20 mt-md0">
               <div class="responsive-video border-video">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/xmz5218o9o"
                     style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                     allowfullscreen=""></iframe>
               </div>
            </div>
            
            <div class="col mt-md30 mt20">
               <div class="responsive-video border-video">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/lu55tby1pb"
                     style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                     allowfullscreen=""></iframe>
               </div>
            </div>

            <div class="col mt20 mt-md30">
               <div class="responsive-video border-video">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/i61vle8tym"
                     style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                     allowfullscreen=""></iframe>   
               </div>
            </div>
            
            
         </div>
      </div>
   </div>
   <!-- Video Section End  -->


   <!-- Testimonials Section -->
   <div class="testimonial-section">
      <div class="container ">
         <div class="row ">
            <div class="f-md-40 f-24 w800 lh140 text-center black-clr">
               Here Is What Few Of Our Beta Users
            </div>
            <div class="f-md-48 f-26 w800 lh140 text-center black-clr">
               Say About Vocalic
            </div>
         </div>
         <div class="row mt25 mt-md50 align-items-center">
            <!-- <div class="col-md-6">
               <div class="f-md-22 f-20 w800 lh140 text-center blue-clr">
                  Mark Wilson Created a Testimonial<br class="d-none d-md-block"> for Us Using Vocalic.  
               </div>
               <div class="responsive-video border-video mt20">
                  <iframe src="https://vocalic.dotcompal.com/video/embed/t4kfasi8bd"
                     style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                     allowfullscreen=""></iframe>
               </div>
            </div> -->
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/testi8.webp" alt="" class="img-fluid d-block mx-auto">
            </div>   
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/testi1.webp" alt="" class="img-fluid d-block mx-auto">
            </div>            

         </div>
         <div class="row mt25 mt-md20 align-items-center">
            <div class="col-md-6">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/testi2.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/testi3.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
         </div>
      </div>
   </div>
   <!-- Testimonials Section End -->


   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">03</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">21</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">32</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>


   <!-- Stats Section -->
   <div class="stats-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-33 f-24 w700 lh150 blue-clr text-center ">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/over-img.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
         <div class="row mt20 mt-md50 align-items-center">
            <div class="col-12 col-md-7">
               <div class="f-18 f-md-20 w400 lh150 black-clr">
                  <span class="w800"> In this never-ending pandemic era, </span> every business 
                  needs to switch to digital media for marketing, and thus, 
                  the need to create professional videos with engaging 
                  voiceover skyrockets without a doubt. 
                  
                  <br><br> <span class="w800"> This has forced more than 86% of businesses 
                     to use videos as a marketing tool. </span>
               </div>
            </div>
            <div class="col-12 col-md-5 mx-auto mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/businesses.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 mt20 mt-md70 text-center">
               <div class="f-20 f-md-24 w700 lh150 blue-clr">
                  So, using videos in marketing campaigns for any business is not an option anymore...
               </div>
            </div>
            <div class="col-12 text-center mt20">
               <div class="f-md-40 f-22 w800 lh140 white-clr red-shape">
                  It’s a Compulsion Today!
               </div>
            </div>
         </div>
         <div class="row mt40 mt-md70 align-items-center">
            <div class="col-12 mx-auto">
               <div class="stats-wrapper">
                  <div class="col-12 f-28 f-md-45 w800 lh140 white-clr text-center">
                     And Here Are Some More Eye-Opening Facts! 
                  </div>
                  <div class="row align-items-center mt20 mt-md50">
                     <div class="col-12 col-md-3">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/stats1.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md0">
                        <div class="f-22 f-md-28 w600 lh150 white-clr text-center text-md-start">
                           <span class="w800">Improvement in engagement, sales, and ROI</span> <br>
                           using videos are reported by the Top Marketers 
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt20 mt-md50">
                     <div class="col-12 col-md-3">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/stats2.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md0">
                        <div class="f-22 f-md-28 w600 lh150 white-clr text-center text-md-start">
                           <span class="w800">Increase in voice-over projects</span> <br>
                           on freelancer portals in the last two years.                            
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt20 mt-md50">
                     <div class="col-12 col-md-3">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/stats3.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md0">
                        <div class="f-22 f-md-28 w600 lh150 white-clr text-center text-md-start">
                           <span class="w800">Increase in Jobs</span> <br>
                           for Videos Creation Since 2021                            
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt20 mt-md50">
                     <div class="col-12 col-md-3">
                        <img src="https://cdn.oppyo.com/launches/vocalic/special/stats4.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md0">
                        <div class="f-22 f-md-28 w600 lh150 white-clr text-center text-md-start">
                           <span class="w800"> Boost in Email CTR & Traffic</span> <br>if use videos

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 f-20 f-md-24 w700 lh140 black-clr text-center">
               So, when it comes to doing a Business, 
            </div>
            <div class="col-12 f-28 f-md-50 w800 lh140 black-clr text-center my10">
               Videos are the Present and The Future!
            </div>
            <div class="col-12 f-20 f-md-24 w700 lh140 black-clr text-center">
               In Easy Words, Every Business Need Videos Today!
            </div>
            <div class="col-12 mt-md50 mt20 f-md-28 f-22 w800 blue-clr lh140 text-center">
               And that opens a door to
            </div>
            <div class="col-12 mt20 f-md-50 f-28 w800 white-clr lh140 text-center">
               <div class="over-shape">
                  HUGE $200 Billion Opportunity
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Stats Section End -->

   <!-- proudly Section -->
   <div class="proudly-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center lh140">
               <div class="per-shape f-28 f-md-32 w800">
                  Presenting
               </div>
            </div>
            <div class="col-12 mt-md40 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100"
                  style="max-height:150px;">
                  <defs>
                     <style>
                        .cls-1 {
                        fill: #fff;
                        }
                        .cls-1,
                        .cls-2 {
                        fill-rule: evenodd;
                        }
                        .cls-2,
                        .cls-3 {
                        fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3"
                              d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                           </path>
                           <path class="cls-3"
                              d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                           </path>
                           <path class="cls-3"
                              d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                           </path>
                           <path class="cls-3"
                              d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                           </path>
                           <path class="cls-3"
                              d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                           </path>
                        </g>
                        <path class="cls-1"
                           d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                        </path>
                        <path class="cls-1"
                           d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                        </path>
                        <path class="cls-1"
                           d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                        </path>
                        <path class="cls-1"
                           d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                        </path>
                        <path class="cls-1"
                           d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                        </path>
                        <path class="cls-1"
                           d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                        </path>
                        <path class="cls-2"
                           d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                        </path>
                     </g>
                  </g>
               </svg>
            </div>
            <div class="col-12 f-md-48 f-28 mt-md30 mt20 w800 text-center white-clr lh140">
               Breakthrough Technology Converts Any Text into Human-Sounding Voiceover and Profitable Videos Hands-Free 
            </div>
            <div class="col-12 f-20 f-md-22 lh140 w500 text-center white-clr mt20">
               NO Camera. No Speaking a Single Word. No Tech Hassles Ever. 
            </div>
         </div>
         <div class="row mt20 mt-md50 align-items-center">
            <div class="col-md-6">
               <img src="https://cdn.oppyo.com/launches/vocalic/special/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                  alt="Product">
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <div class="proudly-list-bg">
                  <div class="col-12">
                     <ul class="proudly-list f-18 w500 lh150 black-clr">
                        <li>
                           <span class="f-22 f-md-24 w800">Create Any Type of Video </span>
                           <div class="mt5">Create Stunning Whiteboard, YouTube Shorts, Sales, Product Training, Affiliate Reviews Videos, etc. 
                           </div>
                        </li>
                        <li>
                           <span class="f-22 f-md-24 w800">Convert Any Text to Speech </span>
                           <div class="mt5">Create Unlimited Voiceovers and Podcast in 150+ Voice and 30+ Languages 
                           </div>
                        </li>
                        <li>
                           <span class="f-22 f-md-24 w800">Newbie Friendly & Easy To Use  </span>
                           <div class="mt5">No Camera, No Speaking, No Tech Hassle, and No Prior Experience is Needed 
                           </div>
                        </li>
                        <li>
                           <span class="f-22 f-md-24 w800">No Limits - Use for Yourself or Clients</span>
                           <div class="mt5">Comes with Commercial License To Sell Video & Voiceovers to Your Clients for BIG Bucks 
                           </div>
                        </li>
                        <li>
                           <span class="f-22 f-md-24 w800">
                              No Worries About Paying Monthly 
                           </span>
                           <div class="mt5">During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee. 
                           </div>
                        </li>
                        <li>
                           <span class="f-22 f-md-24 w800">
                              50+ Other Amazing Features 
                           </span>
                           <div class="mt5">We’ve Left No Stone Unturned to Give You an Unmatched Experience 
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- proudly Section End -->
  
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase Vocalic, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">Evergreen Infographics Pack</div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li class="w700">
                           One of the most important types of visuals for a business owners in the online arena are infographics.
                        </li>
                        <li>
                           Infographics are used to showcase statistics, to sell products, to advertise, promote, attract and connect better with your audience.
                        </li>
                        <li>
                           It is the visual representation that you showcase to your potential customers and that sends a clear message to them!
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color mt20 mt-md0">
                        Find Your Niche
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li class="w700">
                           Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. 
                        </li>
                        <li>
                           Unfortunately, too many new business owners fail within their first year. 
                        </li>
                        <li>
                           While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Email Monetizer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li class="w700">Turning your email list into a passive income money maker isn’t as difficult, or time consuming as you may think.</li>
                        <li>Every day, thousands of online marketers are transforming their mailing lists into powerful cash funnels, and quite often, they don’t even have their own product line!</li>
                        <li>This special report will make it easy for you to start making money with your subscriber base even if you’re just starting out.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Background 4K Stock Videos
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li class="w700">
                           By 2019, Internet Video Traffic will account for 80% of all consumer Internet traffic. Here is an excellent opportunity to leverage the power of Videos and use this medium to catapult your web business to the next level. 
                        </li>
                        <li>
                           There are Endless Possibilities where you can use these videos and here are just some of them.
                           <br> <br>
                           <b>You can:</b> 
                        </li>
                        <li>Use them as Background video on your sales page/sales video to enchance its appearance</li>
                        <li>Use them as Sales video on your Squeeze Page/Landing Page</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Article Rewriter Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                        <b>More and more people are creating their own websites containing videos or other forms of active content, such as games. </b> The problem with this sort of website is that there is nothing on the page to attract search engines. Search engines like text that gives them an indication of the content - preferably unique.
                        </li>
                        <li>
                        Article Rewriter Pro software offers a quick and easy way to create suitable text, with minimal effort. The articles consist of full sentences, so can be analysed by search engines.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Two Step Opt-in Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                           <b>At Last! You Can Easily Increase Your Opt-In Conversions, In Just Minutes...Builds 2 Step Opt-In Pages With Lightbox!</b>
                        </li>
                        <li>
                           The money is the list. This is what successful internet marketers always shared to us if you want to become success in the internet business.
                        </li>
                        <li>
                           The fact is that, building email list is time consuming and sometimes takes time to gather a good amount of subscribers.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Master Shorty
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                           <b>A must have program for all online marketers!</b>
                        </li>
                        <li>
                           If you are a blogger or affiliate marketer, having an IM Tools that will make your business run at ease online is a huge help to lessen the effort and have more productive tasks that you can accomplish.
                        </li>
                        <li>
                           One of the tasks that is included to be time-consuming is the cloaking of affiliate links or URL specially if you are selling affiliate physical products from Amazon or even a digital products from Clickbank or the likes.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Video Marketing Unleashed
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                           <b>Effective video marketing is all about conversions. The video has to play a role in getting the viewer to whip out their credit card and buy something or click on an ad that pays you or enter their email address or zip code into a form.</b>
                        </li>
                        <li>
                           The challenge to video marketers nowadays is that video may have been the victim of its own success.
                        </li>
                        <li>The problem now is that there’s so much video out there that most of them simply don’t have an impact. They don’t get people to convert to buyers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Video Marketing Domination
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                           <b>By 2021 video will make upmore than 80% of internet traffic! After watching a video 64% of users are more likely to buy a product online. YouTube reports that mobile video consumptions rise 100% EVERY YEAR.</b>
                        </li>
                        <li>
                           All of this really proves the point that absolutely every business should leverage video in their marketing.
                        </li>
                        <li>
                           With this video course you will learn to create videos that generate hundreds and thousands of visitors to my websites, funnels, and offers… On a month to month basis.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Unique Exit Popup
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>
                           <b>Grab the Attention of Your Visitors When They Are About to Leave Your Website!</b>
                        </li>
                        <li>
                           Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?
                        </li>
                        <li>
                           Well, inside this product is a software that will change the game of this issue. This plugin will engage and get the attention of your readers that is about to leave your page and offer them something valuable from your website.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 Yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Covert Video Squeeze Page Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li><b>Introducing The Sneaky Video Squeeze Page Maker!</b></li>
                        <li>The money is the in the list. That's what many successful online entrepreneur's are saying.</li>
                        <li>And if you are not doing it also, you are leaving a lot of money in front of you.</li>
                        <li>The question is that, how are you going to build a list? Well, the most effective way to do it is by using video squeeze pages.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Featured Video Plus
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li><b>If a picture is worth a thousand words then how many words is a video worth?</b></li>
                        <li>Add Featured Videos to your posts and pages. Works like magic with most themes which use Featured Images!</li>
                        <li>Featured Video Plus enables you to define Featured Videos, which, if set, take the place of Featured Images.</li>
                        <li>This plugin adds customization options to your Media Settings.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Affiliate Marketing A-Z
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>The Affiliate Marketing A-Z report is a very enticing lead magnet, especially for newbies. It explains the process of affiliate marketing and how to get started in the best possible way. Readers will learn what they must do to get started and what they must avoid. There is an A-Z of the most commonly used affiliate marketing terms that are essential to know.</li>
                        <li>This powerful report is ideal for those new to affiliate marketing. The introduction explains the benefits of getting started with affiliate marketing and provides some different ways for affiliates to make money. Your readers will understand that they do not require any special skills or experience to be an affiliate marketer.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Project Genius
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>Do you have projects to plan? Do you want to take a professional approach? Do you want to increase your success rate? Then Project Genius is the software that will help you plan your projects!</li>
                        <li>From now on, you will be able to take a professional approach to your project planning and increase your success rate. You will no longer have surprises during the course of your project. You will better know what you want, your will organize your brain storming sessions better, and you will set your milestones and deadlines with ease.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        eProfit Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>Completely Automate The Entire Sales & Product Delivery Process For All Of Your PayPal Based Sites in 7 Minutes or Less - No Programming Skills Required! This Could Be The Easiest System Ever Put Together To Automate All Of Your sales site Processes From PayPal IPN, To Emailing Your Customers, Handling Downloads, And More!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"VOCALIC"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Phoenix Podcast Studio
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>EVERYTHING you need to create professional Podcast - with this incredible Podcast software from PodcastDirector.net, you can not only create podcasts of your choice but you can also convert any textual information to a sound file for your podcast.</li>
                        <li>A podcast is basically an audio file that is stored on the Internet (An rss file that contains audio content) that people can download to their computers , iPODS and MP3 players and listen to whenever they want.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Easy Poll Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>Find Out What Your Website Visitors Think, Like, Need & Want! Create Your Own Branded, Fully-Customized, Attractive (Text/Image) Polls, Easily - in Minutes!</li>
                        <li>Knowing all this about your site visitors is the guaranteed way to increased site stickiness, repeat visitors, growing traffic, happy and satisfied visitors/customers, a mega opt-in list, and a lot more, right? What is the easiest and fastest way for you to know what your website visitors think, like, need and want?</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        The Amazing Banner
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>Discover The Innovative Banner Software That Will Force You to Stop Making Excuses and Begin Creating Banners Better Than the Pros - Sales Are Far Better Than Excuses Anyway! Now Get Your Banner Done In Three Steps!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        3D Box Templates V1
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>3D Box Templates allow you to create great looking eBox covers in just a few simple steps; <b>Upload template, Enter text, Set color and you're done!</b> </li>
                        <li>This package comes with 5 eBox templates.</li>
                        <li>Each template comes in 3 versions; <b>with text, without text and with .psd source you you can make changes!</b> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh150 bonus-title-color">
                        Miscellaneous Stock Photos
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li><b>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</b></li>
                        <li>If you are an internet user, you may already noticed that images are everywhere. And if you an internet marketer, images is one of the best media to capture your audience's attention to click your ads or see what's your offer.</li>
                        <li>This is why successful online business owners invest money into this media to have a professionally-looking graphics that converts traffic into subscribers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-60 f-40 lh120 w800 yellow-clr">$2895!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh150 w700">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w800 blue-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w800 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Vocalic + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:60px;">
                  <defs>
                     <style>
                        .cls-1 {
                           fill: #fff;
                        }

                        .cls-1,
                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-2,
                        .cls-3 {
                           fill: #00aced;
                        }
                     </style>
                  </defs>
                  <g id="Layer_1-2">
                     <g>
                        <g>
                           <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z" />
                           <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z" />
                           <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                           <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z" />
                        </g>
                        <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z" />
                        <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z" />
                        <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z" />
                        <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z" />
                        <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z" />
                     </g>
                  </g>
               </svg>
               <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Vocalic</div>
               <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vocalic.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
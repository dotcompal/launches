<!Doctype html>
<html>

<head>
    <title>Academiyo Prelaunch</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	
	<meta name="title" content="Academiyo | Special">
	<meta name="description" content="Academiyo">
	<meta name="keywords" content="Academiyo">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta property="og:image" content="https://www.academiyo.com/special/assets/images/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Academiyo | Special">
	<meta property="og:description" content="Academiyo">
	<meta property="og:image" content="https://www.academiyo.com/special/assets/images/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="Academiyo | Special">
	<meta property="twitter:description" content="Academiyo">
	<meta property="twitter:image" content="https://www.academiyo.com/special/assets/images/thumbnail.png">
	
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
	<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
<script>
        function getUrlVars() {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var first = getUrlVars()["aid"];
        //alert(first);
        document.addEventListener('DOMContentLoaded', (event) => {
                document.getElementById('awf_field_aid').setAttribute('value', first);
            })
            //document.getElementById('myField').value = first;
    </script>
<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-902833396').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-902833396")) {
                document.getElementById("af-form-902833396").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-902833396")) {
                document.getElementById("af-body-902833396").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-902833396")) {
                document.getElementById("af-header-902833396").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-902833396")) {
                document.getElementById("af-footer-902833396").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->

</head>

<body>
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-md-start text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        </style>
                        <g>
                            <g>
                                <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                                 />
                                <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                                c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                                c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                                c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                                <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                                c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                                M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                                <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                                c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                                M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                                <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                                c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                                c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                                c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                                c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                                <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                                c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                                c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                                h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                                c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                                <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                                c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                                C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                                <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                                <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                                c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                                c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                                C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                                c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                                c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                            </g>
                            <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                            c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                            c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                            c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                            c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                            L1194.92,0z"/>
                        </g>
                    </svg>
                </div>
                <div class="col-md-9  mt-md0 mt15">
                    <ul class="leader-ul f-14 f-md-16 w400 white-clr text-md-end text-center ">
                        <li class="affiliate-link-btn">
                            <a href="#form">Subscribe To EarlyBird VIP List</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-16 f-md-18 w700 lh160 white-clr">
						This is Your Ultimate Chance To Tap Into The $315 Billion & Ever Growing <br class="d-none d-md-block"> E-Learning Industry & Build a Profitable Business Online!
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20 f-md-42 f-28 w600 text-center white-clr lh150">
                    Breakthrough 1-Click Software That
                    <span class="w900 yellow-clr under">Makes Us $525/Day by Creating Beautiful Udemy <sup class="f-24">TM</sup> Like Academy Site </span> Preloaded with 400+ HD, Done For You Video Trainings Ready to Sell in Just 60 Seconds

                    <!-- Breakthrough 1-Click Software That Makes Us $525/Day by Creating Beautiful Udemy TM Like Academy Site Preloaded with 400+ HD, Done For You Video Trainings Ready to Sell in Just 60 Seconds -->
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-18 f-md-20 w500 text-center lh150 white-clr">
				        No Course Creation | No Video Editing | No Tech Hassle | No Recurring Fee | <br class="d-none d-md-block"> Just Start Selling & Making Money Right Away
                    </div>
                </div>
                <div class="col-md-10 mx-auto col-12 mt-md50 mt20">
		            <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto">
                    <!--<div class="responsive-video">
                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->

    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 ">
                    <div class="f-16 f-md-16 lh160 w400">
                        <ul class="kaptick pl0">
                            <li>Create Beautiful Mobile Responsive Academy Sites<span class="w700"> With Marketplace, Blog & Members Area In a Few Clicks</span></li>
                            <li><span class="w700">List & Sell Your Own Courses</span> On Your Academy Marketplace.</li>
                            <li><span class="w700">Accept Payments</span> With PayPal, JVzoo, ClickBank, Warrior Plus Etc</li>
                            <li><span class="w700">No Traffic, Leads, or Profit Sharing</span> With Any 3rd Party Plateform - Keep 100% Profits & Control On Your Business</li>
                            <li><span class="w700">In-Built Ready To Use Affiliate System</span> With Swipes & Animated Banners to Boost your Sales & Profits</li>
                            <li><span class="w700">Branded Members Area</span> For Your Students to Make Authority</li>
                            <li><span class="w700">Track Student Progress–In-Progress</span> Or All Lessons Completed</li>
                            <li><span class="w700">Built-In Ticketing System</span> To Provide Support To Students</li>
                            <li>Create Free Courses With DFY Reports To <br class="d-none d-md-block"> <span class="w700">Build A Huge Email List</span></li>
                            <li>Manage Leads With an <span class="w700">Inbuilt Lead Management System</span></li>
                            <li class="w700">Create Sales Pages to Sell Your Courses <br class="d-none d-md-block">With A Live Page Editor</li>
                            <li><span class="w700">Choose From 400+ HD Video Courses</span> To Start Selling & Making Profit Instantly</li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="f-16 f-md-16 lh160 w400">
                        <ul class="kaptick pl0">
                            <li><span class="w700">Build Courses in Any Niche</span> Quick & Easy – Upload Unlimited Rich Media Files For Your Courses (Video, E-Book, Reports, etc.)</li>
                            <li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses</li>
                            <li><span class="w700">Multiple Themes</span> To Beautifully Design Academy Sites</li>
                            <li><span class="w700">100% SEO & Mobile Responsive </span>Academy Site & Marketplace</li>
                            <li><span class="w700">SMO Ready</span> -Enable Social Icons To Grow Your Social Media Following</li>
                            <li><span class="w700">Seamless Integrations</span> with Top Autoresponders</li>
                            <li>Newbie Friendly <span class="w700">One-Click Product Creation With DFY Courses</span></li>
                            <li class="w700">100% GDPR And CAN-Spam Compliant</li>
                            <li><span class="w700">Easy And Intuitive To Use Software</span> With Step By Step Video Training</li>
                            <li class="w700">100% Newbie Friendly with No Course Creation and No Tech Hassle</li>
                            <li class="w700">Top-Notch Customer Support</li>
                            <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!-- CTA Section Start -->                        
    <div class="cta-section-white">
        <div class="container">
            <div class="row">
                <!-- CTA Btn Section Start -->
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                            <a href="#form">Subscribe To EarlyBird VIP List
                            <img src="assets/images/btn-arrow.png" class="pl10 mt5 lr-move">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                        <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                        <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                    </div> -->
                    <div class="col-md-12 mx-auto mt20 mt-md40 f-16 f-md-18 w400 white-clr text-left lh160 text-md-start">
                    <ul class="ul-tick-icon1 p0 m0">
                        <li>Don't Pay $19/M. Get All Benefits For $19 One Time Only</li>
                        <li>Also Get A Chance To Win 10 FREE Copies</li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CTA Section End -->

    <!--aweber form Start-->
	<div class="space-section awform-section" id="form">
		<div class="container">
			<div class="row">					
				<div class="col-12">
					<div class="auto_responder_form inp-phold formbg" editabletype="shape" style="z-index:9">
						<div class="f-22 f-md-32 w700 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
							Awesome! You’re Almost Done.								
						</div>
						<div class="f-20 f-md-22 w400 text-center black-clr">							
							Fill up the details below
						</div>
						
						<div class="mt15 mt-md25" editabletype="form" style="z-index:10">
							<form method="post" class="af-form-wrapper" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
								<div style="display: none;">
									<input type="hidden" name="meta_web_form_id" value="902833396">
									<input type="hidden" name="meta_split_id" value="">
									<input type="hidden" name="listname" value="awlist6270234">
									<input type="hidden" name="redirect" value="https://academiyo.com/prelaunch-thankyou/" id="redirect_202dfc0c1790ba079c41c1700a8d52b8">

									<input type="hidden" name="meta_adtracking" value="My_Web_Form">
									<input type="hidden" name="meta_message" value="1">
									<input type="hidden" name="meta_required" value="name,email">

									<input type="hidden" name="meta_tooltip" value="">
								</div>
								<div id="af-form-902833396" class="af-form">
									<div id="af-body-902833396" class="af-body af-standards row">
										<div class="af-element width-form">
											<div class="af-textWrap">
												<input id="awf_field-114111014" class="text form-control custom-input input-field type=" text"="" name="name" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
											</div>
											<div class="af-clear"></div>
										</div>
										<div class="af-element width-form">
											<div class="af-textWrap"><input class="text" id="awf_field-114111015" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
											</div><div class="af-clear"></div>
										</div>
                                        <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
										<div class="af-element buttonContainer register-btn1 f-16 f-md-21 width-form width-form-button">
											<input name="submit" class="submit" style="max-width: 100%;" type="submit" value="Subscribe To EarlyBird VIP List" tabindex="502">
											<div class="af-clear"></div>
										</div>
									</div>
								</div>
								<div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jEwMzAwMzAzsnA==" alt="" /></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!--aweber form End-->
    

    <!-- footer-section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        </style>
                        <g>
                            <g>
                                <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                                 />
                                <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                                c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                                c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                                c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                                <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                                c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                                M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                                <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                                c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                                M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                                <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                                c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                                c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                                c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                                c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                                <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                                c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                                c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                                h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                                c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                                <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                                c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                                C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                                <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                                <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                                c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                                c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                                C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                                c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                                c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                            </g>
                            <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                            c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                            c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                            c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                            c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                            L1194.92,0z"/>
                        </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;">
                        <span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.
                        <br><br>
                        <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                        our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                        be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                        the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                        information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                    </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © Academiyo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-section End -->



</body>

</html>
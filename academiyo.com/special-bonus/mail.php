<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Academiyo Special Bonuses">
    <meta name="description" content="Academiyo Special Bonuses">
    <meta name="keywords" content="Academiyo Special Bonuses">
    <meta property="og:image" content="https://www.academiyo.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Ayush & Pranshu">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Academiyo Special Bonuses">
    <meta property="og:description" content="Academiyo Special Bonuses">
    <meta property="og:image" content="https://www.academiyo.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Academiyo Special Bonuses">
    <meta property="twitter:description" content="Academiyo Special Bonuses">
    <meta property="twitter:image" content="https://www.academiyo.com/special-bonus/thumbnail.png">


<title>Academiyo  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'may 17 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/cs71kk/0';
	 $_GET['name'] = 'Ayush & Pranshu';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row"> 
				<div class="col-12">
					 <img src="assets/images/potd new.png" class="img-fluid d-block mx-auto">
				</div>
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr mt-md50 mt 20">
						<span class="w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#FFFFFF;}
							.st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
						</style>
<g>
	<g>
		<path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
			/>
		<path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
			c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
			c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
			c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
			c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
		<path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
			c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
			c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
			 M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
			c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
		<path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
			c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
			c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
			 M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
			c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
		<path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
			c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
			c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
			c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
			c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
			c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
		<path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
			c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
			c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
			h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
			c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
		<path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
			c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
			C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
		<path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
		<path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
			c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
			c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
			C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
			c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
			c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
	</g>
	<path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
		c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
		c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
		c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
		c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
		L1194.92,0z"/>
</g>
</svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-16 f-md-18 w700 lh160 white-clr">
						Grab My 20 Exclusive Bonuses Before the Deal Ends…
					</div>
                </div>

				<div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
						<div class="f-md-40 f-28 w500 text-center white-clr lh160">					
							Revealing A BREAKTHROUGH, 1-Click Software That <span class="under yellow-clr w800">Makes $525/ Day Over & Over Again By Creating Beautiful Academy Sites</span> In Next 60 Seconds 
						</div>
				</div>
				<div class="col-md-12 col-12 text-center mt20 mt-md30">
					<div class="f-18 f-md-20 w500 text-center lh160 white-clr">Watch My Quick Review Video</div>
				</div>
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
                       <!-- <img src="assets/images/productbox.png" class="img-fluid"> -->
                       
						<div class="responsive-video">
                            <iframe src="https://academiyo.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                       
                    </div>

            </div>
        </div>
    </div>
	
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
                            <div class="f-16 f-md-16 lh160 w400">
                                <ul class="kaptick pl0">
								<li><span class="w700">Create Beautiful Mobile Responsive Academy Sites</span> With Marketplace, Blog & Members Area within Minutes </li>
						<li><span class="w700">Choose From 400+ HD Video Trainings in 30+ HOT Topics</span> to Start Selling & Making Profit Instantly</li>
						<li><span class="w700">Create Your Own Courses in ANY Niche –</span> Add Unlimited Lessons, Videos and E-books </li>
						<li>Made For Newbies & Experienced Marketers- <span class="w700">Even Camera-Shy People Can Start Selling Courses Right Away</span></li>
						<li><span class="w700">Accept Payments Directly in Your Account</span> With PayPal, JVzoo, ClickBank, Warrior Plus Etc </li>
						<li>No Traffic, Leads, or Profit Sharing With Any 3rd Party Platform<span class="w700"> - Keep 100% Profits & Control On Your Business </span></li>
						<li><span class="w700">Get Started Immediately -</span> Launch Your First Academy Site with Ready-To-Sell Courses In NO Time</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="f-16 f-md-16 lh160 w400">
                                <ul class="kaptick pl0">
								<li><span class="w700">Have Unlimited Earning Potential –</span>Teach Anything or Everything Quick & Easy </li>
                        <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online… </li>
                        <li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses </li>
                        <li><span class="w700">In-Built Ready To Use Affiliate System</span> to Boost your Sales & Profits</li>
                        <li><span class="w700">100% SEO & Mobile Responsive</span> Academy Site & Marketplace </li>
                        <li>Easy And Intuitive To Use Software With<span class="w700">Step By Step Video Training</span></li>
                        <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY </li>
                                </ul>
                            </div>
                        </div>
            </div>
            <div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 lite-black-clr">"acadearly "</span> with <span class="w800 lite-black-clr">$4 discount</span> </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
  <!-- Step Section End -->
  <!-- Step Section End -->
  <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 white-clr text-center ">					
               Start Your Own Profitable E-Learning Business in Just 3 Simple Steps… 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 1
                     </div>
                     <img src="assets/images/upload-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Create/Choose Course 
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 2
                     </div>
                     <img src="assets/images/customize-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Add Payment Options
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 3
                     </div>
                     <img src="assets/images/publish-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                         Publish & Profit 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  See The Sales We Got with A Simple<br class="d-none d-md-block"> Academy Site Created Recently
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                   And, We’re Making an Average $525 In Profits<br class="d-none d-md-block"> Each & Every Day
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
    
	<!---proof section end---->
	  <!-- Video Testimonial -->
	  <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 text-center black-clr"> 
                  Even Beta Users Are Getting EXCITING Results by Selling Our Done-For-You Courses
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-1.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-2.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-4.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
<!-- Video Testimonial -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"acadearly "</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	<div class="leraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <img src="assets/images/e-learning-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-30 f-24 lh140 w600 text-center black-clr mt20 mt-md50">And it is Absolutely Booming Post this Covid-19 Pandemic… 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w500 lh140 text-center">
                  The Global Pandemic has shut doors for many Schools, Universities, and Training Institutes. But in this Period, E-Learning has witnessed a positive trend and opened hundreds of Windows to many E-learning platforms… 
                  </div>
                  <div class="mt20 mt-md50">
                     <img src="assets/images/proof3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 text-center mt20 mt-md30">              
                  And this tremendous growth in student enrolments, shows a HUGE Opportunity! 
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-32 w600 lh140 text-center black-clr">Yes, It’s Very High In-Demand! </div>
               </div>
               <div class="col-12 mt20 f-18 f-md-20 w500 lh140 text-center">
                  And there is no limit in sight! – You can create &amp; sell courses on any topic and in any niche like thousands of others are selling on sites like Udemy<sup>TM</sup>, Skillshare<sup>TM</sup>, Udacity<sup>TM</sup>, Coursera<sup>TM</sup>, and More! 
               </div>
            </div>
         </div>
      </div>

   
	 <!-- Stil No Compettion Section Start -->
	 <div class="still-there-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-42 w700 lh140 text-center  black-clr">
                     Look At These People Generating Six and Seven Figures Just By Selling Simple Courses Online 
                  </div>
               </div>
               <div class="row mt-md70 mt20 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/smith.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Len Smith is making <span class="w700">$90K every month </span>- selling his Copywriting Secrets Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12 order-md-2">
                     <img src="assets/images/john-omar.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0 order-md-1">
                     <div class="col-md-6 col-8 ms-md-0 mx-auto line-5" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        John Omar and Eliot Arntz made <span class="w700">$700K in a month</span> by Selling their iOS App Development Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/rob-percival.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Rob Percival makes <span class="w700">$150K every month</span> - selling the same programming course to new students
                     </div>
                  </div>
               </div>
               <div class="row mt30 mt-md65">
                  <div class="f-md-24 f-20 w400 lh160 black-clr  text-center">
                     <span class="w700 f-md-36 f-22 w400 lh140"> But You’re Probably Wondering?</span><br> “Can I Make A Full-Time Income Selling My Knowledge?”
                  </div>
               </div>
            </div>
         </div>
      </div>
            <!-- Stil No Compettion Section End -->
      <!-- Suport Section Start -->
	  <div class="eleraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-20 f-md-28 w500 lh160 text-center">
                  <span class="w700 f-24 f-md-36"> Of Course, You Can!</span><br>  See How Newbies Made Millions from E-Learning Business During Lockdown 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
			    <div class="col-12 col-md-6 mt20 mt-md0 p-md-0">
                  <img src="assets/images/proofimg1.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                  <img src="assets/images/proofimg2.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg3.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg4.png" class="img-fluid d-block mx-auto">
               </div>
               </div>
               </div>
               <div class="col-12 w600 f-md-28 w500 lh140 text-center mt20 mt-md30">
               So, It is the perfect time to get in and profit from this exponential growth! 
               </div>
            </div>
            <div class="row mt30 mt-md70">
               <div class="col-12 text-center">
                  <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                     <div class="f-28 f-md-45 w800 lh140 text-center white-clr">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12  f-20 f-md-24 lh150 w600 text-center black-clr lh150">
                  It can be stated that…<br>
                  <span class="mt10 d-block">
                  E-Learning and Online Course Selling is the Biggest Income Opportunity Right Now! 
                  </span>
               </div>
               <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh150 mt20 mt-md30">
                  So, How would you like to Get Your Share?
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Suport Section End -->
  
	  <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
                </div>
                <div class="col-12 text-center mt70 mt-md70">
                    <div class="prdly-pres f-24 w700 text-center black-clr">
                        Proudly Introducing…
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:120px;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
	.st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
</style>
<g>
	<g>
		<path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"></path>
		<path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
			c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
			c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
			c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
			c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"></path>
		<path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
			c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
			c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
			 M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
			c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"></path>
		<path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
			c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
			c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
			 M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
			c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"></path>
		<path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
			c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
			c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
			c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
			c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
			c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"></path>
		<path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
			c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
			c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
			h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
			c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"></path>
		<path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
			c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
			C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"></path>
		<path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"></path>
		<path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
			c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
			c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
			C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
			c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
			c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"></path>
	</g>
	<path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
		c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
		c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
		c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
		c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
		L1194.92,0z"></path>
</g>
</svg>
                </div>
                <div class="f-md-30 f-22 w700 yellow-clr lh150 col-12 mt-md30 mt20 text-center">
					
					1-Click Academy Builder Software That Creates and <br class="d-none d-md-block">  Sell Courses Online with Zero Tech Hassles and  Makes Us<br class="d-none d-md-block">  $525/Day Over and Over Again 
                </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                <div class="col-12 col-md-10 mx-auto">
                    <img src="assets/images/product-image.png" class="img-fluid d-block mx-auto margin-bottom-15">
                </div>
                <!--<div class="col-md-5 col-12 mt20 mt-md0">
                    <div class="proudly-list-bg">
                        <ul class="proudly-tick pl0 m0 f-16 w500 black-clr lh150 text-capitalize">
                            <li><span class="w700">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                            <li><span class="w700">Create Beautiful and Proven Converting</span> E-Learning Sites 
                            </li>
                            <li><span class="w700">Preloaded with 700 HD Video Training and 40 DFY Sales Funnels </span>to Start Selling Instantly and Keep 100% of Profit</li>
                            <li><span class="w700">Don’t Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                            <li><span class="w700">Commercial License Included</span> </li>
                            <li><span class="w700">50+ More Features</span></li>
                        </ul>

                    </div>
                </div>-->
            </div>


        </div>
    </div>
	<!------------new bonus-------------->
	<div class="grand">
		<div class="container">
			<div class="col-12 text-center">
				<div class="f-30 f-md-40 lh130 w700 white-clr">Grand Bonus1 - My Recent Launch VidVee (FE+OTO3) with 100 Reseller License (worth $3299)</div>
			</div>
		</div>
	</div>
	<!----header section--->
		<div class="header-section-n">
			<div class="container">
				<div class="row">
<div class="col-12">
<img src="assets/images/logo-gr.png" class="img-fluid mx-auto d-block">
</div>
				<div class="col-12 text-center mt20 mt-md50 lh130">

                    <div class="pre-heading-n f-md-24 f-20 w600 lh130">
				 1 Click Exploits YouTube Loophole to Legally Use Their 800 Million+<br class="d-none d-md-block">
Awesome Videos for Making Us $528 Every Day Again &amp; Again...
                    </div>
                </div>
				<div class="col-12 mt-md25 mt20 text-center">
                    <div class="f-md-50 f-30 w500 white-clr lh130 line-center">
			   

Breakthrough Software <span class="w700 orange-clr1">Creates Self-Growing Beautiful Video Channels Packed With RED-HOT Videos And Drives FREE Autopilot Traffic On Any Topic </span>
In Just 7 Minutes Flat…
                    </div>
                    <div class="mt-md25 mt20 f-20 f-md-28 w500 text-center lh130 white-clr">
						No Content Creation. No Camera or Editing. No Tech Hassles Ever... 100% Beginner Friendly!					   
                    </div>
                    <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                            
							<div class="col-12 responsive-video">
                                <iframe src="https://vidvee.dotcompal.com/video/embed/xcz62wags4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                                 box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                            </div> 
							

                        </div>
						
						<div class="col-12 mt20 mt-md40 key-features-bg-v">
                    <div class="row flex-wrap">
                        <div class="col-12 col-md-6">
                            
                                <ul class="list-head pl0 m0 f-20 lh130 w400 white-clr text-start">
                                    <li>Complete Video &amp; YouTube Business Builder</li>
									<li>Ground-Breaking Method Brings Unlimited Traffic To Any Offer, Page Or Link</li>
									<li>Tap Into 800 Million YouTube Videos &amp; Make Any Video Yours Legally</li>
									<li>Build Beautiful &amp; Branded Video Channels on Any Topic in Any Niche Within Minutes</li>

                                </ul>
                        </div>
                        <div class="col-12 col-md-6">
                          
                                <ul class="list-head pl0 m0 f-20 lh130 w400 white-clr text-start">
                                   <li>Can Upload, Host &amp; Play Your Own Videos</li>
									<li>100% FREE Viral, Social &amp; SEO Traffic (Powerful)</li>
									<li>Make Tons of Sales, Amazon/Affiliate Commissions &amp; Leads.</li>
									<li><u>FREE Commercial License</u> - Build An Incredible Income Offering Services &amp; Keep 100%!</li>
									
                                </ul>
                        </div>
                    </div>
                </div>

                    </div>
                </div>
				</div>
			</div>
		</div>
	<!----header section--->
<div class="ph">
	<div class="container-fluid p0">
	<picture>
		<source media="(min-width:767px)" srcset="assets/images/no-ins-vid.png" class="img-fluid d-block">
		<source media="(min-width:320px)" srcset="assets/images/no-ins-m.png" class="img-fluid d-block">
		<img src="assets/images/no-ins-vid.png" alt="Flowers" class="img-fluid d-block" >
	</picture>
	<img src="assets/images/proof-vid.png" class="img-fluid mx-auto d-md-block d-none">
	<picture>
		<source media="(min-width:767px)" srcset="assets/images/no-dout-vid.png" class="img-fluid d-block">
		<source media="(min-width:320px)" srcset="assets/images/no-dout-m.png" class="img-fluid d-block">
		<img src="assets/images/no-dout-vid.png" alt="Flowers" class="img-fluid d-block" >
	</picture>
	<picture>
		<source media="(min-width:767px)" srcset="assets/images/proudly-vid.png" class="img-fluid d-block">
		<source media="(min-width:320px)" srcset="assets/images/proud-m.png" class="img-fluid d-block">
		<img src="assets/images/proudly-vid.png" alt="Flowers" class="img-fluid d-block" >
	</picture>
	</div>
</div>
	<!------------new bonus-------------->
	<div class="grand">
		<div class="container">
			<div class="col-12 text-center">
				<div class="f-30 f-md-40 lh130 w700 white-clr">Grand Bonus2 - My Recent Launch Tappit (FE+OTO3) with 100 Reseller License (worth $3299)</div>
			</div>
		</div>
	</div>
	<div class="adv-vid-section">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center ">
					<img src="assets/images/logo-gr1.png" class="img-fluid mx-auto d-block">
					<div class="f-20 f-md-22 lh140 w400 white-clr mt30 mt-md40 orangebg">Laser Targeted traffic for any niche in 3 simple steps </div>
					<div class="f-27 f-md-40 lh140 w400 white-clr mt30 mt-md40">REVEALED: <span class="w200">How Viral Content Sites Like</span> BuzzFeed &amp; ViralNova <span class="nutcolor">Use OTHER People's Content and Videos To Bank MILLIONS In Profits</span> Every Month... <span class="w200">And you can do the same</span>, Handsfree</div>
					<div class="f-20 f-md-20 lh130 w300 mt30 white-clr">Let this AI software automatically put most profitable<br> links for unlimited traffic, leads and commissions</div>
				</div>
			</div>


			<div class="row mt30 mt-md40">
				<div class="col-12 col-md-7">
					<div class="responsive-video" style="border-radius:5px;">
						<iframe title="vimeo-player" src="https://player.vimeo.com/video/296170117?autoplay=1&amp;muted=0" width="640" height="360" frameborder="0" allowfullscreen=""></iframe>
					</div>
				</div>
				<div class="col-md-5 col-12 mt-md0 mt20 white-clr">
					<div class="f-md-24 f-16 lh130 w200">
						<span class="w600 nutcolor">Step #1</span> Enter your keywords and automation frequency
					</div>
					<div style="background:#38304f; height:1px; width:100%; margin-top:15px;"></div>
					<div class="f-md-24 f-16 lh130 w200 mt20 mt-md25">
						<span class="w600 nutcolor">Step #2</span> AI software automatically fetch engaging content and best offer for your niche
					</div>
					<div style="background:#38304f; height:1px; width:100%; margin-top:15px;"></div>
					<div class="f-md-24 f-16 lh130 w200 mt20 mt-md25">
						<span class="w600 nutcolor">Step #3</span> Sit back and relax while Tappit gets laser targeted traffic that converts into buyers leads and commission
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="gr-1">
		<div class="container-fluid p0">
			<img src="assets/images/image-gr.png" class="img-fluid d-block mx-auto" style="width: 100%;">
		</div> 
	</div>
	<!------------new bonus-------------->
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Academiyo, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  	Product Launch Workshop LIVE
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Are you willing to make money from selling your own info product? If you want success in your info product, you must know a few things to get your product up and running.</li>
						<li>Don’t worry, with this 5-part video course, you’ll get live sessions taught by an experienced marketer getting your product up and running quickly. You’ll learn to set your funnels, sales pages, autoresponders, sales buttons, graphics, and much more.</li>
						<li>This bonus when combined with Academiyo proves to be a great resource for every success hungry marketer.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		
			<div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   	<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  	<img src="assets/images/bonus2.png" class="img-fluid">
				   	</div>
				   	<div class="col-12 col-md-7 mt-md0 mt20">
				   		<div class=" text-md-start text-center">
							<div class="bonus-title-bg mb-4">
				   				<div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
							</div>
						</div>
			
					  	<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  		How To Create And Launch New Products Every Month
					  	</div>
					  	<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							<li>Launching a product is exciting as well as stressful. The only way to ensure a successful product launch is to start early and get as much real-world feedback as possible before you launch.</li> 
							<li>If you are searching for some result grabbing tips, here comes an end to your searches. These video tutorials will teach you how to get ideas for a new product, how to create a new product, how to write a sales page, how to search a platform etc.</li>
							<li>You can easily use this bonus software with Academiyo and create a big business booster to secure best results with greater traffic and more profit.</li>
						</ul>
				   	</div>
				</div>
			</div>
		</div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row ">
		
			 	<div class="col-12 mt20 mt-md30">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus3.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-center text-md-start">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			Your First Sales Funnel
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>If you are an affiliate marketer or digital product owner who aims to have a hugely successful product launch, having an effective sales funnel will help you close more sales to your product. </li>
								<li>The good news though is that inside this product is a video tutorial that will guide you on how to make your sales funnel for the first time in your internet marketing career. </li>
								<li>Now, stop thinking and build your website with this software and use content curation with Academiyo to boost rankings like you always wanted.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row ">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus4.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 ">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			W+ For New Warriors
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Are you looking for information on WarriorPlus or confused whether WarriorPlus is a legitimate company or not?</li>
								<li>To solve your doubt, we have come up with a video series that will explain to you what Warrior Plus is and what are its advantages. You will also get to know the process of selling products on warrior plus.</li>
								<li>So, what are you waiting for? Combine this package with countless benefits that you get with Academiyo and take your profits to the next level.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row ">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  		<img src="assets/images/bonus5.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			JVZOO Conversion Secrets
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Whether you use JVZoo or another platform, you need to know how to boost your sales and how to implement realistic methods fast.</li>
								<li>To learn the methods, you can grab this 8-part video course and understand how to increase your digital product sales. You will find out what JVZoo features are helpful in boosting your sales. Also, you will learn the methods and reasoning behind it to understand why you need to use them.</li>
								<li>This bonus, when combined with Academiyo, becomes an ultimate growth booster for business owners.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"acadearly "</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			</div>
			<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   	<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  	<img src="assets/images/bonus6.png" class="img-fluid">
				   	</div>
				   	<div class="col-md-7 col-12 mt20 mt-md0">
				   		<div class="text-center text-md-start">
							<div class="bonus-title-bg mb-4">
				   				<div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
							</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			Joint Venture Marketing
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>You might be learning what to do while preparing for your first Joint Venture project. There are important details you have to learn, concerning the right professional steps to set up and take.</li>
								<li>With this bonus package, you will learn effective tips for a successful partnership that will cover the entire JV partnership spectrum.</li>
								<li>This bonus is a boon and when combined with Academiyo, this package becomes a top-notch business booster.</li>
					  		</ul>
				  	 	</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus7.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			JV Secrets 2.0
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Are you finding a way to generate a huge amount of traffic without depending on any kind of platform or websites that are only looking to profit themselves and fatten their bank account?</li>
								<li>Your wait is over! With this bonus, software discovers the secret of driving massive amounts of server crushing traffic without spending any time or money and learn how to build your business from scratch again if you were to lose your list, blog or sites tomorrow.</li>
								<li>So, what are you waiting for? Just combine this ultimate package with Academiyo and take your business to the next level.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus8.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-center text-md-start">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			Social Traffic Plan
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>Social Media has numerous advantages for your business. Learning about effectively utilize the most popular social media platforms is the first step to driving more traffic to your site and finding success.</li>
								<li>This simple guide will guide you how to boost your targeted website traffic using social media and how to utilize social media platforms to increase traffic to your business site properly.</li>
								<li>This package is a must have and when combined with Academiyo, it reaps great results for you in the long run.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus9.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">					 
					  			Backlink Bandit Software
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Backlinks help you grab lots of quality traffic that comes most of from search engines results. The higher the number of quality backlinks you have, the better you rank, and backlink bandit does it for you. So, this ultimate software creates over 5000 backlinks for your website.</li>
								<li>This package is a must have and when combined with Academiyo, it reaps great results for you in the long run.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus10.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			How to Convert
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Conversion is an industry term commonly centered around product launches, sales pages, and by this term, we measure the success of a product launch. If you want your visitor to become a buyer, you must master this term.</li>
								<li>This video course will teach you how to convert your visitors into customers. Also, you will get to know how to attract affiliates and raise a buzz and awareness to make more sales.</li>
								<li>This bonus is a boon and when combined with Academiyo, this package becomes a must have for every growth hungry marketer.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"acadearly "</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus11.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			Effective Paid Traffic Sources
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>If you are a blogger, affiliate marketer or a product launch owner, traffic is very important for your online business. But Traffic is not that easy to get a thing. What if you want quick traffic? Paid traffic is the solution there.</li>
								<li>Paid traffic can be paid advertisements like Google Adwords or Facebook Ads and other web property platforms. With this bonus guide, you will learn more about Paid traffic, its various platforms, and how to use it to boost your benefits.</li>
								<li>When combined with the ultimate software Academiyo, this package will reap great results in the near future.</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus12.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			How to monetize a website
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Monetizing a website looks easy from outside, but needs lots of effort and smart planning in order to achieve desired results. If you also faced this issue, then it’s time to take a breather.</li>
 								<li>With this package, you can follow desired &amp; battle tested steps &amp; make the most from your website in a hassle free manner.</li>
								<li>When combined with the ultimate software Academiyo, this package will reap great results in the near future.</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus13.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
					  			Traffic Beast
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>Advertising online results to be costlier as it involves many quality solo ads. And, sometimes these ads just don't perform as well as you might have hoped. But in order to get traffic for your business, you will need them. So, do you want to reduce the cost and risk of advertising online?</li>
								<li>If yes, then you need to find other webmasters selling something similar and then split the cost of the advertising with them. Traffic Beast is the one that will work for you, and take your efforts in the right direction.</li>
								<li>This bonus when combined with Academiyo proves to be a great resource for every success hungry marketer.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus14.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
				   				Membership Mogul
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
								<li>An online membership website is one of the most profitable types of membership programs of all because you have no inventory or shipping to deal with!</li>
								<li>In this guide, you will learn about how you can start your own membership site in just about any niche or genre you can think of, how to find members and successfully launch your membership program even if you haven’t done it before.</li>
								<li>This package is a must have and when combined with Academiyo, it reaps great results for you in the long run.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus15.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
				   				Membership Launch Formula V2
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  			<li>A membership site is simply an online location that allows people to browse through products, services, forums, and other online functions by signing up with the site.</li>
								<li>If you've been struggling to earn tons of income in your Internet marketing endeavors, inside is a great solution for you. Membership Launch Formula is the complete step-by-step system that walks you through the process of easily and effectively launching your own WordPress Based Membership Site.</li>
								<li>So, get in active mode and use this bonus with Academiyo to intensify your growth prospects like never before.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"acadearly "</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus16.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  	Internet Business Models Video Upgrade
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>There is a proven sequence of steps you can follow to guarantee your success when you're starting a small business online.</li>
								<li>With this step by step video course, you will learn about 4 proven internet business models in the least amount of the time. Models explained inside this course are easy to set up, low cost, evergreen, low risk and highly scalable.</li>
								<li>This bonus is a great add-on to the info selling powers that Academiyo possesses and will take your marketing benefits to the next level.</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->


	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus17.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  	The Sales Funnel Playbook Video Course
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>One of the core concepts in the digital marketing industry is the sales funnel that can take a business from virtually non-existent to a multi-million-dollar marketing machine. A sales funnel is divided into several steps, which differ depending on the particular sales model.</li>
								<li>This course will demonstrate the steps you'll need to take to make sure you have a Sales Funnel that is effective in maximizing your profit.</li>
								<li>When combined with Academiyo, this bonus becomes a lethal combination and boosts traffic and profits hands down.</li>
			   				</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus18.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							  	Easy Video Sales Pages
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Sales page plays a very important role in converting your website visitors into buyers. All you need is to create an engaging one.</li>
								<li>If you want some proven formulas to create an engaging sales page with which you can expect a huge sale in your offers, this bonus guide will help you learn them.</li>
								<li>This package is of multi-utility when combined with the immense powers of Academiyo.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus19.png" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   Webinar Fever
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							  	<li>Hosting a webinar allows your business to showcase its expertise, industry knowledge, abilities, skills, products, and services. Your webinar also benefits helping your audience to engage with and even become emotionally invested in your business.</li>
								<li>Webinar Fever gives a general overview of the Webinar, how to use them, and the general direction of this technology and ultimately boost production in your business for higher profits.</li>
								<li>This bonus is a great add-on to the info selling powers that Academiyo possesses and will take your marketing benefits to the next level.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->


	<!-- Bonus #20 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus20.png" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							   	Covert Video Squeeze Page Creator
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							 	<li>If you are not a web designer, you might want to hire someone to do it for you and the fact is that it cost a lot of money. The good news is that you can now build an amazing yet high-converting squeeze page that will generate a lot of subscribers using this amazing software.</li>
								<li>This bonus, when combined with Academiyo, becomes an ultimate growth booster for business owners.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-12 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Academiyo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#FFFFFF;}
						.st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
					</style>
					<g>
						<g>
							<path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
								/>
							<path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
							c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
							c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
							c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
							c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
							<path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
							c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
							c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
							M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
							c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
							<path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
							c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
							c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
							M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
							c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
							<path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
							c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
							c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
							c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
							c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
							c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
							<path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
							c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
							c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
							h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
							c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
							<path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
							c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
							C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
							<path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
							<path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
							c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
							c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
							C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
							c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
							c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
						</g>
						<path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
						c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
						c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
						c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
						c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
						L1194.92,0z"/>
					</g>
				</svg>
                <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">
					Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
				</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © Academiyo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://academiyo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop ">	
					<div class="col-12 ">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 ">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

<!Doctype html>
<html>
   <head>
      <title>Academiyo Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Academiyo | Special">
      <meta name="description" content="Academiyo">
      <meta name="keywords" content="Academiyo">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.academiyo.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Academiyo | Special">
      <meta property="og:description" content="Academiyo">
      <meta property="og:image" content="https://www.academiyo.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Academiyo | Special">
      <meta property="twitter:description" content="Academiyo">
      <meta property="twitter:image" content="https://www.academiyo.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'academiyo';
allowedDomain = 'academiyo.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
   </head>
   <body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                     </style>
                     <g>
                        <g>
                           <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                              />
                           <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                              c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                              c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                              c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                           <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                              M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                           <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                              M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                           <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                              c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                              c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                              c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                              c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                           <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                              c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                              c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                              h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                              c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                           <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                              c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                              C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                           <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                           <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                              c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                              c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                              C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                              c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                              c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                        </g>
                        <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                           c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                           c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                           c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                           c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                           L1194.92,0z"/>
                     </g>
                  </svg>
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-14 f-md-16 w400 white-clr text-md-end text-center ">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Buy Now</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading1 f-16 f-md-18 w700 lh160 black-clr">
                  The Simple 60 Seconds Academy Gets Automated Sales Daily (Anyone Can Do This)
                  </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-36 f-28 w600 text-center white-clr lh150">
                  <u>EXPOSED</u>–A Breakthrough 1-Click Software That <br><span class="w900 f-md-45 yellow-clr">Makes Us $525/Day Over & Over Again</span><br> <span class="lh130">
                  By Creating Beautiful Udemy<sup class="f-20">TM</sup> Like Sites Pre-Loaded with 400+ HOT Video Courses in JUST 60 Seconds…
                  </span> 
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 text-center lh150 white-clr2">
                     No Course Creation. No Camera or Editing. No Tech Hassles Ever... Even A 100%<br class="d-none d-md-block"> <span class="w700">Beginner or Camera SHY Can Start Profiting</span> Right Away 
                  </div>
               </div>
               <div class="col-md-10 mx-auto col-12 mt-md50 mt20">
                  <!-- <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video">
                     <iframe src="https://academiyo.dotcompal.com/video/embed/4inqz8dc2v" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
               </div>
               <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED 
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 ">
                  <div class="f-16 f-md-16 lh160 w400">
                     <ul class="kaptick pl0">
                        <li><span class="w700">Create Beautiful Mobile Responsive Academy Sites</span> With Marketplace, Blog & Members Area within Minutes </li>
                        <li><span class="w700">Choose From 400+ HD Video Trainings in 30+ HOT Topics</span> to Start Selling & Making Profit Instantly</li>
                        <li><span class="w700">Create Your Own Courses in ANY Niche –</span> Add Unlimited Lessons, Videos and E-books </li>
                        <li>Made For Newbies & Experienced Marketers- <span class="w700">Even Camera-Shy People Can Start Selling Courses Right Away</span></li>
                        <li><span class="w700">Accept Payments Directly in Your Account</span> With PayPal, JVzoo, ClickBank, Warrior Plus Etc </li>
                        <li>No Traffic, Leads, or Profit Sharing With Any 3rd Party Platform<span class="w700"> - Keep 100% Profits & Control On Your Business </span></li>
                        <li><span class="w700">Get Started Immediately -</span> Launch Your First Academy Site with Ready-To-Sell Courses In NO Time</li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-16 f-md-16 lh160 w400">
                     <ul class="kaptick pl0">
                        <li><span class="w700">Have Unlimited Earning Potential –</span>Teach Anything or Everything Quick & Easy </li>
                        <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online… </li>
                        <li><span class="w700">Free Hosting</span> For Images, PDF, Reports & Video Courses </li>
                        <li><span class="w700">In-Built Ready To Use Affiliate System</span> to Boost your Sales & Profits</li>
                        <li><span class="w700">100% SEO & Mobile Responsive</span> Academy Site & Marketplace </li>
                        <li>Easy And Intuitive To Use Software With<span class="w700">Step By Step Video Training</span></li>
                        <li class="w700 blue-clr">PLUS, FREE COMMERCIAL LICENCE IF YOU ACT TODAY </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     Use Discount Coupon <span class="blue-clr w700">"acadearly "</span> for Instant <span class="blue-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- host section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 white-clr text-center ">					
               Start Your Own Profitable E-Learning Business in Just 3 Simple Steps… 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 1
                     </div>
                     <img src="assets/images/upload-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Create/Choose Course 
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 2
                     </div>
                     <img src="assets/images/customize-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                        Add Payment Options
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg1">
                     <div class="f-22 f-md-28 w900 blue-clr lh140 text-center" >
                        STEP 3
                     </div>
                     <img src="assets/images/publish-icon.png" class="img-fluid d-block mx-auto mt-md20 mt20">
                     <div class="f-22 f-md-26 w900 mt-md25 mt20 text-center">
                         Publish & Profit 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- host section end -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  See The Sales We Got with A Simple<br class="d-none d-md-block"> Academy Site Created Recently
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                   And, We’re Making an Average $525 In Profits<br class="d-none d-md-block"> Each & Every Day
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.png" class="img-fluid d-block mx-auto">
               </div>
               <!-- <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr mt20 mt-md70">
                  Even Our Beta Testers Are Getting EXCITING<br class="d-none d-md-block"> Results Like This by Selling Our Done-For-You, Ready to Sell Courses
               </div>
               <div class="col-11 col-md-7 mt20 mt-md50 mx-auto">
                  <video loop autoplay muted id="vid" class="w-100">
                     <source type="video/mp4" src="assets/images/traffic.mp4">
                     </source>
                     <source type="video/ogg" src="assets/images/traffic.ogg">
                     </source>
                  </video>
               </div> -->
            </div>
         </div>
      </div>
    
      <!-- Video Testimonial -->
      <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh140 w700 text-center black-clr"> 
                  Even Beta Users Are Getting EXCITING Results by Selling Our Done-For-You Courses
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-1.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-2.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-4.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     Use Discount Coupon <span class="yellow-clr w700">"acadearly "</span> for Instant <span class="yellow-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <!-- Video Testimonial end -->
      <div class="leraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <img src="assets/images/e-learning-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-30 f-24 lh140 w600 text-center black-clr mt20 mt-md50">And it is Absolutely Booming Post this Covid-19 Pandemic… 
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-20 w500 lh140 text-center">
                  The Global Pandemic has shut doors for many Schools, Universities, and Training Institutes. But in this Period, E-Learning has witnessed a positive trend and opened hundreds of Windows to many E-learning platforms… 
                  </div>
                  <div class="mt20 mt-md50">
                     <img src="assets/images/proof3.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-20 w500 lh140 text-center mt20 mt-md30">              
                  And this tremendous growth in student enrolments, shows a HUGE Opportunity! 
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-32 w600 lh140 text-center black-clr">Yes, It’s Very High In-Demand! </div>
               </div>
               <div class="col-12 mt20 f-18 f-md-20 w500 lh140 text-center">
                  And there is no limit in sight! – You can create &amp; sell courses on any topic and in any niche like thousands of others are selling on sites like Udemy<sup>TM</sup>, Skillshare<sup>TM</sup>, Udacity<sup>TM</sup>, Coursera<sup>TM</sup>, and More! 
               </div>
            </div>
         </div>
      </div>

      <!-- Stil No Compettion Section Start -->
      <div class="still-there-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-42 w700 lh140 text-center  black-clr">
                     Look At These People Generating Six and Seven Figures Just By Selling Simple Courses Online 
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt20 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/smith.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Len Smith is making <span class="w700">$90K every month </span>- selling his Copywriting Secrets Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12 order-md-2">
                     <img src="assets/images/john-omar.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0 order-md-1">
                     <div class="col-md-6 col-8 ms-md-0 mx-auto line-5" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        John Omar and Eliot Arntz made <span class="w700">$700K in a month</span> by Selling their iOS App Development Course
                     </div>
                  </div>
               </div>
               <div class="row mt-md10 mt30 align-items-center">
                  <div class="col-md-4 col-12">
                     <img src="assets/images/rob-percival.png" class="img-fluid d-block mx-auto max-heigh340">
                  </div>
                  <div class="col-md-8 col-12 mt20 mt-md0">
                     <div class="col-md-6 col-8 line-5 ms-md-0 mx-auto" ></div>
                     <div class=" col-12 f-md-30 f-22 lh140 mt15 mt-md25 w400 text-md-start text-center black-clr">
                        Rob Percival makes <span class="w700">$150K every month</span> - selling the same programming course to new students
                     </div>
                  </div>
               </div>
               <div class="row mt30 mt-md65">
                  <div class="f-md-24 f-20 w400 lh160 black-clr  text-center">
                     <span class="w700 f-md-36 f-22 w400 lh140"> But You’re Probably Wondering?</span><br> “Can I Make A Full-Time Income Selling My Knowledge?”
                  </div>
               </div>
         </div>
      </div>
      <!-- Stil No Compettion Section End -->
      <div class="eleraning-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-20 f-md-28 w500 lh160 text-center">
                  <span class="w700 f-24 f-md-36"> Of Course, You Can!</span><br>  See How Newbies Made Millions from E-Learning Business During Lockdown 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
			    <div class="col-12 col-md-6 mt20 mt-md0 p-md-0">
                  <img src="assets/images/proofimg1.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                  <img src="assets/images/proofimg2.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg3.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-6 p-md-0">
                  <img src="assets/images/proofimg4.png" class="img-fluid d-block mx-auto">
               </div>
               </div>
               </div>
               <div class="col-12 w600 f-md-28 w500 lh140 text-center mt20 mt-md30">
               So, It is the perfect time to get in and profit from this exponential growth! 
               </div>
            </div>
            <div class="row mt30 mt-md70">
               <div class="col-12 text-center">
                  <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                     <div class="f-28 f-md-45 w800 lh140 text-center white-clr">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12  f-20 f-md-24 lh150 w600 text-center black-clr lh150">
                  It can be stated that…<br>
                  <span class="mt10 d-block">
                  E-Learning and Online Course Selling is the Biggest Income Opportunity Right Now! 
                  </span>
               </div>
               <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh150 mt20 mt-md30">
                  So, How would you like to Get Your Share?
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="new-dcp-team-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-24 f-20 w600 lh150 black-clr">
                  Dear Struggling Marketer, 
                  </div>
                  <div class="f-md-32 f-24 w700 lh150 blue-clr mt10">
                  From the desk of Ayush Jain and Pranshu Gupta 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-md-20 f-18 w400 lh150 black-clr ">
                        I’m Ayush Jain along with my friend Pranshu Gupta. 
                           <br><br>
                           We are happily living the internet lifestyle since last 10 years with full flexibility of working from anywhere at any time. <br><br>
                        </div>
                        <div class="mt20">
                           <div class="f-md-20 f-18 w400 lh150 black-clr ">
                           And while we don’t say this to brag, <br>
                              <span class="w700">we do want to make one thing clear: </span><br><br>
                              <span class="w700 f-20 f-md-22">E learning is a multi-billion-dollar industry and… </span><br><br>
                              Yes,<br>
                               <span class="w700">It’s rapidly growing by leaps and bounds every year</span> and even during the post pandemic era. 
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/ayush-pranshu.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
    <!-- potential Section Start -->
    <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-42 f-28 w600 lh140 text-center white-clr">
                     No Matter What Niche, You can Teach Anything and Everything Online Including...
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt30">
               <div class="col-md-3 col-6">
                  <div class="">
                     <img src="assets/images/pn1.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Programming
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt0">
                  <div class="">
                     <img src="assets/images/pn2.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     School/College<br> Courses
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn3.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Photography
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn4.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Dining Etiquette
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn5.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Yoga
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn6.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Gardening
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn7.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Makeup Hacks
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn8.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Graphic Designing
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn9.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Weight Loss
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn10.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Pet Care
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn11.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Body Building
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn12.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Cooking
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn13.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Personality Development
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn14.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Video Editing
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn15.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Home Decor
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn16.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Baking
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn17.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Wine Making
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn18.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Car Repairing
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn19.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Foreign Languages
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn20.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Digital Marketing
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn21.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Accounting
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn22.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-20 f-16 lh140 w600 white-clr text-center mt20">
                     Dancing
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 f-md-20 f-18 w500 mt20 mt-md50 lh140 text-center white-clr">
               To encash in multiple niches, we recently started 3 more academy sites using <br class="d-none d-md-block"> Academiyo & amazed with the results… 
               </div>
            </div>
         </div>
      </div>
      <!-- potential Section End -->
      
      <div class="earned-money-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-42 w500 lh160 text-center">
                  Wala! <span class="w700">We Made $3,660 In Sales in Last 15 Days…</span> Following This Proven System
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/proof6.png" class="img-fluid d-block mx-auto">
                  <img src="assets/images/proof5.png" class="img-fluid d-block mx-auto mt20">
               </div>
            </div>
         </div>
      </div>
<!---------f99----------->
<div class="eleraning-section">
   <div class="container">
      <div class="row">
      <div class="col-12 f-md-32 f-24 w500 lh150 text-center black-clr">
               <span class="w700">Every Random Joe & Jane Can Get Results Like These</span><br class="d-none d-md-block"> WITHOUT Quitting Their Current Job, WITHOUT Any Prior Skills Or WITHOUT Risking Their Hard Earned Money. 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md30">
                  <img src="assets/images/earned-money.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-20 f-18 w400 lh150 text-center black-clr mt20 mt-md30">
                  <span class="w600">Yep, you read that right. </span><br><br>
                  Now, even you can start getting REAL profits like this easily <span class="w600">IF YOU DO IT RIGHT!  It’s a REAL DEAL but if go in wrong direction, it can waste your months of time & 100s of dollars.  </span>
               </div>
      </div>
   </div>
</div>
<!---------f99----------->
      <!-- Problem Section -->
      <div class="probelm-bg-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 col-md-10 text-center mx-auto">
                  <div class="row align-items-center">
                     <div class="col-md-2">
                        <div class="d-none d-lg-block">
                           <img src="assets/images/sad-emoji1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-8 f-md-45 f-28 lh140 w700 black-clr  text-center">
                     Here’s The Problem!
                     </div>
                     <div class="col-md-2 ">
                        <div class="d-none d-lg-block">
                           <img src="assets/images/sad-emoji1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 problme-shape">
                  <div class="f-24 f-md-36 w700 lh150 yellow-clr text-center">
                     Creating Engaging Video Trainings <br class="d-none d-md-block"> Is PAINFUL &amp; Time-Consuming
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 white-clr mt20 mt-md30">
                     <ul class="noneed-listing5 pl0">
                        <li><span class="w600">You need to research & plan daily.</span> And staying up-to-date with latest topics needs lots of effort </li>
                        <li><span class="w600">You Need To Be On Camera.</span> This can be a major blocker in case you are shy or introvert and have a fear that people would judge and laugh at your videos. </li>
                        <li><span class="w600">You need to learn COMPLEX video &amp; audio editing skills.</span> Most software are complex and difficult to learn, especially if you are a non-technical guy like us. </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-7 mt-md50">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Buying Expensive Equipment Can Leave Your Back Accounts Dry
                  </div>
                  <div class="f-20 f-md-21 w400 lh150 black-clr mt10">
                     To even get started with first video training, you need expensive equipment, like a
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing6 pl0">
                        <li class="w600">Nice camera, </li>
                        <li class="w600">Microphone, and </li>
                        <li><span class="w600">Video-audio editing software</span> That would cost you THOUSANDS of dollars. </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5">
                  <img src="assets/images/need-img.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 f-24 f-md-36 w700 lh150 black-clr">Not All E-learning Platforms Are Created Equal...</div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-20 f-md-22 w400 lh150 black-clr">
                     <ul class="noneed-listing6 pl0">
                        <li><span class="w600">Pay Profit Share or Fixed Monthly Fee.</span>
                           You must pay a huge share of your profits as fixed or percent fees
                        </li>
                        <li><span class="w600">Lose Traffic.</span>
                           You have no control over your hard-earned traffic, and your visitors gets distracted with your competitors offers
                        </li>
                        <li><span class="w600">A Lot of Competition.</span>
                           It is hard to engage people who checks for the reviews or star ratings and prefer to take courses of established competitors
                        </li>
                        <li><span class="w600">Lose Leads.</span>
                           You get no leads which means you lose any future opportunities to connect with them and offer more services
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-12 col-md-5 order-md-2">
                  <img src="assets/images/problem1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 corder-md-1 mt20 mt-md0">
                  <div class="f-24 f-md-36 w700 lh150 black-clr text-md-start text-center black-clr text-capitalize">
                     Now question comes in mind, why not to setup your own online platform?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="f-20 f-md-22 w400 lh150 black-clr">
                     <ul class="noneed-listing6 pl0">
                        <li><span class="w600">Big Learning Curve.</span>
                           To learn marketing, designing, coding & testing to build a robust platform
                        </li>
                        <li><span class="w600">Buy Multiple Apps & Hire Freelancers.</span>
                           For website, membership site & cart & also waste money & time with inefficient developers
                        </li>
                        <li><span class="w600">Buy Domain & Hosting.</span>
                           Pay Recurring fees to domain, Hosting & video streaming companies
                        </li>
                        <li><span class="w600">Continuous Monitoring.</span>
                           To ensure that payments & training are up & running properly
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 f-md-32 f-22 w600 black-clr lh140 mt20 mt-md30 text-center">And that’s the REASON, 85% of the entrepreneurs leave their business in dreams or within just the first year of starting. 
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Problem Section End-->
      <!-- Well, not anymore Section Start-->
      <div class="anymore-section">
         <div class="container  ">
            <div class="row">
               <div class="col-12 f-md-45 f-28 lh140 w700 text-center black-clr">But not anymore!</div>
               <div class="col-12 f-md-20 f-18 w500 mt20 mt-md30 lh140 text-center ">After years of learning, planning, designing, coding, debugging,<br class="d-none d-md-block"> as well as months of real user testing…
                  <br><br> We are proud to release our solution that will make creating and selling <br class="d-none d-md-block">courses easier and faster than ever…
               </div>
            </div>
         </div>
      </div>
      <!-- Well, not anymore Section End-->
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
               <div class="col-12 text-center mt70 mt-md70">
                  <div class="prdly-pres f-24 w700 text-center black-clr">
                     Proudly Introducing…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:120px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                     </style>
                     <g>
                        <g>
                           <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"></path>
                           <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                              c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                              c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                              c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"></path>
                           <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                              M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"></path>
                           <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                              M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"></path>
                           <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                              c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                              c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                              c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                              c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"></path>
                           <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                              c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                              c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                              h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                              c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"></path>
                           <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                              c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                              C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"></path>
                           <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"></path>
                           <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                              c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                              c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                              C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                              c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                              c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"></path>
                        </g>
                        <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                           c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                           c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                           c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                           c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                           L1194.92,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="f-md-30 f-22 w700 yellow-clr lh150 col-12 mt-md30 mt20 text-center">
               1-Click Academy Builder That Creates and Sells Courses Online with Zero Tech Hassles. It Also Comes Preloaded with 400+ HD Video Trainings Ready to Sell in Next 60 Seconds 
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-image.png" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
               <!--<div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="proudly-list-bg">
                      <ul class="proudly-tick pl0 m0 f-16 w500 black-clr lh150 text-capitalize">
                          <li><span class="w700">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                          <li><span class="w700">Create Beautiful and Proven Converting</span> E-Learning Sites 
                          </li>
                          <li><span class="w700">Preloaded with 700 HD Video Training and 40 DFY Sales Funnels </span>to Start Selling Instantly and Keep 100% of Profit</li>
                          <li><span class="w700">Don’t Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                          <li><span class="w700">Commercial License Included</span> </li>
                          <li><span class="w700">50+ More Features</span></li>
                      </ul>
                  
                  </div>
                  </div>-->
            </div>
         </div>
      </div>
      <div class="steps-section2">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-md-42 f-28 lh140 w700 black-clr  text-center" >
                     <span class="blue-clr">Start Your Own Profitable E-Learning Business</span><br class="d-none d-md-block"> In Just 3 Simple Steps… 
                  </div>
               </div>
               <div class="col-12">
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP1:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                              Create or Choose a Course 
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                              Add your own video lessons, e-books, or choose from 400+ HD Video Trainings in 30+ done-for-you video courses. 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6   mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP2:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                                 Add Payment Options
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                                 Select your payment gateways to accept payments directly in your accounts. (All major options included)
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1  mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
                  <div class="row mt30 mt-md60 align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="d-flex gap-3 flex-column align-items-start left-line">
                           <div class="f-md-24 f-20 white-clr w700 text-nowrap step-bg">
                              STEP3:
                           </div>
                           <div class="ml30 mt20 mb20">
                              <div class="f-20 f-md-32 w700 black-clr lh100 text-nowrap" >
                                 Publish & Profits
                              </div>
                              <div class="f-18 f-md-20 w500 black-clr lh140 mt15">
                                 Publish your courses on your own branded e-learning site and keep 100% of the leads and profits in your pocket.
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <div>
                           <img src="assets/images/step-3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Client Video -->
      <div class="client-video-section" id="productdemo">
         <div class="container  ">
            <div class="row">
               <div class="col-12 f-md-45 f-28 lh140 w700 text-center black-clr">
                  Watch Academiyo In Action:
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md60">
                  <!-- <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video video-shadow">
                     <iframe src="https://academiyo.dotcompal.com/video/embed/khrvzvtdk1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                     <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     Use Discount Coupon <span class="blue-clr w700">"acadearly "</span> for Instant <span class="blue-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cilent Video end -->
      <!-- HUGE Opportunity Section -->
      <!--<div class="hugeopportunity-section">
         <div class="container  ">
             <div class="row">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh140 w700 text-center  white-clr">
                         Academiyo Brings You <br class="d-none d-md-block"> A HUGE Opportunity On A Silver Platter
         
                     </div>
                 </div>
                     <div class="row align-items-center flex-wrap mt20 mt-md60">
                         <div class="col-12 col-md-6  ">
                             <div>
                                 <img src="assets/images/hugeopportunity-img.png" class="img-fluid d-block mx-auto">
                             </div>
                         </div>
                         <div class="col-12 col-md-6 mt20 mt-md0">
                             <div class="f-24 f-md-36 w400 white-clr lh160 text-center text-md-start">You Just Need To Capitalize On It – The Easier Way. No Competition With Others.
                             </div>
                         </div>
                 </div>
             </div>
         </div>
         </div>-->
      <!-- HUGE Opportunity Section end -->
      <!-- Video Testimonial -->
      <div class="testimonial-section1">
         <div class="container  ">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w500 text-center black-clr">And Here's What Some More <br class="d-none d-md-block">
                     <span class="w700">Happy Users Say About Academiyo</span>
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-5.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-6.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-7.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col p-md-0 mt20">
                  <img src="assets/images/test-8.png" class="img-fluid d-block mx-auto">
                  </div>
            </div>
         </div>
      </div>
      <!-- Video Testimonial end -->
      <!-- Feature 1 Section Start -->
      <div class="feature-section" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140  yellow-clr text-center">
                     Here Are The GROUND - BREAKING Features That Makes Academiyo A CUT ABOVE The Rest
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-24 f-md-36 w600 white-clr lh140">
                     Build A Beautiful Academy Site In A Few Minutes
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt30 mt-md30 white-clr">
                  Building An Online Academy just got faster and easier. Simply use our state-of-the-art technology to get started today.
               </div>
               <div class="col-12 mt-md50 mt20">
                  <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 white-clr mt-md40 mt30">
                  Everything you need to run a profitable six or seven figure pro level academy site is already included. Just tap in and profit.
               </div>
            </div>
         </div>
      </div>
      <!-- Feature 1 Section End -->
      <div class="ftr2-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn1.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Elegant Marketplace
                        </div>
                        <!--<div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Sell your courses on your own branded marketplace while building authority at the same time. Plus, there’s no need to share any of your profits with 3rd party marketplaces.
                           </div>-->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn2.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Engaging Blog
                        </div>
                        <!--<div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           A blog is important for getting visitors updated and engaged with your brand and Academiyo creates it for YOU.
                           </div>-->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn3.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Membership Site For All Your Students
                        </div>
                        <!--<div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           A personalized members area in your brand color theme. Students can learn with courses, check their support tickets, purchases & also can buy more courses with 1 click.
                           </div>-->
                     </div>
                  </div>
               </div>
               <!-- Line 2 -->
               <div class="col-12 mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn4.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt-md15 mt20 text-center black-clr  lh140">
                           Academy Home
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           A branded home page with a beautiful slider & separate sections for top courses & top articles of the month to convert visitors into buyers.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn5.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Beautiful Slider
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Academiyo comes with a beautiful slider that includes an attractive full width image, headline, description & CTA buttons to impress visitors and promote your best courses or offers.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn6.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Multiple Color Themes
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Give your academy site a professional look according to your niche & brand with multiple color themes. (No designing necessary)
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- Line 3 -->
               <div class="col-12  mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn7.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Attractive Header & Menu
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Comes with your LOGO, links to your marketplace, blog, help desk, as well as with login & signup buttons.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn8.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt-md50 mt20 text-center black-clr  lh140">
                           Login with Social Media
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Make it easy by allowing students to sign up and sign in using their existing Facebooktm & Googletm accounts.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn9.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Static Pages
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           All necessary page templates are included such as about us, terms & conditions, and privacy-policy pages
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- Line 4 -->
               <div class="col-12 mt-md80">
                  <div class="row">
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn10.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Google Map
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Add Google Maps easily on your contact us page to be found on maps for instant credibility and for faster google business indexing.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn11.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           SEO & Mobile Ready
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Academy site is fully SEO and mobile optimized to cater to search engine and mobile traffic.
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt-md0 mt30">
                        <img src="assets/images/fnn12.png" class="img-fluid d-block mx-auto">
                        <div class="f-20 f-md-22  w700 mt20 text-center black-clr  lh140">
                           Social Media Ready
                        </div>
                        <!-- <div class="f-18 f-md-18 w400 lh140 mt15 text-center">
                           Add your academy’s social details and enable users to reach you on social media to build even more trust.
                           </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- section 3 -->
      <div class="secc-10">
         <div class="container">
            <div class="row">
               <div class="col-12 f-24 f-md-36  text-center w600 black-clr lh140">
                  Quickly Sell Courses That Attract Tons of Buyers.<br class="d-none d-md-block">Academiyo Makes This Child's Play.
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 black-clr lh140 text-xs-center">
                           Create Your Courses– Add Unlimited Lessons (Video, E-Book & Reports)
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140 text-xs-center black-clr mt20 ">
                           Add unlimited videos, PDFs, e-books & reports as Lessons to <span class="w600">create an engaging course so students can progress systematically.</span>
                           <br><br> With the lessons option, we’ve made it super easy for you to create courses that wow your students and help them to understand your concepts better.
                           </div> -->
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/fnn13.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-28 w600 black-clr lh140 text-xs-center">
                           List & Sell Courses On Your Own Marketplace.
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140 text-xs-center black-clr mt20">
                           Academiyo enables you to <span class="w600">list all your courses  & sell them on your own, branded marketplace.</span> <br><br> You can even divide your courses into different levels such as eginner, intermediate & advanced levels, This way you’ll increase customer lifetime value & ROI.
                           </div> -->
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/fnn14.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Create Courses On Any Topic While Becoming An Authority In Any Niche
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140 text-xs-center black-clr mt20">
                           There’s no limit to how many markets you can dominate with Academiyo.<br><br>
                           By using the technology included, you’ll be able to go into tons of niches, create courses, and dominate before the competition even knows what hit them!
                           </div> -->
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/f10.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12  mt30 mt-md100">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-28 w600 lh140 text-xs-center black-clr">
                           Flexible Pricing and Payment Options Are Included Too!
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140 text-xs-center black-clr mt20">
                           Academiyo works seamlessly with the most popular payment options.<br><br>
                           You can accept payments in 100+ currencies & countries easily with PayPal or if you are a fan of platforms like JVzoo, ClickBank or WarriorPlus...you can also sell on those platforms with Academiyo’s easy marketplace integration. <br><br>
                           <span class="w600">This way you can set your own price and start taking orders without any worries. </span> <br><br> Simply focus on delivering best value to your students and Academiyo will take care of the rest.
                           </div> -->
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/set-courses.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ftr8-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36  w600 lh140 text-center black-clr">
                     You’ll Have 100% Control Of Your Business. 
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 text-center black-clr mt20 mt-md30">
                     Academiyo Lets You Keep All Your Leads, Traffic and Profits <br class="d-none d-md-block">Without Sharing A Dime With 3rd Party Marketplaces..
                  </div>
                  <div class="mt-md40 mt20 f-18 f-md-20 w400 lh140 black-clr">
                     Whenever you sell courses through 3rd party platforms you pay fixed fees while also fighting against other competitors on the same marketplace.<br><br>
                     What’s worse is that some of these same marketplaces keeps the leads and buyers for themselves while profiting off your hard work.<br><br>
                     And you don’t get any payback for future products they promote with the leads you supplied!
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                  <div class="mt-md60 mt20 f-18 f-md-20 w400 lh140 black-clr">
                     But with Academiyo you can bypass all of that. <span class="w600"> You won’t have to pay an additional fee and every single lead you get will be yours to keep forever.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="secc-11">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-24 w500 lh140 text-center yellow-clr">
                  No Course Of Your Own To Sell? No Problem! 
                  </div>
                  <div class="mt-md30 mt20 f-24 f-md-36  w600 lh140 text-center white-clr">
                  You Can Get Started Quickly With 400+ HD Video Trainings Packed in 30+ Done-For-You Courses on 30+ RED Hot Topics That Come Included With Academiyo. 
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 white-clr">
                     We want to make sure you have the best possible chance to succeed with Academiyo.  That’s why we’re including these hot and in demand 400+ video trainings. You do not need to write a single word or be on camera to start selling online. <br><br>
                     All the sales materials are included.  Just add your logo, pricing, and buy buttons and start selling in an instant! Take a look at what’s included!
                  </div>
               </div>
               <div class="col-12 col-md-6 mt80">
                  <div class="t-block1">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t1.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                           400+ HD Video Trainings on 30+ Hot Topics 
                           </div>
                        </div>
                        <!-- <div class="col-12 ">
                           <div class="mt-md30 mt10 xs-text-center f-18 f-md-18 w400 lh140">
                              We’ve done the research and all the work necessary with these 10 high quality video courses in Academiyo.  Just plug in your info and you can start selling them right away.
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt80">
                  <div class="t-block2">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t2.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              Training Guides & PDF Reports
                           </div>
                        </div>
                        <!-- <div class="col-12 ">
                           <div class="mt-md30 mt10 xs-text-center f-18 f-md-18 w400 lh140">
                              These are already included in your members area. Simply add your brand name, your pricing, your buy buttons, and you are good to sell online.
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt-md100 mt80 clear">
                  <div class="t-block3">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t3.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              High Converting Sales Material
                           </div>
                        </div>
                        <!-- <div class="col-12 ">
                           <div class="mt-md30 mt10 xs-text-center f-18 f-md-18 w400 lh140">
                              We went even further and added high converting sales pages, upsell pages, thank you pages and doodle style sales videos for all 10 courses in your account.
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt-md100 mt80">
                  <div class="t-block4">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="timg">
                              <img src="assets/images/t4.png" class="img-fluid d-block mx-auto">
                           </div>
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-20 f-md-22 text-xs-center mt-md0 mt20  w700 black-clr lh140">
                              Affiliate System
                           </div>
                        </div>
                        <!-- <div class="col-12 ">
                           <div class="mt-md30 mt10 xs-text-center f-18 f-md-18 w400 lh140">
                              All 10 courses also come with a ready-to-use affiliate page pre-loaded with email swipes, and an animated banner to help your affiliates crack more sales which means more revenue for you.
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="personalised">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36  w600 lh140 text-center">
                     Separate Personalized Membership Sites For<br class="d-none d-md-block">All Your Students 
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 text-center">
                     Academiyo also includes an intuitive members area to suit your brand. Your students will be able to easily access their courses, <span class="w600">check their support tickets, previous purchases as well as purchase any additional course materials with 1 click.</span>						  
                  </div>
                  <div class="mt-md30 mt20 f-20 f-md-24 w600 lh140 black-clr">                              
                     Here’s a look at what’s all included:
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm1.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Clean Members Dashboard
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400  lh140">
                                 Your students will be able to see and manage everything from the dynamic  dashboard.  This includes their name, courses they have access to, & their latest 5 support tickets status.<br><br>
                                 This dashboard is also fully branded according to your brand name and color theme.
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm2.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Check Courses Progress
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                                 The more your students go through your courses, the more chances they have of getting better results. Students can check their courses progress in an instant.  <br><br>
                                 They can see which course they have completed, which course they are currently enrolled in to help save time. 
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm3.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Learn on Any Device, Anywhere and Anytime
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                                 Students can access all content on any device anywhere they have an internet connection.<br><br>
                                 Academiyo automatically makes your content mobile responsive. This way your courses will show great on desktop as well as on mobile devices. 
                              </div>
                              </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row 1 -->
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block ">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm4.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Support System Included
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                                 You’ll be able to manage any support requests your students have with the inbuilt ticketing system. <br><br> 
                                 This way you can manage your total course marketplace business all from one easy to use application.
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <!-- row 2 -->
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block ">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm5.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Upsell To More Courses Easily.
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                                 You’ll also have the option to display other courses in the member’s area with Academiyo.<br><br>
                                 This way you can increase your revenue from the same traffic without investing more out of pocket.
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <!-- row 3 -->
                     <div class="col-12 col-md-4 mt-md100 mt70">
                        <div class="pm-block">
                           <div class="col-md-12 col-12 ">
                              <div class="pm-img">
                                 <img src="assets/images/pm6.png" class="img-fluid d-block mx-auto">
                              </div>
                           </div>
                           <div class="col-md-12 col-12 ">
                              <div class="f-20 f-md-22 text-center mt-md20 mt20  w700 black-clr lh140">
                                 Automatic Payment History & Email Notifications
                              </div>
                           </div>
                           <!-- <div class="col-12 ">
                              <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                                 Besides being able to see previous purchase history in their account, Academiyo automatically sends them a purchase email from your academy name with their purchase and login details.<br><br>
                                 All this happens on while you build your business into a six or seven figure powerhouse!
                              </div>
                              </div> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ar-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-6 col-12">
                        <div class="f-24 f-md-36  w600 lh140 black-clr">
                           Your Academy Will Load Correctly On Every Device And Is 100% Mobile Responsive.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0">
                        <img src="assets/images/mobile-responsive.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="more-seo">
         <div class="container">
            <div class="row">
               <!-- <div class="col-12">
                  <div class="f-24 f-md-36 black-clr w600 lh140 text-center">
                     Engage & Convert  More of Your SEO & Social Traffic, into Paying Customers
                  </div>
                  </div> -->
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-6 col-12 order-md-2">
                        <div class="f-24 f-md-36 black-clr w600 lh140">
                           Engage & Convert  More of Your SEO & Social Traffic, into Paying Customers
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140">
                           When you combine traffic with engagement, the result can be massive  sales for your business.<br>
                           <span class="w600">That’s why you’ll love Academiyo. </span><br><br>
                           These sites are designed to help you convert every single visitor from social and search engines into
                           paying customers.<br><br>
                           With an Inbuilt SEO module – you can set pages meta tiles, 
                           make any page dofollow or nofollow, create XML sitemaps and much more.
                           Plus, you can create a professional blog and <span class="w600">generate fresh content which will help boost search rankings even further. </span>
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                        <img src="assets/images/more-seo-img.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="last-app">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-36 black-clr  w600 lh140 text-center">
                     Academiyo Is The LAST App You Will Ever Need To Start Dominating The HUGE E-Learning Market.
                  </div>
                  <div class="mt-md30 mt20 f-18 f-md-20 w400 lh140 text-center">
                     Academiyo gives you a complete system to create courses, sell them online as well as <span class="w600">manage entire business through 1 central dashboard. </span><br><br>
                     <span class="w600">All the heavy lifting is done for you. You only need to integrate your knowledge into Academiyo and you can create amazing courses easily that attract sales like a magnet on steroids.</span>
                  </div>
               </div>
               <!-- Col 1 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block1 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c1.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           All-In-One Platform
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           AcdemyPro is an All-in-One Knowledge Ecommerce Platform that helps position you as an authority in any niche with your Pro Academy.<br><br>
                           Create beautiful courses, sell them on your own marketplace and manage your all students from 1 central dashboard.
                        </div>
                        </div> -->
                  </div>
                  <!-- block2 -->
                  <div class="c-block2 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Reports & Analytics
                        </div>
                     </div>
                     <!-- <div class="col-12 "> 
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           You’ll be able to see at a moment’s notice how your or your client’s academy or any course is doing online and build better strategies for future growth.<br><br>
                           Get an insight on total visitors, sales, members, and more for any time period you specify. 
                        </div>
                        </div> -->
                  </div>
                  <!-- block3 -->
                  <div class="c-block3 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           SSL Secured Subdomains
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           All your Academy sites get their own personal subdomain for FREE. (i.e.  YogaByJhon.Academiyo.biz) <br><br>
                           Plus, every single site is 100% secured with 56 Bit SSL so you can be rest assured and focus on growth.
                        </div>
                        </div> -->
                  </div>
                  <!-- block4 -->
                  <div class="c-block4 mt-md100 mt70  c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c4.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Members & Lead Management
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           Data is considered the biggest asset of any business these days. And if you manage your leads properly then you can increase your returns multi-fold.<br><br>
                           So, to help you manage your leads without leaving your Academiyo dashboard, we have added an in-built lead management system.
                        </div>
                        </div> -->
                  </div>
               </div>
               <!-- Col 2 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block5 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c5.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           1 Click Product Creation
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           Newbie Friendly One-Click Product Creation with DFY Courses.<br><br> 
                           Also create your own custom courses quickly & easy. Just add your lessons one by one, add pricing & payment details and you’re all set and ready to sell on your marketplace. 
                        </div>
                        </div> -->
                  </div>
                  <!-- block2 -->
                  <div class="c-block6 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c6.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Custom Domain
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           Make your academy even more professional by running everything on your own domain like YogaByJonn.com and intensify your brand.
                           <br><br>
                           Just add your domain details in Academiyo and everything (Your marketplace, blog & members area) will run on your own domain.
                        </div>
                        </div> -->
                  </div>
                  <!-- block3 -->
                  <div class="c-block7 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c7.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Ticket Management System
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           Selling online requires you to have a support system in place for all your students' needs.
                           <br><br>
                           And with Academiyo’s built-in ticket system your customers can submit tickets and you can manage them with priority, courses & status of ticket right inside dashboard without leaving the for a third-party customer support tool.
                        </div>
                        </div> -->
                  </div>
                  <!-- block4 -->
                  <div class="c-block8 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c8.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           100% GDPR And CAN-SPAM Compliant
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           We all are required to adhere to GDPR & CAN-SPAM Compliance.  Especially when we are talking about the data.<br><br> 
                           The good thing is, Academiyo is 100% GDPR & CAN-SPAM compliant. So, just turn on the consent message & sell courses across the world without any worries.
                        </div>
                        </div> -->
                  </div>
               </div>
               <!-- Col 3 -->
               <div class="col-12 col-md-4">
                  <!-- block1 -->
                  <div class="c-block9 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c9.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Sales Page Builder with Editor
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           So, to help you close more sales we are also providing a Sales Page builder to create high-converting sales pages for your courses.<br><br>
                           It also comes with Done-for-you sales page templates & inline editor that allows you to add, delete and edit anything to build your sales pages easily.
                        </div>
                        </div> -->
                  </div>
                  <!-- block2 -->
                  <div class="c-block10 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c10.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           FREE Hosting & Video Player
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           To make your academy and courses shine, you will need to host a lot of images, reports & video lessons.<br><br>
                           So, we are giving free hosting for all your images, reports along with our DFY video courses with HD video player
                        </div>
                        </div> -->
                  </div>
                  <!-- block3 -->
                  <div class="c-block11 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c11.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           Automated Email Notifications
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           Automated emails will be sent from your brand when someone signs up or purchases from your academy.  <br><br>
                           Their user details will automatically be generated.   You’ll also get an email notification when someone raises a ticket for better user experience.
                        </div>
                        </div> -->
                  </div>
                  <!-- block4 -->
                  <div class="c-block12 mt-md100 mt70 c-hei">
                     <div class="col-md-12 col-12 ">
                        <div class="c-img">
                           <img src="assets/images/c12.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                     <div class="col-md-12 col-12 ">
                        <div class="f-20 f-md-22 text-center mt-md30 mt20  w700 black-clr lh140">
                           FREE Courses to Build List
                        </div>
                     </div>
                     <!-- <div class="col-12 ">
                        <div class="mt-md20 mt10 text-center f-18 f-md-18 w400 lh140">
                           One of the best ways to build a huge list is by offering freebies in exchange for leads. <br><br>
                           So, to help you generate a lot of leads and turn them into paying members, we are giving DFY Reports that you can compile as Free Courses & give them to your all subscribers.
                        </div>
                        </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="more-seo">
         <div class="container">
            <div class="row">
               <!-- <div class="col-12">
                  <div class="f-24 f-md-36 black-clr w600 lh140 text-center">
                     Seamless Integrations With TOP Autoresponders
                  </div>
                  </div> -->
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12 order-md-2">
                        <div class="f-24 f-md-36 black-clr w600 lh140">
                           Seamless Integrations With TOP Autoresponders
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140">
                           Did you know that every $1 spent on Email Marketing Returns $38 on average? That’s a huge 3800% ROI.<br><br>
                           So, to help you get the best out of your leads we have integrated Academiyo with some of the major Autoresponder service providers.  <span class="w600">You won’t need to worry about exporting & importing your leads from one platform to another </span>as this integration will directly send your leads to your Autoresponder automatically.  
                           <br><br>
                           This way you can instantly start nurturing them through email sequences to build trust and purchase more of your courses and affiliate recommendations.
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                        <img src="assets/images/seamless.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="ar-sec">
         <div class="container">
            <div class="row">
               <!-- <div class="col-12">
                  <div class="f-24 f-md-36  black-clr w600 lh140 text-center">
                     Easy and Intuitive To Use Software with Step by Step<br class="d-none d-md-block">Video Training
                  </div>
                  </div> -->
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12">
                        <div class="f-24 f-md-36  black-clr w600 lh140">
                           Easy and Intuitive To Use Software with Step by Step<br class="d-none d-md-block">Video Training
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 lh140">
                           We built Academiyo to be easy to use, but powerful to handle an e-learning business’ needs. But to make sure you have the best possible of chance of success with Academiyo, we’ve <span class="w600">created step-by-step video training that covers any questions you might have.</span>
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0">
                        <img src="assets/images/f23.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features End-->
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                      <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     Use Discount Coupon <span class="yellow-clr w700">"acadearly "</span> for Instant <span class="yellow-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="table-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 black-clr">
                     We Literally Have no Competition
                  </div>
               </div>
            </div>
            <div class="row g-0 mt70 mt-md100">
               <div class="col-md-4 col-4">
                  <div class="fist-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-32 f-16 w700 justify-content-start justify-content-md-center">Features</li>
                        <li class="f-md-18 w600">Starting Pricing </li>
                        <li>Transaction Fees</li>
                        <li class="text-start">400+ HD Video Trainings Packed in 30+ Done-For-You Courses </li>
                        <li>Free Hosting for Courses</li>
                        <li> No. of Courses</li>
                        <li>Add Your Own Courses</li>
                        <li>Instant payouts</li>
                        <li>Active Members </li>
                        <li>UNLIMITED Sub-Domains</li>
                        <li>No Hosting & Domain Hassles</li>
                        <li>Video Based Tutorials </li>
                        <li>PDF Training Guide</li>
                        <li>Hot and Evergreen Products</li>
                        <li>DFY 400+ Blogs </li>
                        <li>Add Your Own Blogs </li>
                        <li>100% Mobile Responsive</li>
                        <li> Pre-Installed Lead Generation Page</li>
                        <li>Inbuilt Ticket System</li>
                        <li>Dedicated Members Area  </li>
                        <li>No Profit Sharing</li>
                        <li>No monthly fees or additional charges</li>
                        <li>Pre-Installed & Self-Hosted FE Pages</li>
                        <li>Newbie Friendly Video Training</li>
                        <li>Exclusive Bonuses</li>
                        <li class="text-start">Media Library to Manage Your Doc, PDF and videos</li>
                        <li class="text-start">1 Million+ Stock Images to Boost your Sales and Profits</li>
                        <li>FAST servers</li>
                        <li class="text-start">100% Unique & Latest Content on The Topic</li>
                        <li>In-Built SEO Settings</li>
                        <li>Elegant and eye-pleasing website</li>
                        <li class="text-start">Autoresponder, Webinar and CRM Integration</li>
                        <li>One time fees</li>
                        <li class="text-start">Complete Step-By-Step Video Training & Tutorials Included</li>
                        <li> Premium Support</li>
                        <li>Live Chat - Customer Support</li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="second-row">
                     <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                        <li class="f-md-20 f-16 white-clr">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                              </style>
                              <g>
                                 <g>
                                    <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                                       />
                                    <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                                       c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                                       c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                                       c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                       c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                                    <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                                       c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                       c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                                       M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                       c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                                    <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                                       c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                       c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                                       M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                       c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                                    <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                                       c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                                       c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                       c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                                       c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                                       c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                                    <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                                       c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                                       c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                                       h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                                       c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                                    <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                                       c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                                       C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                                    <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                                    <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                                       c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                                       c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                                       C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                                       c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                                       c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                                 </g>
                                 <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                                    c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                                    c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                                    c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                                    c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                                    L1194.92,0z"/>
                              </g>
                           </svg>
                        </li>
                        <li class="f-md-18 w600"> $19 <br>One Time</li>
                        <li>0% </li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="third-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-20 f-16 w700"><span>Udemy</span></li>
                        <li class="f-md-18 w600">Free Premium <br> Account</li>
                        <li> 30-40% Each Transaction</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="forth-row">
                     <ul class="f-md-16 f-14 w500 lh120">
                        <li class="f-md-20 f-16 w700"><span>Teachable</span></li>
                        <li class="f-md-18 w600">$29/Month</li>
                        <li>5% Each transaction</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-2">
                  <div class="fifth-row">
                     <ul class="f-md-16 f-14">
                        <li class="f-md-20 f-16 w700"><span>Kajabi</span></li>
                        <li class="w600"> $119/Month </li>
                        <li>0%</li>
                        <li>Unlimited</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li>5</li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li>1000</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/tick-icon.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="imagine-sec pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-12">
                  <img src="assets/images/imagine-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md80 mb-md0 mb30">
                  <div class="f-18 f-md-20 w400 lh150 black-clr">
                     ... ! A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br> <b>That’s EXACTLY what we’ve designed Academiyo to do for you.</b><br><br>                    So, if you want to build super engaging academy websites packed with HD Video Trainings on Hot Topics at the push of a button, then get viral social traffic automatically and convert it into SALES, all from start to finish, then Academiyo is made for you! 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="d-flex">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Create instant 1 click video channels packed with trending videos
                                 </div>
                             </div>
                             <div class="d-flex mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Drive tons of laser targeted traffic from RED-HOT social media giants
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Get best results without getting into video creation & editing hassles
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Get more exposure for your offers in a hands down manner
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Boost sales & profits without spending much
                                 </div>
                             </div>
                             <div class="d-flex mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Have complete control on your video marketing campaigns
                                 </div>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/target-arrow.png" class="img-fluid d-block mx-auto">
         
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh150 black-clr">
                         <span class="w600">Seriously, having to do all that manually is expensive,</span> time consuming and put frankly downright irritating, especially once you want to build more than 1 channel that’s packed with gold content.<br><br> 
         That's exactly the reason so many people are afraid to even get started in video marketing and the same <span class="w600"> reason why out of all that DO get started, very few truly "make it".</span> <br><br> We have done all the grunt work for you. So, you don’t need to worry at
                         all
         
                     </div>
                 </div>
             </div>
         </div>
         </div> -->
      <!--
         <div class="potential-sec">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-10 mx-auto text-center">
                     <div class="f-28 f-md-45 w700 shape-testi1 blue-clr">Academiyo Is Perfect For</div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md50 ">
                           <div class="never-ending" >
                              <div class="mb23" >
                                 <img src="assets/images/n2.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Affiliate Marketers
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md50 ">
                           <div class="never-ending" >
                              <div class="mt5" >
                                 <img src="assets/images/video-marketers.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black mt14  text-center" >
                                 Video Marketers
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md50 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n3.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 E-Com Sellers
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="mt5" >
                                 <img src="assets/images/n1.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black mt14  text-center" >
                                 Business Coaches
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="mb23" >
                                 <img src="assets/images/n23.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Digital Product Sellers
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/youtubers.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 YouTubers
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n4.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 All Info Sellers
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n22.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Lead Generation Companies
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="" >
                                 <img src="assets/images/n6.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Music Classes
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n7.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Sports Clubs
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n8.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Bars
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="" >
                                 <img src="assets/images/n9.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Restaurants
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n10.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Hotels
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n11.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Schools
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="" >
                                 <img src="assets/images/n12.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Churches
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n13.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Taxi Services
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div >
                                 <img src="assets/images/n14.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Garage Owners
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt25 mt-md30 ">
                           <div class="never-ending" >
                              <div class="" >
                                 <img src="assets/images/n15.png" class="img-fluid d-block mx-auto">
                              </div>
                              <div class="hr-shape" ></div>
                              <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                                 Dentists
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         -->
      <div class="need-marketing">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-32 w700 lh150 black-clr">
                     So, With Academiyo you can create and sell as many courses online as you want and start making money instantly <span class="red-clr">without…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 f-md-20 w500 lh150 black-clr">
                           <ul class="noneed-listing pl0">
                              <li>Sharing your hard-earned profits</li>
                              <li>Paying monthly fees to 3rd party platforms</li>
                              <li>Spending money repetitively on hosting and domains</li>
                              <li>Hiring expensive developers</li>
                              <li>Any coding or technical skills</li>
                              <li>The hassle of getting courses approved on 3rd party platforms</li>
                              <li>Writing long sales letters or making sales videos</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="assets/images/platfrom.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="look-shape">
                     <div class="f-22 f-md-30 w600 lh150 white-clr">
                        Look, it doesn't matter who you are or what you're doing.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 white-clr mt10">
                     If you want to finally start a profitable online business that’s proven to work, make the<br class="d-none d-md-block"> money that you want and live in a way you dream of, Academiyo is for you. 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="f-20 f-md-26 w700 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     Use Discount Coupon <span class="yellow-clr w700">"acadearly "</span> for Instant <span class="yellow-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1 ">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center px30 px-md0">
                  <div class="but-design">
                     <div class="f-28 f-md-45 w700 lh140 text-center white-clr skew-normal">
                        We're not done yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md20">
                  <div class="f-22 f-md-24 w500 lh150 text-center">
                     When You Grab Your Academiyo Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">
                              DotcomPal- Most Complete Sales Solution Builder-
                           </div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              All-in-one growth platform for Entrepreneurs & SMB’s to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touch point... without any designer, tech team or the usual hassles.
                              <br><br>
                              With this in your marketing arsenal, you can instantly build any type landing pages and even host, manage and share fast loading & high quality videos to from a single dashboard with no technical hassles ever.
                              <br><br>
                              This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Academiyo today.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/bonus1.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Live Training - 0-10K a Month With Academiyo (Limited To First 1000 buyers only – Worth $1000)-</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">This awesome LIVE training will help you to build a SIX FIGURE Business. These are proven techniques on how to get started building a successful business quickly and easily. PLUS THREE lucky attendees will be selected randomly to win $100 cash each. And there will be a Live Q & A Session at the end of the training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Private Access to Online Business VIP Club – It’s Priceless</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Learning from top minds is the best way to scale your business. With this exclusive group, you can connect with a community of like-minded individuals and take your business to the next level. This is a major way to fast track your business and see incredible benefits at the same time.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Secret Traffic Sources Video Training (Useful To Drive Traffic That Converts Into Buyers) (Worth $297)
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              We’ll show you how to drive laser targeted-buyer traffic to your pro academies built with Academiyo using this premium video course. This will help you get tons of leads & sales without any money wasted on paid ads or any of the normal trial and error methods you’re probably used to.
                              <br><br>
                              This limited time bonus and could be taken down at any moment, so don’t delay and act now.
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/bonus4.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #5 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">6 To 7 Figure Video Training (Boost Your Business & Scale It To A New Level) (Worth $297)
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Get premium tips and tricks with this 5-part video course that highlights how you can build a 6 to 7 business of your own. Apply these methods to grow your E-Learning business you create with Academiyo.
                              <br><br>
                              This is high value for your business, so you’ll want to make sure you don’t miss out on this one.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/bonus5.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #6
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-28 w700 lh150 text-start black-clr">Video Training on How To Become A Highly Profitable Speaker (Share Your Passion & Get Paid For Your Expertise) (Worth $197)
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Learn how you can become a professional mentor or coach & create rabid followers in the process. Spread the power of your knowledge and boost engagement for your pro academies.
                              <br><br>
                              Everything you need to use your expertise and convert it into a super profitable business is included in this training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/bonus6.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--Bonus value Section start -->
      <div class="bonus-section-value">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 mx-auto">
                  <img src="assets/images/thats-a-total.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt-md100 mt20">
                  <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
            </div>
         </div>
      </div>
      <!--Bonus value Section ends -->
      <div class="license-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 w500 black-clr  text-center lh140">

                  Also Get Our LIMITED Commercial License* That Will Allow You To <span class="w700">Tap Into A UNIQUE Opportunity </span>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5">
                  <img src="assets/images/commercial-image.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt20 mt-md0">
                  <div class="f-md-20 f-18 w400 lh140">
                     <span class="w600">As we have shown you, there are tons of businesses – yoga gurus, fitness centers, anybody who want to start an online business businesses and much more…</span> that need your services &amp; eager to pay you
                     monthly for your services.<br><br>
                     Build their branded academy to boost their authority and profits by letting them sell their video courses online. They will pay top dollar to you when you deliver top notch services to them. We’ve taken care of everything so that you can deliver those
                     services simply and easily.
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-md-20 f-18 w400 lh140">
                     <span class="w600">Note: </span>This special commercial license is available ONLY during this launch. Take advantage of it now because it will never be offered again.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section -->
      <div class="noneed-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-20 f-md-24 w400 lh150 text-center">
                  Only 3 easy steps are all it takes to start selling courses online... 
                  </div>
                  <div class="f-md-38 f-28 w700 black-clr lh150 text-center mt20 mt-md40">
                  With Academiyo, Say Goodbye to All Your Worries 
                  </div>
                  <div class="noneed-shape mt20 mt-md40">
                     <div class="f-20 f-md-20 w400 lh150">
                        <ul class="noneed-list2 pl0">
                           <li><span class="w700">No need to pay monthly</span> for expensive course creation services</li>
                           <li><span class="w700">No need to worry</span> about sharing profits or transaction fee.</li>
                           <li><span class="w700">No need to lose</span> your traffic & leads with 3rd party marketplaces– EVERYTHING is in your control!</li>
                           <li><span class="w700">No need to spare a thought</span> for a domain or building e-learning sites!</li>
                           <li><span class="w700">No need to worry </span>about lack of technical or design skills</li>
                           <li><span class="w700">No need to worry about</span> complicated & time- consuming processes. It is an easy 3 steps & your courses are ready to get published within minutes.</li>
                           <li class="happy"><span class="w700">And…say YES Now to living a LAPTOP lifestyle</span> that gives you an ability to spend more time with your family and have complete financial FREEDOM.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section End-->
      <!-- Table Section Start -->
      <div class="table-section" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-24 w600 text-center lh150 black-clr">
                     Take Advantage Of Our Limited Time Deal 
                  </div>
                  <div class="f-md-45 f-28 w700 lh150 text-center mt10">
                     Get Academiyo For A Low One-Time- 
                     <br class="d-none d-md-block"> Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <!-- <div class="col-12 col-md-6">
                        <div class="table-wrap">
                            <div class="table-head text-center">
                                <div>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 977.7 276.9" style="enable-background:new 0 0 977.7 276.9; max-height:45px;" xml:space="preserve">
                        <style type="text/css">
                        .stblue{fill:#0041B5;}
                        .storg{fill:#FF7E1D;}
                        </style>
                        <g>
                        <path class="stblue" d="M0,213.5V12.2h99.1c19.4,0,34.5,4.3,45.5,12.7c11,8.5,16.5,21.1,16.5,37.9c0,20.9-8.6,35.7-25.9,44.3
                        c14.1,2.2,24.9,7.6,32.4,16.2c7.5,8.6,11.3,19.8,11.3,33.7c0,11.8-2.6,21.9-7.9,30.3c-5.3,8.4-12.8,14.9-22.6,19.4
                        c-9.8,4.5-21.3,6.8-34.6,6.8H0z M39.4,93.7h50.5c10.7,0,18.9-2.2,24.6-6.6c5.7-4.4,8.5-10.8,8.5-19.1c0-8.4-2.8-14.7-8.4-18.8
                        c-5.6-4.2-13.8-6.3-24.7-6.3H39.4V93.7z M39.4,182.7h60.9c12.4,0,21.7-2.3,27.9-6.8c6.2-4.5,9.3-11.3,9.3-20.3
                        c0-8.9-3.2-15.8-9.6-20.7c-6.4-4.8-15.6-7.3-27.6-7.3H39.4V182.7z"/>
                        <path class="stblue" d="M269.3,217.4c-22.9,0-39.3-4.5-49.3-13.5c-10-9-15.1-23.7-15.1-44.2V88.6h37.4v62.2c0,13.2,2,22.5,6,27.9
                        c4,5.4,11,8.1,20.9,8.1c9.8,0,16.7-2.7,20.7-8.1c4.1-5.4,6.1-14.7,6.1-27.9V88.6h37.4v71.1c0,20.4-5,35.1-15,44.2
                        C308.6,212.9,292.2,217.4,269.3,217.4z"/>
                        <path class="stblue" d="M357.8,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H357.8z"/>
                        <path class="stblue" d="M502.6,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H502.6z"/>
                        <path class="storg" d="M682.9,62.6c-4.1,0-7.8-1-11.2-3c-3.4-2-6.1-4.7-8.1-8.1c-2-3.4-3-7.2-3-11.2c0-4.1,1-7.8,3.1-11.1
                        c2-3.3,4.7-6,8.1-8c3.3-2,7-3,11.1-3c4.1,0,7.8,1,11.1,3c3.3,2,6,4.7,8.1,8c2,3.3,3,7,3,11.1c0,4.1-1,7.8-3,11.2
                        c-2,3.4-4.7,6.1-8.1,8.1C690.6,61.6,686.9,62.6,682.9,62.6z M664.2,213.5V88.6h37.4v124.9H664.2z"/>
                        <path class="storg" d="M751.3,213.5V117h-16.8V88.6h16.8v-31c0-11.8,1.8-22,5.5-30.6c3.7-8.6,8.9-15.3,15.8-19.9
                        c6.9-4.7,15.1-7,24.7-7c5.9,0,11.8,0.9,17.6,2.7c5.8,1.8,10.7,4.4,14.9,7.6l-12.9,26.2c-3.8-2.4-8.1-3.5-12.9-3.5
                        c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.5,8.9c-0.8,3.8-1.2,8.2-1.2,13.2v30.3h29.5V117h-29.5v96.5H751.3z"/>
                        <path class="storg" d="M839.4,276.9l40.3-79L820.3,88.6h42.9l38.2,73.2l33.3-73.2h42.9l-95.4,188.3H839.4z"/>
                        </g>
                        </svg>
                        </div>
                                <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30">Basic Plan</div>
                                <div class="center-line"></div> 
                            </div>
                            <div class="">
                                <ul class="table-list pl0 f-18 f-md-18 w500 lh150 black-clr">
                        <li>Done for You Lead generation Funnels</li>
                        <li>Inbuilt Social Syndication system</li>
                        <li>One Click A.I Analyzed offers and Links</li>
                        <li>Monetize better and MAKE MORE PROFITS with stunning banner ads</li>
                        <li>Click and Point Ecom Commission Pages</li>
                        <li>FB Messenger Chat Integration</li>
                        <li>Ultra-Fast Funnel Creation</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Double the POWER Of Your Campaigns With Smart Animation Effects</li>
                        <li>Integration with over 70+ Channels</li>
                        <li>Safe, Secure & 100% GDPR Complaint</li>
                        <li>Attractive Ready-to-Use Themes That Add Spark To Your Sites</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Curate UNLIMITED Fascinating Viral Videos</li>
                        <li>Instantly Curate Trending and Viral Content</li>
                        <li>Packed with PROVEN Converting & Ready-To-Use Promotional Templates</li>
                        <li>Easy-to-use, Inbuilt Text & Inline Editor</li>
                        <li>Auto-Curation of Top & Related Videos</li>
                        <li>Share your content & video capsules on 3 major Social Networks</li>
                        <li>Inbuilt content-spinner to create unique description for your stories</li>
                        <li>Boost engagement levels of your sites</li>
                        <li>All-in-one cloud-based site builder</li>
                        <li>Personal Use License included</li>
                        <li class="cross-sign">Use For Your Clients</li>
                        <li class="cross-sign">Provide High In Demand Services</li>
                        <li class="headline">BONUSES WORTH $2255 FREE!!!</li>
                        <li class="cross-sign">Fast Action Bonus 1 - DotcomPal- All in One Growth Platform</li>
                        <li class="cross-sign">Fast Action Bonus 2 - LIVE Training( $997 Value)</li>
                        <li class="cross-sign">Fast Action Bonus 3 - Auto Video Creator (Worth $297)</li>
                        <li class="cross-sign">Fast Action Bonus 4 - Advanced Video Marketing (Worth $397)	</li>
                                </ul>
                            </div>
                            <div class="table-btn">
                                <div class="hideme-button">
                                    <a href="https://warriorplus.com/o2/buy/sq4h8c/dtcw83/bntldv"><img src="https://warriorplus.com/o2/btn/fn200011000/sq4h8c/dtcw83/296609" alt="Academiyo Standard" class="img-fluid d-block mx-auto" border="0" /></a>
                                </div>
                            </div>
                        </div>
                        </div> -->
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 relative">
                           <div class="table-head1">
                              <div>
                                 <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                                    <style type="text/css">
                                       .st0{fill:#FFFFFF;}
                                       .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                                    </style>
                                    <g>
                                       <g>
                                          <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                                             />
                                          <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                                             c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                                             c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                                             c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                             c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                                          <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                                             c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                             c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                                             M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                             c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                                          <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                                             c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                                             c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                                             M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                                             c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                                          <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                                             c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                                             c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                                             c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                                             c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                                             c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                                          <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                                             c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                                             c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                                             h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                                             c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                                          <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                                             c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                                             C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                                          <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                                          <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                                             c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                                             c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                                             C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                                             c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                                             c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                                       </g>
                                       <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                                          c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                                          c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                                          c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                                          c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                                          L1194.92,0z"/>
                                    </g>
                                 </svg>
                              </div>
                              <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30 white-clr">Premium</div>
                           </div>
                           <div class="">
                              <ul class="table-list1 pl0 f-16 f-md-16 w400 lh150 white-clr">
                                 <li><span class="w600">No. Of Lessons- Unlimited </span></li>
                                 <li><span class="w600">No. Of Academies – 50</span></li>
                                 <li><span class="w600">Done-For-You 400+ HD Video Trainings Packed in 30+ RED HOT Courses</span></li>
                                 <li><span class="w600">List & Sell Your Courses On Your Own Academy Site.</span></li>
                                 <li><span class="w600">Accept Payments With All Top Platforms – PayPal, Jvzoo, ClickBank, Warrior Plus, PayDotCom etc.</span></li>
                                 <li>Create Beautiful Academy Sites With Marketplace, Blog & Members Area In Few Minutes</li>
                                 <li>No Traffic, Leads Or Profit Sharing With Any 3rd Party Marketplaces- Have 100% Control On Your Business.</li>
                                 <li>Ready To Use Affiliate System </li>
                                 <li>Get 30+ DFY Funnels to Start Selling Right Away </li>
                                 <li>Manage Leads With Inbuilt Lead Management System </li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training </li>
                                 <li>100% Newbie Friendly & Fully Cloud Based Software</li>
                                 <li>Live Chat - Customer Support </li>
                                 <li><span class="w600">Commercial License Included </span></li>
                                 <li>Use For Your Clients </li>
                                 <li>Provide High In Demand Services </li>
                                 <li>Make 5K-10K/Month Extra with Commercial License </li>
                                 <li class="headline">BONUSES WORTH $2465 FREE!!!</li>
                                 <li>Limited Time Bonus #1 – DotcomPal - All in One Platform</li>
                                 <li>Limited Time Bonus #2 – LIVE Training</li>
                                 <li>Limited Time Bonus #3 – Private Access to Online Business VIP Club</li>
                                 <li>Limited Time Bonus #4 – Secret Traffic Sources Video Training</li>
                                 <li>Limited Time Bonus #5 – 6 To 7 Figure Video Training</li>
                                 <li>Limited Time Bonus #6 – How To Become A Highly Profitable Speaker</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j"><img src="https://warriorplus.com/o2/btn/fn200011000/hwcfc8/p29hsc/307801" class="img-fluid d-block mx-auto" border="0"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--<div class="col-12 mt30">
                  <div class="f-md-22 f-20 w500 lh150 text-center">
                     *Commercial License Is ONLY Available with The Purchase Of Premium Plan
                  </div>
                  </div>-->
            </div>
         </div>
      </div>
      <!-- Table Section End -->
      <!-- Guarantee Section Start -->
      <div class="guarantee-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w500 lh150 white-clr text-center">
                  We’re Backing Everything Up with An Iron Clad...
                  <span class="w700 orange-clr f-md-45 f-28">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
               <div class="col-md-8 col-12 order-md-2 offset-md-0">
                  <div class="f-md-20 f-18 lh150 w400 white-clr text-xs-center">
                     I'm 100% confident that Academiyo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.<br><br> If you concluded that, HONESTLY nothing of this
                     has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!<br><br>
                     <span class="">Note:</span> For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!<br><br> I am considering your money to be kept safe on the table between us and
                     waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-4 order-md-1 col-12 mt20 mt-md0">
                  <img src="assets/images/mbg.png" class="img-fluid d-block mx-auto wh300">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Awesome Section Start -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally <span class="w600">help you build beautiful academy site with RED HOT courses ready-to-sell - without wasting tons of money! 
                     </span> <br><br> <span class="w600">So, take action now... and I promise you won't be disappointed! </span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     Use Discount Coupon <span class="blue-clr w700">"acadearly "</span> for Instant <span class="blue-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
               <div class="col-12 w600 f-md-28 f-22 text-start mt20 mt-md100">To your success</div>
               <div class="col-12  col-md-8 mx-auto mt20 mt-md30">
                  <div class="row align-items-md-baseline">
                     <div class="col-md-6 col-12 text-center">
                        <img src="assets/images/ayush-jain.png" class="img-fluid d-block mx-auto">
                        <div class="f-22 w700 lh150 text-center black-clr mt15">
                           Ayush Jain
                        </div>
                        <div class="f-15 lh150 w500 text-center black-clr">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/pranshu-gupta.png" class="img-fluid d-block mx-auto">
                        <div class="f-22 w700 lh150 text-center black-clr mt15">
                           Pranshu Gupta
                        </div>
                        <div class="f-15 lh150 w500 text-center black-clr">
                           (Internet Marketer &amp; Product Creator)
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">P.S- You can try "Academiyo" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an
                     absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.
                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping academy sites packed with RED HOT courses ready-to-sell and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles. 
                     <br><br>
                     <span class="w600">P.S.S Don't Procrastinate - Get your copy of Academiyo! </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to
                     miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Awesome Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  Frequently Asked Questions
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I need to download or install Academiyo somewhere?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! You just create an account online and you can get started immediately. Academiyo is 100% web-based platform hosted on the cloud.<br>
                              This means you never have to download anything ever. And It works across all browsers and all devices including Windows and Mac.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you tried it before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its 100% optional & not mandatory for working with Academiyo. Those are recommended if you want to multiply your benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support for my questions?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member’s area. You will also get live chat - customer support so you never get stuck or have any issues.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Academiyo compliant with all guidelines & compliances?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party services which is not a part of Academiyo while choosing it for your business.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What is the duration of service with this Academiyo launch special deal?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary renewal to our founder members for buying from us early.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How is Academiyo is different from other available tools in the market?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, we have a nice comparison chart with other service providers. We won’t like to boast much about our software, but we can assure you that this is a cutting edge technology that will enable you to create and sell courses on your pro academy site at such a low introductory price.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Academiyo Windows and Mac compatible?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We’ve already stated that Academiyo is fully cloud-based. So, it runs directly on the web and works across all browsers and all devices.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-20 f-md-26 w700 lh150 text-center black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <!-- <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     Use Discount Coupon <span class="blue-clr w700">"acadearly "</span> for Instant <span class="blue-clr w700">4% OFF</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                        <a href="https://warriorplus.com/o2/buy/hwcfc8/p29hsc/s1x35j">Get Instant Access To Academiyo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
	       <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-22 w600 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                     <img src="assets/images/caution-icon.png" class="img-fluid mr-md5 mb5 mb-md0">
                     <u>LIMITED &nbsp;Time Offer </u>– &nbsp;Grab Your Copy Before The Price Increases!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                     </style>
                     <g>
                        <g>
                           <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"
                              />
                           <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                              c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                              c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                              c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"/>
                           <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                              M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"/>
                           <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                              M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"/>
                           <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                              c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                              c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                              c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                              c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"/>
                           <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                              c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                              c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                              h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                              c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"/>
                           <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                              c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                              C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"/>
                           <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"/>
                           <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                              c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                              c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                              C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                              c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                              c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"/>
                        </g>
                        <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                           c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                           c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                           c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                           c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                           L1194.92,0z"/>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © Academiyo 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://academiyo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>
<!Doctype html>
<html>

<head>
    <title>WriterArc Pro</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="WriterArc Pro">
    <meta name="description" content="Remove All Limitations To Go Unlimited  & Get 3X More Profits Faster & Easier With No Extra Efforts">
    <meta name="keywords" content="WriterArc">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.writerarc.com/pro/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WriterArc Pro">
    <meta property="og:description" content="Remove All Limitations To Go Unlimited  & Get 3X More Profits Faster & Easier With No Extra Efforts">
    <meta property="og:image" content="https://www.writerarc.com/pro/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WriterArc Pro">
    <meta property="twitter:description" content="Remove All Limitations To Go Unlimited  & Get 3X More Profits Faster & Easier With No Extra Efforts">
    <meta property="twitter:image" content="https://www.writerarc.com/pro/thumbnail.png">
    <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">

    <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
</head>

<body>

    <!-- New Timer  Start-->
    <?php
    $date = 'September 16 2022 11:59 AM EST';
    $exp_date = strtotime($date);
    $now = time();  
    /*
    
    $date = date('F d Y g:i:s A eO');
    $rand_time_add = rand(700, 1200);
    $exp_date = strtotime($date) + $rand_time_add;
    $now = time();*/
    
    if ($now < $exp_date) {
    ?>
 <?php
    } else {
     echo "Times Up";
    }
    ?>
 <!-- New Timer End -->

    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 mx-auto text-center">
                    <img src="assets/images/progress-bar.webp" alt="progress-bar" class="img-fluid mx-auto d-block">
                </div>
                <div class="col-12 text-center mt20 mt-md40">
                    <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:40px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                </div>
                <div class="col-12 mt20 mt-md40 text-center">
                    <div class="f-18 f-md-22 w500 white-clr lh140">
                       <span class="blue-clr1 w700"> Congratulations!</span> Your order is almost complete. But before we go further, we have a special offer for you…
                    </div>
                </div>
                <div class="col-12 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                    <span class="gradient-orange"> Remove All Limitations to Unlock Full Potential </span> <br class="d-none d-md-block"><span class="gradient-orange">
                    and Get 3X More Profits Faster and Easier</span>
                </div>
                <div class="col-12 mt-md30 mt20  text-center">
                    <div class="post-headline f-18 f-md-22 w500 text-center lh140 white-clr">
                    Generate Unlimited Content | Define Your Own Niche | Create Custom Templates | Start    <br class="d-none d-md-block"> Blogging Advance Integration | Built-In Text to Speech Converter | Commercial License

                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">  
                    <!-- <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="Feat8">                 -->
                    <div class="responsive-video ">
                        <iframe src="https://writerarc.dotcompal.com/video/embed/ie1alrlxzn" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md50">
                <div class="col-12  f-18 f-md-20 lh140 w500 white-clr text-center">
                    This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center d-block d-md-inline-block">Upgrade to WriterArc Pro Now</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                   <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block">
                </div>
                <div class="col-md-5 mx-auto col-12 mt20 mt-md20">
                    <div class="f-18 f-md-20 lh140 w500 white-clr text-center">
                        Deal Ending In…
                    </div>
                    <div class="countdown counter-black mt15"><div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">08</span><br><span class="f-18 w600">Days</span> </div><div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">01</span><br><span class="f-18 w600">Hours</span> </div><div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">48</span><br><span class="f-18 w600">Mins</span> </div><div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">39</span><br><span class="f-18 w600">Sec</span> </div></div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->

    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container mycontainer">
            <div class="row">
                <div class="col-12 text-center text-capitalize">
                    <div class="f-md-45 f-28 w700 lh140 text-capitalize text-center black-clr">
                        You Are About to Witness the RAW Power of <br class="d-none d-md-block">  WriterArc in A Way You Have Never Seen It.
                    </div>
                </div>
                <div class="col-12 mt20">
                    <div class="f-20 f-md-24 text-center w400 lh140">
                        A Whopping 92% Of The WriterArc Members Have Upgraded To WriterArc Pro So Far, Because...
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50">   
                    <div class="header-list-block">   
                        <div class="row gx-4 align-items-center">
                            <div class="col-md-6 col-12 order-md-2">
                                <ul class="f-20 f-md-22 w400 pro-list lh150 mb0 text-capitalize">
                                    <li><span class="w600">Break Free & Go Limitless</span> - Get Everything Unlimited, Plus Addition Pro Features</li>
                                    <li>Don’t Limit Your Content to 300-500 Character, <span class="w600">Unlock Unlimited Longer Content Length</span> for All Your Content Types</li>
                                    <li><span class="w600">Define your Own Niche</span> other than Predefine niche to Automate your Content Creation.</li>
                                    <li><span class="w600">Create your own writing template</span> as per your use case.</li>
                                    <li><span class="w600">Start Blogging</span> by directly posting your blog post from WriterArc to Blogger or WordPress Website with the help of <span class="w600">advanced integration.</span></li>
                                    <li><span class="w600">Built-In Text-to-Speech Converter</span> with 150+ AI & Human Voice in 30+ Languages.</li>
                                    <li><span class="w600">Create Unlimited Voiceover</span> for up to 15000 Characters</li>
                                    <li><span class="w600">Create Unlimited Podcasts</span> in Any Language from Any Script</li>
                                    <li><span class="w600">Convert any E-book to Audiobook</span> in 30+ languages.</li>
                                    <li><span class="w600">Commercial Use License</span> to sell Content and Voiceover Services</li>
                                    <li>Get All These Benefits at An <span class="w600">Unparalleled Price</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                                <img src="assets/images/robot-mockup.webp" alt="Productbox" class="d-block mx-auto img-fluid">
                                <div class="row mt20 mt-md70">
                                    <div class="col-12  f-18 f-md-20 lh140 w500 black-clr text-center">
                                        This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                                    </div>
                                    <div class="col-12 mt20 mt-md20">
                                        <div class="f-20 f-md-35 text-center probtn1">
                                            <a href="#buynow" class="text-center d-block d-md-inline-block">Upgrade to WriterArc Pro Now</a>
                                        </div>
                                    </div>
                                    <div class="col-12 mt20 mt-md20">
                                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options1.webp" class="img-fluid mx-auto d-block">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!--3. Third Section Start -->
    <div class="third-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="f-md-40 f-26 w700 text-center black-clr title">
                        This is your Ultimate Chance to Get an <br class="d-none d-md-block">  Unfair Advantage Over Your Competition
                    </div>
                    <div class="f-18 f-md-20 w400 mt20 mt-md35 lh160 text-center">
                        If you are on this page, you have already realized the power of this Disruptive A.I. Technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results, just think what more this Pro-upgrade can bring to the table. <span class="w600"> Go Break Free and Limitless, Generate Unlimited Content, Unlock Unlimited Longer Content-Length, Define Your Own Niche, Create Custom Templates, Start Blogging Advance Integration, Built-In Text to Speech Converter and Other Pro Features, completely transforms what you can do with WriterArc.</span>
                        <br><br>
                        I know you're probably very eager to get to your member’s area and <span class="w600">use WriterArc to get more engagement, sales, and profits from your content marketing.</span> However, if you can give me a few minutes I'll show you how to take this system beyond basic features using it to skyrocket your profit.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->

    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center lh140">
                    <div class="per-shape f-28 f-md-34 w700">
                        Presenting…
                    </div>
                </div>
                <div class="col-12 text-center mt20 mt-md40">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 841.3 100" style="enable-background:new 0 0 841.3 100; max-height: 100px;" xml:space="preserve">
                        <style type="text/css">
                            .st0{fill:#FFFFFF;}
                            .st1{fill:#5055BE;}
                        </style>
                        <g>
                            <g>
                                <path class="st0" d="M116.7,15.6L93.4,98.7H73.8L58.2,39.5L41.9,98.7l-19.5,0.1L0,15.6h17.9l14.6,64.5l16.9-64.5H68l16,64.2
                                l14.8-64.2H116.7z"/>
                                <path class="st0" d="M152.5,34.8c3.5-2,7.4-3,11.8-3v17.5H160c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7V32.8
                                h16.7V43C146.3,39.5,149.1,36.8,152.5,34.8z"/>
                                <path class="st0" d="M176.7,22.1c-1.9-1.9-2.9-4.2-2.9-7c0-2.8,1-5.1,2.9-7c1.9-1.9,4.4-2.8,7.3-2.8c2.9,0,5.4,0.9,7.3,2.8
                                c1.9,1.9,2.9,4.2,2.9,7c0,2.8-1,5.1-2.9,7c-1.9,1.9-4.4,2.8-7.3,2.8C181.1,24.9,178.6,24,176.7,22.1z M192.2,32.8v65.9h-16.7V32.8
                                H192.2z"/>
                                <path class="st0" d="M228.1,46.4v31.9c0,2.2,0.5,3.8,1.6,4.8c1.1,1,2.9,1.5,5.4,1.5h7.7v14h-10.5c-14,0-21.1-6.8-21.1-20.5V46.4
                                h-7.9V32.8h7.9V16.4h16.8v16.3h14.8v13.7H228.1z"/>
                                <path class="st0" d="M315.7,70.7h-48.2c0.4,4.8,2.1,8.5,5,11.2c2.9,2.7,6.5,4,10.8,4c6.2,0,10.6-2.7,13.2-8h18
                                c-1.9,6.4-5.6,11.6-11,15.7c-5.4,4.1-12,6.1-19.9,6.1c-6.3,0-12-1.4-17.1-4.2c-5-2.8-9-6.8-11.8-12c-2.8-5.2-4.2-11.1-4.2-17.9
                                c0-6.8,1.4-12.8,4.2-18c2.8-5.2,6.7-9.1,11.7-11.9c5-2.8,10.8-4.2,17.3-4.2c6.3,0,11.9,1.4,16.8,4c5,2.7,8.8,6.5,11.5,11.5
                                c2.7,5,4.1,10.7,4.1,17.1C316.1,66.7,316,68.8,315.7,70.7z M298.9,59.5c-0.1-4.3-1.6-7.7-4.6-10.3c-3-2.6-6.7-3.9-11.1-3.9
                                c-4.1,0-7.6,1.2-10.4,3.7c-2.8,2.5-4.5,6-5.2,10.4H298.9z"/>
                                <path class="st0" d="M353.3,34.8c3.5-2,7.4-3,11.8-3v17.5h-4.4c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7V32.8
                                h16.7V43C347.1,39.5,349.9,36.8,353.3,34.8z"/>
                            </g>
                            <g>
                                <path class="st0" d="M534.2,35c3.5-2,7.4-3,11.8-3v17.5h-4.4c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7V33h16.7
                                v10.2C527.9,39.7,530.7,37,534.2,35z"/>
                                <path class="st0" d="M557.1,48c2.8-5.1,6.6-9.1,11.5-11.9c4.9-2.8,10.6-4.2,16.9-4.2c8.2,0,14.9,2,20.3,6.1
                                c5.4,4.1,8.9,9.8,10.8,17.2h-18c-1-2.9-2.6-5.1-4.8-6.7c-2.3-1.6-5.1-2.4-8.4-2.4c-4.8,0-8.5,1.7-11.3,5.2
                                c-2.8,3.5-4.2,8.4-4.2,14.7c0,6.3,1.4,11.1,4.2,14.6c2.8,3.5,6.5,5.2,11.3,5.2c6.7,0,11.1-3,13.2-9h18c-1.8,7.1-5.4,12.8-10.8,17
                                c-5.4,4.2-12.1,6.3-20.2,6.3c-6.4,0-12-1.4-16.9-4.2c-4.9-2.8-8.8-6.8-11.5-11.9c-2.8-5.1-4.2-11.1-4.2-17.9
                                C552.9,59.1,554.3,53.2,557.1,48z"/>
                                <path class="st0" d="M714.6,54.1c-2.1,3.9-5.4,7-9.9,9.4c-4.5,2.4-10.1,3.6-17,3.6h-13.9v31.9h-16.7V15.8h30.6
                                c6.4,0,11.9,1.1,16.4,3.3c4.5,2.2,7.9,5.3,10.2,9.2c2.3,3.9,3.4,8.3,3.4,13.2C717.8,46,716.7,50.2,714.6,54.1z M697.3,50.4
                                c2.2-2.1,3.3-5.1,3.3-8.9c0-8.1-4.5-12.1-13.6-12.1h-13.2v24.2h13.2C691.7,53.6,695.1,52.5,697.3,50.4z"/>
                                <path class="st0" d="M754.6,35c3.5-2,7.4-3,11.8-3v17.5H762c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7V33h16.7
                                v10.2C748.4,39.7,751.2,37,754.6,35z"/>
                                <path class="st0" d="M789.8,95.8c-5.1-2.8-9.1-6.8-12-12c-2.9-5.2-4.3-11.1-4.3-17.9c0-6.7,1.5-12.7,4.5-17.9c3-5.2,7-9.1,12.2-12
                                c5.2-2.8,10.9-4.2,17.3-4.2c6.3,0,12.1,1.4,17.3,4.2c5.2,2.8,9.2,6.8,12.2,12c3,5.2,4.5,11.1,4.5,17.9c0,6.7-1.5,12.7-4.6,17.9
                                c-3.1,5.2-7.2,9.1-12.4,12c-5.2,2.8-11,4.2-17.4,4.2C800.6,100,794.9,98.6,789.8,95.8z M815.4,83.3c2.7-1.5,4.8-3.7,6.4-6.6
                                c1.6-2.9,2.4-6.5,2.4-10.7c0-6.3-1.6-11.1-4.9-14.5c-3.3-3.4-7.3-5.1-12.1-5.1c-4.8,0-8.7,1.7-12,5.1c-3.2,3.4-4.8,8.2-4.8,14.5
                                c0,6.3,1.6,11.1,4.7,14.5c3.1,3.4,7.1,5.1,11.8,5.1C809.9,85.5,812.8,84.7,815.4,83.3z"/>
                            </g>
                            <g>
                                <g>
                                    <path class="st1" d="M468.7,50c-9-2.8-18.6-4.4-28.5-4.4c-12.3,0-24,2.3-34.7,6.5L435.6,0v23.6c-2.6,0.9-4.4,3.3-4.4,6.2
                                    c0,3.6,3,6.6,6.6,6.6c3.6,0,6.6-3,6.6-6.6c0-2.9-1.8-5.3-4.4-6.2V0.2L468.7,50z"/>
                                </g>
                                <g>
                                    <path class="st0" d="M496.8,98.8c-16.2-12.9-36.8-20.7-59.2-20.7c-0.7,0-1.5,0-2.2,0c-21.4,0.5-41.1,8.1-56.8,20.5l21.4-37
                                    c-3.2-0.8-6.6-1.3-9.9-1.7c-3.8-0.5-7.6-0.7-11.5-0.7c8.2-3.4,16.9-5.7,26-6.7c0.2,0,0.5-0.1,0.7-0.1c3.2-0.3,6.4-0.5,9.7-0.5
                                    c34.8,0,65.3,18.7,81.8,46.7L496.8,98.8z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md50">
                    <div class="f-md-36 f-28 text-center white-clr lh140 w700">
                    Go Break Free and Limitless with WriterArc Pro Upgrade to <br class="d-none d-md-block"> Unlock Full Potential and Make 3 Times More Profits.
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                    <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="ProductBox">
                </div>
            </div>
            <div class="row mt20 mt-md50">
                <div class="col-12  f-18 f-md-20 lh140 w500 white-clr text-center">
                    This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center d-block d-md-inline-block">Upgrade to WriterArc Pro Now</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                   <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block">
                </div>
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->

    <!-- Feature-1 -->
    <div class="advance-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-md-50 f-28 lh140 w700 text-center black-clr">
                        Here’s What You’re Getting with <br class="d-none d-md-block"> <span class="blue-clr">This Unfair Advantage PRO UPGRADE</span>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Generate Unlimited Content Each Month  
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Go Break Free and Limitless, generate as many copies as you want every month and every day, there is no limit. 
                    </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Unlock Unlimited Longer Content-Length
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Don’t Limit Your Content to 300-500 Character, Unlock Unlimited Longer Content-Length for All Your Content Types 
                    </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Define Your Own Niche 
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Define your Own Niche as per your need other than Predefine niches to Automate your Content Creation.
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Create Your Own Template
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Create your own writing template as per your need and use case by defining it in the WriterArc. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Advanced Integration
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Start Blogging by directly posting your blog post from WriterArc to Blogger or WordPress Website with the help of advanced integration. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                    <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Built-In Text to Speech Converter
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Generate Voiceover in 150+ Human & AI voices in 35+ languages. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                    <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Create Unlimited Voiceover
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        You can create an unlimited number of voiceovers for up to 15000 Characters with WriterArc’s built-in Text to Speech Converter. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Create Unlimited Podcasts 
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        With AI Text to Speech converter, you can create an unlimited podcast with any script in 30+ languages. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Convert any E-book to Audiobook
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Use text to speech converter to create audiobooks from any E-books  
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                    <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Commercial License Included
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Commercial License to provide content writing and voiceover service for your clients. 
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                    <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                    <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                        Get All These Benefits at An Unparalleled Price
                    </div>
                    <div class="f-18 f-md-20  w400 lh140 mt10 text-left">
                        Ultimately you are saving thousands of dollars monthly with us, and every dollar that you save, adds to your profits. By multiplying the power of WriterArc you’re not only saving money, but you’re also taking your business to the next level.
                    </div>
               </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                    <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
                </div>
            </div>

        </div>
    </div>

    <!--7. Seventh Section Starts -->
    <div class="good-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="f-md-50 f-28 lh140 w700">
                                Act Now For <br> <span class="gradient-orange">2 Very Good Reasons:</span>
                            </div>
                            <div class="">
                                <img src="assets/images/line.webp" alt="line" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="">
                                <ul class="f-18 f-md-20 lh140 w400 good-list">
                                    <li>We’re offering WriterArc Pro at a CUSTOMER’S ONLY discount, so you can grab this <span class="w700">game-changing upgrade for a low ONE-TIME fee.</span> </li>
                                    <li> <span class="w700">Your only chance to get access is right now,</span> on this page. When this page closes, so does this exclusive offer.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <img src="assets/images/girl-image.webp" class="d-block img-fluid mb-md0 mb15 mx-auto" alt="Step1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section End -->

    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container ">
            <div class="row">
                <!-- <div class="col-12">
                    <div class="f-md-38 f-28 lh140 w700 text-center white-clr">
                        Let’s Remove All The Risks For You And Secure It With <br class="d-none d-md-block"><span class="w700">  30 Days Money Back Guarantee.</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 col-md-4">
                            <img src="assets/images/moneyback.webp" class="img-fluid d-block mx-auto" alt="MoneyBack">
                        </div>
                        <div class="col-12 col-md-8 mt20 mt-md0">
                            <div class="f-md-20 f-18 lh140 w400 white-clr">
                                You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w600">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                                <br><br> <span class="w600">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                                <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-12 col-md-10 mx-auto">
                    <div class="money-container">
                        <div class="f-md-28 f-24 lh140 w700 text-center black-clr">
                            Let’s Remove All The Risks For You And Secure It <br class="d-none d-md-block"> With 30 Days Money Back Guarantee.
                        </div>
                        <div class="f-md-20 f-18 lh140 w400 black-clr text-center mt20 mt-md40">
                            You can buy with confidence because if you face any technical issue which we don’t resolve for you, just raise a ticket within 30 days and we'll refund you everything, down to the last penny.
                            <br><br> Our team has a 99.21% proven record of solving customer problems and helping them through any issues.
                            <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                        </div>
                        <img src="assets/images/money-guarantee.webp" alt="Money Guarantee" class="img-fluid mx-auto d-block mt20 mt-md0 money-gurantee-img">
                        <img src="assets/images/robot.webp" alt="Robot" class="img-fluid mx-auto d-none d-md-block robot-img ">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->

    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="f-md-45 f-28 lh140 w700 feature-shape">
                        But That’s Not All
                    </div>
                </div>
                <div class="col-12 mt40">
                    <div class="f-20 f-md-22 w500 text-center lh140">
                        In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block"> today and start profiting from this opportunity.
                    </div>
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus1.webp" class="img-fluid d-block bonus-size" alt="Bonus1">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Live Video Marketing
                        </div>
                        <div class="f-18 f-md-20  w400 lh140 mt10 mt-md10 text-left">
                            Video marketing is the best way to instantly reach targeted audiences and get them glued to your offers. This is an exclusive guide that provides some of the best ways to use live video to promote your own products and generate the kind of interest top
                            companies gets.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto" alt="BonusBox1">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus2.webp" class="img-fluid d-block bonus-size" alt="Bonus2">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            10 Ways to Create the Perfect Online Video
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Videos in online marketing is increasing at an exponential rate. With this eBook you will learn the techniques that have been carefully researched and practiced bringing you the best and quickest methods of generating an income and improving online video.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto" alt="BonusBox2">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus3.webp" class="img-fluid d-block bonus-size" alt="Bonus3">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Mastering and Marketing Online-Video-Made-Simple
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Video marketing is best way to grab attention of website visitors and retain them forever.
                            <br><br> So, here’s a fascinating E-book that will help you reach millions of potential consumers and create brand awareness and promote your products and services with online videos.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto" alt="BonusBox3">
                        </div>
                    </div>
                </div>
                <!-- bonus4 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus4.webp" class="img-fluid d-block bonus-size" alt="Bonus4">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Surefire Keyword Goldmine
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Keyword research is one of three important SEO factors along with link building, and content marketing.
                            <br><br> This amazing software enables you to discover how to research market demand or keywords that people are searching for with the free google keyword planner.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1  mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto" alt="BonusBox4">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->

    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w500 lh140 text-center">
                        Yup...! Take action on this while you can as you won’t be seeing this offer again.
                    </div>
                    <div class="f-md-38 f-28 w700 lh140 mt20 text-center black-clr">
                        Today You Can <span class="blue-clr"> Get Unrestricted Access to WriterArc </span> for
                        LESS Than the Price of Just One Month’s Membership.
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt30 mt-md70">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                        <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:50px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                            <div class="tbbg2-text w600 f-md-36 f-24 text-center mt20 mt-md25">
                                 Pro Upgrade
                            </div>
                        </div>
                        <div class="text-center">
                            <ul class="f-18 f-md-20 w400 lh140 text-center vgreytick mb0 text-capitalize">
                                <li class="even">
                                  <span class="w700">Go Break Free and Limitless</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Generate Unlimited Content Each Month</span>
                                </li>
                                <li class="odd">
                                    <span class="w700">Unlock Unlimited Longer Content-Length</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Define Your Own Niche</span>
                                </li>
                                <li class="odd">
                                    <span class="w700"> Create Your Own Template</span>
                                </li>
                                <li class="odd">
                                    <span class="w700">Advanced Integration</span>
                                </li>
                                <li class="odd">
                                    <span class="w700">Built-In Text to Speech Converter</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Create Unlimited Voiceover</span>
                                </li>
                                <li class="odd">
                                    <span class="w700">Create Unlimited Podcasts</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Convert any E-book to Audiobook</span>
                                </li>
                                <li class="odd">
                                    <span class="w700">Commercial License Included</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Get All These Benefits at An Unparalleled Price</span>
                                </li>
                                <li class="odd headline f-md-24">
                                    <span class="w700">Special Upgrade Bonuses</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Bonus 1 - </span> Live Video Marketing
                                </li>
                                <li class="odd">
                                    <span class="w700">Bonus 2 - </span> 10 Ways to Create the Perfect Online Video
                                </li>
                                <li class="even">
                                    <span class="w700">Bonus 3 - </span> Mastering and Marketing Online-Video-Made-Simple
                                </li>
                                <li class="odd">
                                    <span class="w700">Bonus 4 - </span> Surefire Keyword Goldmine
                                </li>
                            </ul>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme relative mb-md15" editabletype="shape" style="opacity: 1; z-index: 8;">
                            <div class="col-12 p0">
                                <div class="row">
                                    <div class="col-md-6 col-12  mb-md0 mb30 mb-md0">
                                        <div class="f-md-26 f-20 w700 lh140 mb-md20 mb15 text-center">
                                            Monthly Plan
                                        </div>
                                        <div>
                                            <a href="https://www.jvzoo.com/b/106971/386758/2"><img src="https://i.jvzoo.com/106971/386758/2" alt="WriterArc Pro Monthly" border="0" class="img-fluid mx-auto d-block" /></a> 
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12 ">
                                        <div class="f-md-26 f-20 w700 lh140 mb-md20 mb15 text-center">
                                            One Time Plan
                                        </div>
                                        <div>
                                            <a href="https://www.jvzoo.com/b/106971/386760/2"><img src="https://i.jvzoo.com/106971/386760/2" alt="WriterArc Pro" border="0" class="img-fluid mx-auto d-block"/></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 thanks-button text-center">
                    <a class="f-18 f-md-20 lh140 w500 text-capitalize" href="https://writerarc.com/lite/" target="_blank">
                        No Thanks - I Don't Want To Remove All Limitations To Go Unlimited & Get 3X More Profits Faster & Easier With This Pro Upgrade. Please Take Me To The Next Step To Get Access To My Purchase.
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->

    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                    <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                    <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                    <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                    <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                    <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                    <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                    <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                    <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                    <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                    <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                    </svg>
                    <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
                    <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->

          <!-- timer --->
          <?php
          if ($now < $exp_date) {
          
          ?>
       <script type="text/javascript">
          // Count down milliseconds = server_end - server_now = client_end - client_now
          var server_end = <?php echo $exp_date; ?> * 1000;
          var server_now = <?php echo time(); ?> * 1000;
          var client_now = new Date().getTime();
          var end = server_end - server_now + client_now; // this is the real end time
          
          var noob = $('.countdown').length;
          
          var _second = 1000;
          var _minute = _second * 60;
          var _hour = _minute * 60;
          var _day = _hour * 24
          var timer;
          
          function showRemaining() {
              var now = new Date();
              var distance = end - now;
              if (distance < 0) {
                  clearInterval(timer);
                  document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                  return;
              }
          
              var days = Math.floor(distance / _day);
              var hours = Math.floor((distance % _day) / _hour);
              var minutes = Math.floor((distance % _hour) / _minute);
              var seconds = Math.floor((distance % _minute) / _second);
              if (days < 10) {
                  days = "0" + days;
              }
              if (hours < 10) {
                  hours = "0" + hours;
              }
              if (minutes < 10) {
                  minutes = "0" + minutes;
              }
              if (seconds < 10) {
                  seconds = "0" + seconds;
              }
              var i;
              var countdown = document.getElementsByClassName('countdown');
              for (i = 0; i < noob; i++) {
                  countdown[i].innerHTML = '';
          
                  if (days) {
                      countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + days + '</span><br><span class="f-18 w600">Days</span> </div>';
                  }
          
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + hours + '</span><br><span class="f-18 w600">Hours</span> </div>';
          
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + minutes + '</span><br><span class="f-18 w600">Mins</span> </div>';
          
                  countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">' + seconds + '</span><br><span class="f-18 w600">Sec</span> </div>';
              }
          
          }
          timer = setInterval(showRemaining, 1000);
       </script>
       <?php
          } else {
          echo "Times Up";
          }
          ?>
       <!--- timer end-->
</body>

</html>
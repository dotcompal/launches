<!Doctype html>
<html>
   <head>
      <title>WriterArc Agency</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WriterArc Agency">
      <meta name="description" content="Setup Your Own Profitable Agency Instantly and Make Up to $1000 Per Client Without Any Extra Effort?">
      <meta name="keywords" content="">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.writerarc.com/agency/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WriterArc Agency">
      <meta property="og:description" content="Setup Your Own Profitable Agency Instantly and Make Up to $1000 Per Client Without Any Extra Effort?">
      <meta property="og:image" content="https://www.writerarc.com/agency/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WriterArc Agency">
      <meta property="twitter:description" content="Setup Your Own Profitable Agency Instantly and Make Up to $1000 Per Client Without Any Extra Effort?">
      <meta property="twitter:image" content="https://www.writerarc.com/agency/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <!-- End -->
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'September 20 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div>
                     <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:40px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-24 w500 white-clr lh140">                       
                     Before You Access Your Purchase of WriterArc, I have a Question for You!
                  </div>
               </div>
               <div class="col-12 mt-md30 mt15 f-md-48 f-28 w700 text-center white-clr lh140">
                  How Would You Like to Setup Your Own Profitable Agency Instantly and <span class="gradient-orange"> Make Huge </span> <span class="gradient-orange"> Profit Per Client </span> Without Any Extra Effort?
               </div>
               <div class="col-12 mt-md30 mt20  text-center">
                  <div class="f-18 f-md-24 w500 text-center lh140 white-clr">
                     We Did All the Hard Work for You!! You Just Have to Start Serving High in Demand <br class="d-none d-md-block">
                     Content Writing Services to Hungry Clients & Keep 100% Profits                        
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <!-- <img src="assets/images/productbox.webp" class="d-block img-fluid mx-auto"> -->
                  <div class="responsive-video ">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/g1grbzwfvc" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12  f-18 f-md-20 lh140 w500 white-clr text-center">
                  This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
               </div>
               <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                  <div class="f-18 f-md-36 text-center probtn1">
                     <a href="#buynow" class="text-center">Upgrade to WriterArc Agency Now</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <img src="assets/images/payment-options.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-5 mx-auto col-12 mt20 mt-md20">
                  <div class="f-18 f-md-20 lh140 w500 white-clr text-center">
                     Deal Ending In…
                  </div>
                  <div class="countdown counter-black mt15">
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg col-12">01</span><br><span class="f-18 w600">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">16</span><br><span class="f-18 w600">Hours</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">59</span><br><span class="f-18 w600">Mins</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">37</span><br><span class="f-18 w600">Sec</span> </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12">
                        <div class="f-md-40 f-28 w700 lh140 text-capitalize text-center black-clr mt-md50">
                           The Secret Weapon To 10X Your Profits <br class="d-none d-md-block">
                           From Your WriterArc Subscription
                        </div>
                     </div>
                     <div class="col-12 text-center">
                        <div class="f-18 f-md-20 text-center w400 lh140 text-left mt15 mt-md40">
                           This Agency upgrade puts you on another level with a click of a button 
                           and <b>enables you to tap into the $413 Billion Content Marketing Industry. </b>
                           <br><br>
                           The agency upgrade has everything super A.I. content writer, tons of predefine writing 
                           templates & use cases in tons of niches, flexibility to define your own template, 
                           a complete business/team management panel to manage your client’s biz, white label 
                           license to WriterArc, and an agency license to serve unlimited clients from a single dashboard. 
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt30 mt-md70">
                        <div class="row">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block" alt="Sell-Image">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <b>Sell high-in-demand content and copy Writing Services to your clients & Make Huge Profit</b> Per Month.
                              </div>
                           </div>
                        </div>
                        <!---3-->
                        <div class="row mt20 mt-md20 align-items-center">
                           <div class="col-md-2 col-12 px5">
                              <div>
                                 <img src="assets/images/fe3.webp" class="img-fluid mx-auto d-block" alt="Serve Unlimited Client">
                              </div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <b>Serve Unlimited Clients with Agency License</b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt30 mt-md70">
                        <div class="row mt20 mt-md0">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe2.webp" class="img-fluid mx-auto d-block" alt="Bussiness Mangement"></div>
                           </div>
                           <div class="col-md-10 col-12 mt20 mt-md0">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <b>Done-For-Your Team Management Panel</b> -Manage all your clients & your team from a single dashboard to have full control.
                              </div>
                           </div>
                        </div>
                        <!--8-->
                        <!--11-->
                        <div class="row mt20 mt-md30">
                           <div class="col-md-2 col-12 px5">
                              <div><img src="assets/images/fe4.webp" class="img-fluid mx-auto d-block" alt="Benefits"></div>
                           </div>
                           <div class="col-md-10  col-12 mt20 mt-md15">
                              <div class="f-18 f-md-20 lh140 w400 d-left-m-center">
                                 <span class="w700">Get All These Benefits</span> For One Time Price                                        
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="row mt20 mt-md70">
                           <div class="col-12  f-18 f-md-20 lh140 w500 black-clr text-center">
                              This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                           </div>
                           <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                              <div class="f-18 f-md-36 text-center probtn1">
                                 <a href="#buynow" class="text-center">Upgrade to WriterArc Agency Now</a>
                              </div>
                           </div>
                           <div class="col-12 mt20 mt-md20">
                              <img src="assets/images/payment-options1.webp" class="img-fluid mx-auto d-block">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--4. Fourth Section Starts -->
      <div class="fifth-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="per-shape f-28 f-md-38 w700">
                     Introducing… 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <svg id="Layer_11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 844.57 100" style="max-height:100px;">
                     <defs>
                        <style>.cls-11{fill:#fff;}.cls-22{fill:#5055be;}</style>
                     </defs>
                     <g>
                        <path class="cls-11" d="M88.76,11.88l-17.66,63.22h-14.94l-11.86-45.01-12.41,45.01-14.85,.09L0,11.88H13.59l11.14,49.09L37.59,11.88h14.13l12.14,48.82,11.23-48.82h13.68Z"/>
                        <path class="cls-11" d="M116.07,26.46c2.63-1.51,5.63-2.26,9.01-2.26v13.31h-3.35c-3.99,0-6.99,.94-9.01,2.81-2.02,1.87-3.03,5.13-3.03,9.78v25h-12.68V24.92h12.68v7.79c1.63-2.66,3.76-4.74,6.39-6.25Z"/>
                        <path class="cls-11" d="M134.45,16.81c-1.48-1.42-2.22-3.18-2.22-5.3s.74-3.88,2.22-5.3c1.48-1.42,3.34-2.13,5.57-2.13s4.09,.71,5.57,2.13c1.48,1.42,2.22,3.19,2.22,5.3s-.74,3.88-2.22,5.3c-1.48,1.42-3.34,2.13-5.57,2.13s-4.09-.71-5.57-2.13Zm11.82,8.11v50.18h-12.68V24.92h12.68Z"/>
                        <path class="cls-11" d="M173.53,35.34v24.27c0,1.69,.41,2.91,1.22,3.67,.81,.76,2.19,1.13,4.12,1.13h5.89v10.69h-7.97c-10.69,0-16.03-5.19-16.03-15.58v-24.18h-5.98v-10.42h5.98V12.51h12.77v12.41h11.23v10.42h-11.23Z"/>
                        <path class="cls-11" d="M240.19,53.81h-36.68c.3,3.62,1.57,6.46,3.8,8.51,2.23,2.05,4.98,3.08,8.24,3.08,4.71,0,8.06-2.02,10.05-6.07h13.68c-1.45,4.83-4.23,8.8-8.33,11.91-4.11,3.11-9.15,4.66-15.13,4.66-4.83,0-9.16-1.07-13-3.21-3.84-2.14-6.82-5.18-8.97-9.1-2.14-3.92-3.22-8.45-3.22-13.59s1.06-9.75,3.17-13.68c2.11-3.92,5.07-6.94,8.88-9.06,3.8-2.11,8.18-3.17,13.13-3.17s9.04,1.03,12.82,3.08c3.77,2.05,6.7,4.97,8.79,8.74,2.08,3.77,3.12,8.11,3.12,13,0,1.81-.12,3.44-.36,4.89Zm-12.77-8.51c-.06-3.26-1.24-5.87-3.53-7.83-2.3-1.96-5.1-2.94-8.42-2.94-3.14,0-5.78,.95-7.92,2.85-2.14,1.9-3.46,4.54-3.94,7.92h23.82Z"/>
                        <path class="cls-11" d="M268.85,26.46c2.63-1.51,5.63-2.26,9.01-2.26v13.31h-3.35c-3.99,0-6.99,.94-9.01,2.81-2.02,1.87-3.03,5.13-3.03,9.78v25h-12.68V24.92h12.68v7.79c1.63-2.66,3.76-4.74,6.39-6.25Z"/>
                     </g>
                     <g>
                        <path class="cls-11" d="M406.44,26.64c2.63-1.51,5.63-2.26,9.01-2.26v13.31h-3.35c-3.99,0-6.99,.94-9.01,2.81-2.02,1.87-3.03,5.13-3.03,9.78v25h-12.68V25.1h12.68v7.79c1.63-2.66,3.76-4.74,6.39-6.25Z"/>
                        <path class="cls-11" d="M423.88,36.56c2.11-3.89,5.04-6.91,8.79-9.06,3.74-2.14,8.03-3.22,12.86-3.22,6.22,0,11.37,1.56,15.44,4.66,4.08,3.11,6.81,7.47,8.2,13.09h-13.68c-.72-2.17-1.95-3.88-3.67-5.12-1.72-1.24-3.85-1.86-6.38-1.86-3.62,0-6.49,1.31-8.6,3.94-2.11,2.63-3.17,6.35-3.17,11.19s1.06,8.47,3.17,11.1c2.11,2.63,4.98,3.94,8.6,3.94,5.13,0,8.48-2.29,10.05-6.88h13.68c-1.39,5.43-4.14,9.75-8.24,12.95-4.11,3.2-9.24,4.8-15.4,4.8-4.83,0-9.12-1.07-12.86-3.21-3.74-2.14-6.67-5.16-8.79-9.06-2.11-3.89-3.17-8.44-3.17-13.63s1.06-9.74,3.17-13.63Z"/>
                        <path class="cls-11" d="M538.81,63.23h-25.18l-4.17,12.05h-13.31l22.73-63.31h14.76l22.73,63.31h-13.4l-4.17-12.05Zm-3.44-10.14l-9.15-26.45-9.15,26.45h18.3Z"/>
                        <path class="cls-11" d="M594.42,26.5c2.84,1.48,5.07,3.4,6.7,5.75v-7.15h12.77v50.54c0,4.65-.94,8.8-2.81,12.45-1.87,3.65-4.68,6.55-8.42,8.69-3.74,2.14-8.27,3.22-13.59,3.22-7.13,0-12.97-1.66-17.52-4.98-4.56-3.32-7.14-7.85-7.74-13.59h12.59c.66,2.29,2.1,4.12,4.3,5.48,2.2,1.36,4.88,2.04,8.02,2.04,3.68,0,6.67-1.1,8.97-3.31,2.29-2.2,3.44-5.54,3.44-10.01v-7.79c-1.63,2.35-3.88,4.32-6.75,5.89-2.87,1.57-6.14,2.35-9.83,2.35-4.23,0-8.09-1.09-11.59-3.26-3.5-2.17-6.26-5.24-8.29-9.19-2.02-3.95-3.03-8.5-3.03-13.63s1.01-9.57,3.03-13.5c2.02-3.92,4.77-6.94,8.24-9.06,3.47-2.11,7.35-3.17,11.64-3.17,3.74,0,7.03,.74,9.87,2.22Zm4.89,15.76c-1.21-2.2-2.84-3.89-4.89-5.07-2.05-1.18-4.26-1.77-6.61-1.77s-4.53,.57-6.52,1.72c-1.99,1.15-3.61,2.82-4.85,5.03-1.24,2.2-1.86,4.82-1.86,7.83s.62,5.66,1.86,7.92c1.24,2.26,2.87,4,4.89,5.21,2.02,1.21,4.18,1.81,6.48,1.81s4.56-.59,6.61-1.77c2.05-1.18,3.68-2.87,4.89-5.07,1.21-2.2,1.81-4.85,1.81-7.93s-.6-5.72-1.81-7.93Z"/>
                        <path class="cls-11" d="M672.58,53.99h-36.68c.3,3.62,1.57,6.46,3.8,8.51,2.23,2.05,4.98,3.08,8.24,3.08,4.71,0,8.06-2.02,10.05-6.07h13.68c-1.45,4.83-4.23,8.8-8.33,11.91-4.11,3.11-9.15,4.66-15.12,4.66-4.83,0-9.16-1.07-13-3.21-3.84-2.14-6.82-5.18-8.97-9.1-2.14-3.92-3.21-8.45-3.21-13.59s1.06-9.75,3.17-13.68c2.11-3.92,5.07-6.94,8.88-9.06,3.8-2.11,8.18-3.17,13.13-3.17s9.04,1.03,12.82,3.08c3.77,2.05,6.7,4.97,8.79,8.74,2.08,3.77,3.12,8.11,3.12,13,0,1.81-.12,3.44-.36,4.89Zm-12.77-8.51c-.06-3.26-1.24-5.87-3.53-7.83-2.3-1.96-5.1-2.94-8.42-2.94-3.14,0-5.78,.95-7.92,2.85-2.14,1.9-3.46,4.54-3.94,7.92h23.82Z"/>
                        <path class="cls-11" d="M724.48,30.03c3.68,3.77,5.52,9.04,5.52,15.8v29.44h-12.68v-27.71c0-3.98-1-7.05-2.99-9.19-1.99-2.14-4.71-3.22-8.15-3.22s-6.26,1.07-8.29,3.22c-2.02,2.14-3.03,5.21-3.03,9.19v27.71h-12.68V25.1h12.68v6.25c1.69-2.17,3.85-3.88,6.48-5.12,2.63-1.24,5.51-1.86,8.65-1.86,5.98,0,10.81,1.89,14.49,5.66Z"/>
                        <path class="cls-11" d="M741.95,36.56c2.11-3.89,5.04-6.91,8.79-9.06,3.74-2.14,8.03-3.22,12.86-3.22,6.22,0,11.37,1.56,15.44,4.66,4.08,3.11,6.81,7.47,8.2,13.09h-13.68c-.72-2.17-1.95-3.88-3.67-5.12-1.72-1.24-3.85-1.86-6.39-1.86-3.62,0-6.49,1.31-8.6,3.94-2.11,2.63-3.17,6.35-3.17,11.19s1.06,8.47,3.17,11.1c2.11,2.63,4.98,3.94,8.6,3.94,5.13,0,8.48-2.29,10.05-6.88h13.68c-1.39,5.43-4.14,9.75-8.24,12.95-4.11,3.2-9.24,4.8-15.4,4.8-4.83,0-9.12-1.07-12.86-3.21-3.74-2.14-6.67-5.16-8.79-9.06-2.11-3.89-3.17-8.44-3.17-13.63s1.06-9.74,3.17-13.63Z"/>
                        <path class="cls-11" d="M844.57,25.1l-31.07,73.9h-13.49l10.87-25-20.11-48.91h14.22l12.95,35.05,13.13-35.05h13.49Z"/>
                     </g>
                     <g>
                        <path class="cls-22" d="M356.6,38.08c-6.85-2.15-14.15-3.32-21.71-3.32-9.33,0-18.24,1.77-26.43,4.98L331.41,0V17.93c-1.96,.68-3.37,2.55-3.37,4.74,0,2.77,2.25,5.02,5.02,5.02s5.02-2.25,5.02-5.02c0-2.19-1.4-4.05-3.36-4.74V.16l21.89,37.92Z"/>
                        <path class="cls-11" d="M378.03,75.19c-12.36-9.85-28.01-15.73-45.04-15.73-.57,0-1.14,.01-1.71,.02-16.32,.38-31.3,6.16-43.23,15.62l16.26-28.16c-2.47-.57-4.99-1.01-7.55-1.32-2.88-.35-5.81-.53-8.78-.53,6.21-2.58,12.86-4.32,19.8-5.07,.18-.02,.37-.04,.56-.06,2.42-.24,4.88-.37,7.36-.37,26.5,0,49.68,14.25,62.28,35.5l.05,.1Z"/>
                     </g>
                  </svg>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md20">
                  <div class="f-md-36 f-28 text-center white-clr lh140 w700">
                     Setup Your Own Content Marketing Agency To <br class="d-none d-md-block">
                     Serve Hungry Clients and Keep 100% Profits 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block img-animation" alt="ProductBox-Img">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12  f-18 f-md-20 lh140 w500 white-clr text-center">
                  This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
               </div>
               <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                  <div class="f-18 f-md-36 text-center probtn1">
                     <a href="#buynow" class="text-center">Upgrade to WriterArc Agency Now</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <img src="assets/images/payment-options.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-5 mx-auto col-12 mt20 mt-md20">
                  <div class="f-18 f-md-20 lh140 w500 white-clr text-center">
                     Deal Ending In…
                  </div>
                  <div class="countdown counter-black mt15">
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg col-12">01</span><br><span class="f-18 w600">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">16</span><br><span class="f-18 w600">Hours</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">59</span><br><span class="f-18 w600">Mins</span> </div>
                     <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">37</span><br><span class="f-18 w600">Sec</span> </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--4. Fourth Section End -->
      <div class="thats-not-all-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                  With WriterArc Agency Upgrade, You Can Tap into 
                  The Very  <span class="blue-clr">Fast-Growing Content Marketing Industry </span>
               </div>
               <div class="col-12 col-md-12  mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-18 f-md-20 lh140 w400 black-clr text-xs-center lh140 mt20 mt-md0">
                           Also, you can <b>start your own highly profitable freelancing copy writing 
                           services</b> or tap into the $413 billion content marketing industry. 
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="assets/images/freelancing-img.webp" class="img-fluid mx-auto d-block" alt="freelancing-img">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr mt20 mt-md50 text-capitalize">
                     See How Much Freelancers Are Charging for Content Writing Services 
                  </div>
                  <div class="mt10 mt-md15">
                     <img src="assets/images/acquiring.webp" class="img-fluid mx-auto d-block" alt="Sales-Funnel">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-xs-center mt15">
                     With a WriterArc Agency License not only you can serve clients in your local area and write content for them using WriterArc proven templates, but also you can find them on websites like Fiverr, Freelancer, Odesk, and Upwork where tons of businesses are looking for a legitimate copywriter.
                  </div>
               </div>
               <div class="col-12 text-center mt30 mt-md80">
                  <div class="one-time-shape" editabletype="shape" style="z-index: 8;">
                     <div class="f-24 f-md-36 w700 text-center lh120 white-clr text-center">And it’s not a 1-Time Income</div>
                  </div>
               </div>
               <div class="col-12  f-30 f-md-50 w700 lh120 black-clr text-center mt30 mt-md5">Over 500K New Businesses <br>
                  Start Every Single Month 
               </div>
               <div class="col-12 f-18 f-md-20 lh140 w400 text-center black-clr mt30 mt-md30">
                  <b> You can do this for life!</b> Add new clients every month, plus retain existing clients for recurring 
                  income again, and again forever. This will keep increasing your profit exponentially month by month. 
                  <br><br>
                  Yes, Tons of Businesses Are Looking for A Solution and there are <b>Absolutely No Limitations! </b>
               </div>
            </div>
         </div>
      </div>
      <div class="amazing-software-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-42 f-28 w700 lh140 text-capitalize text-center">
                     Nothing To Research, Write or Manage Support... 
                     Setup A Complete Done for You System In 3 Simple Steps
                  </div>
               </div>
               <!-- feature 1 -->
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize">
                           Add A New Client In 1 Minute
                        </div>
                        <div class="f-18 f-md-20 lh140 w400 mt10">
                           All you need to do is add a new client’s details, select niche, 
                           and choose templates & you're all set
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                        <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Step1">
                     </div>
                  </div>
               </div>
               <!-- feature 2 -->
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize">
                           Accept The Monthly Payments & Keep 100% Profits
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10">
                           Charge them monthly, yearly, or one-time high fees for providing them with 
                           high-in-demand copywriting services & Keep 100% profits with you.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Step2">
                     </div>
                  </div>
               </div>
               <!-- feature 3 -->
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 black-clr text-capitalize">
                           We Did All the Hard Work.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10">
                           We invested a lot of money to get this disruptive A.I. technology ready. 
                           And you know what the best part is, we will take care of all the customer 
                           support about you and your client’s queries. You don’t even need to lift a finger.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                        <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Step3">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- feature1 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="f-md-50 f-28 w700 lh140 text-center black-clr mb-md0 mb20">
                  Here’s What You are Getting with
                  <br class="d-md-block d-none"><span class="blue-clr">
                  This Agency Upgrade Today</span>
               </div>
               <div class="col-12 col-md-12 mt30 mt-md50">
                  <div>
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/1.webp" class="img-responsive" alt="Feature-Icon1">
                  </div>
                  <div class="col-12 col-md-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr">
                     A Top-Notch Software, WriterArc with Agency License to Provide High in 
                     Demand Copywriting Services to Your Clients. 
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                     <img src="assets/images/feature1.webp" class="img-fluid mx-auto d-block" alt="Feature1">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md30 text-left col-12">
                     Providing high-in-demand copywriting services couldn’t get easier than this. You’re getting the never offered before WriterArc Agency License to provide these services to your clients. This is something that will get you way above your competitors.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature1 -->
      <!--feature2 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/2.webp" class="img-responsive" alt="Feature-Icon2">
                  </div>
                  <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr pl0">
                     Done-For-Your Team Management Panel - Add Unlimited Team and Clients
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                     <img src="assets/images/feature2.webp" class="img-fluid mx-auto d-block" alt="Bussiness-Manage">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 mt-md30 mt10 mt-md60 text-left col-12">
                     Oh yeah! You can manage all your clients & team members from a single dashboard to have full control. You can assign them limited or full access to features according to their role with separate login details to outsource your manual work. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature3 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/3.webp" class="img-responsive" alt="Feature-icon3">
                  </div>
                  <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                     Serve Unlimited Clients with Agency License
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/feature3.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                     WriterArc Agency gives you the complete power to go beyond the normal and have a passive income source by serving unlimited clients in hands-down manner 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature4 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/4.webp" class="img-responsive" alt="Feature-Icon4">
                  </div>
                  <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                     White Label License 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/feature4.webp" class="img-fluid mx-auto d-block" alt="Team Manage">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left">
                     You will also get White Label License along with Agency License to put your own logo and name and present WriterArc as your own tool to your clients. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-7 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/5.webp" class="img-responsive" alt="Feature-Icon5">
                  <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt15 text-left black-clr ">
                     Get All These Benefits for One Time Price
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20">
                     And here's the best part. When you get access to WriterArc Agency, you can get all these benefits by paying just a small one time price. Now that's something you can't afford to miss out on
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-2 mt20 mt-md0">
                  <img src="assets/images/feature5.webp" class="img-fluid mx-auto d-block" alt="Benefit">
               </div>
            </div>
         </div>
      </div>
      <!--money back Start -->
      <div class="moneyback-section">
         <div class="container">
            <div class="row">
               <div class="col-12 mb-md30">
                  <div class="f-md-45 f-28 lh140 w700 text-center white-clr">
                     30 Days Money Back Guarantee
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap">
               <div class="col-md-4 col-12 mt30 mt-md40">
                  <img src="assets/images/moneyback.webp" class="img-fluid mx-auto d-block" alt="MoneyBack">
               </div>
               <div class="col-md-8 col-12 mt30">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     We have absolutely no doubt that you'll love the extra benefits, training and WriterArc Agency upgraded features.
                     <br>
                     <br> You can try it out without any risk. If, for any reason, you’re not satisfied with your WriterArc Agency upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                     <br>
                     <br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back End-->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 white-clr feature-shape">
                     But That’s Not All
                  </div>
               </div>
               <div class="col-12 mt40">
                  <div class="f-20 f-md-22 w400 text-center lh140">
                     In addition, we have a number of bonuses for those who want to take action <br class="d-none d-md-block">
                     today and start profiting from this opportunity
                  </div>
               </div>
               <!-- bonus1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus1.webp" class="img-fluid d-block bonus-size" alt="Bonus1">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                        How to Start a Freelance Business 
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Starting a freelance business is not just as simple as waking up in the morning, 
                        rolling out of bed, and opening your laptop. Freelancing is not just an easy way 
                        to monetize your hobbies and nor is it easier than a “proper” job. Nevertheless, 
                        freelance work is some of the most rewarding work you will ever do. Freelance 
                        can be done part-time, on the side, or, eventually, it may turn into a full-time job.<br><br>
                        This video course will give you all the tools that you need to be a successful 
                        freelancer. It will tackle common problems and answer the most common 
                        questions that new freelancers have.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox">
                     </div>
                  </div>
               </div>
               <!-- bonus2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus2.webp" class="img-fluid d-block bonus-size" alt="Bonus2">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                        Outsourcing Fundamentals Development and Strategy
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        This internet marketing report is based on outsourcing fundamentals development
                        and strategy includes blog posts, forums, YouTube videos, and other related 
                        stuff for your business growth. Use it with the immense powers of WriterArc 
                        Agency Upgrade to get results that your competitor’s envy.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6">
                     <div class="mt20 mt-md50">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox2">
                     </div>
                  </div>
               </div>
               <!-- bonus3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus3.webp" class="img-fluid d-block bonus-size" alt="Bonus3">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                        Success in Business
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Wondering how to dominate your competition with success in business PLR 
                        content package, you’re at the right place. This Package includes a source 
                        Ebook document, 25 PLR articles, product analysis PDF and a fast action 
                        ideas PDF. Don’t spend time thinking, just use this with WriterArc Agency 
                        Upgrade and scale your business to new heights.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox3">
                     </div>
                  </div>
               </div>
               <!-- bonus4 -->
               <div class="col-12 col-md-12  mt40 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus4.webp" class="img-fluid d-block bonus-size" alt="Bonus4">
                     <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                        Recognizing Target Client
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Venture into the right market and tap into tons of untapped clients that are right 
                        there to be grabbed. When combined with WriterArc Agency Upgrade powers, 
                        this package becomes a sure-shot business booster.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt-md30">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox4">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--10. Table Section Starts -->
      <div class="table-section padding10" id="buynow">
         <div class="container">
            <div class="row gx-6">
               <div class="col-12 col-md-12">
                  <div class="f-md-38 f-28 w700 lh140 mt20 mt-md30 text-center">
                     Today You Can <span class="blue-clr">Get Unrestricted Access to WriterArc Agency</span> Suite for Less Than the Price of Just One Month’s Membership.
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox2" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg2 text-center">
                        <div>
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 247 41" style="enable-background:new 0 0 247 41; max-height: 40px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#5055BE;}
                                 .st1{fill:#292F60;}
                              </style>
                              <path d="M46.7,6.2l-9.3,33.2h-7.9l-6.2-23.7l-6.5,23.7l-7.8,0L0,6.2h7.2L13,32.1l6.8-25.8h7.4l6.4,25.7l5.9-25.7H46.7L46.7,6.2z"/>
                              <path d="M61.1,13.9c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1H51V13.1h6.7v4.1
                                 C58.6,15.8,59.7,14.7,61.1,13.9L61.1,13.9z"/>
                              <path d="M70.8,8.8c-0.8-0.7-1.2-1.7-1.2-2.8s0.4-2,1.2-2.8c0.8-0.7,1.8-1.1,2.9-1.1c1.2,0,2.2,0.4,2.9,1.1c0.8,0.7,1.2,1.7,1.2,2.8
                                 s-0.4,2-1.2,2.8c-0.8,0.7-1.8,1.1-2.9,1.1C72.5,10,71.5,9.6,70.8,8.8z M77,13.1v26.4h-6.7V13.1H77L77,13.1z"/>
                              <path d="M91.3,18.6v12.8c0,0.9,0.2,1.5,0.6,1.9c0.4,0.4,1.2,0.6,2.2,0.6h3.1v5.6H93c-5.6,0-8.4-2.7-8.4-8.2V18.6h-3.1v-5.5h3.1V6.6
                                 h6.7v6.5h5.9v5.5L91.3,18.6L91.3,18.6z"/>
                              <path d="M126.4,28.3h-19.3c0.2,1.9,0.8,3.4,2,4.5c1.2,1.1,2.6,1.6,4.3,1.6c2.5,0,4.2-1.1,5.3-3.2h7.2c-0.8,2.5-2.2,4.6-4.4,6.3
                                 c-2.2,1.6-4.8,2.5-8,2.5c-2.5,0-4.8-0.6-6.8-1.7c-2-1.1-3.6-2.7-4.7-4.8c-1.1-2.1-1.7-4.4-1.7-7.1s0.6-5.1,1.7-7.2
                                 c1.1-2.1,2.7-3.6,4.7-4.8c2-1.1,4.3-1.7,6.9-1.7c2.6,0,4.8,0.5,6.7,1.6c2,1.1,3.5,2.6,4.6,4.6c1.1,2,1.6,4.3,1.6,6.8
                                 C126.6,26.7,126.5,27.5,126.4,28.3L126.4,28.3z M119.7,23.8c0-1.7-0.7-3.1-1.9-4.1c-1.2-1-2.7-1.5-4.4-1.5c-1.7,0-3,0.5-4.2,1.5
                                 c-1.1,1-1.8,2.4-2.1,4.2L119.7,23.8L119.7,23.8z"/>
                              <path d="M141.5,13.9c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1h-6.7V13.1h6.7v4.1
                                 C139,15.8,140.1,14.7,141.5,13.9L141.5,13.9z"/>
                              <path d="M213.9,14c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1h-6.7V13.2h6.7v4.1
                                 C211.4,15.9,212.5,14.8,213.9,14L213.9,14z"/>
                              <path d="M223.1,19.2c1.1-2,2.7-3.6,4.6-4.8c2-1.1,4.2-1.7,6.8-1.7c3.3,0,6,0.8,8.1,2.5c2.1,1.6,3.6,3.9,4.3,6.9h-7.2
                                 c-0.4-1.1-1-2-1.9-2.7c-0.9-0.7-2-1-3.4-1c-1.9,0-3.4,0.7-4.5,2.1c-1.1,1.4-1.7,3.3-1.7,5.9c0,2.5,0.6,4.5,1.7,5.8
                                 c1.1,1.4,2.6,2.1,4.5,2.1c2.7,0,4.5-1.2,5.3-3.6h7.2c-0.7,2.9-2.2,5.1-4.3,6.8c-2.2,1.7-4.9,2.5-8.1,2.5c-2.5,0-4.8-0.6-6.8-1.7
                                 c-2-1.1-3.5-2.7-4.6-4.8c-1.1-2-1.7-4.4-1.7-7.2C221.4,23.7,222,21.3,223.1,19.2L223.1,19.2z"/>
                              <path class="st0" d="M187.7,20c-3.6-1.1-7.4-1.7-11.4-1.7c-4.9,0-9.6,0.9-13.9,2.6L174.4,0v9.4c-1,0.4-1.8,1.3-1.8,2.5
                                 c0,1.5,1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6c0-1.2-0.7-2.1-1.8-2.5V0.1L187.7,20L187.7,20z"/>
                              <path class="st1" d="M198.9,39.5c-6.6-11.2-18.8-18.7-32.8-18.7c-1.3,0-2.6,0.1-3.9,0.2c-0.1,0-0.2,0-0.3,0
                                 c-3.7,0.4-7.1,1.3-10.4,2.7c1.6,0,3.1,0.1,4.6,0.3c1.3,0.2,2.7,0.4,4,0.7l-8.6,14.8c6.3-5,14.2-8,22.8-8.2c0.3,0,0.6,0,0.9,0
                                 C184.2,31.3,192.4,34.4,198.9,39.5L198.9,39.5L198.9,39.5z"/>
                           </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md30 w700 f-md-24 f-22 text-center black-clr lh120">Agency – 100 Client License</div>
                     </div>
                     <div>
                        <ul class="f-18 f-md-20 w400 lh140 text-center vgreytick mb0">
                           <li>Directly Provide Top Notch Copywriting Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting is required</li>
                           <li>Whitelabel License</li>
                           <li>Serve up to 100 Clients with Agency License</li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="" editabletype="button" style="z-index: 10;">
                        <a href="https://www.jvzoo.com/b/106971/386766/2"><img src="https://i.jvzoo.com/106971/386766/2" alt="WriterArc Agency 100 Client License" border="0" class="img-fluid d-block mx-auto" /></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox3" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg3 text-center">
                        <div>
                           <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="max-height:40px;">
                              <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                              <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                              <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                              <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                              <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                              <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                              <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                              <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                              <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                              <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                           </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md30 w700 f-md-24 f-22 text-center white-clr lh120">Agency – Unlimited Client License</div>
                     </div>
                     <div>
                        <ul class="f-18 f-md-20 w400 lh140 text-center grey-tick-last mb0 white-clr">
                           <li>Directly Provide Top Notch Copywriting Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting is required</li>
                           <li>Whitelabel License</li>
                           <li>Serve Unlimited Clients with Agency License</li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast myfeatureslastborder f-md-25 f-16 w400 text-center lh140 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="" editabletype="button" style="z-index: 10;">
                        <a href="https://www.jvzoo.com/b/106971/386768/2"><img src="https://i.jvzoo.com/106971/386768/2" alt="WriterArc Agency Unlimited Client License" border="0" class="img-fluid d-block mx-auto" /></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                  <a href="https://www.writerarc.com/bizdrive" target="_blank" class="kapblue f-18 f-md-20 lh140 w500">
                  No thanks - I don't want to use the untapped POWER to setup my own pro agency without doing any extra work. I know that WriterArc Agency Upgrade can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase. 
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!--10. Table Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + days + '</span><br><span class="f-18 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + hours + '</span><br><span class="f-18 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + minutes + '</span><br><span class="f-18 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">' + seconds + '</span><br><span class="f-18 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
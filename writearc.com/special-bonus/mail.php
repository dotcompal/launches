<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="WriterArc Bonuses">
      <meta name="description" content="WriterArc Bonuses">
      <meta name="keywords" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Atul">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WriterArc Bonuses">
      <meta property="og:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WriterArc Bonuses">
      <meta property="twitter:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="twitter:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <title>WriterArc Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'September 16 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz6.com/c/10103/386602/';
         $_GET['name'] = 'Atul';      
         }
         ?>
            <!-- Header Section Start -->   
            <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 text-center">
                    <div class="preheadline f-20 f-md-24 w500 white-clr lh140">
                           Grab My 20 Exclusive Bonuses Before the Deal Ends… 
                    </div>
                  </div>
                  <div class="col-12 mt-md30 mt15 f-md-50 f-28 w700 text-center white-clr lh140">
                  Revealing: Breath-Taking A.I. Technology That <span class="gradient-orange">Creates Stunning Content for Any Local or Online Niche</span> At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                    <div class="f-18 f-md-24 w500 text-center lh140 white-clr">
                    Watch My Quick Review Video
                    </div>
                </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
                     <div class="col-12 responsive-video border-video">
                        <iframe src="https://writerarc.dotcompal.com/video/embed/ejgej12mng" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1 mt20 mt-md40">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                     <li> <span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                     <li> <span class="orange-clr1">Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Free Commercial License - </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->


<!-- Step Section Start -->
<div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w500 lh130 black-clr1">
                  The Writerarc’s A.I. Engine Will <span class="w700">Accomplish All 
                  The Marketing Content Needs In 3 Easy Steps</span> 
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones, and Languages for which you want to create content. The WriterArc Artificial intelligence is trained by highly professional copywriters to create top converting &amp; plagiarism-free content.                     
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -140px; top: 340px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2 
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter Keyword
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name, &amp; then choose purpose (sales copy, email, or ad etc) to create relative and effective content
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 320px; left: -240px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #3
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Publish &amp; Profit 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     WriterArc will automatically create professional, fresh and 100% plagiarism-free content of your need in just a few seconds.                  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12">
                  <div class="f-20 f-md-24 w700 lh140 text-center">
                     WriterArc is Built for Everyone Across All Levels
                  </div>
                  <div class="mt20 d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec-bg-purple">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"ATULWRITE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEMAX"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>

               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

     <!-- Features Section Start -->
     <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="list-block">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-50 f-28 w500 lh140 text-capitalize text-center white-clr list-title">
                              We Guarantee That This Will Be The <span class="w700"> <br class="d-none d-md-block">Last Content Writing Tool You'll Ever Need</span>
                           </div>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600"> A 10X Faster And Easier Way </span> To Generate Killer Content Effortlessly.</li>
                              <li>An A.I. Writing Assistant That Helps You <span class="w600">Create High-Quality Content In Just A Few Seconds.</span> </li> 
                              <li> <span class="w600">Powered By The State Of Art Language A.I.</span> To Generate Original, And Search Engine Optimized Content For Almost Any Vertical.</li> 
                              <li> <span class="w600">50+ Use Case Based Templates To</span>  Cover All Your Content Needs.</li> 
                              <li> <span class="w600">Choose From 30+ Languages</span> - Write In Your Own Or Other Languages For Your Clients.</li> 
                              <li>Write Every Content With The <span class="w600">Right Emotions Through 22+ Tones Of Voice.</span> </li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                             <li>Loaded With 36+ Predefined Local &amp; Online Business Niches So You <span class="w600">Don't Have To Hassle With Finding Keywords And The Right Descriptions.</span> </li> 
                             <li>Uses Standard Copywriting Features Like <span class="w600">AIDA And PAS To Provide Top Converting Output.</span> </li> 
                             <li> <span class="w600">100% Cloud-Based Software</span> So You Can Access It From Any In The World.</li> 
                             <li>Fast, Responsive, And 100% Mobile Friendly To <span class="w600">Work Seamlessly With Any Device.</span> </li> 
                             <li> <span class="w600">Download All Your Projects In PDF And Word Formats.</span> </li> 
                             <li> <span class="w600">100% Newbie Friendly,</span>  Need No Prior Skills, No Installation, And No Tech Hassles.</li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-12 ">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600">Best In Industry Customer Support With A 96% Customer Satisfaction Rate.</span> </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-md-28 f-22 w500 lh140 text-capitalize text-center black-clr col-12">
                  Let's agree on one thing… 
               </div>
               <div class="w700 f-md-38 f-28 lh140 text-capitalize text-center black-clr col-12">
                  Today's World Has Become Too Packed With Old, Repetitive <br class="d-none d-md-block"> &amp; Boring Content!
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr text-center mt10">
                 <i>So, having fresh, engaging &amp; better content than your opposition <br class="d-none d-md-block">
                  has become important for every business. Because…</i>  
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w700 lh140 blue-clr2">
                        Content is a Backbone for Every Form of Marketing…
                     </div>
                     <div class="f-18 f-md-20 w500 lh140 black-clr">
                        Whatever you do to promote your business, whether Social Media Posts, Facebook &amp; Google Ads, YouTube Marketing, Blogging, Email Marketing, or having Landing Page or Website. You need Unique, Engaging, <br class="d-none d-md-block"> and SEO-friendly content. 

                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <!-- <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-md-28 f-24 w600 black-clr lh140">
                     And according to Forbes, Here Are Top 10 Reasons Why Content Matters 
                  </div>
                  <ul class="agree-list f-md-18 f-16 w400 mb0 mt20 lh140 black-clr">
                     <li>Help in building customer's trust</li>
                     <li>Makes you look like an expert</li>
                     <li>You can build a library of content that helps in branding</li>
                     <li>Content creation is a key element of SEO</li>
                     <li>Customers like helpful data and are more likely to follow your brand</li>
                     <li>Content is an excellent and core part of your marketing campaign</li>
                     <li>Establishes your Brand </li>
                     <li>Content is a great way to spread your brand and increase market share</li>
                     <li>Helps in quality lead generation</li>
                     <li>Drives more and more web traffic</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-22 f-20 w500 lh140 text-center mt15 mt-md30">
                  And that's the reason why businesses capitalize on content that is <span class="w700">above 70% of the Total Web Traffic</span>
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->
<!-- Old & Boring Content Section Starts -->
<section class="old-content-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="old-content-box ">
                     <div class="f-24 f-md-38 w500 lh140">And You Would Agree That </div>
                     <div class="f-md-65 f-38 w800 lh120 mt15">OLD &amp; BORING CONTENT 
                        <br class="d-none d-md-block">IS NOT WORKING! </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-18 f-md-20 w500 white-clr lh140 mt50 mt-md60 text-center">
                     Those are not working on social media, not on YouTube, and not even worth publishing on your <br class="d-none d-md-block">
                     own website or blogs. And that's the reason why you don't get FREE traffic, lose<br class="d-none d-md-block">
                     sales &amp;revenue, and must think of PAID traffic methods, my friend.<br class="d-none d-md-block">
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="old-headline f-md-28 f-24 w600 lh140 white-clr mt-md30 mt20 ">
                     But before revealing our secret. Let me ask you a question!
                  </div>
               </div>
               <div class="col-12">
                  <div class="w700 f-md-38 f-24 lh140 yellow-clr mt-md30 mt20 text-center">
                     How many times have you clicked &amp; read these <br class="d-none d-md-block">
                     amazing headlines?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-8">
                  <img src="assets/images/seo.webp" alt="SEO" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 col-md-4">
                  <img src="assets/images/elon.webp" alt="Elon" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-20 f-md-24 w500 lh140 white-clr text-center">
                     We all have clicked &amp; read such trending posts while surfing online regularly. <br class="d-none d-md-block">
                     <u>People only want fresh &amp; unique content, news &amp; articles. This is working like a miracle.</u>
                  </div>
                  <div class="f-24 f-md-38 w700 lh140 yellow-clr text-center mt20 mt-md30">Fresh &amp; Unique Content is The WINNER In 2022 &amp; Beyond...</div>
               </div>
            </div>
         </div>
      </section>
 <!-- Old & Boring Content Section Ends -->


<!-- Business Section Starts -->
<section class="business-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-32 w700 lh140 black-clr text-center">
                     Now, it is safe to say that if you want to grow your business you
                     must create content better than your opposition.
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w800 lh140 black-clr">
                        Out of 550,000 New Business Every Month
                     </div>
                     <div class="f-24 f-md-32 w700 lh140 black-clr text-center mt10">
                        <span class="blue-clr2">90% of Businesses</span>  Embrace <span class="blue-clr2">Content Marketing</span>  <br class="d-none d-md-block">
                        for their Growth.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start">
                     However, even after so many efforts, and investing
                     time and money in copywriting, business owners do
                     not get the desired results
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/invest.webp" alt="Invest" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="billion-content">
                     <div class="f-24 f-md-36 w800 lh140 black-clr">
                        This Opens the Door to the $413 Billion Content <br class="d-none d-md-block">
                        Marketing Industry
                     </div>
                  </div>
                  <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md30">
                     and it's a huge opportunity for freelance copywriters and newbies <br class="d-none d-md-block">
                     who are trying to build a regular income source.
                  </div>
               </div>
            </div>
         </div>
      </section>
<!-- Business Section Ends -->

<div class="wearenot-alone-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto alone-bg ">
                  <div class="row align-items-center">
                     <div class="col-md-1 ">
                        <img src="assets/images/dollars-image.webp " class="img-fluid d-block dollars-sm-image ">
                     </div>
                     <div class="col-md-11 f-28 f-md-45 w700 lh140 white-clr text-center mt15 mt-md0 ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-45 w600 lh140 black-clr text-center mt20 mt-md80 ">Here’s a simple math</div>
               <div class="col-12 mt20 mt-md50 ">
                  <img src="assets/images/make-icon.webp " class="img-fluid d-none d-md-block mx-auto ">
                  <img src="assets/images/make-icon-xs.webp" alt="Make Icon" class="img-fluid d-block d-md-none mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="title-shape">And It’s Not A 1-Time Income</span>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt20 mt-md40 text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block">
                  services again and again forever.                  
               </div>
            </div>
         </div>
      </div>
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-34 f-24 w700 lh150 black-clr text-center">
                  Here’re some examples of freelance copywriters on Fiverr, Odesk, Freelancers, etc who are making 5-6 figures income by helping businesses with their content.
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7">
                  <img src="assets/images/meet-smith.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20">
                  <b>Meet Smith</b> has made $10,000 on Fiverr in just 2 months by providing 
                  copywriting services to local businesses
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7 order-md-2">
                  <img src="assets/images/boomer-dock.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
                  <b>Boomer Dock</b> has made almost $28k on Freelancer and has completed 
                  Over 18 successfully completed Freelancer projects in under 2 months.  
               </div>
            </div>
            <div class="row mt-md0 mt20 align-items-center">
               <div class="col-md-6">
                  <img src="assets/images/bright-brian.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
                  <b> Bright Brian</b> has earned almost $17,500 in just 47 Days of his start 
                  as a content writer on Upwork.  
               </div>
            </div>  
         </div>
      </div>



      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  And Here's What Some More Beta Users <br class="d-none d-md-block"> Says About WriterArc
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4 mb-md80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Thanks for the early access, WriterArc team. Now I am able to create unique content for my client’s social media &amp; google ads campaigns with the help of WriterArc. And I am amazed to see that it has increased ROI by more than 30% in just two days. I have created content for my real estate and restaurant client.
                        <br><br>
                        WriterArc is truly capable of creating any type of content for any business. <span class="w600">I highly recommend WriterArc to every newbie and growth marketer.</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/drake.webp" class="img-fluid d-block mx-auto" alt="Drake Lewis">
                        <div class="client-details">
                           Drake Lewis
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a Website Agency owner but one of the biggest challenges I was facing in my business is the need for a Content Writer. The majority of my clients were demanding website content along with website development and I was unable to fulfill this demand as I was not able to hire a full-time copywriter as it is too costly and not affordable for me.
                        <br><br>
                       <span class="w600">But, thanks to WriterArc, now I am able to create website content by myself by just entering the keyword.</span> I am experiencing 2X growth in my business by providing a package of website development plus content to my clients. 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/josh.webp" class="img-fluid d-block mx-auto" alt="Josh Paul">
                        <div class="client-details">
                           Josh Paul
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        WriterArc has turned out to be a miracle for my business growth. I am Antonio Martin, a local gift store owner and I usually do promotion of my business through social media ads. But, since the pandemic has happened, more and more businesses have come online which eventually has increased competition for me as well and I was observing a downfall in ROI and customers in my store because my social media ads were not working that well due to old repetitive content, and I am very short of time to hassle for writing new content. But thanks to the WriterArc team for providing me beta access. <span class="w600">Now I am able to generate fresh, and engaging content for my ads campaign, and my ROI is better than ever before.</span>   
                     </p>
                     <div class="client-info">
                        <img src="assets/images/antonio.webp" class="img-fluid d-block mx-auto" alt="Antonio Martin">
                        <div class="client-details">
                           Antonio Martin 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a full-time content writer. But nowadays I was facing challenges to get my client’s websites and google business listingsto rank on the most popular keywords, the reason is that the internet is overcrowded with old and repetitive content, and creating fresh &amp; unique content wasn’t easy anymore. <br><br> 
                        <span class="w600">But thanks to this life-changing A.I. technology called WriterArc,</span> it has eased my problem and it writes perfect and SEO-optimized content with just the help of a keyword. Now I have more satisfied clients and I can create more and more content in just a few seconds.  
                     </p>
                     <div class="client-info">
                        <img src="assets/images/mike.webp" class="img-fluid d-block mx-auto" alt="Mike East">
                        <div class="client-details">
                           Mike East
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="features"></div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="per-shape f-28 f-md-34 w700">
                     Introducing… 
                  </div>
              </div>
               <div class="col-12 mt-md30 mt20 mt-md40 text-center">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="max-height:100px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md50 f-24 f-md-30 text-center white-clr lh140 w500">
                  Breath-Taking A.I. Based Content Creator That Smartly Creates Any Type of  <br class="d-none d-md-block">
                  Marketing Content for Any Local or Online Niche with Just a Click of a Button 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-12">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center">
                  A Better and 10X Faster Way to Write Copies. <br>
                  So, stop wasting time and money on content and copywriting anymore. As, now you can ensure the growth of your <br class="d-none d-md-block"> business and embrace content marketing using the power of Artificial Intelligence. 
               </div>
            </div>
            <div class="prouldly-li-box">
               <div class="row mt20 mt-md30 g-0">
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-rb boxborder top-first ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Investing Time &amp; Money in learning <br class="d-none d-md-block">
                        copywriting.
                        </div>
                     </div>
                     <div class="boxbg boxborder-r boxborder">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Indulging yourself in Hard Work and getting <br class="d-none d-md-block"> Frustrated.  
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-b boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Wasting countless weeks in Researching &amp; <br class="d-none d-md-block"> Writing yourself. 
                        </div>
                     </div>
                     <div class="boxbg boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Spending thousands of dollars on Hiring <br class="d-none d-md-block"> Copywriting Experts.
                        </div>
                     </div>
                  </div>
               </div>   
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : WebPrimo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--WebPrimo Header Section Start -->
<div class="WebPrimo-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto" /></div>
                <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                    <div>
                        The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                    </div>
                    <div class="mt5">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-md-45 f-28 w600 text-center white-clr lh150">
						First Ever on JVZoo…  <br class="d-none d-md-block">
						<span class="w700 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites & Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
					Easily create & sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches & 100+ other niches...
                </div>
				  <div class="col-12 mt20 mt-md40">
				  <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 min-md-video-width-left">

							<div class="col-12 responsive-video">
                                <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                            </div> 

                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                            <div class="WebPrimo-key-features-bg">
							<ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
							<li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
							<li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
							<li>Create Elegant, Fast loading & SEO Friendly Website within 7 Minutes</li>
							<li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
							<li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
							<li>Agency License Included to Build an Incredible Income Helping Clients!</li>
							</ul>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="WebPrimo-list-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row webprimo-header-list-block">
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li><span class="w600">Create Ultra-Fast Loading & Beautiful Websites</li>
                                    <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w600">For Any Business Need.</span>
									</li>
                                    <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
									</li>
                                    <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
									<li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
									<li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
									<li><span class="w600">Analytics & Remarketing Ready</span> Websites</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                                    <li>Customize and Update Themes on Live Website Easily</li>
                                    <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                                    <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                                    <li><span class="w600">Accept Payments</span> For Your Services & Products With Seamless WooCommerce Integration</li>
									<li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>									
                                </ul>
                            </div>
                        </div>
						<div class="col-12  header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
									<li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--WebPrimo Header Section End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--WebPrimo ends-->
   <!-------Coursova Starts----------->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Coursova
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="coursova-bg">
                <div class="container">
                    <div class="row inner-content">
                        <div class="col-12">
                            <div class="col-12 text-center">
                                <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                            </div>
                         
                        </div>
                        <div class="col-12 text-center px-sm15">
                            <div class="mt20 mt-md50 mb20 mb-md50">
                                <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                                   Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                                </div>
                            </div>
                        </div>
           
                        <div class="col-12">
                        <div class="col-md-8 col-sm-3 ml-md70 mt20">
                                <div class="game-changer col-md-12">
                                    <div class="f-md-24 f-20 w600 text-center  black-clr lh120">Game-Changer</div>
                                </div>
                            </div>
                            <div class="col-12 heading-co-bg">
                                <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
								  <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                                </div>
                            </div>
                            <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
								
								Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>

								</div>
						</div>
							  </div>
                        <div class="row align-items-center">
                            <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                          
                               <div class="col-12 p0 mt-md15">
                                    <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                                <ul class="list-head list-head-co pl0 m0">
                                    <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                                    <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                                    <li>Easily Create Courses - Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                                    <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                                    <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                                    <li>Get Commercial License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

      <div class="gr-1">
         <div class="container-fluid p0">			
            <picture>
            <img src="assets/images/coursova-gr1.png" alt="Flowers" style="width:100%;">
            </picture>	
            <picture>
            <img src="assets/images/coursova-gr2.png" alt="Flowers" style="width:100%;">
            </picture>		
            <picture>
            <img src="assets/images/coursova-gr3.png" alt="Flowers" style="width:100%;">
            </picture>			
         </div>
		</div>
<!------Coursova Section ends------>

      <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Trendio
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Trendio Section------>

   <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE COMMERCIAL LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
 <!------Trendio Section Ends------>
   
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : Kyza
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="header-section-ky">
               <div class="container">
                  <div class="row">
                        <div class="col-md-3 col-md-12 mx-auto">
                           <img src="assets/images/kyza-logo.png" class="img-fluid d-block mx-auto">
                        </div>
                     <div class="col-12 text-center">
                     <div class="mt20 mt-md60 text-center px-sm15 px0">
                        <!--<div class="f-20 f-sm-24 w600 text-center post-heading lh140">
                          STOP Paying To Multiple Marketing Apps For Running Your Business!
                        </div>-->
						 <div class="f-20 f-sm-22 w600 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                           <img src="assets/images/stop.png" class="img-responsive mx-xs-center stop-img">
						   <div class="post-heading1">
						  <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
						   </div>
                        </div>
                     </div>
                     </div>
                     <div class="col-12 mt20 mt-md60 text-center">
                        <div class="f-md-41 f-28 w700 black-clr lh140 line-center">	
						Game Changer All-In-One Growth Suite... <br class="visible-lg">
						✔️Builds Beautiful Pages & Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms & Pop-Ups, & ✔️Lets You Send UNLIMITED Emails 					
                        </div>
                        <div class="mt-md25 mt20 f-20 f-md-26 w600 black-clr text-center lh140">
                        All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                        </div>
                        <div class="col-12 col-md-12  mt20 mt-md40">
							<div class="row d-flex flex-wrap align-items-center">
							   <div class="col-md-7 col-12 mt20 mt-md0">
									<div class="responsive-video">  
										<iframe class="embed-responsive-item"  src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
										box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
								  </div>
							   </div>
							   <div class="col-md-5 col-12 mt20 mt-md0">
									<div class="key-features-bg-ky">
									  <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
										<li>Create Unlimited Stunning Pages, Popups, And Forms </li>
										<li>Mobile, SEO, And Social Media Optimized Pages </li>
										<li>Send Unlimited Beautiful Emails For Tons Of Traffic & Sales </li>
										<li>Over 100 Done-For-You High Converting Templates </li>
										<li>2 Million+ Stock Images & Branding Graphics Kit </li>
										<li>Comes With Commercial License To Serve & Charge Your Clients </li>
										<li>100% Newbie Friendly & Step-By-Step Training Included  </li>
									  </ul>
								   </div>
							   </div>
						   </div>
                        </div>
						
						
						
						
                     </div>
                  </div>
               </div>
            </div>
            <!-- Header Section End -->
<img src="assets/images/kyza-gb1.png" class="img-fluid mx-auto d-block" style="width: 100%;">
<img src="assets/images/kyza-gb2.png" class="img-fluid mx-auto d-block" style="width: 100%;">
<img src="assets/images/kyza-gb3.png" class="img-fluid mx-auto d-block" style="width: 100%;">


<!-- Vocalic Section Start -->
<div class="exclusive-bonus">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="white-clr f-24 f-md-36 lh140 w600">
                            Exclusive Bonus #5 : Vocalic
                        </div>
                        <div class="f-18 f-md-20 w500 mt20 white-clr">
                            20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
               Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
                
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                    Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Commercial License Included <br> for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt20 mt-md50 white-clr">
                        Use Coupon Code <span class="w800 yellow-clr">"VOCALIC"</span> for an Extra <span class="w800 yellow-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Commercial License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>                        
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Commercial License Included for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt10 white-clr">
                        Use Coupon Code <span class="w800 yellow-clr">"VOCALIC"</span> for an Extra <span class="w800 yellow-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
            <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
            <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
            <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
<!-- Vocalic Section ENds -->




      

<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #6 : Viddeyo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>



   <div class="viddeyo-header-section">
      <div class="container">
         <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_2_);}
                           </style>
                           <g>
                              <g>
                                 <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                                    l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
                                 <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
                                 <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                                    c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
                                 <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                                    c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
                                 <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                                    V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
                                 <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                                    l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                                    l1.31-1.78H808.03z"></path>
                                 <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                                    C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                                    c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                              </g>
                           </g>
                           <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                              <stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
                              <stop offset="1" style="stop-color:#FC6DAB"></stop>
                           </linearGradient>
                           <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                              c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                              c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                              C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                              c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                              c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                              c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                              c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                              c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                              c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                              c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                              c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                              c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                              c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                              c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="viddeyo-preheadline f-18 f-md-20 w400 white-clr lh140">
                     <span class="w600 text-uppercase">Warning:</span> 1000s of Business Websites are Leaking Profit and Sales by Uploading Videos to YouTube
                  </div>
               </div>
               <div class="col-12 mt-md15 mt10 f-md-48 f-28 w400 text-center white-clr lh140">
                  Start Your Own <span class="w700 highlight-viddeyo">Video Hosting Agency with Unlimited Videos with Unlimited Bandwidth</span> at Lightning Fast Speed with No Monthly FEE Ever...
               </div>
               <div class="col-12 mt20 mt-md20 f-md-21 f-18 w400 white-clr text-center lh170">
                  Yes! Total control over your videos with zero delay, no ads and no traffic leak. Commercial License Included
               </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto" alt="proudly"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://launch.oppyo.com/video/embed/xo6b4lwdid" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                    <div class="calendar-wrap">
                        <div class="calendar-wrap-inline">
                            <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w500 black-clr">
                                <li>100% Control on Your Video Traffic &amp; Channels</li>
                                <li>Close More Prospects for Your Agency Services</li>
                                <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                                <li>Tap Into $398 Billion E-Learning Industry</li>
                                <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                                <li>Boost Your Clients Sales-Commercial License Included</li>
                            </ul>
                        </div>
                    </div>
                </div>
         </div>
      </div>
    </div></div>
    <div class="viddeyo-second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row viddeyo-header-list-block">
                        <div class="col-12 col-md-6">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
                                <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
                                <li><span class="w600">Get Maximum Visitors Engagement</span>-No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
                                <li>Create 100% Mobile &amp; SEO Optimized <span class="w600">Video Channels &amp; Playlists</span></li>
                                <li><span class="w600">Stunning Promo &amp; Social Ads Templates</span> for Monetization &amp; Social Traffic</li>
                                <li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
                                <li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing &amp; Video Agency Industry. </li>
                                <li>Manage All the Videos, Courses, &amp; Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
                                <li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
                                <li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices &amp; Page Builders</span> </li>
                                <li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
                                <li><span class="w600">Completely Cloud-Based</span> &amp; Step-By-Step Video Training Included </li>
                                <li>PLUS, YOU'LL RECEIVE - <span class="w600">A LIMITED COMMERCIAL LICENSE IF YOU BUY TODAY</span> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>
    <!-- Header Viddeyo End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--Viddeyo ends-->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase WriterArc, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

  <!-- Bonus #1 Section Start -->
  <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>The essence of viral marketing is content. In other words, you have to get viral content, so you can pull a lot of traffic from many different places. </li>
                           <li> Most people who try viral marketing are clueless about this.</li>
                           <li> They don't even know that this niche network already exists.</li>
                           <li>With this guide you are going to understand the essence of viral marketing and learn to identify the types of content that go viral every single day.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Finding a Profitable Niche Market
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> Finding a niche that pays and then creating content, products, and services for that niche, is the only way you will be able to build a solid foundation for a successful online business. </li>
                           <li>Finding a profitable niche is also the best way to achieve the level of success and the kind of profits that most online business owners need to make their daily efforts worthwhile.</li>
                           <li>While there are a lot of popular niches that you can choose from, not all of them will be profitable.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           You can have the best product or the best service in the world but if you have no traffic - it's all completely worthless.<br><br>
                           <b>Topics covered:</b><br><br>
                           <li><b>Email Traffic -</b> The money is in the list! This video will show you how you can generate huge amounts of email traffic that are ready to buy your product.</li>
                           <li><b>Facebook Traffic -</b> In this video you will learn tips and tricks to help you avoid the common pitfalls people make.</li>
                           <li><b>Forum Traffic -</b> You will learn how to hunt down these forums for your niche and get laser targeted, and passionate traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Email marketing for business isn't a new concept, and it has been proven to be one of the best marketing tactics for return on investment. </li>
                           <li>With more than 205 billion emails being sent and received every day if your business isn't taking advantage of this powerful and massive marketing channel, then you are missing out on a highly effective way to reach your target audience.</li>
                           <li>Creating a successful email marketing campaign isn't difficult, but it does require you to do more than just send out an occasional newsletter. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Youtube promotion
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>More than 1.9 billion users visit YouTube on a monthly basis and watch billions of hours of videos every day, making YouTube the second most visited website in the world.
                           </li>
                           <li>Because of that YouTube has become a serious marketing platform where businesses are given the opportunity to promote content in a truly visual and highly engaging way. </li>
                           <li>It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"ATULWRITE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEMAX"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Network marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Thanks to advances in technology and new ways that employees interact and meet with one another, virtual networking now makes it possible to reach others and make connections that were not available in the past.</li>
                           <li>Through social media, Zoom, and other tools, you are able to get ahead and really meet people from all around the world. </li>
                           <li>This guide is going to explore some of the different ways that you can reach out to others and really expand your network, without having to go into the office or worry about distance and other barriers. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Concepts
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>With this video course you will discover how to tap into affiliate marketing and make passive commissions by recommending products you love.  </li>
                           <li>You will learn to understand the most powerful concepts of affiliate marketing and develop a clear, long-term strategy to earn passive income as an affiliate. </li>
                           <li>Also, you will discover exactly how affiliate marketing works, and how to start your own affiliate website or blog to make passive income online. This course needs no prior experience in affiliate marketing, online marketing or website building. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Niche Jumpstart
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>How To Create A Profitable Niche Business From Scratch Within Hours From Now... And Make It Pull In Thousands Per Month Without Fail! </li>
                           <li>If you are a blogger, affiliate marketer or an ordinary person who wants to make money online, tapping into the niche marketing industry is a huge advantage for you to get started today.</li>
                           <li>Compared to other make money online industry, niche marketing targets a specific market where you can determine if that certain market is highly profitable or not. </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Networking vedio course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.</b> </li>
                           <li>Virtual networking allows this to happen, whether you are in the office or work from anywhere. </li>
                           <li>Through virtual networking, you will be able to take advantage of online platforms, social media, and even online webinars and conferences in order to form connections with others, even when you are not able to meet them in-person.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        20 best Images for blog
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Get Your Hands On 20 Hi Resolution Images That You Can Use On Your Websites, Blogs, Desktop Or Anywhere Else You Can Think Of! </li>
                           <li>Each image in this package is 1600 x1000 pixels, some are even larger. This give you more freedom and control over how you use them. They all come in a ready to go jpeg format. </li>
                           <li>Remember, the package of 20 high resolutions graphics comes with Resell Rights! Use the images yourself and, or resell them for 100% Profit! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"ATULWRITE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEMAX"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Blogging templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Make Thousands of Dollars Online Offering Mobile Friendly Websites to Your Local Business Clients!</b></li>
                           <li>Since the launch of the latest Google Algorithm update called Mobilegeddon, local business owners, internet marketers, webmasters and SEO's are now concern about the looks of their website in various types of mobile devices as this will also impact the website rankings in mobile search.</li>
                           <li>Though this will not affect the website's ranking if you are browsing the website in desktop computers, this is a huge loss if you will not optimize your website for mobile devices like tablets and smartphones.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Popup 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</b></li>
                           <li>You can trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your product, article or even your profile.  Additionally, you can add a secondary content popup that can contain optin forms, buy buttons, or social sharing icons.This will help boost your traffic, sales and email lists.  This is an “in-content” trigger tool so you have the ability to choose the timing when the popup shows up. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Facebook adsense
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>While the FB Ad Secrets was made for beginners just starting in Facebook advertising, these videos answer the coming challenges you will face as you scale to spending hundreds or thousands of dollars per day while remaining profitable.</b></li>
                        <li>For beginners, you will usually start with a low budget to run your Facebook ads. But eventually when the time comes for you to scale your offer with a bigger budget, you will come across problems such as your ad account being disabled, profit margin becomes thinner and things like when and how to split test your ads.</li>
                        <li>With this upgrade, it will help you prepare to scale your business to possibly 6-7 figures a year! You need to think long term and if you want your business to have massive returns, you must be willing to invest and work towards it as well.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Graphic Designs
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>When you say yes to Garage Sale Graphics, you not only will have a better looking website, but you will notice an increase in the amount of sales you get without an increase in traffic. </li>
                           <li>Simply put, Garage Sale Graphics will help you convert more of your visitors into customers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social marketing 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>A common problem that many of us have is expecting our content to go viral and for our follower count to grow immediately. </li>
                           <li>Many people spend an average of 3 hours on social media per day, and this number increases depending on the demographic. </li>
                           <li>In reality, posts rarely go viral without many hours spent researching, strategizing and planning the most exciting and effective content to share with their engaged followers.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"ATULWRITE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEMAX"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social sites webninar
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this powerful plugin you can create amazing webinar landing pages inside of Facebook!</b></li>
                           <li>There is no limit on how many Facebook webinar pages you can create, create one for all your clients on ONE plugin install.</li>
                           <li>Collect leads inside of WP and export them to CSV and have them automatically opted in to your favourite AR service.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Marketing Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Facebook Messenger has revolutionized how marketers can access and grow their audience on Facebook and websites that use many of Facebook's extensions.</b></li>
                           <li>Facebook is not just one platform, it is actually a set of related properties. It also shares key parts of its functionality with third-party websites.</li>
                           <li>In this video course you will learn how Facebook Messenger works, how you can create bots that would work with Facebook Messenger so you can grow your audience, and most importantly, drive qualified traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Speed up your content
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn to build faster websites from now on. </li>
                        <li>Not all web hosts are created equal - make sure you sign up for a fast web host that will help you reach your business goals. </li>
                        <li>Know how to use and analyze the results of the best website speed testing tools out there. </li>
                        <li>Never underestimate the power of caching ever again - it will help your site load much faster than ever before. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Seo titles
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</b> Watch as your social network shares increase, your google PageRank and more.</li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Boom software 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</b></li>
                           <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li>
                           <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 orange-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 orange-clr">"ATULWRITE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab WriterArc + My 20 Exclusive Bonuses
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEMAX"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      
      <div class="footer-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
               <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
               <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
               <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
               <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
               <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
               <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
               <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
               <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
               <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
               <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
            </svg>
            <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
         </div>
         <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
            <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
            <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
               <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>

      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
<!DOCTYPE html>
<html>

   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WriterArc Special Bonuses">
      <meta name="description" content="WriterArc Special Bonuses">
      <meta name="keywords" content="WriterArc Special Bonuses">
      <meta property="og:image" content="https://www.writearc.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WriterArc Special Bonuses">
      <meta property="og:description" content="WriterArc Special Bonuses">
      <meta property="og:image" content="https://www.writearc.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WriterArc Special Bonuses">
      <meta property="twitter:description" content="WriterArc Special Bonuses">
      <meta property="twitter:image" content="https://www.writearc.com/special-bonus/thumbnail.png">	
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>

   </head>

   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 247 41" style="enable-background:new 0 0 247 41; max-height: 40px;" xml:space="preserve">
                  <style type="text/css">
                     .st0{fill:#5055BE;}
                     .st1{fill:#292F60;}
                  </style>
                  <path d="M46.7,6.2l-9.3,33.2h-7.9l-6.2-23.7l-6.5,23.7l-7.8,0L0,6.2h7.2L13,32.1l6.8-25.8h7.4l6.4,25.7l5.9-25.7H46.7L46.7,6.2z"></path>
                  <path d="M61.1,13.9c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1H51V13.1h6.7v4.1 C58.6,15.8,59.7,14.7,61.1,13.9L61.1,13.9z"></path>
                  <path d="M70.8,8.8c-0.8-0.7-1.2-1.7-1.2-2.8s0.4-2,1.2-2.8c0.8-0.7,1.8-1.1,2.9-1.1c1.2,0,2.2,0.4,2.9,1.1c0.8,0.7,1.2,1.7,1.2,2.8s-0.4,2-1.2,2.8c-0.8,0.7-1.8,1.1-2.9,1.1C72.5,10,71.5,9.6,70.8,8.8z M77,13.1v26.4h-6.7V13.1H77L77,13.1z"></path>
                  <path d="M91.3,18.6v12.8c0,0.9,0.2,1.5,0.6,1.9c0.4,0.4,1.2,0.6,2.2,0.6h3.1v5.6H93c-5.6,0-8.4-2.7-8.4-8.2V18.6h-3.1v-5.5h3.1V6.6h6.7v6.5h5.9v5.5L91.3,18.6L91.3,18.6z"></path>
                  <path d="M126.4,28.3h-19.3c0.2,1.9,0.8,3.4,2,4.5c1.2,1.1,2.6,1.6,4.3,1.6c2.5,0,4.2-1.1,5.3-3.2h7.2c-0.8,2.5-2.2,4.6-4.4,6.3c-2.2,1.6-4.8,2.5-8,2.5c-2.5,0-4.8-0.6-6.8-1.7c-2-1.1-3.6-2.7-4.7-4.8c-1.1-2.1-1.7-4.4-1.7-7.1s0.6-5.1,1.7-7.2c1.1-2.1,2.7-3.6,4.7-4.8c2-1.1,4.3-1.7,6.9-1.7c2.6,0,4.8,0.5,6.7,1.6c2,1.1,3.5,2.6,4.6,4.6c1.1,2,1.6,4.3,1.6,6.8C126.6,26.7,126.5,27.5,126.4,28.3L126.4,28.3z M119.7,23.8c0-1.7-0.7-3.1-1.9-4.1c-1.2-1-2.7-1.5-4.4-1.5c-1.7,0-3,0.5-4.2,1.5c-1.1,1-1.8,2.4-2.1,4.2L119.7,23.8L119.7,23.8z"></path>
                  <path d="M141.5,13.9c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1h-6.7V13.1h6.7v4.1C139,15.8,140.1,14.7,141.5,13.9L141.5,13.9z"></path>
                  <path d="M213.9,14c1.4-0.8,3-1.2,4.7-1.2v7h-1.8c-2.1,0-3.7,0.5-4.7,1.5c-1.1,1-1.6,2.7-1.6,5.1v13.1h-6.7V13.2h6.7v4.1C211.4,15.9,212.5,14.8,213.9,14L213.9,14z"></path>
                  <path d="M223.1,19.2c1.1-2,2.7-3.6,4.6-4.8c2-1.1,4.2-1.7,6.8-1.7c3.3,0,6,0.8,8.1,2.5c2.1,1.6,3.6,3.9,4.3,6.9h-7.2c-0.4-1.1-1-2-1.9-2.7c-0.9-0.7-2-1-3.4-1c-1.9,0-3.4,0.7-4.5,2.1c-1.1,1.4-1.7,3.3-1.7,5.9c0,2.5,0.6,4.5,1.7,5.8c1.1,1.4,2.6,2.1,4.5,2.1c2.7,0,4.5-1.2,5.3-3.6h7.2c-0.7,2.9-2.2,5.1-4.3,6.8c-2.2,1.7-4.9,2.5-8.1,2.5c-2.5,0-4.8-0.6-6.8-1.7c-2-1.1-3.5-2.7-4.6-4.8c-1.1-2-1.7-4.4-1.7-7.2C221.4,23.7,222,21.3,223.1,19.2L223.1,19.2z"></path>
                  <path class="st0" d="M187.7,20c-3.6-1.1-7.4-1.7-11.4-1.7c-4.9,0-9.6,0.9-13.9,2.6L174.4,0v9.4c-1,0.4-1.8,1.3-1.8,2.5c0,1.5,1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6c0-1.2-0.7-2.1-1.8-2.5V0.1L187.7,20L187.7,20z"></path>
                  <path class="st1" d="M198.9,39.5c-6.6-11.2-18.8-18.7-32.8-18.7c-1.3,0-2.6,0.1-3.9,0.2c-0.1,0-0.2,0-0.3,0c-3.7,0.4-7.1,1.3-10.4,2.7c1.6,0,3.1,0.1,4.6,0.3c1.3,0.2,2.7,0.4,4,0.7l-8.6,14.8c6.3-5,14.2-8,22.8-8.2c0.3,0,0.6,0,0.9,0C184.2,31.3,192.4,34.4,198.9,39.5L198.9,39.5L198.9,39.5z"></path>
                  </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>

      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
               <?php
                  if(isset($_POST['submit'])) 
                  {
                     $name=$_REQUEST['name'];
                     $afflink=$_REQUEST['afflink'];
                     //$picname=$_REQUEST['pic'];
                     /*$tmpname=$_FILES['pic']['tmp_name'];
                     $type=$_FILES['pic']['type'];
                     $size=$_FILES['pic']['size']/1024;
                     $rand=rand(1111111,9999999999);*/
                     if($afflink=="")
                     {
                        echo 'Please fill the details.';
                     }
                     else
                     {
                        /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                        {
                           echo "Please upload image file (size must be less than 1 MB)";	
                        }
                        else
                        {*/
                           //$filename=$rand."-".$picname;
                           //move_uploaded_file($tmpname,"images/".$filename);
                           $url="https://writerarc.com/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                           echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                           //header("Location:$url");
                        //}	
                     }
                  }
               ?>
               <form class="row" action="" method="post" enctype="multipart/form-data">
                  <div class="col-12 form_area ">
                     <div class="col-12 clear">
                        <input type="text" name="name" placeholder="Your Name..." required>
                     </div>

                     <div class="col-12 mt20 clear">
                        <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                     </div>

                     <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                        <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                  <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                  <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                  <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                  <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                  <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                  <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                  <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                  <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                  <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                  <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
               </svg>
            <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">
               Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
               <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section End -->
</body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="WriterArc Bonuses">
      <meta name="description" content="WriterArc Bonuses">
      <meta name="keywords" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WriterArc Bonuses">
      <meta property="og:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WriterArc Bonuses">
      <meta property="twitter:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="twitter:image" content="https://www.writerarc.com/special-bonus/thumbnail.png">
      <title>WriterArc Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'September 16 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/47069/386602/';
         $_GET['name'] = 'Ayush';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 text-center">
                    <div class="preheadline f-20 f-md-24 w500 white-clr lh140">
                           Grab My 20 Exclusive Bonuses Before the Deal Ends…
                    </div>
                  </div>
                  <div class="col-12 mt-md30 mt15 f-md-50 f-28 w700 text-center white-clr lh140">
                      Revealing: Breath-Taking A.I. Technology That <span class="gradient-orange">Creates Stunning Content for Any Local or Online Niche</span> At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                    <div class="f-18 f-md-24 w500 text-center lh140 white-clr">
                    Watch My Quick Review Video
                    </div>
                </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
                     <div class="col-12 responsive-video border-video">
                        <iframe src="https://writerarc.dotcompal.com/video/embed/ejgej12mng" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1 mt20 mt-md40">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                     <li> <span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                     <li> <span class="orange-clr1">Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh140 w500 white-clr">
                  <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section Start -->
      <div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w500 lh130 black-clr1">
                  The Writerarc’s A.I. Engine Will <span class="w700">Accomplish All 
                  The Marketing Content Needs In 3 Easy Steps</span> 
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones, and Languages for which you want to create content. The WriterArc Artificial intelligence is trained by highly professional copywriters to create top converting &amp; plagiarism-free content.                     
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -140px; top: 340px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2 
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter Keyword
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name, &amp; then choose purpose (sales copy, email, or ad etc) to create relative and effective content
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 320px; left: -240px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #3
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Publish &amp; Profit 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     WriterArc will automatically create professional, fresh and 100% plagiarism-free content of your need in just a few seconds.                  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12">
                  <div class="f-20 f-md-24 w700 lh140 text-center">
                     WriterArc is Built for Everyone Across All Levels
                  </div>
                  <div class="mt20 d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec-bg-purple">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AYUSHLOVE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEME"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz1.com/c/47069/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

     <!-- Features Section Start -->
     <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="list-block">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-50 f-28 w500 lh140 text-capitalize text-center white-clr list-title">
                              We Guarantee That This Will Be The <span class="w700"> <br class="d-none d-md-block">Last Content Writing Tool You'll Ever Need</span>
                           </div>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600"> A 10X Faster And Easier Way </span> To Generate Killer Content Effortlessly.</li>
                              <li>An A.I. Writing Assistant That Helps You <span class="w600">Create High-Quality Content In Just A Few Seconds.</span> </li> 
                              <li> <span class="w600">Powered By The State Of Art Language A.I.</span> To Generate Original, And Search Engine Optimized Content For Almost Any Vertical.</li> 
                              <li> <span class="w600">50+ Use Case Based Templates To</span>  Cover All Your Content Needs.</li> 
                              <li> <span class="w600">Choose From 30+ Languages</span> - Write In Your Own Or Other Languages For Your Clients.</li> 
                              <li>Write Every Content With The <span class="w600">Right Emotions Through 22+ Tones Of Voice.</span> </li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                             <li>Loaded With 36+ Predefined Local &amp; Online Business Niches So You <span class="w600">Don't Have To Hassle With Finding Keywords And The Right Descriptions.</span> </li> 
                             <li>Uses Standard Copywriting Features Like <span class="w600">AIDA And PAS To Provide Top Converting Output.</span> </li> 
                             <li> <span class="w600">100% Cloud-Based Software</span> So You Can Access It From Any In The World.</li> 
                             <li>Fast, Responsive, And 100% Mobile Friendly To <span class="w600">Work Seamlessly With Any Device.</span> </li> 
                             <li> <span class="w600">Download All Your Projects In PDF And Word Formats.</span> </li> 
                             <li> <span class="w600">100% Newbie Friendly,</span>  Need No Prior Skills, No Installation, And No Tech Hassles.</li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-12 ">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600">Best In Industry Customer Support With A 96% Customer Satisfaction Rate.</span> </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-md-28 f-22 w500 lh140 text-capitalize text-center black-clr col-12">
                  Let's agree on one thing… 
               </div>
               <div class="w700 f-md-38 f-28 lh140 text-capitalize text-center black-clr col-12">
                  Today's World Has Become Too Packed With Old, Repetitive <br class="d-none d-md-block"> &amp; Boring Content!
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr text-center mt10">
                 <i>So, having fresh, engaging &amp; better content than your opposition <br class="d-none d-md-block">
                  has become important for every business. Because…</i>  
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w700 lh140 blue-clr2">
                        Content is a Backbone for Every Form of Marketing…
                     </div>
                     <div class="f-18 f-md-20 w500 lh140 black-clr">
                        Whatever you do to promote your business, whether Social Media Posts, Facebook &amp; Google Ads, YouTube Marketing, Blogging, Email Marketing, or having Landing Page or Website. You need Unique, Engaging, <br class="d-none d-md-block"> and SEO-friendly content. 

                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <!-- <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-md-28 f-24 w600 black-clr lh140">
                     And according to Forbes, Here Are Top 10 Reasons Why Content Matters 
                  </div>
                  <ul class="agree-list f-md-18 f-16 w400 mb0 mt20 lh140 black-clr">
                     <li>Help in building customer's trust</li>
                     <li>Makes you look like an expert</li>
                     <li>You can build a library of content that helps in branding</li>
                     <li>Content creation is a key element of SEO</li>
                     <li>Customers like helpful data and are more likely to follow your brand</li>
                     <li>Content is an excellent and core part of your marketing campaign</li>
                     <li>Establishes your Brand </li>
                     <li>Content is a great way to spread your brand and increase market share</li>
                     <li>Helps in quality lead generation</li>
                     <li>Drives more and more web traffic</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-22 f-20 w500 lh140 text-center mt15 mt-md30">
                  And that's the reason why businesses capitalize on content that is <span class="w700">above 70% of the Total Web Traffic</span>
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->
<!-- Old & Boring Content Section Starts -->
<section class="old-content-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="old-content-box ">
                     <div class="f-24 f-md-38 w500 lh140">And You Would Agree That </div>
                     <div class="f-md-65 f-38 w800 lh120 mt15">OLD &amp; BORING CONTENT 
                        <br class="d-none d-md-block">IS NOT WORKING! </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-18 f-md-20 w500 white-clr lh140 mt50 mt-md60 text-center">
                     Those are not working on social media, not on YouTube, and not even worth publishing on your <br class="d-none d-md-block">
                     own website or blogs. And that's the reason why you don't get FREE traffic, lose<br class="d-none d-md-block">
                     sales &amp;revenue, and must think of PAID traffic methods, my friend.<br class="d-none d-md-block">
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="old-headline f-md-28 f-24 w600 lh140 white-clr mt-md30 mt20 ">
                     But before revealing our secret. Let me ask you a question!
                  </div>
               </div>
               <div class="col-12">
                  <div class="w700 f-md-38 f-24 lh140 yellow-clr mt-md30 mt20 text-center">
                     How many times have you clicked &amp; read these <br class="d-none d-md-block">
                     amazing headlines?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-8">
                  <img src="assets/images/seo.webp" alt="SEO" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 col-md-4">
                  <img src="assets/images/elon.webp" alt="Elon" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-20 f-md-24 w500 lh140 white-clr text-center">
                     We all have clicked &amp; read such trending posts while surfing online regularly. <br class="d-none d-md-block">
                     <u>People only want fresh &amp; unique content, news &amp; articles. This is working like a miracle.</u>
                  </div>
                  <div class="f-24 f-md-38 w700 lh140 yellow-clr text-center mt20 mt-md30">Fresh &amp; Unique Content is The WINNER In 2022 &amp; Beyond...</div>
               </div>
            </div>
         </div>
      </section>
 <!-- Old & Boring Content Section Ends -->


<!-- Business Section Starts -->
<section class="business-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-32 w700 lh140 black-clr text-center">
                     Now, it is safe to say that if you want to grow your business you
                     must create content better than your opposition.
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w800 lh140 black-clr">
                        Out of 550,000 New Business Every Month
                     </div>
                     <div class="f-24 f-md-32 w700 lh140 black-clr text-center mt10">
                        <span class="blue-clr2">90% of Businesses</span>  Embrace <span class="blue-clr2">Content Marketing</span>  <br class="d-none d-md-block">
                        for their Growth.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start">
                     However, even after so many efforts, and investing
                     time and money in copywriting, business owners do
                     not get the desired results
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/invest.webp" alt="Invest" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="billion-content">
                     <div class="f-24 f-md-36 w800 lh140 black-clr">
                        This Opens the Door to the $413 Billion Content <br class="d-none d-md-block">
                        Marketing Industry
                     </div>
                  </div>
                  <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md30">
                     and it's a huge opportunity for freelance copywriters and newbies <br class="d-none d-md-block">
                     who are trying to build a regular income source.
                  </div>
               </div>
            </div>
         </div>
      </section>
<!-- Business Section Ends -->

<div class="wearenot-alone-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto alone-bg ">
                  <div class="row align-items-center">
                     <div class="col-md-1 ">
                        <img src="assets/images/dollars-image.webp " class="img-fluid d-block dollars-sm-image ">
                     </div>
                     <div class="col-md-11 f-28 f-md-45 w700 lh140 white-clr text-center mt15 mt-md0 ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-45 w600 lh140 black-clr text-center mt20 mt-md80 ">Here’s a simple math</div>
               <div class="col-12 mt20 mt-md50 ">
                  <img src="assets/images/make-icon.webp " class="img-fluid d-none d-md-block mx-auto ">
                  <img src="assets/images/make-icon-xs.webp" alt="Make Icon" class="img-fluid d-block d-md-none mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="title-shape">And It’s Not A 1-Time Income</span>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt20 mt-md40 text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block">
                  services again and again forever.                  
               </div>
            </div>
         </div>
      </div>
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-34 f-24 w700 lh150 black-clr text-center">
                  Here’re some examples of freelance copywriters on Fiverr, Odesk, Freelancers, etc who are making 5-6 figures income by helping businesses with their content.
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7">
                  <img src="assets/images/meet-smith.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20">
                  <b>Meet Smith</b> has made $10,000 on Fiverr in just 2 months by providing 
                  copywriting services to local businesses
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7 order-md-2">
                  <img src="assets/images/boomer-dock.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
                  <b>Boomer Dock</b> has made almost $28k on Freelancer and has completed 
                  Over 18 successfully completed Freelancer projects in under 2 months.  
               </div>
            </div>
            <div class="row mt-md0 mt20 align-items-center">
               <div class="col-md-6">
                  <img src="assets/images/bright-brian.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
                  <b> Bright Brian</b> has earned almost $17,500 in just 47 Days of his start 
                  as a content writer on Upwork.  
               </div>
            </div>  
         </div>
      </div>



      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  And Here's What Some More Beta Users <br class="d-none d-md-block"> Says About WriterArc
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4 mb-md80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Thanks for the early access, WriterArc team. Now I am able to create unique content for my client’s social media &amp; google ads campaigns with the help of WriterArc. And I am amazed to see that it has increased ROI by more than 30% in just two days. I have created content for my real estate and restaurant client.
                        <br><br>
                        WriterArc is truly capable of creating any type of content for any business. <span class="w600">I highly recommend WriterArc to every newbie and growth marketer.</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/drake.webp" class="img-fluid d-block mx-auto" alt="Drake Lewis">
                        <div class="client-details">
                           Drake Lewis
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a Website Agency owner but one of the biggest challenges I was facing in my business is the need for a Content Writer. The majority of my clients were demanding website content along with website development and I was unable to fulfill this demand as I was not able to hire a full-time copywriter as it is too costly and not affordable for me.
                        <br><br>
                       <span class="w600">But, thanks to WriterArc, now I am able to create website content by myself by just entering the keyword.</span> I am experiencing 2X growth in my business by providing a package of website development plus content to my clients. 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/josh.webp" class="img-fluid d-block mx-auto" alt="Josh Paul">
                        <div class="client-details">
                           Josh Paul
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        WriterArc has turned out to be a miracle for my business growth. I am Antonio Martin, a local gift store owner and I usually do promotion of my business through social media ads. But, since the pandemic has happened, more and more businesses have come online which eventually has increased competition for me as well and I was observing a downfall in ROI and customers in my store because my social media ads were not working that well due to old repetitive content, and I am very short of time to hassle for writing new content. But thanks to the WriterArc team for providing me beta access. <span class="w600">Now I am able to generate fresh, and engaging content for my ads campaign, and my ROI is better than ever before.</span>   
                     </p>
                     <div class="client-info">
                        <img src="assets/images/antonio.webp" class="img-fluid d-block mx-auto" alt="Antonio Martin">
                        <div class="client-details">
                           Antonio Martin 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a full-time content writer. But nowadays I was facing challenges to get my client’s websites and google business listingsto rank on the most popular keywords, the reason is that the internet is overcrowded with old and repetitive content, and creating fresh &amp; unique content wasn’t easy anymore. <br><br> 
                        <span class="w600">But thanks to this life-changing A.I. technology called WriterArc,</span> it has eased my problem and it writes perfect and SEO-optimized content with just the help of a keyword. Now I have more satisfied clients and I can create more and more content in just a few seconds.  
                     </p>
                     <div class="client-info">
                        <img src="assets/images/mike.webp" class="img-fluid d-block mx-auto" alt="Mike East">
                        <div class="client-details">
                           Mike East
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="features"></div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="per-shape f-28 f-md-34 w700">
                     Introducing… 
                  </div>
              </div>
               <div class="col-12 mt-md30 mt20 mt-md40 text-center">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="max-height:100px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md50 f-24 f-md-30 text-center white-clr lh140 w500">
                  Breath-Taking A.I. Based Content Creator That Smartly Creates Any Type of  <br class="d-none d-md-block">
                  Marketing Content for Any Local or Online Niche with Just a Click of a Button 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-12">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center">
                  A Better and 10X Faster Way to Write Copies. <br>
                  So, stop wasting time and money on content and copywriting anymore. As, now you can ensure the growth of your <br class="d-none d-md-block"> business and embrace content marketing using the power of Artificial Intelligence. 
               </div>
            </div>
            <div class="prouldly-li-box">
               <div class="row mt20 mt-md30 g-0">
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-rb boxborder top-first ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Investing Time &amp; Money in learning <br class="d-none d-md-block">
                        copywriting.
                        </div>
                     </div>
                     <div class="boxbg boxborder-r boxborder">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Indulging yourself in Hard Work and getting <br class="d-none d-md-block"> Frustrated.  
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-b boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Wasting countless weeks in Researching &amp; <br class="d-none d-md-block"> Writing yourself. 
                        </div>
                     </div>
                     <div class="boxbg boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Spending thousands of dollars on Hiring <br class="d-none d-md-block"> Copywriting Experts.
                        </div>
                     </div>
                  </div>
               </div>   
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : Acadmeyio
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->
<div class="header-section-acadmeyio">
         <div class="container">
            <div class="row">
               <div class="col-md-12  text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                     </style>
                     <g>
                        <g>
                           <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"></path>
                           <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                              c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                              c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                              c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"></path>
                           <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                              M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"></path>
                           <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                              M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"></path>
                           <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                              c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                              c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                              c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                              c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"></path>
                           <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                              c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                              c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                              h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                              c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"></path>
                           <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                              c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                              C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"></path>
                           <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"></path>
                           <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                              c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                              c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                              C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                              c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                              c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"></path>
                        </g>
                        <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                           c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                           c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                           c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                           c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                           L1194.92,0z"></path>
                     </g>
                  </svg>
               </div>
              
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading1 f-16 f-md-18 w700 lh160 black-clr">
                  The Simple 60 Seconds Academy Gets Automated Sales Daily (Anyone Can Do This)
                  </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-36 f-28 w600 text-center white-clr lh150">
                  <u>EXPOSED</u>–A Breakthrough 1-Click Software That <br><span class="w900 f-md-45 yellow-clr">Makes Us $525/Day Over &amp; Over Again</span><br> <span class="lh130">
                  By Creating Beautiful Udemy<sup class="f-20">TM</sup> Like Sites Pre-Loaded with 400+ HOT Video Courses in JUST 60 Seconds…
                  </span> 
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 text-center lh150 white-clr2">
                     No Course Creation. No Camera or Editing. No Tech Hassles Ever... Even A 100%<br class="d-none d-md-block"> <span class="w700">Beginner or Camera SHY Can Start Profiting</span> Right Away 
                  </div>
               </div>
               <div class="col-md-10 mx-auto col-12 mt-md50 mt20">
                  <!-- <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video">
                     <iframe src="https://academiyo.dotcompal.com/video/embed/4inqz8dc2v" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
               <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED 
               </div>
            </div>
         </div>
      </div>


      <!--WebPrimo Header Section End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/academiyo.webp">
            <source media="(min-width:320px)" srcset="assets/images/academiyo-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/academiyo.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : WebPull
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--WebPull Header Section Start -->
<div class="webpull-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                  </path>
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                  </path>
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460"></stop>
                     <stop offset="1" style="stop-color:#FCB121"></stop>
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                  </path>
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z">
                  </path>
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                     </path>
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                     </path>
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                     </path>
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                     </path>
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                  </polygon>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-21 f-20 w500 lh140 white-clr">
               Copy Our Secret 60-Seconds System That Makes Us $525/Day Over &amp; Over Again
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w500 text-center white-clr lh140 text-shadow">
               <span class="f-md-42">The Breakthrough Technology Creates Done-For-You</span>
               <span class="w700">Profit-Pulling Websites In 10,000+ Niches</span> <br>
               <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds Flat…</span>
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-24 w500 text-center lh140 white-clr2 text-capitalize">
               <span class="w700">Easily Create &amp; Sell Websites for BIG Profits</span> to Affiliate Marketers, Coaches, <br class="d-none d-md-block">Local Businesses like Attorney, Dentists, Gyms etc., Self Help, Weight Loss, Dating,<br class="d-none d-md-block">  Crypto &amp;  10,000+ Other Niches I No Coding Needed Ever...
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="col-12 responsive-video">
                        <iframe src="https://webpull.dotcompal.com/video/embed/6p5m1zpj87" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>

                    

                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                           <li class="w600">Tap Into Fast-Growing $284 Billion Industry &amp; Bank BIG </li>
                           <li>Create <span class="w600">Elegant, Ultra-Fast Loading &amp; Mobile Ready Websites</span> in 60 Seconds </li>
                           <li class="w600">400+ Eye-Catching Themes with Done-For-You Content </li>
                           <li><span class="w600">100% SEO Ready Websites</span> for Tons of FREE Search Traffic </li>
                           <li><span class="w600">Get More Likes, Shares and Viral Traffic with</span> In-Built Social Media Tools </li>
                           <li><span class="w600">Accept Payments</span> For Your Services &amp; Products </li>
                           <li><span class="w600">Without Touching Any Code,</span> Customize Websites Easily</li>
                           <li>Analytics &amp; Remarketing Ready </li>
                           <li><span class="w600">Newbie-Friendly</span> - No Tech Skills Needed, No Monthly Fees Ever. </li>
                           <li><span class="w600">Commercial License Included</span> to Build an Incredible Income Helping Clients! </li>
                           <li>Bonus Live Training - <span class="w600">Find Tons of Local Clients Hassle-Free</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 mt20 mt-md60 d-md-none">
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr">
                           (Free Commercial License + <br class="d-none d-md-block"> Low 1-Time Price For Launch Period Only)
                        </div>
                        <div class="row">
                           <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                              <a href="https://warriorplus.com/o2/buy/vr5dvn/f8s4cl/ftvdmb" class="cta-link-btn px-md30">Get Instant Access To WEBPULL</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="https://cdn.oppyo.com/launches/webpull/special/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                           <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/webpull/special/v-line.webp " class="img-fluid "></div>
                           <img src="https://cdn.oppyo.com/launches/webpull/special/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                        </div>
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/limited-time.webp" class="mt20 img-fluid d-md-none mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/webpull-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/webpull-mview-steps.webp">
            <img src="assets/images/webpull-steps.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!--WebPrimo ends-->


    <!-------Exclusive Bonus----------->
    <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Buzzify
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Viddeyo Section------>

   <!-- Header buzzify Start -->

   <div class="buz-header-section">
      <div class="container">
         <div class="row">

            <div class="col-12">
               <div class="row">
                  <div class="col-md-3 text-center mx-auto">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                        <style type="text/css">
                           .st0 {
                              fill: #FFFFFF;
                           }
                        </style>
                        <g>
                           <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                           <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                           <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                           <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                           <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                           <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                           <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                        </g>
                     </svg>
                  </div>
               </div>
            </div>
            <!-- <div class="col-12 mt20 mt-md50"> <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/demo-vid1.png" class="img-fluid d-block mx-auto"></div> -->
            <div class="col-12 text-center lh140 mt20 mt-md30">
               <div class="pre-heading-b f-md-20 f-18 w600 lh140">
                  <div class="skew-con d-flex gap20 align-items-center ">
                     It's Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh140">
               Breakthrough 3-Click Software Uses a <span class="under yellow-clr w700"> Secret Method to Make Us $528/Day Over and Over</span> Again Using the Power of Trending Content &amp; Videos
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh140 yellow-clr">
               All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
               <div class="col-12 responsive-video">
                  <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- Header buzzify End -->
    <div class="bsecond-section">
      <div class="container">
         <div class="row">
            <div class="col-12 p-md0">
               <div class="col-12 key-features-bg-b d-flex align-items-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Creates Beautiful &amp; Self-Updating Sites </span>with Hot Trending Content &amp; Videos </li>
                           <li class="w600">Built-In 1-Click Traffic Generating System </li>
                           <li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
                           <li>Legally Use Other's Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
                           <li>1-Click Social Media Automation - <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
                           <li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
                           <li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Set and Forget System</span> With Single Keyword - Set Rules To Find &amp; Publish Trending Posts</li>
                           <li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
                           <li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients &amp; Charge Them</li>
                           <li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
                           <li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
                           <li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
                           <li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3m.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!--buzzify ends-->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : NinjaKash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="headernj-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mt-md7">
							<img src="assets/images/njlogo.png" class="img-fluid mx-auto d-block">
						</div>
					</div>
					<div class="col-12 col-md-12 text-center">
						<div class="mt20 mt-md50">
							<div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-nj">
								Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="mt-md30 mt20">
							<div class="f-28 f-md-50 w500 text-center white-clr lh140">
							
							<span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
							</div>
						</div>
						<div class="f-22 f-md-28 w600 text-center orange lh140 mt-md30 mt20">
							
							NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
							
						</div>
					</div>
				   <div class="col-12 col-md-12">
						<div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left mt20 mt-md50">
                     <div class="col-12 responsive-video">
                        <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div> 

                  </div>
							
							<div class="col-md-4 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md40 mt20 min-md-video-width-right">
								<ul class="list-head pl0 m0">
									<li>Kickstart with 50 Hand-Picked Products</li>
									<li>Promote Any Offer in Any niche HANDS FREE</li>
									<li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
									<li>Drive TONS OF Social &amp; Viral traffic</li>
									<li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
									<li>No Monthly Fees…EVER</li>
								</ul>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
      <div class="">
			<div class="container-fluid p0">
				<img src="assets/images/ninjakash-grandb1.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb2.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb3.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
			</div> 
		</div>
   <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #5 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
 <!-- JOBiin section Starts -->
   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED-A Breakthrough Software That<span class="w700 orange-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 white-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-8 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="features-list-jobin pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w700">Create Multiple UNIQUE Job Sites</span> - Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w700">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w700">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w700">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w700">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list-jobin pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
                        <li><span class="w700">Get Started Immediately</span> - Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w700">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>

                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>

 <!-- JOBiin section ends -->



      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase WriterArc, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
  <!-- Bonus #1 Section Start -->
  <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>The essence of viral marketing is content. In other words, you have to get viral content, so you can pull a lot of traffic from many different places. </li>
                           <li> Most people who try viral marketing are clueless about this.</li>
                           <li> They don't even know that this niche network already exists.</li>
                           <li>With this guide you are going to understand the essence of viral marketing and learn to identify the types of content that go viral every single day.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Finding a Profitable Niche Market
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> Finding a niche that pays and then creating content, products, and services for that niche, is the only way you will be able to build a solid foundation for a successful online business. </li>
                           <li>Finding a profitable niche is also the best way to achieve the level of success and the kind of profits that most online business owners need to make their daily efforts worthwhile.</li>
                           <li>While there are a lot of popular niches that you can choose from, not all of them will be profitable.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           You can have the best product or the best service in the world but if you have no traffic - it's all completely worthless.<br><br>
                           <b>Topics covered:</b><br><br>
                           <li><b>Email Traffic -</b> The money is in the list! This video will show you how you can generate huge amounts of email traffic that are ready to buy your product.</li>
                           <li><b>Facebook Traffic -</b> In this video you will learn tips and tricks to help you avoid the common pitfalls people make.</li>
                           <li><b>Forum Traffic -</b> You will learn how to hunt down these forums for your niche and get laser targeted, and passionate traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Email marketing for business isn't a new concept, and it has been proven to be one of the best marketing tactics for return on investment. </li>
                           <li>With more than 205 billion emails being sent and received every day if your business isn't taking advantage of this powerful and massive marketing channel, then you are missing out on a highly effective way to reach your target audience.</li>
                           <li>Creating a successful email marketing campaign isn't difficult, but it does require you to do more than just send out an occasional newsletter. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Youtube promotion
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>More than 1.9 billion users visit YouTube on a monthly basis and watch billions of hours of videos every day, making YouTube the second most visited website in the world.
                           </li>
                           <li>Because of that YouTube has become a serious marketing platform where businesses are given the opportunity to promote content in a truly visual and highly engaging way. </li>
                           <li>It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AYUSHLOVE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEME"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz1.com/c/47069/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Network marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Thanks to advances in technology and new ways that employees interact and meet with one another, virtual networking now makes it possible to reach others and make connections that were not available in the past.</li>
                           <li>Through social media, Zoom, and other tools, you are able to get ahead and really meet people from all around the world. </li>
                           <li>This guide is going to explore some of the different ways that you can reach out to others and really expand your network, without having to go into the office or worry about distance and other barriers. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Marketing Concepts
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>With this video course you will discover how to tap into affiliate marketing and make passive commissions by recommending products you love.  </li>
                           <li>You will learn to understand the most powerful concepts of affiliate marketing and develop a clear, long-term strategy to earn passive income as an affiliate. </li>
                           <li>Also, you will discover exactly how affiliate marketing works, and how to start your own affiliate website or blog to make passive income online. This course needs no prior experience in affiliate marketing, online marketing or website building. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Niche Jumpstart
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>How To Create A Profitable Niche Business From Scratch Within Hours From Now... And Make It Pull In Thousands Per Month Without Fail! </li>
                           <li>If you are a blogger, affiliate marketer or an ordinary person who wants to make money online, tapping into the niche marketing industry is a huge advantage for you to get started today.</li>
                           <li>Compared to other make money online industry, niche marketing targets a specific market where you can determine if that certain market is highly profitable or not. </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Networking vedio course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.</b> </li>
                           <li>Virtual networking allows this to happen, whether you are in the office or work from anywhere. </li>
                           <li>Through virtual networking, you will be able to take advantage of online platforms, social media, and even online webinars and conferences in order to form connections with others, even when you are not able to meet them in-person.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        20 best Images for blog
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Get Your Hands On 20 Hi Resolution Images That You Can Use On Your Websites, Blogs, Desktop Or Anywhere Else You Can Think Of! </li>
                           <li>Each image in this package is 1600 x1000 pixels, some are even larger. This give you more freedom and control over how you use them. They all come in a ready to go jpeg format. </li>
                           <li>Remember, the package of 20 high resolutions graphics comes with Resell Rights! Use the images yourself and, or resell them for 100% Profit! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AYUSHLOVE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEME"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz1.com/c/47069/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Blogging templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Make Thousands of Dollars Online Offering Mobile Friendly Websites to Your Local Business Clients!</b></li>
                           <li>Since the launch of the latest Google Algorithm update called Mobilegeddon, local business owners, internet marketers, webmasters and SEO's are now concern about the looks of their website in various types of mobile devices as this will also impact the website rankings in mobile search.</li>
                           <li>Though this will not affect the website's ranking if you are browsing the website in desktop computers, this is a huge loss if you will not optimize your website for mobile devices like tablets and smartphones.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Popup 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</b></li>
                           <li>You can trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your product, article or even your profile.  Additionally, you can add a secondary content popup that can contain optin forms, buy buttons, or social sharing icons.This will help boost your traffic, sales and email lists.  This is an “in-content” trigger tool so you have the ability to choose the timing when the popup shows up. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Facebook adsense
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>While the FB Ad Secrets was made for beginners just starting in Facebook advertising, these videos answer the coming challenges you will face as you scale to spending hundreds or thousands of dollars per day while remaining profitable.</b></li>
                        <li>For beginners, you will usually start with a low budget to run your Facebook ads. But eventually when the time comes for you to scale your offer with a bigger budget, you will come across problems such as your ad account being disabled, profit margin becomes thinner and things like when and how to split test your ads.</li>
                        <li>With this upgrade, it will help you prepare to scale your business to possibly 6-7 figures a year! You need to think long term and if you want your business to have massive returns, you must be willing to invest and work towards it as well.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Graphic Designs
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>When you say yes to Garage Sale Graphics, you not only will have a better looking website, but you will notice an increase in the amount of sales you get without an increase in traffic. </li>
                           <li>Simply put, Garage Sale Graphics will help you convert more of your visitors into customers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social marketing 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>A common problem that many of us have is expecting our content to go viral and for our follower count to grow immediately. </li>
                           <li>Many people spend an average of 3 hours on social media per day, and this number increases depending on the demographic. </li>
                           <li>In reality, posts rarely go viral without many hours spent researching, strategizing and planning the most exciting and effective content to share with their engaged followers.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AYUSHLOVE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEME"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz1.com/c/47069/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social sites webninar
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this powerful plugin you can create amazing webinar landing pages inside of Facebook!</b></li>
                           <li>There is no limit on how many Facebook webinar pages you can create, create one for all your clients on ONE plugin install.</li>
                           <li>Collect leads inside of WP and export them to CSV and have them automatically opted in to your favourite AR service.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Marketing Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Facebook Messenger has revolutionized how marketers can access and grow their audience on Facebook and websites that use many of Facebook's extensions.</b></li>
                           <li>Facebook is not just one platform, it is actually a set of related properties. It also shares key parts of its functionality with third-party websites.</li>
                           <li>In this video course you will learn how Facebook Messenger works, how you can create bots that would work with Facebook Messenger so you can grow your audience, and most importantly, drive qualified traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Speed up your content
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Learn to build faster websites from now on. </li>
                        <li>Not all web hosts are created equal - make sure you sign up for a fast web host that will help you reach your business goals. </li>
                        <li>Know how to use and analyze the results of the best website speed testing tools out there. </li>
                        <li>Never underestimate the power of caching ever again - it will help your site load much faster than ever before. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Seo titles
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</b> Watch as your social network shares increase, your google PageRank and more.</li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Boom software 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</b></li>
                           <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li>
                           <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 orange-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 orange-clr">"AYUSHLOVE"</span> for an Additional <span class="w700 orange-clr">15% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20 ">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab WriterArc + My 20 Exclusive Bonuses
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 orange-clr">"WRITEME"</span> for an Additional <span class="w700 orange-clr">$50 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz1.com/c/47069/386776/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab WriterArc Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
    
      <div class="footer-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
               <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
               <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
               <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
               <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
               <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
               <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
               <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
               <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
               <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
               <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
            </svg>
            <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
         </div>
         <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
            <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
            <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
               <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
               <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>

      <!--Footer Section End -->

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
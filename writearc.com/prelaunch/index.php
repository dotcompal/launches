<!Doctype html>
<html>

<head>
   <title>WriterArc Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="WriterArc | Prelaunch">
   <meta name="description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta name="keywords" content="WriterArc">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.writerarc.com/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WriterArc | Prelaunch">
   <meta property="og:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta property="og:image" content="https://www.writerarc.com/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WriterArc | Prelaunch">
   <meta property="twitter:description" content="Discover How to Market & Monetize your Videos And Captivate your Audience To Boost Conversion And ROI">
   <meta property="twitter:image" content="https://www.writerarc.com/prelaunch/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   <script>
      function getUrlVars() {
         var vars = [],
            hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
         for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
         }
         return vars;
      }
      var first = getUrlVars()["aid"];
      //alert(first);
      document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value', first);
      })
      //document.getElementById('myField').value = first;
   </script>
</head>

<body>
   
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row align-items-center">
                  <div class="col-md-3 text-center text-md-start">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                        <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                        <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                        <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                        <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                        <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                        <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                        <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                        <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                        <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                        <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                     </svg>
                  </div>
                  <div class="col-md-9 mt20 mt-md5">
                     <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                        <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="preheadline f-20 f-md-23 w500 white-clr lh140">
               Register for One Time Only Value Packed Free Training & Get <span class="w700 gradient-orange">WebPrimo </span> as a Gift + 10 Free Licenses
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50 f-28 w800 text-center white-clr lh140">
               Discover How to Monetize Your Contentand Instantly Captivate Your Audience to Boost Conversions And ROI
            </div>
            <div class="col-12 mt-md35 mt20  text-center">
               <div class="f-18 f-md-24 w600 text-center lh150 white-clr">
               In this Webinar, we will also reveal, how we have sold over $3Mn of digital products using <br class="d-none d-md-block"> the power of A.I. Copywriting and saved 100's of dollars monthly on hiring copywriter.
               </div>
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/webinar.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                  <div class="calendar-wrap-inline">
                     <div class="f-22 f-md-26 w800 text-center lh180 white-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center white-clr" contenteditable="false">16<sup contenteditable="false">th</sup> September, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1979481070" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6333978" />
                              <input type="hidden" name="redirect" value="https://www.writerarc.com/prelaunch-thankyou" id="redirect_76fa84e3f897e4b6006678878ccf3896" />
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1979481070" class="af-form">
                              <div id="af-body-1979481070" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114651767"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114651767" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114651768"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114651768" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->

   <!-- Header Section Start -->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-36 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                           Why Attend This Free Training Webinar & What Will You Learn?
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                           Our 20+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li>How We Have Sold Over $7Mn In Digital Products Using the Power of Video Marketing and You Can Follow the Same to Build a Profitable Business Online.</li>
                           <li>How You Can Boost Your Conversions and Sales by Simply Using the Power of Content.</li>
                           <li>How Content Marketing Can Help to Build Trust Among Your Audience and Help in Improving ROI.</li>
                           <li>How You Can Tap into The Fastest Growing Copywriting & Content Marketing Agency Market without any prior copywriting experience.</li> 
                           <li>How We Have Saved 1000s Of Dollars Monthly on Expensive Copywriting Expertsand Still Not Getting Desired Results.</li>
                           <li>A Sneak-Peak Of "WriterArc", Disruptive A.I. Technology to Create Any Type of Marketing Content for Any Local or Online Niche.</li> 
                           <li>During This Launch Special Deal, Get All Benefits at A Limited Low One-Time-Fee.</li>
                           <li>Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us Plus We Are Giving 10 Licenses of Our Premium Solution –WriterArc</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Discounted Price Section Start -->
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh140 white-clr" editabletype="text" style="z-index:10;">
                     So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh140 mx-auto my20 text-center white-clr">
                     Register Now! And Join Us Live On September 16th, 10:00 AM EST
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="1979481070">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6333978">
                           <input type="hidden" name="redirect" value="https://www.writerarc.com/prelaunch-thankyou" id="redirect_76fa84e3f897e4b6006678878ccf3896">
                           <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-1979481070" class="af-form">
                           <div id="af-body-1979481070" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114651767"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-114651767" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114651768"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-114651768" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- Discounted Price Section End -->
      </div>
   </div>
   <!-- Header Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
        <div class="container">
           <div class="row">
              <div class="col-12 text-center">
                 <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                    <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                    <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                    <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                    <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                    <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                    <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                    <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                    <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                    <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                    <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                 </svg>
                 <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                 <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
                 <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                    <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                 </ul>
              </div>
           </div>
        </div>
     </div>
    <!--Footer Section End -->
</body>

</html>
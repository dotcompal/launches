<!Doctype html>
<html>

<head>
    <title>WriterArc BizDrive</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="WriterArc BizDrive">
    <meta name="description" content="Supercharge Your Agency Business and Manage it with Speed & Agility">
    <meta name="keywords" content="WriterArc">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.writerarc.com/bizdrive/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="WriterArc BizDrive">
    <meta property="og:description" content="Supercharge Your Agency Business and Manage it with Speed & Agility">
    <meta property="og:image" content="https://www.writerarc.com/bizdrive/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="WriterArc BizDrive">
    <meta property="twitter:description" content="Supercharge Your Agency Business and Manage it with Speed & Agility">
    <meta property="twitter:image" content="https://www.writerarc.com/bizdrive/thumbnail.png">
    <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_https://cdn.oppyo.com/launches/writerarc/bizdrive/favicon.png" type="image/png">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
</head>

<body>
    <!-- New Timer  Start-->
    <?php
    $date = 'September 16 2022 11:59 AM EST';
    $exp_date = strtotime($date);
    $now = time();  
    /*
    
    $date = date('F d Y g:i:s A eO');
    $rand_time_add = rand(700, 1200);
    $exp_date = strtotime($date) + $rand_time_add;
    $now = time();*/
    
    if ($now < $exp_date) {
    ?>
 <?php
    } else {
     echo "Times Up";
    }
    ?>
 <!-- New Timer End -->
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div>
                        <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                            <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                            <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                            <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                            <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                            <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                            <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                            <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                            <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                            <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                            <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                         </svg>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 text-center">
                    <div class="preheadline f-20 f-md-22 w500 white-clr lh140">
                        You’ve come a long way, but before we go further to access your purchase, I would like to ask you...
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                    How Would You Like to <span class="gradient-orange"> Supercharge Your</span> <span class="gradient-orange">Agency Business and Manage </span> it with Speed & Agility on an Enterprise Model?
                </div>
                <div class="col-12 mt-md35 mt20  text-center">
                    <div class="f-18 f-md-24 w600 text-center lh140 white-clr">
                        Store, Share, and Manage All your Business Data, Media Files Client Projects with an Enterprise Grade Security.
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md50">
                <div class="col-md-7 col-12 min-md-video-width-left">
                    <!-- <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/productbox.webp" class="img-fluid mx-auto d-block" alt="Feat8">  -->
                    <div class="responsive-video">
                        <iframe class="embed-responsive-item" src="https://writerarc.dotcompal.com/video/embed/hxpqc9ihim" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                   <div class="calendar-wrap">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                           <li> Get Easy & Secure Access to your content and files. </li>
                           <li> Store, Share & Collaborate on files & folders from any of your mobile devices, tablet, or computer. </li>
                           <li> Flexibility of work from anywhere as you can access all your data from anywhere across the globe. </li> 
                           <li> Cost Efficient & Attractive alternative to traditional storage like in office server or portable hard drive. </li>
                           <li> Enterprise-grade security for your medium or small business with no chance of failure or data loss. </li>
                        </ul>
                   </div>
                </div>
            </div>
            <div class="row mt20 mt-md50">
                <div class="col-12  f-18 f-md-20 lh140 w500 white-clr text-center">
                    This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center">Upgrade to WriterArc BizDrive Now</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                   <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block">
                </div>
                <div class="col-md-5 mx-auto col-12 mt20 mt-md20">
                    <div class="f-18 f-md-20 lh140 w500 white-clr text-center">
                        Deal Ending In…
                    </div>
                    <div class="countdown counter-black mt15">
                        <div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">09</span><br><span class="f-18 w600">Days</span> </div><div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">01</span><br><span class="f-18 w600">Hours</span> </div><div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">42</span><br><span class="f-18 w600">Mins</span> </div><div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">08</span><br><span class="f-18 w600">Sec</span> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="step-sec">
                        <div class="d-flex align-items-center gap-4">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/step-icon1.webp" alt="Step" class="img-fluid d-block">
                            <div class="f-md-24 w700 lh140 f-20">Get File on Demand</div>    
                        </div>
                        <div class="mt10 f-md-20 f-18 w400 lh140">
                            Access all your Biz Drive files on any of your devices without taking up space on your device.
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt30">
                    <div class="step-sec">
                        <div class="d-flex align-items-center gap-4">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/step-icon2.webp" alt="Step" class="img-fluid d-block">
                            <div class="f-md-24 w700 lh140 f-20">Secure Sharing</div>  
                        </div>
                        <div class="mt10 f-md-20 f-18 w400 lh140">
                            All your files and data are protected with enterprise-grade security. And you can share files & projects with your clients with an additional security layer with password protection.
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt30">
                    <div class="step-sec">
                        <div class="d-flex align-items-center gap-4">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/step-icon3.webp" alt="Step" class="img-fluid d-block">
                            <div class="f-md-24 w700 lh140 f-20">Personal Drive</div>
                        </div>
                        <div class="mt10 f-md-20 f-18 w400 lh140">
                            You will have a personal drive for yourself and your team members as well to store personal files, photos, etc.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section Start -->
    <div class="fastest-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-50 w700 text-center lh140">
                    This Will Be the Last Cloud <br class="d-none d-md-block"> Storage Service You’ll Ever Need
                </div>
                <div class="col-12 col-md-6 mt20 mt-md50">
                    <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                        <li>Store Your Files & Project Securely & Share Faster with <span class="w700">Your Clients</span></li>
                        <li>Share Files on Elegant & Brandable <span class="w700">Sharing Pages to Show Your Authority as a Big Agency.</span></li>
                        <li><span class="w700">Single Dashboard</span> to Manage All Types of Files</li>
                        <li>Manage Files in Folders Easily & Share Entire Folder with Clients or Team Members with <span class="700">Folder Management Feature</span></li>
                        <li><span class="w700">Unbreakable File Security</span> with Online Back-Up & 30 Days File Recovery Functionality</li>
                        <li> <span class="w700">Manage & Share Multiple Files</span> and Save Your Time</li>
                        <li>Access Files Anytime, Anywhere & on Any Device with <span class="w700">Business Cloud</span></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 mt30 mt-md50">
                    <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                        <li>Get <span class="w700">Full-Text Search and Filters</span> to Locate Files Instantly</li>
                        <li>Fetch & Sync Valuable Data Effortlessly with <span class="w700">Google Drive, One Drive & Dropbox Integration</span> </li>
                        <li>Use to <span class="w700">Deliver Website Media (Videos, Images etc.)</span>  to Boost your Website Loading Speed</li>
                        <li> <span class="w700">Create Video Channels</span> by Share Pages to Get More Engagement</li>
                        <li>Get Maximum Visitor Engagement with <span class="w700">Like/Dislike Option on Share Pages</span> </li>
                        <li> <span class="w700">Advanced Share Page Analytics</span> to Have a Complete Insight on How Your Files/Videos Are Doing</li>
                        <li><span class="w700">Capture Unlimited Leads</span> & Unlimited Audience from Share Pages</li>
                    </ul>
                </div>
                <div class="col-12 col-md-12 ">
                    <ul class="features-list pl0 f-18 f-md-20 lh140 w500 black-clr text-capitalize">
                        <li> <span class="w700">Preview And Download</span>  the Files Before Sharing</li>
                    </ul>    
                </div>
            </div>

            <div class="row mt20 mt-md50">
                <div class="col-12  f-18 f-md-20 lh140 w500 black-clr text-center">
                    This is WriterArc Customer’s Only One Time Exclusive Deal, Available for Limited Time!
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center">Upgrade to WriterArc BizDrive Now</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                   <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options1.webp" class="img-fluid mx-auto d-block">
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->
    <!--3. Third Section Start -->
    <div class="third-section">
        <div class="container">
            <div class="row ">
                <div class="col-12 ">
                    <div class="f-18 f-md-20 w400 lh140">
                        <span class="w700 f-24 f-md-28">No matter what business you do.</span>   
                        <br><br> 
                        If you’re in a Local Niche, An Agency Owner, Affiliate Marketer, Email Marketer, Freelancer, Ecom Store Owner, Consultant or you Sell Product/Services Online, it is essential to deliver tons of website images, videos, documents, and other files. Whether it’s for selling, explaining your services, training your team, or delivering client projects we all have to do it.
                        <br><br>
                        Whether it be through a website with images, and videos or sending files directly to others, the faster loading it is, the better your results will be across the board!
                        <br><br> 
                        Supercharge your websites, lead pages, training & client projects and get more sales, leads & customer satisfaction.
                        <br><br> 
                        The faster the delivery - the higher the engagement, meaning...
                    </div>
                    <div class="mt15 mt-md25">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w600 black-clr">
                            <li>More Leads</li>
                            <li>More Sales</li>
                            <li>High Customer Satisfaction</li>
                        </ul>
                    </div>
                    <div class="f-18 f-md-20 w400 mt20 mt-md35 lh140">
                        <span class="w700">Top companies & marketers know this secret and make it a priority to get their marketing material delivered faster.</span> 
                        <br><br>
                        I am sure you want to do the exact thing. Look no further than this special offer to do just that, <span class="w700">and maximize your customers, boost engagement, and ultimately leads and sales. </span>
                        <br><br>
                        <span class="w700">Just imagine having the complete solution at your fingertips.</span> Everything is under one roof with this WriterArc upgrade. It's built for speed and built for marketers, all while keeping your budget in mind.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container">
            <div class="row ">
                <div class="col-12 text-center">
                    <div class="per-shape f-28 f-md-38 w700">
                        Introducing
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40 text-center">
                    <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 1123.4 100" style="enable-background:new 0 0 1123.4 100; max-height:80px" xml:space="preserve">
               <style type="text/css">
                   .st00{fill:#FFFFFF;}
                   .st11{fill:#5055BE;}
               </style>
               <g>
                   <path class="st00" d="M116.7,15.6L93.4,98.7H73.8L58.2,39.5L41.9,98.7l-19.5,0.1L0,15.6h17.9l14.6,64.5l16.9-64.5H68l16,64.2
                       l14.8-64.2C98.8,15.6,116.7,15.6,116.7,15.6z"/>
                   <path class="st00" d="M152.5,34.8c3.5-2,7.4-3,11.8-3v17.5H160c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7v-66h16.7
                       V43C146.3,39.5,149.1,36.8,152.5,34.8z"/>
                   <path class="st00" d="M176.7,22.1c-1.9-1.9-2.9-4.2-2.9-7s1-5.1,2.9-7s4.4-2.8,7.3-2.8c2.9,0,5.4,0.9,7.3,2.8c1.9,1.9,2.9,4.2,2.9,7
                       s-1,5.1-2.9,7s-4.4,2.8-7.3,2.8C181.1,24.9,178.6,24,176.7,22.1z M192.2,32.8v65.9h-16.7V32.8H192.2z"/>
                   <path class="st00" d="M228.1,46.4v31.9c0,2.2,0.5,3.8,1.6,4.8s2.9,1.5,5.4,1.5h7.7v14h-10.5c-14,0-21.1-6.8-21.1-20.5V46.4h-7.9
                       V32.8h7.9V16.4H228v16.3h14.8v13.7C242.8,46.4,228.1,46.4,228.1,46.4z"/>
                   <path class="st00" d="M315.7,70.7h-48.2c0.4,4.8,2.1,8.5,5,11.2s6.5,4,10.8,4c6.2,0,10.6-2.7,13.2-8h18c-1.9,6.4-5.6,11.6-11,15.7
                       s-12,6.1-19.9,6.1c-6.3,0-12-1.4-17.1-4.2c-5-2.8-9-6.8-11.8-12s-4.2-11.1-4.2-17.9s1.4-12.8,4.2-18s6.7-9.1,11.7-11.9
                       c5-2.8,10.8-4.2,17.3-4.2c6.3,0,11.9,1.4,16.8,4c5,2.7,8.8,6.5,11.5,11.5s4.1,10.7,4.1,17.1C316.1,66.7,316,68.8,315.7,70.7z
                        M298.9,59.5c-0.1-4.3-1.6-7.7-4.6-10.3s-6.7-3.9-11.1-3.9c-4.1,0-7.6,1.2-10.4,3.7s-4.5,6-5.2,10.4L298.9,59.5L298.9,59.5z"/>
                   <path class="st00" d="M353.3,34.8c3.5-2,7.4-3,11.8-3v17.5h-4.4c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9v32.9h-16.7v-66h16.7
                       V43C347.1,39.5,349.9,36.8,353.3,34.8z"/>
               </g>
               <g>
                   <path class="st00" d="M534.2,35c3.5-2,7.4-3,11.8-3v17.5h-4.4c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9V99h-16.7V33h16.7v10.2
                       C527.9,39.7,530.7,37,534.2,35z"/>
                   <path class="st00" d="M557.1,48c2.8-5.1,6.6-9.1,11.5-11.9c4.9-2.8,10.6-4.2,16.9-4.2c8.2,0,14.9,2,20.3,6.1
                       c5.4,4.1,8.9,9.8,10.8,17.2h-18c-1-2.9-2.6-5.1-4.8-6.7c-2.3-1.6-5.1-2.4-8.4-2.4c-4.8,0-8.5,1.7-11.3,5.2s-4.2,8.4-4.2,14.7
                       c0,6.3,1.4,11.1,4.2,14.6s6.5,5.2,11.3,5.2c6.7,0,11.1-3,13.2-9h18c-1.8,7.1-5.4,12.8-10.8,17c-5.4,4.2-12.1,6.3-20.2,6.3
                       c-6.3,0-12-1.4-16.9-4.2c-4.9-2.8-8.8-6.8-11.5-11.9c-2.8-5.1-4.2-11.1-4.2-17.9C552.9,59.1,554.3,53.2,557.1,48z"/>
                   <path class="st00" d="M715.9,63.3c3,3.8,4.5,8.2,4.5,13.1c0,4.4-1.1,8.4-3.3,11.7c-2.2,3.4-5.3,6-9.5,7.9c-4.1,1.9-9,2.9-14.6,2.9
                       h-35.8V15.8h34.3c5.6,0,10.5,0.9,14.6,2.7s7.2,4.4,9.3,7.6c2.1,3.3,3.2,6.9,3.2,11.1c0,4.8-1.3,8.9-3.9,12.1
                       c-2.6,3.3-6,5.6-10.3,6.9C709,57.2,712.8,59.5,715.9,63.3z M673.8,50.1H689c4,0,7-0.9,9.2-2.7c2.1-1.8,3.2-4.3,3.2-7.7
                       c0-3.3-1.1-5.9-3.2-7.7c-2.1-1.8-5.2-2.7-9.2-2.7h-15.2V50.1z M700.1,82.5c2.3-1.9,3.4-4.6,3.4-8.1c0-3.6-1.2-6.4-3.6-8.4
                       c-2.4-2-5.6-3-9.6-3h-16.4v22.4h16.8C694.7,85.4,697.8,84.4,700.1,82.5z"/>
                   <path class="st00" d="M734.8,22.3c-1.9-1.9-2.9-4.2-2.9-7s1-5.1,2.9-7s4.4-2.8,7.3-2.8c2.9,0,5.4,0.9,7.3,2.8c1.9,1.9,2.9,4.2,2.9,7
                       s-1,5.1-2.9,7s-4.4,2.8-7.3,2.8C739.2,25.1,736.8,24.2,734.8,22.3z M750.4,33v65.9h-16.7V33H750.4z"/>
                   <path class="st00" d="M782,85.2h29.4v13.7h-48.3V85.5l28.8-38.8h-28.7V33h47.9v13.5L782,85.2z"/>
                   <path class="st00" d="M876.4,21c6.6,3.4,11.7,8.3,15.4,14.6c3.6,6.3,5.4,13.6,5.4,22c0,8.3-1.8,15.6-5.4,21.8
                       c-3.6,6.2-8.7,11.1-15.4,14.5c-6.6,3.4-14.3,5.1-23,5.1h-29V15.8h29C862.1,15.8,869.7,17.6,876.4,21z M873,77.6
                       c4.8-4.8,7.1-11.5,7.1-20.1s-2.4-15.4-7.1-20.3c-4.8-4.9-11.5-7.3-20.2-7.3H841v54.9h11.8C861.5,84.8,868.2,82.4,873,77.6z"/>
                   <path class="st00" d="M934.7,35c3.5-2,7.4-3,11.8-3v17.5h-4.4c-5.2,0-9.2,1.2-11.8,3.7c-2.7,2.5-4,6.7-4,12.9V99h-16.7V33h16.7v10.2
                       C928.5,39.7,931.2,37,934.7,35z"/>
                   <path class="st00" d="M958.9,22.3c-1.9-1.9-2.9-4.2-2.9-7s1-5.1,2.9-7s4.4-2.8,7.3-2.8c2.9,0,5.4,0.9,7.3,2.8c1.9,1.9,2.9,4.2,2.9,7
                       s-1,5.1-2.9,7s-4.4,2.8-7.3,2.8C963.2,25.1,960.8,24.2,958.9,22.3z M974.4,33v65.9h-16.7V33H974.4z"/>
                   <path class="st00" d="M1018.3,83.6L1035,33h17.7l-24.4,65.9h-20.2L983.8,33h17.9L1018.3,83.6z"/>
                   <path class="st00" d="M1122.9,71h-48.2c0.4,4.8,2.1,8.5,5,11.2s6.5,4,10.8,4c6.2,0,10.6-2.7,13.2-8h18c-1.9,6.4-5.6,11.6-11,15.7
                       s-12,6.1-19.9,6.1c-6.3,0-12-1.4-17.1-4.2c-5-2.8-9-6.8-11.8-12s-4.2-11.1-4.2-17.9s1.4-12.8,4.2-18c2.8-5.2,6.7-9.1,11.7-11.9
                       c5-2.8,10.8-4.2,17.3-4.2c6.3,0,11.9,1.4,16.8,4c5,2.7,8.8,6.5,11.5,11.5s4.1,10.7,4.1,17.1C1123.4,66.9,1123.3,69.1,1122.9,71z
                        M1106.2,59.8c-0.1-4.3-1.6-7.7-4.6-10.3s-6.7-3.9-11.1-3.9c-4.1,0-7.6,1.2-10.4,3.7s-4.5,6-5.2,10.4h31.3V59.8z"/>
               </g>
               <g>
                   <g>
                       <path class="st11" d="M468.7,50c-9-2.8-18.6-4.4-28.5-4.4c-12.3,0-24,2.3-34.7,6.5L435.6,0v23.6c-2.6,0.9-4.4,3.3-4.4,6.2
                           c0,3.6,3,6.6,6.6,6.6s6.6-3,6.6-6.6c0-2.9-1.8-5.3-4.4-6.2V0.2L468.7,50z"/>
                   </g>
                   <g>
                       <path class="st00" d="M496.8,98.8c-16.2-12.9-36.8-20.7-59.2-20.7c-0.7,0-1.5,0-2.2,0c-21.4,0.5-41.1,8.1-56.8,20.5l21.4-37
                           c-3.2-0.8-6.6-1.3-9.9-1.7c-3.8-0.5-7.6-0.7-11.5-0.7c8.2-3.4,16.9-5.7,26-6.7c0.2,0,0.5-0.1,0.7-0.1c3.2-0.3,6.4-0.5,9.7-0.5
                           c34.8,0,65.3,18.7,81.8,46.7V98.8z"/>
                   </g>
               </g>
               </svg>
               
                </div>

                <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="f-md-36 f-28 text-center white-clr lh140 w700">
                    Store, Share & Collaborate on files & folders from  <br class="d-none d-md-block">
                    anywhere and on any device to Supercharge your Business.
                  </div>
               </div>

                <div class="col-12 mt20 mt-md40">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/productbox.webp" class="img-fluid d-block mx-auto " alt="Product-Box">
                </div>
                
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->

    <!-- Step Section Start -->
    <div class="step-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-40 w700 lh140 text-center">
                        Captivating your audience & getting your files <br class="d-none d-md-block"> delivered super-fast is as easy as 1, 2, 3…
                    </div>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
                <div class="col-12 col-md-6 relative">
                    <div class="shape-skew f-md-24 f-22 lh140 w700 text-center mb20">
                        <span class="skew white-clr">
                            #1 Get File on Demand 
                        </span>
                    </div>
                    <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                        Just upload any file- Images, videos, audios, documents, etc. from your PC once and the WriterArc BizDrive optimizes those according to speed & make resolutions for faster delivery 
                    </div>
                    <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -120px; top: 220px;">
                </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                <video autoplay loop muted class="w-100">
                    <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/upload.mp4" type="video/mp4">
                    <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/upload.ogg" type="video/ogg">
                    Your browser does not support the video tag.
                </video>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
                <div class="col-12 col-md-6 order-md-2 relative">
                    <div class="shape-skew f-md-24 f-22 lh140 w700 text-center mb20">
                        <span class="skew white-clr">
                            Step #2 - Manage 
                        </span>
                    </div>
                    <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                        With just a few clicks, manage these files according to your requirements, find them effortlessly according to file type or simply search
                    </div>
                    <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 220px; left: -250px;">
                </div>
                <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                <video autoplay loop muted  class="w-100">
                        <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/manage.mp4" type="video/mp4">
                        <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/manage.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
                <div class="col-12 col-md-6 relative">
                    <div class="shape-skew f-md-24 f-22 lh140 w700 text-center mb20">
                        <span class="skew white-clr">
                            Step #3 - Publish  
                        </span>
                    </div>
                    <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                        You can share, publish or access any file wherever you want instantly & start getting more eyeballs & more customers glued
                    </div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md0">
                <video autoplay loop muted  class="w-100">
                        <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/publish.mp4" type="video/mp4">
                        <source src="https://cdn.oppyo.com/launches/writerarc/bizdrive/publish.ogg" type="video/ogg">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
            <div class="mt20 mt-md40 d-flex gap-3 justify-content-center flex-wrap">
                <div class="">
                    <div class="d-flex align-items-center gap-4 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No installation</div>
                    </div>
                </div>
                <div class="mt10 mt-md-0">
                    <div class="d-flex align-items-center gap-4 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Coding or Designing</div>
                    </div>
                </div>
                <div class="mt10 mt-md-0">
                    <div class="d-flex align-items-center gap-4 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Step Section End -->

    <!-- feature1 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <div class="f-28 f-md-40 w700 text-center">
                        WriterArc Business <span class="blue-gradient">Drive Is Forever Going to Change the Way
                            You Host,</span> Manage and Share Your Business Files (For the Better!)
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 mt30 mt-md50">
                <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/1.webp" class="img-responsive" alt="Feature-Icon1">
                <div class="col-12 col-md-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr">
                    Use WriterArc BizDrive to Store Your Media Securely & Share Faster with Your Clients
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f1.webp" class="img-fluid mx-auto d-block" alt="Feature1">
                </div>
                <div class="f-18 f-md-20 w400 lh140 mt10 mt-md30 text-left col-12">
                    <span class="w700"> Store Unlimited Files</span> like images, videos, audios and documents with WriterArc BizDrive Quick delivery is most important pre-requisite for every successful marketer today. With our fast CDN’s, sharing your files,
                    documents, videos etc. becomes quicker, faster and easier with low latency and high transfer speeds.
                </div>
            </div>
        </div>
    </div>
    <!--feature1 -->
    <!--feature2 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/2.webp" class="img-responsive" alt="Feature-Icon2">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr pl0">
                        Engage Maximum Audience with Royalty Free Stock Images & Videos- Pixabay & Pexels Integration
                    </div>
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f2.webp" class="img-fluid mx-auto d-block" alt="Bussiness-Manage">
                    </div>
                    <div class="f-18 f-md-20 w400 lh140 mt15 mt-md30 mt10 mt-md60 text-left col-12">
                        Eye-catchy images are best way to attract audience and get them hooked to your sites.
                        <br><br>So, with WriterArc BizDrive, you’re getting the power of utilizing <span class="w700">Millions of Royalty Free Images &amp; videos for your benefit through Pixabay,
                            Pexels &amp; Shutterstock Integration. </span>Just use them and let the magic begin.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature3 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/3.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Share Files on Elegant, Brandable & SEO optimized Sharing Pages
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f3.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Add your LOGO to make file sharing with your clients and customers on <span class="w700">your
                            own branded share page &amp; stand out of the crowd.</span><br><br> You get elegant, 100% SEO &amp; Mobile optimized file sharing pages to take user experience to another level.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature3 -->
    <!--feature4 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/4.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Advanced share page analytics to have a complete insight on How Your Files Are Doing
                    </div>
                </div>
                <div class="col-12 mx-auto mt20 mt-md50">
                    <div class="row align-items-center">
                        <div class="col-md-6"> <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f4a.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client"></div>
                        <div class="col-md-6 mt20 mt-md0"> <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f4b.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client"></div>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Get <span class="w700">complete analytics on your shared pages</span> and take the corrective actions if needed. Know your numbers to improve your marketing campaigns &amp; better results.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature5 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/5.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Single Dashboard to Manage All Type of Files. No Need to Buy Multiple Apps
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f5.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Hate buying multiple apps and logging in to them repeatedly to access your files; here’s some great news for you.<br><br> With WriterArc BizDrive, kick out all headaches of buying and managing multiple apps. Use our <span class="w700">single user-friendly dashboard to manage everything</span>                        like a boss.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature6 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/6.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Preview and Download the Files Before Sharing
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f6.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Need to make last minute changes in order to make your files more engaging &amp; attractive; you’re at the right place. WriterArc BizDrive gives you the <span class="w700">power to
                            preview the files before sharing</span> and make the necessary changes in order to get best results.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature7 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/7.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Manage files in folders easily & share entire folder with clients or team members with Folder Management Feature
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f7.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        No more worries to accumulate your scattered files at different locations. With WriterArc BizDrive, you can <span class="w700">easily manage all your folders</span> at one safe central location and access them whenever you need.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature8 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/8.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Capture Unlimited Leads & Unlimited Audience from Share Pages
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f8.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Getting targeted leads for your offers just got simpler. With WriterArc BizDrive, you’ll be instantly able to <span class="w700">drive unlimited viral traffic &amp; leads</span> and convert them into lifetime buyers once and for all.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature9 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/9.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Get Maximum Visitor Engagement with Like/Dislike Option on Share Pages
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f9.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Getting maximum engagement is a must for every successful marketer. So WriterArc BizDrive gives you the power to boost brand engagement &amp; get maximum leads as the <span class="w700">users can like/dislike your files that ultimately gets them hooked to your
                            offers and</span> gets them connected in the long run.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature10 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/10.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Get Full Text Search and Filters to Locate Files Instantly
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f10.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        <span class="w700">Search any file by putting the name of file in search box</span> and by using filters to spot it almost instantly.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature11 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/11.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Unbreakable File Security with Online Back-Up & 30 Days File Recovery Functionality
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f11.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Kick out the hackers and keep your files &amp; valuable data secured from sniffing attacks with
                        <span class="w700">our SSL Security &amp; Akamai &amp; AWs architecture.</span><br><br> By <span class="w700">2-way authentication using OTP</span> enabled login feature, it’s unbreakable security &amp; data hacking chances are
                        dealt with 99.99% effectively.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature12 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/12.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Speed-Up Your Website Speed with Fast Loading & Optimized Images
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f12.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        53% visitors abandon your website if it takes more than 3 seconds to load. So, we’ve created WriterArc BizDrive to bail you out of this menace easily. Just use our state-of-the-art infrastructure to make a competitive move with
                        <span class="w700">websites and landing pages that load faster than most.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/13.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Access Files Anytime, Anywhere with Business Cloud
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto">
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f13.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Cloud is the new fad today. With WriterArc BizDrive, access your images, videos, files, documents, zip files etc. anywhere and anytime you need.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature16 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/14.webp" class="img-responsive" alt="Feature-icon3">
                    </div>
                    <div class="col-md-12 col-12 f-22 f-md-32 w700 lh140 mt10 mt-md10 text-left black-clr ">
                        Fetch & Sync Valuable Data Effortlessly with Google Drive, One Drive & Dropbox Integration
                    </div>
                </div>
                <div class="col-12 mx-auto">
                <div class="row align-items-center row-cols-1 row-cols-md-2">
                    <div class="col mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f14a.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                    <div class="col mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/f14b.webp" class="img-fluid mx-auto d-block" alt="Unlimited Client">
                    </div>
                </div>
                </div>
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                        Having files spread across different cloud accounts can be possible now. With <span class="w700">Google Drive, One Drive &amp; Dropbox Integration</span> you can easily fetch and sync your valuable data.
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">
                        Get Instant Access to WriterArc Bizdrive
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-45 f-28 lh140 w700 white-clr feature-shape">
                        But That’s Not All
                    </div>
                </div>
                <div class="col-12 mt40">
                    <div class="f-20 f-md-22 w500 text-center lh140">
                        In addition, we have a number of bonuses for those who want to take action today...
                    </div>
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 px-md15">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus1.webp" class="img-fluid d-block bonus-size" alt="Bonus1">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Ready-Made Niche Websites
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Have your website up and running in a matter of minutes! They're perfect for ClickBank affiliate sales or for creating high traffic sites for use with Google AdSense. Now create websites for almost any niche using this masterpiece and keep its media safe
                            and secured with WriterArc BizDrive.
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mt20 mt-md0">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus2.webp" class="img-fluid d-block bonus-size" alt="Bonus2">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Auto Video Creator
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Discover how to create your own professional videos in a snap! You don't even have to speak ... the software will do it for you. Now you too can create video without using cameras, PowerPoint, Camtasia and even voice overs. When combined with WriterArc
                            BizDrive, this package proves to be of immense worth for every marketer.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-6">
                        <div class="mt20 mt-md50">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox2">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 px-md15">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus3.webp" class="img-fluid d-block bonus-size" alt="Bonus3">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            101 Photoshop Tips
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            This step by step tutorial on how to scale your business with Photoshop proves to be of immense importance for every marketer. Now use this package with WriterArc BizDrive to get an unfair advantage over your competitors.
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mt20 mt-md0">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox3">
                        </div>
                    </div>
                </div>
                <!-- bonus4 -->
                <div class="col-12 col-md-12  mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus4.webp" class="img-fluid d-block bonus-size" alt="Bonus4">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Home Studio On A Budget
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Learn how to setup a mini-studio in your home or office, and start churning out pro-quality videos, podcasts & music. When it combines the awesome powers of WriterArc BizDrive, it scales your business to the next level.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-6 mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox4">
                        </div>
                    </div>
                </div>
                <!-- bonus5 -->
                <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 px-md15">
                        <img src="https://cdn.oppyo.com/launches/writerarc/common_assets/images/bonus5.webp" class="img-fluid d-block bonus-size" alt="Bonus5">
                        <div class="f-22 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                            Author Preneur Mandate
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                            Powerful info packed video course enables you to write your own book and market it in just 30 days. Now what’s the point waiting; Just use this useful package with WriterArc BizDrive to scale your business to new heights.
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="mt20 mt-md0">
                            <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/bonusbox.webp" class="img-fluid mx-auto d-block" alt="BonusBox5">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->
    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-md-12">
                    <div class="f-md-40 f-28 w700 lh140 text-center black-clr">
                        Get Instant Access To<span class="blue-clr"> WriterArc <br class="d-none d-md-block">
                            BizDrive </span> Now
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt30 mt-md50">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div class="tbbg2-text mt0 mt-md0 w700 f-md-40 f-28 white-clr text-center">
                                WriterArc BizDrive
                            </div>
                        </div>
                        <div class="text-center">
                            <ul class="f-18 f-md-20 w400 lh140 text-center vgreytick mb0 text-capitalize">
                                <li class="odd">
                                    Store Your Media Securely & Share Faster with Your Clients-Future upgrade
                                </li>
                                <li class="even">
                                    Engage Maximum Audience with <span class="w700">Royalty Free Stock Images &
                                        Videos- Pixabay & Pexels Integration </span>
                                </li>
                                <li class="odd">
                                    Share Files on Elegant, Brandable & <span class="w700">SEO optimized Sharing
                                        Pages</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Advanced share page analytics</span> to have a complete insight on How Your Files Are Doing.
                                </li>
                                <li class="odd">
                                    <span class="w700">Single Dashboard</span> to Manage All Type of Files. No Need to Buy Multiple Apps
                                </li>
                                <li class="even">
                                    <span class="w700">Preview</span> and Download the Files Before Sharing
                                </li>
                                <li class="odd">
                                    Manage files in folders easily & share entire folder with clients or team members with <span class="w700">Folder Management Feature</span>
                                </li>
                                <li class="even">
                                    <span class="w700">Capture Unlimited Leads</span> & Unlimited Audience from Share Pages
                                </li>
                                <li class="odd">
                                    Get Maximum Visitor Engagement with <span class="w700">Like/Dislike Option on
                                        Share Pages</span>
                                </li>
                                <li class="even">
                                    Get <span class="w700">Full Text Search and Filters</span> to Locate Files Instantly
                                </li>
                                <li class="odd">
                                    Get <span class="w700">Online Back-up</span> & 30 Days File recovery functionality
                                </li>
                                <li class="even">
                                    <span class="w700">Speed-Up Your Website Speed</span> with Fast Loading & Optimized Images
                                </li>
                                <li class="odd">
                                    <span class="w700">Manage & Share Multiple Files</span> and save your time.
                                </li>
                                <li class="even">
                                    Keep your files & valuable data secured with <span class="w700">Unbreakable File
                                        Security</span> using SSL & OTP
                                </li>
                                <li class="odd">
                                    <span class="w700">Folders Effortlessly Enabled Login</span>
                                </li>
                                <li class="even">
                                    Access Files Anytime, Anywhere with <span class="w700">Business cloud</span>
                                </li>
                                <li class="odd">
                                    Fetch & Sync Valuable Data Effortlessly with <span class="w700">Google Drive,
                                        One Drive & Dropbox Integration</span>
                                </li>
                            </ul>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme relative mb-md15">
                            <div class="row">
                                <div class="col-md-6 mx-auto">
                                    <a href="https://www.jvzoo.com/b/106971/386764/2"><img src="https://i.jvzoo.com/106971/386764/2" alt="WriterArc BizDrive" border="0" class="img-fluid d-block mx-auto" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50 text-center">
                    *Your storage is dependent on the plan you've purchased on the front end offer. In case you need more storage, you can anytime upgrade with help of our customer success team.
                </div>
                <div class="col-12 mt20 mt-md40 thanks-button text-center">
                    <a class="f-18 f-md-20 lh140 w500 text-capitalize" href="https://www.writerarc.com/premium-membership">
                        No thanks - I Don't Want To Store, Manage And Deliver Unlimited Website Images, PDFs, Audios &
                        Any Marketing File At A Huge Discounted Price. I know that WriterArc BizDrive can increase
                        my profits & I duly realize that I won't get this offer again. Please take me to the next step
                        to get access to my purchase.
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->
     <!--Footer Section Start -->
     <div class="footer-section">
        <div class="container">
           <div class="row">
              <div class="col-12 text-center">
                 <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                    <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                    <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                    <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                    <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                    <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                    <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                    <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                    <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                    <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                    <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                 </svg>
                 <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                 <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
                 <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                    <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                    <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                 </ul>
              </div>
           </div>
        </div>
     </div>
    <!--Footer Section End -->

          <!-- timer --->
          <?php
          if ($now < $exp_date) {
          
          ?>
       <script type="text/javascript">
          // Count down milliseconds = server_end - server_now = client_end - client_now
          var server_end = <?php echo $exp_date; ?> * 1000;
          var server_now = <?php echo time(); ?> * 1000;
          var client_now = new Date().getTime();
          var end = server_end - server_now + client_now; // this is the real end time
          
          var noob = $('.countdown').length;
          
          var _second = 1000;
          var _minute = _second * 60;
          var _hour = _minute * 60;
          var _day = _hour * 24
          var timer;
          
          function showRemaining() {
              var now = new Date();
              var distance = end - now;
              if (distance < 0) {
                  clearInterval(timer);
                  document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                  return;
              }
          
              var days = Math.floor(distance / _day);
              var hours = Math.floor((distance % _day) / _hour);
              var minutes = Math.floor((distance % _hour) / _minute);
              var seconds = Math.floor((distance % _minute) / _second);
              if (days < 10) {
                  days = "0" + days;
              }
              if (hours < 10) {
                  hours = "0" + hours;
              }
              if (minutes < 10) {
                  minutes = "0" + minutes;
              }
              if (seconds < 10) {
                  seconds = "0" + seconds;
              }
              var i;
              var countdown = document.getElementsByClassName('countdown');
              for (i = 0; i < noob; i++) {
                  countdown[i].innerHTML = '';
          
                  if (days) {
                      countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + days + '</span><br><span class="f-18 w600">Days</span> </div>';
                  }
          
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + hours + '</span><br><span class="f-18 w600">Hours</span> </div>';
          
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + minutes + '</span><br><span class="f-18 w600">Mins</span> </div>';
          
                  countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">' + seconds + '</span><br><span class="f-18 w600">Sec</span> </div>';
              }
          
          }
          timer = setInterval(showRemaining, 1000);
       </script>
       <?php
          } else {
          echo "Times Up";
          }
          ?>
       <!--- timer end-->

       <script>
    document.getElementById('vid').play();
</script>
</body>

</html>
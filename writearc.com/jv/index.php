<html>
   <head>
      <title>JV Page - WriterArc JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="WriterArc | JV">
      <meta name="description" content="A Ground Breaking A.I. Based Content Creator">
      <meta name="keywords" content="WriterArc">
      <meta property="og:image" content="https://www.writerarc.com/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="WriterArc | JV">
      <meta property="og:description" content="A Ground Breaking A.I. Based Content Creator">
      <meta property="og:image" content="https://www.writerarc.com/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="WriterArc | JV">
      <meta property="twitter:description" content="A Ground Breaking A.I. Based Content Creator">
      <meta property="twitter:image" content="https://www.writerarc.com/jv/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/writerarc/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/jquery.bxslider.min.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script src="assets/css/jquery.bxslider.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'September 16 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                     <ul class="leader-ul f-16 f-md-18">
                        <li>
                           <a href="https://docs.google.com/document/d/1V0gNW2MHCRRT2jY7E2giA8Vzmnn00ed6/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">JV Doc</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="https://docs.google.com/document/d/1LPXNuidxjf512JfbrmruFy-6xOWnzE5b/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="#funnel">Funnel</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/386602" target="_blank" class="affiliate-link-btn ml-md15 mt15 mt-md0">Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-24 w500 white-clr lh140">
                     Join Us On September 16th For Something EXTRAORDINARY! 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt15 f-md-50 f-28 w500 text-center lh140 white-clr">
                  Disruptive A.I. Technology <br class="d-none d-md-block"> <span class="gradient-orange w700">Creates Any Type of Marketing Content</span> <br class="d-none d-md-block"> for Any Local or Online Niche Hand Free
               </div>
               <!-- <div class="col-12 mt20 mt-md20 f-md-22 f-20 w700 white-clr text-center">
                  <ul class="bxslider text f-28 f-md-50">
                     <li class="text-clr1">
                        <div class="text">Blogs & Blog Idea</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Landing Page & Website Copy</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Product Description</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Taglines & Headlines</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Facebook & Instagram Ads</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Googel Ads</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">SEO Titles</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Profile Bio</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">YouTube Description</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Social Media Post</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Story Plot</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Video Scipts</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Marketing Emails</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Testimonials & Reviews</div>
                     </li>
                     <li class="text-clr1">
                        <div class="text">Interview Question … and Much More </div>
                     </li>
                  </ul>
               </div> -->
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-8 col-12">
                        <img src="assets/images/webinar.webp" class="d-block img-fluid mx-auto" style="border-radius:20px;">
                        <!-- <div class="responsive-video">
                           <iframe class="embed-responsive-item" src="https://launch.oppyo.com/video/embed/pmqohwdz8n" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                              box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div> -->
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="calendar-wrap">
                           <div class="calendar-wrap-inline">
                              <!-- <div class="text-uppercase f-26 f-md-32 w500">Thursday</div> -->
                              <div class="f-28 f-md-33 w700">September, 2022</div>
                              <div class="date mt15">16 <span>th</span></div>
                              <div class="text-uppercase f-20 f-md-24 w500 mt15">At 11:00 AM EST</div>
                              <img src="assets/images/prelaunch.webp" alt="Prelaunch Date" class="img-fluid mx-auto d-block mt10">
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="countdown counter-black mt15">
                           <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg col-12">01</span><br><span class="f-18 w600">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">16</span><br><span class="f-18 w600">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">59</span><br><span class="f-18 w600">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-38 f-md-45 lh100 timerbg">37</span><br><span class="f-18 w600">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="marketing-section">
         <div class="container mycontainer">
            <div class="row">
               <div class="col-12">
                  <div class="welcome-box">
                     <div class="row">
                        <div class="col-12 f-28 f-md-50 w700 text-center white-clr lh140">
                           Welcome to The Future of Content Creation</span>
                        </div>
                     </div>
                     <div class="row align-items-center mt20 mt-md50">
                        <div class="col-md-6 offset-md-1 pr-md0 pl-md50">
                           <img src="assets/images/robot.webp" class="img-fluid d-none robot-img">
                           <img src="assets/images/google-ads.webp" alt="Google Ads" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="col-md-5">
                           <ul class="fe-list f-md-18 f-16 w400 mb0 mt-md0 mt20 pl0 pl-md20 lh140 white-clr">
                              <li>Create Highly Quality, Human Like Content for Any Local or Online Business</li>
                              <li>Super Powerfull A.I. Engine to Create Professional & Top Converting in 35+ Use Case</li>
                              <li>Create Customized Use Case As Per Your Need</li>
                              <li>Workes in 35+ Languages with 22+ Different Tones</li>
                              <li>Social Media & WordPress Integration to Automate Your Content Sharing and Get Max Traffic</li>
                              <li>Download Your Content in Word or PDF Format</li>
                              <li>Built-In Text to Speech Converter to Create Voiceover from Any Text</li>
                              <li>Create Voiceover In 150+ Human & AI Voices And 30+ Languages.</li>
                              <li>Securely Store & Share Unlimited Files & Media With Integrated Storage Drive.</li>
                              <li>Commercial License Included To Build On Incredible Income Helping Clients</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="form-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
                     <div class="f-md-34 f-24 d-block mb0 lh140 w500 text-center black-clr1">
                        <span class="w700 f-28 f-md-60 lh140">Subscribe To Our JV List</span> <br class="d-none d-md-block"> and Be The First to Know Our Special Contest, Events and Discounts
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt15 mt-md50">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1469108899" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6333977" />
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_13cba59411aa74c4196b0299dd6d53e1" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1469108899" class="af-form">
                              <div id="af-body-1469108899" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-4">
                                    <label class="previewLabel" for="awf_field-114651755" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb15 mb-md15 input-type">
                                       <input id="awf_field-114651755" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb15 mb-md25  col-md-4">
                                    <label class="previewLabel" for="awf_field-114651756" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-114651756" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-4">
                                    <input name="submit" class="submit f-20 f-md-26 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jCxsnIwMHBycnA==" alt="" /></div>
                        </form>
                        <!-- Aweber Form Code -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md50 p-md0">
                  <div class="f-24 f-md-38 w600 text-center lh140 black-clr1">
                     Grab Your JVZoo Affiliate Link to Jump on This <br class="d-none d-md-block">
                     Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="assets/images/jvzoo.png" class="img-fluid d-block mx-auto" alt="Jvzoo"/>
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/386602" target="_blank" class="f-22 f-md-26 w600 mx-auto">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 white-clr text-center w700 lh140">
                     This Exciting Launch Event Is Divided Into 2 Phases 
                  </div>
               </div>
               <div class="col-12 mt-md60 mt30">
                  <div class="row align-items-center g-0">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/phase1.webp" class="img-fluid d-block mx-auto" alt="Phase" />
                     </div>
                     <div class="col-md-7 col-12 p-md0 mt-md0 mt20 order-md-1 white-clr">
                        <div class="prelaunch-shape1">
                           <div class="f-md-34 f-22 lh140 w500">
                              To Make You Max Commissions 
                           </div>
                           <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md30 mt20 pl20 lh140">
                              <li>All Leads Are Hardcoded</li>
                              <li>Exciting $2000 Pre-Launch Contest</li>
                              <li>We'll Re-Market Your Leads Heavily</li>
                              <li>Pitch Bundle Offer on webinars.</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="row mt-md50 mt40 align-items-center g-0">
                     <div class="col-md-5 col-12 z-index1">
                        <img src="assets/images/phase2.webp" class="img-fluid d-block mx-auto" alt="Phase"/>
                     </div>
                     <div class="col-md-7 col-12 mt-md0 mt20 white-clr">
                        <div class="prelaunch-shape2">
                           <div class="f-md-34 f-22 lh140 w500">
                              Big Opening Contest & Bundle Offer
                           </div>
                           <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md30 mt20 pl20 lh140">
                              <li> High in Demand Product with Top Conversion </li>
                              <li> Deep Funnel to Make You Double Digit EPCs </li>
                              <li> Earn up to $415/Sale </li>
                              <li> Huge $12K JV Prizes </li>
                              <li> We'll Re-Market Your Visitors Heavily </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="awesome-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-50 f-28 lh140 w600 white-clr heading-design">
                              Hello Awesome JV’s
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md30">
                           <div class="row align-items-center">
                              <div class="col-12 col-md-6">
                                 <div class="f-18 f-md-20 lh140 w400 black-clr1">
                                    It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner  Atul Pareek (Internet Marketer & Product Creator).
                                    <br><br>
                                    We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates.
                                    <br><br>
                                    With the combined experience of 25+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for all your video marketing needs under one dashboard.
                                    <br><br>
                                    Check out the incredible features of this amazing technology that will blow away your mind. And we guarantee that this offer will convert like Hot Cakes starting from 16th September'22 at 11:00 AM EST! Get Ready!!
                                 </div>
                              </div>
                              <div class="col-md-6 col-12 text-center" data-aos="fade-left">
                                 <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto mt20" alt="Amit Pareek">
                                 
                                 <img src="assets/images/atul-parrek.webp" class="img-fluid d-block mx-auto mt20" alt="Atul Pareek">
                              </div>
                           </div>
                        </div>
                        <div class="col-12 mt-md50 mt20">
                           <div class="awesome-feature-shape">
                              <div class="row ">
                                 <div class="col-12 f-md-24 f-20 lh140 w600 text-center mb20 mb-md30">
                                    Also, here are some stats from our previous launches:
                                 </div>
                                 <div class="col-12 col-md-6 f-16 f-md-18 lh140 w400">
                                    <ul class="awesome-list">
                                       <li>Over 150 Pick of The Day Awards</li>
                                       <li>Over $4Mn in Affiliate Sales for Partners</li>
                                    </ul>
                                 </div>
                                 <div class="col-12 col-md-6 f-16 f-md-18 lh140 w400">
                                    <ul class="awesome-list">
                                       <li>Under Top 10 JVZoo Affiliate (High Performance Leader)</li>
                                       <li>In Top-10 of JVZoo Top Selling Vendors</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w600 lh140 white-clr">
                  Presenting…
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="max-height:100px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 f-md-32 f-22 mt-md50 mt20 w600 text-center white-clr lh140">
                  Breath-Taking A.I. Technique to Create Any Type of Marketing 
                  Content for Any Local or Online Niche with Just a Click of a Button
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation" alt="Product Box">
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- STEP SECTION START -->
      <div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w700 lh130 black-clr1">
                  The WriterArc’s A.I. Engine will Accomplish all 
                  the Marketing Content Needs in 3 Easy Steps
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones and Language for which you want to create content or  create your own use case. The WriterArc A.I. is trained by highly professional copywriters to create top converting & plagiarism-free content.  
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -120px; top: 320px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md110">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name and a short description with keywords  to create relative and effective content 
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 290px; left: -260px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                  Step #3  
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                  Create
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     WriterArc will automatically create professional and 100% plagiarism-free content of your need in just a few seconds.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
         </div>
      </div>
      <!-- STEP SECTION END -->
      <div class="feature-seca">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="features-headline f-28 f-md-50 w700 lh140 white-clr text-md-start">
                     <span>For A Great Marketing Strategy, you <br class="d-none d-md-block"> need Great Content and WriterArc <br class="d-none d-md-block"> will turn out to be a Game Changer…</span>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90" data-aos="fade-left">
               <div class="col-md-6 col-12">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block" alt="Left Arrow">
                  <div class="f-md-32 f-22 w600 lh140">
                     Create Fresh & SEO-Friendly Copy Every Time 
                  </div>
                  <div class="f-md-22 f-20 w400 lh140 mt10">
                     WriterArc A.I. Engine Generate 100% Fresh and Unique Copy which is search engine optimized and easy to rank.   
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block" alt="SEO-Friendly">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  <div class="f-md-32 f-22 w600 lh140">
                     Create Content for Any Local or Online Niche in 35+ Use Cases
                  </div>
                  <div class="f-md-22 f-20 w400 lh140 mt10">
                     WriterArc has tons of predefined Local and Online business categories to create content  for or you can create your own category. You can create content in for any business in 35+ use cases or can create your own use case as well.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block" alt="Create Content">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  <div class="f-md-32 f-22 w600 lh140">
                     35+ Languages & 22+ Tones
                  </div>
                  <div class="f-md-22 f-20 w400 lh140 mt10">
                     WriterArc can write copy in 35+ languages and over 22+ tones of voice available to fit your business need. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block" alt="Languages">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  <div class="f-md-32 f-22 w600 lh140">
                     Social Media & WordPress Integration to Automate Content Sharing 
                  </div>
                  <div class="f-md-22 f-20 w400 lh140 mt10">
                     You can directly share the content generated by WriterArc to Social Media or to your WordPress Blog/Website with the help of integration. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  <div class="f-md-32 f-22 w600 lh140">
                     Built-in Text to Speech Converter
                  </div>
                  <div class="f-md-22 f-20 w400 lh140 mt10">
                     WriterArc is loaded with inbuilt A.I. based text to speech converter to create voiceover, podcasts, or even eBooks in 150+ Human & A.I. voices in 30+ languages. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="feature-list">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 lh140 text-center white-clr">
                  Here are Some More Amazing Features
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="feature-list-container">
                     <div class="row">
                        <div class="col-12 col-md-4 mt-md30">
                           <div class="feature-list-box">
                              <img src="assets/images/af1.webp" class="img-fluid mx-auto d-block">
                              <p class="description">Download Content in Word or PDF</p>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt30">
                           <div class="feature-list-box">
                              <img src="assets/images/af2.webp" class="img-fluid mx-auto d-block">
                              <p class="description">Cloud Management</p>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt30">
                           <div class="feature-list-box">
                              <img src="assets/images/af3.webp" class="img-fluid mx-auto d-block">
                              <p class="description">Client Management</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12 col-md-4 mt30">
                           <div class="feature-list-box">
                              <img src="assets/images/af4.webp" class="img-fluid mx-auto d-block">
                              <p class="description">Team Management</p>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt30">
                           <div class="feature-list-box">
                              <img src="assets/images/af5.webp" class="img-fluid mx-auto d-block">
                              <p class="description">White Label License</p>
                           </div>
                        </div>
                        <div class="col-12 col-md-4 mt30">
                           <div class="feature-list-box">
                              <img src="assets/images/af6.webp" class="img-fluid mx-auto d-block">
                              <p class="description">Commercial Use License</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-60 f-28 w700 lh140 text-center gradient-orange">
                     Watch The Demo
                  </div>
                  <div class="f-md-34 f-24 w600 lh140 text-center">
                     Discover How Easy & Powerful It Is
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
                  <img src="assets/images/demo-video-poster.png" class="img-fluid d-block mx-auto">
                  <!--<div class="responsive-video">
                     <iframe src="https://Academiyo.dotcompal.com/video/embed/yaa3911t5h" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>-->
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w700 text-center lh140">
                     Here is the List of Potential Use Cases WriterArc can be Used For.
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt20">
               <div class="col-md-2 col-6">
                  <div class="list-shape">
                     <img src="assets/images/pn1.webp" class="img-fluid mx-auto d-block" alt="Business Coaches">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Business Coaches
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt0">
                  <div class="list-shape">
                     <img src="assets/images/pn2.webp" class="img-fluid mx-auto d-block" alt="Courses Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Courses Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn3.webp" class="img-fluid mx-auto d-block" alt="Architect">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Architect
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn4.webp" class="img-fluid mx-auto d-block" alt="Hotels">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Hotels
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn5.webp" class="img-fluid mx-auto d-block" alt="E-Book Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Book Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn6.webp" class="img-fluid mx-auto d-block" alt="Yoga Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Yoga Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn7.webp" class="img-fluid mx-auto d-block" alt="Painters & Decorators">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Painters & Decorators
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn8.webp" class="img-fluid mx-auto d-block" alt="Schools">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Schools
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn9.webp" class="img-fluid mx-auto d-block" alt="Freelancers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Freelancers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn10.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Tutors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn11.webp" class="img-fluid mx-auto d-block" alt="Computer Repair">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Computer Repair
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn12.webp" class="img-fluid mx-auto d-block" alt="Gym">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Gym
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn13.webp" class="img-fluid mx-auto d-block" alt="Real Estate">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Real Estate
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn14.webp" class="img-fluid mx-auto d-block" alt="Dance & Music Trainers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dance & Music Trainers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn15.webp" class="img-fluid mx-auto d-block" alt="Home Security">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Security
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn16.webp" class="img-fluid mx-auto d-block" alt="Dentists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dentists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn17.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn18.webp" class="img-fluid mx-auto d-block" alt="Cooking Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Cooking Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn19.webp" class="img-fluid mx-auto d-block" alt="Lock Smith">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lock Smith
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn20.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lawyers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn21.webp" class="img-fluid mx-auto d-block" alt="Digital Product Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Product Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn22.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Affiliate Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn23.webp" class="img-fluid mx-auto d-block" alt="Veterinarians">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Veterinarians
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn24.webp" class="img-fluid mx-auto d-block" alt="Financial Adviser">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Financial Adviser
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn25.webp" class="img-fluid mx-auto d-block" alt="YouTubers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     YouTubers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn26.webp" class="img-fluid mx-auto d-block" alt="Digital Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn27.webp" class="img-fluid mx-auto d-block" alt="Florist">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Florist
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn28.webp" class="img-fluid mx-auto d-block" alt="Dermatologists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dermatologists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn29.webp" class="img-fluid mx-auto d-block" alt="Car &Taxi Services">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Car &Taxi Services
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn30.webp" class="img-fluid mx-auto d-block" alt="Plumbers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Plumbers
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn31.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Sports Clubs
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn32.webp" class="img-fluid mx-auto d-block" alt="Counsellors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Counsellors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn33.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Garage Owners
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn34.webp" class="img-fluid mx-auto d-block" alt="Carpenter">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Carpenter
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn35.webp" class="img-fluid mx-auto d-block" alt="Bars & Restaurants">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Bars & Restaurants
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn36.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Chiropractors
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 text-center lh140 white-clr">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md50">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto" alt="Funnel">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-38 f-24 w700 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-50 f-28 w700 text-center gradient-orange lh140">
                  $10000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center black-clr1">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-20 f-18 w500 lh140 mt20 text-center black-clr1">
                  Contest Runs From 12th Sept’22, 10:00 AM EST to 16th Sept’22 10:00 AM EST
               </div>
               <div class="col-12 f-md-20 f-18 w500 lh140 mt10 text-center black-clr1">
                  (Get Flat <span class="w700">$0.50c</span> For Every Lead You Send for <span class="w700">Pre-Launch Webinar</span>)
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/happy-man.webp" class="img-fluid d-none d-md-block mx-auto" alt="Contest">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="assets/images/contest-img2.webp" class="img-fluid d-block mx-auto" alt="Contest">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt-md40 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads) 
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-65 f-28 lh140 w800 white-clr feature-shape">
                     $8,000 launch contest
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-8 mx-auto">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <img src="assets/images/prize-img1.webp" alt="Prize Image" class="mx-auto d-block img-fluid">
                     </div>
                     <div class="col-md-6 position-relative">
                        <img src="assets/images/prize-img2.webp" alt="Prize Image" class="mx-auto d-block img-fluid">
                        <img src="assets/images/prize-img3.webp" alt="Prize Image" class="mx-auto d-none d-md-block img-fluid" style="position:absolute; bottom: -90px; right: -330px;">
                     </div>
                  </div>
               </div>

               <!-- <div class="col-md-4">
               <img src="assets/images/prize-img3.webp" alt="Prize Image" class="mx-auto d-block img-fluid">
               </div> -->
            </div>
         </div>
      </div>
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 lh140 w700 black-clr">
                     Our Solid Track Record of <br class="d-lg-block d-none"> <span class="black-clr"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.png" class="img-fluid d-block mx-auto" alt="Product Logo">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md30 black-clr">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs. <br><br>  
                  So, if you have a top-notch product with top conversions and that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.png" class="img-fluid d-block mx-auto" alt="Logo">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="contact-section">
         <div class="container">
            <div class="contact-container">
               <div class="row">
                  <div class="col-12">
                     <div class="f-md-50 f-28 w700 lh140 text-center white-clr">
                        Have any Query? Contact us Anytime
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md50 ">
                  <div class="col-md-6 col-12 text-center">
                     <div class="contact-shape">
                        <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto" alt="Amit Pareek">
                        <div class="f-22 f-md-32 w700 mt15 mt-md20 lh140 text-center white-clr">
                           Dr Amit Pareek
                        </div>
                        <div class="f-15 w500 lh140 text-center white-clr">
                           (Techpreneur & Marketer)
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                           <a href="skype:amit.pareek77" class="link-text mr20">
                              <div class="col-12 ">
                                 <img src="assets/images/skype.png" class="center-block" alt="Skype">
                              </div>
                           </a>
                           <a href="http://facebook.com/Dr.AmitPareek" class="link-text">
                           <img src="assets/images/am-fb.png" class="center-block" alt="Facebook">
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-md-4 col-12 text-center mt20 mt-md0">
                     <div class="contact-shape">
                        <img src="assets/images/achal-goswami-sir.webp" class="img-fluid d-block mx-auto" alt="Achal Goswami">
                        <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                           Achal Goswami
                        </div>
                        <div class="f-15 w500 lh140 text-center white-clr" >
                           (Entrepreneur & Internet Marketer)
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                           <a href="skype:live:.cid.78f368e20e6d5afa" class="link-text mr20">
                              <div class="col-12 ">
                                 <img src="assets/images/skype.png" class="center-block" alt="Skype">
                              </div>
                           </a>
                           <a href="https://www.facebook.com/dcp.ambassador.achal/" class="link-text">
                           <img src="assets/images/am-fb.png" class="center-block" alt="Facebook">
                           </a>
                        </div>
                     </div>
                  </div> -->
                  <div class="col-md-6 col-12 text-center mt20 mt-md0">
                     <div class="contact-shape">
                        <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto" alt="Atul Pareek">
                        <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                           Atul Pareek
                        </div>
                        <div class="f-15 w500  lh140 text-center white-clr">
                           (Entrepreneur & Product Creator)
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                           <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text mr20">
                              <div class="col-12 ">
                                 <img src="assets/images/skype.png" class="center-block" alt="Skype">
                              </div>
                           </a>
                           <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text">
                           <img src="assets/images/am-fb.png" class="center-block" alt="Facebook">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div>
      <div class="terms-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center">
                  Affiliate Promotions Terms & Conditions 
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below: 
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list pl0 m0 f-16 lh140 w400">
                     <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No
                        exceptions will be entertained. 
                     </li>
                     <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed
                        from our system with immediate effect. 
                     </li>
                     <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
                     <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © WriterArc</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://writerarc.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + days + '</span><br><span class="f-18 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + hours + '</span><br><span class="f-18 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-38 f-md-45 timerbg">' + minutes + '</span><br><span class="f-18 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-38 f-md-45 timerbg">' + seconds + '</span><br><span class="f-18 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
      <script id="rendered-js" >
         $('.bxslider.text').bxSlider({
           mode: 'vertical',
           pager: false,
           controls: false,
           infiniteLoop: true,
           auto: true,
           speed: 300,
           pause: 2000 });
                
      </script>
   </body>
</html>
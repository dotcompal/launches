<!Doctype html>
<html>

<head>
    <title>Prelaunch Special Webinar | Trendio</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
    <meta name="title" content="Prelaunch Special Webinar | Trendio">
	<meta name="description" content="Prelaunch Special Webinar | Trendio">
	<meta name="keywords" content="Prelaunch Special Webinar | Trendio">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

    <!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Prelaunch Special Webinar | Trendio">
	<meta property="og:description" content="Prelaunch Special Webinar | Trendio">
	<meta property="og:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="Prelaunch Special Webinar | Trendio">
	<meta property="twitter:description" content="Prelaunch Special Webinar | Trendio">
	<meta property="twitter:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
	<link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
    <script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>
	<script src="../common_assets/js/bootstrap.min.js"></script>   
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css" >
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css">
	
    <!-- End -->
<script>
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
var first = getUrlVars()["aid"];
//alert(first);
document.addEventListener('DOMContentLoaded', (event) => {
document.getElementById('awf_field_aid').setAttribute('value',first);
})
//document.getElementById('myField').value = first;
</script>



</head>

<body>



<!-- New Timer  Start-->
      <?php
         $date = 'May 04 2022 9:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->

<div id="templateBody">
	<div id="space-parent">
		<!-- Header Section Start -->
		<div class="space-section banner-section">
			<div class="container">
				<div class="row">	
					<div class="col-12 text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                        <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{opacity:0.3;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                        .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                        .st5{fill:#023047;}
                        </style>
                        <g>
                        <g>
                        <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                        </g>
                        <g>
                        <g>
                        <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                        </g>
                        </g>
                        </g>
                        <g class="st1">
                        <g>
                        
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                        
                        <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                        
                        <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                        </g>
                        </g>
                        <g>
                        <g>
                        <g>
                        <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                        </g>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                        </g>
                        </g>
                        <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                        <g>
                        <g>
                        <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                        </g>
                        </g>
                        <g>
                        <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                        </g>
                        </svg> </div>
                <div class="col-12 mt20 mt-md40 text-center px15 px-md0">
                    <div class="f-20 f-md-24 w600 lh150 highlihgt-heading yellow-clr">
                      <span>Register for Trendio Pre-Launch Webinar & Get an Assured Gift + 10 Free Licenses</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md30">
                    <h1 class="f-md-45 f-28 w500 text-center black-clr lh150">
                      Discover A Breakthrough AI Technology <span class="w700 blue-clr">That Automatically Creates Beautiful, Traffic Pulling Websites packed With Trendy Content & Videos </span>In Any Niche
                    </h1>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-22 f-18 w600 black-clr text-center lh150">
                  Discover how I have sold over $9 Mn in products & earned $4Mn in affiliate commissions <br class="d-none d-md-block"> by doing it all from one single dashboard and without any tech hassles.
                </div>
					
					<div class="col-12">
						<div class="row">
							<div class="col-12 col-md-7 mt20 mt-md70">
								<!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
									<iframe src="https://Coursova.dotcompal.com/video/embed/9bo529lat3" style="width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
								</div> -->
								<div class="text-center">
									<img src="assets/images/video-bg.png" class="img-fluid">
								</div>
							</div>
							<div class="col-12 col-md-5 mt20 mt-md40">
								<div class="register-form-back">
									<div class="">
										<div class="f-md-30 f-24 w700 lh140 text-center black-clr spartan-font" editabletype="text" style="z-index: 10;">
											Register for Free Training
										</div>
										<div class="f-md-19 f-20 text-center w400 lh150 mb15 mt8" editabletype="text" style="z-index: 10;">
										  <span class="w700">On 4<sup>th</sup> MAY, 10 AM EST</span> <br>
										  Book Your Seat now (Limited to 100 Users)
										</div>
									</div>
									<!-- Form Code -->
									<div class="">

								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
								  <div style="display: none;">
									 <input type="hidden" name="meta_web_form_id" value="1061882643" />
									 <input type="hidden" name="meta_split_id" value="" />
									 <input type="hidden" name="listname" value="awlist6259348" />
									 <input type="hidden" name="redirect" value="https://www.trendio.biz/prelaunch-thankyou" id="redirect_d52fdc3bab8c7943771224e816e1ea27" />
									 <input type="hidden" name="meta_adtracking" value="Prelaunch_Web_Form" />
									 <input type="hidden" name="meta_message" value="1" />
									 <input type="hidden" name="meta_required" value="name,email" />
									 <input type="hidden" name="meta_tooltip" value="" />
								  </div>
								  <div id="af-form-1061882643" class="af-form">
									 <div id="af-body-1061882643" class="af-body af-standards">
										<div class="af-element">
										   <label class="previewLabel" for="awf_field-114007080"></label>
										   <div class="af-textWrap ">
											  <div class="input-type" style="z-index: 11;">
												 <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
												 <input id="awf_field-114007080" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
											  </div>
										   </div>
										   <div class="af-clear bottom-margin"></div>
										</div>
										<div class="af-element">
										   <label class="previewLabel" for="awf_field-114007081"> </label>
										   <div class="af-textWrap">
											  <div class="input-type" style="z-index: 11;">
												 <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
												 <input class="text form-control custom-input input-field" id="awf_field-114007081" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
											  </div>
										   </div>
										   <div class="af-clear bottom-margin"></div>
										</div>
										<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
									   
										 <div class="af-element" style="padding:0px;">
										   <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
										   <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
										   <div class="af-clear"></div>
										</div>
										
									   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn f-22 f-md-26" style="padding:0;">
										   <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
										   <div class="af-clear bottom-margin"></div>
										</div>
									 </div>
								  </div>
								<div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jAxsjBwcTGwszA==" alt="" /></div>
							   </form>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Section End -->

		<!-- Prize Section Start -->
		<div class="space-section prize-section">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-5 mx-auto mb-sm50 mb20 timer-block text-center p-md0 px15">
						<div class="f-md-22 f-20 w500 lh140 px0 px-sm15">
							Hurry Up! Free Offer Going Away in… 
						</div>
						<div class="col-12 mt-md20 mt20">
							<div class="countdown counter-black">
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
								<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr mt15">
						Register for FREE Training and WIN Gifts
						</div>
					</div>
					<div class="col-12 col-md-8 mx-auto">
						<div class="f-md-22 f-20 w600 lh150 mt10 mt-md15">
						My 10+ Years of experience is packaged in this state-of-art 1 hour webinar where you’ll learn:
						</div>
						<div class="f-md-20 f-18 w400 lh150 p0 mt20 mt-md40">
							<ul class="list-style lh150">
                                <li>How I sold over $9Mn in digital products online and <span class="w600">you can follow the same to build a profitable business online.</span></li>
                            <li>
     A Sneak-Peak of Trendio to Create Beautiful, Traffic Pulling Websites Packed With Trendy Content & Videos In Any Niche<span class="w600">                        –From One Platform</span>
							</li>
							 <li>During This Launch Special Deal, <span class="w600">Get All Benefits at Limited Low One-Time-Fee.</span> </li>
							 <li>Lastly, everyone who attend this session will get an assured gift from us plus <span class="w600">we are giving 10 licenses of our Premium solution - Trendio</span></li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-10 mx-auto mt25 mt-md85" id="form">
						<div class="auto_responder_form inp-phold formbg col-12">
							<div class="f-20 f-md-22 w400 text-center lh180 black-clr">
								Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
								<span class="f-22 f-md-32 w700 text-center black-clr" contenteditable="false">4<sup contenteditable="false">th</sup> MAY, 10 AM EST(100 Seats Only)</span>
							</div>
							<div class="row">
								<div class="col-12 col-md-6 mx-auto  p-md0 px15 my20 timer-block text-center">
									<div class="col-md-12 col-12 f-md-22 f-20 w500 lh140 px0 px-sm15">
										Hurry Up! Free Offer Going Away in… 
									</div>
									<div class="col-12 col-md-10 col-md-offset-0 p0 mt-md20 mt20">
										<div class="countdown counter-black">
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
											<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
											<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
										</div>
									</div>
								</div>
							</div>
							<!-- Aweber Form Code -->
							<div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
								
								<!-- Aweber Form Code -->
								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="1061882643" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6259348" />
                                 <input type="hidden" name="redirect" value="https://www.trendio.biz/prelaunch-thankyou" id="redirect_d52fdc3bab8c7943771224e816e1ea27" />
                                 <input type="hidden" name="meta_adtracking" value="Tredio_Prelaunch" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-1061882643" class="af-form">
                                 <div id="af-body-1061882643" class="af-body af-standards">
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114007080"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-114007080" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114007081"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-114007081" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <!--<div class="af-element">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>-->
									
								   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn1 f-22 f-md-21 width-form" style="padding-top:5px;">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                            <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jAxsjBwcTGwszA==" alt="" /></div>
                           </form>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Prize Section End -->	
		 <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 912.39 250" style="max-height:50px;"><defs><style>.cls-1{fill:#ffb703;}.cls-2{opacity:0.3;}.cls-3{fill:url(#linear-gradient);}.cls-4{fill:url(#linear-gradient-2);}.cls-5{fill:url(#linear-gradient-3);}.cls-6{fill:#fff;}</style><linearGradient id="linear-gradient" x1="17.43" y1="209.55" x2="18.97" y2="209.55" gradientUnits="userSpaceOnUse"><stop offset="0"/><stop offset="1" stop-color="#333"/></linearGradient><linearGradient id="linear-gradient-2" x1="18.97" y1="181.92" x2="91.57" y2="181.92" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="104.55" y1="132.74" x2="333.22" y2="132.74" xlink:href="#linear-gradient"/></defs><path class="cls-1" d="M19,210.68l70-69.1,2.57,17.67,10.86,74.65,2.12-2L264.73,84,270,79.2l6.8-6.27L282,68.07l-25.41-27.5-12,11.1-5.25,4.86-108,99.67-2.12,2L118,80.59,115.4,62.91,0,176.79l.94,2.15c.59,1.35,1.2,2.67,1.82,4,.49,1,1,2.06,1.5,3.09.37.76.76,1.54,1.17,2.31q3.86,7.53,8.57,14.93c.57.89,1.13,1.76,1.73,2.65Z" transform="translate(0 0.38)"/><polygon class="cls-1" points="328.54 0 322.97 17.91 295.28 106.97 279.91 90.32 269.97 79.57 254.51 62.82 244.59 52.05 232.01 38.45 219.28 24.65 328.54 0"/><g class="cls-2"><path class="cls-3" d="M17.43,208.42,19,210.68l-1.09-1.58s0,0,0,0l-.17-.26Z" transform="translate(0 0.38)"/><path class="cls-4" d="M89,141.58l2.57,17.67-63.83,63c-1.18-1.45-2.34-2.91-3.46-4.36q-2.76-3.6-5.31-7.22Z" transform="translate(0 0.38)"/><polygon class="cls-5" points="333.22 15.6 299.96 122.57 274.65 95.17 107.11 249.87 104.55 232.31 264.73 84.41 269.97 79.57 279.91 90.32 295.28 106.97 322.97 17.91 333.22 15.6"/></g><path class="cls-6" d="M229.46,241.56q-18.42,8.07-33.58,8.06a45.93,45.93,0,0,1-25.7-7.34c-23.05-15.07-23.72-46.18-23.72-49.67V61.29h32.2v30h39.27v32.2H178.66v69c.07,4.46,1.88,18.11,9.2,22.83,5.51,3.55,15.7,2.38,28.68-3.3Z" transform="translate(0 0.38)"/><path class="cls-6" d="M250.4,163.91c0-16-.24-27.35-1-38h25.9l1,22.51h1c5.81-16.7,19.6-25.17,32.19-25.17a30.9,30.9,0,0,1,7,.73v28.07a41.61,41.61,0,0,0-8.71-1c-14.28,0-24,9.2-26.63,22.51a51.55,51.55,0,0,0-1,9.2v61H250.4Z" transform="translate(0 0.38)"/><path class="cls-6" d="M459.28,161c0-13.56-.24-24.93-1-35.1h26.14l1.45,17.67h.73c5.08-9.2,17.91-20.33,37.51-20.33,20.58,0,41.88,13.31,41.88,50.59v69.95H536.25V177.23c0-16.95-6.29-29.78-22.51-29.78-11.86,0-20.09,8.48-23.23,17.43a30.26,30.26,0,0,0-1.21,9.68v69.23h-30Z" transform="translate(0 0.38)"/><path class="cls-6" d="M706.41,71.93v138.7c0,12.1.48,25.17,1,33.16H680.75l-1.21-18.64h-.48c-7,13.07-21.3,21.3-38.49,21.3-28.07,0-50.34-24-50.34-60.27-.24-39.45,24.44-62.93,52.76-62.93,16.22,0,27.84,6.78,33.16,15.49h.49V71.93ZM676.64,175.05a41.49,41.49,0,0,0-.73-7.75c-2.66-11.62-12.1-21.06-25.65-21.06-19.13,0-29.78,16.95-29.78,39,0,21.55,10.65,37.28,29.53,37.28,12.11,0,22.76-8.23,25.66-21.06a33,33,0,0,0,1-8.71Z" transform="translate(0 0.38)"/><path class="cls-1" d="M769.68,89.57c-.11-.52-.24-1-.39-1.54s-.25-.77-.39-1.14-.25-.64-.39-.94-.25-.55-.39-.81-.25-.47-.38-.7-.26-.43-.39-.63-.26-.38-.39-.57l-.39-.51-.39-.48c-.12-.15-.25-.29-.39-.44l-.38-.41L765,81l-.39-.35-.39-.33-.39-.3-.39-.28c-.13-.09-.25-.19-.39-.27s-.25-.17-.38-.25L762.3,79l-.39-.22-.39-.2-.39-.19-.39-.17-.38-.17-.39-.15-.39-.14-.39-.13-.39-.12-.39-.11-.38-.1-.39-.09-.39-.08-.39-.08-.39-.06-.39-.06-.38,0-.39,0c-.13,0-.26,0-.39,0l-.39,0-.39,0h-1.55l-.39,0-.39,0-.39,0-.39,0-.38,0-.39.06-.39.06-.39.08-.39.08-.39.1-.38.1-.39.11-.39.12-.39.13-.39.15-.39.15-.38.16-.39.18-.39.19-.39.2-.39.22-.39.23-.39.25-.38.26-.39.28-.39.3-.39.33-.39.34-.39.37c-.13.13-.26.26-.38.4a5.22,5.22,0,0,0-.39.43l-.39.45-.39.5-.39.56-.39.61c-.13.22-.26.45-.38.68s-.27.5-.39.76-.27.6-.39.91-.27.73-.39,1.1-.28,1-.39,1.49a17.76,17.76,0,0,0-.39,3.69,17.56,17.56,0,0,0,.39,3.67q.16.75.39,1.47t.39,1.11c.12.3.25.59.39.89s.25.51.39.76.25.45.38.67l.39.61.39.55.39.5c.12.15.26.3.39.45s.25.28.39.42.25.27.38.39l.39.37.39.34.39.31q.19.16.39.3c.12.1.26.19.39.28l.38.26.39.24.39.22.39.22.39.19.39.19.39.17.38.16c.13.06.26.1.39.15l.39.14.39.12c.13,0,.26.09.39.12l.39.1.38.11.39.08.39.08.39.07.39.06.39,0,.38,0,.39,0,.39,0,.39,0h1.94l.39,0,.39,0,.39,0,.39,0,.38,0,.39-.06.39-.07.39-.07.39-.08.39-.1.38-.09.39-.11.39-.12.39-.13.39-.14.39-.15.38-.16.39-.17.39-.19.39-.19.39-.21.39-.23.38-.24c.14-.08.26-.17.39-.26l.39-.27.39-.29.39-.32.39-.33.39-.37a4.68,4.68,0,0,0,.38-.39,5,5,0,0,0,.39-.41l.39-.45.39-.5.39-.54.39-.6c.13-.22.26-.44.38-.67s.27-.5.39-.76.27-.58.39-.88.27-.73.39-1.1.28-1,.39-1.46a17.64,17.64,0,0,0,.39-3.67A21.05,21.05,0,0,0,769.68,89.57Z" transform="translate(0 0.38)"/><rect class="cls-6" x="738.36" y="126.29" width="30.01" height="117.88"/><path class="cls-6" d="M912.39,183.76c0,43.33-30.5,62.69-60.51,62.69-33.41,0-59.06-23-59.06-60.75,0-38.73,25.41-62.45,61-62.45C888.91,123.25,912.39,147.94,912.39,183.76ZM823.56,185c0,22.75,11.13,39.94,29.29,39.94,16.94,0,28.8-16.7,28.8-40.42,0-18.4-8.23-39.46-28.56-39.46C832,145,823.56,165.36,823.56,185Z" transform="translate(0 0.38)"/><path class="cls-6" d="M386,243.14a60.38,60.38,0,0,1-8.87-.66A59.61,59.61,0,1,1,433.76,148l7.61,10.25-55.9,41.45-15.21-20.51,33.4-24.77a34.07,34.07,0,1,0,12.23,45.22l22.51,12.08a59.72,59.72,0,0,1-52.4,31.4Z" transform="translate(0 0.38)"/></svg>
                    <div editabletype="text" class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © Trendio</div>
                    <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.biz/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.biz/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.biz/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.bizlegal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.biz/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://trendio.biz/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
	</div>
</div>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-1061882643').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1061882643")) {
                document.getElementById("af-form-1061882643").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1061882643")) {
                document.getElementById("af-body-1061882643").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1061882643")) {
                document.getElementById("af-header-1061882643").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1061882643")) {
                document.getElementById("af-footer-1061882643").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->

<!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
</body>

</html>
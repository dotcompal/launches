<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Trendio Special Bonuses">
    <meta name="description" content="Trendio Special Bonuses">
    <meta name="keywords" content="Trendio Special Bonuses">
    <meta property="og:image" content="https://www.trendio.biz/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Trendio Special Bonuses">
    <meta property="og:description" content="Trendio Special Bonuses">
    <meta property="og:image" content="https://www.trendio.biz/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Trendio Special Bonuses">
    <meta property="twitter:description" content="Trendio Special Bonuses">
    <meta property="twitter:image" content="https://www.trendio.biz/special-bonus/thumbnail.png">

	<title>Trendio Special Bonuses</title>
	<!-- Shortcut Icon  -->
	<link rel="shortcut icon" href="assets/images/favicon.png"/>
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style-bottom.css">
	<link rel="stylesheet" type="text/css" href="assets/css/m-style.css">
	<!-- Font Family CDN Load Links -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Javascript File Load --><script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>
	<script src="../common_assets/js/bootstrap.min.js"></script>
	<!-- Buy Button Lazy load Script -->



	<script>
	$(document).ready(function() {
	/* Every time the window is scrolled ... */
	$(window).scroll(function() {
		/* Check the location of each desired element */
		$('.hideme').each(function(i) {
			var bottom_of_object = $(this).offset().top + $(this).outerHeight();
			var bottom_of_window = $(window).scrollTop() + $(window).height();
			/* If the object is completely visible in the window, fade it it */
			if ((bottom_of_window - bottom_of_object) > -200) {
				$(this).animate({
					'opacity': '1'
				}, 300);
			}
		});
	});
	});
	</script>
	<!-- Smooth Scrolling Script -->
	<script>
	$(document).ready(function() {
	// Add smooth scrolling to all links
	$("a").on('click', function(event) {
	
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();
	
			// Store hash
			var hash = this.hash;
	
			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function() {
	
				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
		} // End if
	});
	});
	</script>
	
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TS8PQQR');</script>
<!-- End Google Tag Manager -->

</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS8PQQR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<!-- New Timer  Start-->
	<?php
		$date = 'May 05 2022 10:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
		?>
	<?php
		} else {
			echo "Times Up";
		}
		?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://cutt.ly/pGSa7D6';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>



	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 black-clr d-block d-md-flex align-items-center justify-content-center">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp; <span class=" w-155 mt15 mt-md0">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
<style type="text/css">
.st0{fill:#FFFFFF;}
.st1{opacity:0.3;}
.st2{fill:url(#SVGID_1_);}
.st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
.st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
.st5{fill:#023047;}
</style>
<g>
<g>
<path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
</g>
<g>
<g>
<polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
</g>
</g>
</g>
<g class="st1">
<g>

<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>

<linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>

<linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
</g>
</g>
<g>
<g>
<g>
<path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
</g>
</g>
</g>
<g>
<g>
<path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
</g>
</g>
<path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
<g>
<g>
<rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
</g>
</g>
<g>
<g>
<path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
</g>
</g>
<g>
<path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
</g>
</svg></span>
                    	</div>
                    </div>

               
					<div class="col-12 mt20 mt-md50 text-center px15 px-md0">
                  <div class="f-20 f-md-24 w600 lh150 highlihgt-heading yellow-clr">
                    <span>Grab My 20 Exclusive Bonuses Before the Deal Ends… </span>
                  </div>
               </div>
                    <div class="mt20 mt-md40 p0">
						<div class="">
							<div class="f-md-45 f-28 w600 text-center black-clr lh150 line-center worksans">
							 Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content & Videos from Just One Keyword</span> in 60 Seconds... </div>
						</div>
                    </div>

                    <div class="text-center mt20 mt-sm30 p0">
                        <div class="f-md-22 f-18 lh160 w600 black-clr">
							Watch My Quick Review Video
						</div>
                    </div>

                </div>
            </div>

            <div class="row mt20 mt-md30">
                <div class="col-12 col-md-10 mx-auto">
					<!-- <img src="assets/images/product-box.png" class="img-fluid mx-auto d-block"> -->
                    <div class="video-frame">
						<div class="responsive-video">
							<iframe class="embed-responsive-item" src="https://trendio.dotcompal.com/video/embed/vz8ovfpse2" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
						</div>
                    </div> 
                </div>
            </div>
        </div>
	</div>
	<div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-10 mx-auto ">
                    <div class="justify-content-between d-flex">
                        <img src="assets/images/sep-line.png" class="img-fluid d-block">
                        <img src="assets/images/sep-line.png" class="img-fluid d-block">
                    </div>
                </div>
                <div class="col-12">
				<div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
				<div class="row">
                            <div class="col-12 col-md-6">
                                <ul class="list-head pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
<li>Create Beautiful, Traffic Pulling Sites With <span class="w600">Trendy Content &amp; Videos In 3 Simple Steps</span></li>
<li><span class="w600">Built-In Traffic Generating System</span> To Drive Unlimited Search, Viral And Social Traffic </li>
<li><span class="w600">Use AI To Put Most Profitable Links</span> On Your Websites </li>
<li><span class="w600">Legally Use Other’s Trending Content To Generate Profits-</span> Works In Any Niche Or TOPIC </li>
<li><span class="w600">1-Click Social Media Automation –</span> People ONLY WANT TRENDY Topics These Days To Click. </li>
<li><span class="w600">100% SEO Friendly Websites</span> And Built-In Remarketing System </li>
<li>Complete Newbie Friendly.<span class="w600"> No Prior Experience And Technical Skills Needed</span></li>                      

                                </ul>
                            </div>
                            <div class="col-12 col-md-6">
                                <ul class="list-head pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
<li><span class="w600">Set And Forget System With Single Keyword –</span> Set Rules To Find &amp; Publish Trending Posts </li> 
<li><span class="w600">Automatically Translate Your Sites In 15+ Languages</span> According To Geo-Location For More Traffic </li> 
<li><span class="w600">Integration With Major Platforms</span> Including Autoresponders And Social Media Apps </li> 
<li><span class="w600">In-Built Content Spinner</span> To Make Your Content Fresh And More Engaging </li> 
<li><span class="w600">A-Z Complete Video Training</span> Is Included To Make It Easier For You</li>  
<li><span class="w600">Limited-Time Special Bonuses Worth $2255</span> If You Buy Today </li> 
<li class="w600">FREE COMMERCIAL LICENSE TO SERVE YOUR CLIENTS &amp; BUILD AN INCREDIBLE INCOME </li> 

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md70">
                <!-- CTA Btn Section Start -->
      <!-- CTA Button Section Start -->

			<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr">
					Use Coupon Code <span class="w700 red-clr">“trendioearly”</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->

	<!-- CTA Button Section End   -->
            </div>
        </div>
    </div>
	<div class="no-installation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-38 f-24 w500 lh160 text-capitalize text-center black-clr">
                        Instantly Create Traffic Pulling Websites <br class="d-none d-md-block"> Loaded With Trendy Content & Videos
                        <br> <span class="w700 blue-clr f-28 f-md-50 text-uppercase">In Just 3 SIMPLE Steps…</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md60">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 1
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt20 mt-md30 text-center text-uppercase">
									INSERT KEYWORD 
                                    </div>
                                    <img src="assets/images/step-1.png" alt="" class="img-fluid mx-auto d-block mt20 mt-md50">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
									To begin, just enter a keyword to find trending content in seconds.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 2
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt10 text-center text-uppercase">
									CHOOSE TRENDY VIDEOS & POSTS 
                                    </div>
                                    <img src="assets/images/step-2.png" alt="" class="img-fluid mx-auto d-block mt20">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
                                        Now find trending videos & content & use them to monetize the trend with best offer or affiliate links
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 3
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt10 text-center text-uppercase">
                                        Sit Back, Relax&amp; Profit
                                    </div>
                                    <img src="assets/images/step-3.png" alt="" class="img-fluid mx-auto d-block mt20">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
                                        Now, just sit back and relax while Trendio works for you 24x7
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mx-auto mt20 mt-md70">
                    <div class="row">
                        <div class="col-12 col-md-4 col-md-4">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Download/Installation
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Prior Knowledge
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                100% Beginners Friendly
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 text-center black-clr lh150">
						In just a few clicks, you can drive endless free traffic and convert it into passive commissions, ad profits & sales for you on autopilot.
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="thatsall-section">
        <div class="container">		
            <div class="shape-testi1">
                <div class="row">
				<div class="col-12">
                    <img src="assets/images/arrow.png" class="img-fluid d-block mx-auto vert-move minus-top2">
                </div>
                    <div class="col-12 col-md-10 mx-auto text-center">
                        <div class="f-30 f-md-50 w700 white-clr">Let's Face It</div>
                    </div>

                    <div class="col-12 f-md-80 f-28 w700 black-clr lh140 text-center">
						Old & Boring Content <br class="">Is NOT Working Today...
                    </div>
					


                </div>
            </div>
            <div class="row">
								<div class="col-12 text-center f-18 w400 white-clr lh150 mt20 mt-md20">
                 They are not working on social media, not on YouTube and not even worth publishing on your own website. And that’s the reason why you don’t get FREE traffic, lose sales &amp; commission and must think of PAID traffic methods my friend. 
               </div>
			   <div class="col-12 text-center f-18 w400 white-clr lh150 mt20 mt-md50">
               But before revealing our secret. Let me ask you a question!
               </div>
                <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt20">
                    How many times have you watched these trendy videos <br class="d-none d-md-block"> in your niche?
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md30">
                <div class="col-md-6">
                    <img src="./assets/images/v1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md0">
                    <img src="./assets/images/v2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md50">
                    <img src="./assets/images/v3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md50">
                    <img src="./assets/images/v4.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>

            <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt40 mt-md80">
				
				How many times have you clicked & read these<br class="d-none d-md-block">
amazing headlines?

            </div>

            <div class="row align-items-center mt20 mt-md30">
                <div class="col-md-7">
                    <img src="./assets/images/g1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-5 mt20 mt-md0">
                    <img src="./assets/images/g2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>


            <div class="row mt-md50 mt20 align-items-center">
                     <div class="col-12 text-center f-md-32 f-22 w500 white-clr lh150 white-clr lh150 mt20 mt-md50">
                 We all have clicked & read such viral post while surfing online regularly. <span class="under">People only want trending videos, content, news & articles.</span> This is working like a miracle 
               </div>
			   
			   <div class="col-12 f-md-38 f-28 w700 yellow-clr lh150 text-center mt20 mt-md70">
                 Trending Content Is The WINNER In 2022 & Beyond... 
               </div>
               <div class="col-12 text-center f-18 w400 white-clr lh150 mt10">
                 The idea is simple, the more trending videos, content, news & articles you have, more traffic you get, more leads and commissions you generate. 
               </div>
            </div>
        </div>
    </div>
	
	<div class="still-there-section">
        <div class="container">
            <div class="row">
			
                <div class="col-12">
                    <div class="f-28 f-md-45 w700 lh150 text-center  black-clr">
                        See How Much People Are Making Using The POWER of Trending Content & Videos
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
                <div class="col-8 mx-auto col-md-4">
                    <img src="assets/images/ryan.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-8 mt30 mt-md0">
                    <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start">
                        <span class="w600">Here’s “Ryan’s World” – </span> A 10-Year-Old Kid that has over 31.9M Subscribers with whooping <span class="w600">Net Worth of $32M... </span>
                    </div>
                    <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start mt20">
                        Yes! You heard me right... He is just 10-year-kid but making BIG.
                    </div>
                    <img src="assets/images/left-arrow.png" class="img-fluid d-none d-md-block mt10">
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
                <div class="col-8 mx-auto col-md-5 order-md-2">
                    <img src="assets/images/gary-vaynerchuk.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-7 mt30 mt-md0 order-md-1">
                    <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">
                        Even Pro marketers are bidding heavily on trendy content:
                    </div>
                    <div class="f-md-28 f-24 lh150 w400 text-center text-md-start mt20">
                        <span class="w600">Mr. Gary Vaynerchuk,</span> an entrepreneur and business Guru who’s also known as the first wine guru has <span class="w600">Net Worth of $200M+</span>
                    </div>
                    <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block mt10 ms-md-auto">
                </div>
            </div>
        </div>
    </div>

 <!-- Suport Section Start -->
    <div class="stats-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <div class="f-md-32 f-24 w500 lh150 black-clr text-center ">
                        And Now You Will Be Shocked to Know How Much These
                        <br><span class="black-clr f-md-45 f-28 w700 d-block mt10 mt-md15 ">Viral Content Sites Make Online!</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 ">
                    <div class="row no-gutters ">
                        <!-- first -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder top-first ">
                                <img src="assets/images/buzzfeed.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    5 Billion Traffic<br>
                                    <span class="green-clr w900">$300M </span>Revenue
                                </div>
                            </div>
                        </div>
                        <!-- second -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder ">
                                <img src="assets/images/upworthy.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    5 Million Traffic<br>
                                    <span class="green-clr w900">$10M </span>Revenue
                                </div>
                            </div>
                        </div>
                        <!-- third -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-b boxborder ">
                                <img src="assets/images/wittyfeed.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    1 Million Traffic<br>
                                    <span class="green-clr w900">$10M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- fourth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/viralnova.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    1 Million Traffic<br>
                                    <span class="green-clr w900">$100M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- fifth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/techcrunch.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    25 Million Traffic<br>
                                    <span class="green-clr w900">$25M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- sixth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder ">
                                <img src="assets/images/wp.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    200 Million Traffic<br>
                                    <span class="green-clr w900">$100M</span> Revenue
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row mt30 mt-md80">
                <div class="col-12 text-center">
                    <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                        <div class="f-30 f-md-50 w700 lh150 text-center white-clr skew-normal">
                            Impressive, right?
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md20">
                <div class="col-12  f-22 f-md-28 lh150 w500 text-center black-clr lh150">
                    So, it can be safely stated that…<br>
                    <span class="f-24 f-md-36 w600 mt25 d-block">
Trending Content, Videos, News & Articles Is The BIGGEST, Traffic And Income Opportunity Right Now!
</span>
                </div>
            </div>
        </div>
    </div>
    <!-- Suport Section End -->
	<div class="cta-btn-section white">
		<div class="container">
		  <div class="row">
			
			<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr">
					Use Coupon Code <span class="w700 red-clr">“trendioearly”</span> for an Additional <span class="w700 red-clr">11% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
			</div>
		</div>
	</div>

   <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="prdly-pres f-md-24 f-24 w600 black-clr lh150">
                        Proudly Presenting...
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:120px;color: #023047;" xml:space="preserve">
<style type="text/css">
.st0{fill:#FFFFFF;}
.st1{opacity:0.3;}
.st2{fill:url(#SVGID_1_);}
.st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
.st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
.st5{fill:#023047;}
</style>
<g>
<g>
<path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
</g>
<g>
<g>
<polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
</g>
</g>
</g>
<g class="st1">
<g>

<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>

<linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>

<linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
</g>
</g>
<g>
<g>
<g>
<path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
</g>
</g>
</g>
<g>
<g>
<path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
</g>
</g>
<path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
<g>
<g>
<rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
</g>
</g>
<g>
<g>
<path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
</g>
</g>
<g>
<path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
</g>
</svg>
                </div>
                <div class="f-md-24 f-22 w600 black-clr lh150 col-12 mt-md50 mt20 text-center">
                     Breakthrough A.I Technology That Automatically Creates Beautiful, <span class="w700 blue-clr">Traffic Pulling Websites Packed With Trendy Content & Videos</span> In Any Niche From Any Keyword In Next 60 Seconds
                </div>
                <div class="col-12 mt-md30 mt20">
                    <img src="assets/images/proudly.png" class="img-fluid d-block mx-auto">
                </div>
            </div>
        </div>
    </div>

	<div class="testimonial-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 f-md-45 f-28 lh150 w700 text-center black-clr">
                    And Here's What Some More<br class="d-none d-md-block"> Happy Users Say About Trendio
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t1.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                    <img src="./assets/images/t2.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t3.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                    <img src="./assets/images/t4.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
            </div>
        </div>
    </div>
	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700">When You Purchase Trendio, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.png" class="img-fluid mx-auto d-block">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">Trendio Website Manager Software</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
Manage your website easily & make the most from your visitors with this software. It has everything you need to manage with ease & convert random website visitors into lifetime happy customers.
</li><li>
Use this bonus to manage your traffic pulling websites created with Trendio to get best results.
</li>						
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
					   <div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus2.png" class="img-fluid mx-auto d-block">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md0">
							  Trendio Artificial Intelligence In Digital Marketing
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								
								<li>Artificial Intelligence & machine learning have taken over digital marketing completely & marketers are leaving no stone unturned to use it for boosting their online business.
</li>
								<li>So, make the best use of this useful bonus pack with Trendio to create websites that understand your customers needs with the best use of Artificial Intelligence.
</li>
													  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Monetizing And Utilizing Your Website
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
<li>Are you aware, more than 50% consumers make online purchases with a website. So, if you’re not making best use of your website, then you are heading in the wrong direction.</li>
<li>With this ebook, you will learn several strategies and processes that can be resourceful when it comes to making money through your websites created with Trendio.
</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus4.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Market Site Template V22
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
<li>							  If you are an affiliate marketer, blogger or an online business owner, having a high-converting marketing minisite template to promote a product online is essential.
</li><li>
But, if you are not good at doing web design then you might end up hiring someone else & wasting tons of money.
</li><li>
With this product, you’re getting is a marketing minisite template that you can use on your marketing campaigns & get best results in a cost-effective manner.
</li></ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Affiliate List Pro
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Affiliate marketing is one of the best ways to have a cool passive income without creating any product or service.
</li><li>
With this bonus, we’re giving you the power to use affiliate marketing to its best potential & insert your own affiliate links inside websites created with Trendio to boost your commissions hands down.</li>
							</ul>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 yellow-clr">“trendioearly”</span> for an Additional <span class="w700 yellow-clr">11% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center cta-link-btn1">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #6</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus6.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Text To Speech Converter

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Effective communication is one of the best ways to grab your audience attention & get them hooked to your offers.
</li>
								<li>
So, checkout this useful bonus pack of text to speech convertor that readily converts and audio file into engaging text & helps to share your marketing message with globally scattered audience.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #7</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus7.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Instant Squeeze Builder
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
Squeeze pages are the most powerful tool to attract audience to subscribe into your list. But most marketers face challenges in creating a squeeze page for their email marketing campaign.</li>			
								<li>
Now, all you need to do is create engaging websites loaded with trendy content & videos with Trendio & boost their results using squeeze pages created with this tool.
</li>			
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #8</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus8.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Writing Tips
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>There’s no denying the fact that creating good marketing content is of prime importance for every success hungry marketer.
</li>
								<li>So, in order to help you get these benefits, checkout this bonus page to create super engaging marketing content & get visitors hooked to websites created with Trendio.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #9</div>
					</div>
				</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
						<div class="col-md-5 col-12">
							<img src="assets/images/bonus9.png" class="img-fluid mx-auto d-block ">
						</div>
						<div class="col-md-7 col-12 mt20 mt-md0">
							<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							Trendio Password Saver
							</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Managing multiple affiliate accounts can be a big prove for most marketers as this is a cumbersome & time eating process.</li>
								<li>Fortunately, this bonus pack takes away all the headache & helps to save multiple details including login name, password, login link, cloaked link, payout info etc.
</li>
<li>So, use this package along with Trendio to get best results for your business.</li>
							</ul>
						</div>
					</div>
				</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #10</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus10.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Adword Annihilation
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>AdWords need no further introduction & business owners use them in a variety of ways to boost their profits.</li>
								<li>With this never offered before bonus, you'll discover how to design profitable AdWords campaigns that are scalable & how to choose your keywords, how to create your ads, how to start bidding and more.
</li>
<li>When combined with Trendio, the results you get will truly outclass your expectations.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 yellow-clr">“trendioearly”</span> for an Additional <span class="w700 yellow-clr">11% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center cta-link-btn1">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #11</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus11.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
					  		Trendio Marketing Secret For Every Niche
					  		</div>
					 		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Know the ins & outs of Niche marketing and use them to reach out easily to your targeted audience without depending on any marketing gurus.</li>
								<li>This package consists of valuable information & when used wisely with the stunning niche websites created with Trendio, give unmatched benefits for your business.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #12</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus12.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
					  		Trendio Traffic And Lead Fast Track
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Driving traffic & leads is one of the biggest concerns for every success hungry marketer.</li>
								<li>In this 5-part video course you will discover why you should use a ClixSense for advertising. You will learn how to set a campaign on this platform. Also, there are great tips on how to increase your conversions.</li>
								<li>When used on websites created with Trendio, it gives best results in a cost-effective manner.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #13</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus13.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Email Marketing Success
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>It’s no secret that Email marketing yields 3800% ROI for every dollar spent.</li>
								<li>Keeping this in mind, this comprehensive guide will show you how to build an email list and create successful marketing campaigns. Now, just use this package with Trendio to make the most from your email marketing campaigns in a hand’s down manner.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #14</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus14.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
					  		Trendio Online Income Streams
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Having a passive online income stream is a dream for most marketers, but hardly 1% of them are achieve to get desired results with it.</li>
								<li>To bail you out from this menace, this bonus is a must have that has proven & tested techniques to have your own easy online income stream. </li>
								<li>Use this with trending website creation prowess of Trendio & leave your competitors biting the dust forever.</li>
							  </ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->

	

	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #15</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus15.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Influencer Marketing
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>In this special report, you will learn what the top influencers are doing to secure profitable partnerships and how they set themselves up for ongoing success. </li>
								<li>This report will show you exactly how to start making money as an influencer, while increasing brand awareness and boosting engagement, all at the same time! </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 yellow-clr">“trendioearly”</span> for an Additional <span class="w700 yellow-clr">11% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center cta-link-btn1">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="assets/images/payment.png" class="img-fluid mx-auto d-block">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #16</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus16.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio YouTube Channel SEO
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>There’s no denying the fact that YouTube is one of the best sources to boost viewership on your website & get better results in a hands down manner. </li>
								<li>With this bonus, you get battle tested information to take your YouTube Channel to brand new heights. </li>				
								<li>When combined with content filled websites created with Trendio, the results you get are commendable. </li>
								  </ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	  	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #17</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus17.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio 123 Logo Kit
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>This bonus solves all those problems that every internet marketer and business owners are facing right now. </li>
								<li>Get access to 38 eye-popping, ready-made logos you can use for your next product or business! simply choose a logo, customize and you're done! </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #18</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus18.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio RPI Engine
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Is your business not as good as your customers? Do you believe that you may outrank your competitors and be at the top?  </li>
								<li>Yes, it’s completely possible now.  </li>
								<li>With this software, marketers can lift from the status they are into. This tool will give the possibilities of providing the things you need to be at the top. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #19</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				  		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus19.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Trendio Commission Gorilla Review Pack
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>If making commissions from promoting other people's products is part of your business model, successful affiliate marketers have found a way that could double or triple your earnings with the same traffic you send to the offer. </li>
								<li>Use this bonus with Trendio & take your results to the next level. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">FREE UPGRADE #20</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="assets/images/bonus20.png" class="img-fluid mx-auto d-block ">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  How To Use Social Network To Build A Trendy Site?
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Social networks have made building and widening your social contacts extremely easy. In the past, you might have had to attend numerous functions and paying for gas, food, and clothing just to show up and hand out a few business cards that later got tossed. </li>
								<li>So, use this with our latest website creation technology Trendio, & get the results you always aspired for. </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 black-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 blue-clr">$2255!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center">
                	<div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Trendio + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
				</div>
				<!-- Timer -->
				<div class="col-md-8 col-md-10 mx-auto col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="strip_footer clear mt20 mt-md40">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
				<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 912.39 250" style="max-height:50px;"><defs><style>.cls-1{fill:#ffb703;}.cls-2{opacity:0.3;}.cls-3{fill:url(#linear-gradient);}.cls-4{fill:url(#linear-gradient-2);}.cls-5{fill:url(#linear-gradient-3);}.cls-6{fill:#fff;}</style><linearGradient id="linear-gradient" x1="17.43" y1="209.55" x2="18.97" y2="209.55" gradientUnits="userSpaceOnUse"><stop offset="0"></stop><stop offset="1" stop-color="#333"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="18.97" y1="181.92" x2="91.57" y2="181.92" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="104.55" y1="132.74" x2="333.22" y2="132.74" xlink:href="#linear-gradient"></linearGradient></defs><path class="cls-1" d="M19,210.68l70-69.1,2.57,17.67,10.86,74.65,2.12-2L264.73,84,270,79.2l6.8-6.27L282,68.07l-25.41-27.5-12,11.1-5.25,4.86-108,99.67-2.12,2L118,80.59,115.4,62.91,0,176.79l.94,2.15c.59,1.35,1.2,2.67,1.82,4,.49,1,1,2.06,1.5,3.09.37.76.76,1.54,1.17,2.31q3.86,7.53,8.57,14.93c.57.89,1.13,1.76,1.73,2.65Z" transform="translate(0 0.38)"></path><polygon class="cls-1" points="328.54 0 322.97 17.91 295.28 106.97 279.91 90.32 269.97 79.57 254.51 62.82 244.59 52.05 232.01 38.45 219.28 24.65 328.54 0"></polygon><g class="cls-2"><path class="cls-3" d="M17.43,208.42,19,210.68l-1.09-1.58s0,0,0,0l-.17-.26Z" transform="translate(0 0.38)"></path><path class="cls-4" d="M89,141.58l2.57,17.67-63.83,63c-1.18-1.45-2.34-2.91-3.46-4.36q-2.76-3.6-5.31-7.22Z" transform="translate(0 0.38)"></path><polygon class="cls-5" points="333.22 15.6 299.96 122.57 274.65 95.17 107.11 249.87 104.55 232.31 264.73 84.41 269.97 79.57 279.91 90.32 295.28 106.97 322.97 17.91 333.22 15.6"></polygon></g><path class="cls-6" d="M229.46,241.56q-18.42,8.07-33.58,8.06a45.93,45.93,0,0,1-25.7-7.34c-23.05-15.07-23.72-46.18-23.72-49.67V61.29h32.2v30h39.27v32.2H178.66v69c.07,4.46,1.88,18.11,9.2,22.83,5.51,3.55,15.7,2.38,28.68-3.3Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M250.4,163.91c0-16-.24-27.35-1-38h25.9l1,22.51h1c5.81-16.7,19.6-25.17,32.19-25.17a30.9,30.9,0,0,1,7,.73v28.07a41.61,41.61,0,0,0-8.71-1c-14.28,0-24,9.2-26.63,22.51a51.55,51.55,0,0,0-1,9.2v61H250.4Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M459.28,161c0-13.56-.24-24.93-1-35.1h26.14l1.45,17.67h.73c5.08-9.2,17.91-20.33,37.51-20.33,20.58,0,41.88,13.31,41.88,50.59v69.95H536.25V177.23c0-16.95-6.29-29.78-22.51-29.78-11.86,0-20.09,8.48-23.23,17.43a30.26,30.26,0,0,0-1.21,9.68v69.23h-30Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M706.41,71.93v138.7c0,12.1.48,25.17,1,33.16H680.75l-1.21-18.64h-.48c-7,13.07-21.3,21.3-38.49,21.3-28.07,0-50.34-24-50.34-60.27-.24-39.45,24.44-62.93,52.76-62.93,16.22,0,27.84,6.78,33.16,15.49h.49V71.93ZM676.64,175.05a41.49,41.49,0,0,0-.73-7.75c-2.66-11.62-12.1-21.06-25.65-21.06-19.13,0-29.78,16.95-29.78,39,0,21.55,10.65,37.28,29.53,37.28,12.11,0,22.76-8.23,25.66-21.06a33,33,0,0,0,1-8.71Z" transform="translate(0 0.38)"></path><path class="cls-1" d="M769.68,89.57c-.11-.52-.24-1-.39-1.54s-.25-.77-.39-1.14-.25-.64-.39-.94-.25-.55-.39-.81-.25-.47-.38-.7-.26-.43-.39-.63-.26-.38-.39-.57l-.39-.51-.39-.48c-.12-.15-.25-.29-.39-.44l-.38-.41L765,81l-.39-.35-.39-.33-.39-.3-.39-.28c-.13-.09-.25-.19-.39-.27s-.25-.17-.38-.25L762.3,79l-.39-.22-.39-.2-.39-.19-.39-.17-.38-.17-.39-.15-.39-.14-.39-.13-.39-.12-.39-.11-.38-.1-.39-.09-.39-.08-.39-.08-.39-.06-.39-.06-.38,0-.39,0c-.13,0-.26,0-.39,0l-.39,0-.39,0h-1.55l-.39,0-.39,0-.39,0-.39,0-.38,0-.39.06-.39.06-.39.08-.39.08-.39.1-.38.1-.39.11-.39.12-.39.13-.39.15-.39.15-.38.16-.39.18-.39.19-.39.2-.39.22-.39.23-.39.25-.38.26-.39.28-.39.3-.39.33-.39.34-.39.37c-.13.13-.26.26-.38.4a5.22,5.22,0,0,0-.39.43l-.39.45-.39.5-.39.56-.39.61c-.13.22-.26.45-.38.68s-.27.5-.39.76-.27.6-.39.91-.27.73-.39,1.1-.28,1-.39,1.49a17.76,17.76,0,0,0-.39,3.69,17.56,17.56,0,0,0,.39,3.67q.16.75.39,1.47t.39,1.11c.12.3.25.59.39.89s.25.51.39.76.25.45.38.67l.39.61.39.55.39.5c.12.15.26.3.39.45s.25.28.39.42.25.27.38.39l.39.37.39.34.39.31q.19.16.39.3c.12.1.26.19.39.28l.38.26.39.24.39.22.39.22.39.19.39.19.39.17.38.16c.13.06.26.1.39.15l.39.14.39.12c.13,0,.26.09.39.12l.39.1.38.11.39.08.39.08.39.07.39.06.39,0,.38,0,.39,0,.39,0,.39,0h1.94l.39,0,.39,0,.39,0,.39,0,.38,0,.39-.06.39-.07.39-.07.39-.08.39-.1.38-.09.39-.11.39-.12.39-.13.39-.14.39-.15.38-.16.39-.17.39-.19.39-.19.39-.21.39-.23.38-.24c.14-.08.26-.17.39-.26l.39-.27.39-.29.39-.32.39-.33.39-.37a4.68,4.68,0,0,0,.38-.39,5,5,0,0,0,.39-.41l.39-.45.39-.5.39-.54.39-.6c.13-.22.26-.44.38-.67s.27-.5.39-.76.27-.58.39-.88.27-.73.39-1.1.28-1,.39-1.46a17.64,17.64,0,0,0,.39-3.67A21.05,21.05,0,0,0,769.68,89.57Z" transform="translate(0 0.38)"></path><rect class="cls-6" x="738.36" y="126.29" width="30.01" height="117.88"></rect><path class="cls-6" d="M912.39,183.76c0,43.33-30.5,62.69-60.51,62.69-33.41,0-59.06-23-59.06-60.75,0-38.73,25.41-62.45,61-62.45C888.91,123.25,912.39,147.94,912.39,183.76ZM823.56,185c0,22.75,11.13,39.94,29.29,39.94,16.94,0,28.8-16.7,28.8-40.42,0-18.4-8.23-39.46-28.56-39.46C832,145,823.56,165.36,823.56,185Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M386,243.14a60.38,60.38,0,0,1-8.87-.66A59.61,59.61,0,1,1,433.76,148l7.61,10.25-55.9,41.45-15.21-20.51,33.4-24.77a34.07,34.07,0,1,0,12.23,45.22l22.51,12.08a59.72,59.72,0,0,1-52.4,31.4Z" transform="translate(0 0.38)"></path></svg>
				</div>
                <div class="col-md-12 col-md-12 col-12 f-16 f-md-18 w300 mt20 lh150 white-clr text-center">
                   Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
                </div>
                <div class="col-lg-12 col-md-12 col-md-12 col-12">
					<div class="row">
						<div class="col-md-3 col-12 f-16 f-md-18 w400 w300 mt10 mt-md40 lh150 white-clr d-left-m-center">Copyright © Trendio</div>
						<div class="col-md-9 col-12 f-md-18 w300 f-16 white-clr mt10 mt-md40 xstext-center d-right-m-center">
							<a href="https://support.bizomart.com/">Contact</a>&nbsp;&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/privacy-policy.html">Privacy</a>&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/terms-of-service.html">T&amp;C</a>&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/disclaimer.html">Disclaimer</a>&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/gdpr.html">GDPR</a>&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/dmca.html">DMCA</a>&nbsp;|&nbsp;
							<a href="http://trendio.biz/legal/anti-spam.html">Anti-Spam</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop px0">	
					<div class="col-12 px0">
						<img src="assets/images/waitimg.png" class="img-fluid mx-auto d-block">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 px0">
						<div class="f-20 f-md-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="hidden-xs"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<!-- End Google Tag Manager (noscript) -->
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
		if (f.fbq) return;
		n = f.fbq = function() {
			n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq) f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
		}(window,
		document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1777584378755780');
		fbq('track', 'PageView');
	</script>
  	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
	<!-- Google Code for Remarketing Tag -->
	<!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  	<div style="display:none;">
	 	<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 748114601;
			var google_custom_params = window.google_tag_params;
			var google_remarketing_only = true;
			var google_user_id = '<unique user id>';
			/* ]]> */
	 	</script>
	 	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 	<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
			</div>
	 	</noscript>
  	</div>
	<!-- Google Code for Remarketing Tag -->
	<!-- timer --->
  	<?php
	 	if ($now < $exp_date) {
	?>

  	<script type="text/javascript">
		// Count down milliseconds = server_end - server_now = client_end - client_now
		var server_end = <?php echo $exp_date; ?> * 1000;
		var server_now = <?php echo time(); ?> * 1000;
		var client_now = new Date().getTime();
		var end = server_end - server_now + client_now; // this is the real end time
		
		var noob = $('.countdown').length;
		
		var _second = 1000;
		var _minute = _second * 60;
		var _hour = _minute * 60;
		var _day = _hour * 24
		var timer;
		
		function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
			return;
		}
		
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);
		if (days < 10) {
			days = "0" + days;
		}
		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var i;
		var countdown = document.getElementsByClassName('countdown');
		for (i = 0; i < noob; i++) {
			countdown[i].innerHTML = '';
		
			if (days) {
				countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
			}
		
			countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
		}
		
		}
		timer = setInterval(showRemaining, 1000);
  	</script>
  	<?php
	 	} else {
	 	echo "Times Up";
	 	}
	?>
  <!--- timer end-->
  

</body>
</html>

<!Doctype html>
<html>

<head>
    <title>Trendio Prelaunch</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Trendio | Prelaunch">
    <meta name="description" content="Trendio">
    <meta name="keywords" content="Trendio">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Trendio | Prelaunch">
    <meta property="og:description" content="Trendio">
    <meta property="og:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Trendio | Prelaunch">
    <meta property="twitter:description" content="Trendio">
    <meta property="twitter:image" content="https://www.trendio.biz/prelaunch/thumbnail.png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/general.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="../common_assets/js/jquery.min.js"></script>
    <script src="../common_assets/js/popper.min.js"></script>
    <script src="../common_assets/js/bootstrap.min.js"></script>

    <script>
        function getUrlVars() {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var first = getUrlVars()["aid"];
        //alert(first);
        document.addEventListener('DOMContentLoaded', (event) => {
                document.getElementById('awf_field_aid').setAttribute('value', first);
            })
            //document.getElementById('myField').value = first;
    </script>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TS8PQQR');</script>
<!-- End Google Tag Manager -->
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS8PQQR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- New Timer  Start-->
      <?php
         $date = 'May 04 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
    <!-- Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-3 text-center text-md-start">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                        <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{opacity:0.3;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                        .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                        .st5{fill:#023047;}
                        </style>
                        <g>
                        <g>
                        <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                        </g>
                        <g>
                        <g>
                        <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                        </g>
                        </g>
                        </g>
                        <g class="st1">
                        <g>
                        
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                        
                        <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                        
                        <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                        <stop offset="0" style="stop-color:#000000"></stop>
                        <stop offset="1" style="stop-color:#333333"></stop>
                        </linearGradient>
                        <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                        </g>
                        </g>
                        <g>
                        <g>
                        <g>
                        <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                        </g>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                        </g>
                        </g>
                        <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                        <g>
                        <g>
                        <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                        </g>
                        </g>
                        <g>
                        <g>
                        <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                        </g>
                        </g>
                        <g>
                        <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                        </g>
                        </svg>
                        </div>
                        <div class="col-md-9 mt20 mt-md5">
                            <ul class="leader-ul f-md-16 f-16 w400 black-clr text-md-end text-center">
                                <li><a href="#features" class="t-decoration-none">Features</a><span class="pl9 black-clr">|</span></li>
                                <li><a href="#demo" class="t-decoration-none">Demo</a></li>
                                <li class="affiliate-link-btn"><a href="#form" class="t-decoration-none ">Book Your Free Seat</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                    <div class="pre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                        <span>
                    Register for Trendio Pre-Launch Webinar & Get an Assured Gift + 10 Free Licenses</span>
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                    Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content & Videos from Just One Keyword</span> in 60 Seconds... 
                </div>
                <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                     Anyone can easily create & sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, & 100+ other niches...
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                <div class="col-md-7 col-12 min-md-video-width-left">
                    <div class="col-12 responsive-video border-video">
                                <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                            </div> 

                </div>
                <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                    <div class="key-features-bg1">
                        <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                           		<li>Works Seamlessly in Any Niche or Topic – <span class="w600">Just One Keyword</span> </li>
							<li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
							<li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
							<li>Grab More Authority, Engagement, & Leads on your Business Website </li>
							<li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers & AdSense </li>
							<li class="w600">FREE COMMERCIAL LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Section End -->
    <!-- Header Section Start -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-10 mx-auto ">
                    <div class="justify-content-between d-flex">
                        <img src="assets/images/sep-line.png" class="img-fluid d-block">
                        <img src="assets/images/sep-line.png" class="img-fluid d-block">
                    </div>
                </div>
                <div class="col-12">
                    <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <ul class="list-head pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                   <li>Create Beautiful, Traffic Pulling Sites With <span class="w600">Trendy Content & Videos In 3 Simple Steps</span></li>
<li><span class="w600">Built-In Traffic Generating System</span> To Drive Unlimited Search, Viral And Social Traffic </li>
<li><span class="w600">Use AI To Put Most Profitable Links</span> On Your Websites </li>
<li><span class="w600">Legally Use Other’s Trending Content To Generate Profits-</span> Works In Any Niche Or TOPIC </li>
<li><span class="w600">1-Click Social Media Automation –</span> People ONLY WANT TRENDY Topics These Days To Click. </li>
<li><span class="w600">100% SEO Friendly Websites</span> And Built-In Remarketing System </li>
<li>Complete Newbie Friendly.<span class="w600"> No Prior Experience And Technical Skills Needed</span></li>   

                                </ul>
                            </div>
                            <div class="col-12 col-md-6">
                                <ul class="list-head pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                   <li><span class="w600">Set And Forget System With Single Keyword –</span> Set Rules To Find & Publish Trending Posts </li> 
<li><span class="w600">Automatically Translate Your Sites In 15+ Languages</span> According To Geo-Location For More Traffic </li> 
<li><span class="w600">Integration With Major Platforms</span> Including Autoresponders And Social Media Apps </li> 
<li><span class="w600">In-Built Content Spinner</span> To Make Your Content Fresh And More Engaging </li> 
<li><span class="w600">A-Z Complete Video Training</span> Is Included To Make It Easier For You</li>  
<li><span class="w600">Limited-Time Special Bonuses Worth $2255</span> If You Buy Today </li> 
<li class="w600">FREE COMMERCIAL LICENSE TO SERVE YOUR CLIENTS & BUILD AN INCREDIBLE INCOME </li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md70">
                <!-- CTA Btn Section Start -->
                <div class="col-12 col-md-12 mx-auto" id="form">
						<div class="auto_responder_form inp-phold formbg">
							<div class="f-20 f-md-24 w500 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
								Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
								<span class="f-22 f-md-32 w700 text-center black-clr" contenteditable="false">4<sup contenteditable="false">th</sup> MAY, 10 AM EST (100 Seats Only)</span>
							</div>
							<div class="col-12 col-md-6 mx-auto my20 timer-block text-center">
								<div class="col-md-12 col-12 f-md-22 f-20 w600 lh140 px0 px-md15">
									Hurry Up! Free Offer Going Away in… 
								</div>
								<div class="col-12 col-md-10 mx-auto mt-md20 mt20">
									<div class="countdown counter-black">
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
										<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
									</div>
								</div>
							</div>
							<!-- Aweber Form Code -->
							<div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
								
								<!-- Aweber Form Code -->
								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="885892634" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6258786" />
                                 <input type="hidden" name="redirect" value="https://www.trendio.biz/prelaunch-thankyou" id="redirect_8cbadda9f3f2065956e0718e44d13a6b" />
                                 <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-885892634" class="af-form">
                                 <div id="af-body-885892634" class="af-body af-standards">
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114042796"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-114042796" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114042797"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-114042797" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <!--<div class="af-element">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>-->
									
								   <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                             <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="" /></div>
                           </form>		
							</div>
						</div>
					</div>
            </div>
        </div>
    </div>
    <!-- Header Section End -->
    <!-- Step Section End -->
    <div class="no-installation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-38 f-24 w500 lh160 text-capitalize text-center black-clr">
                        Instantly Create Traffic Pulling Websites <br class="d-none d-md-block"> Loaded With Trendy Content & Videos
                        <br> <span class="w700 blue-clr f-28 f-md-50 text-uppercase">In Just 3 SIMPLE Steps…</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md60">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 1
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt20 mt-md30 text-center text-uppercase">
                                        INSERT KEYWORD 
                                    </div>
                                    <img src="assets/images/step-1.png" alt="" class="img-fluid mx-auto d-block mt20 mt-md50">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
                                       To begin, just enter a keyword to find trending content in seconds. 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 2
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt10 text-center text-uppercase">
                                        CHOOSE TRENDY VIDEOS & POSTS 
                                    </div>
                                    <img src="assets/images/step-2.png" alt="" class="img-fluid mx-auto d-block mt20">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
                                        Now find trending videos & content & use them to monetize the trend with best offer or affiliate links
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="steps-block">
                                <div class="steps-inside">
                                    <div class="step-bg">
                                        <div class="f-26 w600 black-clr">
                                            <img src="assets/images/arrow-right.png" alt=""> STEP 3
                                        </div>
                                    </div>
                                    <div class="f-24 f-md-32 w700 white-clr lh150 mt10 text-center text-uppercase">
                                        Sit Back, Relax& Profit
                                    </div>
                                    <img src="assets/images/step-3.png" alt="" class="img-fluid mx-auto d-block mt20">
                                    <div class="f-18 w400 white-clr lh150 mt20 mt-md30 text-center">
                                        Now, just sit back and relax while Trendio works for you 24x7
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mx-auto mt20 mt-md70">
                    <div class="row">
                        <div class="col-12 col-md-4 col-md-4">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Download/Installation
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Prior Knowledge
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                100% Beginners Friendly
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 text-center black-clr lh150">
                        In just a few clicks, you can drive endless free traffic and convert it into  <br class="d-none d-md-block">passive commissions, ad profits & sales for you.
                    </div>
                </div>
				
            </div>
        </div>
    </div>
    <div class="thatsall-section">
        <div class="container">
            <div class="shape-testi1">
                <div class="row">
                    <div class="col-12 col-md-10 mx-auto text-center">
                        <div class="f-30 f-md-50 w700 white-clr">Let's Face It</div>
                    </div>

                    <div class="col-12 f-md-80 f-28 w700 black-clr lh140 text-center">
                        Old & Boring Content <br class="">Is NOT Working Today...
                    </div>



                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center f-18 w400 white-clr lh150 mt20 mt-md20">
                    They are not working on social media, not on YouTube and not even worth publishing on your own website. And that’s the reason why you don’t get FREE traffic, lose sales &amp; commission and must think of PAID traffic methods my friend.
                </div>
                <div class="col-12 text-center f-18 w400 white-clr lh150 mt20 mt-md50">
                    But before revealing our secret. Let me ask you a question!
                </div>
                <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt20">
                    How many times have you watched these trendy videos <br class="d-none d-md-block"> in your niche?
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md30">
                <div class="col-md-6">
                    <img src="./assets/images/v1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md0">
                    <img src="./assets/images/v2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md50">
                    <img src="./assets/images/v3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-6 mt20 mt-md50">
                    <img src="./assets/images/v4.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            
            </div>

            <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt40 mt-md80">

                How many times have you clicked & read these<br class="d-none d-md-block"> amazing headlines?

            </div>

            <div class="row align-items-center mt20 mt-md30">
                <div class="col-md-7">
                    <img src="./assets/images/g1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-5 mt20 mt-md0">
                    <img src="./assets/images/g2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>


            <div class="row mt-md50 mt20 align-items-center">
                <div class="col-12 text-center f-md-32 f-22 w500 white-clr lh150 white-clr lh150 mt20 mt-md50">
                    We all have clicked & read such viral post while surfing online regularly. <span class="under">People only want trending videos, content, news & articles.</span> This is working like a miracle
                </div>

                <div class="col-12 f-md-38 f-28 w700 yellow-clr lh150 text-center mt20 mt-md70">
                    Trending Content Is The WINNER In 2022 & Beyond...
                </div>
                <div class="col-12 text-center f-18 w400 white-clr lh150 mt10">
                    The idea is simple, the more trending videos, content, news & articles you have, more traffic you get, more leads and commissions you generate.
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="level-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/arrow.png" class="img-fluid d-block mx-auto vert-move minus-top">
                </div>
                <div class="col-12 mb-md60 f-28 f-md-45 w600 lh150 text-center mt20 mt-md60">
                    Trendio Is Built for Everyone Across All Levels
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon1.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w500 black-clr text-center mt20">
                        No Prior Experience <br class="d-none d-md-block"> Needed
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon2.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w500 black-clr text-center mt20">
                        Simple Yet Powerful <br class="d-none d-md-block"> Technology
                    </div>
                </div>
                <div class="col-md-4 mt-md0 mt20">
                    <img src="./assets/images/icon3.png" alt="" class="img-fluid d-block mx-auto">
                    <div class="f-md-22 f-20 w500 black-clr text-center mt20">
                        Newbie-Friendly with No <br class="d-none d-md-block"> Learning Curve
                    </div>
                </div>
            </div>

        </div>
    </div> -->

    <div class="still-there-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-45 w700 lh150 text-center  black-clr">
                        See How Much People Are Making Using The POWER of Trending Content & Videos
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
                <div class="col-8 mx-auto col-md-4">
                    <img src="assets/images/ryan.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-8 mt30 mt-md0">
                    <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start">
                        <span class="w600">Here’s “Ryan’s World” – </span> A 10-Year-Old Kid that has over 31.9M Subscribers with whooping <span class="w600">Net Worth of $32M... </span>
                    </div>
                    <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start mt20">
                        Yes! You heard me right... He is just 10-year-kid but making BIG.
                    </div>
                    <img src="assets/images/left-arrow.png" class="img-fluid d-none d-md-block mt10">
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
                <div class="col-8 mx-auto col-md-5 order-md-2">
                    <img src="assets/images/gary-vaynerchuk.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-7 mt30 mt-md0 order-md-1">
                    <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">
                        Even Pro marketers are bidding heavily on trendy content:
                    </div>
                    <div class="f-md-28 f-24 lh150 w400 text-center text-md-start mt20">
                        <span class="w600">Mr. Gary Vaynerchuk,</span> an entrepreneur and business Guru who’s also known as the first wine guru has <span class="w600">Net Worth of $200M+</span>
                    </div>
                    <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block mt10 ms-md-auto">
                </div>
            </div>
        </div>
    </div>

    <!-- Suport Section Start -->
    <div class="stats-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <div class="f-md-32 f-24 w500 lh150 black-clr text-center ">
                        And Now You Will Be Shocked to Know How Much These
                        <br><span class="black-clr f-md-45 f-28 w700 d-block mt10 mt-md15 ">Viral Content Sites Make Online!</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 ">
                    <div class="row no-gutters ">
                        <!-- first -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder top-first ">
                                <img src="assets/images/buzzfeed.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    5 Billion Traffic<br>
                                    <span class="green-clr w900">$300M </span>Revenue
                                </div>
                            </div>
                        </div>
                        <!-- second -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder ">
                                <img src="assets/images/upworthy.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    5 Million Traffic<br>
                                    <span class="green-clr w900">$10M </span>Revenue
                                </div>
                            </div>
                        </div>
                        <!-- third -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-b boxborder ">
                                <img src="assets/images/wittyfeed.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    1 Million Traffic<br>
                                    <span class="green-clr w900">$10M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- fourth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/viralnova.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    1 Million Traffic<br>
                                    <span class="green-clr w900">$100M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- fifth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/techcrunch.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    25 Million Traffic<br>
                                    <span class="green-clr w900">$25M</span> Revenue
                                </div>
                            </div>
                        </div>
                        <!-- sixth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder ">
                                <img src="assets/images/wp.png " class="img-fluid d-block mx-auto ">
                                <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                                    200 Million Traffic<br>
                                    <span class="green-clr w900">$100M</span> Revenue
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row mt30 mt-md80">
                <div class="col-12 text-center">
                    <div class="black-design1 d-flex align-items-center justify-content-center mx-auto">
                        <div class="f-30 f-md-50 w700 lh150 text-center white-clr skew-normal">
                            Impressive, right?
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md20">
                <div class="col-12  f-22 f-md-28 lh150 w500 text-center black-clr lh150">
                    So, it can be safely stated that…<br>
                    <span class="f-24 f-md-36 w600 mt25 d-block">
Trending Content, Videos, News & Articles Is The BIGGEST, Traffic And Income Opportunity Right Now!
</span>
                </div>
            </div>
        </div>
    </div>
    <!-- Suport Section End -->

    <div class="new-dcp-team-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-40 f-28 lh150 w700 black-clr heading-design text-center">
                        HEY! FELLOW MARKETER,
                    </div>
                </div>
                <div class="col-12 mt20">
                    <div class="f-md-28 f-22 w600 lh150 black-clr">
                        I am Dr. Amit Pareek along with my partners Achal Goswami & Atul Pareek!
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row">
                        <div class="col-md-4 col-12 text-center">
                            <img src="assets/images/amit-pareek.png" class="img-fluid d-block mx-auto">
                            <div class="f-22 f-md-26 w600 mt15 mt-md20 lh150 text-center black-clr">
                                Dr Amit Pareek
                            </div>
                            <div class="f-16 w500 lh150 text-center black-clr">
                                (Techpreneur &amp; Marketer)
                            </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                            <img src="assets/images/achal-goswami.png" class="img-fluid d-block mx-auto">
                            <div class="f-22 f-md-26 w600 mt15 mt-md20 lh150 text-center black-clr">
                                Achal Goswami
                            </div>
                            <div class="f-16 w500 lh150 text-center black-clr">
                                (Entrepreneur &amp; Internet Marketer)
                            </div>

                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                            <img src="assets/images/atul-parrek.png" class="img-fluid d-block mx-auto">
                            <div class="f-22 f-md-26 w600 mt15 mt-md20  lh150 text-center black-clr">
                                Atul Pareek
                            </div>
                            <div class="f-16 w500  lh150 text-center black-clr">
                                (Entrepreneur &amp; JV Manager)
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50">
                    <div class="f-18 w400 lh150 black-clr">
                        We have 20+ Years of Combined Experience in this Internet Space. Not only have we made millions of dollars ourselves, but more importantly, we have also helped thousands of people get their desired profitable business online with our tools strategy and
                        training.
                        <br><br> Over time, we’ve learnt that to be successful online, you must get your hands on a fool proof system that works again and again and again.
                    </div>
                    <div class="mt20 f-md-26 w600 f-22 lh150 black-clr">
                        And without a doubt that’s Trending Content.
                    </div>
                    <div class="mt20 f-18 w400 lh150 black-clr">
                        Till now, you might already have discovered that <span class="w600">  trending content is an untapped goldmine to tap into oceans</span> of viral traffic that’s ready to be pounced upon to boost leads, sales & profits for your
                        offers.
                    </div>
                </div>
                <!--<div class="col-12 mt20 mt-md80">
                    <div class="f-md-42 f-28 w600 lh150 text-capitalize text-center black-clr">
                        And Getting 1000s Of Visitors, Leads & Commissions Every Day Following This Proven System…
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/proof2.png" class="img-fluid d-block mx-auto">
                </div>-->
            </div>
        </div>
    </div>







    <div class="problem-bg-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-45 w500 lh150 text-center black-clr">
                        That’s A REAL Deal but
                    </div>
                    <div class="d-inline-flex flex-column relative">
                        <div class="f-30 f-md-50 w700 lh150 text-center converting-shape1 white-clr">
                            But Here’s the Problem…
                        </div>
                        <img src="assets/images/down-arrow.png" class="img-fluid d-block down-arrow">
                    </div>

                </div>
            </div>
            <div class="row mt50 mt-md70 px15 d-flex align-items-center flex-wrap">
                <div class="col-12 problme-area-shape1">
                    <div class="row d-flex align-items-center flex-wrap">

                        <div class="col-12 col-md-7">

                            <div class="f-24 f-md-32 w600 lh150 white-clr">
                                Creating Engaging Content Daily Is PAINFUL & Time-Consuming
                            </div>
                            <div class="f-18 w400 lh150 white-clr mt20 mt-md30">
                                <ul class="noneed-listing5 pl0">
                                    <li><span class="w600">You need to research, plan, and write content daily. </span> And staying up-to-date with latest niche trends needs lots of effort</li>
                                    <li><span class="w600">You Need To Be On Camera.</span> This can be a major blocker in case you are shy or introvert and have a fear that people would judge and laugh at your videos.</li>
                                    <li><span class="w600">You need to learn COMPLEX video & audio editing skills. </span> Most software are complex and difficult to learn, especially if you are a non-technical guy like us.</li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-md-5 z-index9 mt20 mt-md0">
                            <img src="assets/images/cost-you-thousands.png" class="img-fluid d-block mx-auto img-right">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
                <div class="col-12 col-md-7 mt-md50">
                    <div class="f-24 f-md-36 w600 lh150 red-clr">
                        Buying Expensive Equipment Can Leave Your Bank Accounts Dry
                    </div>
                    <div class="f-20 f-md-22 w400 lh150 black-clr mt10">
                        To even get started with first video, you need expensive equipment, like a
                    </div>
                    <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                        <ul class="noneed-listing3 pl0">
                            <li class="w600">Nice camera, </li>
                            <li class="w600">Microphone, and </li>
                            <li><span class="w600">Video-audio editing software  </span> That would cost you THOUSANDS of dollars.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <img src="assets/images/need-img.png" class="img-fluid d-block mx-auto">
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                <div class="col-12 col-md-7 order-md-2">
                    <div class="f-24 f-md-36 w600 lh150 red-clr">
                        Driving TRAFFIC To Your Content Is The BIGGEST Challenge And Costs Hundreds Of Dollars
                    </div>
                    <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                        <ul class="noneed-listing3 pl0">
                            <li>
                                <span class="w600">You need to be consistent </span> with uploading videos every day for months until you see good visitors.
                            </li>
                            <li><span class="w600">If you only have channels on YouTube, you’re 100% DEPENDING on them.</span> Many times, they Shut down accounts without any prior notice. That’s a scary stuff so you need FULL control on your business by
                                having self-growing video channels on Your own domains.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-5 order-md-1">
                    <img src="assets/images/sad.png" class="img-fluid d-block mx-auto">
                </div>
            </div>

            <div class="row">
                <div class="col-12 f-24 f-md-34 w600 lh150 text-center black-clr mt20 mt-md50">
                    That’s Why Over 80% Entrepreneurs Fail During Their First Year & Only A Handful Of People Are Successful At Making It Big.
                </div>
                <div class="col-12 f-20 f-md-20 w400 lh150 text-center black-clr mt20">
                    There’re tons of areas where marketers can face challenges which we had faced in our own success journey.
                </div>
            </div>
            <div class="row mt20 mt-md30">
                <div class="col-12 text-center">
                    <div class="f-30 f-md-60 w600 lh150 text-center butnot-shape white-clr">
                        BUT NOT ANYMORE!
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md20">
                <div class="col-12  f-20 f-md-20 lh150 w400 text-center black-clr lh150">
                    That is why we have created a software that allows you to bring massive traffic in just a couple of clicks, and you don’t have to waste any time on creating content, funnels, SEO, paid traffic, complex tools etc. once & for all.
                </div>
            </div>
        </div>
    </div>
    <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="prdly-pres f-md-24 f-24 w600 black-clr lh150">
                        Proudly Presenting...
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:120px;color: #023047;" xml:space="preserve">
<style type="text/css">
.st0{fill:#FFFFFF;}
.st1{opacity:0.3;}
.st2{fill:url(#SVGID_1_);}
.st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
.st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
.st5{fill:#023047;}
</style>
<g>
<g>
<path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
</g>
<g>
<g>
<polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
</g>
</g>
</g>
<g class="st1">
<g>

<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>

<linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>

<linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
<stop offset="0" style="stop-color:#000000"></stop>
<stop offset="1" style="stop-color:#333333"></stop>
</linearGradient>
<polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
</g>
</g>
<g>
<g>
<g>
<path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
</g>
</g>
</g>
<g>
<g>
<path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
</g>
</g>
<g>
<g>
<path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
</g>
</g>
<path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
<g>
<g>
<rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
</g>
</g>
<g>
<g>
<path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
</g>
</g>
<g>
<path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
</g>
</svg>
                </div>
                <div class="f-md-24 f-22 w600 black-clr lh150 col-12 mt-md50 mt20 text-center">
                    Breakthrough AI Technology That Automatically Creates Beautiful, <span class="w700 blue-clr">Traffic Pulling Websites Packed With Trendy Content & Videos</span> In Any Niche From Any Keyword In Next 60 Seconds
                </div>
                <div class="col-12 mt-md30 mt20">
                    <img src="assets/images/proudly.png" class="img-fluid d-block mx-auto">
                </div>
            </div>
        </div>
    </div>

    <div class="new-steps-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="f-md-45 f-28 w600 lh160 text-capitalize text-center black-clr">
                        Instantly Create Traffic Pulling Websites <br class="d-none d-md-block"> Loaded With Trendy Content & Videos<br class="d-none d-md-block">
                        <span class="w700 blue-clr f-28 f-md-50 text-uppercase">In Just 3 SIMPLE Steps…</span>
                    </div>


                </div>
                <div class=" col-12">
                    <div class="row g-2 g-md-0 d-flex align-items-center flex-wrap my20 my-md60">
                        <div class="col-12 col-md-8 mt-md0 mt20 mx-auto my-md50 new_shape11">
                            <div class="problme-area-shape">
                                <div class="step-box-new">
                                    <div class="f-16 f-md-20 w600 lh150 black-clr step-shape1">
                                        Step 1
                                    </div>
                                    <div class="f-18 f-md-25 w700 lh150 black-clr step-shape2">
                                        Insert Keyword
                                    </div>
                                </div>
                                <div class="step-content-new">
                                    <div class="f-18 w400 lh150 black-clr mb20 mb-md0">
                                         To begin, just enter a keyword to find trending  <br class="d-none b-md-block"> content in seconds.
                                    </div>
                                    <img src="assets/images/stepimg1.png" class="img-fluid d-md-none d-block mx-auto  ">
                                </div>
                            </div>
                            <img src="assets/images/stepimg1.png" class="img-fluid d-md-block d-none  mx-auto float-md-end float-md-none step-img-shap">
                        </div>
                        <div class="col-12 col-md-8 mt-md0 mt20 mx-auto my-md50 new_shape11">
                            <div class="problme-area-shape2">
                                <div class="step-box-new2">
                                    <div class="f-16 f-md-20 w600 lh150 black-clr step-shape1 step-shape11">
                                        Step 2
                                    </div>
                                    <div class="f-18 f-md-25 w700 lh150 black-clr step-shape2 step-shape22">
                                        Choose Trendy Videos & Posts
                                    </div>
                                </div>
                                <div class="step-content-new2">
                                    <div class="f-18 w400 lh160 black-clr mb20 mb-md0">
                                        Now find trending videos & content & use them to monetize the trend with best offer or affiliate links
                                    </div>
                                    <img src="assets/images/stepimg2.png" class="img-fluid d-md-none d-block mx-auto  ">
                                </div>
                            </div>
                            <img src="assets/images/stepimg2.png" class="img-fluid d-md-block d-none  mx-auto float-md-start float-md-none step-img-shap2">
                        </div>
                        <div class="col-12 col-md-8 mt-md0 mt20 mx-auto my-md50">
                            <div class="problme-area-shape last">
                                <div class="step-box-new">
                                    <div class="f-16 f-md-20 w600 lh150 black-clr step-shape1">
                                        Step 3
                                    </div>
                                    <div class="f-18 f-md-25 w700 lh150 black-clr step-shape2">
                                        Sit Back, Relax & Profit
                                    </div>
                                </div>
                                <div class="step-content-new">
                                    <div class="f-18 w400 lh150 black-clr mb20 mb-md0">
                                        To begin, just enter a keyword to publish trending<br class="d-none b-md-block"> content in seconds.
                                    </div>
                                    <img src="assets/images/stepimg3.png" class="img-fluid d-md-none d-block mx-auto  ">
                                </div>
                            </div>
                            <img src="assets/images/stepimg3.png" class="img-fluid d-md-block d-none mx-auto step-img-shap float-md-end float-md-none ">
                        </div>

                    </div>
                </div>
                <div class="col-12 col-md-12 mx-auto mt20 mt-md20">
                    <div class="row">
                        <div class="col-12 col-md-4 col-md-4">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Download/Installation
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                No Prior Knowledge
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="w600 f-20 f-md-28 black-clr text-center lh150">
                                100% Beginners Friendly
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 text-center black-clr lh150">
                        In just a few clicks, you can drive endless free traffic and convert it into passive commissions, ad profits & sales for you on autopilot.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Demo Section Start -->
    <div class="demo-section" id="demo">
        <div class="container">
            <div class="row">
                <div class="col-12 f-md-45 f-28 w700 lh150 text-center">
                    Watch The Demo To See <br class="d-none d-md-block"> How Easy It Is To Use Trendio

                </div>
                <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">                    
                    <div class="col-12 responsive-video border-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/sak7tkiak1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> 
                  </div>
                </div>
            </div>
            <div class="row mt20 mt-md70">
                <!-- CTA Btn Section Start -->
                
                        <div class="col-md-6 mx-auto col-12 text-center">
                            <a href="#form" class="cta-link-btn d-block">Book Your Free Seat</a>
                        </div>
            </div>
        </div>
    </div>
    <!--!Demo Section End--->
   <div class="testimonial-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 f-md-45 f-28 lh150 w700 text-center black-clr">
                    And Here's What Some More<br class="d-none d-md-block"> Happy Users Say About Trendio
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t1.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                    <img src="./assets/images/t2.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t3.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                    <img src="./assets/images/t4.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                </div>
            </div>
        </div>
    </div> 

    <!-- Features 1 Section Start -->
    <div class="features-one" id="features">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/arrow.png" class="img-fluid d-block mx-auto vert-move minus-top">
                </div>
                <div class="col-12 text-center mt20 mt-md70">
                    <div class="features-headline f-28 f-md-45 w700 lh150 black-clr">
                        Checkout The Ground Breaking Features
                    </div>
                    <div class="f-28 f-md-45 w600 lh150 white-clr mt-md15 mt15">
                        That Make Trendio A CUT ABOVE The Rest
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 f-18 mt20 mt-md0 ">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize white-clr text-center">
                                Create unlimited self-updating sites with DFY hot trending content
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 white-clr mt20">
                                Eye-catchy Sites packed with trendy content and engaging videos are the best way to bring visitors and convert them into customers. Trendio gives you the power to create unlimited, beautifully designed & self-updating sites with hot trending content that’s
                                fully ready to monetise the traffic with your own products or affiliate offers.
                            </div>
                        </div>
                        <div class="col-10 mx-auto mt-md40 mt20">
                            <img src="assets/images/feature1.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 1 Section End -->
    <!-- Features 1 Section Start -->
    <div class="features-two">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-2 f-18 mt20 mt-md0 ">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Find hot & trendy content in seconds for any niche and spin it to make it UNIQUE.
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15 text-md-start text-center">
                                Trendy content is the KING, and Trendio helps you to fill your sites with trendy content that grabs eyeballs. Just enter your niche related keywords and BANG!!!
                                <br><br><span class="w500">You’ll be amazed to see fresh, popular and trending content & articles that gets visitors glued right on your desktops in less than 60 seconds.</span> Even modify the content using our Ultra-Fast
                                & Super Easy Spinner and create engagement boosters sipping your HOT Cuppa!
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/feature2.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 1 Section End -->
    <!-- Features 1 Section Start -->
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 ">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Legally use other people’s trendy videos and profit from it
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15 text-md-start text-center">
                                Videos are the best way to share more in few seconds. But creating them is not everyone’s cup of tea. With Trendio, that’ll be an issue of the past.
                                <br><br> Just enter few keywords for your niche and you have all the HOT & Popular videos from the video sharing Giant, YouTube. The secret formula is 100% Legal and you can start PROFITING from Today!
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/feature3.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 1 Section End -->
    <!-- Features 2 Section Start -->
    <div class="features-two">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Make trendy content viral to drive tons of targeted traffic
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                It’s no secret that social media is the ULTIMATE way to give push to your trendy content for driving targeted traffic.
                                <br><br><span class="w500"> So, with Trendio, you can easily share your articles & videos on TOP social platforms</span> without investing time & money on creating and optimizing posts. As you already have the Power of
                                Viral Content added with engagement boosters.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                            <img src="assets/images/feature4.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 2 Section End -->
    <!-- Features 3 Section Start -->
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Set and forget system with just one keyword
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                Gone are the days of doing stuff manually and wasting time and energy.
                                <br><br>Simply by settings rules once, you have the POWER to get TOP of the content each time flowing right to you as soon as they are published. Trendio has built-in settings for automating content curation from Top &
                                trusted web publishers.
                                <br><br>Syndicate your selected social accounts and be the first one to share viral content to your subscribers and make them loyal to your business.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                            <img src="assets/images/feature5.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 3 Section End -->
    <!-- Features 4 Section Start -->
    <div class="features-four">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Get unstoppable leads in your favourite Autoresponder or the dashboard
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15 text-md-start text-center">
                                Trendio has been custom created to enable you get fast and easy integration with TOP autoresponders. Import your leads directly to your favorite autoresponder and you’re all set to rock.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                            <img src="assets/images/feature6.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features 4 Section End -->
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Done-For-You, Colour Themes to Create Your Beautiful & Trendy Content Rich Viral Sites
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                These attractive, eye-catchy and premium themes have been created keeping every marketer’s needs in mind. They will prove to be a great add-on to your purchase.
                                <br><br>All you need to do is just use them, and see a huge boost in conversions, leads and sales hands down.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                            <img src="assets/images/feature7.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-four">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Loaded with PROVEN Converting & Ready-To-Use Lead & Promo Templates & Easy & FAST Editor
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                Just tweak them within minutes using our editor to create a high converting promo to maximise profits.
                                <br><br>No matter what industry you’re in, our enticing templates offers your business a way to truly stand out and skyrocket conversions.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                            <img src="assets/images/feature8.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Market your content in 15+ Languages in one click
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                When it comes to gathering audience attention, adding a <span class="w500">personalized</span> touch to your audience is of immense importance for every marketer’s success. So, Trendio comes pre-loaded with <span class="w500">15+ languages</span>                                that gets every visitor glued to your sites.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                            <img src="assets/images/feature9.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-four">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Create SEO friendly pages automatically
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                Worried whether Trendio follows SEO guidelines to staying compliant? Yes, you have built-in settings for making all your sites completely SEO friendly and all that comes extremely fast and easy.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                            <img src="assets/images/feature11.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6  f-18 mt20 mt-md0">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                No Domain or hosting required
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                With Trendio, you <span class="w500">save hundreds of dollars as yearly charges for hosting services.</span> Now, when we have everything in place for you, you just sit and relax and save your money.
                            </div>
                        </div>
                        <div class="col-12 col-md-6  mt20 mt-md0 fe-box-img">
                            <img src="assets/images/feature12.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-four">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Precise analytics included
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                Get your viral site analytics with smart metrics on articles and videos count, leads captured, views along with quick-to-understand graphs for articles and videos. So you can see how successful your campaigns are and scale them accordingly.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                            <img src="assets/images/feature13.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="features-three">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                            <div class="f-22 f-md-32 w600 lh150 text-capitalize black-clr text-md-start text-center">
                                Complete A-Z Training
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt15">
                                When you are diving into such a large pool of features, we are not leaving you alone there. You will see complete step-by-step training in PDF and Video format to take you by the hand and make everything fast and easy.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                            <img src="assets/images/feature14.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cta-section-new">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto col-12 text-center">
                            <a href="#form" class="cta-link-btn1 d-block">Book Your Free Seat</a>
                        </div>
            </div>
        </div>
    </div>

    <!-- Suport Section Start -->
    <div class="support-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 ">
                    <div class="f-md-45 f-28 w600 white-clr text-center ">
                        Here’re Some More Features
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 ">
                    <div class="row g-0 ">
                        <!-- first -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder top-first ">
                                <img src="assets/images/k1.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    Round-the-Clock Support
                                </div>
                            </div>
                        </div>
                        <!-- second -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-rb boxborder ">
                                <img src="assets/images/k2.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    No Coding, Design or Technical
                                    <br class="visible-lg "> Skills Required
                                </div>
                            </div>
                        </div>
                        <!-- third -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-b boxborder ">
                                <img src="assets/images/k3.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    Regular Updates
                                </div>
                            </div>
                        </div>
                        <!-- fourth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/k4.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    Complete Step-by-Step Video
                                    <br class="visible-lg ">Training and Tutorials Included
                                </div>
                            </div>
                        </div>
                        <!-- fifth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder-r boxborder ">
                                <img src="assets/images/k5.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    Newbie Friendly & Easy To Use
                                </div>
                            </div>
                        </div>
                        <!-- sixth -->
                        <div class="col-12 col-md-4 ">
                            <div class="boxbg boxborder ">
                                <img src="assets/images/k6.png " class="img-fluid d-block mx-auto ">
                                <div class="f-18 f-md-18 w300 lh150 mt15 mt-md40 text-center white-clr ">
                                    Limited Commercial License
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Suport Section End -->
    <!-- Compare Table Section -->
    <section class="comp-table">
        <div class="comparetable-section ">
            <div class="container ">
                <div class="row ">
                    <div class="col-12 f-18 f-md-22 w400 lh150 black-clr text-center ">
                        Trendio Is A Complete Pro Website Builder.
                    </div>
                    <div class="col-12 mt10 ">
                        <div class="f-md-45 f-28 w600 black-clr text-center ">
                            No Other Software Even Comes Close!
                        </div>
                        <div class="row g-0 mt70 mt-md100">
                            <div class="col-md-6 col-6">
                                <div class="fist-row">
                                    <ul class="f-md-16 f-14 w500">
                                        <li class="f-md-20 f-16">Features</li>
                                        <li><span class="w600">Set and Forget System </span> with Single Keyword </li>
                                        <li>Self-Updating &amp; Beautiful Trendy Sites </li>
                                        <li>1-Click Traffic Generating System </li>
                                        <li>Auto-Curate Trendy Videos </li>
                                        <li>Auto-Curate Trendy Articles </li>
                                        <li>UNLIMITED Campaigns </li>
                                        <li>Puts Most Profitable Affiliate Links using AI </li>
                                        <li>Done-For-You Promo &amp; Lead Gen Templates </li>
                                        <li>Excellent, Fast &amp; Easy Template Editor </li>
                                        <li>Campaign Scheduling (pop ups effect)</li>
                                        <li>Built-in Content Spinner </li>
                                        <li>100% SEO Friendly Website and Built-In Remarketing System </li>
                                        <li>Accelerated Mobile Pages </li>
                                        <li>Built-in Slider Management </li>
                                        <li>Amazon Ads Integration </li>
                                        <li>Monetization with Banner Ads </li>
                                        <li>Precise Site Analytics </li>
                                        <li>SSL Certification &amp; GDPR Compliance </li>
                                        <li>One-Time Payment </li>
                                        <li>FREE Commercial License Included </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-3">
                                <div class="second-row">
                                    <ul class="f-md-16 f-14">
                                        <li>
                                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 912.39 250" style="max-height:40px;"><defs><style>.cls-1{fill:#ffb703;}.cls-2{opacity:0.3;}.cls-3{fill:url(#linear-gradient);}.cls-4{fill:url(#linear-gradient-2);}.cls-5{fill:url(#linear-gradient-3);}.cls-6{fill:#fff;}</style><linearGradient id="linear-gradient" x1="17.43" y1="209.55" x2="18.97" y2="209.55" gradientUnits="userSpaceOnUse"><stop offset="0"></stop><stop offset="1" stop-color="#333"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="18.97" y1="181.92" x2="91.57" y2="181.92" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="104.55" y1="132.74" x2="333.22" y2="132.74" xlink:href="#linear-gradient"></linearGradient></defs><path class="cls-1" d="M19,210.68l70-69.1,2.57,17.67,10.86,74.65,2.12-2L264.73,84,270,79.2l6.8-6.27L282,68.07l-25.41-27.5-12,11.1-5.25,4.86-108,99.67-2.12,2L118,80.59,115.4,62.91,0,176.79l.94,2.15c.59,1.35,1.2,2.67,1.82,4,.49,1,1,2.06,1.5,3.09.37.76.76,1.54,1.17,2.31q3.86,7.53,8.57,14.93c.57.89,1.13,1.76,1.73,2.65Z" transform="translate(0 0.38)"></path><polygon class="cls-1" points="328.54 0 322.97 17.91 295.28 106.97 279.91 90.32 269.97 79.57 254.51 62.82 244.59 52.05 232.01 38.45 219.28 24.65 328.54 0"></polygon><g class="cls-2"><path class="cls-3" d="M17.43,208.42,19,210.68l-1.09-1.58s0,0,0,0l-.17-.26Z" transform="translate(0 0.38)"></path><path class="cls-4" d="M89,141.58l2.57,17.67-63.83,63c-1.18-1.45-2.34-2.91-3.46-4.36q-2.76-3.6-5.31-7.22Z" transform="translate(0 0.38)"></path><polygon class="cls-5" points="333.22 15.6 299.96 122.57 274.65 95.17 107.11 249.87 104.55 232.31 264.73 84.41 269.97 79.57 279.91 90.32 295.28 106.97 322.97 17.91 333.22 15.6"></polygon></g><path class="cls-6" d="M229.46,241.56q-18.42,8.07-33.58,8.06a45.93,45.93,0,0,1-25.7-7.34c-23.05-15.07-23.72-46.18-23.72-49.67V61.29h32.2v30h39.27v32.2H178.66v69c.07,4.46,1.88,18.11,9.2,22.83,5.51,3.55,15.7,2.38,28.68-3.3Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M250.4,163.91c0-16-.24-27.35-1-38h25.9l1,22.51h1c5.81-16.7,19.6-25.17,32.19-25.17a30.9,30.9,0,0,1,7,.73v28.07a41.61,41.61,0,0,0-8.71-1c-14.28,0-24,9.2-26.63,22.51a51.55,51.55,0,0,0-1,9.2v61H250.4Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M459.28,161c0-13.56-.24-24.93-1-35.1h26.14l1.45,17.67h.73c5.08-9.2,17.91-20.33,37.51-20.33,20.58,0,41.88,13.31,41.88,50.59v69.95H536.25V177.23c0-16.95-6.29-29.78-22.51-29.78-11.86,0-20.09,8.48-23.23,17.43a30.26,30.26,0,0,0-1.21,9.68v69.23h-30Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M706.41,71.93v138.7c0,12.1.48,25.17,1,33.16H680.75l-1.21-18.64h-.48c-7,13.07-21.3,21.3-38.49,21.3-28.07,0-50.34-24-50.34-60.27-.24-39.45,24.44-62.93,52.76-62.93,16.22,0,27.84,6.78,33.16,15.49h.49V71.93ZM676.64,175.05a41.49,41.49,0,0,0-.73-7.75c-2.66-11.62-12.1-21.06-25.65-21.06-19.13,0-29.78,16.95-29.78,39,0,21.55,10.65,37.28,29.53,37.28,12.11,0,22.76-8.23,25.66-21.06a33,33,0,0,0,1-8.71Z" transform="translate(0 0.38)"></path><path class="cls-1" d="M769.68,89.57c-.11-.52-.24-1-.39-1.54s-.25-.77-.39-1.14-.25-.64-.39-.94-.25-.55-.39-.81-.25-.47-.38-.7-.26-.43-.39-.63-.26-.38-.39-.57l-.39-.51-.39-.48c-.12-.15-.25-.29-.39-.44l-.38-.41L765,81l-.39-.35-.39-.33-.39-.3-.39-.28c-.13-.09-.25-.19-.39-.27s-.25-.17-.38-.25L762.3,79l-.39-.22-.39-.2-.39-.19-.39-.17-.38-.17-.39-.15-.39-.14-.39-.13-.39-.12-.39-.11-.38-.1-.39-.09-.39-.08-.39-.08-.39-.06-.39-.06-.38,0-.39,0c-.13,0-.26,0-.39,0l-.39,0-.39,0h-1.55l-.39,0-.39,0-.39,0-.39,0-.38,0-.39.06-.39.06-.39.08-.39.08-.39.1-.38.1-.39.11-.39.12-.39.13-.39.15-.39.15-.38.16-.39.18-.39.19-.39.2-.39.22-.39.23-.39.25-.38.26-.39.28-.39.3-.39.33-.39.34-.39.37c-.13.13-.26.26-.38.4a5.22,5.22,0,0,0-.39.43l-.39.45-.39.5-.39.56-.39.61c-.13.22-.26.45-.38.68s-.27.5-.39.76-.27.6-.39.91-.27.73-.39,1.1-.28,1-.39,1.49a17.76,17.76,0,0,0-.39,3.69,17.56,17.56,0,0,0,.39,3.67q.16.75.39,1.47t.39,1.11c.12.3.25.59.39.89s.25.51.39.76.25.45.38.67l.39.61.39.55.39.5c.12.15.26.3.39.45s.25.28.39.42.25.27.38.39l.39.37.39.34.39.31q.19.16.39.3c.12.1.26.19.39.28l.38.26.39.24.39.22.39.22.39.19.39.19.39.17.38.16c.13.06.26.1.39.15l.39.14.39.12c.13,0,.26.09.39.12l.39.1.38.11.39.08.39.08.39.07.39.06.39,0,.38,0,.39,0,.39,0,.39,0h1.94l.39,0,.39,0,.39,0,.39,0,.38,0,.39-.06.39-.07.39-.07.39-.08.39-.1.38-.09.39-.11.39-.12.39-.13.39-.14.39-.15.38-.16.39-.17.39-.19.39-.19.39-.21.39-.23.38-.24c.14-.08.26-.17.39-.26l.39-.27.39-.29.39-.32.39-.33.39-.37a4.68,4.68,0,0,0,.38-.39,5,5,0,0,0,.39-.41l.39-.45.39-.5.39-.54.39-.6c.13-.22.26-.44.38-.67s.27-.5.39-.76.27-.58.39-.88.27-.73.39-1.1.28-1,.39-1.46a17.64,17.64,0,0,0,.39-3.67A21.05,21.05,0,0,0,769.68,89.57Z" transform="translate(0 0.38)"></path><rect class="cls-6" x="738.36" y="126.29" width="30.01" height="117.88"></rect><path class="cls-6" d="M912.39,183.76c0,43.33-30.5,62.69-60.51,62.69-33.41,0-59.06-23-59.06-60.75,0-38.73,25.41-62.45,61-62.45C888.91,123.25,912.39,147.94,912.39,183.76ZM823.56,185c0,22.75,11.13,39.94,29.29,39.94,16.94,0,28.8-16.7,28.8-40.42,0-18.4-8.23-39.46-28.56-39.46C832,145,823.56,165.36,823.56,185Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M386,243.14a60.38,60.38,0,0,1-8.87-.66A59.61,59.61,0,1,1,433.76,148l7.61,10.25-55.9,41.45-15.21-20.51,33.4-24.77a34.07,34.07,0,1,0,12.23,45.22l22.51,12.08a59.72,59.72,0,0,1-52.4,31.4Z" transform="translate(0 0.38)"></path></svg>
                                        </li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3 col-3">
                                <div class="third-row">
                                    <ul class="f-md-16 f-14">
                                        <li class="f-md-20 f-16">Others</li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="mt-md30 mt20 f-16 f-md-16 w400 lh150 "><span class="w700 ">Note :</span> All the features mentioned in the above table are bifurcated in different upgrade options according to the need of individual users and the features that you will get with purchase of Trendio Start
                            or Pro Commercial plan are mentioned in the pricing table below on this page.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Compare Table Section End -->
    <!-- No Comparsion Section -->
    <div class="nocomparsion-section ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 col-md-12 no-comparison-shape ">
                    <div class="text-capitalize f-md-45 f-28 w500 lh120 black-clr text-center text-md-left ">
                        There Is <span class="w700 blue-clr ">ABSOLUTELY NO Comparison Of Trendio </span>At <br class="d-none d-md-block "> Any Price.
                    </div>
                    <div class="f-18 f-md-18 w400 lh150 mt15 text-center text-md-left ">
                        That’s correct! There’s absolutely no match to the power packed Trendio platform. It’s built with years of experience, designing, coding, debugging & real-user-testing to help get you maximum results.<br><br> When it comes to creating
                        super engaging websites for any business NEED, no other software even comes close! Trendio gives you the power to literally crush your competition so…<br><br> Grab it today for this low one-time price because
                        <span class="w600 ">it will never be available again after launch at this price.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- No Comparsion Section End -->
    <div class="cta-section-new">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mx-auto col-12 text-center">
                            <a href="#form" class="cta-link-btn1 d-block">Book Your Free Seat</a>
                        </div>
            </div>
        </div>
    </div>
    <!-- Silver Platter Section Start -->
    <!--<div class="silver-platter-section ">
        <div class="container ">
            <div class="row mx-0 ">
                <div class="col-12 huge-opportunity-shape ">
                    <div class="f-28 f-md-45 w600 black-clr text-center ">
                        Trendio Brings You<br class="d-none d-md-block ">A HUGE Opportunity On A Silver Platter
                    </div>
                    <div class="f-24 f-md-36 w500 lh150 black-clr text-center mt15 ">
                        You Just Need To Capitalize On It – The Easier Way.No Competition With Others.
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- Silver Platter Section End -->
    <!-- Silver Platter Section End -->
    <!--<div class="wearenot-alone-section ">
        <div class="container ">
            <div class="row mx-0 ">
                <div class="col-12 col-md-10 mx-auto alone-bg ">
                    <div class="row ">
                        <div class="col-md-1 ">
                            <img src="assets/images/dollars-image.png " class="img-fluid d-block dollars-md-image " />
                        </div>
                        <div class="col-md-11 f-28 f-md-40 w400  black-clr text-center mt15 mt-md0 d-flex align-items-center ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                        </div>
                    </div>
                </div>
                <div class="col-12 px10 f-28 f-md-45 w600 black-clr text-center mt20 mt-md80 ">Here’s a simple math</div>
                <div class="col-12 mt20 mt-md50 ">
                    <img src="assets/images/make-icon.png " class="img-fluid d-block mx-auto " />
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="f-md-45 f-28 w600 lh140 text-center black-clr">
                        <span class="converting-shape">And It’s Not A 1-Time Income</span>
                    </div>
                </div>
                <div class="col-12 f-18 f-md-20 w400 mt20 text-center ">
                    You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block ">services again and again forever.
                </div>
            </div>
        </div>
    </div>-->
    <!-- Silver Platter Section End -->



    <div class="potential-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-45 f-28 w600 text-center white-clr">
                        Here’s a List of All Potential Niche You Can <br class="d-none d-md-block">
                        <span class="w700 yellow-clr">Start Getting Clients &amp; Collecting Checks</span>
                    </div>
                </div>
            </div>
            <div class="row mt-md40 mt20">
                <div class="col-md-3 col-6">
                    <div class="">
                        <img src="assets/images/pn2.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Affiliate Marketers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt0">
                    <div class="">
                        <img src="assets/images/pn1.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Video Marketers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn3.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        E-Com Sellers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn4.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Business Coaches
                    </div>
                </div>
            </div>
            <div class="row mt-md50 mt0">
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn5.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Digital Product Sellers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn6.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        YouTubers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn7.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        All Info Sellers
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn8.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Lead Generation Companies
                    </div>
                </div>
            </div>
            <div class="row mt-md50 mt0">
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn9.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Music Classes
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn10.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Sports Clubs
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn11.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Bars
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn12.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Restaurants
                    </div>
                </div>
            </div>
            <div class="row mt-md50 mt0">
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn13.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Hotels
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn14.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Schools
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn15.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Churches
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn16.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Taxi Services
                    </div>
                </div>
            </div>
            <div class="row mt-md50 mt0">
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn17.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Garage Owners
                    </div>
                </div>
                <div class="col-md-3 col-6 mt-md0 mt30">
                    <div class="">
                        <img src="assets/images/pn18.png" class="img-fluid mx-auto d-block">
                    </div>
                    <div class="f-18 lh140 w600 white-clr text-center mt5">
                        Dentists
                    </div>
                </div>


            </div>

        </div>
    </div>

    <div class="need-marketing">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-36 w600 lh150 blue-clr">
                        It Works Seamlessly for ANY NICHE… All at the push of a button.
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50">
                    <div class="row align-items-center flex-wrap">
                        <div class="col-12 col-md-7">
                            <div class="f-20 f-md-24 w600 lh150 black-clr text-center text-md-start">
                                Trendio Is Designed to Meet Every Marketer’s Need:
                            </div>
                            <div class="f-18 f-md-18 w400 lh150 black-clr mt20">
                                <ul class="noneed-listing pl0">
                                    <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                                    <li>Lazy people who want easy profits</li>
                                    <li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
                                    <li>People without products who want to kickstart online</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                            <img src="assets/images/platfrom.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70 text-center">
                    <div class="look-shape">
                        <div class="f-22 f-md-30 w600 lh150 black-clr">
                            Look, it doesn't matter who you are or what you're doing.
                        </div>
                        <div class="f-18 f-md-18 w400 lh150 black-clr mt10">
                            If you want to finally drive laser targeted traffic to your offers, make the money that you want <br class="d-none d-md-block"> and live in a way you dream of, Trendio is for you.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="testimonial-section2">
        <div class="container ">
            <div class="row ">
                <div class="col-12">
                    <div class="f-md-45 f-28 lh150 w700 text-center black-clr">Checkout What <span class="blue-clr">Early Trendio Users</span> Have to SAY</div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t5.png" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                    <img src="./assets/images/t6.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t7.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md70">
                    <img src="./assets/images/t8.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
            </div>
        </div>
    </div> 

    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="col-12 f-28 f-md-45 w700 black-clr text-center ">
                        <span class="converting-shape mb15">We're not done yet!</span>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt10 mt-md40">
                    <div class="f-20 f-md-22 w400 lh150 text-center">
                        When You Grab Your Trendio Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-28 f-md-55 w700 lh150 text-center">
                        Limited Time Bonuses!
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Limited Time Bonus #1
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7 order-md-2">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">
                                    LIVE Training: See How You Can Launch, Sell &amp; Market Any Product, Course or Service - Fast &amp; Easy!! (First 1000 buyers only - $997 Value)
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    Launch Your Own Product In FEW Minutes.<br><br> This Awesome LIVE Training will Help you to Start Online and Grow your Online Business for Marketing &amp; Selling your Products and Services. And Free Access to a Private
                                    Community of Like-Minded Entrepreneurs to learn and grow together with each other.<br><br> And There will be a Live Q &amp; A Session at the end of the training.
                                </div>
                            </div>
                            <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                                <div> <img src="assets/images/bonus1.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Limited Time Bonus #2
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">DotcomPal- All in One Growth Platform for Entrepreneurs (That's PRICELESS)</div>
                                <div class="f-18 w400 lh150 mt10 text-start"> All-in-one growth platform for Entrepreneurs &amp; SMBs to generate more leads, bring more growth &amp; sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer
                                    touchpoint... without any designer, tech team, or the usual hassles.
                                    <br><br> This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today.
                                </div>
                            </div>
                            <div class="col-12 col-md-5 mt20 mt-md0">
                                <img src="assets/images/bonus2.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Limited Time Bonus #3
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7 order-md-2">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">Ultimate Notification System That Shows Your Perfect Offer to Visitors According to What They NEED &amp; Generate You More Leads Sales and Commissions.</div>
                                <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                                    Target your audience precisely on the basis of geo-location, behavior, keywords in URL &amp; much more…
                                    <div class="f-18 w400 lh150 black-clr mt20">
                                        <ul class="noneed-listing pl0">
                                            <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                                            <li>Show your visitors exactly what they WANT &amp; maximize your affiliate commissions &amp; leads</li>
                                            <li>Grab visitors from the neck and literally force them to take the desired action</li>
                                            <li>Comes with ready-to-use and proven lead &amp; promo notification templates</li>
                                            <li>Use Inbuilt Inline Editor to change text, images, CTA buttons, or anything in templates &amp; it’s very easy</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                                <img src="assets/images/bonus3.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="f-24 f-md-55 w700 lh150 text-center">
                        Super Value Bonuses
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md100">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Super Value Bonus #4
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">Done-For-You Setup Service (It’s Priceless)
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    Complete Setup Service so you can kickstart your online marketing business with Trenzy instantly.<br><br> You don’t need to worry about planning and designing landing pages, newsletter templates, web forms, notification
                                    pop-up templates, A/B testing them, or even for low email opt-in rates and poor Inboxing. Just tweak these templates according to your brand or campaign's motive and target audience. There are no huge Ad Budgets, low-engaging
                                    designs, and almost everything that holds you back. Not even any additional costs to get started with the system.<br><br> Just provide us with a few details, and our dedicated team will set up everything for you. We
                                    want to see you succeed in the shortest time possible.
                                </div>
                            </div>
                            <div class="col-12 col-md-5  mt20 mt-md0">
                                <div> <img src="assets/images/bonus4.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Super Value Bonus #5
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7 order-md-2">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">How To Monetizing Your Website
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    This Video Training Will Teach You Website and Blog Monetization Strategies. It will show you how to start earning money from your trending content website - <br><br> You will learn a lot about:<br>
                                    <div class="f-18 w400 lh150 black-clr mt20">
                                        <ul class="noneed-listing pl0">
                                            <li>Ad placement</li>
                                            <li>Advertisement</li>
                                            <li>Pre-Selling Strategies</li>
                                            <li>Finding Offers &amp; Deals In Your Niche</li>
                                            <li>Building And Monetizing Your List</li>
                                            <li>List Automation</li>
                                            <li>Marketing Funnel</li>
                                            <li>And much more!</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                                <div> <img src="assets/images/bonus5.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Super Value Bonus #6
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">How to Start Affiliate Marketing
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    When done properly, Affiliate Marketing can be a Very Lucrative Component of an Online Business. This course will teach you what you need to know so you can be a successful affiliate marketer.<br><br> You'll Learn How
                                    To:
                                    <br>
                                    <div class="f-18 w400 lh150 black-clr mt20">
                                        <ul class="noneed-listing pl0">
                                            <li>How to Pick a Niche Market</li>
                                            <li>Finding Quality Products that Fit</li>
                                            <li>Implementing the Bonus Strategy</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-5  mt20 mt-md0">
                                <div> <img src="assets/images/bonus6.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Super Value Bonus #7
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7 order-md-2">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">How To Encash The Power of Viral Marketing
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. With this Video Training, you will Discover a Shortcut to Online Viral Marketing Secrets.<br><br> Topics covered:
                                    <br>
                                    <div class="f-18 w400 lh150 black-clr mt20">
                                        <ul class="noneed-listing pl0">
                                            <li>The Two-Step Trick to Effective Viral Marketing</li>
                                            <li>How Do You Find Hot Content?</li>
                                            <li>Maximize Niche Targeting for Your Curated Content</li>
                                            <li>Filter your content format to go viral on many platforms</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                                <div> <img src="assets/images/bonus7.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt60 mt-md70">
                    <div class="bonus-section-shape text-center">
                        <div class="col-12 margin-t-60">
                            <div class="bonus-headline">
                                <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                                    Super Value Bonus #8
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                            <div class="col-12 col-md-7">
                                <div class="w600 f-22 f-md-24 black-clr lh150 text-start">Video Marketing Profit Kit
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-start">
                                    Video is able to put a face to your brand and make your brand talk to the needs, fears, hopes, and aspirations of your prospective customers. And with this Specialised Training, you will discover how to use video marketing to build a thriving online business!<br><br>                                    You'll Learn How To:<br>
                                    <div class="f-18 w400 lh150 black-clr mt20">
                                        <ul class="noneed-listing pl0">
                                            <li>Find Winning Video Topics</li>
                                            <li>Save Money on Video Creation</li>
                                            <li>Make Money from Your Videos</li>
                                            <li>Includes ready sales materials!</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-5  mt20 mt-md0">
                                <div> <img src="assets/images/bonus8.png" class="img-fluid d-block mx-auto max-h450"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt30 mt-md70 ">
                    <div class="f-22 f-md-36 w600 black-clr lh180 text-center ">
                        That’s A Total Value of <br class="visible-lg "><span class="f-45 f-md-80 w900">$2255</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---Bonus Section Starts-->

    <!-- Amazing Deal Section -->
    <!-- <div class="amazing-tool-bg ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 text-center ">
                    <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                        <span class="red-shape">Wow! That Sounds An Amazing Deal....</span>
                    </div>
                </div>
                <div class="col-12 text-center mt20 mt-md20 ">
                    <div class="w400 f-24 f-md-32 black-clr lh150 ">
                        But “I’m Just Getting Started Online. Can Trendio <br class="visible-lg ">Help Me Build An Online Business Too?”
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70 ">
                    <div class="row align-items-center ">
                        <div class="col-12 col-md-8 ">
                            <div class="f-18 w400 lh150 text-left ">
                                <span class="w600 f-20 f-md-36 "><u>Absolutely,</u></span><br> You easily can tap into the huge website building service selling industry right from the comfort of your home. Help local businesses to get online during this
                                pandemic and make a sustainable income stream. <br><br>
                                <span class="w600 ">You can literally create a highly engaging business websites your own and start an online business today</span> or for local clients without any tech hassles. Even better, you won’t go bankrupt in the
                                process by hiring third party money sucking service providers. <br><br>
                                <span class="w600 ">But even with, we want to offer you something great today that will <u>open a HUGE Business Opportunity For YOU!</u></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0 ">
                            <img src="assets/images/sounds-great-img.png " class="img-fluid d-block mx-auto ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- Amazing Deal Section end  -->
    <!-- License Section Start -->
    <div class="license-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 f-28 f-md-45 w500 black-clr text-center ">
                    <span class="converting-shape w600 mb15">Alert! FREE Limited Time Upgrade:</span><br> Get Commercial License To Use Trendio To Create An Incredible Income!
                </div>
                <div class="col-12 mt20 mt-md30 f-md-22 f-20 w400 lh150 black-clr text-center ">
                    As we have shown you, there are tons of businesses enter in the online market every day – yoga gurus, fitness centers or anybody else who want to grow online, desperately need your services & eager to pay you BIG for your services.
                </div>
                <div class="col-12 mt20 mt-md70 ">
                    <div class="row align-items-center ">
                        <div class="col-12 col-md-6">
                            <img src="assets/images/commercial-license-img.png " class="img-fluid d-block mx-auto ">
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 ">
                            <div class="f-md-18 f-18 w400 lh150 black-clr ">Build their branded and professional websites to them get more exposure, boost their authority and profits online. They will pay top dollar to you when you deliver top notch services to them. We’ve taken care of everything
                                so that you can deliver those services easily.<br><br>
                                <span class="w600 ">Notice: </span>This special commercial license is being included in the Pro plan and ONLY during this launch.<span class="w600 "> Take advantage of it now because it will never be offered again.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Guarantee Section Start -->

    <div class="noneed-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-20 f-md-22 w400 lh150 text-center">
                        <span class="w600">Only 3 easy steps is all it takes </span>to publish trendy <br class="d-none d-md-block"> content &amp; bring hordes of TRAFFIC &amp; Sales…
                    </div>
                    <div class="f-md-45 f-28 w700 black-clr lh150 text-center mt20 mt-md40">
                        With Trendio, Turn Your Worries <br class="d-none d-md-block"> Into A HUGE Profitable Opportunity

                    </div>
                    <div class="noneed-shape mt20 mt-md40">
                        <div class="f-18 w400 lh150">
                            <ul class="noneed-list pl0">
                                <li><span class="w600">No more worrying</span> for researching and writing content daily </li>
                                <li><span class="w600">No need for learning</span> complex video &amp; audio editing skills</li>
                                <li><span class="w600">No worries for buying</span> expensive equipment that can leave your back accounts dry </li>
                                <li><span class="w600">No more frustration</span> of not being able to face the camera &amp; creating a mockery of yourself </li>
                                <li><span class="w600">No worries</span> for lack of technical, designing or marketing skills </li>
                                <li><span class="w600">Say no to wasting</span> any time on creating funnels, SEO, paid traffic, complex tools etc. </li>
                                <li class="happy"><span class="w700">And…say YES Now to living a LAPTOP lifestyle</span> that gives you an ability to spend more time with your family and have complete financial FREEDOM.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="riskfree-section ">
        <div class="container ">
            <div class="row align-items-center ">
                <div class="f-28 f-md-45 w700 black-clr text-center col-12 mb-md40">Test Drive Trendio Risk FREE For 30 Days
                </div>
                <div class="col-md-7 col-12 order-md-2">
                    <div class="f-md-18 f-18 w400 lh150 black-clr mt15 "><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                        <br><br>
                        <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your client’s risk free.</span> If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk
                        within 30 days and we'll refund you everything, down to the last penny.<br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact
                        our support and we will refund your hard-earned money.
                    </div>
                </div>
                <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                    <img src="assets/images/riskfree-img.png " class="img-fluid d-block mx-auto ">
                </div>
            </div>
        </div>
    </div>
    <!-- Guarantee Section End -->

    <!-- Discounted Price Section Start -->
    <div class="table-section " id="buynow">
        <div class="container ">
            <div class="row ">
                <div class="col-12 col-md-11 mx-auto" id="form">
						<div class="auto_responder_form inp-phold formbg">
							<div class="f-20 f-md-24 w500 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
								Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
								<span class="f-22 f-md-32 w700 text-center black-clr" contenteditable="false">4<sup contenteditable="false">th</sup> MAY, 10 AM EST (100 Seats Only)</span>
							</div>
							<div class="col-12 col-md-6 mx-auto my20 timer-block text-center">
								<div class="col-md-12 col-12 f-md-22 f-20 w600 lh140 px0 px-md15">
									Hurry Up! Free Offer Going Away in… 
								</div>
								<div class="col-12 col-md-10 mx-auto mt-md20 mt20">
									<div class="countdown counter-black">
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
										<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
									</div>
								</div>
							</div>
							<!-- Aweber Form Code -->
							<div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
								
								<!-- Aweber Form Code -->
								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="885892634" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6258786" />
                                 <input type="hidden" name="redirect" value="https://www.trendio.biz/prelaunch-thankyou" id="redirect_8cbadda9f3f2065956e0718e44d13a6b" />
                                 <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-885892634" class="af-form">
                                 <div id="af-body-885892634" class="af-body af-standards">
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114042796"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-114042796" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-114042797"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-114042797" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <!--<div class="af-element">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>-->
									
								   <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                             <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="" /></div>
                           </form>		
							</div>
						</div>
					</div>
            </div>
        </div>
    </div>
    <!-- Discounted Price Section End -->


    <!-- Warning Section Start
    <div class="extrasecwhite ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 ">
                    <div class="f-28 f-md-45 w600 black-clr text-center ">
                        Warning! Time Is Running Out Fast
                    </div>
                </div>
                <div class="col-12 ">
                    <div class="row align-items-center ">
                        <div class="col-md-6 col-12 ">
                            <div class="f-18 f-md-18 w300 text-center text-md-left black-clr lh150 mt20 mt-md35 ">
                                <span class="w500 ">The good news</span> is that you can get Trendio for a LOW price today. I’m talking about an INSANELY HUGE discount that others will think we’re crazy for offering.<br><br> But This discount is only
                                available for a limited time. As a matter of fact, the price for Trendio rises with the timer on this page. <span class="w500
                              ">So when the clock resets, you’ll be forced to pay more for access to Trendio.</span><br><br> So, if you’re serious about building a real business online and powering that business using websites, (which no business will
                                survive without) <span class="w500 ">then we URGE you to sign up for Trendio now.</span>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mt20 mt-md0 ">
                            <img src="assets/images/fact1.png " class="img-fluid d-block mx-auto ">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-md40 mt20 f-22 f-md-36 w400 text-center black-clr lh150 ">
                    Pick the plan that is right for you below to <span class="w500 "><u>lock in your launch special low one-time price </u></span>and we’ll see you inside our members area.
                </div>
            </div>
        </div>
    </div>
	Warning Section End -->

    <!-- To your awesome section -->
    <div class="awesome-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 ">
                    <div class="col-12 f-28 f-md-45 w600 white-clr text-center ">Thanks for Checking out Trendio</div>
                </div>
            </div>
            <div class="row mt20 mt-md50 ">
                <div class="col-md-4 col-12 text-center ">
                    <img src="assets/images/amit-pareek-sir.png " class="img-fluid d-block mx-auto ">
                    <div class="f-22 f-md-32 w600 mt15 mt-md20 lh150 text-center white-clr ">
                        Dr Amit Pareek
                    </div>
                    <div class="f-16 w500 lh150 text-center white-clr ">
                        (Techpreneur &amp; Marketer)
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center mt20 mt-md0 ">
                    <img src="assets/images/achal-goswami-sir.png " class="img-fluid d-block mx-auto ">
                    <div class="f-22 f-md-32 w600 mt15 mt-md20 lh150 text-center white-clr ">
                        Achal Goswami
                    </div>
                    <div class="f-16 w500 lh150 text-center white-clr ">
                        (Entrepreneur &amp; Internet Marketer)
                    </div>
                </div>
                <div class="col-md-4 col-12 text-center mt20 mt-md0 ">
                    <img src="assets/images/atul-parrek-sir.png " class="img-fluid d-block mx-auto ">
                    <div class="f-22 f-md-32 w600 mt15 mt-md20 lh150 text-center white-clr ">
                        Atul Pareek
                    </div>
                    <div class="f-16 w500 lh150 text-center white-clr ">
                        (Entrepreneur &amp; JV Manager)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- To your awesome section End -->
    <!-- FAQ Section Start -->
    <div class="faq-section ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 f-28 f-md-45 lh150 w700 text-center black-clr ">
                    Frequently Asked <span class="blue-clr ">Questions</span>
                </div>
                <div class="col-12 mt20 mt-md30 ">
                    <div class="row ">
                        <div class="col-md-6 col-12 ">
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Do I need to download or install Trendio somewhere?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    NO! You just create an account online and you can get started immediately. To build a site, you need to download our Agencies Framework and upload it to your WordPress site only once. After that it run as 100% web-based solution hosted on the cloud.<br><br>                                    This means you never have to download or update anything again. 1 Click updating of your themes and templates. And It works across all browsers and all devices including Windows and Mac.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Is my investment risk free?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof
                                    that you tried it before asking for a refund.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Do you charge any monthly fees?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its
                                    100% optional & not mandatory for working with Trendio. Those are recommended if you want to multiply your benefits.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Will I get any training or support for my questions?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member’s area. You will also get live chat - customer support so you never get stuck or have
                                    any issues.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 ">
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Do you charge any monthly fees?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    NOT AT ALL. There are NO monthly fees to use Trendio during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Is Trendio compliant with all guidelines & compliances?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party
                                    services which is not a part of Trendio while choosing it for your business.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    What is the duration of service with this Trendio launch special deal?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary
                                    renewal to our founder members for buying from us early.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    How is Trendio is different from other available tools in the market?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    Well, we have a nice comparison chart with other service providers. We won’t like to boast much about our software, but we can assure you that this is a cutting edge technology that will enable you to create and sell stunning wesbites at such a low introductory
                                    price.
                                </div>
                            </div>
                            <div class="faq-list ">
                                <div class="f-md-22 f-20 w600 black-clr lh150 ">
                                    Is Trendio Windows and Mac compatible?
                                </div>
                                <div class="f-18 w400 lh150 mt10 text-left ">
                                    YES. We’ve already stated that Trendio WordPress Framework is a web-based solution. So, it runs directly on the web and works across all browsers and all devices.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50 ">
                    <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh150 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="blue-clr ">support@bizomart.com</a></div>
                </div>
                <!-- <div class="col-12 mt40 mt-md60">
                    <div class="f-24 f-md-34 w700 lh150 text-center black-clr animate-charcter">
                        <img src=" ./assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                        <span class="red-clr ">Hurry!</span> Limited Time Only. No Monthly Fees 
                    </div>
                    <div class="f-18 f-md-20 w400 lh150 text-center mt20 black-clr ">
                        Use Coupon Code <span class="red-clr w600 ">“web10”</span> for an Additional <span class="red-clr w600 ">11% Discount</span>
                    </div>
                    <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                            <a href="#form " class="cta-link-btn ">Get Started with “Trendio” Now!</a>
                        </div>
                    </div>
                    <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                        <img src="assets/images/compaitable-with.png " class="img-fluid d-block mx-xs-center md-img-right ">
                        <div class="d-lg-block d-none visible-md px-md30 "><img src="assets/images/v-line.png " class="img-fluid "></div>
                        <img src="assets/images/days-gurantee.png " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- FAQ Section End -->

    <!--final section-->
    <div class="final-section ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 passport-content">
                    <div class="text-center">
                        <div class="f-45 f-md-45 lh140 w600 text-center black-clr red-shape2 converting-shape">
                            FINAL CALL
                        </div>
                    </div>
                    <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0 mb20 mb-md50">
                        You Still Have The Opportunity To Take Your Game To The Next Level… <br> Remember, It’s Your Make Or Break Time!
                    </div>
                    <!-- CTA Btn Section Start -->
                    <div class="col-md-6 mx-auto col-12 text-center">
                            <a href="#form" class="cta-link-btn1 d-block">Book Your Free Seat</a>
                        </div>
                    <!-- CTA Btn Section End -->
                </div>
            </div>
        </div>
    </div>
    <!--final section-->
    <!--Footer Section Start -->
    <div class="footer-section ">
        <div class="container ">
            <div class="row ">
                <div class="col-12 text-center ">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 912.39 250" style="max-height:40px;"><defs><style>.cls-1{fill:#ffb703;}.cls-2{opacity:0.3;}.cls-3{fill:url(#linear-gradient);}.cls-4{fill:url(#linear-gradient-2);}.cls-5{fill:url(#linear-gradient-3);}.cls-6{fill:#fff;}</style><linearGradient id="linear-gradient" x1="17.43" y1="209.55" x2="18.97" y2="209.55" gradientUnits="userSpaceOnUse"><stop offset="0"></stop><stop offset="1" stop-color="#333"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="18.97" y1="181.92" x2="91.57" y2="181.92" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="104.55" y1="132.74" x2="333.22" y2="132.74" xlink:href="#linear-gradient"></linearGradient></defs><path class="cls-1" d="M19,210.68l70-69.1,2.57,17.67,10.86,74.65,2.12-2L264.73,84,270,79.2l6.8-6.27L282,68.07l-25.41-27.5-12,11.1-5.25,4.86-108,99.67-2.12,2L118,80.59,115.4,62.91,0,176.79l.94,2.15c.59,1.35,1.2,2.67,1.82,4,.49,1,1,2.06,1.5,3.09.37.76.76,1.54,1.17,2.31q3.86,7.53,8.57,14.93c.57.89,1.13,1.76,1.73,2.65Z" transform="translate(0 0.38)"></path><polygon class="cls-1" points="328.54 0 322.97 17.91 295.28 106.97 279.91 90.32 269.97 79.57 254.51 62.82 244.59 52.05 232.01 38.45 219.28 24.65 328.54 0"></polygon><g class="cls-2"><path class="cls-3" d="M17.43,208.42,19,210.68l-1.09-1.58s0,0,0,0l-.17-.26Z" transform="translate(0 0.38)"></path><path class="cls-4" d="M89,141.58l2.57,17.67-63.83,63c-1.18-1.45-2.34-2.91-3.46-4.36q-2.76-3.6-5.31-7.22Z" transform="translate(0 0.38)"></path><polygon class="cls-5" points="333.22 15.6 299.96 122.57 274.65 95.17 107.11 249.87 104.55 232.31 264.73 84.41 269.97 79.57 279.91 90.32 295.28 106.97 322.97 17.91 333.22 15.6"></polygon></g><path class="cls-6" d="M229.46,241.56q-18.42,8.07-33.58,8.06a45.93,45.93,0,0,1-25.7-7.34c-23.05-15.07-23.72-46.18-23.72-49.67V61.29h32.2v30h39.27v32.2H178.66v69c.07,4.46,1.88,18.11,9.2,22.83,5.51,3.55,15.7,2.38,28.68-3.3Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M250.4,163.91c0-16-.24-27.35-1-38h25.9l1,22.51h1c5.81-16.7,19.6-25.17,32.19-25.17a30.9,30.9,0,0,1,7,.73v28.07a41.61,41.61,0,0,0-8.71-1c-14.28,0-24,9.2-26.63,22.51a51.55,51.55,0,0,0-1,9.2v61H250.4Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M459.28,161c0-13.56-.24-24.93-1-35.1h26.14l1.45,17.67h.73c5.08-9.2,17.91-20.33,37.51-20.33,20.58,0,41.88,13.31,41.88,50.59v69.95H536.25V177.23c0-16.95-6.29-29.78-22.51-29.78-11.86,0-20.09,8.48-23.23,17.43a30.26,30.26,0,0,0-1.21,9.68v69.23h-30Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M706.41,71.93v138.7c0,12.1.48,25.17,1,33.16H680.75l-1.21-18.64h-.48c-7,13.07-21.3,21.3-38.49,21.3-28.07,0-50.34-24-50.34-60.27-.24-39.45,24.44-62.93,52.76-62.93,16.22,0,27.84,6.78,33.16,15.49h.49V71.93ZM676.64,175.05a41.49,41.49,0,0,0-.73-7.75c-2.66-11.62-12.1-21.06-25.65-21.06-19.13,0-29.78,16.95-29.78,39,0,21.55,10.65,37.28,29.53,37.28,12.11,0,22.76-8.23,25.66-21.06a33,33,0,0,0,1-8.71Z" transform="translate(0 0.38)"></path><path class="cls-1" d="M769.68,89.57c-.11-.52-.24-1-.39-1.54s-.25-.77-.39-1.14-.25-.64-.39-.94-.25-.55-.39-.81-.25-.47-.38-.7-.26-.43-.39-.63-.26-.38-.39-.57l-.39-.51-.39-.48c-.12-.15-.25-.29-.39-.44l-.38-.41L765,81l-.39-.35-.39-.33-.39-.3-.39-.28c-.13-.09-.25-.19-.39-.27s-.25-.17-.38-.25L762.3,79l-.39-.22-.39-.2-.39-.19-.39-.17-.38-.17-.39-.15-.39-.14-.39-.13-.39-.12-.39-.11-.38-.1-.39-.09-.39-.08-.39-.08-.39-.06-.39-.06-.38,0-.39,0c-.13,0-.26,0-.39,0l-.39,0-.39,0h-1.55l-.39,0-.39,0-.39,0-.39,0-.38,0-.39.06-.39.06-.39.08-.39.08-.39.1-.38.1-.39.11-.39.12-.39.13-.39.15-.39.15-.38.16-.39.18-.39.19-.39.2-.39.22-.39.23-.39.25-.38.26-.39.28-.39.3-.39.33-.39.34-.39.37c-.13.13-.26.26-.38.4a5.22,5.22,0,0,0-.39.43l-.39.45-.39.5-.39.56-.39.61c-.13.22-.26.45-.38.68s-.27.5-.39.76-.27.6-.39.91-.27.73-.39,1.1-.28,1-.39,1.49a17.76,17.76,0,0,0-.39,3.69,17.56,17.56,0,0,0,.39,3.67q.16.75.39,1.47t.39,1.11c.12.3.25.59.39.89s.25.51.39.76.25.45.38.67l.39.61.39.55.39.5c.12.15.26.3.39.45s.25.28.39.42.25.27.38.39l.39.37.39.34.39.31q.19.16.39.3c.12.1.26.19.39.28l.38.26.39.24.39.22.39.22.39.19.39.19.39.17.38.16c.13.06.26.1.39.15l.39.14.39.12c.13,0,.26.09.39.12l.39.1.38.11.39.08.39.08.39.07.39.06.39,0,.38,0,.39,0,.39,0,.39,0h1.94l.39,0,.39,0,.39,0,.39,0,.38,0,.39-.06.39-.07.39-.07.39-.08.39-.1.38-.09.39-.11.39-.12.39-.13.39-.14.39-.15.38-.16.39-.17.39-.19.39-.19.39-.21.39-.23.38-.24c.14-.08.26-.17.39-.26l.39-.27.39-.29.39-.32.39-.33.39-.37a4.68,4.68,0,0,0,.38-.39,5,5,0,0,0,.39-.41l.39-.45.39-.5.39-.54.39-.6c.13-.22.26-.44.38-.67s.27-.5.39-.76.27-.58.39-.88.27-.73.39-1.1.28-1,.39-1.46a17.64,17.64,0,0,0,.39-3.67A21.05,21.05,0,0,0,769.68,89.57Z" transform="translate(0 0.38)"></path><rect class="cls-6" x="738.36" y="126.29" width="30.01" height="117.88"></rect><path class="cls-6" d="M912.39,183.76c0,43.33-30.5,62.69-60.51,62.69-33.41,0-59.06-23-59.06-60.75,0-38.73,25.41-62.45,61-62.45C888.91,123.25,912.39,147.94,912.39,183.76ZM823.56,185c0,22.75,11.13,39.94,29.29,39.94,16.94,0,28.8-16.7,28.8-40.42,0-18.4-8.23-39.46-28.56-39.46C832,145,823.56,165.36,823.56,185Z" transform="translate(0 0.38)"></path><path class="cls-6" d="M386,243.14a60.38,60.38,0,0,1-8.87-.66A59.61,59.61,0,1,1,433.76,148l7.61,10.25-55.9,41.45-15.21-20.51,33.4-24.77a34.07,34.07,0,1,0,12.23,45.22l22.51,12.08a59.72,59.72,0,0,1-52.4,31.4Z" transform="translate(0 0.38)"></path></svg>
                    <div editabletype="text " class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center ">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40 ">
                    <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center ">Copyright © Trendio</div>
                    <ul class="footer-ul w300 f-16 f-md-18 white-clr text-center text-md-right ">
                        <li><a href="https://support.bizomart.com/" class="white-clr t-decoration-none">Contact</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/privacy-policy.html " class="white-clr t-decoration-none ">Privacy</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/terms-of-service.html " class="white-clr t-decoration-none ">T&amp;C</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/disclaimer.html " class="white-clr t-decoration-none ">Disclaimer</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/gdpr.html " class="white-clr t-decoration-none ">GDPR</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/dmca.html " class="white-clr t-decoration-none ">DMCA</a> <span class="px5 ">|</span> </li>
                        <li><a href="http://trendio.biz/legal/anti-spam.html " class="white-clr t-decoration-none ">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
	
	<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-885892634').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-885892634")) {
                document.getElementById("af-form-885892634").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-885892634")) {
                document.getElementById("af-body-885892634").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-885892634")) {
                document.getElementById("af-header-885892634").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-885892634")) {
                document.getElementById("af-footer-885892634").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->
	<!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->

</body>

</html>